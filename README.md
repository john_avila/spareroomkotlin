# spareroom-android-app
SpareRoom Android Application

## Build status
Development: [![Nevercode build status](https://app.nevercode.io/api/projects/417d74c0-843a-4185-9dd0-8f3fcb0e8e54/workflows/90a54208-9835-4357-91e1-266ef9897529/status_badge.svg?branch=Development&style=shields)](https://app.nevercode.io/#/project/417d74c0-843a-4185-9dd0-8f3fcb0e8e54/workflow/90a54208-9835-4357-91e1-266ef9897529/latestBuild?branch=Development)

Master: [![Nevercode build status](https://app.nevercode.io/api/projects/417d74c0-843a-4185-9dd0-8f3fcb0e8e54/workflows/a441563b-25cd-4697-9eb2-04c356d35eb4/status_badge.svg?branch=master&style=shields)](https://app.nevercode.io/#/project/417d74c0-843a-4185-9dd0-8f3fcb0e8e54/workflow/a441563b-25cd-4697-9eb2-04c356d35eb4/latestBuild?branch=master)
