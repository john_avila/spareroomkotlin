object Versions {

    const val COMPILE_SDK = 28
    const val MIN_SDK = 16
    const val TARGET_SDK = 28

    const val BROWSER = "1.0.0-rc02"
    const val GRID_LAYOUT = "1.0.0-rc02"
    const val MATERIAL_DESIGN = "1.0.0-rc01"
    const val LIFECYCLE_EXTENSIONS = "2.0.0-rc01"
    const val PLAY_SERVICES_MAPS = "15.0.1"
    const val PLAY_SERVICES_ANALYTICS = "16.0.3"
    const val PLAY_SERVICES_LOCATION = "15.0.1"
    const val FIREBASE_CORE = "16.0.3"
    const val FIREBASE_MESSAGING = "17.3.0"
    const val CONSTRAINT_LAYOUT = "1.1.3"
    const val DAGGER = "2.16"
    const val LEAK_CANARY = "1.6.1"
    const val OKHTTP = "3.11.0"
    const val CRASHLYTICS = "2.9.5"
    const val MAPS_UTIL = "0.5"
    const val MULTIDEX = "2.0.0"
    const val RX_ANDROID = "2.1.0"
    const val RX_JAVA = "2.2.1"
    const val GSON = "2.8.5"
    const val APPS_FLYER = "4.8.15"
    const val INSTALL_REFERRER = "1.0"
    const val MOCKITO = "2.21.0"
    const val OKIO = "2.0.0"
    const val ROBOLECTRIC = "3.8"
    // TODO update facebook and glide
    const val FACEBOOK = "4.17.0"
    const val GLIDE = "3.7.0"
    const val KOTLIN = "1.2.60"
    const val CI_BUILD_NUMBER = "NEVERCODE_BUILD_NUMBER"
    const val JAVA = 1.8
    const val FABRIC_PLUGIN = "1.25.4"
    const val GRADLE_PLUGIN = "3.2.0-rc02"
    const val GOOGLE_SERVICES_PLUGIN = "4.1.0"
    const val FIREBASE_PLUGIN = "1.1.5"

}