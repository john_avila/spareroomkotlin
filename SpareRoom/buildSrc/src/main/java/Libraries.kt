object Libraries {

    const val GRID_LAYOUT = "androidx.gridlayout:gridlayout:${Versions.GRID_LAYOUT}"
    const val MATERIAL_DESIGN = "com.google.android.material:material:${Versions.MATERIAL_DESIGN}"
    const val BROWSER = "androidx.browser:browser:${Versions.BROWSER}"
    const val CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout:${Versions.CONSTRAINT_LAYOUT}"
    const val LIFECYCLE_EXTENSIONS = "androidx.lifecycle:lifecycle-extensions:${Versions.LIFECYCLE_EXTENSIONS}"

    const val MAPS_UTIL = "com.google.maps.android:android-maps-utils:${Versions.MAPS_UTIL}"
    const val PLAY_SERVICES_LOCATION = "com.google.android.gms:play-services-location:${Versions.PLAY_SERVICES_LOCATION}"
    const val PLAY_SERVICES_MAPS = "com.google.android.gms:play-services-maps:${Versions.PLAY_SERVICES_MAPS}"
    const val PLAY_SERVICES_ANALYTICS = "com.google.android.gms:play-services-analytics:${Versions.PLAY_SERVICES_ANALYTICS}"
    const val FIREBASE_MESSAGING = "com.google.firebase:firebase-messaging:${Versions.FIREBASE_MESSAGING}"
    const val FIREBASE_CORE = "com.google.firebase:firebase-core:${Versions.FIREBASE_CORE}"

    const val OKHTTP = "com.squareup.okhttp3:okhttp:${Versions.OKHTTP}"
    const val OKHTTP_URLCONNECTION = "com.squareup.okhttp3:okhttp-urlconnection:${Versions.OKHTTP}"
    const val OKIO = "com.squareup.okio:okio:${Versions.OKIO}"

    const val DAGGER = "com.google.dagger:dagger:${Versions.DAGGER}"
    const val DAGGER_COMPILER = "com.google.dagger:dagger-compiler:${Versions.DAGGER}"
    const val DAGGER_ANDROID = "com.google.dagger:dagger-android:${Versions.DAGGER}"
    const val DAGGER_ANDROID_SUPPORT = "com.google.dagger:dagger-android-support:${Versions.DAGGER}"
    const val DAGGER_ANDROID_PROCESSOR = "com.google.dagger:dagger-android-processor:${Versions.DAGGER}"

    const val LEAK_CANARY = "com.squareup.leakcanary:leakcanary-android:${Versions.LEAK_CANARY}"
    const val LEAK_CANARY_NO_OP = "com.squareup.leakcanary:leakcanary-android-no-op:${Versions.LEAK_CANARY}"

    const val RX_ANDROID = "io.reactivex.rxjava2:rxandroid:${Versions.RX_ANDROID}"
    const val RX_JAVA = "io.reactivex.rxjava2:rxjava:${Versions.RX_JAVA}"

    const val GLIDE = "com.github.bumptech.glide:glide:${Versions.GLIDE}"
    const val GSON = "com.google.code.gson:gson:${Versions.GSON}"
    const val APPS_FLYER = "com.appsflyer:af-android-sdk:${Versions.APPS_FLYER}@aar"
    const val INSTALL_REFERRER = "com.android.installreferrer:installreferrer:${Versions.INSTALL_REFERRER}"
    const val KOTLIN_STD_LIB = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.KOTLIN}"
    const val MULTIDEX = "androidx.multidex:multidex:${Versions.MULTIDEX}"
    const val FACEBOOK = "com.facebook.android:facebook-android-sdk:${Versions.FACEBOOK}"
    const val CRASHLYTICS = "com.crashlytics.sdk.android:crashlytics:${Versions.CRASHLYTICS}@aar"

    // test
    const val MOCKITO = "org.mockito:mockito-core:${Versions.MOCKITO}"
    const val MOCK_WEBSERVER = "com.squareup.okhttp3:mockwebserver:${Versions.OKHTTP}"
    const val ROBOLECTRIC = "org.robolectric:robolectric:${Versions.ROBOLECTRIC}"

}