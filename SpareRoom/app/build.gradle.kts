import org.apache.commons.io.output.ByteArrayOutputStream

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("kotlin-kapt")
    id("io.fabric")
    id("com.google.gms.google-services")
}

android {
    compileSdkVersion(Versions.COMPILE_SDK)

    defaultConfig {
        minSdkVersion(Versions.MIN_SDK)
        targetSdkVersion(Versions.TARGET_SDK)
        vectorDrawables.useSupportLibrary = true
        multiDexEnabled = true
    }

    flavorDimensions("defaultFlavourDimension")

    productFlavors {
        val appVersionCode = getVersionCode()
        productFlavors.create("spareroomuk") {
            applicationId = "com.spareroom.spareroomuk"
            versionCode = appVersionCode
            versionName = getVersionName("uk")
            dimension = "defaultFlavourDimension"
        }

        productFlavors.create("spareroomus") {
            applicationId = "com.spareroom.spareroomus"
            versionCode = appVersionCode
            versionName = getVersionName("us")
            dimension = "defaultFlavourDimension"
        }
    }

    signingConfigs {
        signingConfigs.create("releaseuk") {
            if (System.getenv("CI")?.isNotEmpty() == true) {
                storeFile = file(System.getenv("KEYSTORE_PATH_UK"))
                storePassword = System.getenv("KEYSTORE_PASSWORD_UK")
                keyAlias = System.getenv("KEY_ALIAS_UK")
                keyPassword = System.getenv("KEY_PASSWORD_UK")
            }
        }

        signingConfigs.create("releaseus") {
            if (System.getenv("CI")?.isNotEmpty() == true) {
                storeFile = file(System.getenv("KEYSTORE_PATH_US"))
                storePassword = System.getenv("KEYSTORE_PASSWORD_US")
                keyAlias = System.getenv("KEY_ALIAS_US")
                keyPassword = System.getenv("KEY_PASSWORD_US")
            }
        }

        getByName("debug") {
            storeFile = file("debug.keystore")
            storePassword = "android"
            keyAlias = "androiddebugkey"
            keyPassword = "android"
        }
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            isShrinkResources = true
            isZipAlignEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            productFlavors.findByName("spareroomuk")?.signingConfig = signingConfigs.findByName("releaseuk")
            productFlavors.findByName("spareroomus")?.signingConfig = signingConfigs.findByName("releaseus")
        }

        getByName("debug") {
            versionNameSuffix = "-dev"
            setMatchingFallbacks("release")
            //ext.enableCrashlytics = false
        }
    }

/*    sourceSets {
        main {
            manifest.srcFile 'src/main/AndroidManifest.xml'
        }

        spareroomuk {
            res.srcDirs = ['src/spareroomuk/res']
        }

        spareroomus {
            res.srcDirs = ['src/spareroomus/res']
        }
    }*/

/*excludePackageNames(
    "META-INF/DEPENDENCIES.txt",
    "META-INF/LICENSE.txt",
    "META-INF/NOTICE.txt",
    "META-INF/NOTICE",
    "META-INF/LICENSE",
    "META-INF/DEPENDENCIES",
    "META-INF/notice.txt",
    "META-INF/license.txt",
    "META-INF/dependencies.txt",
    "META-INF/LGPL2.1")*/

    compileOptions {
        setSourceCompatibility(Versions.JAVA)
        setTargetCompatibility(Versions.JAVA)
    }

    lintOptions {
        lintConfig = file("lint.xml")
    }

    dexOptions {
        maxProcessCount = 3
        javaMaxHeapSize = "3584MB"
    }

    testOptions {
        unitTests.isIncludeAndroidResources = true
    }
}

dependencies {
    implementation(Libraries.GRID_LAYOUT)
    implementation(Libraries.MATERIAL_DESIGN)
    implementation(Libraries.BROWSER)
    implementation(Libraries.CONSTRAINT_LAYOUT)
    implementation(Libraries.FACEBOOK)

// Crashlytics reports crashes and analytics (Answers) to Fabric
    implementation(Libraries.CRASHLYTICS) {
        setTransitive(true)
    }

// Location
    implementation(Libraries.PLAY_SERVICES_LOCATION)

// Google maps
    implementation(Libraries.MAPS_UTIL)
    implementation(Libraries.PLAY_SERVICES_MAPS)

    implementation(Libraries.PLAY_SERVICES_ANALYTICS)
    implementation(Libraries.FIREBASE_MESSAGING)
    implementation(Libraries.FIREBASE_CORE)

// networking
    implementation(Libraries.OKHTTP)
    implementation(Libraries.OKHTTP_URLCONNECTION)
    implementation(Libraries.OKIO)

// dagger - dependency injection
    implementation(Libraries.DAGGER)
    implementation(Libraries.DAGGER_ANDROID)
    implementation(Libraries.DAGGER_ANDROID_SUPPORT)
    kapt(Libraries.DAGGER_ANDROID_PROCESSOR)
    kapt(Libraries.DAGGER_COMPILER)
    kaptTest(Libraries.DAGGER_COMPILER)

// glide - image loading
    implementation(Libraries.GLIDE)

// multi dex support (due to app containing more than 65536 method references)
    implementation(Libraries.MULTIDEX)

// leak canary - helps detect memory leaks
    debugImplementation(Libraries.LEAK_CANARY)
    releaseImplementation(Libraries.LEAK_CANARY_NO_OP)
    testImplementation(Libraries.LEAK_CANARY_NO_OP)

// view model and live data
    implementation(Libraries.LIFECYCLE_EXTENSIONS)

// rx - reactive programming
    implementation(Libraries.RX_ANDROID)
    implementation(Libraries.RX_JAVA)

// gson - parsing JSON
    implementation(Libraries.GSON)

// AppsFlyer - apps installation and events tracking
    implementation(Libraries.APPS_FLYER)

// Play Install Referrer (used by AppsFlyer)
    implementation(Libraries.INSTALL_REFERRER)

// Kotlin standard library
    implementation(Libraries.KOTLIN_STD_LIB)

// test - mocking classes
    testImplementation(Libraries.MOCKITO)

// test - mocking web server
    testImplementation(Libraries.MOCK_WEBSERVER)

// test - using Android classes on JVM
    testImplementation(Libraries.ROBOLECTRIC)
}

fun getVersionCode(): Int {
    val versionCode: String? = System.getenv(Versions.CI_BUILD_NUMBER)
// 1444 is where BB stopped and build number in Nevercode cannot be modified
    return if (versionCode.isNullOrEmpty()) 1 else Integer.parseInt(versionCode) + 1444
}

fun getVersionName(suffix: String): String {
    val versionNumber = getVersionNumberFromTheLatestGitTag()
    val versionCode = getVersionCode()
    val versionName = "${if (versionCode == 0) "" else versionNumber} $versionCode"
    return "$versionName-$suffix"
}

fun getVersionNumberFromTheLatestGitTag(): String {
    return try {
        val stdout = ByteArrayOutputStream()
        exec {
            commandLine("git", "describe", "--tags", "--exact-match")
            standardOutput = stdout
        }
        stdout.toString().trim() + "."
    } catch (ignored: Exception) {
        "#.#.#."
    }
}

