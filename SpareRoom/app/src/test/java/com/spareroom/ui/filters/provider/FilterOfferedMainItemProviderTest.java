package com.spareroom.ui.filters.provider;

import android.app.Application;

import com.spareroom.*;
import com.spareroom.model.business.SearchAdvertListProperties.*;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.Landlord;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.MinFlatmates;
import com.spareroom.ui.util.DateUtils;
import com.spareroom.ui.util.StringUtils;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Locale;

import static com.spareroom.ui.filters.provider.FilterOfferedMainItemProvider.OFFERED_UNSUITABLE;
import static com.spareroom.ui.filters.provider.FilterOfferedMainItemProvider.OFFERED_WITH_PHOTOS;
import static com.spareroom.ui.filters.provider.ItemProviderTestUtil.*;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class FilterOfferedMainItemProviderTest {

    private final Application application = RuntimeEnvironment.application;
    private final String BULLET_POINT_STRING = application.getString(R.string.bulletPoint);
    private final String MILES_STRING = application.getString(R.string.miles).toLowerCase();
    private final String BILLS_INCLUDED_STRING = application.getString(R.string.billsIncluded);
    private final String SHORT_TERM_LETS_STRING = application.getString(R.string.shortTermLets);
    private final String WEEKDAY_ONLY_ADVERTS_STRING = application.getString(R.string.weekdayOnlyAdverts);
    private final String ROOMS_STRING = application.getString(R.string.rooms);
    private final String STUDIOS_1_BED_FLATS_STRING = application.getString(R.string.studios1bedFlats);
    private final String WHOLE_PROPERTIES_STRING = application.getString(R.string.wholeProperties);
    private final String THREE_OR_MORE_BEDROOMS_STRING = application.getString(R.string.threeOrMoreBedrooms);
    private final String PARKING_STRING = application.getString(R.string.parking);
    private final String SHARED_LIVING_ROOM_STRING = application.getString(R.string.sharedLivingRoom);
    private final String DISABLED_ACCESS_STRING = application.getString(R.string.disabledAccess);
    private final String EN_SUITE_STRING = application.getString(R.string.enSuite);
    private final String COUPLES_STRING = application.getString(R.string.couples);
    private final String NOT_WITH_LANDLORD_STRING = application.getString(R.string.notWithLandlord);
    private final String EN_DASH_STRING = application.getString(R.string.en_dash);
    private final String FROM_STRING = application.getString(R.string.from);
    private final String UP_TO_STRING = application.getString(R.string.upTo);
    private final String FEMALES_STRING = application.getString(R.string.females);
    private final String BENEFITS_RECIPIENTS_STRING = application.getString(R.string.benefitsRecipients);
    private final String PETS_OWNERS_STRING = application.getString(R.string.petsOwners);
    private final String NON_SMOKERS_STRING = application.getString(R.string.nonSmokers);
    private final String AGENTS_STRING = application.getString(R.string.agents);
    private final String BETWEEN_STRING = application.getString(R.string.between);
    private final String CURRENCY_SYMBOL_STRING = application.getString(R.string.currency_symbol);
    private final String MONTHS_STRING = application.getString(R.string.months);
    private final String DOUBLE_BEDROOM_STRING = application.getString(R.string.doubleBedroom);
    private final String ALL_WEEK_ADVERTS_STRING = application.getString(R.string.allWeekAdverts);
    private final String YEARS_STRING = application.getString(R.string.years);
    private final String PROFESSIONALS_STRING = application.getString(R.string.professionals);
    private final String FLATMATES_STRING = application.getString(R.string.flatmates).toLowerCase();

    private FilterOfferedMainItemProvider provider;
    private final ItemProviderTestUtil itemProviderTestUtil = new ItemProviderTestUtil();
    private SubtitleUtil subtitleUtil;

    @Before
    public void setUp() {
        provider = new FilterOfferedMainItemProvider();
        provider.createNewList(new SearchAdvertListPropertiesOffered(), null);
        subtitleUtil = new SubtitleUtil(new DateUtils(Locale.getDefault(), application));
    }

    @Test
    public void get_whenRetrievingList_thenAllItemsAdded() {
        provider.createNewList(new SearchAdvertListPropertiesOffered(), null);
        assertEquals(15, provider.get().size());
    }

    @Test
    public void createNewList_whenEmptyPropertyList_thenDefaultFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);

        assertFalse(((CompoundButtonParams) itemProviderTestUtil.getParameter(provider.get(), OFFERED_UNSUITABLE)).selected());
        assertFalse(((CompoundButtonParams) itemProviderTestUtil.getParameter(provider.get(), OFFERED_WITH_PHOTOS)).selected());
    }

    @Test
    public void createNewList_whenFullPropertyList_thenFilledFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);

        assertTrue(((CompoundButtonParams) itemProviderTestUtil.getParameter(provider.get(), OFFERED_UNSUITABLE)).selected());
        assertTrue(((CompoundButtonParams) itemProviderTestUtil.getParameter(provider.get(), OFFERED_WITH_PHOTOS)).selected());
    }

    @Test
    public void saveProperties_whenEmptyList_andEmptyProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        testEmptyList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenEmptyList_andFullProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        testEmptyList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenFullList_andEmptyProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        testFullList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenFullList_andFullProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        testFullList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered());
    }

    @Test
    public void getSearchRadiusSubtitle_whenAllSet_thenWholeString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered();
        // "Manchester + 15 miles"
        assertEquals(
                DEFAULT_WHERE_VALUE + " + " + DEFAULT_MILES_FROM_MAX_INT_VALUE + " " + MILES_STRING,
                provider.getSearchRadiusSubtitle(savedProperties));
    }

    @Test
    public void getSearchRadiusSubtitle_whenMilesNotSet_thenOnlyWhere() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        savedProperties.setWhere(DEFAULT_WHERE_VALUE);
        // "Manchester"
        assertEquals(DEFAULT_WHERE_VALUE, provider.getSearchRadiusSubtitle(savedProperties));
    }

    @Test
    public void getSearchRadiusSubtitle_whenNotSet_thenEmptyString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        assertEquals("", provider.getSearchRadiusSubtitle(savedProperties));
    }

    @Test
    public void getMonthlyBudgetSubtitle_whenAllSet_thenWholeString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered();

        // "Between $10 – $100  •  Bills included"
        assertEquals(
                BETWEEN_STRING + " "
                        + CURRENCY_SYMBOL_STRING + DEFAULT_MIN_RENT_INT_VALUE
                        + EN_DASH_STRING
                        + CURRENCY_SYMBOL_STRING + DEFAULT_MAX_RENT_INT_VALUE
                        + BULLET_POINT_STRING
                        + BILLS_INCLUDED_STRING,
                provider.getMonthlyBudgetSubtitle(savedProperties));
    }

    @Test
    public void getMonthlyBudgetSubtitle_whenSomeSet_thenSomeString() {
        // "Up to £150"
        SearchAdvertListPropertiesOffered upToProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        upToProperties.maxMonthlyRent(DEFAULT_MAX_RENT_INT_VALUE);
        assertEquals(
                UP_TO_STRING + " " + CURRENCY_SYMBOL_STRING + DEFAULT_MAX_RENT_INT_VALUE,
                provider.getMonthlyBudgetSubtitle(upToProperties));

        // "From £390"
        SearchAdvertListPropertiesOffered fromProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        fromProperties.minMonthlyRent(DEFAULT_MIN_RENT_INT_VALUE);
        assertEquals(
                FROM_STRING + " " + CURRENCY_SYMBOL_STRING + DEFAULT_MIN_RENT_INT_VALUE,
                provider.getMonthlyBudgetSubtitle(fromProperties));

        // "Bills included"
        SearchAdvertListPropertiesOffered billsIncludedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        billsIncludedProperties.billsInc(true);
        assertEquals(BILLS_INCLUDED_STRING, provider.getMonthlyBudgetSubtitle(billsIncludedProperties));
    }

    @Test
    public void getMonthlyBudgetSubtitle_whenNotSet_thenEmptyString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        assertTrue(StringUtils.isNullOrEmpty(provider.getMonthlyBudgetSubtitle(savedProperties)));
    }

    @Test
    public void getAvailabilitySubtitle_whenAllSet_thenWholeString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered();
        // "2017-02-05  •  8 months – 11 months  •  Short term lets  •  Weekday-only adverts"
        assertEquals(
                subtitleUtil.getMoveInText(DEFAULT_AVAILABLE_FROM_VALUE) + BULLET_POINT_STRING
                        + BETWEEN_STRING + " "
                        + DEFAULT_MIN_TERM_VALUE.getValue() + EN_DASH_STRING
                        + DEFAULT_MAX_TERM_VALUE.getValue() + " " + MONTHS_STRING + BULLET_POINT_STRING
                        + SHORT_TERM_LETS_STRING + BULLET_POINT_STRING
                        + WEEKDAY_ONLY_ADVERTS_STRING,
                provider.getAvailabilitySubtitle(savedProperties));
    }

    @Test
    public void getAvailabilitySubtitle_whenSomeSet_thenSomeString() {
        // "2017-02-05"
        SearchAdvertListPropertiesOffered availableFromProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        availableFromProperties.setAvailableFrom(DEFAULT_AVAILABLE_FROM_VALUE);
        assertEquals(subtitleUtil.getMoveInText(DEFAULT_AVAILABLE_FROM_VALUE), provider.getAvailabilitySubtitle(availableFromProperties));

        // "From 8 months"
        SearchAdvertListPropertiesOffered someProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        someProperties.setMinTerm(DEFAULT_MIN_TERM_VALUE);
        assertEquals(
                FROM_STRING + " " + DEFAULT_MIN_TERM_VALUE.getValue() + " " + MONTHS_STRING,
                provider.getAvailabilitySubtitle(someProperties));

        // "From 8 months  •  All-week adverts"
        someProperties.setDaysOfWeekAvailable(DaysOfWeek.SEVEN_DAYS);
        assertEquals(
                FROM_STRING + " " + DEFAULT_MIN_TERM_VALUE.getValue() + " " + MONTHS_STRING + BULLET_POINT_STRING + ALL_WEEK_ADVERTS_STRING,
                provider.getAvailabilitySubtitle(someProperties));
    }

    @Test
    public void getAvailabilitySubtitle_whenNotSet_thenEmptyString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        assertEquals("", provider.getAvailabilitySubtitle(savedProperties));
    }

    @Test
    public void getPropertyTypeSubtitle_whenAllSet_thenWholeString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered();
        // "Rooms  •  Studios + 1 bed flats  •  Whole properties  •  3 or more bedrooms  •  Parking  •  Shared living room  •  Disabled access"
        assertEquals(
                ROOMS_STRING + BULLET_POINT_STRING
                        + STUDIOS_1_BED_FLATS_STRING + BULLET_POINT_STRING
                        + WHOLE_PROPERTIES_STRING + BULLET_POINT_STRING
                        + THREE_OR_MORE_BEDROOMS_STRING + BULLET_POINT_STRING
                        + PARKING_STRING + BULLET_POINT_STRING
                        + SHARED_LIVING_ROOM_STRING + BULLET_POINT_STRING
                        + DISABLED_ACCESS_STRING,
                provider.getPropertyTypeSubtitle(savedProperties));
    }

    @Test
    public void getPropertyTypeSubtitle_whenSomeSet_thenSomeString() {
        SearchAdvertListPropertiesOffered someProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        // "Rooms"
        someProperties.setShowRooms(true);
        assertEquals(ROOMS_STRING, provider.getPropertyTypeSubtitle(someProperties));
        // "Rooms  •  Whole properties"
        someProperties.setShowWholeProperty(true);
        assertEquals(ROOMS_STRING + BULLET_POINT_STRING + WHOLE_PROPERTIES_STRING, provider.getPropertyTypeSubtitle(someProperties));
        // "Rooms  •  Whole properties  •  Parking  •  Shared living room  •  Parking"
        someProperties.setParking(true);
        assertEquals(
                ROOMS_STRING + BULLET_POINT_STRING + WHOLE_PROPERTIES_STRING + BULLET_POINT_STRING + PARKING_STRING,
                provider.getPropertyTypeSubtitle(someProperties));
        // "Rooms  •  Whole properties  •  Parking  •  Shared living room  •  Parking  •  Shared living room"
        someProperties.setLivingRoom(true);
        assertEquals(
                ROOMS_STRING + BULLET_POINT_STRING
                        + WHOLE_PROPERTIES_STRING + BULLET_POINT_STRING
                        + PARKING_STRING + BULLET_POINT_STRING
                        + SHARED_LIVING_ROOM_STRING,
                provider.getPropertyTypeSubtitle(someProperties));
    }

    @Test
    public void getPropertyTypeSubtitle_whenNotSet_thenEmptyString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        assertEquals("", provider.getPropertyTypeSubtitle(savedProperties));
    }

    @Test
    public void getBedroomTypeSubtitle_whenAllSet_thenWholeString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered();
        // "Double bedroom  •  En-suite"
        assertEquals(DOUBLE_BEDROOM_STRING + " with " + EN_SUITE_STRING.toLowerCase(), provider.getBedroomTypeSubtitle(savedProperties));
    }

    @Test
    public void getBedroomTypeSubtitle_whenSomeSet_thenSomeString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        // "Double bedroom
        savedProperties.setRoomType(RoomTypes.DOUBLE);
        assertEquals(DOUBLE_BEDROOM_STRING, provider.getBedroomTypeSubtitle(savedProperties));

        // "En-suite
        savedProperties.setRoomType(RoomTypes.NOT_SET);
        savedProperties.setEnSuite(true);
        assertEquals(EN_SUITE_STRING, provider.getBedroomTypeSubtitle(savedProperties));
    }

    @Test
    public void getBedroomTypeSubtitle_whenNotSet_thenEmptyString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        assertEquals("", provider.getBedroomTypeSubtitle(savedProperties));
    }

    @Test
    public void getSharingWithSubtitle_whenAllSet_thenWholeString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered();
        // "18 – 99 years  •  LGBTQ  •  Professionals  •  5 – 6 roommates  •  Not with landlord"
        assertEquals(
                BETWEEN_STRING + " "
                        + DEFAULT_MIN_AGE_REQ_INT_VALUE + EN_DASH_STRING
                        + DEFAULT_MAX_AGE_REQ_INT_VALUE + " " + YEARS_STRING + BULLET_POINT_STRING
                        + FEMALES_STRING + BULLET_POINT_STRING
                        + PROFESSIONALS_STRING + BULLET_POINT_STRING
                        + BETWEEN_STRING + " "
                        + DEFAULT_MIN_FLATMATES_VALUE.getValue() + EN_DASH_STRING
                        + DEFAULT_MAX_FLATMATES_VALUE.getValue() + " " + FLATMATES_STRING + BULLET_POINT_STRING
                        + NOT_WITH_LANDLORD_STRING,
                provider.getSharingWithSubtitle(savedProperties));
    }

    @Test
    public void getSharingWithSubtitle_whenSomeSet_thenSomeString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        // "From 18 years"
        savedProperties.setMinAge(DEFAULT_MIN_AGE_REQ_INT_VALUE);
        assertEquals(FROM_STRING + " " + DEFAULT_MIN_AGE_REQ_INT_VALUE + " " + YEARS_STRING, provider.getSharingWithSubtitle(savedProperties));
        // "From 18 years  •  From 5 roommates"
        savedProperties.setMinFlatmates(MinFlatmates.FIVE);
        assertEquals(
                FROM_STRING + " " + DEFAULT_MIN_AGE_REQ_INT_VALUE + " " + YEARS_STRING + BULLET_POINT_STRING
                        + FROM_STRING + " " + DEFAULT_MIN_FLATMATES_VALUE.getValue() + " " + FLATMATES_STRING,
                provider.getSharingWithSubtitle(savedProperties));
        // "From 18 years  •  From 5 roommates  •  Not with landlord"
        savedProperties.setLandlord(Landlord.LIVE_OUT);
        assertEquals(
                FROM_STRING + " " + DEFAULT_MIN_AGE_REQ_INT_VALUE + " " + YEARS_STRING + BULLET_POINT_STRING
                        + FROM_STRING + " " + DEFAULT_MIN_FLATMATES_VALUE.getValue() + " " + FLATMATES_STRING + BULLET_POINT_STRING
                        + NOT_WITH_LANDLORD_STRING,
                provider.getSharingWithSubtitle(savedProperties));
    }

    @Test
    public void getSharingWithSubtitle_whenNotSet_thenEmptyString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        assertEquals("", provider.getSharingWithSubtitle(savedProperties));
    }

    @Test
    public void getMinMaxFlatmatesText_whenAllSet_thenWholeString() {
        // "Between 5 – 6 roommates"
        assertEquals(
                BETWEEN_STRING + " " + DEFAULT_MIN_FLATMATES_VALUE.getValue() + EN_DASH_STRING + DEFAULT_MAX_FLATMATES_VALUE.getValue() + " " + FLATMATES_STRING,
                provider.getMinMaxFlatmatesText(DEFAULT_MIN_FLATMATES_VALUE.getValue(), DEFAULT_MAX_FLATMATES_VALUE.getValue()));
    }

    @Test
    public void getMinMaxFlatmatesText_whenNotSet_thenEmptyString() {
        assertEquals("", provider.getMinMaxFlatmatesText(null, null));
    }

    @Test
    public void getMinMaxFlatmatesText_whenMinNull_thenUpToString() {
        // "Up to 6 roommates"
        assertEquals(
                UP_TO_STRING + " " + DEFAULT_MAX_FLATMATES_VALUE.getValue() + " " + FLATMATES_STRING,
                provider.getMinMaxFlatmatesText(null, DEFAULT_MAX_FLATMATES_VALUE.getValue()));
    }

    @Test
    public void getMinMaxFlatmatesText_whenMaxNull_thenUpToString() {
        // "From 5 roommates"
        assertEquals(
                FROM_STRING + " " + DEFAULT_MIN_FLATMATES_VALUE.getValue() + " " + FLATMATES_STRING,
                provider.getMinMaxFlatmatesText(DEFAULT_MIN_FLATMATES_VALUE.getValue(), null));
    }

    @Test
    public void getSuitableForSubtitle_whenAllSet_thenWholeString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered();
        // "Couples  •  Benefits recipients  •  Pet owners  •  Non-smokers"
        assertEquals(
                COUPLES_STRING + BULLET_POINT_STRING
                        + BENEFITS_RECIPIENTS_STRING + BULLET_POINT_STRING
                        + PETS_OWNERS_STRING + BULLET_POINT_STRING
                        + NON_SMOKERS_STRING,
                provider.getSuitableForSubtitle(savedProperties));
    }

    @Test
    public void getSuitableForSubtitle_whenSomeSet_thenSomeString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        // "Benefits recipients"
        savedProperties.setBenefits(true);
        assertEquals(BENEFITS_RECIPIENTS_STRING, provider.getSuitableForSubtitle(savedProperties));
        // "Benefits recipients  •  Non-smokers"
        savedProperties.setSmoking(Smoking.NO);
        assertEquals(BENEFITS_RECIPIENTS_STRING + BULLET_POINT_STRING + NON_SMOKERS_STRING, provider.getSuitableForSubtitle(savedProperties));
    }

    @Test
    public void getSuitableForSubtitle_whenNotSet_thenEmptyString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        assertEquals("", provider.getSuitableForSubtitle(savedProperties));
    }

    @Test
    public void getPostedBySubtitle_whenAllSet_thenWholeString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered();
        // "Agents"
        assertEquals(AGENTS_STRING, provider.getPostedBySubtitle(savedProperties));
    }

    @Test
    public void getPostedBySubtitle_whenNotSet_thenEmptyString() {
        SearchAdvertListPropertiesOffered savedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        assertEquals("", provider.getPostedBySubtitle(savedProperties));
    }

    private void testFullList(final SearchAdvertListPropertiesOffered properties) {
        provider.updateProperties(properties, true);

        assertTrue(properties.getIncludeHidden());
        assertTrue(properties.getPhotoAdvertsOnly());

    }

    private void testEmptyList(final SearchAdvertListPropertiesOffered properties) {
        provider.updateProperties(properties, true);

        assertFalse(properties.getIncludeHidden());
        assertFalse(properties.getPhotoAdvertsOnly());
    }

}
