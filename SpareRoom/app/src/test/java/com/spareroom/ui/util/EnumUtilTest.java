package com.spareroom.ui.util;

import com.spareroom.lib.util.EnumUtil;
import com.spareroom.model.business.SearchAdvertListProperties.*;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.*;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted.Couples;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted.MaxOtherAreas;

import org.junit.Test;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;

@Config(manifest = Config.NONE)
public class EnumUtilTest {

    @Test
    public void searchItemByValue_whenValidValueToSearch_thenReturnValidOption() {

        assertEquals(Couples.NOT_SET, EnumUtil.searchItemByValue(Couples.values(), Couples.NOT_SET.getValue(), Couples.NOT_SET));
        assertEquals(Couples.NO, EnumUtil.searchItemByValue(Couples.values(), Couples.NO.getValue(), Couples.NOT_SET));

        assertEquals(MaxOtherAreas.NOT_SET, EnumUtil.searchItemByValue(MaxOtherAreas.values(), MaxOtherAreas.NOT_SET.getValue(), MaxOtherAreas.NOT_SET));
        assertEquals(MaxOtherAreas.UP_TO_FIVE, EnumUtil.searchItemByValue(MaxOtherAreas.values(), MaxOtherAreas.UP_TO_FIVE.getValue(), MaxOtherAreas.NOT_SET));

        assertEquals(LocationType.NOT_SET, EnumUtil.searchItemByValue(LocationType.values(), LocationType.NOT_SET.getValue(), LocationType.NOT_SET));
        assertEquals(LocationType.COMMUTER, EnumUtil.searchItemByValue(LocationType.values(), LocationType.COMMUTER.getValue(), LocationType.NOT_SET));

        assertEquals(BillsInc.NOT_SET, EnumUtil.searchItemByValue(BillsInc.values(), BillsInc.NOT_SET.getValue(), BillsInc.NOT_SET));
        assertEquals(BillsInc.SOME, EnumUtil.searchItemByValue(BillsInc.values(), BillsInc.SOME.getValue(), BillsInc.NOT_SET));

        assertEquals(NoOfRooms.NOT_SET, EnumUtil.searchItemByValue(NoOfRooms.values(), NoOfRooms.NOT_SET.getValue(), NoOfRooms.NOT_SET));
        assertEquals(NoOfRooms.MIN_THREE, EnumUtil.searchItemByValue(NoOfRooms.values(), NoOfRooms.MIN_THREE.getValue(), NoOfRooms.NOT_SET));

        assertEquals(MinFlatmates.NOT_SET, EnumUtil.searchItemByValue(MinFlatmates.values(), MinFlatmates.NOT_SET.getValue(), MinFlatmates.NOT_SET));
        assertEquals(MinFlatmates.FOUR, EnumUtil.searchItemByValue(MinFlatmates.values(), MinFlatmates.FOUR.getValue(), MinFlatmates.NOT_SET));

        assertEquals(MaxFlatmates.NOT_SET, EnumUtil.searchItemByValue(MaxFlatmates.values(), MaxFlatmates.NOT_SET.getValue(), MaxFlatmates.NOT_SET));
        assertEquals(MaxFlatmates.MORE_THAN_SIX, EnumUtil.searchItemByValue(MaxFlatmates.values(), MaxFlatmates.MORE_THAN_SIX.getValue(), MaxFlatmates.NOT_SET));

        assertEquals(Landlord.NOT_SET, EnumUtil.searchItemByValue(Landlord.values(), Landlord.NOT_SET.getValue(), Landlord.NOT_SET));
        assertEquals(Landlord.LIVE_OUT, EnumUtil.searchItemByValue(Landlord.values(), Landlord.LIVE_OUT.getValue(), Landlord.NOT_SET));

        assertEquals(RoomsFor.NOT_SET, EnumUtil.searchItemByValue(RoomsFor.values(), RoomsFor.NOT_SET.getValue(), RoomsFor.NOT_SET));
        assertEquals(RoomsFor.FEMALES, EnumUtil.searchItemByValue(RoomsFor.values(), RoomsFor.FEMALES.getValue(), RoomsFor.NOT_SET));

        assertEquals(PostedBy.NOT_SET, EnumUtil.searchItemByValue(PostedBy.values(), PostedBy.NOT_SET.getValue(), PostedBy.NOT_SET));
        assertEquals(PostedBy.AGENTS, EnumUtil.searchItemByValue(PostedBy.values(), PostedBy.AGENTS.getValue(), PostedBy.NOT_SET));

        assertEquals(MinTerm.NOT_SET, EnumUtil.searchItemByValue(MinTerm.values(), MinTerm.NOT_SET.getValue(), MinTerm.NOT_SET));
        assertEquals(MinTerm.ONE, EnumUtil.searchItemByValue(MinTerm.values(), MinTerm.ONE.getValue(), MinTerm.NOT_SET));
        assertEquals(MinTerm.TWO, EnumUtil.searchItemByValue(MinTerm.values(), MinTerm.TWO.getValue(), MinTerm.NOT_SET));

        assertEquals(MaxTerm.NOT_SET, EnumUtil.searchItemByValue(MaxTerm.values(), MaxTerm.NOT_SET.getValue(), MaxTerm.NOT_SET));
        assertEquals(MaxTerm.SEVEN, EnumUtil.searchItemByValue(MaxTerm.values(), MaxTerm.SEVEN.getValue(), MaxTerm.NOT_SET));

        assertEquals(DaysOfWeek.NOT_SET, EnumUtil.searchItemByValue(DaysOfWeek.values(), DaysOfWeek.NOT_SET.getValue(), DaysOfWeek.NOT_SET));
        assertEquals(DaysOfWeek.WEEKENDS, EnumUtil.searchItemByValue(DaysOfWeek.values(), DaysOfWeek.WEEKENDS.getValue(), DaysOfWeek.NOT_SET));

        assertEquals(Smoking.NOT_SET, EnumUtil.searchItemByValue(Smoking.values(), Smoking.NOT_SET.getValue(), Smoking.NOT_SET));
        assertEquals(Smoking.NO, EnumUtil.searchItemByValue(Smoking.values(), Smoking.NO.getValue(), Smoking.NOT_SET));
        assertEquals(Smoking.YES, EnumUtil.searchItemByValue(Smoking.values(), Smoking.YES.getValue(), Smoking.NOT_SET));

        assertEquals(RoomTypes.NOT_SET, EnumUtil.searchItemByValue(RoomTypes.values(), RoomTypes.NOT_SET.getValue(), RoomTypes.NOT_SET));
        assertEquals(RoomTypes.DOUBLE, EnumUtil.searchItemByValue(RoomTypes.values(), RoomTypes.DOUBLE.getValue(), RoomTypes.NOT_SET));

        assertEquals(ShareType.NOT_SET, EnumUtil.searchItemByValue(ShareType.values(), ShareType.NOT_SET.getValue(), ShareType.NOT_SET));
        assertEquals(ShareType.PROFESSIONALS, EnumUtil.searchItemByValue(ShareType.values(), ShareType.PROFESSIONALS.getValue(), ShareType.NOT_SET));

        assertEquals(GenderFilter.NOT_SET, EnumUtil.searchItemByValue(GenderFilter.values(), GenderFilter.NOT_SET.getValue(), GenderFilter.NOT_SET));
        assertEquals(GenderFilter.MIXED, EnumUtil.searchItemByValue(GenderFilter.values(), GenderFilter.MIXED.getValue(), GenderFilter.NOT_SET));
    }

    @Test
    public void searchItemByValue_whenWrongValue_thenReturnDefaultValue() {
        assertEquals(Couples.NOT_SET, EnumUtil.searchItemByValue(Couples.values(), "", Couples.NOT_SET));
        assertEquals(Couples.NOT_SET, EnumUtil.searchItemByValue(Couples.values(), "asdgadfh", Couples.NOT_SET));
        assertEquals(MaxOtherAreas.NOT_SET, EnumUtil.searchItemByValue(MaxOtherAreas.values(), "", MaxOtherAreas.NOT_SET));
        assertEquals(MaxOtherAreas.NOT_SET, EnumUtil.searchItemByValue(MaxOtherAreas.values(), "asdgadfh", MaxOtherAreas.NOT_SET));
        assertEquals(LocationType.NOT_SET, EnumUtil.searchItemByValue(LocationType.values(), "", LocationType.NOT_SET));
        assertEquals(LocationType.NOT_SET, EnumUtil.searchItemByValue(LocationType.values(), "asdgadfh", LocationType.NOT_SET));
        assertEquals(BillsInc.NOT_SET, EnumUtil.searchItemByValue(BillsInc.values(), "", BillsInc.NOT_SET));
        assertEquals(BillsInc.NOT_SET, EnumUtil.searchItemByValue(BillsInc.values(), "asdgadfh", BillsInc.NOT_SET));
        assertEquals(NoOfRooms.NOT_SET, EnumUtil.searchItemByValue(NoOfRooms.values(), "", NoOfRooms.NOT_SET));
        assertEquals(NoOfRooms.NOT_SET, EnumUtil.searchItemByValue(NoOfRooms.values(), "asdgadfh", NoOfRooms.NOT_SET));
        assertEquals(MinFlatmates.NOT_SET, EnumUtil.searchItemByValue(MinFlatmates.values(), "", MinFlatmates.NOT_SET));
        assertEquals(MinFlatmates.NOT_SET, EnumUtil.searchItemByValue(MinFlatmates.values(), "asdgadfh", MinFlatmates.NOT_SET));
        assertEquals(MaxFlatmates.NOT_SET, EnumUtil.searchItemByValue(MaxFlatmates.values(), "", MaxFlatmates.NOT_SET));
        assertEquals(MaxFlatmates.NOT_SET, EnumUtil.searchItemByValue(MaxFlatmates.values(), "asdgadfh", MaxFlatmates.NOT_SET));
        assertEquals(Landlord.NOT_SET, EnumUtil.searchItemByValue(Landlord.values(), "", Landlord.NOT_SET));
        assertEquals(Landlord.NOT_SET, EnumUtil.searchItemByValue(Landlord.values(), "asdgadfh", Landlord.NOT_SET));
        assertEquals(RoomsFor.NOT_SET, EnumUtil.searchItemByValue(RoomsFor.values(), "", RoomsFor.NOT_SET));
        assertEquals(RoomsFor.NOT_SET, EnumUtil.searchItemByValue(RoomsFor.values(), "asdgadfh", RoomsFor.NOT_SET));
        assertEquals(PostedBy.NOT_SET, EnumUtil.searchItemByValue(PostedBy.values(), "", PostedBy.NOT_SET));
        assertEquals(PostedBy.NOT_SET, EnumUtil.searchItemByValue(PostedBy.values(), "asdgadfh", PostedBy.NOT_SET));
        assertEquals(MinTerm.NOT_SET, EnumUtil.searchItemByValue(MinTerm.values(), "", MinTerm.NOT_SET));
        assertEquals(MinTerm.NOT_SET, EnumUtil.searchItemByValue(MinTerm.values(), "asdgadfh", MinTerm.NOT_SET));
        assertEquals(MaxTerm.NOT_SET, EnumUtil.searchItemByValue(MaxTerm.values(), "", MaxTerm.NOT_SET));
        assertEquals(MaxTerm.NOT_SET, EnumUtil.searchItemByValue(MaxTerm.values(), "asdgadfh", MaxTerm.NOT_SET));
        assertEquals(DaysOfWeek.NOT_SET, EnumUtil.searchItemByValue(DaysOfWeek.values(), "", DaysOfWeek.NOT_SET));
        assertEquals(DaysOfWeek.NOT_SET, EnumUtil.searchItemByValue(DaysOfWeek.values(), "asdgadfh", DaysOfWeek.NOT_SET));
        assertEquals(Smoking.NOT_SET, EnumUtil.searchItemByValue(Smoking.values(), "", Smoking.NOT_SET));
        assertEquals(Smoking.NOT_SET, EnumUtil.searchItemByValue(Smoking.values(), "asdgadfh", Smoking.NOT_SET));
        assertEquals(RoomTypes.NOT_SET, EnumUtil.searchItemByValue(RoomTypes.values(), "", RoomTypes.NOT_SET));
        assertEquals(RoomTypes.NOT_SET, EnumUtil.searchItemByValue(RoomTypes.values(), "asdgadfh", RoomTypes.NOT_SET));
        assertEquals(ShareType.NOT_SET, EnumUtil.searchItemByValue(ShareType.values(), "", ShareType.NOT_SET));
        assertEquals(ShareType.NOT_SET, EnumUtil.searchItemByValue(ShareType.values(), "asdgadfh", ShareType.NOT_SET));
        assertEquals(GenderFilter.NOT_SET, EnumUtil.searchItemByValue(GenderFilter.values(), "", GenderFilter.NOT_SET));
        assertEquals(GenderFilter.NOT_SET, EnumUtil.searchItemByValue(GenderFilter.values(), "asdgadfh", GenderFilter.NOT_SET));
    }

}
