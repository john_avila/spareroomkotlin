package com.spareroom.ui.util

import android.app.*
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Icon
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.*
import androidx.core.content.ContextCompat
import com.spareroom.*
import com.spareroom.TestUtils.restoreCurrentApiLevel
import com.spareroom.TestUtils.setApiLevel
import com.spareroom.controller.AppVersion
import org.junit.*
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

private const val MESSAGE_NOTIFICATION_ID = 1
private const val VIDEO_UPLOAD_NOTIFICATION_ID = 2

private val MESSAGES_CHANNEL_ID = AppVersion.appId() + "_messages"
private val VIDEO_UPLOAD_CHANNEL_ID = AppVersion.appId() + "_videoUpload"

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [(TestConstants.CONFIG_SDK)], application = TestApplication::class)
class NotificationUtilsTest {

    private lateinit var app: Application
    private lateinit var notificationManager: NotificationManager

    private val largeIcon = mock(Bitmap::class.java)
    private val group = "group"
    private val title = "title"
    private val message = "message"
    private val pendingIntent = mock(PendingIntent::class.java)

    @Before
    fun setUp() {
        internalSetUp()
    }

    @After
    fun tearDown() {
        restoreCurrentApiLevel()
    }

    @Test
    fun closeNotifications_shouldCloseAllNotifications() {
        createNotifications(notificationManager)

        assertNotificationsShown()

        NotificationUtils.closeNotifications(app)
        verify(notificationManager, times(1)).cancel(MESSAGE_NOTIFICATION_ID)
        verify(notificationManager, times(1)).cancel(VIDEO_UPLOAD_NOTIFICATION_ID)
        assertEquals(0, notificationManager.activeNotifications.size)
    }

    @Test
    fun createNotificationChannels_shouldCreateNoChannels_whenAndroidIsBelowOreo() {
        for (apiLevel in Build.VERSION_CODES.JELLY_BEAN until Build.VERSION_CODES.O) {
            setApiLevel(apiLevel)
            NotificationUtils.createNotificationChannels(app)
            verify(notificationManager, times(0)).createNotificationChannels(anyList())
        }
    }

    @Test
    fun createNotificationChannels_shouldCreateAllChannels_whenAndroidIsAtLeastOreo() {
        for (apiLevel in Build.VERSION_CODES.O until 100) {
            internalSetUp()
            setApiLevel(apiLevel)
            NotificationUtils.createNotificationChannels(app)
            verify(notificationManager, times(1)).createNotificationChannels(anyList())
            val notificationChannels = notificationManager.notificationChannels
            assertEquals(2, notificationChannels.size)
            assertChannel(
                channel = notificationChannels.find { it.id == MESSAGES_CHANNEL_ID }!!,
                name = app.getString(R.string.navigationDrawer_messages),
                showBadge = true,
                showLights = true,
                vibrate = true)

            assertChannel(
                channel = notificationChannels.find { it.id == VIDEO_UPLOAD_CHANNEL_ID }!!,
                name = app.getString(R.string.video_upload),
                showBadge = false,
                showLights = true,
                vibrate = true)
        }
    }

    @Test
    fun showMessagesNotification_shouldBeProperlyConfigured_andShown() {
        NotificationUtils.showMessagesNotification(app, largeIcon, group, title, message, pendingIntent)
        assertNotification(MESSAGE_NOTIFICATION_ID, MESSAGES_CHANNEL_ID, group, largeIcon, title, message, pendingIntent, NotificationCompat.BADGE_ICON_SMALL)
    }

    @Test
    fun showVideoUploadNotification_shouldBeProperlyConfigured_andShown() {
        NotificationUtils.showVideoUploadNotification(app, message, pendingIntent)
        assertNotification(VIDEO_UPLOAD_NOTIFICATION_ID, VIDEO_UPLOAD_CHANNEL_ID,
            group = app.getString(R.string.app_name),
            title = app.getString(R.string.app_name),
            message = message,
            intent = pendingIntent,
            badgeIconType = NotificationCompat.BADGE_ICON_NONE)

        NotificationUtils.showVideoUploadNotification(app, message, pendingIntent, silent = true)
        assertNotification(VIDEO_UPLOAD_NOTIFICATION_ID, VIDEO_UPLOAD_CHANNEL_ID,
            group = app.getString(R.string.app_name),
            title = app.getString(R.string.app_name),
            message = message,
            intent = pendingIntent,
            badgeIconType = NotificationCompat.BADGE_ICON_NONE,
            silent = true)
    }

    private fun internalSetUp() {
        app = spy(RuntimeEnvironment.application)
        notificationManager = spy(app.getSystemService(Context.NOTIFICATION_SERVICE)) as NotificationManager
        doReturn(notificationManager).`when`(app).getSystemService(Context.NOTIFICATION_SERVICE)
    }

    private fun createNotifications(notificationManager: NotificationManager) {
        notificationManager.notify(MESSAGE_NOTIFICATION_ID, mock(Notification::class.java))
        notificationManager.notify(VIDEO_UPLOAD_NOTIFICATION_ID, mock(Notification::class.java))
    }

    private fun assertNotificationsShown() {
        val notifications = notificationManager.activeNotifications
        assertEquals(2, notifications.size)
        assertNotNull(notifications.find { it.id == MESSAGE_NOTIFICATION_ID })
        assertNotNull(notifications.find { it.id == VIDEO_UPLOAD_NOTIFICATION_ID })
    }

    private fun assertChannel(channel: NotificationChannel, name: String, showBadge: Boolean, showLights: Boolean, vibrate: Boolean) {
        assertEquals(name, channel.name)
        assertEquals(showBadge, channel.canShowBadge())
        assertEquals(showLights, channel.shouldShowLights())
        assertEquals(vibrate, channel.shouldVibrate())
        assertEquals(NotificationManager.IMPORTANCE_DEFAULT, channel.importance)
    }

    private fun assertNotification(notificationId: Int, channelId: String, group: String, largeIcon: Bitmap? = null, title: String,
                                   message: String, intent: PendingIntent, badgeIconType: Int, silent: Boolean = false) {

        val notifications = notificationManager.activeNotifications
        assertEquals(1, notifications.size)
        val notification = notifications.find { it.id == notificationId }!!.notification!!
        assertEquals(channelId, notification.channelId)
        assertEquals(if (silent) 0 else notification.defaults and (DEFAULT_SOUND or DEFAULT_VIBRATE or DEFAULT_LIGHTS), notification.defaults)

        val extras = notification.extras
        assertEquals(group, notification.group)
        assertEquals(Icon.createWithResource(app, R.drawable.ic_notification).toString(), notification.smallIcon.toString())
        assertEquals(largeIcon, (notification.getLargeIcon()?.loadDrawable(app) as? BitmapDrawable)?.bitmap)
        assertEquals(title, extras[Notification.EXTRA_TITLE])
        assertEquals(message, extras[Notification.EXTRA_BIG_TEXT])
        assertEquals(Notification.FLAG_AUTO_CANCEL, notification.flags and Notification.FLAG_AUTO_CANCEL)
        assertEquals(message, extras[Notification.EXTRA_TEXT])
        assertEquals(ContextCompat.getColor(app, R.color.material_deep_sea_blue_dark), notification.color)
        assertEquals(intent, notification.contentIntent)
        assertEquals(Notification.FLAG_ONLY_ALERT_ONCE, notification.flags and Notification.FLAG_ONLY_ALERT_ONCE)
        assertEquals(badgeIconType, notification.badgeIconType)

    }
}