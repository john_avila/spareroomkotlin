package com.spareroom.ui.filters.provider

import com.spareroom.TestApplication
import com.spareroom.TestConstants
import com.spareroom.lib.util.NumberUtil.MIN_AGE
import com.spareroom.model.business.SearchAdvertListProperties
import com.spareroom.model.business.SearchAdvertListProperties.*
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams
import com.spareroom.ui.filters.provider.ItemProviderTestUtil.*
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [(TestConstants.CONFIG_SDK)], application = TestApplication::class)
class AbstractItemProviderTest {
    private lateinit var genericItemProvider: AbstractItemProvider

    @Before
    fun setUp() {
        genericItemProvider = object : AbstractItemProvider() {
            override fun titleId(): Int {
                return 0
            }

            override fun groupList(): Map<String, List<GroupedCompoundButtonParams>>? {
                return null
            }

            override fun updateProperties(propertiesToUpdate: SearchAdvertListProperties, sortProperties: Boolean) {

            }
        }
    }

    @Test
    fun sortAgeRange_whenAgeNull_thenNoChanges() {
        val properties = createNewSearchAdvertListProperties()

        properties.maxAge = null
        properties.minAge = null
        genericItemProvider.sortAgeRange(properties)

        assertNull(properties.maxAge)
        assertNull(properties.minAge)
    }

    @Test
    fun sortAgeRange_whenAgeEqualOrLessThanZero_thenNull() {
        val properties = createNewSearchAdvertListProperties()

        properties.maxAge = 0
        properties.minAge = -15
        genericItemProvider.sortAgeRange(properties)

        assertNull(properties.maxAge)
        assertNull(properties.minAge)
    }

    @Test
    fun sortAgeRange_whenAgeLessThanLegalAge_thenForcedLegalAge() {
        val properties = createNewSearchAdvertListProperties()

        properties.maxAge = 2
        properties.minAge = 15
        genericItemProvider.sortAgeRange(properties)

        assertEquals(MIN_AGE, properties.maxAge)
        assertEquals(MIN_AGE, properties.minAge)
    }

    @Test
    fun sortAgeRange_whenMaxGreaterThanMin_thenNoChanges() {

        val properties = createNewSearchAdvertListProperties()
        properties.minAge = DEFAULT_MIN_AGE_REQ_INT_VALUE
        properties.maxAge = DEFAULT_MAX_AGE_REQ_INT_VALUE
        genericItemProvider.sortAgeRange(properties)
        assertEquals(DEFAULT_MIN_AGE_REQ_INT_VALUE, properties.minAge)
        assertEquals(DEFAULT_MAX_AGE_REQ_INT_VALUE, properties.maxAge)

        val minNullProperties = createNewSearchAdvertListProperties()
        minNullProperties.minAge = null
        minNullProperties.maxAge = DEFAULT_MAX_AGE_REQ_INT_VALUE
        genericItemProvider.sortAgeRange(minNullProperties)
        assertNull(minNullProperties.minAge)
        assertEquals(DEFAULT_MAX_AGE_REQ_INT_VALUE, minNullProperties.maxAge)

        val maxNullProperties = createNewSearchAdvertListProperties()
        maxNullProperties.minAge = DEFAULT_MIN_AGE_REQ_INT_VALUE
        maxNullProperties.maxAge = null
        genericItemProvider.sortAgeRange(maxNullProperties)
        assertEquals(DEFAULT_MIN_AGE_REQ_INT_VALUE, maxNullProperties.minAge)
        assertNull(maxNullProperties.maxAge)
    }

    @Test
    fun sortAgeRange_whenMinGreaterThanMax_thenMinMaxSwapped() {
        val properties = createNewSearchAdvertListProperties()

        properties.minAge = DEFAULT_MAX_AGE_REQ_INT_VALUE
        properties.maxAge = DEFAULT_MIN_AGE_REQ_INT_VALUE
        genericItemProvider.sortAgeRange(properties)

        assertEquals(DEFAULT_MIN_AGE_REQ_INT_VALUE, properties.minAge)
        assertEquals(DEFAULT_MAX_AGE_REQ_INT_VALUE, properties.maxAge)
    }

    @Test
    fun sortPriceRange_whenNull_thenNoChanges() {
        val properties = createNewSearchAdvertListProperties()

        properties.minMonthlyRent(null)
        properties.maxMonthlyRent(null)
        genericItemProvider.sortPriceRange(properties)

        assertNull(properties.minRent)
        assertNull(properties.maxRent)
    }

    @Test
    fun sortPriceRange_whenZero_thenSetToNull() {
        val properties = createNewSearchAdvertListProperties()

        properties.minMonthlyRent(0)
        properties.maxMonthlyRent(0)
        genericItemProvider.sortPriceRange(properties)

        assertNull(properties.minRent)
        assertNull(properties.maxRent)
    }

    @Test
    fun sortPriceRange_whenMaxGreaterThanMin_thenNoChanges() {

        val properties = createNewSearchAdvertListProperties()
        properties.minMonthlyRent(DEFAULT_MIN_RENT_INT_VALUE)
        properties.maxMonthlyRent(DEFAULT_MAX_RENT_INT_VALUE)
        genericItemProvider.sortPriceRange(properties)
        assertEquals(DEFAULT_MIN_RENT_INT_VALUE, properties.minRent)
        assertEquals(DEFAULT_MAX_RENT_INT_VALUE, properties.maxRent)

        val minNullProperties = createNewSearchAdvertListProperties()
        minNullProperties.minMonthlyRent(null)
        minNullProperties.maxMonthlyRent(DEFAULT_MAX_RENT_INT_VALUE)
        genericItemProvider.sortPriceRange(minNullProperties)
        assertNull(minNullProperties.minRent)
        assertEquals(DEFAULT_MAX_RENT_INT_VALUE, minNullProperties.maxRent)

        val maxNullProperties = createNewSearchAdvertListProperties()
        maxNullProperties.minMonthlyRent(DEFAULT_MIN_RENT_INT_VALUE)
        maxNullProperties.maxMonthlyRent(null)
        genericItemProvider.sortPriceRange(maxNullProperties)
        assertEquals(DEFAULT_MIN_RENT_INT_VALUE, maxNullProperties.minRent)
        assertNull(maxNullProperties.maxRent)
    }

    @Test
    fun sortPriceRange_whenMinGreaterThanMax_thenMinMaxSwapped() {
        val properties = createNewSearchAdvertListProperties()

        properties.minMonthlyRent(DEFAULT_MAX_RENT_INT_VALUE)
        properties.maxMonthlyRent(DEFAULT_MIN_RENT_INT_VALUE)
        genericItemProvider.sortPriceRange(properties)

        assertEquals(DEFAULT_MIN_RENT_INT_VALUE, properties.minRent)
        assertEquals(DEFAULT_MAX_RENT_INT_VALUE, properties.maxRent)
    }

    @Test
    fun sortLengthOfStay_whenMaxGreaterThanMin_thenNoChanges() {
        val properties = createNewSearchAdvertListProperties()

        properties.minTerm = MinTerm.ONE
        properties.maxTerm = MaxTerm.EIGHTEEN
        genericItemProvider.sortLengthOfStay(properties)

        assertEquals(MinTerm.ONE, properties.minTerm)
        assertEquals(MaxTerm.EIGHTEEN, properties.maxTerm)
    }

    @Test
    fun sortLengthOfStay_whenMinGreaterThanMax_thenMinMaxSwapped() {
        val properties = createNewSearchAdvertListProperties()

        properties.minTerm = MinTerm.EIGHTEEN
        properties.maxTerm = MaxTerm.ONE
        genericItemProvider.sortLengthOfStay(properties)

        assertEquals(MinTerm.ONE, properties.minTerm)
        assertEquals(MaxTerm.EIGHTEEN, properties.maxTerm)
    }

    private fun createNewSearchAdvertListProperties() = object : SearchAdvertListProperties {
        override var where: String? = null
        override var page: String? = null
        override var maxPerPage: String? = null
        override var keyword: String? = null
        override var featured: Boolean = false
        override var sortedBy: SortedBy = SortedBy.DEFAULT
        override var gayShare: Boolean = false
        override var photoAdvertsOnly: Boolean = false
        override var includeHidden: Boolean = false
        override var availableFrom: String? = null
        override var minRent: Int? = null
        override var maxRent: Int? = null
        override var minAge: Int? = null
        override var maxAge: Int? = null
        override var minTerm: MinTerm = MinTerm.NOT_SET
        override var maxTerm: MaxTerm = MaxTerm.NOT_SET
        override var daysOfWeekAvailable: DaysOfWeek = DaysOfWeek.NOT_SET
        override var smoking: Smoking = Smoking.NOT_SET
        override var roomType: RoomTypes = RoomTypes.NOT_SET
        override var shareType: ShareType = ShareType.NOT_SET
        override var genderFilter: GenderFilter = GenderFilter.NOT_SET
    }

}
