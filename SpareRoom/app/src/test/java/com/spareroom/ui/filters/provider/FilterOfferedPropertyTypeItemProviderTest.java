package com.spareroom.ui.filters.provider;

import com.spareroom.TestApplication;
import com.spareroom.TestConstants;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.NoOfRooms;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static com.spareroom.ui.filters.provider.FilterOfferedPropertyTypeItemProvider.*;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class FilterOfferedPropertyTypeItemProviderTest {

    private FilterOfferedPropertyTypeItemProvider provider;
    private final ItemProviderTestUtil itemProviderTestUtil = new ItemProviderTestUtil();

    @Before
    public void setUp() {
        provider = new FilterOfferedPropertyTypeItemProvider();
    }

    @Test
    public void get_whenRetrievingList_thenAllItemsAdded() {
        provider.createNewList(new SearchAdvertListPropertiesOffered(), null);
        assertEquals(15, provider.get().size());
    }

    @Test
    public void createNewList_whenEmptyPropertyList_thenDefaultFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        final List<FilterItem> list = provider.get();

        assertFalse(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, ROOMS)).selected());
        assertFalse(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, STUDIOS)).selected());
        assertFalse(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, WHOLE_PROPERTY)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, ONE_BEDROOM)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, TWO_BEDROOMS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, THREE_BEDROOMS)).selected());
        assertFalse(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, PARKING)).selected());
        assertFalse(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, SHARED_LIVING_ROOM)).selected());
        assertFalse(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, DISABLED_ACCESS)).selected());
    }

    @Test
    public void createNewList_whenFullPropertyList_thenFilledFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        final List<FilterItem> list = provider.get();

        assertTrue(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, ROOMS)).selected());
        assertTrue(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, STUDIOS)).selected());
        assertTrue(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, WHOLE_PROPERTY)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, ONE_BEDROOM)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, TWO_BEDROOMS)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, THREE_BEDROOMS)).selected());
        assertTrue(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, PARKING)).selected());
        assertTrue(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, SHARED_LIVING_ROOM)).selected());
        assertTrue(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, DISABLED_ACCESS)).selected());
    }

    @Test
    public void saveProperties_whenEmptyList_andEmptyProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        testEmptyList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenEmptyList_andFullProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        testEmptyList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenFullList_andEmptyProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        testFullList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenFullList_andFullProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        testFullList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered());
    }

    private void testFullList(final SearchAdvertListPropertiesOffered properties) {
        provider.updateProperties(properties, true);

        assertTrue(properties.getShowRooms());
        assertTrue(properties.getShowStudios());
        assertTrue(properties.getShowWholeProperty());
        assertTrue(properties.getParking());
        assertTrue(properties.getLivingRoom());
        assertTrue(properties.getDisabledAccess());
        assertEquals(NoOfRooms.MIN_THREE, properties.getNoOfRooms());
    }

    private void testEmptyList(final SearchAdvertListPropertiesOffered properties) {
        provider.updateProperties(properties, true);

        assertFalse(properties.getShowRooms());
        assertFalse(properties.getShowStudios());
        assertFalse(properties.getShowWholeProperty());
        assertFalse(properties.getParking());
        assertFalse(properties.getLivingRoom());
        assertFalse(properties.getDisabledAccess());
        assertEquals(NoOfRooms.NOT_SET, properties.getNoOfRooms());
    }

}
