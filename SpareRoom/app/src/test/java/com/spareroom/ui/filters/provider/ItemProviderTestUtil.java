package com.spareroom.ui.filters.provider;

import com.spareroom.model.business.*;
import com.spareroom.model.business.SearchAdvertListProperties.*;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.*;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted.Couples;
import com.spareroom.ui.filters.adapter.viewparams.IResettableParams;

import java.util.List;

class ItemProviderTestUtil {

    private final static String DEFAULT_PAGE_VALUE = "page";
    private final static String DEFAULT_MAX_PER_PAGE_VALUE = "maxPerPage";
    private final static String DEFAULT_USER_ID_VALUE = "123456";
    private final static double DEFAULT_LATITUDE_VALUE = 123;
    private final static double DEFAULT_LATITUDE_DELTA_VALUE = 456;
    private final static double DEFAULT_LONGITUDE_VALUE = 789;
    private final static double DEFAULT_LONGITUDE_DELTA_VALUE = 987;
    private final static SortedBy DEFAULT_SORTED_BY_VALUE = SortedBy.LAST_UPDATED;
    private final static LocationType DEFAULT_LOCATION_TYPE_VALUE = LocationType.COMMUTER;
    private final static NoOfRooms DEFAULT_NO_OF_ROOMS_VALUE = NoOfRooms.MIN_THREE;

    // Offered
    final static String DEFAULT_WHERE_VALUE = "Manchester";
    final static Integer DEFAULT_MILES_FROM_MAX_INT_VALUE = 15;
    final static RoomsFor DEFAULT_ROOMS_FOR_VALUE = RoomsFor.COUPLES;
    final static Landlord DEFAULT_LANDLORD_VALUE = Landlord.LIVE_OUT;
    final static MinFlatmates DEFAULT_MIN_FLATMATES_VALUE = MinFlatmates.FIVE;
    final static MaxFlatmates DEFAULT_MAX_FLATMATES_VALUE = MaxFlatmates.SIX;
    final static PostedBy DEFAULT_POSTED_BY_VALUE = PostedBy.AGENTS;

    // Common
    final static Couples DEFAULT_COUPLES_VALUE = Couples.YES;
    final static String DEFAULT_AVAILABLE_FROM_VALUE = "2017-02-05";
    final static Integer DEFAULT_MIN_AGE_REQ_INT_VALUE = 18;
    final static Integer DEFAULT_MAX_AGE_REQ_INT_VALUE = 99;
    final static Integer DEFAULT_MIN_RENT_INT_VALUE = 10;
    final static Integer DEFAULT_MAX_RENT_INT_VALUE = 100;
    final static MinTerm DEFAULT_MIN_TERM_VALUE = MinTerm.EIGHT;
    final static MaxTerm DEFAULT_MAX_TERM_VALUE = MaxTerm.ELEVEN;
    final static GenderFilter DEFAULT_GENDER_FILTER_VALUE = GenderFilter.FEMALES;
    final static DaysOfWeek DEFAULT_DAYS_OF_WEEK_AVAILABLE_VALUE = DaysOfWeek.WEEKDAYS;
    final static RoomTypes DEFAULT_ROOM_TYPES_VALUE = RoomTypes.DOUBLE;
    final static ShareType DEFAULT_SHARE_TYPE_VALUE = ShareType.PROFESSIONALS;
    final static Smoking DEFAULT_SMOKING_VALUE = Smoking.NO;

    IResettableParams getParameter(List<FilterItem> filterItemList, final String itemName) {
        for (int i = 0; i < filterItemList.size(); i++) {
            if (filterItemList.get(i).tag().equals(itemName))
                return filterItemList.get(i).params();
        }
        return null;
    }

    SearchAdvertListPropertiesOffered createFullSearchAdvertListPropertiesOffered() {
        SearchAdvertListPropertiesOffered propertiesList = new SearchAdvertListPropertiesOffered();

        setCommonProperties(propertiesList);

        propertiesList.setShowRooms(true);
        propertiesList.setShowStudios(true);
        propertiesList.setShowWholeProperty(true);
        propertiesList.setParking(true);
        propertiesList.setDisabledAccess(true);
        propertiesList.setEnSuite(true);
        propertiesList.setBenefits(true);
        propertiesList.setPets(true);
        propertiesList.setLivingRoom(true);
        propertiesList.billsInc(true);
        propertiesList.setShortLets(true);
        propertiesList.setVegetarians(true);
        propertiesList.setMilesFromMax(DEFAULT_MILES_FROM_MAX_INT_VALUE);
        propertiesList.setDaysOfWeekAvailable(DEFAULT_DAYS_OF_WEEK_AVAILABLE_VALUE);
        propertiesList.setLocationType(DEFAULT_LOCATION_TYPE_VALUE);
        propertiesList.setNoOfRooms(DEFAULT_NO_OF_ROOMS_VALUE);
        propertiesList.setMinFlatmates(DEFAULT_MIN_FLATMATES_VALUE);
        propertiesList.maxFlatmates(DEFAULT_MAX_FLATMATES_VALUE);
        propertiesList.setLandlord(DEFAULT_LANDLORD_VALUE);
        propertiesList.setRoomsFor(DEFAULT_ROOMS_FOR_VALUE);
        propertiesList.setPostedBy(DEFAULT_POSTED_BY_VALUE);

        Coordinates coordinates = propertiesList.getCoordinates();
        coordinates.setLatitude(DEFAULT_LATITUDE_VALUE);
        coordinates.setLatitudeDelta(DEFAULT_LATITUDE_DELTA_VALUE);
        coordinates.setLongitude(DEFAULT_LONGITUDE_VALUE);
        coordinates.setLongitudeDelta(DEFAULT_LONGITUDE_DELTA_VALUE);

        return propertiesList;
    }

    SearchAdvertListPropertiesOffered createEmptySearchAdvertListPropertiesOffered() {
        return new SearchAdvertListPropertiesOffered();
    }

    SearchAdvertListPropertiesWanted createFullSearchAdvertListPropertiesWanted() {
        SearchAdvertListPropertiesWanted propertiesList = new SearchAdvertListPropertiesWanted();

        setCommonProperties(propertiesList);

        propertiesList.setDaysOfWeekAvailable(DEFAULT_DAYS_OF_WEEK_AVAILABLE_VALUE);
        propertiesList.setCouples(DEFAULT_COUPLES_VALUE);
        propertiesList.setUserId(DEFAULT_USER_ID_VALUE);

        return propertiesList;
    }

    SearchAdvertListPropertiesWanted createEmptySearchAdvertListPropertiesWanted() {
        return new SearchAdvertListPropertiesWanted();
    }

    private void setCommonProperties(SearchAdvertListProperties properties) {
        properties.setWhere(DEFAULT_WHERE_VALUE);
        properties.setPage(DEFAULT_PAGE_VALUE);
        properties.setMaxPerPage(DEFAULT_MAX_PER_PAGE_VALUE);
        properties.setFeatured(true);
        properties.setSortedBy(DEFAULT_SORTED_BY_VALUE);
        properties.setGayShare(true);
        properties.setPhotoAdvertsOnly(true);
        properties.setIncludeHidden(true);
        properties.minMonthlyRent(DEFAULT_MIN_RENT_INT_VALUE);
        properties.maxMonthlyRent(DEFAULT_MAX_RENT_INT_VALUE);
        properties.setMinAge(DEFAULT_MIN_AGE_REQ_INT_VALUE);
        properties.setMaxAge(DEFAULT_MAX_AGE_REQ_INT_VALUE);
        properties.setMinTerm(DEFAULT_MIN_TERM_VALUE);
        properties.setMaxTerm(DEFAULT_MAX_TERM_VALUE);
        properties.setRoomType(DEFAULT_ROOM_TYPES_VALUE);
        properties.setShareType(DEFAULT_SHARE_TYPE_VALUE);
        properties.setAvailableFrom(DEFAULT_AVAILABLE_FROM_VALUE);
        properties.setGenderFilter(DEFAULT_GENDER_FILTER_VALUE);
        properties.setSmoking(DEFAULT_SMOKING_VALUE);
    }

}
