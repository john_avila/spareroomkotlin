package com.spareroom.ui.filters.provider;

import com.spareroom.TestApplication;
import com.spareroom.TestConstants;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted;
import com.spareroom.ui.widget.params.MinMaxEditTextParams;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static com.spareroom.ui.filters.provider.FilterWantedAgeItemProvider.AGE_RANGE;
import static com.spareroom.ui.filters.provider.ItemProviderTestUtil.DEFAULT_MAX_AGE_REQ_INT_VALUE;
import static com.spareroom.ui.filters.provider.ItemProviderTestUtil.DEFAULT_MIN_AGE_REQ_INT_VALUE;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class FilterWantedAgeItemProviderTest {

    private FilterWantedAgeItemProvider provider;
    private final ItemProviderTestUtil itemProviderTestUtil = new ItemProviderTestUtil();

    @Before
    public void setUp() {
        provider = new FilterWantedAgeItemProvider();
    }

    @Test
    public void get_whenRetrievingList_thenAllItemsAdded() {
        provider.createNewList(new SearchAdvertListPropertiesWanted(), null);
        assertEquals(2, provider.get().size());
    }

    @Test
    public void createNewList_whenEmptyPropertyList_thenDefaultFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted(), null);
        final List<FilterItem> list = provider.get();

        assertNull(((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, AGE_RANGE)).min());
        assertNull(((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, AGE_RANGE)).min());
    }

    @Test
    public void createNewList_whenFullPropertyList_thenFilledFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted(), null);
        final List<FilterItem> list = provider.get();

        assertEquals(DEFAULT_MIN_AGE_REQ_INT_VALUE, ((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, AGE_RANGE)).min());
        assertEquals(DEFAULT_MAX_AGE_REQ_INT_VALUE, ((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, AGE_RANGE)).max());
    }

    @Test
    public void saveProperties_whenEmptyList_andEmptyProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted(), null);
        testEmptyList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenEmptyList_andFullProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted(), null);
        testEmptyList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenFullList_andEmptyProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted(), null);
        testFullList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenFullList_andFullProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted(), null);
        testFullList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenSortFlagIsTrue_thenShouldSortAge() {
        SearchAdvertListPropertiesWanted ageProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted();
        setRevertedAge(ageProperties);
        provider.updateProperties(ageProperties, true);

        assertNotNull(ageProperties.getMinAge());
        assertEquals(19, ageProperties.getMinAge(), 0);
        assertNotNull(ageProperties.getMaxAge());
        assertEquals(20, ageProperties.getMaxAge(), 0);
    }

    @Test
    public void saveProperties_whenSortFlagIsFalse_thenShouldNotSortAge() {
        SearchAdvertListPropertiesWanted ageProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted();
        setRevertedAge(ageProperties);
        provider.updateProperties(ageProperties, false);

        assertNotNull(ageProperties.getMinAge());
        assertEquals(20, ageProperties.getMinAge(), 0);
        assertNotNull(ageProperties.getMaxAge());
        assertEquals(19, ageProperties.getMaxAge(), 0);
    }

    private void testFullList(final SearchAdvertListPropertiesWanted properties) {
        provider.updateProperties(properties, true);

        assertEquals(DEFAULT_MIN_AGE_REQ_INT_VALUE, properties.getMinAge());
        assertEquals(DEFAULT_MAX_AGE_REQ_INT_VALUE, properties.getMaxAge());
    }

    private void testEmptyList(final SearchAdvertListPropertiesWanted properties) {
        provider.updateProperties(properties, true);

        assertNull(properties.getMinAge());
        assertNull(properties.getMaxAge());
    }

    private void setRevertedAge(SearchAdvertListPropertiesWanted properties) {
        properties.setMinAge(20);
        properties.setMaxAge(19);
        provider.createNewList(properties, null);
    }

}
