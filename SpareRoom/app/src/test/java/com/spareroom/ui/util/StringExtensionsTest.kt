package com.spareroom.ui.util

import org.junit.Assert.assertEquals
import org.junit.Test

class StringExtensionsTest {

    @Test
    fun removeExtraWhiteSpaces_shouldReturnEmptyString_whenOnlyWhiteSpaces() {
        assertEquals("", "".removeExtraWhiteSpaces())
        assertEquals("", " ".removeExtraWhiteSpaces())
        assertEquals("", "\n".removeExtraWhiteSpaces())
        assertEquals("", " \n ".removeExtraWhiteSpaces())
        assertEquals("", "\t".removeExtraWhiteSpaces())
        assertEquals("", " \t ".removeExtraWhiteSpaces())
        assertEquals("", " \t\n ".removeExtraWhiteSpaces())
    }

    @Test
    fun removeExtraWhiteSpaces_shouldReplaceWhiteSpacesWithSingleSpace() {
        assertEquals("æfghê", "æfghê ".removeExtraWhiteSpaces())
        assertEquals("æfghê æfghê", "æfghê  æfghê ".removeExtraWhiteSpaces())
        assertEquals("æfghê æfghê", "æfghê\næfghê ".removeExtraWhiteSpaces())
        assertEquals("æfghê æfghê", "æfghê    æfghê ".removeExtraWhiteSpaces())
        assertEquals("æfghê æfghê", " æfghê\n\næfghê ".removeExtraWhiteSpaces())
        assertEquals("æfghê æfghê", "æfghê\n   \næfghê ".removeExtraWhiteSpaces())
        assertEquals("æfghê æfghê", "æfghê  \n   \n   æfghê ".removeExtraWhiteSpaces())
        assertEquals("æfghê æfghê", "æfghê  \n  \n  æfghê\n\n ".removeExtraWhiteSpaces())
        assertEquals("æfghê æfghê", "æfghê  \n\n  \n\n  æfghê\n\n ".removeExtraWhiteSpaces())
        assertEquals("æfghê æfghê æfghê", "æfghê  \n  æfghê  \n   æfghê ".removeExtraWhiteSpaces())
        assertEquals("æfghê æfghê", "\tæfghê  \t\n\n  \n\t\n  æfghê\n\t\n ".removeExtraWhiteSpaces())
    }

}
