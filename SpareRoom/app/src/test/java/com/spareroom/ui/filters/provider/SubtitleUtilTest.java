package com.spareroom.ui.filters.provider;

import android.app.Application;

import com.spareroom.*;
import com.spareroom.model.business.SearchAdvertListProperties.*;
import com.spareroom.ui.util.DateUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class SubtitleUtilTest {
    private final Application application = RuntimeEnvironment.application;
    private SubtitleUtil subtitleUtil;

    private final String EN_DASH_STRING_RESOURCE = application.getString(R.string.en_dash);

    @Before
    public void setUp() {
        subtitleUtil = new SubtitleUtil(new DateUtils(Locale.getDefault(), application));
    }

    @Test
    public void getRentAmountText_whenAtLeastOneValue_thenProperlyFormatted() {
        String currency = application.getString(R.string.currency_symbol);

        // "Between £140-£230"
        int min = 140;
        int max = 230;
        assertEquals(
                application.getString(R.string.between) + " " + currency + min + EN_DASH_STRING_RESOURCE + currency + max,
                subtitleUtil.getRentAmountText(application, min, max));

        // "From £390"
        min = 390;
        assertEquals(
                application.getString(R.string.from) + " " + currency + min,
                subtitleUtil.getRentAmountText(application, min, null));

        // "Up to £150"
        max = 150;
        assertEquals(
                application.getString(R.string.upTo) + " " + currency + max,
                subtitleUtil.getRentAmountText(application, null, max));
    }

    @Test
    public void getRentAmountText_whenNull_thenEmpty() {
        assertEquals("", subtitleUtil.getRentAmountText(application, null, null));
    }

    @Test
    public void getMoveInText_whenExpectedDateFormat_thenProperlyFormattedForApiRequest() {
        // "Move in on 9 February 2018"
        assertEquals("6 June 2017", subtitleUtil.getMoveInText("2017-06-06"));
        assertEquals("16 June 2016", subtitleUtil.getMoveInText("2016-06-16"));
        assertEquals("6 December 2017", subtitleUtil.getMoveInText("2017-12-06"));
        assertEquals("18 November 2021", subtitleUtil.getMoveInText("2021-11-18"));
    }

    @Test
    public void getMoveInText_whenNull_thenEmpty() {
        assertEquals("", subtitleUtil.getMoveInText(null));
    }

    @Test
    public void getLengthOfStayText_whenAtLeastOneValueNotNull_thenProperlyFormatted() {
        int minMonths = 3;
        int maxMonths = 9;
        int minYears = 15;
        int maxYears = 25;

        // "Between 3 – 9 months"
        assertEquals(
                application.getString(R.string.between) + " " + "3" + EN_DASH_STRING_RESOURCE + "9 months",
                subtitleUtil.getLengthOfStayText(application, minMonths, maxMonths));
        // "From 3 months"
        assertEquals("From 3 months", subtitleUtil.getLengthOfStayText(application, minMonths, null));
        // "Up to 9 months"
        assertEquals("Up to 9 months", subtitleUtil.getLengthOfStayText(application, null, maxMonths));
        // "1 year 3 months – 2 years 1 month"
        assertEquals(
                application.getString(R.string.between) + " " + "1 year 3 months" + EN_DASH_STRING_RESOURCE + "2 years 1 month",
                subtitleUtil.getLengthOfStayText(application, minYears, maxYears));
        // "From 1 year 3 months"
        assertEquals("From 1 year 3 months", subtitleUtil.getLengthOfStayText(application, minYears, null));
        // "Up to 2 years 1 month"
        assertEquals("Up to 2 years 1 month", subtitleUtil.getLengthOfStayText(application, null, maxYears));
        // "3 months – 2 years 1 month"
        assertEquals(
                application.getString(R.string.between) + " " + "3 months" + EN_DASH_STRING_RESOURCE + "2 years 1 month",
                subtitleUtil.getLengthOfStayText(application, minMonths, maxYears));
    }

    @Test
    public void getLengthOfStayText_whenNull_thenEmpty() {
        assertEquals("", subtitleUtil.getLengthOfStayText(application, null, null));
    }

    @Test
    public void getRoomTypeText_whenOptionSet_thenProperlyFormatted() {
        assertEquals(application.getString(R.string.singleBedroom), subtitleUtil.getRoomTypeText(application, RoomTypes.SINGLE));
        assertEquals(application.getString(R.string.doubleBedroom), subtitleUtil.getRoomTypeText(application, RoomTypes.DOUBLE));
    }

    @Test
    public void getRoomTypeText_whenNull_thenEmpty() {
        assertEquals("", subtitleUtil.getRoomTypeText(application, RoomTypes.NOT_SET));
    }

    @Test
    public void getGenderFilterText_whenAnOptionSelected_thenProperlyFormatted() {
        assertEquals(application.getString(R.string.females), subtitleUtil.getGenderFilterText(application, GenderFilter.FEMALES));
        assertEquals(application.getString(R.string.males), subtitleUtil.getGenderFilterText(application, GenderFilter.MALES));
        assertEquals(application.getString(R.string.mixed), subtitleUtil.getGenderFilterText(application, GenderFilter.MIXED));
    }

    @Test
    public void getGenderFilterText__whenNull_thenEmpty() {
        assertEquals("", subtitleUtil.getGenderFilterText(application, null));
    }

    @Test
    public void getAgeText_whenAtLeastAnAgeSet_thenProperlyFormatted() {
        String min = "18";
        String max = "24";
        String years = application.getString(R.string.years);

        // "Between 18 - 24 years"
        assertEquals(
                application.getString(R.string.between) + " " + min + EN_DASH_STRING_RESOURCE + max + " " + years,
                subtitleUtil.getAgeText(application, 18, 24));

        // "From 18 years"
        assertEquals(application.getString(R.string.from) + " " + min + " " + years, subtitleUtil.getAgeText(application, Integer.parseInt(min), null));

        // "Up to 24 years"
        assertEquals(application.getString(R.string.upTo) + " " + max + " " + years, subtitleUtil.getAgeText(application, null, Integer.parseInt(max)));
    }

    @Test
    public void getAgeText_whenNull_thenEmpty() {
        assertEquals("", subtitleUtil.getAgeText(application, null, null));
    }

    @Test
    public void getEmploymentStatusText_whenAtLeastAnOptionSelected_thenProperlyFormatted() {
        assertEquals(application.getString(R.string.professionals), subtitleUtil.getEmploymentStatusText(application, ShareType.PROFESSIONALS));
        assertEquals(application.getString(R.string.students), subtitleUtil.getEmploymentStatusText(application, ShareType.STUDENTS));
    }

    @Test
    public void getEmploymentStatusText_whenNull_thenEmpty() {
        assertEquals("", subtitleUtil.getEmploymentStatusText(application, null));
    }

    @Test
    public void getTermText_whenBiggerThanOneMonth_thenProperlyFormatted() {
        String month = application.getString(R.string.month);
        String months = application.getString(R.string.months);
        String year = application.getString(R.string.year);
        String years = application.getString(R.string.years);

        assertEquals(1 + " " + month, subtitleUtil.getTermText(application, 1));
        assertEquals(1 + " " + year, subtitleUtil.getTermText(application, 12));
        assertEquals(1 + " " + year + " " + 1 + " " + month, subtitleUtil.getTermText(application, 13));
        assertEquals(1 + " " + year + " " + 3 + " " + months, subtitleUtil.getTermText(application, 15));
        assertEquals(2 + " " + years, subtitleUtil.getTermText(application, 24));
    }

    @Test
    public void getFlatmatesText_whenNullOrValueSet_thenProperlyFormatted() {
        String flatmates = application.getString(R.string.flatmates).toLowerCase();

        assertEquals("", subtitleUtil.getFlatmatesText(application, null));
        assertEquals("", subtitleUtil.getFlatmatesText(application, ""));
        assertEquals("1 " + flatmates, subtitleUtil.getFlatmatesText(application, "1"));
        assertEquals("6+ " + flatmates, subtitleUtil.getFlatmatesText(application, "6+"));

    }

}
