package com.spareroom.ui.filters.provider;

import com.spareroom.TestApplication;
import com.spareroom.TestConstants;
import com.spareroom.model.business.SearchAdvertListProperties.GenderFilter;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static com.spareroom.ui.filters.provider.FilterWantedGenderTypeItemProvider.*;
import static com.spareroom.ui.filters.provider.ItemProviderTestUtil.DEFAULT_GENDER_FILTER_VALUE;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class FilterWantedGenderTypeItemProviderTest {

    private FilterWantedGenderTypeItemProvider provider;
    private final ItemProviderTestUtil itemProviderTestUtil = new ItemProviderTestUtil();

    @Before
    public void setUp() {
        provider = new FilterWantedGenderTypeItemProvider();
    }

    @Test
    public void get_whenRetrievingList_thenAllItemsAdded() {
        provider.createNewList(new SearchAdvertListPropertiesWanted(), null);
        assertEquals(8, provider.get().size());
    }

    @Test
    public void createNewList_whenEmptyPropertyList_thenDefaultFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted(), null);
        final List<FilterItem> list = provider.get();

        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, MALES)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, FEMALES)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, NO_PREFERENCE)).selected());
        assertFalse(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, LGBTQ)).selected());
    }

    @Test
    public void createNewList_whenFullPropertyList_thenFilledFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted(), null);
        final List<FilterItem> list = provider.get();

        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, MALES)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, FEMALES)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, NO_PREFERENCE)).selected());
        assertTrue(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, LGBTQ)).selected());
    }

    @Test
    public void saveProperties_whenEmptyList_andEmptyProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted(), null);
        testEmptyList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenEmptyList_andFullProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted(), null);
        testEmptyList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenFullList_andEmptyProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted(), null);
        testFullList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenFullList_andFullProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted(), null);
        testFullList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted());
    }

    private void testFullList(final SearchAdvertListPropertiesWanted properties) {
        provider.updateProperties(properties, true);

        assertEquals(DEFAULT_GENDER_FILTER_VALUE, properties.getGenderFilter());
        assertTrue(properties.getGayShare());
    }

    private void testEmptyList(final SearchAdvertListPropertiesWanted properties) {
        provider.updateProperties(properties, true);

        assertEquals(GenderFilter.NOT_SET, properties.getGenderFilter());
        assertFalse(properties.getGayShare());
    }

}
