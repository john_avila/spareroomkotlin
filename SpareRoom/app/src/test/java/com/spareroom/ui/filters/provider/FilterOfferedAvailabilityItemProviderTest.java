package com.spareroom.ui.filters.provider;

import com.spareroom.TestApplication;
import com.spareroom.TestConstants;
import com.spareroom.model.business.SearchAdvertListProperties.*;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.ui.filters.adapter.viewparams.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static com.spareroom.ui.filters.provider.AbstractItemProvider.LENGTH_OF_STAY_MAX;
import static com.spareroom.ui.filters.provider.AbstractItemProvider.LENGTH_OF_STAY_MIN;
import static com.spareroom.ui.filters.provider.FilterOfferedAvailabilityItemProvider.*;
import static com.spareroom.ui.filters.provider.ItemProviderTestUtil.*;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class FilterOfferedAvailabilityItemProviderTest {

    private FilterOfferedAvailabilityItemProvider provider;
    private final ItemProviderTestUtil itemProviderTestUtil = new ItemProviderTestUtil();

    @Before
    public void setUp() {
        provider = new FilterOfferedAvailabilityItemProvider();
    }

    @Test
    public void get_whenRetrievingList_thenAllItemsAdded() {
        provider.createNewList(new SearchAdvertListPropertiesOffered(), null);
        assertEquals(14, provider.get().size());
    }

    @Test
    public void createNewList_whenEmptyPropertyList_thenDefaultFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        final List<FilterItem> list = provider.get();

        assertNull(((SummaryViewDateParams) itemProviderTestUtil.getParameter(list, MOVE_IN)).date());
        assertEquals(MinTerm.NOT_SET, ((SummaryViewMinTermParams) itemProviderTestUtil.getParameter(list, LENGTH_OF_STAY_MIN)).minTerm());
        assertEquals(MaxTerm.NOT_SET, ((SummaryViewMaxTermParams) itemProviderTestUtil.getParameter(list, LENGTH_OF_STAY_MAX)).maxTerm());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, ALL_WEEK)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, WEEK_DAYS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, WEEK_ENDS)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, NO_PREFERENCE)).selected());
    }

    @Test
    public void createNewList_whenFullPropertyList_thenFilledFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        final List<FilterItem> list = provider.get();

        assertEquals(DEFAULT_AVAILABLE_FROM_VALUE, ((SummaryViewDateParams) itemProviderTestUtil.getParameter(list, MOVE_IN)).date());
        assertEquals(DEFAULT_MIN_TERM_VALUE, ((SummaryViewMinTermParams) itemProviderTestUtil.getParameter(list, LENGTH_OF_STAY_MIN)).minTerm());
        assertEquals(DEFAULT_MAX_TERM_VALUE, ((SummaryViewMaxTermParams) itemProviderTestUtil.getParameter(list, LENGTH_OF_STAY_MAX)).maxTerm());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, ALL_WEEK)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, WEEK_DAYS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, WEEK_ENDS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, NO_PREFERENCE)).selected());
    }

    @Test
    public void saveProperties_whenEmptyList_andEmptyProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        testEmptyList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenEmptyList_andFullProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        testEmptyList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenFullList_andEmptyProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        testFullList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenFullList_andFullProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        testFullList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenSortFlagIsFalse_thenShouldNotSortTerm() {
        SearchAdvertListPropertiesOffered termProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        setReversedMinMaxTerm(termProperties);
        provider.updateProperties(termProperties, false);

        assertEquals(MinTerm.EIGHTEEN, termProperties.getMinTerm());
        assertEquals(MaxTerm.ONE, termProperties.getMaxTerm());
    }

    @Test
    public void saveProperties_whenSortFlagIsTrue_thenShouldSortTerm() {
        SearchAdvertListPropertiesOffered termProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        setReversedMinMaxTerm(termProperties);
        provider.updateProperties(termProperties, true);

        assertEquals(MinTerm.ONE, termProperties.getMinTerm());
        assertEquals(MaxTerm.EIGHTEEN, termProperties.getMaxTerm());
    }

    private void testFullList(final SearchAdvertListPropertiesOffered properties) {
        provider.updateProperties(properties, true);

        assertEquals(DEFAULT_AVAILABLE_FROM_VALUE, properties.getAvailableFrom());
        assertEquals(DEFAULT_MIN_TERM_VALUE, properties.getMinTerm());
        assertEquals(DEFAULT_MAX_TERM_VALUE, properties.getMaxTerm());
        assertTrue(properties.getShortLets());
        assertEquals(DEFAULT_DAYS_OF_WEEK_AVAILABLE_VALUE, properties.getDaysOfWeekAvailable());
    }

    private void testEmptyList(final SearchAdvertListPropertiesOffered properties) {
        provider.updateProperties(properties, false);

        assertNull(properties.getAvailableFrom());
        assertEquals(MinTerm.NOT_SET, properties.getMinTerm());
        assertEquals(MaxTerm.NOT_SET, properties.getMaxTerm());
        assertFalse(properties.getShortLets());
        assertEquals(DaysOfWeek.NOT_SET, properties.getDaysOfWeekAvailable());
    }

    private void setReversedMinMaxTerm(SearchAdvertListPropertiesOffered properties) {
        properties.setMinTerm(MinTerm.EIGHTEEN);
        properties.setMaxTerm(MaxTerm.ONE);
        provider.createNewList(properties, null);
    }

}
