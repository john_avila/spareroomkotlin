package com.spareroom.ui.filters.provider;

import com.spareroom.TestApplication;
import com.spareroom.TestConstants;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.ui.widget.params.CompoundButtonParams;
import com.spareroom.ui.widget.params.MinMaxEditTextParams;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static com.spareroom.ui.filters.provider.FilterOfferedBudgetItemProvider.BILLS_INCLUDED;
import static com.spareroom.ui.filters.provider.FilterOfferedBudgetItemProvider.PRICE_RANGE;
import static com.spareroom.ui.filters.provider.ItemProviderTestUtil.DEFAULT_MAX_RENT_INT_VALUE;
import static com.spareroom.ui.filters.provider.ItemProviderTestUtil.DEFAULT_MIN_RENT_INT_VALUE;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class FilterOfferedBudgetItemProviderTest {

    private FilterOfferedBudgetItemProvider provider;
    private final ItemProviderTestUtil itemProviderTestUtil = new ItemProviderTestUtil();

    @Before
    public void setUp() {
        provider = new FilterOfferedBudgetItemProvider();
    }

    @Test
    public void get_whenRetrievingList_thenAllItemsAdded() {
        provider.createNewList(new SearchAdvertListPropertiesOffered(), null);
        assertEquals(4, provider.get().size());
    }

    @Test
    public void createNewList_whenEmptyPropertyList_thenDefaultFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        final List<FilterItem> list = provider.get();

        assertNull(((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, PRICE_RANGE)).min());
        assertNull(((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, PRICE_RANGE)).max());
        assertFalse(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, BILLS_INCLUDED)).selected());
    }

    @Test
    public void createNewList_whenFullPropertyList_thenFilledFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        final List<FilterItem> list = provider.get();

        assertEquals(DEFAULT_MIN_RENT_INT_VALUE, ((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, PRICE_RANGE)).min());
        assertEquals(DEFAULT_MAX_RENT_INT_VALUE, ((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, PRICE_RANGE)).max());
        assertTrue(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, BILLS_INCLUDED)).selected());
    }

    @Test
    public void saveProperties_whenEmptyList_andEmptyProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        testEmptyList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenEmptyList_andFullProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        testEmptyList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenFullList_andEmptyProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        testFullList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenFullList_andFullProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        testFullList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenSortFlagIsFalse_thenShouldNotSortBudget() {
        SearchAdvertListPropertiesOffered budgetProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        setRevertedMinMaxRent(budgetProperties);
        provider.updateProperties(budgetProperties, false);

        assertNotNull(budgetProperties.getMinRent());
        assertEquals(2, budgetProperties.getMinRent(), 0);
        assertNotNull(budgetProperties.getMaxRent());
        assertEquals(1, budgetProperties.getMaxRent(), 0);
    }

    @Test
    public void saveProperties_whenSortFlagIsTrue_thenShouldSortBudget() {
        SearchAdvertListPropertiesOffered budgetProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        setRevertedMinMaxRent(budgetProperties);
        provider.updateProperties(budgetProperties, true);

        assertNotNull(budgetProperties.getMinRent());
        assertEquals(1, budgetProperties.getMinRent(), 0);
        assertNotNull(budgetProperties.getMaxRent());
        assertEquals(2, budgetProperties.getMaxRent(), 0);
    }

    private void testFullList(final SearchAdvertListPropertiesOffered properties) {
        provider.updateProperties(properties, true);

        assertNotNull(properties.getMinRent());
        assertEquals(DEFAULT_MAX_RENT_INT_VALUE, properties.getMaxRent());
        assertNotNull(properties.getMaxRent());
        assertEquals(DEFAULT_MIN_RENT_INT_VALUE, properties.getMinRent());
        assertTrue(properties.billsInc());
    }

    private void testEmptyList(final SearchAdvertListPropertiesOffered properties) {
        provider.updateProperties(properties, true);

        assertNull(properties.getMaxRent());
        assertNull(properties.getMinRent());
        assertFalse(properties.billsInc());
    }

    private void setRevertedMinMaxRent(SearchAdvertListPropertiesOffered properties) {
        properties.minMonthlyRent(2);
        properties.maxMonthlyRent(1);
        provider.createNewList(properties, null);
    }

}
