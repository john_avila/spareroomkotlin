package com.spareroom.ui.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringUtilsTest {

    @Test
    public void equals_shouldNotBeEqual_whenStringsAreDifferent() {
        assertFalse(StringUtils.equals("1", "2"));
        assertFalse(StringUtils.equals("1", "22"));
    }

    @Test
    public void equals_shouldNotBeEqual_whenOneStringIsNull() {
        assertFalse(StringUtils.equals(null, ""));
        assertFalse(StringUtils.equals(null, " "));
        assertFalse(StringUtils.equals(null, "1"));
        assertFalse(StringUtils.equals("", null));
        assertFalse(StringUtils.equals("1", null));
    }

    @Test
    public void equals_shouldBeEqual_whenBothStringsAreNull() {
        assertTrue(StringUtils.equals(null, null));
    }

    @Test
    public void equals_shouldBeEqual_whenBothStringsAreEmpty() {
        assertTrue(StringUtils.equals("", ""));
    }

    @Test
    public void equals_shouldBeEqual_whenBothStringsAreEqual() {
        String stringOne = "";
        assertTrue(StringUtils.equals(stringOne, stringOne));
        assertTrue(StringUtils.equals("1", "1"));
        assertTrue(StringUtils.equals("abc", "abc"));
        assertTrue(StringUtils.equals(new StringBuilder("abc"), "abc"));
    }

    @Test
    public void isNullOrEmpty_shouldReturnTrue_whenStringNullOrEmpty() {
        assertTrue(StringUtils.isNullOrEmpty(null));
        assertTrue(StringUtils.isNullOrEmpty(""));
    }

    @Test
    public void isNullOrEmpty_shouldReturnFalse_whenStringIsNotNullOrEmpty() {
        assertFalse(StringUtils.isNullOrEmpty("1"));
        assertFalse(StringUtils.isNullOrEmpty(" "));
    }

    @Test
    public void getFirstString_shouldReturnEmptyString_whenNullOrEmptyStringProvided() {
        assertEquals("", StringUtils.getFirstString(null));
        assertEquals("", StringUtils.getFirstString(""));
    }

    @Test
    public void getFirstString_shouldReturnSameString_whenOnlyOneStringProvided() {
        assertEquals("hey", StringUtils.getFirstString("hey"));
        assertEquals("a", StringUtils.getFirstString("a "));
        assertEquals("a", StringUtils.getFirstString(" a"));
        assertEquals("a", StringUtils.getFirstString(" a "));
    }

    @Test
    public void getFirstString_shouldReturnFirstString_whenMultipleStringsProvided() {
        assertEquals("one", StringUtils.getFirstString("one two"));
        assertEquals("one", StringUtils.getFirstString(" one  two"));
        assertEquals("one", StringUtils.getFirstString("    one       two"));
    }

    @Test
    public void getInitials_shouldReturnEmptyStringWhenNoNames() {
        assertEquals("", StringUtils.getInitials("", ""));
        assertEquals("", StringUtils.getInitials(null, null));
        assertEquals("", StringUtils.getInitials("", null));
        assertEquals("", StringUtils.getInitials(null, ""));
        assertEquals("", StringUtils.getInitials(" ", " "));
    }

    @Test
    public void getInitials_shouldReturnProperInitials_whenAtLeastOneNameProvided() {
        assertEquals("RZ", StringUtils.getInitials("Robert", "Z"));
        assertEquals("RZ", StringUtils.getInitials("Robert R", "Z"));
        assertEquals("RZ", StringUtils.getInitials("robert r", "z"));
        assertEquals("R", StringUtils.getInitials("robert z", ""));
        assertEquals("R", StringUtils.getInitials("robert", ""));
        assertEquals("R", StringUtils.getInitials("", "r"));
    }

    @Test
    public void getInitialsWithFullFirstName_shouldReturnEmptyString_whenNoNamesProvided() {
        assertEquals("", StringUtils.getInitialsWithFullFirstName("", ""));
        assertEquals("", StringUtils.getInitialsWithFullFirstName(null, null));
        assertEquals("", StringUtils.getInitialsWithFullFirstName("", null));
        assertEquals("", StringUtils.getInitialsWithFullFirstName(null, ""));
        assertEquals("", StringUtils.getInitialsWithFullFirstName(" ", " "));
    }

    @Test
    public void getInitialsWithFullFirstName_shouldReturnProperInitials_whenAtLeastOneNameProvided() {
        assertEquals("Robert Z.", StringUtils.getInitialsWithFullFirstName("Robert", "Z"));
        assertEquals("Robert Jake Z.", StringUtils.getInitialsWithFullFirstName("Robert Jake", "Z"));
        assertEquals("Robert Jake L.", StringUtils.getInitialsWithFullFirstName("Robert Jake", "LastNameOne LastNameTwo"));
        assertEquals("Robert R Z.", StringUtils.getInitialsWithFullFirstName("Robert r", "Z"));
        assertEquals("Robert R Z.", StringUtils.getInitialsWithFullFirstName("robert r", "z"));
        assertEquals("Robert Z", StringUtils.getInitialsWithFullFirstName("robert z", ""));
        assertEquals("Robert", StringUtils.getInitialsWithFullFirstName("robert", ""));
        assertEquals("R.", StringUtils.getInitialsWithFullFirstName("", "r"));
        assertEquals("R", StringUtils.getInitialsWithFullFirstName("r", ""));
        assertEquals("R R.", StringUtils.getInitialsWithFullFirstName("r", "      r"));
        assertEquals("R R.", StringUtils.getInitialsWithFullFirstName("   r  ", " r "));
    }

    @Test
    public void trim_shouldReturnEmptyString_whenStringIsNullOrEmpty() {
        assertEquals("", StringUtils.trim(null));
        assertEquals("", StringUtils.trim(""));
    }

    @Test
    public void trim_shouldReturnTrimmedString() {
        assertEquals("test", StringUtils.trim("test"));
        assertEquals("test", StringUtils.trim(" test"));
        assertEquals("test", StringUtils.trim("test "));
        assertEquals("test", StringUtils.trim(" test "));
        assertEquals("test", StringUtils.trim("\ntest "));
        assertEquals("test", StringUtils.trim("test\n"));
        assertEquals("test", StringUtils.trim("\ntest\n"));
        assertEquals("test", StringUtils.trim("\n test\n "));
        assertEquals("test", StringUtils.trim(" \n test\n "));
        assertEquals("test", StringUtils.trim("   \n test   \n "));
        assertEquals("test", StringUtils.trim(" \t test \t\n "));
        assertEquals("test", StringUtils.trim("\t\n\t\ntest\t\n\t\n"));
        assertEquals("t e s t", StringUtils.trim("\t\n\t\nt e s t\t\n\t\n"));
    }

    @Test
    public void toDouble_whenProperFormat_thenReturnDouble() {
        Double number = StringUtils.toDouble("12000.95");
        assertNotNull(number);
        assertEquals(12000.95, number, 0);
    }

    @Test
    public void toDouble_whenWrongFormat_thenNull() {
        Double numberWithCommaAsThousandsSeparator = StringUtils.toDouble("12,000.00");
        assertNull(numberWithCommaAsThousandsSeparator);

        Double numberWithCommaAsDecimalSeparator = StringUtils.toDouble("12.000,00");
        assertNull(numberWithCommaAsDecimalSeparator);

        Double randomString = StringUtils.toDouble("This is a random String");
        assertNull(randomString);
    }

}
