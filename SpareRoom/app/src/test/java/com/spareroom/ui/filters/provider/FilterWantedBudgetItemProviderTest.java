package com.spareroom.ui.filters.provider;

import com.spareroom.TestApplication;
import com.spareroom.TestConstants;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted;
import com.spareroom.ui.widget.params.MinMaxEditTextParams;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static com.spareroom.ui.filters.provider.FilterWantedBudgetItemProvider.PRICE_RANGE;
import static com.spareroom.ui.filters.provider.ItemProviderTestUtil.DEFAULT_MAX_RENT_INT_VALUE;
import static com.spareroom.ui.filters.provider.ItemProviderTestUtil.DEFAULT_MIN_RENT_INT_VALUE;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class FilterWantedBudgetItemProviderTest {

    private FilterWantedBudgetItemProvider provider;
    private final ItemProviderTestUtil itemProviderTestUtil = new ItemProviderTestUtil();

    @Before
    public void setUp() {
        provider = new FilterWantedBudgetItemProvider();
    }

    @Test
    public void get_whenRetrievingList_thenAllItemsAdded() {
        provider.createNewList(new SearchAdvertListPropertiesWanted(), null);
        assertEquals(2, provider.get().size());
    }

    @Test
    public void createNewList_whenEmptyPropertyList_thenDefaultFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted(), null);
        final List<FilterItem> list = provider.get();

        assertNull(((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, PRICE_RANGE)).min());
        assertNull(((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, PRICE_RANGE)).max());
    }

    @Test
    public void createNewList_whenFullPropertyList_thenFilledFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted(), null);
        final List<FilterItem> list = provider.get();

        assertEquals(DEFAULT_MIN_RENT_INT_VALUE, ((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, PRICE_RANGE)).min());
        assertEquals(DEFAULT_MAX_RENT_INT_VALUE, ((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, PRICE_RANGE)).max());
    }

    @Test
    public void saveProperties_whenEmptyList_andEmptyProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted(), null);
        testEmptyList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenEmptyList_andFullProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted(), null);
        testEmptyList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenFullList_andEmptyProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted(), null);
        testFullList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenFullList_andFullProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted(), null);
        testFullList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenSortFlagIsFalse_thenShouldNotSortBudget() {
        SearchAdvertListPropertiesWanted budgetProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted();
        setRevertedMinMaxRent(budgetProperties);
        provider.updateProperties(budgetProperties, false);

        assertNotNull(budgetProperties.getMinRent());
        assertEquals(2, budgetProperties.getMinRent(), 0);
        assertNotNull(budgetProperties.getMaxRent());
        assertEquals(1, budgetProperties.getMaxRent(), 0);
    }

    @Test
    public void saveProperties_whenSortFlagIsTrue_thenShouldSortBudget() {
        SearchAdvertListPropertiesWanted budgetProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted();
        setRevertedMinMaxRent(budgetProperties);
        provider.updateProperties(budgetProperties, true);

        assertNotNull(budgetProperties.getMinRent());
        assertEquals(1, budgetProperties.getMinRent(), 0);
        assertNotNull(budgetProperties.getMaxRent());
        assertEquals(2, budgetProperties.getMaxRent(), 0);
    }

    private void testFullList(final SearchAdvertListPropertiesWanted properties) {
        provider.updateProperties(properties, true);

        assertEquals(DEFAULT_MIN_RENT_INT_VALUE, properties.getMinRent());
        assertEquals(DEFAULT_MAX_RENT_INT_VALUE, properties.getMaxRent());
    }

    private void testEmptyList(final SearchAdvertListPropertiesWanted properties) {
        provider.updateProperties(properties, true);

        assertNull(properties.getMinRent());
        assertNull(properties.getMaxRent());
    }

    private void setRevertedMinMaxRent(SearchAdvertListPropertiesWanted properties) {
        properties.minMonthlyRent(2);
        properties.maxMonthlyRent(1);
        provider.createNewList(properties, null);
    }

}
