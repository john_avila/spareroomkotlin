package com.spareroom.ui.filters.provider;

import com.spareroom.TestApplication;
import com.spareroom.TestConstants;
import com.spareroom.model.business.SearchAdvertListProperties.Smoking;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.RoomsFor;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static com.spareroom.ui.filters.provider.FilterOfferedSuitableForItemProvider.*;
import static com.spareroom.ui.filters.provider.ItemProviderTestUtil.DEFAULT_ROOMS_FOR_VALUE;
import static com.spareroom.ui.filters.provider.ItemProviderTestUtil.DEFAULT_SMOKING_VALUE;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class FilterOfferedSuitableForItemProviderTest {

    private FilterOfferedSuitableForItemProvider provider;
    private final ItemProviderTestUtil itemProviderTestUtil = new ItemProviderTestUtil();

    @Before
    public void setUp() {
        provider = new FilterOfferedSuitableForItemProvider();
    }

    @Test
    public void get_whenRetrievingList_thenAllItemsAdded() {
        provider.createNewList(new SearchAdvertListPropertiesOffered(), null);
        assertEquals(13, provider.get().size());
    }

    @Test
    public void createNewList_whenEmptyPropertyList_thenDefaultFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        final List<FilterItem> list = provider.get();

        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, ALL_FEMALE)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, ALL_MALE)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, COUPLES)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, ROOMS_FOR_NO_PREFERENCE)).selected());
        assertFalse(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, BENEFITS)).selected());
        assertFalse(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, PETS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, SMOKERS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, NON_SMOKERS)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, SMOKERS_NO_PREFERENCE)).selected());
    }

    @Test
    public void createNewList_whenFullPropertyList_thenFilledFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        final List<FilterItem> list = provider.get();

        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, ALL_FEMALE)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, ALL_MALE)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, COUPLES)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, ROOMS_FOR_NO_PREFERENCE)).selected());
        assertTrue(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, BENEFITS)).selected());
        assertTrue(((CompoundButtonParams) itemProviderTestUtil.getParameter(list, PETS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, SMOKERS)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, NON_SMOKERS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, SMOKERS_NO_PREFERENCE)).selected());
    }

    @Test
    public void saveProperties_whenEmptyList_andEmptyProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        testEmptyList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenEmptyList_andFullProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        testEmptyList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenFullList_andEmptyProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        testFullList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenFullList_andFullProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        testFullList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered());
    }

    private void testFullList(final SearchAdvertListPropertiesOffered properties) {
        provider.updateProperties(properties, false);

        assertEquals(DEFAULT_ROOMS_FOR_VALUE, properties.getRoomsFor());
        assertTrue(properties.getBenefits());
        assertTrue(properties.getPets());
        assertEquals(DEFAULT_SMOKING_VALUE, properties.getSmoking());
    }

    private void testEmptyList(final SearchAdvertListPropertiesOffered properties) {
        provider.updateProperties(properties, false);

        assertEquals(RoomsFor.NOT_SET, properties.getRoomsFor());
        assertFalse(properties.getBenefits());
        assertFalse(properties.getPets());
        assertEquals(Smoking.NOT_SET, properties.getSmoking());
    }

}
