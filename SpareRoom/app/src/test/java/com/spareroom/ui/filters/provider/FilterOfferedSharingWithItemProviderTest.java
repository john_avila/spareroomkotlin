package com.spareroom.ui.filters.provider;

import com.spareroom.TestApplication;
import com.spareroom.TestConstants;
import com.spareroom.model.business.SearchAdvertListProperties.GenderFilter;
import com.spareroom.model.business.SearchAdvertListProperties.ShareType;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.*;
import com.spareroom.ui.filters.adapter.viewparams.*;
import com.spareroom.ui.widget.params.MinMaxEditTextParams;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static com.spareroom.ui.filters.provider.FilterOfferedSharingWithItemProvider.*;
import static com.spareroom.ui.filters.provider.ItemProviderTestUtil.*;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class FilterOfferedSharingWithItemProviderTest {

    private FilterOfferedSharingWithItemProvider provider;
    private final ItemProviderTestUtil itemProviderTestUtil = new ItemProviderTestUtil();

    @Before
    public void setUp() {
        provider = new FilterOfferedSharingWithItemProvider();
    }

    @Test
    public void get_whenRetrievingList_thenAllItemsAdded() {
        provider.createNewList(new SearchAdvertListPropertiesOffered(), null);
        assertEquals(23, provider.get().size());
    }

    @Test
    public void createNewList_whenEmptyPropertyList_thenDefaultFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        final List<FilterItem> list = provider.get();

        assertNull(((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, AGE_RANGE)).min());
        assertNull(((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, AGE_RANGE)).max());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, MIXED)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, FEMALES)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, MALES)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, GENDER_NO_PREFERENCES)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, PROFESSIONALS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, STUDENTS)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, SHARED_TYPE_NO_PREFERENCE)).selected());
        assertEquals(MinFlatmates.NOT_SET, ((SummaryViewMinFlatmatesParams) itemProviderTestUtil.getParameter(list, NUMBER_OF_FLATMATES_MIN)).minFlatmates());
        assertEquals(MaxFlatmates.NOT_SET, ((SummaryViewMaxFlatmatesParams) itemProviderTestUtil.getParameter(list, NUMBER_OF_FLATMATES_MAX)).maxFlatmates());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, WITH_LANDLORD)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, NO_LANDLORD)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, LANDLORD_NO_PREFERENCE)).selected());
    }

    @Test
    public void createNewList_whenFullPropertyList_thenFilledFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        final List<FilterItem> list = provider.get();

        assertEquals(DEFAULT_MIN_AGE_REQ_INT_VALUE, ((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, AGE_RANGE)).min());
        assertEquals(DEFAULT_MAX_AGE_REQ_INT_VALUE, ((MinMaxEditTextParams) itemProviderTestUtil.getParameter(list, AGE_RANGE)).max());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, MIXED)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, FEMALES)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, MALES)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, GENDER_NO_PREFERENCES)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, PROFESSIONALS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, STUDENTS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, SHARED_TYPE_NO_PREFERENCE)).selected());
        assertEquals(DEFAULT_MIN_FLATMATES_VALUE, ((SummaryViewMinFlatmatesParams) itemProviderTestUtil.getParameter(list, NUMBER_OF_FLATMATES_MIN)).minFlatmates());
        assertEquals(DEFAULT_MAX_FLATMATES_VALUE, ((SummaryViewMaxFlatmatesParams) itemProviderTestUtil.getParameter(list, NUMBER_OF_FLATMATES_MAX)).maxFlatmates());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, WITH_LANDLORD)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, NO_LANDLORD)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, LANDLORD_NO_PREFERENCE)).selected());
    }

    @Test
    public void saveProperties_whenEmptyList_andEmptyProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        testEmptyList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenEmptyList_andFullProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        testEmptyList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenFullList_andEmptyProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        testFullList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenFullList_andFullProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        testFullList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenSortFlagIsTrue_thenShouldSortAge_andShouldSortFlatmates() {
        SearchAdvertListPropertiesOffered ageProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        setRevertedAge(ageProperties);
        provider.updateProperties(ageProperties, true);
        assertNotNull(ageProperties.getMinAge());
        assertEquals(19, ageProperties.getMinAge(), 0);
        assertNotNull(ageProperties.getMaxAge());
        assertEquals(20, ageProperties.getMaxAge(), 0);

        SearchAdvertListPropertiesOffered flatmatesProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        setRevertedFlatmates(flatmatesProperties);
        provider.updateProperties(flatmatesProperties, true);
        assertEquals(MinFlatmates.TWO, flatmatesProperties.getMinFlatmates());
        assertEquals(MaxFlatmates.FIVE, flatmatesProperties.maxFlatmates());

    }

    @Test
    public void saveProperties_whenSortFlagIsFalse_thenShouldNotSortAge_andShouldNotSortFlatmates() {
        SearchAdvertListPropertiesOffered ageProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        setRevertedAge(ageProperties);
        provider.updateProperties(ageProperties, false);
        assertNotNull(ageProperties.getMinAge());
        assertEquals(20, ageProperties.getMinAge(), 0);
        assertNotNull(ageProperties.getMaxAge());
        assertEquals(19, ageProperties.getMaxAge(), 0);

        SearchAdvertListPropertiesOffered flatmatesProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered();
        setRevertedFlatmates(flatmatesProperties);
        provider.updateProperties(flatmatesProperties, false);
        assertEquals(MinFlatmates.FIVE, flatmatesProperties.getMinFlatmates());
        assertEquals(MaxFlatmates.TWO, flatmatesProperties.maxFlatmates());
    }

    private void testFullList(final SearchAdvertListPropertiesOffered properties) {
        provider.updateProperties(properties, true);

        assertEquals(DEFAULT_GENDER_FILTER_VALUE, properties.getGenderFilter());
        assertEquals(DEFAULT_MIN_AGE_REQ_INT_VALUE, properties.getMinAge());
        assertEquals(DEFAULT_MAX_AGE_REQ_INT_VALUE, properties.getMaxAge());
        assertEquals(DEFAULT_SHARE_TYPE_VALUE, properties.getShareType());
        assertEquals(DEFAULT_MIN_FLATMATES_VALUE, properties.getMinFlatmates());
        assertEquals(DEFAULT_MAX_FLATMATES_VALUE, properties.maxFlatmates());
        assertEquals(DEFAULT_LANDLORD_VALUE, properties.getLandlord());
    }

    private void testEmptyList(final SearchAdvertListPropertiesOffered properties) {
        provider.updateProperties(properties, true);

        assertEquals(GenderFilter.NOT_SET, properties.getGenderFilter());
        assertNull(properties.getMinAge());
        assertNull(properties.getMaxAge());
        assertEquals(ShareType.NOT_SET, properties.getShareType());
        assertEquals(MinFlatmates.NOT_SET, properties.getMinFlatmates());
        assertEquals(MaxFlatmates.NOT_SET, properties.maxFlatmates());
        assertEquals(Landlord.NOT_SET, properties.getLandlord());
    }

    private void setRevertedFlatmates(SearchAdvertListPropertiesOffered properties) {
        properties.setMinFlatmates(MinFlatmates.FIVE);
        properties.maxFlatmates(MaxFlatmates.TWO);
        provider.createNewList(properties, null);
    }

    private void setRevertedAge(SearchAdvertListPropertiesOffered properties) {
        properties.setMinAge(20);
        properties.setMaxAge(19);
        provider.createNewList(properties, null);
    }

}
