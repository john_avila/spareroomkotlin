package com.spareroom.ui.filters.provider;

import com.spareroom.TestApplication;
import com.spareroom.TestConstants;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.PostedBy;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static com.spareroom.ui.filters.provider.FilterOfferedPostedByItemProvider.*;
import static com.spareroom.ui.filters.provider.ItemProviderTestUtil.DEFAULT_POSTED_BY_VALUE;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class FilterOfferedPostedByItemProviderTest {

    private FilterOfferedPostedByItemProvider provider;
    private final ItemProviderTestUtil itemProviderTestUtil = new ItemProviderTestUtil();

    @Before
    public void setUp() {
        provider = new FilterOfferedPostedByItemProvider();
    }

    @Test
    public void get_whenRetrievingList_thenAllItemsAdded() {
        provider.createNewList(new SearchAdvertListPropertiesOffered(), null);
        assertEquals(4, provider.get().size());
    }

    @Test
    public void createNewList_whenEmptyPropertyList_thenDefaultFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        final List<FilterItem> list = provider.get();

        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, AGENTS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, INDIVIDUAL_USER)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, NO_PREFERENCE)).selected());
    }

    @Test
    public void createNewList_whenFullPropertyList_thenFilledFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        final List<FilterItem> list = provider.get();

        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, AGENTS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, INDIVIDUAL_USER)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, NO_PREFERENCE)).selected());
    }

    @Test
    public void saveProperties_whenEmptyList_andEmptyProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        testEmptyList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenEmptyList_andFullProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered(), null);
        testEmptyList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenFullList_andEmptyProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        testFullList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesOffered());
    }

    @Test
    public void saveProperties_whenFullList_andFullProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered(), null);
        testFullList(itemProviderTestUtil.createFullSearchAdvertListPropertiesOffered());
    }

    private void testFullList(final SearchAdvertListPropertiesOffered properties) {
        provider.updateProperties(properties, true);

        assertEquals(DEFAULT_POSTED_BY_VALUE, properties.getPostedBy());
    }

    private void testEmptyList(final SearchAdvertListPropertiesOffered properties) {
        provider.updateProperties(properties, true);

        assertEquals(PostedBy.NOT_SET, properties.getPostedBy());
    }

}
