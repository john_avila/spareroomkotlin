package com.spareroom.ui;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EmailFieldValidatorTest {
    private EmailFieldValidator validator = new EmailFieldValidator();

    @Test
    public void validate_whenValidEmail_thenReturnValidStatus() {
        assertEquals(EmailFieldValidator.VALID, validator.validate("local@domain.com"));
        assertEquals(EmailFieldValidator.VALID, validator.validate("robert.rz!#$%&'*+-/=?^_`{|}~@spareroom.co.uk"));
        assertEquals(EmailFieldValidator.VALID, validator.validate("robert.asa       &'*+-/=?^_`{|}~@spar     oom.co.uk"));
        assertEquals(EmailFieldValidator.VALID, validator.validate("robert.rz@@@@@@@@@'*+-/=?^_`{|}~@spareroom.co.uk"));
        assertEquals(EmailFieldValidator.VALID, validator.validate("   r obe r t. r z' *+ -/ = ? ^ _`{ | } ~ @sp a r e r o o m. c o . u k @@@"));
    }

    @Test
    public void validate_whenEmptyEmail_thenReturnEmptyStatus() {
        assertEquals(EmailFieldValidator.EMPTY, validator.validate(""));
    }

    @Test
    public void validate_whenNoAtInTheEmail_thenReturnMalformedAddressStatus() {
        assertEquals(EmailFieldValidator.MALFORMED_ADDRESS, validator.validate("domain.com"));
        assertEquals(EmailFieldValidator.MALFORMED_ADDRESS, validator.validate("robert.asa       &'*+-/=?^_`{|} spar     oom.co.uk"));
    }

    @Test
    public void validate_whenNoDotInTheEmail_thenReturnMalformedAddressStatus() {
        assertEquals(EmailFieldValidator.MALFORMED_ADDRESS, validator.validate("local@domain"));
        assertEquals(EmailFieldValidator.MALFORMED_ADDRESS, validator.validate("robert.rz@@@@@@@@@'*+-/=?^_`{|}~@spareroomcouk"));
    }

    @Test
    public void validate_whenNoStringAtBeginningOfTheEmail_thenReturnMalformedAddressStatus() {
        assertEquals(EmailFieldValidator.MALFORMED_ADDRESS, validator.validate("@domain.com"));
        assertEquals(EmailFieldValidator.MALFORMED_ADDRESS, validator.validate("   @robert.rz@@@@@@@@@'*+-/=?^_`{|}~@spareroomcouk"));
    }

    @Test
    public void validate_whenNoStringInTheMiddleOfTheEmail_thenReturnMalformedAddressStatus() {
        assertEquals(EmailFieldValidator.MALFORMED_ADDRESS, validator.validate("domain@.com"));
        assertEquals(EmailFieldValidator.MALFORMED_ADDRESS, validator.validate("robert.rz@.'*+-/=?^_`{|}~@spareroomcouk"));
    }

    @Test
    public void validate_whenNoStringAtTheEndOfTheEmail_thenReturnMalformedAddressStatus() {
        assertEquals(EmailFieldValidator.MALFORMED_ADDRESS, validator.validate("domain@domain."));
        assertEquals(EmailFieldValidator.MALFORMED_ADDRESS, validator.validate("domain@domain.      "));
    }

    @Test
    public void isEmailFormatValid_whenValidEmail_thenReturnTrue() {
        assertTrue(validator.isEmailFormatValid("local@domain.com"));
        assertTrue(validator.isEmailFormatValid("robert.rz!#$%&'*+-/=?^_`{|}~@spareroom.co.uk"));
    }

    @Test
    public void isEmailFormatValid_whenNoAtInTheEmail_thenReturnFalse() {
        assertFalse(validator.isEmailFormatValid("domain.com"));
    }

    @Test
    public void isEmailFormatValid_whenNoDotInTheEmail_thenReturnFalse() {
        assertFalse(validator.isEmailFormatValid("local@domain"));
    }

    @Test
    public void isEmailFormatValid_whenNoStringAtBeginningOfTheEmail_thenReturnFalse() {
        assertFalse(validator.isEmailFormatValid("@domain.com"));
    }

    @Test
    public void isEmailFormatValid_whenNoStringInTheMiddleOfTheEmail_thenReturnFalse() {
        assertFalse(validator.isEmailFormatValid("domain@.com"));
    }

    @Test
    public void isEmailFormatValid_whenNoStringAtTheEndOfTheEmail_thenReturnFalse() {
        assertFalse(validator.isEmailFormatValid("domain@domain."));
    }

}
