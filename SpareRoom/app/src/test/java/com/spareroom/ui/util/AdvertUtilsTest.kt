package com.spareroom.ui.util

import android.app.Application
import android.content.Context
import androidx.annotation.StringRes
import com.spareroom.*
import com.spareroom.TestUtils.any
import com.spareroom.model.business.*
import com.spareroom.model.business.advertiser.*
import com.spareroom.ui.util.StringUtils.EN_DASH
import org.junit.*
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import java.util.*
import java.util.Arrays.*
import kotlin.collections.ArrayList
import kotlin.math.ceil

private const val NOT_AVAILABLE = "n/a"
private const val NEWNESS_NEW = "NEW"
private const val NEWNESS_NEW_TODAY = "NEW TODAY"
private const val NEWNESS_BOOSTED = "BOOSTED"
private const val STUDIO = "studio"
private const val FLAT = "flat"
private const val HOUSE = "house"
private const val PROPERTY = "property"
private const val BEDROOM = "bedroom"
private const val BEDROOMS = "bedrooms"
private const val SINGLE = "Single"
private const val SINGLE_LOWER_CASE = "single"
private const val DOUBLE = "Double"
private const val DOUBLE_LOWER_CASE = "double"
private const val WITH_ENSUITE = "with Ensuite"
private const val IN = "in"
private const val SEARCHING_FOR = "Searching for"
private const val AREAS = "areas"
private const val INCLUDING = "including"
private const val INCLUDING_BILLS = "including bills"
private const val INCLUDING_SOME_BILLS = "including some bills"
private const val EXCLUDING_BILLS = "excluding bills"
private const val CURRENCY_SYMBOL = "£"
private const val NO_CONNECTION = "no connection"
private const val OR = "or"
private const val TWIN = "a twin"
private const val WHOLE = "whole"
private const val ADVERTS = "Adverts"

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [(TestConstants.CONFIG_SDK)], application = TestApplication::class)
class AdvertUtilsTest {

    private lateinit var advertUtils: AdvertUtils
    private lateinit var defaultLocale: Locale
    private lateinit var context: Application

    @Before
    fun setUp() {
        defaultLocale = Locale.getDefault()
        Locale.setDefault(Locale.US)

        context = spy(RuntimeEnvironment.application)
        mockStrings(context)

        advertUtils = AdvertUtils(Locale.getDefault(), context, mock(ConnectivityChecker::class.java))
    }

    @After
    fun tearDown() {
        Locale.setDefault(defaultLocale)
    }

    private fun mockStrings(context: Context) {
        doReturn(CURRENCY_SYMBOL).`when`(context).getString(R.string.currency_symbol)
        doReturn(NOT_AVAILABLE).`when`(context).getString(R.string.not_available)
        doReturn(INCLUDING_BILLS).`when`(context).getString(R.string.includingBills)
        doReturn(INCLUDING_SOME_BILLS).`when`(context).getString(R.string.includingSomeBills)
        doReturn(EXCLUDING_BILLS).`when`(context).getString(R.string.excludingBills)
        doReturn(NEWNESS_NEW).`when`(context).getString(R.string.newness_new)
        doReturn(NEWNESS_NEW_TODAY).`when`(context).getString(R.string.newness_new_today)
        doReturn(NEWNESS_BOOSTED).`when`(context).getString(R.string.newness_boosted)
        doReturn(STUDIO).`when`(context).getString(R.string.studio)
        doReturn(HOUSE).`when`(context).getString(R.string.house)
        doReturn(PROPERTY).`when`(context).getString(R.string.property)
        doReturn(FLAT).`when`(context).getString(R.string.flat)
        doReturn(BEDROOM).`when`(context).getString(R.string.bedroom)
        doReturn(SINGLE).`when`(context).getString(R.string.single_first_letter_upper_case)
        doReturn(DOUBLE).`when`(context).getString(R.string.double_first_letter_upper_case)
        doReturn(WITH_ENSUITE).`when`(context).getString(R.string.with_ensuite)
        doReturn(BEDROOMS).`when`(context).getString(R.string.bedrooms)
        doReturn(SINGLE_LOWER_CASE).`when`(context).getString(R.string.single)
        doReturn(DOUBLE_LOWER_CASE).`when`(context).getString(R.string.double_lower_case)
        doReturn(IN).`when`(context).getString(R.string.in_string)
        doReturn(SEARCHING_FOR).`when`(context).getString(R.string.searching_for)
        doReturn(INCLUDING).`when`(context).getString(R.string.including)
        doReturn(NO_CONNECTION).`when`(context).getString(R.string.no_connection)
        doReturn(OR).`when`(context).getString(R.string.or)
        doReturn(TWIN).`when`(context).getString(R.string.twin)
        doReturn(WHOLE).`when`(context).getString(R.string.whole)
        doReturn(ADVERTS).`when`(context).getString(R.string.adverts)
    }

    /* Monthly price */

    @Test
    fun getFormattedMonthlyPrice_shouldReturnNotAvailable_whenRentIsPerRoom_andAdvertHasNoRooms() {
        val advert = createOfferedAdvertWithRentPerRoom(rooms = emptyList())
        assertEquals("n/a", getMonthlyPriceWithBills(advert))
    }

    @Test
    fun getFormattedMonthlyPrice_shouldReturnNotAvailable_whenRentIsPerRoom_andAdvertHasNoAvailableRooms() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("500", Price.PERIODICITY_WEEK, status = "")))
        assertEquals("n/a", getMonthlyPriceWithBills(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_TAKEN),
            createRoom("600", Price.PERIODICITY_WEEK, AdRoom.STATUS_TAKEN)))

        assertEquals("n/a", getMonthlyPriceWithBills(advert))
    }

    @Test
    fun getFormattedMonthlyPrice_shouldReturnNotAvailable_whenRentIsPerRoom_andAdvertHasRoomsWithMalformedPrice() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("500ABC", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE)))
        assertEquals("n/a", getMonthlyPriceWithBills(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("500ABC", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE)))
        assertEquals("n/a", getMonthlyPriceWithBills(advert))

        assertEquals("n/a", getMonthlyPriceWithBills(advert))
    }

    @Test
    fun getFormattedMonthlyPrice_shouldReturnNotAvailable_whenRentIsPerRoom_andNotAllRoomsHavePrices() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE)))
        assertEquals("n/a", getMonthlyPriceWithBills(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("500", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE),
            createRoom("", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE)))

        assertEquals("n/a", getMonthlyPriceWithBills(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE),
            createRoom("", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE)))

        assertEquals("n/a", getMonthlyPriceWithBills(advert))
    }

    @Test
    fun getFormattedMonthlyPrice_shouldReturnNotAvailable_whenRentIsForWholeProperty_andNoMinRent_andNoAvailableRoomsWithPrice() {
        // no min rent, no rooms
        var advert = createAdvertAsWholeProperty(minRent = "")
        assertEquals("n/a", getMonthlyPriceWithBills(advert))

        // no min rent, no price in rooms
        advert = spy(createAdvertAsWholeProperty(
            rooms = asList(createRoom("", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE)), minRent = ""))
        assertEquals("n/a", getMonthlyPriceWithBills(advert))

        // no min rent, no available rooms
        advert = spy(createAdvertAsWholeProperty(
            rooms = asList(createRoom("500", Price.PERIODICITY_WEEK, AdRoom.STATUS_TAKEN)), minRent = ""))
        assertEquals("n/a", getMonthlyPriceWithBills(advert))
    }

    @Test
    fun getFormattedMonthlyPrice_shouldReturnSumOfRoomPrices_whenRentIsForWholeProperty_andMinRentNotSpecified() {
        var advert = createAdvertAsWholeProperty(rooms = asList(createRoom("500", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE)))
        assertEquals("£${pwToPcm(500)}", getMonthlyPriceWithOutBills(advert))

        advert = createAdvertAsWholeProperty(rooms = asList(createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE)))
        assertEquals("£500", getMonthlyPriceWithOutBills(advert))

        advert = createAdvertAsWholeProperty(rooms = asList(
            createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
            createRoom("600", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE),
            createRoom("0", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE)))

        assertEquals("£${pwToPcm(600) + 500}", getMonthlyPriceWithOutBills(advert))
    }

    @Test
    fun getFormattedMonthlyPrice_shouldReturnMinRent_whenRentIsForWholeProperty_andMinRentIsSpecified() {
        var advert = createAdvertAsWholeProperty(rooms = asList(createRoom("500", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE)),
            pricePeriod = Price.PERIODICITY_MONTH, minRent = "1001")
        assertEquals("£1001", getMonthlyPriceWithOutBills(advert))

        advert = createAdvertAsWholeProperty(rooms = asList(createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE)),
            pricePeriod = Price.PERIODICITY_WEEK, minRent = "1000")
        assertEquals("£${pwToPcm(1000)}", getMonthlyPriceWithOutBills(advert))

        advert = createAdvertAsWholeProperty(rooms = asList(
            createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
            createRoom("600", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE),
            createRoom("0", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE)))

        doReturn("1000").`when`(advert).minRent
        assertEquals("£1000", getMonthlyPriceWithOutBills(advert))

        advert = createAdvertAsWholeProperty(rooms = asList(
            createRoom("", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE)), minRent = "1000")
        assertEquals("£1000", getMonthlyPriceWithOutBills(advert))

        advert = createAdvertAsWholeProperty(rooms = asList(
            createRoom("0", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE)), minRent = "1000")
        assertEquals("£1000", getMonthlyPriceWithOutBills(advert))
    }

    @Test
    fun getFormattedMonthlyPrice_shouldReturnRangeOfPrices_whenRentIsPerRoomOrEitherOrNotSpecified() {
        var advert = createAdvertWithRentPerRoom(
            asList(createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
                createRoom("600", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE),
                createRoom("0", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE)))

        assertEquals(range("0", "${pwToPcm(600)}"), getMonthlyPriceWithOutBills(advert))

        advert = createAdvertWithRentPerEither(
            asList(createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
                createRoom("600", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE),
                createRoom("0", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE)))

        assertEquals(range("0", "${pwToPcm(600)}"), getMonthlyPriceWithOutBills(advert))

        doReturn("").`when`(advert).rentOption
        assertEquals(range("0", "${pwToPcm(600)}"), getMonthlyPriceWithOutBills(advert))

        doReturn("notACorrectRentOption").`when`(advert).rentOption
        assertEquals(range("0", "${pwToPcm(600)}"), getMonthlyPriceWithOutBills(advert))
    }

    @Test
    fun getFormattedMonthlyPrice_shouldReturnSinglePrice_whenRentIsPerRoom_andAdvertHasRoomsWithSamePrice() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
            createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE)))

        assertEquals("£500 ", getMonthlyPriceWithBills(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("434", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
            createRoom("100", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE)))

        assertEquals("£${pwToPcm(100)} ", getMonthlyPriceWithBills(advert))
    }

    @Test
    fun getFormattedMonthlyPrice_shouldReturnPriceRange_whenRentIsPerRoom_andAdvertHasRoomsWithDifferentPrices() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
            createRoom("600", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
            createRoom("400", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE)))

        assertEquals(range("500", "${pwToPcm(400)} "), getMonthlyPriceWithBills(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("40", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
            createRoom("600", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
            createRoom("0", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE)))

        assertEquals(range("0", "600 "), getMonthlyPriceWithBills(advert))
    }

    @Test
    fun getFormattedMonthlyPrice_shouldReturnPrice_andSkipNotAvailableRooms_whenRentIsPerRoom() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
            createRoom("600", Price.PERIODICITY_MONTH, AdRoom.STATUS_TAKEN)))

        assertEquals("£500 ", getMonthlyPriceWithBills(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_TAKEN),
            createRoom("100", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE)))

        assertEquals("£${pwToPcm(100)} ", getMonthlyPriceWithBills(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
            createRoom("600", Price.PERIODICITY_MONTH, AdRoom.STATUS_TAKEN),
            createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE)))

        assertEquals("£500 ", getMonthlyPriceWithBills(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("434", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
            createRoom("100", Price.PERIODICITY_WEEK, AdRoom.STATUS_AVAILABLE),
            createRoom("100", Price.PERIODICITY_WEEK, AdRoom.STATUS_TAKEN)))

        assertEquals("£${pwToPcm(100)} ", getMonthlyPriceWithBills(advert))
    }

    @Test
    fun getFormattedMonthlyPrice_shouldReturnPrice_andBillsExcludedOption_whenBillsInfoRequested() {
        val advert = createOfferedAdvertWithRentPerRoom(billsIncluded = Price.BILLS_INCLUDED_NO,
            rooms = asList(
                createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
                createRoom("600", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE)))

        assertEquals("${range("500", "600")} $EXCLUDING_BILLS", getMonthlyPriceWithBills(advert))
    }

    @Test
    fun getFormattedMonthlyPrice_shouldReturnPrice_andBillsIncludedOption_whenBillsInfoRequested() {
        val advert = createOfferedAdvertWithRentPerRoom(billsIncluded = Price.BILLS_INCLUDED_YES,
            rooms = asList(
                createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
                createRoom("600", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE)))

        assertEquals("${range("500", "600")} $INCLUDING_BILLS", getMonthlyPriceWithBills(advert))
    }

    @Test
    fun getFormattedMonthlyPrice_shouldReturnPrice_andSomeBillsIncludedOption_whenBillsInfoRequested() {
        val advert = createOfferedAdvertWithRentPerRoom(billsIncluded = Price.BILLS_INCLUDED_SOME,
            rooms = asList(
                createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
                createRoom("600", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE)))

        assertEquals("${range("500", "600")} $INCLUDING_SOME_BILLS", getMonthlyPriceWithBills(advert))
    }

    @Test
    fun getFormattedMonthlyPrice_shouldReturnPrice_andNoInfoAboutBills_whenBillsInfoNotRequested() {
        var advert = createOfferedAdvertWithRentPerRoom(billsIncluded = Price.BILLS_INCLUDED_SOME,
            rooms = asList(
                createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
                createRoom("600", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE)))

        assertEquals(range("500", "600"), getMonthlyPriceWithOutBills(advert))

        advert = createOfferedAdvertWithRentPerRoom(billsIncluded = Price.BILLS_INCLUDED_NO,
            rooms = asList(
                createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
                createRoom("600", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE)))

        assertEquals(range("500", "600"), getMonthlyPriceWithOutBills(advert))

        advert = createOfferedAdvertWithRentPerRoom(billsIncluded = Price.BILLS_INCLUDED_YES,
            rooms = asList(
                createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE),
                createRoom("600", Price.PERIODICITY_MONTH, AdRoom.STATUS_AVAILABLE)))

        assertEquals(range("500", "600"), getMonthlyPriceWithOutBills(advert))
    }

    /* Location */

    @Test
    fun getFormattedLocation_shouldReturnDefaultString_whenLocationNotAvailable() {
        assertEquals(NOT_AVAILABLE, advertUtils.getFormattedLocation(createOfferedAdvertWithRentPerRoom(), NOT_AVAILABLE))
        assertEquals(NOT_AVAILABLE, advertUtils.getFormattedLocation(createOfferedAdvertWithRentPerRoom(" ", " "), NOT_AVAILABLE))
    }

    @Test
    fun getFormattedLocation_shouldReturnNeighbourhood_whenOnlyNeighbourhoodSpecified() {
        assertEquals("Salford", advertUtils.getFormattedLocation(createOfferedAdvertWithRentPerRoom("Salford"), NOT_AVAILABLE))
        assertEquals("Trafford", advertUtils.getFormattedLocation(createOfferedAdvertWithRentPerRoom("Trafford", ""), NOT_AVAILABLE))
        assertEquals("Trafford", advertUtils.getFormattedLocation(createOfferedAdvertWithRentPerRoom("Trafford", " "), NOT_AVAILABLE))
    }

    @Test
    fun getFormattedLocation_shouldReturnPostcode_whenOnlyPostcodeSpecified() {
        assertEquals("MM11B", advertUtils.getFormattedLocation(createOfferedAdvertWithRentPerRoom(postCode = "MM11B"), NOT_AVAILABLE))
        assertEquals("MM11B", advertUtils.getFormattedLocation(createOfferedAdvertWithRentPerRoom("", "MM11B"), NOT_AVAILABLE))
        assertEquals("MM11B", advertUtils.getFormattedLocation(createOfferedAdvertWithRentPerRoom(" ", "MM11B"), NOT_AVAILABLE))
    }

    @Test
    fun getFormattedLocation_shouldReturnNeighbourhoodAndPostcode_whenBothSpecified() {
        assertEquals("Manchester (MM11B)", advertUtils.getFormattedLocation(createOfferedAdvertWithRentPerRoom("Manchester", "MM11B"), NOT_AVAILABLE))
    }

    /* Monthly budget */

    @Test
    fun getFormattedMonthlyBudget_shouldReturnNotAvailable_whenPriceIsNull() {
        var advert = createWantedAdvert(period = Price.PERIODICITY_WEEK)
        assertEquals("n/a", advertUtils.getFormattedMonthlyBudget(advert))

        advert = createWantedAdvert("", Price.PERIODICITY_WEEK)
        assertEquals("n/a", advertUtils.getFormattedMonthlyBudget(advert))
    }

    @Test
    fun getFormattedMonthlyBudget_shouldReturnNotAvailable_whenPriceAndPeriodIsNull() {
        val advert = createWantedAdvert("", "")
        assertEquals("n/a", advertUtils.getFormattedMonthlyBudget(advert))
    }

    @Test
    fun getFormattedMonthlyBudget_shouldAssumeMonthlyPrice_whenPeriodIsNull() {
        val advert = createWantedAdvert("500", "")
        assertEquals("£500", advertUtils.getFormattedMonthlyBudget(advert))
    }

    @Test
    fun getFormattedMonthlyBudget_shouldReturnCorrectPrice_whenPriceSpecifiedPerWeek() {
        var advert = createWantedAdvert("500", Price.PERIODICITY_WEEK)
        assertEquals("£${pwToPcm(500)}", advertUtils.getFormattedMonthlyBudget(advert))

        advert = createWantedAdvert("0", Price.PERIODICITY_WEEK)
        assertEquals("£0", advertUtils.getFormattedMonthlyBudget(advert))
    }

    @Test
    fun getFormattedMonthlyBudget_shouldReturnCorrectPrice_whenPriceSpecifiedPerMonth() {
        var advert = createWantedAdvert("500", Price.PERIODICITY_MONTH)
        assertEquals("£500", advertUtils.getFormattedMonthlyBudget(advert))

        advert = createWantedAdvert("0", Price.PERIODICITY_MONTH)
        assertEquals("£0", advertUtils.getFormattedMonthlyBudget(advert))
    }

    /* Deposit */

    @Test
    fun getFormattedDeposit_shouldReturnNotAvailable_whenRentIsPerRoom_andAdvertHasNoRooms() {
        var advert = createOfferedAdvertWithRentPerRoom()
        assertEquals("n/a", getDeposit(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = emptyList())
        assertEquals("n/a", getDeposit(advert))
    }

    @Test
    fun getFormattedDeposit_shouldReturnNotAvailable_whenRentIsPerRoom_andAdvertHasNoAvailableRooms() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("500", Price.PERIODICITY_WEEK, "")))
        assertEquals("n/a", getDeposit(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("500", Price.PERIODICITY_MONTH, AdRoom.STATUS_TAKEN),
            createRoom("600", Price.PERIODICITY_WEEK, AdRoom.STATUS_TAKEN)))

        assertEquals("n/a", getDeposit(advert))
    }

    @Test
    fun getFormattedDeposit_shouldReturnNotAvailable_whenAdvertIsWholeProperty_AndHasNoDepositSpecifiedInHeaderAndRooms() {
        var advert = createAdvertAsWholeProperty(rooms = asList(createRoom(""), createRoom("")))
        assertEquals("n/a", getDeposit(advert))

        advert = createAdvertAsWholeProperty(rooms = asList(createRoom(""), createRoom("")), securityDeposit = "")
        assertEquals("n/a", getDeposit(advert))

        advert = createAdvertAsWholeProperty(rooms = asList(createRoom(""), createRoom("")), securityDeposit = "null")
        assertEquals("n/a", getDeposit(advert))
    }

    @Test
    fun getFormattedDeposit_shouldReturnDeposit_whenAdvertIsWholeProperty_andHasDepositSpecifiedInHeaderOrRooms() {
        var advert = createAdvertAsWholeProperty(rooms = asList(createRoom("400"), createRoom("500")))
        assertEquals("£900", getDeposit(advert))

        advert = createAdvertAsWholeProperty(rooms = asList(createRoom("100")), securityDeposit = "500")
        assertEquals("£500", getDeposit(advert))

        advert = createAdvertAsWholeProperty(rooms = asList(createRoom("100"), createRoom("200")), securityDeposit = "500")
        assertEquals("£500", getDeposit(advert))
    }

    @Test
    fun getFormattedDeposit_shouldReturnHeaderDeposit_whenAdvertIsWholeProperty_ansHasDepositSpecifiedInHeaderAndRooms() {
        var advert = createAdvertAsWholeProperty(rooms = asList(createRoom("100"), createRoom("200")), securityDeposit = "500")
        assertEquals("£500", getDeposit(advert))

        advert = createAdvertAsWholeProperty(rooms = asList(createRoom("100"), createRoom("200")), securityDeposit = "0")
        assertEquals("£0", getDeposit(advert))
    }

    @Test
    fun getFormattedDeposit_shouldReturnNotAvailable_whenRentIsPerRoom_andHasNoDeposits() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("")))
        assertEquals("n/a", getDeposit(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("")))
        assertEquals("n/a", getDeposit(advert))
    }

    @Test
    fun getFormattedDeposit_shouldReturnNotAvailable_whenRentIsPerRoom_andHasRoomsWithMalformedDeposit() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("not_a_number")))
        assertEquals("n/a", getDeposit(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("100,400")))
        assertEquals("n/a", getDeposit(advert))
    }

    @Test
    fun getFormattedDeposit_shouldReturnDeposit_andSkipRoomsWithoutDeposit_whenRentIsPerRoom() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom(""), createRoom("100")))
        assertEquals("£100", getDeposit(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom(""), createRoom("100"), createRoom("100")))
        assertEquals("£100", getDeposit(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom(""), createRoom("100"), createRoom("200"), createRoom("")))
        assertEquals(range("100", "200"), getDeposit(advert))
    }

    @Test
    fun getFormattedDeposit_shouldReturnDeposit_andSkipNotAvailableRooms_whenRentIsPerRoom() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("100", AdRoom.STATUS_AVAILABLE),
            createRoom("200", AdRoom.STATUS_TAKEN)))

        assertEquals("£100", getDeposit(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("100", AdRoom.STATUS_AVAILABLE),
            createRoom("100", AdRoom.STATUS_AVAILABLE),
            createRoom("200", AdRoom.STATUS_TAKEN)))

        assertEquals("£100", getDeposit(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("100", AdRoom.STATUS_AVAILABLE),
            createRoom("200", AdRoom.STATUS_AVAILABLE),
            createRoom("300", AdRoom.STATUS_TAKEN)))

        assertEquals(range("100", "200"), getDeposit(advert))
    }

    @Test
    fun getFormattedDeposit_shouldReturnDeposit_andSkipNotAvailableRooms_andRoomsWithoutDeposit_whenRentIsPerRoom() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("100", AdRoom.STATUS_AVAILABLE),
            createRoom("200", AdRoom.STATUS_TAKEN),
            createRoom("", AdRoom.STATUS_AVAILABLE)))

        assertEquals("£100", getDeposit(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("100", AdRoom.STATUS_AVAILABLE),
            createRoom("100", AdRoom.STATUS_AVAILABLE),
            createRoom("", AdRoom.STATUS_AVAILABLE),
            createRoom("200", AdRoom.STATUS_TAKEN)))
        assertEquals("£100", getDeposit(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(
            createRoom("100", AdRoom.STATUS_AVAILABLE),
            createRoom("200", AdRoom.STATUS_AVAILABLE),
            createRoom("300", AdRoom.STATUS_AVAILABLE),
            createRoom("", AdRoom.STATUS_AVAILABLE),
            createRoom("", AdRoom.STATUS_AVAILABLE),
            createRoom("400", AdRoom.STATUS_TAKEN)))

        assertEquals(range("100", "300"), getDeposit(advert))
    }

    @Test
    fun getFormattedDeposit_shouldReturnSingleDeposit_whenAdvertHasRoomsWithSameDeposit_andRentIsPerRoom() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("500"), createRoom("500")))
        assertEquals("£500", getDeposit(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("400"), createRoom("400")))
        assertEquals("£400", getDeposit(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("0"), createRoom("0")))
        assertEquals("£0", getDeposit(advert))
    }

    @Test
    fun getFormattedDeposit_shouldReturnDepositRange_whenAdvertHasRoomsWithDifferentDeposits_andRentIsPerRoom() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("500"), createRoom("600"), createRoom("400")))
        assertEquals(range("400", "600"), getDeposit(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("40"), createRoom("600"), createRoom("0")))
        assertEquals(range("0", "600"), getDeposit(advert))
    }

    @Test
    fun getFormattedDeposit_shouldReturnDeposit_withFraction_Removed() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("200"), createRoom("100.1"), createRoom("100.006")))
        assertEquals(range("100", "200"), getDeposit(advert))

        for (i in 1..9) {
            // 0.001, 0.002 etc will give 0
            advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("0.0"), createRoom("0.00$i")))
            assertEquals("£0", getDeposit(advert))
        }

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createRoom("0.1"), createRoom("0.01"), createRoom("0.0099")))
        assertEquals("£0", getDeposit(advert))
    }
    /* Newness (new, new today, boosted) */

    @Test
    fun getFormattedNewnessStatus_shouldReturnEmptyString_whenNewnessNotSpecified() {
        assertEquals("", advertUtils.getFormattedNewnessStatus(createOfferedAdvertWithRentPerRoom()))
        assertEquals("", advertUtils.getFormattedNewnessStatus(createWantedAdvert()))

        val advert = createWantedAdvert(newnessStatus = "")
        assertEquals("", advertUtils.getFormattedNewnessStatus(advert))

        val offeredAdvert = createOfferedAdvertWithRentPerRoom(newnessStatus = "")
        assertEquals("", advertUtils.getFormattedNewnessStatus(offeredAdvert))
    }

    @Test
    fun getFormattedNewnessStatus_shouldReturnEmptyString_whenNewnessNotRecognised() {
        val advertWanted = createWantedAdvert(newnessStatus = "NotProperNewnessStatusWanted")
        assertEquals("", advertUtils.getFormattedNewnessStatus(advertWanted))

        val advertOffered = createOfferedAdvertWithRentPerRoom(newnessStatus = "NotProperNewnessStatusOffered")
        assertEquals("", advertUtils.getFormattedNewnessStatus(advertOffered))
    }

    @Test
    fun getFormattedNewnessStatus_shouldReturnFormattedString_whenNewnessRecognised() {
        var advert: AbstractAd = createWantedAdvert(newnessStatus = AbstractAd.NEWNESS_STATUS_NEW)
        assertEquals(NEWNESS_NEW, advertUtils.getFormattedNewnessStatus(advert))

        advert = createOfferedAdvertWithRentPerRoom(newnessStatus = AbstractAd.NEWNESS_STATUS_NEW_TODAY)
        assertEquals(NEWNESS_NEW_TODAY, advertUtils.getFormattedNewnessStatus(advert))

        advert = createOfferedAdvertWithRentPerRoom(newnessStatus = AbstractAd.NEWNESS_STATUS_SUPER_RENEWED_TODAY)
        assertEquals(NEWNESS_BOOSTED, advertUtils.getFormattedNewnessStatus(advert))
    }

    /* Title (bedrooms) */

    @Test
    fun getFormattedOfferedAdvertTitle_shouldReturnEmptyString_whenWholeProperty_andNotStudio_andNoRooms() {
        var advert = createAdvertAsWholeProperty(AbstractAd.PROPERTY_TYPE_FLAT, roomsInProperty = "0")
        assertEquals("", advertUtils.getFormattedOfferedAdvertTitle(advert))

        advert = createAdvertAsWholeProperty(AbstractAd.PROPERTY_TYPE_FLAT, roomsInProperty = "-1")
        assertEquals("", advertUtils.getFormattedOfferedAdvertTitle(advert))

        advert = createAdvertAsWholeProperty(AbstractAd.PROPERTY_TYPE_HOUSE, roomsInProperty = "0")
        assertEquals("", advertUtils.getFormattedOfferedAdvertTitle(advert))

        advert = createAdvertAsWholeProperty(AbstractAd.PROPERTY_TYPE_HOUSE, roomsInProperty = "-1")
        assertEquals("", advertUtils.getFormattedOfferedAdvertTitle(advert))

        advert = createAdvertAsWholeProperty(AbstractAd.PROPERTY_TYPE_PROPERTY, roomsInProperty = "0")
        assertEquals("", advertUtils.getFormattedOfferedAdvertTitle(advert))

        advert = createAdvertAsWholeProperty(AbstractAd.PROPERTY_TYPE_PROPERTY, roomsInProperty = "-1")
        assertEquals("", advertUtils.getFormattedOfferedAdvertTitle(advert))
    }

    @Test
    fun getFormattedOfferedAdvertTitle_shouldReturnStudioFlat_whenWholePropertyIsStudioAndFlat() {
        val advert = createAdvertAsWholeProperty(AbstractAd.PROPERTY_TYPE_FLAT, accommodationType = AbstractAd.ACCOMMODATION_STUDIO)
        assertEquals("$STUDIO $FLAT", advertUtils.getFormattedOfferedAdvertTitle(advert))
    }

    @Test
    fun getFormattedOfferedAdvertTitle_shouldReturnStudioHouse_whenWholePropertyIsStudioAndHouse() {
        val advert = createAdvertAsWholeProperty(AbstractAd.PROPERTY_TYPE_HOUSE, accommodationType = AbstractAd.ACCOMMODATION_STUDIO)
        assertEquals("$STUDIO $HOUSE", advertUtils.getFormattedOfferedAdvertTitle(advert))
    }

    @Test
    fun getFormattedOfferedAdvertTitle_shouldReturnStudioProperty_whenWholePropertyIsStudioAndProperty() {
        val advert = createAdvertAsWholeProperty(AbstractAd.PROPERTY_TYPE_PROPERTY, accommodationType = AbstractAd.ACCOMMODATION_STUDIO)
        assertEquals("$STUDIO $PROPERTY", advertUtils.getFormattedOfferedAdvertTitle(advert))
    }

    @Test
    fun getFormattedOfferedAdvertTitle_shouldReturnFlatWithRooms_whenWholePropertyIsFlat() {
        var advert = createAdvertAsWholeProperty(AbstractAd.PROPERTY_TYPE_FLAT, roomsInProperty = "1")
        assertEquals(flat("1"), advertUtils.getFormattedOfferedAdvertTitle(advert))

        advert = createAdvertAsWholeProperty(AbstractAd.PROPERTY_TYPE_FLAT, roomsInProperty = "2")
        assertEquals(flat("2"), advertUtils.getFormattedOfferedAdvertTitle(advert))
    }

    @Test
    fun getFormattedOfferedAdvertTitle_shouldReturnHouseWithRooms_whenWholePropertyIsHouse() {
        var advert = createAdvertAsWholeProperty(AbstractAd.PROPERTY_TYPE_HOUSE, roomsInProperty = "1")
        assertEquals(house("1"), advertUtils.getFormattedOfferedAdvertTitle(advert))

        advert = createAdvertAsWholeProperty(AbstractAd.PROPERTY_TYPE_HOUSE, roomsInProperty = "2")
        assertEquals(house("2"), advertUtils.getFormattedOfferedAdvertTitle(advert))
    }

    @Test
    fun getFormattedOfferedAdvertTitle_shouldReturnPropertyWithRooms_whenWholePropertyIsProperty() {
        var advert = createAdvertAsWholeProperty(AbstractAd.PROPERTY_TYPE_PROPERTY, roomsInProperty = "1")
        assertEquals(property("1"), advertUtils.getFormattedOfferedAdvertTitle(advert))

        advert = createAdvertAsWholeProperty(AbstractAd.PROPERTY_TYPE_PROPERTY, roomsInProperty = "2")
        assertEquals(property("2"), advertUtils.getFormattedOfferedAdvertTitle(advert))
    }

    @Test
    fun getFormattedOfferedAdvertTitle_shouldReturnEmptyString_whenRoomBasedProperty_andHasNoRooms() {
        assertEquals("", advertUtils.getFormattedOfferedAdvertTitle(createOfferedAdvertWithRentPerRoom(rooms = ArrayList())))
    }

    @Test
    fun getFormattedOfferedAdvertTitle_shouldReturnSingleRoom_whenRoomBasedProperty_andOnlySingleRoomAvailable() {
        assertEquals("$SINGLE $BEDROOM", advertUtils.getFormattedOfferedAdvertTitle(createOfferedAdvertWithRentPerRoom(
            rooms = asList(createSingleRoom()))))
    }

    @Test
    fun getFormattedOfferedAdvertTitle_shouldReturnDoubleRoom_whenRoomBasedProperty_andOnlyDoubleRoomAvailable() {
        assertEquals("$DOUBLE $BEDROOM", advertUtils.getFormattedOfferedAdvertTitle(createOfferedAdvertWithRentPerRoom(
            rooms = asList(createDoubleRoom()))))
    }

    @Test
    fun getFormattedOfferedAdvertTitle_shouldReturnEnSuite_whenRoomBasedProperty_andEnsuiteAvailable() {
        assertEquals("$SINGLE $BEDROOM $WITH_ENSUITE", advertUtils.getFormattedOfferedAdvertTitle(createOfferedAdvertWithRentPerRoom(
            rooms = asList(createSingleRoom(true)))))
        assertEquals("$DOUBLE $BEDROOM $WITH_ENSUITE", advertUtils.getFormattedOfferedAdvertTitle(createOfferedAdvertWithRentPerRoom(
            rooms = asList(createDoubleRoom(true)))))
    }

    @Test
    fun getFormattedOfferedAdvertTitle_shouldReturnNumberOfBedrooms_whenRoomBasedProperty_andHasMoreThanOneRoom() {
        var advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createSingleRoom(true), createSingleRoom(), createDoubleRoom()))
        assertEquals("2 $SINGLE_LOWER_CASE & 1 $DOUBLE_LOWER_CASE $BEDROOMS", advertUtils.getFormattedOfferedAdvertTitle(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createSingleRoom(true), createSingleRoom()))
        assertEquals("2 $SINGLE_LOWER_CASE $BEDROOMS", advertUtils.getFormattedOfferedAdvertTitle(advert))

        advert = createOfferedAdvertWithRentPerRoom(rooms = asList(createDoubleRoom(true), createDoubleRoom()))
        assertEquals("2 $DOUBLE_LOWER_CASE $BEDROOMS", advertUtils.getFormattedOfferedAdvertTitle(advert))
    }

    /* Location (offered advert) */

    @Test
    fun getFormattedLocationForOfferedAdvert_shouldReturnEmptyString_whenLocationNotSpecified() {
        val advert = createOfferedAdvertWithRentPerRoom()
        val advertUtilsSpy = spy(advertUtils)

        assertEquals("", advertUtilsSpy.getFormattedLocationForOfferedAdvert(advert))

        doReturn("").`when`(advertUtilsSpy).getFormattedLocation(any(), any())
        assertEquals("", advertUtilsSpy.getFormattedLocationForOfferedAdvert(advert))
    }

    @Test
    fun getFormattedLocationForOfferedAdvert_shouldReturnProperlyFormattedString_whenLocationIsSpecified() {
        val advert = createOfferedAdvertWithRentPerRoom()
        val advertUtilsSpy = spy(advertUtils)

        doReturn("Manchester (M3)").`when`(advertUtilsSpy).getFormattedLocation(any(), any())
        assertEquals("$IN Manchester (M3)", advertUtilsSpy.getFormattedLocationForOfferedAdvert(advert))

        doReturn("London (L21)").`when`(advertUtilsSpy).getFormattedLocation(any(), any())
        assertEquals("$IN London (L21)", advertUtilsSpy.getFormattedLocationForOfferedAdvert(advert))
    }

    /* Location (wanted advert) */

    @Test
    fun getFormattedLocationForWantedAdvert_shouldReturnEmptyString_whenNoAreas() {
        var advert = createWantedAdvert(numberOfAreas = "")
        assertEquals("", advertUtils.getFormattedLocationForWantedAdvert(advert))

        advert = createWantedAdvert(searchAreas = "")
        assertEquals("", advertUtils.getFormattedLocationForWantedAdvert(advert))

        advert = createWantedAdvert(numberOfAreas = "", searchAreas = "")
        assertEquals("", advertUtils.getFormattedLocationForWantedAdvert(advert))

        advert = createWantedAdvert(numberOfAreas = "1", searchAreas = "")
        assertEquals("", advertUtils.getFormattedLocationForWantedAdvert(advert))
    }

    @Test
    fun getFormattedLocationForWantedAdvert_shouldReturnEmptyString_whenNumberOfAreasIsLessThanOneOrNotNumber() {
        var advert = createWantedAdvert(numberOfAreas = "0")
        assertEquals("", advertUtils.getFormattedLocationForWantedAdvert(advert))

        advert = createWantedAdvert(numberOfAreas = "-1")
        assertEquals("", advertUtils.getFormattedLocationForWantedAdvert(advert))

        advert = createWantedAdvert(numberOfAreas = "not a number")
        assertEquals("", advertUtils.getFormattedLocationForWantedAdvert(advert))
    }

    @Test
    fun getFormattedLocationForWantedAdvert_shouldReturnProperlyFormattedString_whenProperNumberAndAreasProvided() {
        var advert = createWantedAdvert(numberOfAreas = "1", searchAreas = "Salford")
        assertEquals("$IN Salford", advertUtils.getFormattedLocationForWantedAdvert(advert))

        advert = createWantedAdvert(numberOfAreas = "1", searchAreas = "Salford & Something")
        assertEquals("$IN Salford & Something", advertUtils.getFormattedLocationForWantedAdvert(advert))

        advert = createWantedAdvert(numberOfAreas = "1", searchAreas = "Salford & Something, Something else")
        assertEquals("$IN Salford & Something, Something else", advertUtils.getFormattedLocationForWantedAdvert(advert))

        advert = createWantedAdvert(numberOfAreas = "2", searchAreas = "Salford, Trafford")
        assertEquals("$IN 2 $AREAS $INCLUDING Salford", advertUtils.getFormattedLocationForWantedAdvert(advert))

        advert = createWantedAdvert(numberOfAreas = "3", searchAreas = "Salford, Trafford, SomeOtherCity")
        assertEquals("$IN 3 $AREAS $INCLUDING Salford", advertUtils.getFormattedLocationForWantedAdvert(advert))

        advert =
            createWantedAdvert(numberOfAreas = "7", searchAreas = "", areasLookingIn = asList(Area(" Salford Main ", "Man"), Area("Trafford", "Man")))
        assertEquals("$IN 7 $AREAS $INCLUDING Salford Main", advertUtils.getFormattedLocationForWantedAdvert(advert))

        advert = createWantedAdvert(numberOfAreas = "1", searchAreas = "", areasLookingIn = asList(Area("Salford Main", "Man")))
        assertEquals("$IN Salford Main", advertUtils.getFormattedLocationForWantedAdvert(advert))
    }

    /* Title (type of room) */

    @Test
    fun getFormattedWantedAdvertTitle_shouldReturnSingleOrDoubleBedroom_whenRoomSizeIsSingleOrDoubleBedroom() {
        val advert = createWantedAdvert(roomSize = SINGLE_OR_DOUBLE_ROOM)
        assertEquals("$SEARCHING_FOR $SINGLE_LOWER_CASE $OR $DOUBLE_LOWER_CASE $BEDROOM", advertUtils.getFormattedWantedAdvertTitle(advert))
    }

    @Test
    fun getFormattedWantedAdvertTitle_shouldReturnSingleOrDoubleBedroom_whenRoomSizeIsNotDefined() {
        val advert = createWantedAdvert(roomSize = "")
        assertEquals("$SEARCHING_FOR $SINGLE_LOWER_CASE $OR $DOUBLE_LOWER_CASE $BEDROOM", advertUtils.getFormattedWantedAdvertTitle(advert))
    }

    @Test
    fun getFormattedWantedAdvertTitle_shouldReturnSingleOrDoubleBedroom_whenRoomSizeIsUnknown() {
        var advert = createWantedAdvert(roomSize = "someNewType")
        assertEquals("$SEARCHING_FOR $SINGLE_LOWER_CASE $OR $DOUBLE_LOWER_CASE $BEDROOM", advertUtils.getFormattedWantedAdvertTitle(advert))

        advert = createWantedAdvert(roomSize = " and yet another type")
        assertEquals("$SEARCHING_FOR $SINGLE_LOWER_CASE $OR $DOUBLE_LOWER_CASE $BEDROOM", advertUtils.getFormattedWantedAdvertTitle(advert))
    }

    @Test
    fun getFormattedWantedAdvertTitle_shouldReturnDoubleBedroom_whenRoomSizeStartsWithDoubleRoom() {
        var advert = createWantedAdvert(roomSize = DOUBLE_ROOM)
        assertEquals("$SEARCHING_FOR $DOUBLE_LOWER_CASE $BEDROOM", advertUtils.getFormattedWantedAdvertTitle(advert))

        advert = createWantedAdvert(roomSize = "$DOUBLE_ROOM (we are partners)")
        assertEquals("$SEARCHING_FOR $DOUBLE_LOWER_CASE $BEDROOM", advertUtils.getFormattedWantedAdvertTitle(advert))

        advert = createWantedAdvert(roomSize = "$DOUBLE_ROOM some extra text")
        assertEquals("$SEARCHING_FOR $DOUBLE_LOWER_CASE $BEDROOM", advertUtils.getFormattedWantedAdvertTitle(advert))
    }

    @Test
    fun getFormattedWantedAdvertTitle_shouldReturnTwoBedrooms_whenRoomSizeIsTwoRooms() {
        val advert = createWantedAdvert(roomSize = TWO_ROOMS)
        assertEquals("$SEARCHING_FOR 2 $BEDROOMS", advertUtils.getFormattedWantedAdvertTitle(advert))
    }

    @Test
    fun getFormattedWantedAdvertTitle_shouldReturnTwoBedroomsOrTwin_whenRoomSizeIsTwoRoomsOrTwin() {
        val advert = createWantedAdvert(roomSize = TWIN_ROOM_OR_2_ROOMS)
        assertEquals("$SEARCHING_FOR 2 $BEDROOMS $OR $TWIN", advertUtils.getFormattedWantedAdvertTitle(advert))
    }

    @Test
    fun getFormattedWantedAdvertTitle_shouldReturnTwoDoubleBedrooms_whenRoomSizeIsTwoDoubleRooms() {
        val advert = createWantedAdvert(roomSize = TWO_DOUBLE_ROOMS)
        assertEquals("$SEARCHING_FOR 2 $DOUBLE_LOWER_CASE $BEDROOMS", advertUtils.getFormattedWantedAdvertTitle(advert))
    }

    @Test
    fun getFormattedWantedAdvertTitle_shouldReturnTwoSingleBedrooms_whenRoomSizeIsTwoSingleRooms() {
        val advert = createWantedAdvert(roomSize = TWO_SINGLE_ROOMS)
        assertEquals("$SEARCHING_FOR 2 $SINGLE_LOWER_CASE $BEDROOMS", advertUtils.getFormattedWantedAdvertTitle(advert))
    }

    @Test
    fun getFormattedWantedAdvertTitle_shouldReturnWholeProperty_whenRoomSizeIsFlatOrHouseToRent() {
        val advert = createWantedAdvert(roomSize = FLAT_OR_HOUSE_TO_RENT)
        assertEquals("$SEARCHING_FOR $WHOLE $PROPERTY", advertUtils.getFormattedWantedAdvertTitle(advert))
    }

    @Test
    fun getFormattedOtherUserAdvertsHeader_shouldReturnAdvertsString_whenUsernameIsEmpty() {
        assertEquals(ADVERTS, advertUtils.getFormattedOtherUserAdvertsHeader(""))
    }

    @Test
    fun getFormattedOtherUserAdvertsHeader_shouldReturnStringWithApostrophe_whenUsernameEndsWith_S() {
        assertEquals("James' ${ADVERTS.toLowerCase(defaultLocale)}", advertUtils.getFormattedOtherUserAdvertsHeader("James"))
        assertEquals("Jamesss' ${ADVERTS.toLowerCase(defaultLocale)}", advertUtils.getFormattedOtherUserAdvertsHeader("Jamesss"))
    }

    @Test
    fun getFormattedOtherUserAdvertsHeader_shouldReturnStringWithApostropheWithS_whenUsernameDoesNotEndWith_S() {
        assertEquals("John's ${ADVERTS.toLowerCase(defaultLocale)}", advertUtils.getFormattedOtherUserAdvertsHeader("John"))
        assertEquals("Johny's ${ADVERTS.toLowerCase(defaultLocale)}", advertUtils.getFormattedOtherUserAdvertsHeader("Johny"))
    }

    @Test
    fun getFormattedOtherUserAdvertsHeader_shouldReturnStringWithCapitalisedName_whenUsernameIsLowerCase() {
        assertEquals("John's ${ADVERTS.toLowerCase(defaultLocale)}", advertUtils.getFormattedOtherUserAdvertsHeader("john"))
        assertEquals("JoHn's ${ADVERTS.toLowerCase(defaultLocale)}", advertUtils.getFormattedOtherUserAdvertsHeader("joHn"))
    }

    @Test
    fun fromPwToPcmRent_shouldRoundItUp() {
        assertEquals(0, advertUtils.fromPwToPcmRent(0))
        assertEquals(5, advertUtils.fromPwToPcmRent(1))
        assertEquals(44, advertUtils.fromPwToPcmRent(10))
        assertEquals(434, advertUtils.fromPwToPcmRent(100))

        assertEquals(477, advertUtils.fromPwToPcmRent(110))
        assertEquals(481, advertUtils.fromPwToPcmRent(111))
        assertEquals(486, advertUtils.fromPwToPcmRent(112))
    }

    @Test
    fun getFormattedAdvertiserType_shouldReturnProperlyFormattedString() {
        assertEquals("", advertUtils.getFormattedAdvertiserType(""))
        assertEquals("", advertUtils.getFormattedAdvertiserType("unknown type"))
        assertEquals(string(R.string.agent), advertUtils.getFormattedAdvertiserType(AdvertiserType.AGENT))
        assertEquals(string(R.string.live_in_landlord), advertUtils.getFormattedAdvertiserType(AdvertiserType.LIVE_IN_LANDLORD))
        assertEquals(string(R.string.live_out_landlord), advertUtils.getFormattedAdvertiserType(AdvertiserType.LIVE_OUT_LANDLORD))
        assertEquals(string(R.string.current_flatmate), advertUtils.getFormattedAdvertiserType(AdvertiserType.CURRENT_FLATMATE))
        assertEquals(string(R.string.former_flatmate), advertUtils.getFormattedAdvertiserType(AdvertiserType.FORMER_FLATMATE))
        assertEquals(string(R.string.current_flatmate), advertUtils.getFormattedAdvertiserType(AdvertiserType.CURRENT_TENANTS))
    }

    @Test
    fun getFormattedProfession_shouldReturnEmptyString_andDisregardOtherArguments_whenNoSeekers() {
        assertEquals("", advertUtils.getFormattedProfession("", 0, false))
        assertEquals("", advertUtils.getFormattedProfession(AbstractAd.PROFESSION_STUDENT, -1, false))
        assertEquals("", advertUtils.getFormattedProfession(AbstractAd.PROFESSION_STUDENT, -1, true))
        assertEquals("", advertUtils.getFormattedProfession(AbstractAd.PROFESSION_PROFESSIONAL, 0, true))
        assertEquals("", advertUtils.getFormattedProfession(AbstractAd.PROFESSION_PROFESSIONAL, 0, false))
    }

    @Test
    fun getFormattedProfession_shouldReturnEmptyString_whenOneSeeker_andProfessionUnspecified() {
        assertEquals("", advertUtils.getFormattedProfession("", 1, false))
        assertEquals("", advertUtils.getFormattedProfession("unknown profession", 1, false))
    }

    @Test
    fun getFormattedProfession_shouldReturnOnlyProfession_whenOneSeeker_andProfessionSpecified() {
        assertEquals(string(R.string.professional), advertUtils.getFormattedProfession(AbstractAd.PROFESSION_PROFESSIONAL, 1, false))
        assertEquals(string(R.string.student), advertUtils.getFormattedProfession(AbstractAd.PROFESSION_STUDENT, 1, true))
    }

    @Test
    fun getFormattedProfession_shouldReturnOnlyGroupType_whenMoreThanOneSeeker_andProfessionUnSpecified() {
        assertEquals(string(R.string.couple).capitalize(), advertUtils.getFormattedProfession("", 2, true))
        assertEquals(string(R.string.friends).capitalize(), advertUtils.getFormattedProfession("", 3, false))
        assertEquals(string(R.string.friends).capitalize(), advertUtils.getFormattedProfession("unknown profession", 3, false))
    }

    @Test
    fun getFormattedProfession_shouldReturnProfessionAndRelationship_whenMoreThanOneSeeker_andProfessionSpecified() {
        assertEquals("${string(R.string.professional)} ${string(R.string.couple)}", advertUtils.getFormattedProfession(AbstractAd.PROFESSION_PROFESSIONAL, 2, true))
        assertEquals("${string(R.string.professional)} ${string(R.string.friends)}", advertUtils.getFormattedProfession(AbstractAd.PROFESSION_PROFESSIONAL, 2, false))
        assertEquals("${string(R.string.student)} ${string(R.string.couple)}", advertUtils.getFormattedProfession(AbstractAd.PROFESSION_STUDENT, 3, true))
        assertEquals("${string(R.string.student)} ${string(R.string.friends)}", advertUtils.getFormattedProfession(AbstractAd.PROFESSION_STUDENT, 3, false))
    }

    @Test
    fun getFormattedCompanyName_shouldReturnEmptyString_whenCompanyNameAndFullName_areNotLettersAndDigits() {
        assertEquals("", getFormattedCompanyName("", ""))
        assertEquals("", getFormattedCompanyName("-", "+"))
        assertEquals("", getFormattedCompanyName(" ", " "))
        assertEquals("", getFormattedCompanyName(" ", " "))
        assertEquals("", getFormattedCompanyName(".", "."))
        assertEquals("", getFormattedCompanyName("${TestConstants.SMILEY_EMOTICON}", "${TestConstants.SMILEY_EMOTICON}"))
    }

    @Test
    fun getFormattedCompanyName_shouldReturnCompanyName_andDisregardFullName_whenCompanyNameSpecified() {
        assertEquals("Company Name", getFormattedCompanyName(companyName = "Company Name", fullName = ""))
        assertEquals("Company Name", getFormattedCompanyName(companyName = "Company Name", fullName = "Full Name"))
        assertEquals("Company Name", getFormattedCompanyName(companyName = "Company Name "))
        assertEquals("Company Name", getFormattedCompanyName(companyName = " Company Name"))
        assertEquals("Company Name", getFormattedCompanyName(companyName = "company name "))
        assertEquals("Company Name", getFormattedCompanyName(companyName = "COMPANY NAME"))
        assertEquals("Company Name88", getFormattedCompanyName(companyName = "Company Name88"))
        assertEquals("Company  Name88", getFormattedCompanyName(companyName = "Company  Name88 "))
        assertEquals("Company Name", getFormattedCompanyName(companyName = "Company Name+-"))
    }

    @Test
    fun getFormattedCompanyName_shouldReturnFullName_whenCompanyNameUnSpecified() {
        assertEquals("Full Name", getFormattedCompanyName(companyName = "", fullName = "Full Name"))
        assertEquals("Full Name", getFormattedCompanyName(fullName = "Full Name "))
        assertEquals("Full Name", getFormattedCompanyName(fullName = " Full Name "))
        assertEquals("Full Name", getFormattedCompanyName(fullName = "full name "))
        assertEquals("Full Name", getFormattedCompanyName(fullName = "FULL NAME"))
        assertEquals("Full Name88", getFormattedCompanyName(fullName = "Full Name88"))
        assertEquals("Company  Name88", getFormattedCompanyName(fullName = "Company  Name88 "))
        assertEquals("Full Name", getFormattedCompanyName(fullName = "Full Name+-"))
        assertNotEquals("Full Name", getFormattedCompanyName(companyName = "Company Name", fullName = "Full Name+-"))
    }

    @Test
    fun getFormattedExtraSeekersCount_shouldReturnProperlyFormattedString() {
        assertEquals("", advertUtils.getFormattedExtraSeekersCount(OfferedAdvertiser("", "", "", "", "", false)))
        assertEquals("", getFormattedExtraSeekersCount(-1))
        assertEquals("", getFormattedExtraSeekersCount(0))
        assertEquals("", getFormattedExtraSeekersCount(1))
        assertEquals("+1", getFormattedExtraSeekersCount(2))
        assertEquals("+2", getFormattedExtraSeekersCount(3))
        assertEquals("+100", getFormattedExtraSeekersCount(101))
    }

    private fun getFormattedExtraSeekersCount(seekersCount: Int) =
        advertUtils.getFormattedExtraSeekersCount(WantedAdvertiser("", "", "", false, seekersCount))

    private fun getFormattedCompanyName(companyName: String = "", fullName: String = "") =
        advertUtils.getFormattedCompanyName(companyName, fullName)

    private fun getDeposit(adOffered: AdOffered): String {
        return advertUtils.getFormattedDeposit(adOffered)
    }

    private fun getMonthlyPriceWithBills(adOffered: AdOffered): String {
        return advertUtils.getFormattedMonthlyPrice(adOffered, true)
    }

    private fun getMonthlyPriceWithOutBills(adOffered: AdOffered): String {
        return advertUtils.getFormattedMonthlyPrice(adOffered, false)
    }

    private fun flat(rooms: String): String {
        return String.format("%s %s %s", rooms, BEDROOM, FLAT)
    }

    private fun house(rooms: String): String {
        return String.format("%s %s %s", rooms, BEDROOM, HOUSE)
    }

    private fun property(rooms: String): String {
        return String.format("%s %s %s", rooms, BEDROOM, PROPERTY)
    }

    private fun string(@StringRes stringResId: Int) = context.getString(stringResId)

    private fun createWantedAdvert(price: String = "", period: String = "", roomSize: String = "", numberOfAreas: String = "",
                                   searchAreas: String = "", newnessStatus: String = "",
                                   areasLookingIn: List<Area> = emptyList()): AdWanted {

        val advert = spy(AdWanted())
        doReturn(price).`when`(advert).combinedBudget
        doReturn(period).`when`(advert).pricePeriod
        doReturn(roomSize).`when`(advert).roomSize
        doReturn(numberOfAreas.toIntOrNull() ?: 0).`when`(advert).numberOfAreas
        doReturn(searchAreas).`when`(advert).searchAreas
        doReturn(areasLookingIn).`when`(advert).areasLookingIn
        doReturn(newnessStatus).`when`(advert).newnessStatus

        return advert
    }

    private fun createOfferedAdvertWithRentPerRoom(neighbourHood: String = "", postCode: String = "", billsIncluded: String = "",
                                                   rooms: List<AdRoom> = emptyList(), newnessStatus: String = ""): AdOffered {
        val advert = spy(AdOffered())
        doReturn(postCode).`when`(advert).postcode
        doReturn(neighbourHood).`when`(advert).area
        doReturn(billsIncluded).`when`(advert).billsIncluded
        doReturn(rooms).`when`(advert).rooms
        doReturn(AbstractAd.RENT_OPTION_ROOM).`when`(advert).rentOption
        doReturn(newnessStatus).`when`(advert).newnessStatus

        return advert
    }

    private fun createRoom(price: String, period: String, status: String): AdRoom {
        val room = spy(AdRoom())
        doReturn(price).`when`(room).price
        doReturn(period).`when`(room).pricePeriod
        doReturn(status).`when`(room).status

        return room
    }

    private fun createSingleRoom(withEnsuite: Boolean = false): AdRoom {
        val room = spy(AdRoom())
        doReturn(AdRoom.STATUS_AVAILABLE).`when`(room).status
        doReturn(AdRoom.ROOM_TYPE_SINGLE).`when`(room).type
        doReturn(withEnsuite).`when`(room).hasEnsuite()

        return room
    }

    private fun createDoubleRoom(withEnsuite: Boolean = false): AdRoom {
        val room = spy(AdRoom())
        doReturn(AdRoom.STATUS_AVAILABLE).`when`(room).status
        doReturn(AdRoom.ROOM_TYPE_DOUBLE).`when`(room).type
        doReturn(withEnsuite).`when`(room).hasEnsuite()

        return room
    }

    private fun createRoom(deposit: String, status: String = AdRoom.STATUS_AVAILABLE): AdRoom {
        val room = spy(AdRoom())
        doReturn(deposit).`when`(room).securityDeposit
        doReturn(status).`when`(room).status

        return room
    }

    private fun createAdvertAsWholeProperty(propertyType: String = "", rooms: List<AdRoom> = emptyList(), roomsInProperty: String = "",
                                            accommodationType: String = "", minRent: String = "", pricePeriod: String = "",
                                            securityDeposit: String = ""): AdOffered {
        val advert = spy(AdOffered())
        doReturn(rooms).`when`(advert).rooms
        doReturn(true).`when`(advert).isWholePropertyForRent()
        doReturn(roomsInProperty.toIntOrNull() ?: 0).`when`(advert).roomsInProperty
        doReturn(accommodationType).`when`(advert).accommodationType
        doReturn(pricePeriod).`when`(advert).pricePeriod
        doReturn(minRent).`when`(advert).minRent
        doReturn(securityDeposit).`when`(advert).securityDeposit
        doReturn(AbstractAd.ACCOMMODATION_STUDIO.equals(accommodationType, true)).`when`(advert).isStudio()

        when (propertyType) {
            AbstractAd.PROPERTY_TYPE_FLAT -> doReturn(true).`when`(advert).isFlat()
            AbstractAd.PROPERTY_TYPE_HOUSE -> doReturn(true).`when`(advert).isHouse()
            AbstractAd.PROPERTY_TYPE_PROPERTY -> doReturn(true).`when`(advert).isProperty()
        }

        return advert
    }

    private fun createAdvertWithRentPerRoom(rooms: List<AdRoom>): AdOffered {
        val advert = spy(AdOffered())
        doReturn(rooms).`when`(advert).rooms
        doReturn(AbstractAd.RENT_OPTION_ROOM).`when`(advert).rentOption

        return advert
    }

    private fun createAdvertWithRentPerEither(rooms: List<AdRoom>): AdOffered {
        val advert = spy(AdOffered())
        doReturn(rooms).`when`(advert).rooms
        doReturn(AbstractAd.RENT_OPTION_EITHER).`when`(advert).rentOption

        return advert
    }

    private fun range(min: String, max: String): String {
        return "$CURRENCY_SYMBOL$min$EN_DASH£$max"
    }

    private fun pwToPcm(pricePerWeek: Int): Int {
        return ceil(pricePerWeek * (52f / 12f)).toInt()
    }

}
