package com.spareroom.ui.filters.provider;

import com.spareroom.TestApplication;
import com.spareroom.TestConstants;
import com.spareroom.model.business.SearchAdvertListProperties.*;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted;
import com.spareroom.ui.filters.adapter.viewparams.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static com.spareroom.ui.filters.provider.AbstractItemProvider.LENGTH_OF_STAY_MAX;
import static com.spareroom.ui.filters.provider.AbstractItemProvider.LENGTH_OF_STAY_MIN;
import static com.spareroom.ui.filters.provider.FilterWantedAvailabilityItemProvider.*;
import static com.spareroom.ui.filters.provider.ItemProviderTestUtil.*;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class FilterWantedAvailabilityItemProviderTest {

    private FilterWantedAvailabilityItemProvider provider;
    private final ItemProviderTestUtil itemProviderTestUtil = new ItemProviderTestUtil();

    @Before
    public void setUp() {
        provider = new FilterWantedAvailabilityItemProvider();
    }

    @Test
    public void get_whenRetrievingList_thenAllItemsAdded() {
        provider.createNewList(new SearchAdvertListPropertiesWanted(), null);
        assertEquals(13, provider.get().size());
    }

    @Test
    public void createNewList_whenEmptyPropertyList_thenDefaultFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted(), null);
        final List<FilterItem> list = provider.get();

        assertNull(((SummaryViewDateParams) itemProviderTestUtil.getParameter(list, MOVE_IN)).date());
        assertEquals(MinTerm.NOT_SET, ((SummaryViewMinTermParams) itemProviderTestUtil.getParameter(list, LENGTH_OF_STAY_MIN)).minTerm());
        assertEquals(MaxTerm.NOT_SET, ((SummaryViewMaxTermParams) itemProviderTestUtil.getParameter(list, LENGTH_OF_STAY_MAX)).maxTerm());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, ALL_WEEK_RENTERS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, WEEKDAY_RENTERS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, WEEKEND_RENTERS)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, NO_PREFERENCE)).selected());
    }

    @Test
    public void createNewList_whenFullPropertyList_thenFilledFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted(), null);
        final List<FilterItem> list = provider.get();

        assertEquals(DEFAULT_AVAILABLE_FROM_VALUE, ((SummaryViewDateParams) itemProviderTestUtil.getParameter(list, MOVE_IN)).date());
        assertEquals(DEFAULT_MIN_TERM_VALUE, ((SummaryViewMinTermParams) itemProviderTestUtil.getParameter(list, LENGTH_OF_STAY_MIN)).minTerm());
        assertEquals(DEFAULT_MAX_TERM_VALUE, ((SummaryViewMaxTermParams) itemProviderTestUtil.getParameter(list, LENGTH_OF_STAY_MAX)).maxTerm());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, ALL_WEEK_RENTERS)).selected());
        assertTrue(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, WEEKDAY_RENTERS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, WEEKEND_RENTERS)).selected());
        assertFalse(((GroupedCompoundButtonParams) itemProviderTestUtil.getParameter(list, NO_PREFERENCE)).selected());
    }

    @Test
    public void saveProperties_whenEmptyList_andEmptyProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted(), null);
        testEmptyList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenEmptyList_andFullProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted(), null);
        testEmptyList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenFullList_andEmptyProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted(), null);
        testFullList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenFullList_andFullProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted(), null);
        testFullList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenSortFlagIsFalse_thenShouldNotSortTerm() {
        SearchAdvertListPropertiesWanted termProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted();
        setReversedMinMaxTerm(termProperties);
        provider.updateProperties(termProperties, false);

        assertEquals(MinTerm.EIGHTEEN, termProperties.getMinTerm());
        assertEquals(MaxTerm.ONE, termProperties.getMaxTerm());
    }

    @Test
    public void saveProperties_whenSortFlagIsTrue_thenShouldSortTerm() {
        SearchAdvertListPropertiesWanted termProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted();
        setReversedMinMaxTerm(termProperties);
        provider.updateProperties(termProperties, true);

        assertEquals(MinTerm.ONE, termProperties.getMinTerm());
        assertEquals(MaxTerm.EIGHTEEN, termProperties.getMaxTerm());
    }

    private void testFullList(final SearchAdvertListPropertiesWanted properties) {
        provider.updateProperties(properties, true);

        assertEquals(DEFAULT_AVAILABLE_FROM_VALUE, properties.getAvailableFrom());
        assertEquals(DEFAULT_MIN_TERM_VALUE, properties.getMinTerm());
        assertEquals(DEFAULT_MAX_TERM_VALUE, properties.getMaxTerm());
        assertEquals(DEFAULT_DAYS_OF_WEEK_AVAILABLE_VALUE, properties.getDaysOfWeekAvailable());
    }

    private void testEmptyList(final SearchAdvertListPropertiesWanted properties) {
        provider.updateProperties(properties, true);

        assertNull(properties.getAvailableFrom());
        assertEquals(MinTerm.NOT_SET, properties.getMinTerm());
        assertEquals(MaxTerm.NOT_SET, properties.getMaxTerm());
        assertEquals(DaysOfWeek.NOT_SET, properties.getDaysOfWeekAvailable());
    }

    private void setReversedMinMaxTerm(SearchAdvertListPropertiesWanted properties) {
        properties.setMinTerm(MinTerm.EIGHTEEN);
        properties.setMaxTerm(MaxTerm.ONE);
        provider.createNewList(properties, null);
    }

}
