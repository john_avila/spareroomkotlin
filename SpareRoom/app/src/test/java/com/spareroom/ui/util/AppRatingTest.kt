package com.spareroom.ui.util

import com.spareroom.TestApplication
import com.spareroom.TestConstants
import com.spareroom.TestDateUtils.date
import com.spareroom.TestDateUtils.moveCalendarBack
import com.spareroom.integration.sharedpreferences.EventsDao
import com.spareroom.integration.sharedpreferences.UserEvents
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.util.*

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [(TestConstants.CONFIG_SDK)], application = TestApplication::class)
class AppRatingTest {

    private val appRating = AppRating(mock(EventsDao::class.java), mock(ConnectivityChecker::class.java))

    // initial rating popup

    @Test
    fun shouldShowInitialRatingPopup_shouldReturnFalse_whenInitialPopupAlreadyShown() {
        assertTrue(shouldShowInitialRatingPopup())

        assertFalse(shouldShowInitialRatingPopup(initialRatingShownDate = date()))
    }

    @Test
    fun shouldShowInitialRatingPopup_shouldReturnFalse_whenDaysOfUsageLessThanTwo() {
        assertTrue(shouldShowInitialRatingPopup())

        assertFalse(shouldShowInitialRatingPopup(daysOfUsage = 1))
        assertFalse(shouldShowInitialRatingPopup(daysOfUsage = 0))
        assertFalse(shouldShowInitialRatingPopup(daysOfUsage = -1))
    }

    @Test
    fun shouldShowInitialRatingPopup_shouldReturnFalse_whenLessThanOneSearch() {
        assertTrue(shouldShowInitialRatingPopup())

        assertFalse(shouldShowInitialRatingPopup(search = 0))
    }

    @Test
    fun shouldShowInitialRatingPopup_shouldReturnFalse_whenLessThanOneSavedAdvertAndMessage() {
        assertTrue(shouldShowInitialRatingPopup())

        assertFalse(shouldShowInitialRatingPopup(saveAdvert = 0, messageAdvert = 0))
        assertTrue(shouldShowInitialRatingPopup(saveAdvert = 1, messageAdvert = 0))
        assertTrue(shouldShowInitialRatingPopup(saveAdvert = 0, messageAdvert = 1))
    }

    @Test
    fun shouldShowInitialRatingPopup_shouldReturnFalse_whenTotalEventsCountLessThanFifteen() {
        assertTrue(shouldShowInitialRatingPopup())

        assertFalse(shouldShowInitialRatingPopup(search = 0, saveAdvert = 0, messageAdvert = 0))
        assertFalse(shouldShowInitialRatingPopup(search = 12, saveAdvert = 1, messageAdvert = 0))
        assertFalse(shouldShowInitialRatingPopup(search = 12, saveAdvert = 1, messageAdvert = 1))
        assertFalse(shouldShowInitialRatingPopup(search = 12, saveAdvert = 0, messageAdvert = 1))
    }

    @Test
    fun shouldShowInitialRatingPopup_shouldReturnTrue_whenRequirementsMet() {
        assertFalse(appRating.shouldShowInitialRatingPopup(userEvents()))

        var userEvents = userEvents(searchCount = 14, saveAdvertCount = 1, messageAdvertCount = 0, daysOfUsage = 2)
        assertTrue(appRating.shouldShowInitialRatingPopup(userEvents))

        userEvents = userEvents(searchCount = 14, saveAdvertCount = 0, messageAdvertCount = 1, daysOfUsage = 2)
        assertTrue(appRating.shouldShowInitialRatingPopup(userEvents))

        userEvents = userEvents(searchCount = 1, saveAdvertCount = 0, messageAdvertCount = 100, daysOfUsage = 2)
        assertTrue(appRating.shouldShowInitialRatingPopup(userEvents))

        userEvents = userEvents(searchCount = 1, saveAdvertCount = 100, messageAdvertCount = 0, daysOfUsage = 2)
        assertTrue(appRating.shouldShowInitialRatingPopup(userEvents))

        userEvents = userEvents(searchCount = 100, saveAdvertCount = 100, messageAdvertCount = 100, daysOfUsage = 100)
        assertTrue(appRating.shouldShowInitialRatingPopup(userEvents))
    }

    // initial rating popup after deactivation

    @Test
    fun shouldShowInitialRatingPopupAfterAdvertDeactivation_shouldReturnFalse_whenAdvertHasNotBeenDeactivated() {
        assertTrue(shouldShowInitialRatingPopupAfterDeactivation())

        assertFalse(shouldShowInitialRatingPopupAfterDeactivation(advertDeactivated = false))
    }

    @Test
    fun shouldShowInitialRatingPopupAfterAdvertDeactivation_shouldReturnFalse_whenAdvertHasNotBeenLive() {
        assertTrue(shouldShowInitialRatingPopupAfterDeactivation())

        assertFalse(shouldShowInitialRatingPopupAfterDeactivation(advertLastLive = null))
    }

    @Test
    fun shouldShowInitialRatingPopupAfterAdvertDeactivation_shouldReturnFalse_whenInitialPopupAlreadyShown() {
        assertTrue(shouldShowInitialRatingPopupAfterDeactivation())

        assertFalse(shouldShowInitialRatingPopupAfterDeactivation(initialRatingShownDate = date()))
    }

    @Test
    fun shouldShowInitialRatingPopupAfterAdvertDeactivation_shouldReturnFalse_whenDaysOfUsageLessThanTwo() {
        assertTrue(shouldShowInitialRatingPopupAfterDeactivation())

        assertFalse(shouldShowInitialRatingPopupAfterDeactivation(daysOfUsage = 1))
        assertFalse(shouldShowInitialRatingPopupAfterDeactivation(daysOfUsage = 0))
        assertFalse(shouldShowInitialRatingPopupAfterDeactivation(daysOfUsage = -1))
    }

    @Test
    fun shouldShowInitialRatingPopupAfterAdvertDeactivation_shouldReturnFalse_whenAdvertHasBeenLiveForLesThan2Days() {
        assertTrue(shouldShowInitialRatingPopupAfterDeactivation())

        val advertLastLive = Calendar.getInstance()

        assertFalse(shouldShowInitialRatingPopupAfterDeactivation(advertLastLive = advertLastLive))

        moveCalendarBack(advertLastLive, 1)
        assertFalse(shouldShowInitialRatingPopupAfterDeactivation(advertLastLive = advertLastLive))
    }

    @Test
    fun shouldShowInitialRatingPopupAfterAdvertDeactivation_shouldReturnTrue_whenAllRequirementsMet() {
        assertFalse(shouldShowInitialRatingPopupAfterDeactivation(
            advertDeactivated = false,
            advertLastLive = null,
            initialRatingShownDate = date(),
            daysOfUsage = 0))

        val advertLastLive = date()

        for (i in 1 until 10) {
            moveCalendarBack(advertLastLive, i + 1)
            assertTrue(shouldShowInitialRatingPopupAfterDeactivation(
                advertDeactivated = true,
                advertLastLive = advertLastLive,
                initialRatingShownDate = null,
                daysOfUsage = i + 1))
        }
    }

    // reminder rating popup

    @Test
    fun shouldShowReminderRatingPopup_shouldReturnFalse_whenAppRated() {
        assertTrue(shouldShowReminderRatingPopup())

        assertFalse(shouldShowReminderRatingPopup(appRated = true))
    }

    @Test
    fun shouldShowReminderRatingPopup_shouldReturnFalse_whenReminderPopupAlreadyShown() {
        assertTrue(shouldShowReminderRatingPopup())

        assertFalse(shouldShowReminderRatingPopup(reminderRatingShownDate = date()))
    }

    @Test
    fun shouldShowReminderRatingPopup_shouldReturnFalse_whenInitialPopupHasNotBeenShown() {
        assertTrue(shouldShowReminderRatingPopup())

        assertFalse(shouldShowReminderRatingPopup(initialRatingShownDate = null))
    }

    @Test
    fun shouldShowReminderRatingPopup_shouldReturnFalse_whenUsageBetweenInitialAndReminderPopup_lessThan10Days() {
        assertTrue(shouldShowReminderRatingPopup())

        val initialRatingShownDate = Calendar.getInstance()

        for (i in 0 until 9) {
            // initial rating popup will be marked as shown 1 day ago, 2 days ago, etc
            moveCalendarBack(initialRatingShownDate, days = 1)
            assertFalse(shouldShowReminderRatingPopup(initialRatingShownDate))
        }
    }

    @Test
    fun shouldShowReminderRatingPopup_shouldReturnTrue_whenRequirementsMet() {
        assertFalse(appRating.shouldShowReminderRatingPopup(userEvents()))

        val initialRatingShownDate = Calendar.getInstance()
        for (i in 0 until 20) {
            // initial rating popup will be marked as shown 10 days ago, 11 days ago, etc
            moveCalendarBack(initialRatingShownDate, days = 10 + i)
            val userEvents = userEvents(
                appRated = false,
                reminderRatingShownDate = null,
                initialRatingShownDate = initialRatingShownDate)
            assertTrue(appRating.shouldShowReminderRatingPopup(userEvents))
        }
    }

    private fun shouldShowInitialRatingPopup(search: Int = 100, saveAdvert: Int = 100, messageAdvert: Int = 100, daysOfUsage: Int = 100,
                                             initialRatingShownDate: Calendar? = null): Boolean {

        val userEvents = UserEvents(search, saveAdvert, messageAdvert, daysOfUsage, initialRatingShownDate, null, false)
        return appRating.shouldShowInitialRatingPopup(userEvents)
    }

    private fun shouldShowInitialRatingPopupAfterDeactivation(advertDeactivated: Boolean = true, advertLastLive: Calendar? = date(),
                                                              daysOfUsage: Int = 100, initialRatingShownDate: Calendar? = null): Boolean {

        val userEvents = UserEvents(0, 0, 0, daysOfUsage, initialRatingShownDate, null, false)
        return appRating.shouldShowInitialRatingPopupAfterAdvertDeactivation(advertDeactivated, advertLastLive, userEvents)
    }

    private fun shouldShowReminderRatingPopup(initialRatingShownDate: Calendar? = date(), reminderRatingShownDate: Calendar? = null,
                                              daysOfUsage: Int = 100, appRated: Boolean = false): Boolean {

        val userEvents = UserEvents(0, 0, 0, daysOfUsage, initialRatingShownDate, reminderRatingShownDate, appRated)
        return appRating.shouldShowReminderRatingPopup(userEvents)
    }

    private fun userEvents(searchCount: Int = 0, saveAdvertCount: Int = 0, messageAdvertCount: Int = 0, daysOfUsage: Int = 0,
                           initialRatingShownDate: Calendar? = null, reminderRatingShownDate: Calendar? = null,
                           appRated: Boolean = false): UserEvents {

        return UserEvents(searchCount, saveAdvertCount, messageAdvertCount, daysOfUsage,
            initialRatingShownDate, reminderRatingShownDate, appRated)
    }
}