package com.spareroom.ui.util;

import android.app.Application;

import com.spareroom.R;

import org.junit.*;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.spareroom.ui.util.DateUtils.DOT_GLYPH;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DateUtilsTest {

    private static final String OVER_A_WEEK_AGO = "over a week ago";
    private static final String YESTERDAY = "yesterday";
    private static final String TODAY = "today";
    private static final String NOW = "now";
    private static final String TOMORROW = "tomorrow";
    private static final String NOT_AVAILABLE = "n/a";

    private TimeZone defaultTimeZone;
    private Locale defaultLocale;

    private DateUtils dateUtils;

    @Before
    public void setUp() {
        defaultTimeZone = TimeZone.getDefault();
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));

        defaultLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        Application context = mock(Application.class);
        when(context.getString(R.string.not_available)).thenReturn(NOT_AVAILABLE);
        when(context.getString(R.string.today)).thenReturn(TODAY);
        when(context.getString(R.string.yesterday)).thenReturn(YESTERDAY);
        when(context.getString(R.string.over_week_ago)).thenReturn(OVER_A_WEEK_AGO);
        when(context.getString(R.string.tomorrow)).thenReturn(TOMORROW);
        when(context.getString(R.string.now)).thenReturn(NOW);

        dateUtils = new DateUtils(defaultLocale, context);
    }

    @After
    public void tearDown() {
        TimeZone.setDefault(defaultTimeZone);
        Locale.setDefault(defaultLocale);
    }

    @Test
    public void clearTime_shouldRemoveTimePart() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 1, 2, 13, 47, 30);
        calendar.set(Calendar.MILLISECOND, 600);

        assertCalendar(calendar, 2017, 1, 2, 13, 47, 30, 600);
        DateUtils.clearTime(calendar);
        assertCalendar(calendar, 2017, 1, 2, 0, 0, 0, 0);
    }

    @Test
    public void getTimeDifferenceInDays_shouldReturnCorrectDaysCount() {
        assertDay(0, 0);
        assertDay(1, 1);
        assertDay(100, 100);
        assertDay(-1, -1);
    }

    @Test
    public void getFormattedOnlineDate_shouldReturnNotAvailable_whenDateNotProvided() {
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedOnlineDate(null));
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedOnlineDate(""));
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedOnlineDate(" "));
    }

    @Test
    public void getFormattedOnlineDate_shouldReturnNotAvailable_whenDateMalformed() {
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedOnlineDate("2017-06-05"));
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedOnlineDate("date"));
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedOnlineDate("10:22:22"));
    }

    @Test
    public void getFormattedOnlineDate_shouldReturnNotAvailable_whenDateFromFuture() {
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedOnlineDate(getFormattedInputDate(getDate(1, 0))));
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedOnlineDate(getFormattedInputDate(getDate(2, 0))));
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedOnlineDate(getFormattedInputDate(getDate(100, 0))));
    }

    @Test
    public void getFormattedOnlineDate_shouldReturnProperlyFormattedString() {
        assertEquals(TODAY, dateUtils.getFormattedOnlineDate(getFormattedInputDate(getDate(0, 0))));
        assertEquals(TODAY, dateUtils.getFormattedOnlineDate(getFormattedInputDate(getDate(0, 1))));
        assertEquals(YESTERDAY, dateUtils.getFormattedOnlineDate(getFormattedInputDate(getDate(-1, 0))));
        assertEquals(YESTERDAY, dateUtils.getFormattedOnlineDate(getFormattedInputDate(getDate(-1, 1))));

        for (int i = -2; i >= -6; i--) {
            Calendar calendar = getDate(i, 0);
            assertEquals(new SimpleDateFormat("EEEE", defaultLocale).format(calendar.getTime()), dateUtils.getFormattedOnlineDate(getFormattedInputDate(calendar)));
        }

        Calendar calendar = getDate(-7, 0);
        assertNotEquals(new SimpleDateFormat("EEEE", defaultLocale).format(calendar.getTime()), dateUtils.getFormattedOnlineDate(getFormattedInputDate(calendar)));

        assertEquals(OVER_A_WEEK_AGO, dateUtils.getFormattedOnlineDate(getFormattedInputDate(calendar)));
    }

    @Test
    public void getFormattedAvailabilityDate_shouldReturnNotAvailable_whenDateNotProvided() {
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedAvailabilityDate(null));
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedAvailabilityDate(""));
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedAvailabilityDate(" "));
    }

    @Test
    public void getFormattedAvailabilityDate_shouldReturnDefaultDate_whenDateMalformed() {
        assertEquals("2017-06", dateUtils.getFormattedAvailabilityDate("2017-06"));
        assertEquals("2017", dateUtils.getFormattedAvailabilityDate("2017"));
        assertEquals("10:22:22", dateUtils.getFormattedAvailabilityDate("10:22:22"));
    }

    @Test
    public void getFormattedAvailabilityDate_shouldReturnProperlyFormattedString() {
        for (int i = -10; i <= 0; i++) {
            assertEquals(NOW, dateUtils.getFormattedAvailabilityDate(getFormattedInputDate(getDate(i, 0))));
        }

        assertEquals(TOMORROW, dateUtils.getFormattedAvailabilityDate(getFormattedInputDate(getDate(1, 0))));

        for (int i = 2; i <= 10; i++) {
            Calendar calendar = getDate(i, 0);
            assertEquals(new SimpleDateFormat("d MMMM", defaultLocale).format(calendar.getTime()), dateUtils.getFormattedAvailabilityDate(getFormattedInputDate(calendar)));
        }
    }

    @Test
    public void getFormattedDateOfContact_shouldReturnNotAvailable_whenDateNotProvided() {
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedDateOfContact(null));
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedDateOfContact(""));
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedDateOfContact(" "));

        // Trimmed time
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedDateOfContact(null, true));
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedDateOfContact("", true));
        assertEquals(NOT_AVAILABLE, dateUtils.getFormattedDateOfContact(" ", true));
    }

    @Test
    public void getFormattedDateOfContact_shouldReturnDefaultDate_whenDateMalformed() {
        assertEquals("2017-06", dateUtils.getFormattedDateOfContact("2017-06"));
        assertEquals("2017", dateUtils.getFormattedDateOfContact("2017"));
        assertEquals("10:22:22", dateUtils.getFormattedDateOfContact("10:22:22"));

        // Trimmed time
        assertEquals("2017-06", dateUtils.getFormattedDateOfContact("2017-06", true));
        assertEquals("2017", dateUtils.getFormattedDateOfContact("2017", true));
        assertEquals("10:22:22", dateUtils.getFormattedDateOfContact("10:22:22", true));
    }

    @Test
    public void getFormattedDateOfContact_shouldReturnProperlyFormattedString() {
        Calendar calendar = getDate(0, 23, 15);
        assertEquals(getExpectedDateOfContact(calendar, TODAY), dateUtils.getFormattedDateOfContact(getFormattedInputDate(calendar)));
        assertEquals(TODAY, dateUtils.getFormattedDateOfContact(getFormattedInputDate(calendar), true));

        calendar = getDate(-1, 22, 20);
        assertEquals(getExpectedDateOfContact(calendar, YESTERDAY), dateUtils.getFormattedDateOfContact(getFormattedInputDate(calendar)));
        assertEquals(YESTERDAY, dateUtils.getFormattedDateOfContact(getFormattedInputDate(calendar), true));

        for (int i = -2; i >= -6; i--) {
            calendar = getDate(i, 0);
            assertEquals(getExpectedDateOfContact("EEEE", calendar), dateUtils.getFormattedDateOfContact(getFormattedInputDate(calendar)));
            assertEquals(getTimeTrimmedExpectedDateOfContact("EEEE", calendar), dateUtils.getFormattedDateOfContact(getFormattedInputDate(calendar), true));
        }

        calendar = getDate(-7, 0);
        assertNotEquals(getExpectedDateOfContact("EEEE", calendar), dateUtils.getFormattedDateOfContact(getFormattedInputDate(calendar)));
        assertNotEquals(getTimeTrimmedExpectedDateOfContact("EEEE", calendar), dateUtils.getFormattedDateOfContact(getFormattedInputDate(calendar), true));

        assertEquals(getExpectedDateOfContact("d MMMM", calendar), dateUtils.getFormattedDateOfContact(getFormattedInputDate(calendar)));
        assertEquals(getTimeTrimmedExpectedDateOfContact("d MMMM", calendar), dateUtils.getFormattedDateOfContact(getFormattedInputDate(calendar), true));
    }

    @Test
    public void prefixWithZero_whenInteger_thenZeroAdded() {
        assertEquals("01", dateUtils.prefixWithZero(1));
        assertEquals("11", dateUtils.prefixWithZero(11));
        assertEquals("123456", dateUtils.prefixWithZero(123456));
    }

    @Test
    public void createStringDate_whenProperValues_thenDateProperlyFormatted() {
        assertEquals("0-00-00", dateUtils.createStringDate(0, 0, 0));
        assertEquals("1-01-01", dateUtils.createStringDate(1, 1, 1));
        assertEquals("12-12-12", dateUtils.createStringDate(12, 12, 12));
        assertEquals("123-123-123", dateUtils.createStringDate(123, 123, 123));
    }

    @Test
    public void getFormattedDateOfContact_shouldReturnProperlyFormattedString_whenHourIsMidnight() {
        String date = "2017-06-01 00:00:00";
        assertEquals(String.format("%s %s %s", "12:00 AM", DOT_GLYPH, "1 June"), dateUtils.getFormattedDateOfContact(date));
        assertEquals("1 June", dateUtils.getFormattedDateOfContact(date, true));

        date = "2017-06-01 00:01:01";
        assertEquals(String.format("%s %s %s", "12:01 AM", DOT_GLYPH, "1 June"), dateUtils.getFormattedDateOfContact(date));
        assertEquals("1 June", dateUtils.getFormattedDateOfContact(date, true));
    }

    @Test
    public void getFormattedDateOfContact_shouldReturnProperlyFormattedString_withSingleDigitHour_whenHourBefore10() {
        // AM
        String date = "2017-06-01 01:00:00";
        assertEquals(String.format("1:00 AM %s %s", DOT_GLYPH, "1 June"), dateUtils.getFormattedDateOfContact(date));

        // PM
        date = "2017-06-01 13:00:00";
        assertEquals(String.format("1:00 PM %s %s", DOT_GLYPH, "1 June"), dateUtils.getFormattedDateOfContact(date));

        // check all AM single digit hours
        for (int i = 1; i < 10; i++) {
            date = String.format("2017-06-01 %s:00:00", i);
            assertEquals(String.format("%s:00 AM %s %s", i, DOT_GLYPH, "1 June"), dateUtils.getFormattedDateOfContact(date));
        }

        // check all PM single digit hours
        for (int i = 13; i < 22; i++) {
            date = String.format("2017-06-01 %s:00:00", i);
            int time = i - 12;
            assertTrue(time < 10);
            assertEquals(String.format("%s:00 PM %s %s", time, DOT_GLYPH, "1 June"), dateUtils.getFormattedDateOfContact(date));
        }
    }

    @Test
    public void getFormattedDateOfContact_shouldReturnProperlyFormattedString_whenHourIsNoon() {
        String date = "2017-06-01 12:00:00";
        assertEquals(String.format("%s %s %s", "12:00 PM", DOT_GLYPH, "1 June"), dateUtils.getFormattedDateOfContact(date));
        assertEquals("1 June", dateUtils.getFormattedDateOfContact(date, true));

        date = "2017-06-01 12:01:01";
        assertEquals(String.format("%s %s %s", "12:01 PM", DOT_GLYPH, "1 June"), dateUtils.getFormattedDateOfContact(date));
        assertEquals("1 June", dateUtils.getFormattedDateOfContact(date, true));
    }

    private String getExpectedDateOfContact(Calendar calendar, String day) {
        return String.format("%s %s %s", new SimpleDateFormat("h:mm aaa", defaultLocale).format(calendar.getTime()), DOT_GLYPH, day);
    }

    private String getExpectedDateOfContact(String datePattern, Calendar calendar) {
        return String.format("%s %s %s", new SimpleDateFormat("h:mm aaa", defaultLocale).format(calendar.getTime()),
                DOT_GLYPH, new SimpleDateFormat(datePattern, defaultLocale).format(calendar.getTime()));
    }

    private String getTimeTrimmedExpectedDateOfContact(String datePattern, Calendar calendar) {
        return new SimpleDateFormat(datePattern, defaultLocale).format(calendar.getTime());
    }

    private Calendar getDate(int addedDays, int hour, int min) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, addedDays);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, min);
        return calendar;
    }

    private Calendar getDate(int addedDays, int addedSeconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, addedDays);
        calendar.add(Calendar.SECOND, addedSeconds);
        return calendar;
    }

    private String getFormattedInputDate(Calendar calendar) {
        return String.format("%s-%s-%s %s:%s:%s", calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH),
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
    }

    private void assertCalendar(Calendar calendar, int year, int month, int day, int hour, int minute, int second, int millis) {
        assertEquals(year, calendar.get(Calendar.YEAR));
        assertEquals(month, calendar.get(Calendar.MONTH));
        assertEquals(day, calendar.get(Calendar.DAY_OF_MONTH));
        assertEquals(hour, calendar.get(Calendar.HOUR_OF_DAY));
        assertEquals(minute, calendar.get(Calendar.MINUTE));
        assertEquals(second, calendar.get(Calendar.SECOND));
        assertEquals(millis, calendar.get(Calendar.MILLISECOND));
    }

    private void assertDay(int expectedValue, int daysAgo) {
        Calendar calendarNow = Calendar.getInstance();
        DateUtils.clearTime(calendarNow);

        Calendar calendarToCompare = Calendar.getInstance();
        calendarToCompare.setTimeInMillis(calendarNow.getTimeInMillis());
        calendarToCompare.add(Calendar.DAY_OF_YEAR, -daysAgo);

        assertEquals(expectedValue, DateUtils.getTimeDifferenceInDays(calendarNow, calendarToCompare));
    }
}
