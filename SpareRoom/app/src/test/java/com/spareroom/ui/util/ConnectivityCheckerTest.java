package com.spareroom.ui.util;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConnectivityCheckerTest {

    @Test
    public void isNetworkAvailable_shouldReturnFalse_whenNoNetworkInfo() {
        assertFalse(new ConnectivityChecker(getContext(getConnectivityManager(null))).isConnected());
    }

    @Test
    public void isNetworkAvailable_shouldReturnFalse_whenNotConnected() {
        assertFalse(new ConnectivityChecker(getContext(getConnectivityManager(getNetworkInfo(false)))).isConnected());
    }

    @Test
    public void isNetworkAvailable_shouldReturnTrue_whenConnected() {
        assertTrue(new ConnectivityChecker(getContext(getConnectivityManager(getNetworkInfo(true)))).isConnected());
    }

    private ConnectivityManager getConnectivityManager(NetworkInfo mockNetworkInfo) {
        ConnectivityManager mockConnectivityManager = mock(ConnectivityManager.class);
        when(mockConnectivityManager.getActiveNetworkInfo()).thenReturn(mockNetworkInfo);
        return mockConnectivityManager;
    }

    private NetworkInfo getNetworkInfo(boolean isConnected) {
        NetworkInfo mockNetworkInfo = mock(NetworkInfo.class);
        when(mockNetworkInfo.isConnected()).thenReturn(isConnected);

        return mockNetworkInfo;
    }

    private Application getContext(ConnectivityManager mockConnectivityManager) {
        Application mockContext = mock(Application.class);
        when(mockContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(mockConnectivityManager);
        return mockContext;
    }
}
