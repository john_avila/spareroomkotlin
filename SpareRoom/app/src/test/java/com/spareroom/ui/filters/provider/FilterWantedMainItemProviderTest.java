package com.spareroom.ui.filters.provider;

import android.app.Application;

import com.spareroom.*;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted;
import com.spareroom.ui.util.DateUtils;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Locale;

import static com.spareroom.ui.filters.provider.FilterWantedMainItemProvider.WANTED_UNSUITABLE;
import static com.spareroom.ui.filters.provider.FilterWantedMainItemProvider.WANTED_WITH_PHOTOS;
import static com.spareroom.ui.filters.provider.ItemProviderTestUtil.*;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class FilterWantedMainItemProviderTest {

    private final Application application = RuntimeEnvironment.application;
    private final String SPACE = " ";
    private final String BULLET_POINT_STRING_RESOURCE = application.getString(R.string.bulletPoint);
    private final String WEEKDAY_ONLY_RENTERS_STRING_RESOURCE = application.getString(R.string.weekdayOnlyRenters);
    private final String LGBTQ_STRING_RESOURCE = application.getString(R.string.lgbtq);
    private final String COUPLES_STRING_RESOURCE = application.getString(R.string.couples);
    private final String NON_SMOKERS_STRING_RESOURCE = application.getString(R.string.nonSmokers);
    private final String BETWEEN_STRING = application.getString(R.string.between);
    private final String CURRENCY_STRING_RESOURCE = application.getString(R.string.currency_symbol);
    private final String EN_DASH_STRING_RESOURCE = application.getString(R.string.en_dash);
    private final String FROM_STRING_RESOURCE = application.getString(R.string.from);
    private final String UP_TO_STRING_RESOURCE = application.getString(R.string.upTo);
    private final String MONTHS_STRING_RESOURCE = application.getString(R.string.months);
    private final String DOUBLE_BEDROOM_STRING_RESOURCE = application.getString(R.string.doubleBedroom);
    private final String FEMALES_STRING_RESOURCE = application.getString(R.string.females);
    private final String YEARS_STRING_RESOURCE = application.getString(R.string.years);
    private final String PROFESSIONALS_STRING_RESOURCE = application.getString(R.string.professionals);

    private FilterWantedMainItemProvider provider;
    private final ItemProviderTestUtil itemProviderTestUtil = new ItemProviderTestUtil();
    private SubtitleUtil subtitleUtil;

    @Before
    public void setUp() {
        provider = new FilterWantedMainItemProvider();
        provider.createNewList(new SearchAdvertListPropertiesWanted(), null);
        subtitleUtil = new SubtitleUtil(new DateUtils(Locale.getDefault(), application));
    }

    @Test
    public void get_whenRetrievingList_thenAllItemsAdded() {
        provider.createNewList(new SearchAdvertListPropertiesWanted(), null);
        assertEquals(14, provider.get().size());
    }

    @Test
    public void createNewList_whenEmptyPropertyList_thenDefaultFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted(), null);

        assertFalse(((CompoundButtonParams) itemProviderTestUtil.getParameter(provider.get(), WANTED_UNSUITABLE)).selected());
        assertFalse(((CompoundButtonParams) itemProviderTestUtil.getParameter(provider.get(), WANTED_WITH_PHOTOS)).selected());
    }

    @Test
    public void createNewList_whenFullPropertyList_thenFilledFilterItemList() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted(), null);

        assertTrue(((CompoundButtonParams) itemProviderTestUtil.getParameter(provider.get(), WANTED_UNSUITABLE)).selected());
        assertTrue(((CompoundButtonParams) itemProviderTestUtil.getParameter(provider.get(), WANTED_WITH_PHOTOS)).selected());
    }

    @Test
    public void saveProperties_whenEmptyList_andEmptyProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted(), null);
        testEmptyList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenEmptyList_andFullProperties_thenSaveDefaultValues() {
        provider.createNewList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted(), null);
        testEmptyList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenFullList_andEmptyProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted(), null);
        testFullList(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted());
    }

    @Test
    public void saveProperties_whenFullList_andFullProperties_thenSaveListValues() {
        provider.createNewList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted(), null);
        testFullList(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted());
    }

    @Test
    public void getMonthlyBudgetSubtitle_whenAllSet_thenWholeString() {
        // "Between $1 – $897"
        assertEquals(
                BETWEEN_STRING + SPACE
                        + CURRENCY_STRING_RESOURCE + DEFAULT_MIN_RENT_INT_VALUE
                        + EN_DASH_STRING_RESOURCE
                        + CURRENCY_STRING_RESOURCE + DEFAULT_MAX_RENT_INT_VALUE,
                provider.getMonthlyBudgetSubtitle(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted()));
    }

    @Test
    public void getMonthlyBudgetSubtitle_whenSomeSet_thenSomeString() {
        // "From $1"
        SearchAdvertListPropertiesWanted minPriceSavedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted();
        minPriceSavedProperties.minMonthlyRent(DEFAULT_MIN_RENT_INT_VALUE);
        assertEquals(
                FROM_STRING_RESOURCE + SPACE + CURRENCY_STRING_RESOURCE + DEFAULT_MIN_RENT_INT_VALUE,
                provider.getMonthlyBudgetSubtitle(minPriceSavedProperties));

        // "Up to $897"
        SearchAdvertListPropertiesWanted maxPriceSavedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted();
        maxPriceSavedProperties.maxMonthlyRent(DEFAULT_MIN_RENT_INT_VALUE);
        assertEquals(
                UP_TO_STRING_RESOURCE + SPACE + CURRENCY_STRING_RESOURCE + DEFAULT_MIN_RENT_INT_VALUE,
                provider.getMonthlyBudgetSubtitle(maxPriceSavedProperties));

    }

    @Test
    public void getMonthlyBudgetSubtitle_whenNotSet_thenEmptyString() {
        assertEquals("", provider.getMonthlyBudgetSubtitle(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted()));
    }

    @Test
    public void getAvailabilitySubtitle_whenAllSet_thenWholeString() {
        // "2017-02-05  •  8 – 11 months  •  Weekday-only adverts"
        assertEquals(
                subtitleUtil.getMoveInText(DEFAULT_AVAILABLE_FROM_VALUE) + BULLET_POINT_STRING_RESOURCE
                        + BETWEEN_STRING + SPACE
                        + DEFAULT_MIN_TERM_VALUE.getValue() + EN_DASH_STRING_RESOURCE
                        + DEFAULT_MAX_TERM_VALUE.getValue() + SPACE + MONTHS_STRING_RESOURCE + BULLET_POINT_STRING_RESOURCE
                        + WEEKDAY_ONLY_RENTERS_STRING_RESOURCE,
                provider.getAvailabilitySubtitle(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted()));
    }

    @Test
    public void getAvailabilitySubtitle_whenSomeSet_thenSomeString() {
        SearchAdvertListPropertiesWanted savedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted();
        // "2017-02-05"
        savedProperties.setAvailableFrom(DEFAULT_AVAILABLE_FROM_VALUE);
        assertEquals(subtitleUtil.getMoveInText(DEFAULT_AVAILABLE_FROM_VALUE), provider.getAvailabilitySubtitle(savedProperties));
        // "2017-02-05  •  8 – 11 months"
        savedProperties.setMinTerm(DEFAULT_MIN_TERM_VALUE);
        savedProperties.setMaxTerm(DEFAULT_MAX_TERM_VALUE);
        assertEquals(
                subtitleUtil.getMoveInText(DEFAULT_AVAILABLE_FROM_VALUE) + BULLET_POINT_STRING_RESOURCE
                        + BETWEEN_STRING + SPACE
                        + DEFAULT_MIN_TERM_VALUE.getValue() + EN_DASH_STRING_RESOURCE
                        + DEFAULT_MAX_TERM_VALUE.getValue() + SPACE + MONTHS_STRING_RESOURCE,
                provider.getAvailabilitySubtitle(savedProperties));

    }

    @Test
    public void getAvailabilitySubtitle_whenNotSet_thenEmptyString() {
        assertEquals("", provider.getAvailabilitySubtitle(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted()));
    }

    @Test
    public void getBedroomTypeSubtitle_whenAllSet_thenWholeString() {
        // "Double bedroom"
        assertEquals(DOUBLE_BEDROOM_STRING_RESOURCE, provider.getBedroomTypeSubtitle(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted()));
    }

    @Test
    public void getBedroomTypeSubtitle_whenNotSet_thenEmptyString() {
        assertEquals("", provider.getBedroomTypeSubtitle(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted()));
    }

    @Test
    public void getSharingWithSubtitle_whenAllSet_thenWholeString() {
        // "All-female household  •  LGBTQ"
        assertEquals(
                FEMALES_STRING_RESOURCE + BULLET_POINT_STRING_RESOURCE + LGBTQ_STRING_RESOURCE,
                provider.getGenderSexualitySubtitle(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted()));
    }

    @Test
    public void getSharingWithSubtitle_whenSomeSet_thenSomeString() {
        // "All-female household"
        SearchAdvertListPropertiesWanted savedProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted();
        savedProperties.setGenderFilter(DEFAULT_GENDER_FILTER_VALUE);
        assertEquals(FEMALES_STRING_RESOURCE, provider.getGenderSexualitySubtitle(savedProperties));
    }

    @Test
    public void getSharingWithSubtitle_whenNotSet_thenEmptyString() {
        assertEquals("", provider.getGenderSexualitySubtitle(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted()));
    }

    @Test
    public void getAgeSubtitle_whenAllSet_thenWholeString() {
        // "18 - 24 years"
        assertEquals(
                BETWEEN_STRING + SPACE + DEFAULT_MIN_AGE_REQ_INT_VALUE + EN_DASH_STRING_RESOURCE + DEFAULT_MAX_AGE_REQ_INT_VALUE + SPACE + YEARS_STRING_RESOURCE,
                provider.getAgeSubtitle(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted()));
    }

    @Test
    public void getAgeSubtitle_whenSomeSet_thenSomeString() {
        // "From 18 years"
        SearchAdvertListPropertiesWanted fromProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted();
        fromProperties.setMinAge(DEFAULT_MIN_AGE_REQ_INT_VALUE);
        assertEquals(
                FROM_STRING_RESOURCE + SPACE + DEFAULT_MIN_AGE_REQ_INT_VALUE + SPACE + YEARS_STRING_RESOURCE,
                provider.getAgeSubtitle(fromProperties));

        // "Up to 24 years"
        SearchAdvertListPropertiesWanted upToProperties = itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted();
        upToProperties.setMaxAge(DEFAULT_MAX_AGE_REQ_INT_VALUE);
        assertEquals(
                UP_TO_STRING_RESOURCE + SPACE + DEFAULT_MAX_AGE_REQ_INT_VALUE + SPACE + YEARS_STRING_RESOURCE,
                provider.getAgeSubtitle(upToProperties));
    }

    @Test
    public void getAgeSubtitle_whenNotSet_thenEmptyString() {
        assertEquals("", provider.getAgeSubtitle(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted()));
    }

    @Test
    public void getEmploymentStatusSubtitle_whenAllSet_thenWholeString() {
        // "Professionals"
        assertEquals(
                PROFESSIONALS_STRING_RESOURCE,
                provider.getEmploymentStatusSubtitle(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted()));
    }

    @Test
    public void getEmploymentStatusSubtitle_whenNotSet_thenEmptyString() {
        assertEquals("", provider.getEmploymentStatusSubtitle(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted()));
    }

    @Test
    public void getRelationshipSubtitle_whenAllSet_thenWholeString() {
        // "Couples"
        assertEquals(
                COUPLES_STRING_RESOURCE,
                provider.getRelationshipSubtitle(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted()));
    }

    @Test
    public void getRelationshipSubtitle_whenNotSet_thenEmptyString() {
        assertEquals("", provider.getRelationshipSubtitle(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted()));
    }

    @Test
    public void getSmokingSubtitle_whenAllSet_thenWholeString() {
        // "Couples"
        assertEquals(
                NON_SMOKERS_STRING_RESOURCE,
                provider.getSmokingSubtitle(itemProviderTestUtil.createFullSearchAdvertListPropertiesWanted()));
    }

    @Test
    public void getSmokingSubtitle_whenNotSet_thenEmptyString() {
        assertEquals("", provider.getSmokingSubtitle(itemProviderTestUtil.createEmptySearchAdvertListPropertiesWanted()));
    }

    private void testFullList(final SearchAdvertListPropertiesWanted properties) {
        provider.updateProperties(properties, true);

        assertTrue(properties.getIncludeHidden());
        assertTrue(properties.getPhotoAdvertsOnly());

    }

    private void testEmptyList(final SearchAdvertListPropertiesWanted properties) {
        provider.updateProperties(properties, true);

        assertFalse(properties.getIncludeHidden());
        assertFalse(properties.getPhotoAdvertsOnly());
    }

}
