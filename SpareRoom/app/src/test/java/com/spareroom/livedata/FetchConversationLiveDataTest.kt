package com.spareroom.livedata

import com.spareroom.TestApplication
import com.spareroom.TestConstants
import com.spareroom.controller.SpareroomContext
import com.spareroom.integration.business.apiv2.*
import com.spareroom.model.business.Conversation
import com.spareroom.ui.util.AdvertUtils
import io.reactivex.disposables.CompositeDisposable
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

private const val OTHER_USER_ID = "123456"
private const val MY_USER_ID = "000000"

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [(TestConstants.CONFIG_SDK)], application = TestApplication::class)
class FetchConversationLiveDataTest {

    private val spareroomContext = mock(SpareroomContext::class.java)

    @Before
    fun setUp() {
        `when`(spareroomContext.userId).thenReturn(MY_USER_ID)
    }

    @Test
    fun showOtherUserOfferedAdverts_whenAdvertIsOffered_andPlacedByOtherUser_andOtherUserHasOfferedAdverts() {
        assertTrue(showOtherUserOfferedAdverts(otherUserOfferedAds = 1, otherUserWantedAds = 0, otherUserId = OTHER_USER_ID, placedBy = OTHER_USER_ID, isAdOffered = true))
        assertTrue(showOtherUserOfferedAdverts(otherUserOfferedAds = 1, otherUserWantedAds = 1, otherUserId = OTHER_USER_ID, placedBy = OTHER_USER_ID, isAdOffered = true))
    }

    @Test
    fun showOtherUserOfferedAdverts_whenAdvertIsOffered_andPlacedByMe_andOtherUserDoesNotHaveWantedAdverts() {
        assertTrue(showOtherUserOfferedAdverts(otherUserOfferedAds = 0, otherUserWantedAds = 0, otherUserId = MY_USER_ID, placedBy = MY_USER_ID, isAdOffered = true))
        assertTrue(showOtherUserOfferedAdverts(otherUserOfferedAds = 1, otherUserWantedAds = 0, otherUserId = MY_USER_ID, placedBy = MY_USER_ID, isAdOffered = true))
    }

    @Test
    fun showOtherUserOfferedAdverts_whenAdvertIsWanted_andPlacedByMe_andOtherUserHasOfferedAdverts() {
        assertTrue(showOtherUserOfferedAdverts(otherUserOfferedAds = 1, otherUserWantedAds = 0, otherUserId = MY_USER_ID, placedBy = MY_USER_ID, isAdOffered = false))
        assertTrue(showOtherUserOfferedAdverts(otherUserOfferedAds = 1, otherUserWantedAds = 1, otherUserId = MY_USER_ID, placedBy = MY_USER_ID, isAdOffered = false))
    }

    @Test
    fun showOtherUserOfferedAdverts_whenAdvertIsWanted_andPlacedByOtherUser_andOtherUserDoesNotHaveWantedAdverts() {
        assertTrue(showOtherUserOfferedAdverts(otherUserOfferedAds = 0, otherUserWantedAds = 0, otherUserId = OTHER_USER_ID, placedBy = OTHER_USER_ID, isAdOffered = false))
        assertTrue(showOtherUserOfferedAdverts(otherUserOfferedAds = 1, otherUserWantedAds = 0, otherUserId = OTHER_USER_ID, placedBy = OTHER_USER_ID, isAdOffered = false))
    }

    private fun showOtherUserOfferedAdverts(otherUserOfferedAds: Int, otherUserWantedAds: Int, otherUserId: String, placedBy: String, isAdOffered: Boolean): Boolean {
        val conversation = spy(Conversation::class.java)
        doReturn(otherUserOfferedAds).`when`(conversation).otherUserOfferedAdvertCount
        doReturn(otherUserWantedAds).`when`(conversation).otherUserWantedAdvertCount
        doReturn(placedBy).`when`(conversation).advertPlacedBy
        doReturn(isAdOffered).`when`(conversation).isAdvertOffered

        val fetchConversation = spy(FetchConversationLiveData(
            mock(IConversationDao::class.java),
            mock(IWantedAdvertsDao::class.java),
            mock(IOfferedAdvertsDao::class.java),
            spareroomContext,
            mock(AdvertUtils::class.java),
            mock(OtherUserAdvertsLiveData::class.java),
            mock(CompositeDisposable::class.java)))

        return fetchConversation.showOtherUserOfferedAdverts(conversation, otherUserId)
    }

}
