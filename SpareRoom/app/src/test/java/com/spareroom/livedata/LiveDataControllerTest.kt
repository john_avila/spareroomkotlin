package com.spareroom.livedata

import android.os.Looper
import androidx.lifecycle.MutableLiveData
import com.spareroom.TestApplication
import com.spareroom.TestConstants
import com.spareroom.TestUtils.any
import com.spareroom.integration.analytics.AnalyticsTrackerComposite
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatcher
import org.mockito.Mockito.*
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [(TestConstants.CONFIG_SDK)], application = TestApplication::class)
class LiveDataControllerTest {

    private val success = TestResult.TestResultSuccess()
    private val failure = TestResult.TestResultFailure()
    private val scheduler = AndroidSchedulers.from(Looper.getMainLooper())
    private val analyticsTracker = mock(AnalyticsTrackerComposite::class.java)
    private val exception = Exception()

    @Before
    fun setUp() {
        RxJavaPlugins.setErrorHandler(null)
    }

    @Test
    fun init_shouldAssignErrorHandler() {
        assertNull(RxJavaPlugins.getErrorHandler())
        TestLiveData(CompositeDisposable())
        assertNotNull(RxJavaPlugins.getErrorHandler())
    }

    @Test
    fun execute_shouldAddCompositeDisposableToTheList() {
        val compositeDisposable = spy(CompositeDisposable())
        val testLiveData = spy(TestLiveData(compositeDisposable))
        testLiveData.execute(task = { success })
        verify(testLiveData).add(any())
        assertEquals(1, compositeDisposable.size())
    }

    @Test
    fun execute_shouldFireSuccessCallback_whenTaskIsSuccessful() {
        val testLiveData = spyTestLiveData()
        testLiveData.execute(task = { success })
        assertEquals(success, testLiveData.value)
        verify(analyticsTracker, times(0)).logHandledException(eq("TestLiveData"), eq(exception))
    }

    @Test
    fun execute_shouldFireFailureCallback_whenTaskIsUnsuccessful() {
        val testLiveData = spyTestLiveData()
        testLiveData.execute(task = { throw exception })
        assertEquals(failure, testLiveData.value)
    }

    @Test
    fun execute_shouldNotFireAnyCallbacks_whenTaskIsDisposedOf_andUnsuccessful() {
        val testLiveData = spyTestLiveData()
        testLiveData.execute(task = {
            it.dispose()
            throw exception
        })

        assertNull(testLiveData.value)
        verify(testLiveData, times(1)).onPreExecute()
        verify(testLiveData, times(0)).onPostExecute()
    }

    @Test
    fun execute_shouldLogHandledException_whenTaskIsUnsuccessful() {
        val testLiveData = spyTestLiveData()
        testLiveData.execute(task = { throw exception })
        verify(analyticsTracker).logHandledException(eq("TestLiveData"), eq(exception))
    }

    @Test
    fun execute_shouldProperlyIndicateLoading_whenTaskIsSuccessful() {
        val testLiveData = TestLiveData(CompositeDisposable())
        assertFalse(testLiveData.isLoading)
        testLiveData.execute(task = {
            assertTrue(testLiveData.isLoading)
            success
        })
        assertFalse(testLiveData.isLoading)
    }

    @Test
    fun execute_shouldProperlyIndicateLoading_whenTaskIsUnsuccessful() {
        val testLiveData = TestLiveData(CompositeDisposable())
        assertFalse(testLiveData.isLoading)
        testLiveData.execute(task = {
            assertTrue(testLiveData.isLoading)
            throw exception
        })
        assertFalse(testLiveData.isLoading)
    }

    @Test
    fun execute_shouldProperlyIndicateLoading_whenTaskIsDisposedOf_andUnsuccessful() {
        val testLiveData = spyTestLiveData()
        assertFalse(testLiveData.isLoading)
        testLiveData.execute(task = {
            assertTrue(testLiveData.isLoading)
            it.dispose()
            throw exception
        })

        // that is fine, task will not be re-used
        assertTrue(testLiveData.isLoading)
    }

    @Test
    fun execute_shouldProperlyCall_onPreAndOnPostExecute_whenTaskIsSuccessful() {
        val testLiveData = spyTestLiveData()
        verify(testLiveData, times(0)).onPreExecute()
        verify(testLiveData, times(0)).onPostExecute()

        testLiveData.execute(task = {
            verify(testLiveData, times(1)).onPreExecute()
            verify(testLiveData, times(0)).onPostExecute()
            success
        })

        verify(testLiveData, times(1)).onPreExecute()
        verify(testLiveData, times(1)).onPostExecute()
    }

    @Test
    fun execute_shouldProperlyCall_onPreAndOnPostExecute_whenTaskIsUnsuccessful() {
        val testLiveData = spyTestLiveData()
        verify(testLiveData, times(0)).onPreExecute()
        verify(testLiveData, times(0)).onPostExecute()

        testLiveData.execute(task = {
            verify(testLiveData, times(1)).onPreExecute()
            verify(testLiveData, times(0)).onPostExecute()
            throw exception
        })

        verify(testLiveData, times(1)).onPreExecute()
        verify(testLiveData, times(1)).onPostExecute()
    }

    @Test
    fun execute_shouldProperlyCall_onPreAndOnPostExecute_whenTaskIsDisposedOf_andUnsuccessful() {
        val testLiveData = spyTestLiveData()
        verify(testLiveData, times(0)).onPreExecute()
        verify(testLiveData, times(0)).onPostExecute()

        testLiveData.execute(task = {
            verify(testLiveData, times(1)).onPreExecute()
            verify(testLiveData, times(0)).onPostExecute()

            it.dispose()
            throw exception
        })

        verify(testLiveData, times(1)).onPreExecute()
        verify(testLiveData, times(0)).onPostExecute()
    }

    @Test
    fun execute_shouldNotStartNewTask_whenTaskAlreadyInProgress() {
        val testLiveData = spyTestLiveData()
        testLiveData.execute(subscribeOn = Schedulers.newThread(), task = {
            Thread.sleep(Long.MAX_VALUE)
            throw exception
        })
        assertTrue(testLiveData.isLoading)

        // run task again while current one is executing
        testLiveData.execute(task = { success })
        verify(analyticsTracker).logHandledException(matches("Execute in LiveDataController"), argThat(ExceptionMatcher("Trying to call 'execute' when already in progress")))
        verify(testLiveData, times(1)).onPreExecute()

        // first task runs forever
        verify(testLiveData, times(0)).onPostExecute()
    }

    @Test
    fun trackTaskDuration_shouldTrackDurationWithAnalytics() {
        val testLiveData = spyTestLiveData()
        testLiveData.trackTaskDuration(taskName = "TestingLiveDataTask", analyticsTracker = analyticsTracker,
            task = {
                Thread.sleep(1)
                return testLiveData.execute(task = { success })
            })

        verify(analyticsTracker).trackTaskDuration(eq("TestingLiveDataTask"), floatThat { it >= 1 && it < 1000 })
    }

    private fun spyTestLiveData() = spy(TestLiveData(spy(CompositeDisposable())))

    private class ExceptionMatcher(private val message: String) : ArgumentMatcher<Exception> {
        override fun matches(argument: Exception) = argument.message == message
    }

    private inner class TestLiveData(disposables: CompositeDisposable) : LiveDataController<TestResult>(MutableLiveData(), disposables) {

        fun execute(subscribeOn: Scheduler = scheduler, task: (disposable: Disposable) -> TestResult) {
            return execute(subscribeOn = subscribeOn,
                observeOn = scheduler,
                analyticsTracker = analyticsTracker,
                task = { task(it) },
                failureResponse = { failure })
        }
    }

    private sealed class TestResult {
        class TestResultSuccess : TestResult()
        class TestResultFailure : TestResult()
    }

}