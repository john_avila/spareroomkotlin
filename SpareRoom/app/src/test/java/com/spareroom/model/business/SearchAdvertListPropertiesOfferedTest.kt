package com.spareroom.model.business

import com.spareroom.integration.business.SearchAdvertListParametersUtil
import org.junit.Assert.*
import org.junit.Test

class SearchAdvertListPropertiesOfferedTest {

    private val util = SearchAdvertListParametersUtil()

    @Test
    fun duplicate_shouldReturnDeepCopy() {
        val properties = util.createSearchAdvertListPropertiesOffered()
        val newProperties = util.createSearchAdvertListPropertiesOffered()
        var duplicate = properties.duplicate()

        // Testing:
        // They have the same values but don't point to the same objects
        assertEquals(properties, newProperties)
        assertFalse(properties === newProperties)
        assertEquals(properties.coordinates, newProperties.coordinates)
        assertFalse(properties.coordinates === newProperties.coordinates)

        assertEquals(properties, duplicate)
        assertFalse(properties === duplicate)
        assertEquals(properties.coordinates, duplicate.coordinates)
        assertFalse(properties.coordinates === duplicate.coordinates)

        assertEquals(newProperties, duplicate)
        assertFalse(newProperties === duplicate)
        assertEquals(newProperties.coordinates, duplicate.coordinates)
        assertFalse(newProperties.coordinates === duplicate.coordinates)

        // Dummy tests
        duplicate = properties.duplicate()
        duplicate.maxCommuteTime = "modifiedField"
        assertNotEquals(properties, duplicate)

        duplicate = properties.duplicate()
        duplicate.where = "modifiedField"
        assertNotEquals(properties, duplicate)

        duplicate = properties.duplicate()
        duplicate.coordinates.longitude = 6849.0
        assertNotEquals(properties, duplicate)
    }

}
