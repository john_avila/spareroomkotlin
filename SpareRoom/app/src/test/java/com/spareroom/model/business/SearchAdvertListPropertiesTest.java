package com.spareroom.model.business;

import org.junit.Test;

import static org.junit.Assert.assertNull;

public class SearchAdvertListPropertiesTest {

    @Test
    public void availableFrom_whenWanted_andNull_thenNull() {
        test_whenNull_thenNull(new SearchAdvertListPropertiesWanted());
    }

    @Test
    public void availableFrom_whenOffered_andNull_thenNull() {
        test_whenNull_thenNull(new SearchAdvertListPropertiesOffered());
    }

    private void test_whenNull_thenNull(SearchAdvertListProperties properties) {
        properties.setAvailableFrom(null);
        assertNull(properties.getAvailableFrom());
    }

}
