package com.spareroom.model.business

import com.spareroom.integration.business.SearchAdvertListParametersUtil
import org.junit.Assert.*
import org.junit.Test

class SearchAdvertListPropertiesWantedTest {

    private val util = SearchAdvertListParametersUtil()

    @Test
    fun duplicate_shouldReturnDeepCopy() {
        val properties = util.createSearchAdvertListPropertiesWanted()
        val newProperties = util.createSearchAdvertListPropertiesWanted()
        var duplicate = properties.duplicate()

        // Testing:
        // They have the same values but don't point to the same objects
        assertEquals(properties, newProperties)
        assertFalse(properties === newProperties)

        assertEquals(properties, duplicate)
        assertFalse(properties === duplicate)

        assertEquals(newProperties, duplicate)
        assertFalse(newProperties === duplicate)

        // Dummy tests
        duplicate = properties.duplicate()
        duplicate.userId = "modifiedField"
        assertNotEquals(properties, duplicate)

        duplicate = properties.duplicate()
        duplicate.where = "modifiedField"
        assertNotEquals(properties, duplicate)
    }

}
