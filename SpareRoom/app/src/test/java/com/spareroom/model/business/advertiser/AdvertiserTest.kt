package com.spareroom.model.business.advertiser

import com.spareroom.TestConstants.SMILEY_EMOTICON
import org.junit.Assert.assertEquals
import org.junit.Test

class AdvertiserTest {

    @Test
    fun lastName_shouldReturnEmptyString_whenNoFullName() {
        assertEquals("", lastName(advertiser("")))
    }

    @Test
    fun lastName_shouldReturnEmptyString_whenOnlyFirstNameProvided() {
        assertEquals("", lastName(advertiser("  Jake  ")))
        assertEquals("", lastName(advertiser("  Jake")))
        assertEquals("", lastName(advertiser("Jake")))
        assertEquals("", lastName(advertiser("Jake  ")))
        assertEquals("", lastName(advertiser("Jake $SMILEY_EMOTICON")))
    }

    @Test
    fun lastName_shouldTrimNonLetterCharacters() {
        assertEquals("Moss", lastName(advertiser("Jake Moss.")))
        assertEquals("Moss", lastName(advertiser("Jake -Moss-")))
        assertEquals("Moss", lastName(advertiser("Jake _Moss")))
        assertEquals("Moss", lastName(advertiser("Jake _Moss88")))
        assertEquals("Moss", lastName(advertiser("Jake Moss$SMILEY_EMOTICON")))
        assertEquals("Mossć", lastName(advertiser("Jake Mossć")))
    }

    @Test
    fun lastName_shouldReturnLastName_whenFirstAndLastNameProvided() {
        assertEquals("Moss", lastName(advertiser("Jake Moss")))
        assertEquals("Moss", lastName(advertiser("Jake  Moss")))
        assertEquals("Moss", lastName(advertiser("Jake Boss Moss")))
        assertEquals("Moss", lastName(advertiser("_ Moss")))
    }

    private fun advertiser(fullName: String) = WantedAdvertiser("", fullName, "", false, 0)

    private fun lastName(advertiser: Advertiser) = advertiser.lastName
}