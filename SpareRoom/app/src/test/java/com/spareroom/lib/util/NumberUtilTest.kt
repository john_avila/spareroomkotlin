package com.spareroom.lib.util

import com.spareroom.lib.util.NumberUtil.A_MILE_IN_METERS
import org.junit.Assert.assertEquals
import org.junit.Test

class NumberUtilTest {

    @Test
    fun milesToMeters_whenEqualsOrLessThanZero_thenZero() {
        assertEquals(0.0, NumberUtil.milesToMeters(-256.0), 0.0)
        assertEquals(0.0, NumberUtil.milesToMeters(-10.0), 0.0)
        assertEquals(0.0, NumberUtil.milesToMeters(0.0), 0.0)
    }

    @Test
    fun milesToMeters_whenBiggerThanZero_thenMeters() {
        assertEquals(A_MILE_IN_METERS * 425, NumberUtil.milesToMeters(425.0), 0.0)
        assertEquals(A_MILE_IN_METERS * 3651, NumberUtil.milesToMeters(3651.0), 0.0)
    }

}
