package com.spareroom.lib;

import com.spareroom.ui.NameFieldValidator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Testing for the NameTranslatorPackage
 * <p></p>
 * Created by miguel.rossi on 20/10/2016.
 */

public class NameFieldValidatorTest {
    private NameFieldValidator _validator;

    @Before
    public void setUp() {
        _validator = new NameFieldValidator();
    }

    @Test
    public void validate_null_empty() {
        String name = null;
        if (_validator.validate(name) == NameFieldValidator.EMPTY)
            assertTrue(true);
        else
            fail("Null String accepted");
    }

    @Test
    public void validate_empty_empty() {
        String name = "";
        if (_validator.validate(name) == NameFieldValidator.EMPTY)
            assertTrue(true);
        else
            fail("Empty String accepted");
    }

    @Test
    public void validate_space_empty() {
        String name = "         ";
        if (_validator.validate(name) == NameFieldValidator.EMPTY)
            assertTrue(true);
        else
            fail("String of spaces accepted");
    }

    @Test
    public void validate_specialChars_empty() {
        String name = "!@£$%^&() !@£$%^&()";
        if (_validator.validate(name) == NameFieldValidator.EMPTY)
            assertTrue(true);
        else
            fail("Special characters accepted");
    }

    @Test
    public void validate_numberName_empty() {
        String name = "12 34";
        if (_validator.validate(name) == NameFieldValidator.EMPTY)
            assertTrue(true);
        else
            fail("Numbers recognised as words");
    }

    @Test
    public void validate_oneWord_tooFewWords() {
        String name = "aaa";
        if (_validator.validate(name) == NameFieldValidator.TOO_FEW_WORDS)
            assertTrue(true);
        else
            fail("One word name accepted");
    }

    @Test
    public void validate_shortName_firstNameIsTooShort() {
        String name = "a aaa";
        if (_validator.validate(name) == NameFieldValidator.FIRST_NAME_IS_TOO_SHORT)
            assertTrue(true);
        else
            fail("First name with one character accepted");
    }

    @Test
    public void validate_alphanumericShortName_firstNameIsTooShort() {
        String name = "a1 a2";
        if (_validator.validate(name) == NameFieldValidator.FIRST_NAME_IS_TOO_SHORT)
            assertTrue(true);
        else
            fail("Numbers recognised as characters");
    }

    @Test
    public void validate_valid_valid() {
        String name = "aaa aaa";
        assertTrue(_validator.validate(name) == NameFieldValidator.VALID);
    }

    @Test
    public void validate_nonAsciiName_valid() {
        String name = "ƁƂ ñá";
        if (_validator.validate(name) == NameFieldValidator.VALID)
            assertTrue(true);
        else
            fail("Non-ascii alphabet characters not recognised");
    }

    @Test
    public void validate_threeWord_valid() {
        String name = "aa bb cc";
        if (_validator.validate(name) == NameFieldValidator.VALID)
            assertTrue(true);
        else
            fail("Three word name not accepted");
    }

}
