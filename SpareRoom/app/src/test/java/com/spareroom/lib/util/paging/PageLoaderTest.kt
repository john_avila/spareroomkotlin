package com.spareroom.lib.util.paging

import org.junit.Assert.*
import org.junit.Test

private class ItemToDisplay(override val uniqueId: String) : PageItem
private class PageWithSameSrcAndDestItems(
    override val rawItems: List<ItemToDisplay>,
    override val success: Boolean
) : Page<ItemToDisplay>

class PageLoaderTest {

    private val items = itemsToDisplay(100, startUniqueIdsWith = -20)

    @Test
    fun loadPage_shouldReturnNewListOnly_whenRefreshing_andNoCurrentItems() {
        val currentItems = emptyList<ItemToDisplay>()
        val newItems = itemsToDisplay(10)
        assertFalse(hasDuplicates(duplicates(currentItems, newItems)))

        val pageResponse = refresh(pageLoader = pageLoader(currentItems), newItems = newItems)
        assertLoad(pageResponse, hasMoreItems = false, rawItemsCount = 10, newItemsCount = 10)
    }

    @Test
    fun loadPage_shouldReturnNewListOnly_whenRefreshing_andCurrentItemsExist() {
        val currentItems = itemsToDisplay(10)
        val newItems = itemsToDisplay(10, startUniqueIdsWith = 11)
        assertFalse(hasDuplicates(duplicates(currentItems, newItems)))

        val pageResponse = refresh(pageLoader = pageLoader(currentItems), newItems = newItems)
        assertLoad(pageResponse, hasMoreItems = false, rawItemsCount = 10, newItemsCount = 10)
    }

    @Test
    fun loadPage_shouldReturnNewListOnly_whenRefreshing_andCurrentItemsExist_andNewListHasDuplicates() {
        val currentItems = itemsToDisplay(10)
        val newItems = itemsToDisplay(10)
        assertTrue(hasDuplicates(duplicates(currentItems, newItems)))

        val pageResponse = refresh(pageLoader = pageLoader(currentItems), newItems = newItems)
        assertLoad(pageResponse, hasMoreItems = false, rawItemsCount = 10, newItemsCount = 10)
    }

    @Test
    fun loadPage_shouldReturnNewListOnly_andReportItHasMoreItems_whenRefreshing_andHasMoreItems() {
        val currentItems = emptyList<ItemToDisplay>()
        val newItems = itemsToDisplay(20)
        assertFalse(hasDuplicates(duplicates(currentItems, newItems)))

        val pageResponse = refresh(pageLoader = pageLoader(currentItems), newItems = newItems)
        assertLoad(pageResponse, hasMoreItems = true, rawItemsCount = 20, newItemsCount = 20)
    }

    @Test
    fun loadPage_shouldReturnEmptyList_andReportItHasMoreItems_whenRefreshing_andResponseFailed() {
        val currentItems = emptyList<ItemToDisplay>()
        val newItems = itemsToDisplay(20)
        assertFalse(hasDuplicates(duplicates(currentItems, newItems)))

        val pageResponse = refresh(pageLoader = pageLoader(currentItems), newItems = newItems, success = false)
        assertLoad(pageResponse, hasMoreItems = true, rawItemsCount = 20, newItemsCount = 0)
    }

    @Test
    fun loadPage_shouldReturnNewListOnly_andNeverMergeWithPreviousPage_whenRefreshing() {
        val currentItems = itemsToDisplay(10)
        val newItems = itemsToDisplay(10, startUniqueIdsWith = 11)
        assertFalse(hasDuplicates(duplicates(currentItems, newItems)))

        var pageResponse = refresh(
            pageLoader = pageLoader(currentItems, mergeWithPreviousPages = true),
            newItems = newItems)
        assertLoad(pageResponse, hasMoreItems = false, rawItemsCount = 10, newItemsCount = 10)

        pageResponse = refresh(
            pageLoader = pageLoader(currentItems, mergeWithPreviousPages = false),
            newItems = newItems)
        assertLoad(pageResponse, hasMoreItems = false, rawItemsCount = 10, newItemsCount = 10)
    }

    @Test
    fun loadPage_shouldReturnNewListOnly_andWithoutDuplicates_whenLoadingNextPage_andMergeWithPreviousPageIsOff() {
        val currentItems = itemsToDisplay(10)
        val newItems = itemsToDisplay(11)
        val duplicates = duplicates(currentItems, newItems)
        assertTrue(hasDuplicates(duplicates))
        assertEquals(10, duplicates.size)

        val pageResponse = loadNextPage(
            pageLoader = pageLoader(currentItems, mergeWithPreviousPages = false),
            newItems = newItems)

        assertLoad(pageResponse, hadDuplicates = true, hasMoreItems = false, rawItemsCount = 11, newItemsCount = 1, offset = 20)
    }

    @Test
    fun loadPage_shouldReturnNewListOnly_andWithoutDuplicates_whenLoadingNextPage_andNextPageHasOnlyDuplicates_andMergeWithPreviousPageIsOff() {
        val currentItems = itemsToDisplay(20)

        val pageLoader = pageLoader(currentItems, mergeWithPreviousPages = false)

        // loads items from 20 to 40 which have ids from 0 to 20 meaning it's a page full of duplicates
        // it then fetches the next page ('while' loop goes twice)
        val pageResponse = pageLoader.loadPage(false) { offset, _ -> loadItems(items.subList(offset, offset + 20)) }

        assertLoad(pageResponse, hadDuplicates = true, hasMoreItems = true, rawItemsCount = 20, newItemsCount = 20, offset = 40)
    }

    @Test
    fun loadPage_shouldReturnMergedList_andWithoutDuplicates_whenLoadingNextPage_andMergeWithPreviousPageIsOn() {
        val currentItems = itemsToDisplay(10)
        val newItems = itemsToDisplay(10, startUniqueIdsWith = 9)
        val duplicates = duplicates(currentItems, newItems)
        assertTrue(hasDuplicates(duplicates))
        assertEquals(1, duplicates.size)

        val pageResponse = loadNextPage(
            pageLoader = pageLoader(currentItems, mergeWithPreviousPages = true),
            newItems = newItems)

        assertLoad(pageResponse, hadDuplicates = true, hasMoreItems = false, rawItemsCount = 10, newItemsCount = 19, offset = 20)
    }

    @Test
    fun loadPage_shouldNotUpdateOffset_whenLoadThrowsException() {
        val currentItems = itemsToDisplay(10)
        var newItems = itemsToDisplay(10, startUniqueIdsWith = 11)
        assertFalse(hasDuplicates(duplicates(currentItems, newItems)))

        val pageLoader = pageLoader(currentItems, mergeWithPreviousPages = false)

        // load page with success to set offset
        var pageResponse = pageLoader.loadPage(false) { _, _ -> loadItems(newItems) }
        assertLoad(pageResponse, hadDuplicates = false, hasMoreItems = false, rawItemsCount = 10, newItemsCount = 10, offset = 20)

        // load page with exception from the task (offset will not change)
        try {
            pageLoader.loadPage(false) { _, _ -> throw Exception("test exception to check offset") }
        } catch (ignored: Exception) {
        }

        // load page with success to verify offset changed twice (and not 3x)
        newItems = itemsToDisplay(10, startUniqueIdsWith = 21)
        pageResponse = pageLoader.loadPage(false) { _, _ -> loadItems(newItems) }
        assertLoad(pageResponse, hadDuplicates = false, hasMoreItems = false, rawItemsCount = 10, newItemsCount = 10, offset = 40)
    }

    @Test
    fun loadPage_shouldNotUpdateOffset_whenResponseIsNotSuccessful() {
        val currentItems = itemsToDisplay(10)
        var newItems = itemsToDisplay(10, startUniqueIdsWith = 11)
        assertFalse(hasDuplicates(duplicates(currentItems, newItems)))

        val pageLoader = pageLoader(currentItems, mergeWithPreviousPages = false)

        // load page with success to set offset
        newItems = itemsToDisplay(10, startUniqueIdsWith = 21)
        var pageResponse = pageLoader.loadPage(false) { _, _ -> loadItems(newItems, success = true) }
        assertLoad(pageResponse, hadDuplicates = false, hasMoreItems = false, rawItemsCount = 10, newItemsCount = 10, offset = 20)

        // load page with failure response (offset will not change and hasMoreItems will report true)
        newItems = itemsToDisplay(10, startUniqueIdsWith = 31)
        pageResponse = pageLoader.loadPage(false) { _, _ -> loadItems(newItems, success = false) }
        assertLoad(pageResponse, hadDuplicates = false, hasMoreItems = true, rawItemsCount = 10, newItemsCount = 0, offset = 20)
    }

    @Test(expected = Exception::class)
    fun loadPage_shouldThrowException_whenTransformationChangesListSize() {
        refresh(
            pageLoader = pageLoader(transformation = { itemsToDisplay(amount = 10) }),
            newItems = itemsToDisplay(amount = 11))
    }

    @Test(expected = Exception::class)
    fun loadPage_shouldThrowException_whenTransformationChangesListItemsIds_byIntroducingNewOnes() {
        refresh(
            pageLoader = pageLoader(transformation = { listOf(ItemToDisplay("3"), ItemToDisplay("4")) }),
            newItems = listOf(ItemToDisplay("1"), ItemToDisplay("2")))
    }

    @Test(expected = Exception::class)
    fun loadPage_shouldThrowException_whenTransformationChangesListItemsIds_byUsingSubsetOfIds() {
        refresh(
            pageLoader = pageLoader(transformation = { listOf(ItemToDisplay("1"), ItemToDisplay("1")) }),
            newItems = listOf(ItemToDisplay("1"), ItemToDisplay("2")))
    }

    private fun refresh(pageLoader: PageLoader<ItemToDisplay, ItemToDisplay, PageWithSameSrcAndDestItems>,
                        newItems: List<ItemToDisplay>, success: Boolean = true)
        : PageResponse<ItemToDisplay, PageWithSameSrcAndDestItems> {

        return pageLoader.loadPage(true) { _, _ -> loadItems(newItems, success) }
    }

    private fun loadNextPage(pageLoader: PageLoader<ItemToDisplay, ItemToDisplay, PageWithSameSrcAndDestItems>,
                             newItems: List<ItemToDisplay>)
        : PageResponse<ItemToDisplay, PageWithSameSrcAndDestItems> {

        return pageLoader.loadPage(false) { _, _ -> loadItems(newItems) }
    }

    private fun assertLoad(pageResponse: PageResponse<ItemToDisplay, PageWithSameSrcAndDestItems>,
                           hadDuplicates: Boolean = false, hasMoreItems: Boolean, newItemsCount: Int, rawItemsCount: Int, offset: Int = 0) {
        assertEquals(hasMoreItems, pageResponse.hasMoreItems)
        assertEquals(hadDuplicates, pageResponse.hadDuplicates)
        assertEquals(newItemsCount, pageResponse.items.size)
        assertEquals(rawItemsCount, pageResponse.page.rawItems.size)
        assertEquals(offset, pageResponse.offset)
    }

    private fun hasDuplicates(duplicates: List<ItemToDisplay>) = duplicates.isNotEmpty()

    private fun duplicates(oldItems: List<ItemToDisplay>, newItems: List<ItemToDisplay>): List<ItemToDisplay> {
        val duplicates = mutableListOf<ItemToDisplay>()
        for (newItem in newItems) {
            if (oldItems.find { it.uniqueId == newItem.uniqueId } != null)
                duplicates.add(newItem)
        }

        return duplicates
    }

    private fun loadItems(newItems: List<ItemToDisplay>, success: Boolean = true) = PageWithSameSrcAndDestItems(newItems, success)

    private fun itemsToDisplay(amount: Int, startUniqueIdsWith: Int = 0): List<ItemToDisplay> {
        val items = mutableListOf<ItemToDisplay>()
        for (i in 0 until amount) {
            items.add(ItemToDisplay("${i + startUniqueIdsWith}"))
        }

        return items
    }

    private fun pageLoader(currentItems: List<ItemToDisplay> = emptyList(), mergeWithPreviousPages: Boolean = false,
                           transformation: (items: List<ItemToDisplay>) -> List<ItemToDisplay> = { items -> items }) =
        PageLoader<ItemToDisplay, ItemToDisplay, PageWithSameSrcAndDestItems>(
            currentItems = currentItems,
            maxPerPage = 20,
            mergeWithPreviousPages = mergeWithPreviousPages,
            transformation = transformation)
}