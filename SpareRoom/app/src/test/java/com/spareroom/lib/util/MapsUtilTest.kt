package com.spareroom.lib.util

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.spareroom.model.business.Coordinates
import com.spareroom.ui.util.MapsUtil
import org.junit.Assert.assertEquals
import org.junit.Test

class MapsUtilTest {

    @Test
    fun createLatLngBounds_whenValuesInRange_thenCreateBounds() {
        assertEquals(
            LatLngBounds.builder().include(LatLng(0.0, 0.0)).include(LatLng(0.0, 0.0)).build(),
            MapsUtil.createLatLngBounds(Coordinates(0.0, 0.0, 0.0, 0.0)))
        assertEquals(
            LatLngBounds.builder().include(LatLng(53.35484534063605, -2.480583816726468)).include(LatLng(53.59657340354997, -2.072684787924695)).build(),
            MapsUtil.createLatLngBounds(Coordinates(-2.2766343023255815, 53.47570937209301, 0.407899028801773, 0.241728062913921)))
        assertEquals(
            LatLngBounds.builder().include(LatLng(53.35484533855, -2.4805838164000003)).include(LatLng(53.596573401449994, -2.0726847876)).build(),
            MapsUtil.createLatLngBounds(Coordinates(-2.276634302, 53.47570937, 0.4078990288, 0.2417280629)))
        assertEquals(
            LatLngBounds.builder().include(LatLng(53.38300452693, -2.19516282064)).include(LatLng(53.412995473070005, -2.14483717936)).build(),
            MapsUtil.createLatLngBounds(Coordinates(-2.17, 53.398, 0.05032564128, 0.02999094614)))
    }

    @Test
    fun createLatLngBounds_whenValuesOutOfRange_thenClampAndNormalizeValues() {
        val longitudeExcess = 35.0
        val longitudeDelta = 1.0
        assertEquals(
            LatLngBounds
                .builder()
                .include(LatLng(-90.0, -180.0 + longitudeExcess + longitudeDelta))
                .include(LatLng(-90.0, -180 + longitudeExcess - longitudeDelta))
                .build(),
            MapsUtil.createLatLngBounds(Coordinates(180 + longitudeExcess, -654.0, 2.0, longitudeDelta)))
        assertEquals(
            LatLngBounds.builder().include(LatLng(90.0, 100.0)).include(LatLng(90.0, 100.0)).build(),
            MapsUtil.createLatLngBounds(Coordinates(460.0, 91.0, 0.0, 0.0)))
        assertEquals(
            LatLngBounds.builder().include(LatLng(-90.0, 91.0)).include(LatLng(90.0, -103.0)).build(),
            MapsUtil.createLatLngBounds(Coordinates(100000524545454.0, 100453243400.0, 10000003524321434.0, 10000005452543243540.0)))
        assertEquals(
            LatLngBounds.builder().include(LatLng(90.0, 174.0)).include(LatLng(90.0, 174.0)).build(),
            MapsUtil.createLatLngBounds(Coordinates(100000524545454.0, 100453243400.0, 0.0, 0.0)))
    }

}
