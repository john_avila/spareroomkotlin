package com.spareroom

import android.app.Application
import com.spareroom.controller.SpareroomContext
import com.spareroom.integration.business.legacy.rest.apiv1.util.UpgradeSessionJSONParser
import com.spareroom.integration.webservice.rest.client.methods.*
import com.spareroom.ui.util.ConnectivityChecker
import okhttp3.OkHttpClient
import org.mockito.Mockito.mock

object TestNetworkUtils {

    fun httpGet(spareroomContext: SpareroomContext = mock(SpareroomContext::class.java),
                upgradeSessionJSONParser: UpgradeSessionJSONParser = mock(UpgradeSessionJSONParser::class.java),
                okHttpClient: OkHttpClient = mock(OkHttpClient::class.java),
                userAgent: String = HttpConstants.USER_AGENT_VALUE,
                connectivityChecker: ConnectivityChecker = mock(ConnectivityChecker::class.java),
                application: Application = mock(Application::class.java),
                url: String = ""): HttpGet {

        return HttpGet(spareroomContext, upgradeSessionJSONParser, okHttpClient, userAgent, connectivityChecker, application, url)
    }

    fun httpPost(spareroomContext: SpareroomContext = mock(SpareroomContext::class.java),
                 upgradeSessionJSONParser: UpgradeSessionJSONParser = mock(UpgradeSessionJSONParser::class.java),
                 okHttpClient: OkHttpClient = mock(OkHttpClient::class.java),
                 userAgent: String = HttpConstants.USER_AGENT_VALUE,
                 connectivityChecker: ConnectivityChecker = mock(ConnectivityChecker::class.java),
                 application: Application = mock(Application::class.java),
                 url: String = "", names: Array<String> = emptyArray(), values: Array<String> = emptyArray()): HttpPost {

        return HttpPost(spareroomContext, upgradeSessionJSONParser, okHttpClient, userAgent, connectivityChecker, application, url,
                names, values)
    }

    fun httpPostMultipart(spareroomContext: SpareroomContext = mock(SpareroomContext::class.java),
                          upgradeSessionJSONParser: UpgradeSessionJSONParser = mock(UpgradeSessionJSONParser::class.java),
                          okHttpClient: OkHttpClient = mock(OkHttpClient::class.java),
                          userAgent: String = HttpConstants.USER_AGENT_VALUE,
                          connectivityChecker: ConnectivityChecker = mock(ConnectivityChecker::class.java),
                          application: Application = mock(Application::class.java), url: String = "", names: Array<String> = emptyArray(),
                          values: Array<String> = emptyArray(), namesMultiPart: Array<String> = emptyArray(),
                          valuesMultiPart: Array<String> = emptyArray(), extension: String = ""): HttpPostMultipart {

        return HttpPostMultipart(spareroomContext, upgradeSessionJSONParser, okHttpClient, userAgent, connectivityChecker, application,
                url, names, values, namesMultiPart, valuesMultiPart, extension)
    }

}