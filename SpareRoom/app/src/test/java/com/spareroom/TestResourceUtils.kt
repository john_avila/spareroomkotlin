package com.spareroom

import org.robolectric.RuntimeEnvironment
import java.io.BufferedReader
import java.io.InputStreamReader

object TestResourceUtils {

    fun responseWithSession() = resourcesFileAsString("response_with_session.json")

    fun offeredAdvert() = resourcesFileAsString("offered_advert.json")

    fun wantedAdvert() = resourcesFileAsString("wanted_advert.json")

    fun advertsDetails() = resourcesFileAsString("advert_details.json")

    fun advertListResponseSuggestions() = resourcesFileAsString("response_advert_list_suggestions.json")

    fun advertListResponseNoMatches() = resourcesFileAsString("response_advert_list_no_matches.json")

    fun offeredAdvertListResponseSuccessful() = resourcesFileAsString("response_offered_advert_list_successful.json")

    fun wantedAdvertListResponseSuccessful() = resourcesFileAsString("response_wanted_advert_list_successful.json")

    fun offeredAdvertListResponseWarning() = resourcesFileAsString("response_offered_advert_list_warning.json")

    fun wantedAdvertListResponseWarning() = resourcesFileAsString("response_wanted_advert_list_warning.json")

    private fun resourcesFileAsString(resFileNameWithExtension: String): String {
        val inputStream = RuntimeEnvironment.application.classLoader.getResourceAsStream(resFileNameWithExtension)
        val reader = BufferedReader(InputStreamReader(inputStream))
        val sb = StringBuilder()
        for (line in reader.lines()) {
            sb.append(line)
        }
        reader.close()
        return sb.toString()
    }

}
