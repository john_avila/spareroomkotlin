package com.spareroom

import java.util.*

object TestDateUtils {

    fun date(): Calendar {
        val time = Calendar.getInstance()
        time.set(Calendar.YEAR, 2018)
        time.set(Calendar.MONTH, Calendar.JANUARY)
        time.set(Calendar.DAY_OF_YEAR, 1)
        return time
    }

    fun moveCalendarBack(calendar: Calendar, days: Int) {
        calendar.add(Calendar.DAY_OF_YEAR, -days)
    }
}