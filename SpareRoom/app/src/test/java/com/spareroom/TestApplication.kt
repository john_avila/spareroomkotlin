package com.spareroom

import android.app.Application
import com.spareroom.integration.dependency.component.ComponentRepository
import com.spareroom.integration.dependency.component.DaggerTestAppComponent
import com.spareroom.integration.dependency.module.TestAppModule

class TestApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        val appComponent = DaggerTestAppComponent.builder()
            .testAppModule(TestAppModule(this))
            .build()

        ComponentRepository.get().setAppComponent(appComponent, this)
    }
}
