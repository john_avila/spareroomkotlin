package com.spareroom

import android.os.Build
import org.mockito.Mockito
import org.robolectric.util.ReflectionHelpers

object TestUtils {

    @Suppress("UNCHECKED_CAST")
    fun <T> any(): T {
        Mockito.any<T>()
        return null as T
    }

    fun restoreCurrentApiLevel() {
        setApiLevel(TestConstants.CONFIG_SDK)
    }

    fun setApiLevel(apiLevel: Int) {
        ReflectionHelpers.setStaticField(Build.VERSION::class.java, "SDK_INT", apiLevel)
    }
}