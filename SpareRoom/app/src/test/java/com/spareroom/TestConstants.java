package com.spareroom;

import android.os.Build;

public class TestConstants {

    public static final int CONFIG_SDK = Build.VERSION_CODES.O;
    public static final char SMILEY_EMOTICON = '\uD83D';
}
