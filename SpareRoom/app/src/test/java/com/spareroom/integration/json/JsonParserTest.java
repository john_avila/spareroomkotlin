package com.spareroom.integration.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.spareroom.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class JsonParserTest {

    @Test
    public void fromJson() {
        String advert = TestResourceUtils.INSTANCE.advertsDetails();
        TestClass testClass = new JsonParser(mock(IsoDateAdapter.class)).fromJson(advert, TestClass.class);

        assertNotNull(testClass);
        assertEquals("3648465", testClass.getId());
        assertEquals("Adam Wants A Room", testClass.getTitle());
    }

    private class TestClass {

        @Expose
        @SerializedName("advert_id")
        private String id;

        @Expose
        @SerializedName("ad_title")
        private String title;

        private String getId() {
            return id;
        }

        private String getTitle() {
            return title;
        }
    }
}
