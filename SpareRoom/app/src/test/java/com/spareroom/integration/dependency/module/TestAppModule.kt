package com.spareroom.integration.dependency.module

import android.app.Application
import com.spareroom.integration.business.legacy.rest.apiv1.security.Authenticator
import com.spareroom.integration.webservice.rest.client.RestClient
import dagger.Module
import dagger.Provides
import org.mockito.Mockito.mock

@Module
class TestAppModule(application: Application,
                    private val authenticator: Authenticator = mock(Authenticator::class.java),
                    private val restClient: RestClient = mock(RestClient::class.java)
) : AppModule(application) {

    @Provides
    fun providesTestAuthenticator() = authenticator

    @Provides
    fun providesRestClient() = restClient
}