package com.spareroom.integration.business.apiv2.util;

import com.spareroom.TestApplication;
import com.spareroom.TestConstants;
import com.spareroom.integration.business.SearchAdvertListParametersUtil;
import com.spareroom.model.business.*;
import com.spareroom.model.business.SearchAdvertListProperties.*;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.*;
import com.spareroom.ui.util.AdvertUtils;
import com.spareroom.ui.util.ConnectivityChecker;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Locale;

import static com.spareroom.integration.business.SearchAdvertListParametersUtil.*;
import static com.spareroom.integration.business.apiv2.util.SearchAdvertListParametersAdapter.MAX_AGE;
import static com.spareroom.integration.business.apiv2.util.SearchAdvertListParametersAdapter.MAX_PRICE;
import static com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum.*;
import static com.spareroom.lib.util.NumberUtil.MIN_AGE;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class SearchAdvertListParametersAdapterTest {

    private final AdvertUtils advertUtils = new AdvertUtils(Locale.getDefault(), RuntimeEnvironment.application, mock(ConnectivityChecker.class));
    private final SearchAdvertListParametersAdapter adapter = new SearchAdvertListParametersAdapter(advertUtils);

    private final SearchAdvertListParametersUtil searchAdvertListParametersUtil = new SearchAdvertListParametersUtil();

    @Test
    public void adapt_whenOfferedAndParamsSet_thenSuccessful() {
        Parameters parameters = searchAdvertListParametersUtil.createFullSetOfParameters();
        SearchAdvertListPropertiesOffered properties = (SearchAdvertListPropertiesOffered) adapter.adapt(parameters, SearchType.OFFERED);

        testCommonFilledProperties(properties);

        assertEquals(DEFAULT_DAYS_OF_WEEK_AVAILABLE_VALUE, properties.getDaysOfWeekAvailable().getValue());

        assertTrue(properties.getShowRooms());
        assertTrue(properties.getShowStudios());
        assertTrue(properties.getShowWholeProperty());
        assertTrue(properties.getParking());
        assertTrue(properties.getDisabledAccess());
        assertTrue(properties.getEnSuite());
        assertTrue(properties.getBenefits());
        assertTrue(properties.getPets());
        assertTrue(properties.getLivingRoom());
        assertTrue(properties.billsInc());
        assertTrue(properties.getShortLets());
        assertTrue(properties.getVegetarians());
        assertNotNull(properties.getMinRent());
        assertEquals(DEFAULT_MONTHLY_MIN_RENT_INT_VALUE, properties.getMinRent(), 0);
        assertNotNull(properties.getMaxRent());
        assertEquals(DEFAULT_MONTHLY_MAX_RENT_INT_VALUE, properties.getMaxRent(), 0);
        assertNotNull(properties.getMilesFromMax());
        assertEquals(DEFAULT_MILES_FROM_MAX_INT_VALUE, properties.getMilesFromMax(), 0);
        assertEquals(DEFAULT_MAX_COMMUTE_TIME_VALUE, properties.getMaxCommuteTime());
        assertEquals(DEFAULT_KEYWORD_VALUE, properties.getKeyword());
        assertEquals(DEFAULT_LOCATION_TYPE_VALUE, properties.getLocationType().getValue());
        assertEquals(DEFAULT_NO_OF_ROOMS_VALUE, properties.getNoOfRooms().getValue());
        assertEquals(DEFAULT_MIN_BEDS_VALUE, properties.getMinFlatmates().getValue());
        assertEquals(DEFAULT_MAX_BEDS_VALUE, properties.maxFlatmates().getValue());
        assertEquals(DEFAULT_LANDLORD_VALUE, properties.getLandlord().getValue());
        assertEquals(DEFAULT_ROOMS_FOR_VALUE, properties.getRoomsFor().getValue());
        assertEquals(DEFAULT_POSTED_BY_VALUE, properties.getPostedBy().getValue());

        Coordinates coordinates = properties.getCoordinates();
        assertEquals(DEFAULT_LATITUDE_DOUBLE_VALUE, coordinates.getLatitude(), 0);
        assertEquals(DEFAULT_LATITUDE_DELTA_DOUBLE_VALUE, coordinates.getLatitudeDelta(), 0);
        assertEquals(DEFAULT_LONGITUDE_DOUBLE_VALUE, coordinates.getLongitude(), 0);
        assertEquals(DEFAULT_LONGITUDE_DELTA_DOUBLE_VALUE, coordinates.getLongitudeDelta(), 0);
    }

    @Test
    public void adapt_whenOfferedWeeklyRent_thenSuccessful() {
        Parameters parameters = new Parameters();
        parameters.add(PER, SearchAdvertListPropertiesOffered.PW);
        parameters.add(MIN_RENT, DEFAULT_WEEKLY_MIN_RENT_VALUE);
        parameters.add(MAX_RENT, DEFAULT_WEEKLY_MAX_RENT_VALUE);

        SearchAdvertListPropertiesOffered properties = (SearchAdvertListPropertiesOffered) adapter.adapt(parameters, SearchType.OFFERED);

        assertNotNull(properties.getMinRent());
        assertEquals(DEFAULT_MONTHLY_MIN_RENT_INT_VALUE, properties.getMinRent(), 0);
        assertNotNull(properties.getMaxRent());
        assertEquals(DEFAULT_MONTHLY_MAX_RENT_INT_VALUE, properties.getMaxRent(), 0);
    }

    @Test
    public void adapt_whenOfferedCoordinatesNull_thenZero() {
        Parameters parameters = new Parameters();
        parameters.add(LATITUDE, (String) null);
        parameters.add(LATITUDE_DELTA, (String) null);
        parameters.add(LONGITUDE, (String) null);
        parameters.add(LONGITUDE_DELTA, (String) null);

        SearchAdvertListPropertiesOffered properties = (SearchAdvertListPropertiesOffered) adapter.adapt(parameters, SearchType.OFFERED);
        Coordinates coordinates = properties.getCoordinates();

        assertEquals(DEFAULT_COORDINATES_VALUE, coordinates.getLatitude(), 0);
        assertEquals(DEFAULT_COORDINATES_VALUE, coordinates.getLatitudeDelta(), 0);
        assertEquals(DEFAULT_COORDINATES_VALUE, coordinates.getLongitude(), 0);
        assertEquals(DEFAULT_COORDINATES_VALUE, coordinates.getLongitudeDelta(), 0);
    }

    @Test
    public void adapt_whenOfferedCoordinatesEmpty_thenZero() {
        Parameters parameters = new Parameters();
        parameters.add(LATITUDE, "");
        parameters.add(LATITUDE_DELTA, "");
        parameters.add(LONGITUDE, "");
        parameters.add(LONGITUDE_DELTA, "");

        SearchAdvertListPropertiesOffered properties = (SearchAdvertListPropertiesOffered) adapter.adapt(parameters, SearchType.OFFERED);
        Coordinates coordinates = properties.getCoordinates();

        assertEquals(DEFAULT_COORDINATES_VALUE, coordinates.getLatitude(), 0);
        assertEquals(DEFAULT_COORDINATES_VALUE, coordinates.getLatitudeDelta(), 0);
        assertEquals(DEFAULT_COORDINATES_VALUE, coordinates.getLongitude(), 0);
        assertEquals(DEFAULT_COORDINATES_VALUE, coordinates.getLongitudeDelta(), 0);
    }

    @Test
    public void adapt_whenWantedAndParamsSet_thenSuccessful() {
        Parameters parameters = searchAdvertListParametersUtil.createFullSetOfParameters();
        SearchAdvertListPropertiesWanted properties = (SearchAdvertListPropertiesWanted) adapter.adapt(parameters, SearchType.WANTED);

        testCommonFilledProperties(properties);

        assertEquals(DEFAULT_DAYS_OF_WEEK_AVAILABLE_VALUE, properties.getDaysOfWeekAvailable().getValue());
        assertEquals(DEFAULT_GENDER_FILTER_VALUE, properties.getGenderFilter().getValue());
        assertEquals(DEFAULT_COUPLES_VALUE, properties.getCouples().getValue());
        assertEquals(DEFAULT_USER_ID_VALUE, properties.getUserId());
        assertEquals(DEFAULT_MAX_OTHER_AREAS_VALUE, properties.getMaxOtherAreas().getValue());
    }

    @Test
    public void adapt_whenOfferedAndEmptyParams_thenSuccessful() {
        Parameters parameters = new Parameters();
        SearchAdvertListPropertiesOffered properties = (SearchAdvertListPropertiesOffered) adapter.adapt(parameters, SearchType.OFFERED);

        testCommonEmptyProperties(properties);

        Coordinates coordinates = properties.getCoordinates();
        assertEquals(0.0, coordinates.getLatitude(), 0);
        assertEquals(0.0, coordinates.getLatitudeDelta(), 0);
        assertEquals(0.0, coordinates.getLongitude(), 0);
        assertEquals(0.0, coordinates.getLongitudeDelta(), 0);

        assertFalse(properties.getShowRooms());
        assertFalse(properties.getShowStudios());
        assertFalse(properties.getShowWholeProperty());
        assertFalse(properties.getParking());
        assertFalse(properties.getDisabledAccess());
        assertFalse(properties.getEnSuite());
        assertFalse(properties.getBenefits());
        assertFalse(properties.getPets());
        assertFalse(properties.getLivingRoom());
        assertFalse(properties.billsInc());
        assertFalse(properties.getShortLets());
        assertFalse(properties.getVegetarians());
        assertNull(properties.getMilesFromMax());
        assertEquals(LocationType.NOT_SET, properties.getLocationType());
        assertEquals(NoOfRooms.NOT_SET, properties.getNoOfRooms());
        assertEquals(MinFlatmates.NOT_SET, properties.getMinFlatmates());
        assertEquals(MaxFlatmates.NOT_SET, properties.maxFlatmates());
        assertEquals(Landlord.NOT_SET, properties.getLandlord());
        assertEquals(RoomsFor.NOT_SET, properties.getRoomsFor());
        assertEquals(PostedBy.NOT_SET, properties.getPostedBy());

    }

    @Test
    public void adapt_whenWantedAndEmptyParams_thenSuccessful() {
        Parameters parameters = new Parameters();
        SearchAdvertListPropertiesWanted properties = (SearchAdvertListPropertiesWanted) adapter.adapt(parameters, SearchType.WANTED);

        testCommonEmptyProperties(properties);

        assertNull(properties.getUserId());
    }

    @Test
    public void checkPrice_whenNull_thenNull() {
        assertNull(adapter.checkPrice(null));
    }

    @Test
    public void checkPrice_whenEqualsOrLessThanZero_thenNull() {
        assertNull(adapter.checkPrice("-15"));
        assertNull(adapter.checkPrice("0"));
    }

    @Test
    public void checkPrice_whenMoreThanZero_thenPrice() {
        Integer checkedPrice = adapter.checkPrice("15");
        assertNotNull(checkedPrice);
        assertEquals(15, checkedPrice, 0);
    }

    @Test
    public void checkPrice_whenMoreThanFiveDigits_thenMaxPrice() {
        Integer checkedPrice = adapter.checkPrice("123456");
        assertNotNull(checkedPrice);
        assertEquals(MAX_PRICE, checkedPrice, 0);
    }

    @Test
    public void checkPrice_whenPriceHaveDecimals_thenTrunkedToInteger() {
        Integer priceCloserToIntegerValue = adapter.checkPrice("12.00");
        assertNotNull(priceCloserToIntegerValue);
        assertEquals(12, priceCloserToIntegerValue, 0);

        Integer priceCloserToNextInteger = adapter.checkPrice("12.95");
        assertNotNull(priceCloserToNextInteger);
        assertEquals(12, priceCloserToNextInteger, 0);
    }

    @Test
    public void checkAge_whenNull_thenNull() {
        assertNull(adapter.checkAge(null));
    }

    @Test
    public void checkAge_whenEqualsOrLessThanZero_thenNull() {
        assertNull(adapter.checkAge("-15"));
        assertNull(adapter.checkAge("0"));
    }

    @Test
    public void checkAge_whenEqualsOrLessThanLegalAge_thenLegalAge() {
        Integer checkedAge = adapter.checkAge("1");
        assertNotNull(checkedAge);
        assertEquals(MIN_AGE, checkedAge, 0);

        checkedAge = adapter.checkAge("15");
        assertNotNull(checkedAge);
        assertEquals(MIN_AGE, checkedAge, 0);

        Integer onlyDecimals = adapter.checkAge(".654");
        assertNotNull(onlyDecimals);
        assertEquals(MIN_AGE, onlyDecimals, 0);
    }

    @Test
    public void checkAge_whenBiggerThanMaxAge_thenMaxAge() {
        Integer checkedAge = adapter.checkAge("9591");
        assertNotNull(checkedAge);
        assertEquals(MAX_AGE, checkedAge, 0);

        Integer withDecimals = adapter.checkAge("3254.654");
        assertNotNull(withDecimals);
        assertEquals(MAX_AGE, withDecimals, 0);
    }

    @Test
    public void checkAge_whenBetweenLegalAndNinetyNine_thenAge() {
        Integer checkedAge = adapter.checkAge("51");
        assertNotNull(checkedAge);
        assertEquals(51, checkedAge, 0);

        Integer withDecimals = adapter.checkAge("32.654");
        assertNotNull(withDecimals);
        assertEquals(32, withDecimals, 0);
    }

    @Test
    public void checkAge_whenNotValidFormat_thenNull() {
        assertNull(adapter.checkAge("asdf"));
        assertNull(adapter.checkAge("$^%&!£$)"));
        assertNull(adapter.checkAge(",654"));
    }

    private void testCommonFilledProperties(SearchAdvertListProperties properties) {
        assertEquals(DEFAULT_WHERE_VALUE, properties.getWhere());
        assertEquals(DEFAULT_PAGE_VALUE, properties.getPage());
        assertEquals(DEFAULT_MAX_PER_PAGE_VALUE, properties.getMaxPerPage());
        assertTrue(properties.getFeatured());
        assertEquals(DEFAULT_SORTED_BY_VALUE, properties.getSortedBy().getValue());
        assertTrue(properties.getGayShare());
        assertTrue(properties.getPhotoAdvertsOnly());
        assertFalse(properties.getIncludeHidden());
        assertNotNull(properties.getMinRent());
        assertEquals(DEFAULT_MONTHLY_MIN_RENT_INT_VALUE, properties.getMinRent(), 0);
        assertNotNull(properties.getMaxRent());
        assertEquals(DEFAULT_MONTHLY_MAX_RENT_INT_VALUE, properties.getMaxRent(), 0);
        assertNotNull(properties.getMinAge());
        assertEquals(DEFAULT_MIN_AGE_REQ_INT_VALUE, properties.getMinAge(), 0);
        assertNotNull(properties.getMaxAge());
        assertEquals(DEFAULT_MAX_AGE_REQ_INT_VALUE, properties.getMaxAge(), 0);
        assertEquals(DEFAULT_MIN_TERM_VALUE, properties.getMinTerm().getValue());
        assertEquals(DEFAULT_MAX_TERM_VALUE, properties.getMaxTerm().getValue());
        assertEquals(DEFAULT_SMOKING_VALUE, properties.getSmoking().getValue());
        assertEquals(DEFAULT_ROOM_TYPES_VALUE, properties.getRoomType().getValue());
        assertEquals(DEFAULT_SHARE_TYPE_VALUE, properties.getShareType().getValue());

        // Specific case
        properties.setAvailableFrom(DEFAULT_AVAILABLE_FROM_VALUE);
        assertEquals(DEFAULT_AVAILABLE_FROM_VALUE, properties.getAvailableFrom());
    }

    private void testCommonEmptyProperties(SearchAdvertListProperties properties) {
        assertNull(properties.getWhere());
        assertNull(properties.getPage());
        assertNull(properties.getMaxPerPage());
        assertFalse(properties.getFeatured());
        assertEquals(SortedBy.DEFAULT, properties.getSortedBy());
        assertFalse(properties.getGayShare());
        assertFalse(properties.getPhotoAdvertsOnly());
        assertTrue(properties.getIncludeHidden());
        assertNull(properties.getMinRent());
        assertNull(properties.getMaxRent());
        assertNull(properties.getMinAge());
        assertNull(properties.getMaxAge());
        assertEquals(MinTerm.NOT_SET, properties.getMinTerm());
        assertEquals(MaxTerm.NOT_SET, properties.getMaxTerm());
        assertEquals(Smoking.NOT_SET, properties.getSmoking());
        assertEquals(RoomTypes.NOT_SET, properties.getRoomType());
        assertEquals(ShareType.NOT_SET, properties.getShareType());
        assertNull(properties.getAvailableFrom());
        assertEquals(DaysOfWeek.NOT_SET, properties.getDaysOfWeekAvailable());
        assertEquals(properties.getGenderFilter(), GenderFilter.NOT_SET);
    }

}
