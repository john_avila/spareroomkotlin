package com.spareroom.integration.webservice.rest.client.methods;

import okhttp3.MediaType;

public class HttpConstants {

    public static final String USER_AGENT_VALUE = "UserAgent-Android";
    static final String URL_VALUE = "https://www.spareroom.co.uk/";
    static final String UTF_8 = "UTF-8";
    static final String USER_AGENT_HEADER = "User-Agent";
    static final String CHARSET_HEADER = "Charset";
    static final String METHOD_GET = "GET";
    static final String METHOD_POST = "POST";
    static final String HEADER_CONNECTION = "Connection";
    static final String CONNECTION_KEEP_ALIVE = "Keep-Alive";

    static final MediaType MEDIA_TYPE_FORM_URL_ENCODED = MediaType.parse("application/x-www-form-urlencoded");
    static final MediaType MEDIA_TYPE_ALL = MediaType.parse("*/*");
}
