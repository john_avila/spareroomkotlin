package com.spareroom.integration.webservice.rest.client.methods

import com.spareroom.TestNetworkUtils.httpGet
import com.spareroom.TestApplication
import com.spareroom.TestConstants
import com.spareroom.integration.webservice.rest.client.methods.HttpConstants.*
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [TestConstants.CONFIG_SDK], application = TestApplication::class)
class HttpGetTest {

    @Test
    fun getRequest_shouldBeProperlySetUp() {
        val request = httpGet(userAgent = USER_AGENT_VALUE, url = URL_VALUE).request
        assertNotNull(request)
        assertEquals(USER_AGENT_VALUE, request.header(USER_AGENT_HEADER))
        assertEquals(UTF_8, request.header(CHARSET_HEADER))
        assertEquals(URL_VALUE, request.url().toString())
        assertEquals(METHOD_GET, request.method())
        assertNull(request.body())
    }
}