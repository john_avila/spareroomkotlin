package com.spareroom.integration.webservice.rest.client.methods

import com.spareroom.TestNetworkUtils.httpPostMultipart
import com.spareroom.TestApplication
import com.spareroom.TestConstants
import com.spareroom.integration.webservice.rest.client.methods.HttpConstants.*
import okhttp3.CacheControl
import okhttp3.MultipartBody
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

private val names = arrayOf("key1", "key12")
private val values = arrayOf("value1", "value12")

private val namesMultipart = arrayOf("multipartKey1", "multipartKey12")
private val valuesMultipart = arrayOf("multipartValue1", "multipartValue12")

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [TestConstants.CONFIG_SDK], application = TestApplication::class)
class HttpPostMultipartTest {

    @Test
    fun getRequest_shouldBeProperlySetUp() {
        val request = httpPostMultipart(userAgent = USER_AGENT_VALUE, url = URL_VALUE, names = names, values = values,
                namesMultiPart = namesMultipart, valuesMultiPart = valuesMultipart).request
        assertNotNull(request)
        assertEquals(USER_AGENT_VALUE, request.header(USER_AGENT_HEADER))
        assertEquals(UTF_8, request.header(CHARSET_HEADER))
        assertEquals(CONNECTION_KEEP_ALIVE, request.header(HEADER_CONNECTION))
        assertEquals(URL_VALUE, request.url().toString())
        assertEquals(METHOD_POST, request.method())
        assertEquals(CacheControl.FORCE_NETWORK.toString(), request.cacheControl().toString())

        val requestBody = request.body()
        assertNotNull(requestBody)
        val contentType = requestBody?.contentType()
        assertNotNull(contentType)
        assertEquals(true, contentType?.toString()?.startsWith(MultipartBody.FORM.toString()))
    }

    @Test
    fun getRequestBody_shouldReturnRequestBody() {
        val requestBody = httpPostMultipart(names = names, values = values,
                namesMultiPart = namesMultipart, valuesMultiPart = valuesMultipart).requestBody

        assertNotNull(requestBody)
        assertEquals(4, requestBody.size().toLong())
    }
}
