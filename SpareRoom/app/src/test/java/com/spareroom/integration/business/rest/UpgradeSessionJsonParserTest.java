package com.spareroom.integration.business.rest;

import com.spareroom.TestApplication;
import com.spareroom.TestConstants;
import com.spareroom.integration.business.legacy.rest.apiv1.util.UpgradeSessionJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.Session;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class UpgradeSessionJsonParserTest {

    @Test(expected = NullPointerException.class)
    public void parse_shouldThrowNullPointerException_whenInputStringIsNull() throws Exception {
        UpgradeSessionJSONParser parser = new UpgradeSessionJSONParser();
        parser.parse(null);
    }

    @Test(expected = JSONException.class)
    public void parse_shouldThrowJSONException_whenInputStringIsEmpty() throws Exception {
        UpgradeSessionJSONParser parser = new UpgradeSessionJSONParser();
        parser.parse("");
    }

    @Test(expected = JSONException.class)
    public void parse_shouldThrowJSONException_whenInputStringIsNotProperJsonObject() throws Exception {
        UpgradeSessionJSONParser parser = new UpgradeSessionJSONParser();
        parser.parse("not a json object");
    }

    @Test(expected = InvalidJSONFormatException.class)
    public void parse_shouldThrowInvalidJSONFormatException_whenJsonDoesNotHaveSession() throws Exception {
        UpgradeSessionJSONParser parser = new UpgradeSessionJSONParser();
        parser.parse("{key: value}");
    }

    @Test(expected = InvalidJSONFormatException.class)
    public void parse_shouldThrowInvalidJSONFormatException_whenSessionJsonIsMalformed() throws Exception {
        UpgradeSessionJSONParser parser = new UpgradeSessionJSONParser();
        parser.parse("{session: value}");
    }

    @Test
    public void parse_ShouldReturnSession_whenSessionParsedFine() throws Exception {
        UpgradeSessionJSONParser parser = mock(UpgradeSessionJSONParser.class);
        when(parser.parse(any(String.class))).thenReturn(mock(Session.class));
        assertNotNull(parser.parse("{session: value}"));
    }
}
