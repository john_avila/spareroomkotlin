package com.spareroom.integration.dependency.component

import com.spareroom.integration.dependency.module.DaoModule
import com.spareroom.integration.dependency.module.TestAppModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [TestAppModule::class, DaoModule::class, AndroidInjectionModule::class])
interface TestAppComponent : AppComponent