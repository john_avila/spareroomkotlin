package com.spareroom.integration.business.apiv2.util;

import com.spareroom.TestApplication;
import com.spareroom.TestConstants;
import com.spareroom.integration.business.SearchAdvertListParametersUtil;
import com.spareroom.integration.business.apiv2.ApiParams;
import com.spareroom.model.business.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static com.spareroom.integration.business.SearchAdvertListParametersUtil.*;
import static com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum.*;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class SearchAdvertListPropertiesAdapterTest {

    private final SearchAdvertListParametersUtil searchAdvertListParametersUtil = new SearchAdvertListParametersUtil();

    @Test
    public void adapt_whenOfferedAndPropertiesSet_thenSuccessful() {
        SearchAdvertListPropertiesOffered properties = searchAdvertListParametersUtil.createSearchAdvertListPropertiesOffered();
        Parameters parameters = new SearchAdvertListPropertiesAdapter().adapt(properties, false);

        testCommonPropertiesSet(parameters);

        assertEquals(ApiParams.YES, parameters.get(SHOW_ROOMS));
        assertEquals(ApiParams.YES, parameters.get(SHOW_STUDIOS));
        assertEquals(ApiParams.YES, parameters.get(SHOW_WHOLE_PROPERTY));
        assertEquals(ApiParams.YES, parameters.get(PARKING));
        assertEquals(ApiParams.YES, parameters.get(DISABLED_ACCESS));
        assertEquals(ApiParams.YES, parameters.get(EN_SUITE));
        assertEquals(ApiParams.YES, parameters.get(BENEFITS));
        assertEquals(ApiParams.YES, parameters.get(PETS));
        assertEquals(ApiParams.YES, parameters.get(SHORT_LETS_CONSIDERED));
        assertEquals(ApiParams.YES, parameters.get(VEGETARIANS));
        assertEquals(SHARED, parameters.get(LIVING_ROOM));
        assertEquals(SearchAdvertListPropertiesOffered.BillsInc.YES.getValue(), parameters.get(BILLS_INC));
        assertEquals(DEFAULT_LOCATION_TYPE_VALUE, parameters.get(LOCATION_TYPE));
        assertEquals(DEFAULT_MONTHLY_MIN_RENT_STRING_VALUE, parameters.get(MIN_RENT));
        assertEquals(DEFAULT_MONTHLY_MAX_RENT_STRING_VALUE, parameters.get(MAX_RENT));
        assertEquals(DEFAULT_MAX_COMMUTE_TIME_VALUE, parameters.get(MAX_COMMUTE_TIME));
        assertEquals(DEFAULT_MILES_FROM_MAX_VALUE, parameters.get(MILES_FROM_MAX));
        assertEquals(DEFAULT_NO_OF_ROOMS_VALUE, parameters.get(NO_OF_ROOMS));
        assertEquals(DEFAULT_MIN_BEDS_VALUE, parameters.get(MIN_BEDS));
        assertEquals(DEFAULT_MAX_BEDS_VALUE, parameters.get(MAX_BEDS));
        assertEquals(DEFAULT_LANDLORD_VALUE, parameters.get(LANDLORD));
        assertEquals(DEFAULT_ROOMS_FOR_VALUE, parameters.get(ROOMS_FOR));
        assertEquals(DEFAULT_POSTED_BY_VALUE, parameters.get(POSTED_BY));
        assertEquals(DEFAULT_KEYWORD_VALUE, parameters.get(KEYWORD));
    }

    @Test
    public void adapt_whenIncludeHiddenTrue_thenHiddenUnsuitableNull() {
        SearchAdvertListPropertiesWanted searchAdvertListPropertiesWanted = new SearchAdvertListPropertiesWanted();
        searchAdvertListPropertiesWanted.setIncludeHidden(true);
        Parameters parameters = new SearchAdvertListPropertiesAdapter().adapt(searchAdvertListPropertiesWanted, false);

        assertNull(parameters.get(HIDE_UNSUITABLE));
    }

    @Test
    public void adapt_whenOffered_andUseCoordinates_thenSuccessful() {
        SearchAdvertListPropertiesOffered properties = searchAdvertListParametersUtil.createSearchAdvertListPropertiesOffered();
        Parameters parameters = new SearchAdvertListPropertiesAdapter().adapt(properties, true);

        testCommonPropertiesSet(parameters);

        assertEquals(DEFAULT_LATITUDE_DOUBLE_VALUE, Double.parseDouble(parameters.get(LATITUDE)), 0);
        assertEquals(DEFAULT_LATITUDE_DELTA_DOUBLE_VALUE, Double.parseDouble(parameters.get(LATITUDE_DELTA)), 0);
        assertEquals(DEFAULT_LONGITUDE_DOUBLE_VALUE, Double.parseDouble(parameters.get(LONGITUDE)), 0);
        assertEquals(DEFAULT_LONGITUDE_DELTA_DOUBLE_VALUE, Double.parseDouble(parameters.get(LONGITUDE_DELTA)), 0);
    }

    @Test
    public void adapt_whenWantedAndPropertiesSet_thenSuccessful() {
        SearchAdvertListPropertiesWanted searchAdvertListPropertiesWanted = searchAdvertListParametersUtil.createSearchAdvertListPropertiesWanted();
        Parameters parameters = new SearchAdvertListPropertiesAdapter().adapt(searchAdvertListPropertiesWanted, false);

        testCommonPropertiesSet(parameters);

        assertEquals(DEFAULT_USER_ID_VALUE, parameters.get(USER_ID));
        assertEquals(DEFAULT_COUPLES_VALUE, parameters.get(COUPLES));
        assertEquals(DEFAULT_MAX_OTHER_AREAS_VALUE, parameters.get(MAX_OTHER_AREAS));
    }

    private void testCommonPropertiesSet(Parameters parameters) {
        assertEquals(DEFAULT_WHERE_VALUE, parameters.get(WHERE));
        assertEquals(DEFAULT_MONTHLY_MIN_RENT_STRING_VALUE, parameters.get(MIN_RENT));
        assertEquals(DEFAULT_MONTHLY_MAX_RENT_STRING_VALUE, parameters.get(MAX_RENT));
        assertEquals(DEFAULT_MIN_AGE_REQ_VALUE, parameters.get(MIN_AGE_REQ));
        assertEquals(DEFAULT_MAX_AGE_REQ_VALUE, parameters.get(MAX_AGE_REQ));
        assertEquals(ApiParams.YES_INT, parameters.get(FEATURED));
        assertEquals(ApiParams.YES, parameters.get(GAY_SHARE));
        assertEquals(ApiParams.YES, parameters.get(PHOTO_ADS_ONLY));
        assertEquals(ApiParams.YES_INT, parameters.get(HIDE_UNSUITABLE));
        assertEquals(DEFAULT_MAX_PER_PAGE_VALUE, parameters.get(MAX_PER_PAGE));
        assertEquals(DEFAULT_PAGE_VALUE, parameters.get(PAGE));
        assertEquals(DEFAULT_SORTED_BY_VALUE, parameters.get(SORTING));
        assertTrue(parameters.has(PER));
        assertEquals(DEFAULT_MIN_TERM_VALUE, parameters.get(MIN_TERM));
        assertEquals(DEFAULT_DAYS_OF_WEEK_AVAILABLE_VALUE, parameters.get(DAYS_OF_WEEK_AVAILABLE));
        assertEquals(DEFAULT_MAX_TERM_VALUE, parameters.get(MAX_TERM));
        assertEquals(DEFAULT_SMOKING_VALUE, parameters.get(SMOKING));
        assertEquals(DEFAULT_ROOM_TYPES_VALUE, parameters.get(ROOM_TYPES));
        assertEquals(DEFAULT_SHARE_TYPE_VALUE, parameters.get(SHARE_TYPE));
        assertEquals(DEFAULT_AVAILABLE_FROM_VALUE, parameters.get(AVAILABLE_FROM));
        assertEquals(ApiParams.YES, parameters.get(AVAILABLE_SEARCH));
        assertEquals(DEFAULT_GENDER_FILTER_VALUE, parameters.get(GENDER_FILTER));
    }

}
