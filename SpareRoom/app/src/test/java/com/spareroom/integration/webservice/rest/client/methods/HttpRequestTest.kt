package com.spareroom.integration.webservice.rest.client.methods

import android.os.Build
import com.spareroom.*
import com.spareroom.TestNetworkUtils.httpGet
import com.spareroom.TestNetworkUtils.httpPost
import com.spareroom.TestNetworkUtils.httpPostMultipart
import com.spareroom.controller.AppVersion
import com.spareroom.controller.SpareroomContext
import com.spareroom.integration.business.legacy.rest.apiv1.util.UpgradeSessionJSONParser
import com.spareroom.integration.webservice.exception.*
import com.spareroom.integration.webservice.rest.client.Tls12SocketFactory
import com.spareroom.integration.webservice.rest.client.methods.HttpConstants.MEDIA_TYPE_ALL
import com.spareroom.integration.webservice.rest.client.methods.HttpConstants.URL_VALUE
import com.spareroom.model.business.Session
import com.spareroom.model.business.SpareroomStatusCode
import okhttp3.*
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.*
import org.mockito.verification.VerificationMode
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.util.ReflectionHelpers
import java.net.SocketTimeoutException
import javax.net.ssl.SSLHandshakeException

private const val RESPONSE = "response"
private const val ERROR_MESSAGE = "errorMessage"
private const val TIMEOUT = 30000

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [TestConstants.CONFIG_SDK], application = TestApplication::class)
class HttpRequestTest {

    private val request = Request.Builder().url(URL_VALUE).build()

    @Test(expected = NetworkConnectivityException::class)
    fun execute_shouldThrowNetworkException_whenNoNetwork() {
        val mockHttpRequest = spy(httpGet())
        doThrow(NetworkConnectivityException()).`when`(mockHttpRequest).checkNetworkConnection()

        mockHttpRequest.execute()
    }

    @Test(expected = ClientErrorException::class)
    fun execute_shouldThrowClientErrorException_whenSSLHandshakeExceptionThrown() {
        executeWithException(request, SSLHandshakeException(null))
    }

    @Test(expected = ServerErrorException::class)
    fun execute_shouldThrowServerErrorException_whenSocketTimeoutExceptionThrown() {
        executeWithException(request, SocketTimeoutException())
    }

    @Test(expected = NetworkConnectivityException::class)
    fun execute_shouldThrowNetworkConnectivityException_whenUnexpectedExceptionThrown() {
        executeWithException(request, RuntimeException())
    }

    @Test(expected = ClientErrorException::class)
    fun execute_shouldThrowClientErrorException_whenResponseNotNull_andClientErrorResponseCode() {
        executeWithResponse(request, getResponse(400))
        executeWithResponse(request, getResponse(499))
    }

    @Test(expected = ServerErrorException::class)
    fun execute_shouldThrowServerErrorException_whenResponseNotNull_andServerErrorResponseCode() {
        executeWithResponse(request, getResponse(500))
        executeWithResponse(request, getResponse(599))
    }

    @Test(expected = ServerErrorException::class)
    fun execute_shouldThrowServerErrorException_whenResponseNull_andResponseCodeOk() {
        executeWithResponse(request, getResponse(200, null))
    }

    @Test
    fun execute_shouldReturnResult_whenResponseNotNull_andResponseCodeOk() {
        assertEquals(RESPONSE, executeWithResponse(request, getResponse(200)))
    }

    @Test
    fun updateSession_shouldNotSaveSession_whenResponseDoesNotContainProperSession() {
        val mockSpareroomContext = mock(SpareroomContext::class.java)
        var mockWebServer = setUpMockWebServer("{session: malformedSession}")
        var request = setUpRealRequest(mockWebServer, mockSpareroomContext)
        request.execute()

        verifyUpdateSession(mockSpareroomContext, request, times(1), times(0), times(1), times(0))

        mockWebServer.shutdown()

        mockWebServer = setUpMockWebServer("")
        request = setUpRealRequest(mockWebServer, mockSpareroomContext)
        request.execute()

        verifyUpdateSession(mockSpareroomContext, request, times(1), times(0), times(1), times(0))

        mockWebServer.shutdown()
    }

    @Test
    fun updateSession_shouldSaveSession_whenResponseContainsSession() {
        val mockSpareroomContext = mock(SpareroomContext::class.java)
        val mockWebServer = setUpMockWebServer(TestResourceUtils.responseWithSession())
        val request = setUpRealRequest(mockWebServer, mockSpareroomContext)
        request.execute()

        verifyUpdateSession(mockSpareroomContext, request, times(1), times(1), times(0), times(0))

        mockWebServer.shutdown()
    }

    @Test
    fun updateSession_shouldNotDeleteSession_whenErrorCodeDifferentThan600() {
        val mockSpareroomContext = mock(SpareroomContext::class.java)
        val mockWebServer = setUpMockWebServer(String.format("{%s : %s}", SpareroomStatusCode.CODE_RESPONSE_ID, 500))
        val request = setUpRealRequest(mockWebServer, mockSpareroomContext)
        request.execute()

        verifyUpdateSession(mockSpareroomContext, request, times(1), times(0), times(1), times(0))

        mockWebServer.shutdown()
    }

    @Test
    fun updateSession_shouldNotDeleteSession_whenResponseCodeDifferentThan600() {
        val mockSpareroomContext = mock(SpareroomContext::class.java)
        val mockWebServer = setUpMockWebServer(String.format("{%s : %s}", SpareroomStatusCode.CODE_RESPONSE_ID, 500))
        val request = setUpRealRequest(mockWebServer, mockSpareroomContext)
        request.execute()

        verifyUpdateSession(mockSpareroomContext, request, times(1), times(0), times(1), times(0))

        mockWebServer.shutdown()
    }

    @Test
    fun updateSession_shouldDeleteSession_whenErrorCodeIs600() {
        val mockSpareroomContext = mock(SpareroomContext::class.java)
        val mockWebServer =
            setUpMockWebServer(String.format("{%s : %s}", SpareroomStatusCode.CODE_ERROR_ID, SpareroomStatusCode.NOT_LOGGED_IN))
        val request = setUpRealRequest(mockWebServer, mockSpareroomContext)
        request.execute()

        verifyUpdateSession(mockSpareroomContext, request, times(1), times(0), times(1), times(1))

        mockWebServer.shutdown()
    }

    @Test
    fun updateSession_shouldDeleteSession_whenResponseCodeIs600() {
        val mockSpareroomContext = mock(SpareroomContext::class.java)
        val mockWebServer =
            setUpMockWebServer(String.format("{%s : %s}", SpareroomStatusCode.CODE_RESPONSE_ID, SpareroomStatusCode.NOT_LOGGED_IN))
        val request = setUpRealRequest(mockWebServer, mockSpareroomContext)
        request.execute()

        verifyUpdateSession(mockSpareroomContext, request, times(1), times(0), times(1), times(1))

        mockWebServer.shutdown()
    }

    @Test
    fun newClient_shouldBeProperlySetUp() {
        assertClient(httpPostMultipart(okHttpClient = OkHttpClient()))
        assertClient(httpPost(okHttpClient = OkHttpClient()))
        assertClient(httpGet(okHttpClient = OkHttpClient()))
    }

    @Test
    fun newClient_shouldNotForceEnableTls12O_startingFromLollipopMr1() {
        val currentApiLevel = Build.VERSION.SDK_INT

        for (i in Build.VERSION_CODES.LOLLIPOP_MR1..99) {
            ReflectionHelpers.setStaticField(Build.VERSION::class.java, "SDK_INT", i)
            assertTls12NotForced()
        }

        // restore current API level
        ReflectionHelpers.setStaticField(Build.VERSION::class.java, "SDK_INT", currentApiLevel)
    }

    @Test
    fun newClient_shouldForceEnableTls12_onAndroidVersion_beforeLollipopMr1() {
        val currentApiLevel = Build.VERSION.SDK_INT

        for (i in Build.VERSION_CODES.JELLY_BEAN until Build.VERSION_CODES.LOLLIPOP_MR1) {
            ReflectionHelpers.setStaticField(Build.VERSION::class.java, "SDK_INT", i)
            assertTls12Forced()
        }

        // restore current API level
        ReflectionHelpers.setStaticField(Build.VERSION::class.java, "SDK_INT", currentApiLevel)
    }

    @Test
    fun isUnregisteringFirebaseToken_shouldReturnCorrectBoolean() {
        assertIsNotUnregisteringToken(AppVersion.flavor().domain + "flatshare/push_token.pll?action=unregister")
        assertIsNotUnregisteringToken(AppVersion.flavor().domain + "/push_token.pll?action=unregister")
        assertIsNotUnregisteringToken(AppVersion.flavor().domain + "push_token.pll?action=unregister")
        assertIsNotUnregisteringToken("https://google.com/flatshare/push_token.pll?action=register")

        assertIsUnregisteringToken(AppVersion.flavor().domain + "/flatshare/push_token.pl?action=unregister")
        assertIsUnregisteringToken(AppVersion.flavor().domain + "/flatshare/push_token.pl?format=json&action=unregister")
    }

    private fun assertIsUnregisteringToken(url: String) {
        val request = httpGet(url = url)
        assertTrue(request.isUnregisteringFirebaseToken(request.request))
    }

    private fun assertIsNotUnregisteringToken(url: String) {
        val request = httpGet(url = url)
        assertFalse(request.isUnregisteringFirebaseToken(request.request))
    }

    private fun verifyUpdateSession(mockSpareroomContext: SpareroomContext, request: HttpRequest, saveSessionAttempt: VerificationMode, saveSession: VerificationMode,
                                    deleteSessionAttempt: VerificationMode, deleteSession: VerificationMode) {
        verify(request, times(1)).updateSession(any(String::class.java), any(Request::class.java))
        verify(request, saveSessionAttempt).saveSession(any(String::class.java), any(Request::class.java))
        verify(mockSpareroomContext, saveSession).saveUserSession(any(Session::class.java))
        verify(request, deleteSessionAttempt).deleteSessionIfUserLoggedOut(any(String::class.java))
        verify(mockSpareroomContext, deleteSession).deleteUserSession()
    }

    private fun setUpMockWebServer(responseBody: String): MockWebServer {
        val mockWebServer = MockWebServer()
        mockWebServer.enqueue(MockResponse().setBody(responseBody))
        mockWebServer.start()

        return mockWebServer
    }

    private fun setUpRealRequest(mockWebServer: MockWebServer, spareroomContext: SpareroomContext): HttpRequest {
        val request = spy(httpGet(spareroomContext = spareroomContext, upgradeSessionJSONParser = UpgradeSessionJSONParser(),
            okHttpClient = OkHttpClient(), url = mockWebServer.url("/testUrl/").toString()))
        doNothing().`when`(request).checkNetworkConnection()
        `when`(request.cookieJar).thenReturn(mock(CookieJar::class.java))

        return request
    }

    private fun executeWithResponse(request: Request, response: Response): String {
        val mockCall = mock(Call::class.java)
        `when`(mockCall.execute()).thenReturn(response)
        return execute(mockCall, request)
    }

    private fun executeWithException(request: Request, thrownException: Exception) {
        val mockCall = mock(Call::class.java)
        `when`(mockCall.execute()).thenThrow(thrownException)
        execute(mockCall, request)
    }

    private fun execute(mockCall: Call, request: Request): String {
        val mockOkHttpClient = mock(OkHttpClient::class.java)
        doReturn(mockCall).`when`(mockOkHttpClient).newCall(request)

        val mockHttpRequest = spy(httpGet())
        doReturn(request).`when`(mockHttpRequest).request
        doReturn(mockOkHttpClient).`when`(mockHttpRequest).newClient()

        doNothing().`when`(mockHttpRequest).checkNetworkConnection()
        doNothing().`when`(mockHttpRequest).checkNetworkConnectionAfterFailure(any(Exception::class.java))
        return mockHttpRequest.execute()
    }

    private fun getResponse(code: Int, responseBody: ResponseBody? = ResponseBody.create(MEDIA_TYPE_ALL, RESPONSE)): Response {
        return Response.Builder().code(code).request(request).protocol(Protocol.HTTP_1_1).body(responseBody).message(ERROR_MESSAGE).build()
    }

    private fun assertClient(request: HttpRequest) {
        val client = request.newClient()
        assertEquals(TIMEOUT, client.connectTimeoutMillis())
        assertEquals(TIMEOUT, client.readTimeoutMillis())
        assertEquals(TIMEOUT, client.writeTimeoutMillis())
        assertTrue(client.followRedirects())
        assertTrue(client.followSslRedirects())
        assertNotNull(client.cookieJar())
    }

    private fun assertTls12Forced() {
        val request = spy(httpGet(okHttpClient = OkHttpClient()))
        val client = request.newClient()
        verify(request, times(1)).enableTls12(any())
        val sslSocketFactory = client.sslSocketFactory()
        assertNotNull(sslSocketFactory)
        assertTrue(sslSocketFactory is Tls12SocketFactory)
    }

    private fun assertTls12NotForced() {
        val request = spy(httpGet(okHttpClient = OkHttpClient()))
        val client = request.newClient()
        verify(request, times(0)).enableTls12(any())
        val sslSocketFactory = client.sslSocketFactory()
        assertNotNull(sslSocketFactory)
        assertFalse(sslSocketFactory is Tls12SocketFactory)
    }

}
