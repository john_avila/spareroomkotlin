package com.spareroom.integration.business.apiv2;

import android.app.Application;

import com.spareroom.*;
import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.SearchAdvertListParametersUtil;
import com.spareroom.integration.business.apiv2.util.SearchAdvertListParametersAdapter;
import com.spareroom.integration.business.legacy.rest.apiv1.security.Authenticator;
import com.spareroom.integration.business.legacy.rest.apiv1.util.UpgradeSessionJSONParser;
import com.spareroom.integration.dependency.component.DependencyTestComponent;
import com.spareroom.integration.webservice.rest.client.RestClient;
import com.spareroom.integration.webservice.rest.client.methods.HttpGet;
import com.spareroom.integration.webservice.rest.client.methods.HttpRequest;
import com.spareroom.integration.webservice.rest.service.AdRestService;
import com.spareroom.model.business.*;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.BillsInc;
import com.spareroom.model.business.SearchAdvertListResponse.ResponseTypes;
import com.spareroom.ui.util.AdvertUtils;
import com.spareroom.ui.util.ConnectivityChecker;

import org.junit.*;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.net.URLEncoder;
import java.util.Locale;

import okhttp3.OkHttpClient;

import static com.spareroom.integration.business.SearchAdvertListParametersUtil.DEFAULT_WHERE_VALUE;
import static com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum.*;
import static com.spareroom.integration.business.legacy.rest.apiv1.URIBuilder.*;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = TestConstants.CONFIG_SDK, application = TestApplication.class)
public class AdvertListDaoTest {
    private final static String API_KEY_VALUE = "123";
    private final static String API_SIG_VALUE = "asdfg123456";
    private final static String ERROR_MESSAGE_DEFAULT = "";
    private final static String ERROR_MESSAGE_SUCCESSFUL = null;
    private final static String ERROR_MESSAGE_NO_MATCHES = "No matches found";
    private final static String ERROR_MESSAGE_SUGGESTIONS = "We found several possible matches for your search";
    private final static String ERROR_MESSAGE_WARNING = "Your search returned no results, try broadening your criteria";

    private IAdvertListDao advertListDao;
    private final Application mockedApplication = RuntimeEnvironment.application;

    private final DependencyTestComponent dependencyTestComponent = new DependencyTestComponent();
    private final SearchAdvertListParametersUtil searchAdvertListParametersUtil = new SearchAdvertListParametersUtil();

    private String expectedUrl;
    private String mockedResponse;

    private final AdvertUtils advertUtils = new AdvertUtils(Locale.getDefault(), mockedApplication, mock(ConnectivityChecker.class));
    private final SearchAdvertListParametersAdapter adapter = new SearchAdvertListParametersAdapter(advertUtils);

    @Before
    public void setUp() throws Exception {
        dependencyTestComponent.setUp(mockedApplication, spyRestClient(), mockAuthenticator());
        advertListDao = new AdvertListDao(new AdRestService(), mockedApplication);
    }

    @After
    public void tearDown() {
        dependencyTestComponent.clear();
    }

    @Test
    public void getAdvertList_whenOffered_andUseCoordinates_andNoMatchesFound_thenNoMatchesResponse() throws Exception {
        testNoMatchesResponse(SearchType.OFFERED, searchAdvertListParametersUtil.createFullSetOfOfferedParameters(true), true);
    }

    @Test
    public void getAdvertList_whenOffered_andNoMatchesFound_thenNoMatchesResponse() throws Exception {
        testNoMatchesResponse(SearchType.OFFERED, searchAdvertListParametersUtil.createFullSetOfOfferedParameters(false), false);
    }

    @Test
    public void getAdvertList_whenWanted_andNoMatchesFound_thenNoMatchesResponse() throws Exception {
        testNoMatchesResponse(SearchType.WANTED, searchAdvertListParametersUtil.createFullSetOfWantedParameters());
    }

    @Test
    public void getAdvertList_whenOffered_andUseCoordinates_andSuggestionList_thenSuggestionListResponse() throws Exception {
        testSuggestionListResponse(SearchType.OFFERED, searchAdvertListParametersUtil.createFullSetOfOfferedParameters(true), true);
    }

    @Test
    public void getAdvertList_whenOffered_andSuggestionList_thenSuggestionListResponse() throws Exception {
        testSuggestionListResponse(SearchType.OFFERED, searchAdvertListParametersUtil.createFullSetOfOfferedParameters(false), false);
    }

    @Test
    public void getAdvertList_whenWanted_andSuggestionList_thenSuggestionListResponse() throws Exception {
        testSuggestionListResponse(SearchType.WANTED, searchAdvertListParametersUtil.createFullSetOfWantedParameters());
    }

    @Test
    public void getAdvertList_whenFullSetOfOfferedParameters_andUseCoordinates_thenWarningResponse() throws Exception {
        mockedResponse = TestResourceUtils.INSTANCE.offeredAdvertListResponseWarning();
        testWarningResponse(SearchType.OFFERED, searchAdvertListParametersUtil.createFullSetOfOfferedParameters(true), true);
    }

    @Test
    public void getAdvertList_whenFullSetOfOfferedParameters_thenWarningResponse() throws Exception {
        mockedResponse = TestResourceUtils.INSTANCE.offeredAdvertListResponseWarning();
        testWarningResponse(SearchType.OFFERED, searchAdvertListParametersUtil.createFullSetOfOfferedParameters(false), false);
    }

    @Test
    public void getAdvertList_whenFullSetOfWantedParameters_thenWarningResponse() throws Exception {
        mockedResponse = TestResourceUtils.INSTANCE.wantedAdvertListResponseWarning();
        testWarningResponse(SearchType.WANTED, searchAdvertListParametersUtil.createFullSetOfWantedParameters());
    }

    @Test
    public void getAdvertList_whenMinSetOfOfferedParameters_andUseCoordinates_thenSuccessfulResponse() throws Exception {
        mockedResponse = TestResourceUtils.INSTANCE.offeredAdvertListResponseSuccessful();
        testSuccessfulResponse(SearchType.OFFERED, searchAdvertListParametersUtil.createMinSetOfOfferedParameters(true), true);
    }

    @Test
    public void getAdvertList_whenMinSetOfOfferedParameters_thenSuccessfulResponse() throws Exception {
        mockedResponse = TestResourceUtils.INSTANCE.offeredAdvertListResponseSuccessful();
        testSuccessfulResponse(SearchType.OFFERED, searchAdvertListParametersUtil.createMinSetOfOfferedParameters(false), false);
    }

    @Test
    public void getAdvertList_whenMinSetOfWantedParameters_thenSuccessfulResponse() throws Exception {
        mockedResponse = TestResourceUtils.INSTANCE.wantedAdvertListResponseSuccessful();
        testSuccessfulResponse(SearchType.WANTED, searchAdvertListParametersUtil.createMinSetOfWantedParameters());
    }

    private void testSuccessfulResponse(SearchType searchType, Parameters minSetOfParameters) throws Exception {
        testSuccessfulResponse(searchType, minSetOfParameters, false);
    }

    private void testSuccessfulResponse(SearchType searchType, Parameters minSetOfParameters, boolean useCoordinates) throws Exception {
        expectedUrl = createExpectedUrl(minSetOfParameters, searchType);
        SearchAdvertListResponse actualResponse =
                advertListDao.getAdvertList(adapter.adapt(minSetOfParameters, searchType), searchType, useCoordinates);

        assertEquals(ResponseTypes.SUCCESS, actualResponse.getResponseType());
        assertTrue(actualResponse.getResponse() instanceof AdBoard);
        assertTrue(((AdBoard) actualResponse.getResponse()).get_total() > 0);
        assertTrue(actualResponse.isSuccessful());
        assertFalse(actualResponse.hasError());
        assertEquals(ERROR_MESSAGE_SUCCESSFUL, actualResponse.getErrorMessage(mockedApplication, ERROR_MESSAGE_SUCCESSFUL));
        assertFalse(actualResponse.isNoConnectionException());

    }

    private void testWarningResponse(SearchType searchType, Parameters fullSetOfParameters) throws Exception {
        testWarningResponse(searchType, fullSetOfParameters, false);
    }

    private void testWarningResponse(SearchType searchType, Parameters fullSetOfParameters, boolean useCoordinates) throws Exception {
        expectedUrl = createExpectedUrl(fullSetOfParameters, searchType);
        SearchAdvertListResponse actualResponse =
                advertListDao.getAdvertList(adapter.adapt(fullSetOfParameters, searchType), searchType, useCoordinates);

        assertEquals(ResponseTypes.WARNING, actualResponse.getResponseType());
        assertTrue(actualResponse.getResponse() instanceof AdBoard);
        assertFalse(((AdBoard) actualResponse.getResponse()).get_total() > 0);
        assertTrue(actualResponse.isSuccessful());
        assertFalse(actualResponse.hasError());
        assertEquals(ERROR_MESSAGE_WARNING, actualResponse.getErrorMessage(mockedApplication, ERROR_MESSAGE_WARNING));
        assertFalse(actualResponse.isNoConnectionException());
    }

    private void testSuggestionListResponse(SearchType searchType, Parameters fullSetOfParameters) throws Exception {
        testSuggestionListResponse(searchType, fullSetOfParameters, false);
    }

    private void testSuggestionListResponse(SearchType searchType, Parameters fullSetOfParameters, boolean useCoordinates) throws Exception {
        expectedUrl = createExpectedUrl(fullSetOfParameters, searchType);
        mockedResponse = TestResourceUtils.INSTANCE.advertListResponseSuggestions();
        SearchAdvertListResponse actualResponse =
                advertListDao.getAdvertList(adapter.adapt(fullSetOfParameters, searchType), searchType, useCoordinates);

        assertEquals(ResponseTypes.SUGGESTION_LIST, actualResponse.getResponseType());
        assertTrue(actualResponse.getResponse() instanceof SuggestionList);
        assertTrue(((SuggestionList) actualResponse.getResponse()).get_suggestion().size() > 0);
        assertEquals(DEFAULT_WHERE_VALUE, ((SuggestionList) actualResponse.getResponse()).get_location());
        assertTrue(actualResponse.isSuccessful());
        assertTrue(actualResponse.hasError());
        assertEquals(ERROR_MESSAGE_SUGGESTIONS, actualResponse.getErrorMessage(mockedApplication, ERROR_MESSAGE_SUGGESTIONS));
        assertFalse(actualResponse.isNoConnectionException());
    }

    private void testNoMatchesResponse(SearchType searchType, Parameters fullSetOfParameters) throws Exception {
        testNoMatchesResponse(searchType, fullSetOfParameters, false);
    }

    private void testNoMatchesResponse(SearchType searchType, Parameters fullSetOfParameters, boolean useCoordinates) throws Exception {
        expectedUrl = createExpectedUrl(fullSetOfParameters, searchType);
        mockedResponse = TestResourceUtils.INSTANCE.advertListResponseNoMatches();
        SearchAdvertListResponse actualResponse =
                advertListDao.getAdvertList(adapter.adapt(fullSetOfParameters, searchType), searchType, useCoordinates);

        assertEquals(ResponseTypes.NO_MATCHES, actualResponse.getResponseType());
        assertTrue(actualResponse.getResponse() instanceof AdBoard);
        assertFalse(((AdBoard) actualResponse.getResponse()).get_total() > 0);
        assertTrue(actualResponse.isSuccessful());
        assertTrue(actualResponse.hasError());
        assertEquals(ERROR_MESSAGE_NO_MATCHES, actualResponse.getErrorMessage(mockedApplication, ERROR_MESSAGE_DEFAULT));
        assertFalse(actualResponse.isNoConnectionException());
    }

    private ConnectivityChecker mockConnectivityChecker() {
        ConnectivityChecker connectivityChecker = mock(ConnectivityChecker.class);
        when(connectivityChecker.isConnected()).thenReturn(true);
        return connectivityChecker;
    }

    private Authenticator mockAuthenticator() throws Exception {
        Authenticator authenticator = mock(Authenticator.class);
        when(authenticator.get_apiKey()).thenReturn(API_KEY_VALUE);
        when(authenticator.authenticate(any(Parameters.class))).thenReturn(API_SIG_VALUE);
        return authenticator;
    }

    private RestClient spyRestClient() throws Exception {
        RestClient restClient = spy(new RestClient(mock(SpareroomContext.class), mock(UpgradeSessionJSONParser.class),
                mock(OkHttpClient.class), "", mockConnectivityChecker(), mockedApplication));
        doAnswer(invocation -> {
            HttpRequest request = mock(HttpGet.class);
            assertEquals(expectedUrl, invocation.getArgument(0));
            when(request.execute()).thenReturn(mockedResponse);
            return request.execute();
        }).when(restClient).get(anyString());

        return restClient;
    }

    private String createExpectedUrl(Parameters parameters, SearchType searchType) throws Exception {

        addExpectedExtraParameters(parameters);

        StringBuilder uri = new StringBuilder(getExpectedSearchUri(searchType) + QUESTION_MARK);
        String[] parameterNames = parameters.getParameterNames();

        for (String name : parameterNames) {
            uri.append(URLEncoder.encode(name, URL_ENCODING)).append(EQUALS).append(URLEncoder.encode(parameters.get(name), URL_ENCODING)).append(AMPERSAND);
        }

        uri.append(API_KEY).append(API_KEY_VALUE).append(API_SIG).append(API_SIG_VALUE);

        return uri.toString();
    }

    private void addExpectedExtraParameters(Parameters parameters) {

        // Transforms min and max rent from "per week" to "per month"
        if (parameters.has(PER)) {
            AdvertUtils advertUtils = new AdvertUtils(Locale.getDefault(), mockedApplication, mock(ConnectivityChecker.class));
            parameters.add(PER, SearchAdvertListProperties.PCM);
            if (parameters.has(MIN_RENT))
                parameters.add(MIN_RENT, String.valueOf(advertUtils.fromPwToPcmRent(Integer.parseInt(parameters.get(MIN_RENT)))));
            if (parameters.has(MAX_RENT))
                parameters.add(MAX_RENT, String.valueOf(advertUtils.fromPwToPcmRent(Integer.parseInt(parameters.get(MAX_RENT)))));
        }

        // Enable the search with the "available_from" param
        if (parameters.has(AVAILABLE_FROM))
            parameters.add(AVAILABLE_SEARCH, ApiParams.YES);

        // We don't set any other value than "Yes" regardless it can have others
        if (parameters.has(BILLS_INC))
            parameters.add(BILLS_INC, BillsInc.YES.getValue());
    }

    private String getExpectedSearchUri(SearchType searchType) {
        return AppVersion.flavor().getDomain()
                + (searchType == SearchType.OFFERED ? AdRestService.SEARCH_OFFERED_AD : AdRestService.SEARCH_WANTED_AD);
    }

}
