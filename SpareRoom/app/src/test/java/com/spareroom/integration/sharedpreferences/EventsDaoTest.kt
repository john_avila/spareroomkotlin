package com.spareroom.integration.sharedpreferences

import android.content.Context
import com.spareroom.TestApplication
import com.spareroom.TestConstants
import com.spareroom.TestDateUtils.date
import com.spareroom.ui.util.DateUtils
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import java.text.SimpleDateFormat
import java.util.*

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [(TestConstants.CONFIG_SDK)], application = TestApplication::class)
class EventsDaoTest {

    private val app = RuntimeEnvironment.application
    private val eventsDao = EventsDao(app)
    private val prefs = app.getSharedPreferences(EventsDao.PREFS_NAME, Context.MODE_PRIVATE)
    private val dateFormatter = SimpleDateFormat("yyyy-MM-dd", Locale.US)

    @After
    fun tearDown() {
        clearPrefs()
    }

    @Test
    fun logSearchEvent_shouldProperlyUpdateSharedPreferences() {
        assertSharedPreferencesUpdatedForInt(EventsDao.SEARCH, { logSearch() })
    }

    @Test
    fun logSaveAdvertEvent_shouldProperlyUpdateSharedPreferences() {
        assertSharedPreferencesUpdatedForInt(EventsDao.SAVE_ADVERT, { logSaveAdvert() })
    }

    @Test
    fun logMessageAdvertEvent_shouldProperlyUpdateSharedPreferences() {
        assertSharedPreferencesUpdatedForInt(EventsDao.MESSAGE_ADVERT, { logMessageAdvert() })
    }

    @Test
    fun logShowInitialRatingPopupEvent_shouldProperlyUpdateSharedPreferences() {
        assertSharedPreferencesUpdatedForDate(EventsDao.INITIAL_RATING_POPUP_SHOWN_DATE, { logShowInitialRatingPopup() })
    }

    @Test
    fun logShowReminderRatingPopupEvent_shouldProperlyUpdateSharedPreferences() {
        assertSharedPreferencesUpdatedForDate(EventsDao.REMINDER_RATING_POPUP_SHOWN_DATE, { logShowReminderRatingPopup() })
    }

    @Test
    fun logRateAppEvent_shouldProperlyUpdateSharedPreferences() {
        assertSharedPreferencesUpdatedForBoolean(EventsDao.RATE_APP, { logRateApp() })
    }

    @Test
    fun logDaysOfUsage_shouldNotUpdateDaysAndTime_whenLoggedAgainOnSameDay() {
        assertEquals(0, prefsSize())

        // first usage
        logUsage(date())
        assertDaysOfUsage(prefsSize = 2, daysOfUsage = 1, lastDateOfUsage = date())

        // second usage in same day
        logUsage(date())
        assertDaysOfUsage(prefsSize = 2, daysOfUsage = 1, lastDateOfUsage = date())
    }

    @Test
    fun logDaysOfUsage_shouldNotUpdateDaysAndTime_whenLoggedFromThePast() {
        assertEquals(0, prefsSize())

        // first usage
        logUsage(date())
        assertDaysOfUsage(prefsSize = 2, daysOfUsage = 1, lastDateOfUsage = date())

        // usage from previous days e.g.: when user changes device's date
        for (i in 1..5) {
            val time = date()
            time.add(Calendar.DAY_OF_YEAR, -1)
            logUsage(time)
            assertDaysOfUsage(prefsSize = 2, daysOfUsage = 1, lastDateOfUsage = date())
        }
    }

    @Test
    fun logDaysOfUsage_shouldUpdateDaysAndTime_whenLoggedFirstTime() {
        assertEquals(0, prefsSize())

        logUsage(date())
        assertDaysOfUsage(prefsSize = 2, daysOfUsage = 1, lastDateOfUsage = date())
    }

    @Test
    fun logDaysOfUsage_shouldUpdateDaysAndTime_whenLoggedInSeparateDays() {
        assertEquals(0, prefsSize())

        // first usage
        logUsage(date())
        assertDaysOfUsage(prefsSize = 2, daysOfUsage = 1, lastDateOfUsage = date())

        // consecutive usage
        val date = date()
        var days = 0
        for (i in 1..5) {
            days++
            date.add(Calendar.DAY_OF_YEAR, 1)
            logUsage(date)
            assertDaysOfUsage(prefsSize = 2, daysOfUsage = 1 + i, lastDateOfUsage = date)
        }

        // log initial rating popup shown (+1 event - 'ratingShown')
        logShowInitialRatingPopup()
        assertDaysOfUsage(prefsSize = 3, daysOfUsage = 1 + days, lastDateOfUsage = date)

        for (i in 1..5) {
            date.add(Calendar.DAY_OF_YEAR, 1)
            logUsage(date)
            assertDaysOfUsage(prefsSize = 3, daysOfUsage = 1 + days + i, lastDateOfUsage = date)
        }
    }

    @Test
    fun logAllEvents_shouldProperlyUpdateSharedPreferences() {
        assertEquals(0, prefsSize())

        logAllEvents()

        assertEquals(8, prefsSize())
        assertEquals(1, prefs.getInt(EventsDao.SEARCH, 0))
        assertEquals(1, prefs.getInt(EventsDao.SAVE_ADVERT, 0))
        assertEquals(1, prefs.getInt(EventsDao.MESSAGE_ADVERT, 0))
        assertEquals(1, prefs.getInt(EventsDao.DAYS_OF_USAGE, 0))
        assertEquals(formattedDate(), prefs.getString(EventsDao.INITIAL_RATING_POPUP_SHOWN_DATE, ""))
        assertEquals(formattedDate(), prefs.getString(EventsDao.REMINDER_RATING_POPUP_SHOWN_DATE, ""))
        assertEquals(formattedDate(), prefs.getString(EventsDao.LAST_DATE_OF_USAGE, ""))
        assertTrue(prefs.getBoolean(EventsDao.RATE_APP, false))
    }

    @Test
    fun getEvents_shouldReturnProperEventsCount() {
        assertUserEvent(savePreference = {})
        assertUserEvent(search = 1, savePreference = { logSearch() })
        assertUserEvent(saveAdvert = 1, savePreference = { logSaveAdvert() })
        assertUserEvent(messageAdvert = 1, savePreference = { logMessageAdvert() })
        assertUserEvent(initialRatingShownDate = date(), savePreference = { logShowInitialRatingPopup() })
        assertUserEvent(reminderRatingShownDate = date(), savePreference = { logShowReminderRatingPopup() })
        assertUserEvent(appRated = true, savePreference = { logRateApp() })
    }

    private fun assertSharedPreferencesUpdatedForInt(key: String, savePreference: () -> Unit) {
        assertEquals(0, prefsSize())

        savePreference()
        assertEquals(1, prefsSize())
        assertEquals(1, prefs.getInt(key, 0))

        for (i in 0..99) {
            savePreference()
        }

        assertEquals(1, prefsSize())
        assertEquals(101, prefs.getInt(key, 0))
    }

    private fun assertSharedPreferencesUpdatedForBoolean(key: String, savePreference: () -> Unit) {
        assertEquals(0, prefsSize())

        savePreference()
        assertEquals(1, prefsSize())
        assertTrue(prefs.getBoolean(key, false))

        savePreference()
        assertEquals(1, prefsSize())
        assertTrue(prefs.getBoolean(key, false))
    }

    private fun assertSharedPreferencesUpdatedForDate(key: String, savePreference: () -> Unit) {
        assertEquals(0, prefsSize())

        savePreference()
        assertEquals(1, prefsSize())
        assertEquals(formattedDate(), prefs.getString(key, ""))

        savePreference()
        assertEquals(1, prefsSize())
        assertEquals(formattedDate(), prefs.getString(key, ""))
    }

    private fun logAllEvents() {
        logSearch()
        logSaveAdvert()
        logMessageAdvert()
        logShowInitialRatingPopup()
        logShowReminderRatingPopup()
        logRateApp()
        logUsage(date())
    }

    private fun assertUserEvent(search: Int = 0, saveAdvert: Int = 0, messageAdvert: Int = 0, daysOfUsage: Int = 0,
                                initialRatingShownDate: Calendar? = null, reminderRatingShownDate: Calendar? = null,
                                appRated: Boolean = false, savePreference: () -> Unit) {
        clearPrefs()
        savePreference()
        initialRatingShownDate?.let { DateUtils.clearTime(it) }
        reminderRatingShownDate?.let { DateUtils.clearTime(it) }
        assertEquals(UserEvents(search, saveAdvert, messageAdvert, daysOfUsage,
            initialRatingShownDate, reminderRatingShownDate, appRated), eventsDao.getEvents())
    }

    private fun clearPrefs() {
        prefs.edit().clear().apply()
    }

    private fun assertDaysOfUsage(prefsSize: Int, daysOfUsage: Int, lastDateOfUsage: Calendar) {
        assertEquals(prefsSize, prefsSize())
        assertEquals(daysOfUsage, daysOfUsage())
        assertDate(lastDateOfUsage, lastDateOfUsage())
    }

    private fun assertDate(expected: Calendar, value: Calendar) {
        assertEquals(expected.get(Calendar.YEAR), value.get(Calendar.YEAR))
        assertEquals(expected.get(Calendar.MONTH), value.get(Calendar.MONTH))
        assertEquals(expected.get(Calendar.DAY_OF_YEAR), value.get(Calendar.DAY_OF_YEAR))
    }

    private fun logSearch() {
        eventsDao.logSearchEvent()
    }

    private fun logSaveAdvert() {
        eventsDao.logSaveAdvertEvent()
    }

    private fun logMessageAdvert() {
        eventsDao.logMessageAdvertEvent()
    }

    private fun logShowInitialRatingPopup() {
        eventsDao.logShowInitialRatingPopupEvent(date())
    }

    private fun logShowReminderRatingPopup() {
        eventsDao.logShowReminderRatingPopupEvent(date())
    }

    private fun logRateApp() {
        eventsDao.logRateAppEvent()
    }

    private fun logUsage(dateOfUsage: Calendar) {
        eventsDao.logDaysOfUseEvent(dateOfUsage)
    }

    private fun lastDateOfUsage(): Calendar {
        val date = dateFormatter.parse(prefs.getString(EventsDao.LAST_DATE_OF_USAGE, formattedDate()))
        val calendar = Calendar.getInstance()
        calendar.time = date
        return calendar
    }

    private fun daysOfUsage() = prefs.getInt(EventsDao.DAYS_OF_USAGE, 0)
    private fun prefsSize() = prefs.all.size
    private fun formattedDate() = dateFormatter.format(date().time)

}
