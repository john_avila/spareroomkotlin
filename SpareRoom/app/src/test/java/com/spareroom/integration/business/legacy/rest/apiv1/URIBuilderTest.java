package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.apiv1.security.Authenticator;
import com.spareroom.model.business.Parameters;

import org.junit.Before;
import org.junit.Test;

import static com.spareroom.integration.business.legacy.rest.apiv1.URIBuilder.AMPERSAND;
import static com.spareroom.integration.business.legacy.rest.apiv1.URIBuilder.API_KEY;
import static com.spareroom.integration.business.legacy.rest.apiv1.URIBuilder.API_SIG;
import static com.spareroom.integration.business.legacy.rest.apiv1.URIBuilder.EQUALS;
import static com.spareroom.integration.business.legacy.rest.apiv1.URIBuilder.QUESTION_MARK;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class URIBuilderTest {
    private final static String BASE_URI = "https://www.spareroom.co.uk/";
    private final static String API_KEY_VALUE = "123";
    private final static String API_SIG_VALUE = "asdfg123456";
    private final static String EXPECTED_URI_END = API_KEY + API_KEY_VALUE + API_SIG + API_SIG_VALUE;

    private final static String PARAM_NAME = "param_name_";
    private final static String PARAM_VALUE = "param_value_";

    private URIBuilder uriBuilder;

    @Before
    public void setUp() throws Exception {
        Authenticator authenticator = mock(Authenticator.class);
        when(authenticator.get_apiKey()).thenReturn(API_KEY_VALUE);
        when(authenticator.authenticate(any(Parameters.class))).thenReturn(API_SIG_VALUE);
        uriBuilder = new URIBuilder(authenticator);
    }

    @Test(expected = NullPointerException.class)
    public void buildGetURI_whenParamsNull_thenNullPointerException() throws Exception {
        uriBuilder.buildGetURI(BASE_URI, null);
    }

    @Test
    public void buildGetURI_whenBaseUriNull_thenNullAddedAsString() throws Exception {
        String expectedUri = null + "?" + EXPECTED_URI_END;
        assertEquals(expectedUri, uriBuilder.buildGetURI(null, new Parameters()));
    }

    @Test
    public void buildGetURI_whenParamsEmpty() throws Exception {
        assertEquals(createExpectedUri(0), uriBuilder.buildGetURI(BASE_URI, new Parameters()));
    }

    @Test
    public void buildGetURI_whenArgsOk_thenReturnUri() throws Exception {
        assertEquals(createExpectedUri(5), uriBuilder.buildGetURI(BASE_URI, createParameters(5)));
        assertEquals(createExpectedUri(52), uriBuilder.buildGetURI(BASE_URI, createParameters(52)));
        assertEquals(createExpectedUri(1), uriBuilder.buildGetURI(BASE_URI, createParameters(1)));
        assertEquals(createExpectedUri(123), uriBuilder.buildGetURI(BASE_URI, createParameters(123)));
    }

    private String createExpectedUri(int length) {
        StringBuilder expectedUri = new StringBuilder(BASE_URI + QUESTION_MARK);
        Parameters parameters = createParameters(length);
        String parameterNames[] = parameters.getParameterNames();

        for (int i = 0; i < length; i++) {
            expectedUri
                    .append(parameterNames[i])
                    .append(EQUALS)
                    .append(parameters.get(parameterNames[i]))
                    .append(AMPERSAND);
        }
        expectedUri.append(EXPECTED_URI_END);

        return expectedUri.toString();
    }

    private Parameters createParameters(int length) {
        Parameters parameters = new Parameters();

        for (int i = 0; i < length; i++) {
            parameters.add(PARAM_NAME + i, PARAM_VALUE + i);
        }

        return parameters;
    }

}
