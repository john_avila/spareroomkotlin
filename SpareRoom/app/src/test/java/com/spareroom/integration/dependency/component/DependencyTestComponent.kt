package com.spareroom.integration.dependency.component

import android.app.Application
import com.spareroom.integration.business.legacy.rest.apiv1.security.Authenticator
import com.spareroom.integration.dependency.module.TestAppModule
import com.spareroom.integration.webservice.rest.client.RestClient
import org.mockito.Mockito.mock

class DependencyTestComponent {

    fun setUp(application: Application, restClient: RestClient = mock(RestClient::class.java),
              authenticator: Authenticator = mock(Authenticator::class.java)) {

        val appComponent = DaggerTestAppComponent.builder()
            .testAppModule(TestAppModule(application = application, restClient = restClient, authenticator = authenticator))
            .build()

        ComponentRepository.get().setAppComponent(appComponent, application)
    }

    fun clear() {
        ComponentRepository.get().setAppComponent(null, mock(Application::class.java))
    }
}
