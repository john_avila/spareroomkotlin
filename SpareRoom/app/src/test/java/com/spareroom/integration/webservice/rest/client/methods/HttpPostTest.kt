package com.spareroom.integration.webservice.rest.client.methods

import com.spareroom.TestNetworkUtils.httpPost
import com.spareroom.TestApplication
import com.spareroom.TestConstants
import com.spareroom.integration.webservice.rest.client.methods.HttpConstants.*
import okhttp3.CacheControl
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

private val names = arrayOf("key1", "key2")
private val values = arrayOf("value1", "value2")
private const val urlEncodedRequestBody = "key1=value1&key2=value2"

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [TestConstants.CONFIG_SDK], application = TestApplication::class)
class HttpPostTest {

    @Test
    fun getRequest_shouldBeProperlySetUp() {
        val request = httpPost(userAgent = USER_AGENT_VALUE, url = URL_VALUE, names = names, values = values).request
        assertNotNull(request)
        assertEquals(USER_AGENT_VALUE, request.header(USER_AGENT_HEADER))
        assertEquals(UTF_8, request.header(CHARSET_HEADER))
        assertEquals(URL_VALUE, request.url().toString())
        assertEquals(METHOD_POST, request.method())
        assertEquals(CacheControl.FORCE_NETWORK.toString(), request.cacheControl().toString())

        val requestBody = request.body()
        assertNotNull(requestBody)
        val contentType = requestBody?.contentType()
        assertNotNull(contentType)
        assertNotNull(MEDIA_TYPE_FORM_URL_ENCODED)
        assertTrue(contentType.toString().startsWith(MEDIA_TYPE_FORM_URL_ENCODED.toString()))
        assertEquals(urlEncodedRequestBody.length.toLong(), requestBody?.contentLength())
    }

    @Test
    fun getUrlEncodedRequestBody_shouldReturnCorrectString() {
        assertEquals(urlEncodedRequestBody, httpPost(names = names, values = values).urlEncodedRequestBody)
    }
}
