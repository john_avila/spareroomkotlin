package com.spareroom.integration.business;

import com.spareroom.integration.business.apiv2.ApiParams;
import com.spareroom.lib.util.EnumUtil;
import com.spareroom.model.business.*;
import com.spareroom.model.business.SearchAdvertListProperties.*;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.*;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted.Couples;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted.MaxOtherAreas;

import static com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum.*;

public class SearchAdvertListParametersUtil {
    public static final String DEFAULT_AVAILABLE_FROM_VALUE = "2017-02-05";

    private static final double WEEKS_IN_A_YEAR = 52.0;
    private static final double MONTHS_IN_A_YEAR = 12.0;
    public static final int DEFAULT_WEEKLY_MAX_RENT_VALUE = 100;
    public static final int DEFAULT_WEEKLY_MIN_RENT_VALUE = 10;
    public static final int DEFAULT_MONTHLY_MAX_RENT_INT_VALUE = (int) Math.ceil(DEFAULT_WEEKLY_MAX_RENT_VALUE * (WEEKS_IN_A_YEAR / MONTHS_IN_A_YEAR));
    public static final int DEFAULT_MONTHLY_MIN_RENT_INT_VALUE = (int) Math.ceil(DEFAULT_WEEKLY_MIN_RENT_VALUE * (WEEKS_IN_A_YEAR / MONTHS_IN_A_YEAR));
    public static final String DEFAULT_MONTHLY_MAX_RENT_STRING_VALUE = String.valueOf(DEFAULT_MONTHLY_MAX_RENT_INT_VALUE);
    public static final String DEFAULT_MONTHLY_MIN_RENT_STRING_VALUE = String.valueOf(DEFAULT_MONTHLY_MIN_RENT_INT_VALUE);

    public static final double DEFAULT_COORDINATES_VALUE = 0.0;
    public static final double DEFAULT_LATITUDE_DOUBLE_VALUE = 123.123;
    public static final double DEFAULT_LATITUDE_DELTA_DOUBLE_VALUE = 456.123;
    public static final double DEFAULT_LONGITUDE_DOUBLE_VALUE = 789.123;
    public static final double DEFAULT_LONGITUDE_DELTA_DOUBLE_VALUE = 987.123;
    private static final String DEFAULT_LATITUDE_STRING_VALUE = String.valueOf(DEFAULT_LATITUDE_DOUBLE_VALUE);
    private static final String DEFAULT_LATITUDE_DELTA_STRING_VALUE = String.valueOf(DEFAULT_LATITUDE_DELTA_DOUBLE_VALUE);
    private static final String DEFAULT_LONGITUDE_STRING_VALUE = String.valueOf(DEFAULT_LONGITUDE_DOUBLE_VALUE);
    private static final String DEFAULT_LONGITUDE_DELTA_STRING_VALUE = String.valueOf(DEFAULT_LONGITUDE_DELTA_DOUBLE_VALUE);

    public static final int DEFAULT_MILES_FROM_MAX_INT_VALUE = 15;
    public static final String DEFAULT_MILES_FROM_MAX_VALUE = String.valueOf(DEFAULT_MILES_FROM_MAX_INT_VALUE);

    public static final int DEFAULT_MIN_AGE_REQ_INT_VALUE = 18;
    public static final String DEFAULT_MIN_AGE_REQ_VALUE = String.valueOf(DEFAULT_MIN_AGE_REQ_INT_VALUE);
    public static final int DEFAULT_MAX_AGE_REQ_INT_VALUE = 99;
    public static final String DEFAULT_MAX_AGE_REQ_VALUE = String.valueOf(DEFAULT_MAX_AGE_REQ_INT_VALUE);

    public static final String DEFAULT_DAYS_OF_WEEK_AVAILABLE_VALUE = DaysOfWeek.WEEKDAYS.getValue();
    public static final String DEFAULT_GENDER_FILTER_VALUE = GenderFilter.FEMALES.getValue();
    public static final String DEFAULT_LANDLORD_VALUE = Landlord.LIVE_OUT.getValue();
    public static final String DEFAULT_LOCATION_TYPE_VALUE = LocationType.AREA.getValue();
    public static final String DEFAULT_MAX_BEDS_VALUE = MaxFlatmates.SIX.getValue();
    public static final String DEFAULT_MAX_OTHER_AREAS_VALUE = MaxOtherAreas.UP_TO_FIVE.getValue();
    public static final String DEFAULT_MAX_TERM_VALUE = MaxTerm.ELEVEN.getValue();
    public static final String DEFAULT_MIN_BEDS_VALUE = MinFlatmates.FIVE.getValue();
    public static final String DEFAULT_MIN_TERM_VALUE = MinTerm.EIGHT.getValue();
    public static final String DEFAULT_NO_OF_ROOMS_VALUE = NoOfRooms.MIN_THREE.getValue();
    public static final String DEFAULT_POSTED_BY_VALUE = PostedBy.AGENTS.getValue();
    public static final String DEFAULT_ROOM_TYPES_VALUE = RoomTypes.DOUBLE.getValue();
    public static final String DEFAULT_ROOMS_FOR_VALUE = RoomsFor.COUPLES.getValue();
    public static final String DEFAULT_SHARE_TYPE_VALUE = ShareType.PROFESSIONALS.getValue();
    public static final String DEFAULT_SMOKING_VALUE = Smoking.NO.getValue();
    public static final String DEFAULT_SORTED_BY_VALUE = SortedBy.LAST_UPDATED.getValue();
    public static final String DEFAULT_COUPLES_VALUE = Couples.YES.getValue();
    private static final String DEFAULT_BILLS_INC_VALUE = BillsInc.SOME.getValue();
    private static final String DEFAULT_FEATURED_YES_VALUE = ApiParams.YES_INT;
    private static final String DEFAULT_SORTING_VALUE = SortedBy.LAST_UPDATED.getValue();

    public static final String DEFAULT_MAX_PER_PAGE_VALUE = "200";
    public static final String DEFAULT_PAGE_VALUE = "2";
    public static final String DEFAULT_USER_ID_VALUE = "123456";
    public static final String DEFAULT_WHERE_VALUE = "Manchester";
    public static final String DEFAULT_MAX_COMMUTE_TIME_VALUE = "40";
    public static final String DEFAULT_KEYWORD_VALUE = "random default keyword value";

    public SearchAdvertListPropertiesWanted createSearchAdvertListPropertiesWanted() {
        SearchAdvertListPropertiesWanted properties = (SearchAdvertListPropertiesWanted) createCommonProperties(new SearchAdvertListPropertiesWanted());

        properties.setCouples(EnumUtil.searchItemByValue(Couples.values(), DEFAULT_COUPLES_VALUE, Couples.NOT_SET));
        properties.setMaxOtherAreas(EnumUtil.searchItemByValue(MaxOtherAreas.values(), DEFAULT_MAX_OTHER_AREAS_VALUE, MaxOtherAreas.NOT_SET));
        properties.setUserId(DEFAULT_USER_ID_VALUE);

        return properties;
    }

    public SearchAdvertListPropertiesOffered createSearchAdvertListPropertiesOffered() {
        SearchAdvertListPropertiesOffered properties = (SearchAdvertListPropertiesOffered) createCommonProperties(new SearchAdvertListPropertiesOffered());

        properties.setBenefits(true);
        properties.billsInc(true);
        properties.setDisabledAccess(true);
        properties.setEnSuite(true);
        properties.setLandlord(EnumUtil.searchItemByValue(Landlord.values(), DEFAULT_LANDLORD_VALUE, Landlord.NOT_SET));
        properties.setLivingRoom(true);
        properties.setLocationType(EnumUtil.searchItemByValue(LocationType.values(), DEFAULT_LOCATION_TYPE_VALUE, LocationType.NOT_SET));
        properties.maxFlatmates(EnumUtil.searchItemByValue(MaxFlatmates.values(), DEFAULT_MAX_BEDS_VALUE, MaxFlatmates.NOT_SET));
        properties.setMaxCommuteTime(DEFAULT_MAX_COMMUTE_TIME_VALUE);
        properties.setKeyword(DEFAULT_KEYWORD_VALUE);
        properties.setMilesFromMax(DEFAULT_MILES_FROM_MAX_INT_VALUE);
        properties.setMinFlatmates(EnumUtil.searchItemByValue(MinFlatmates.values(), DEFAULT_MIN_BEDS_VALUE, MinFlatmates.NOT_SET));
        properties.setNoOfRooms(EnumUtil.searchItemByValue(NoOfRooms.values(), DEFAULT_NO_OF_ROOMS_VALUE, NoOfRooms.NOT_SET));
        properties.setParking(true);
        properties.setPets(true);
        properties.setPostedBy(EnumUtil.searchItemByValue(PostedBy.values(), DEFAULT_POSTED_BY_VALUE, PostedBy.NOT_SET));
        properties.setRoomsFor(EnumUtil.searchItemByValue(RoomsFor.values(), DEFAULT_ROOMS_FOR_VALUE, RoomsFor.NOT_SET));
        properties.setShortLets(true);
        properties.setShowRooms(true);
        properties.setShowStudios(true);
        properties.setShowWholeProperty(true);
        properties.setVegetarians(true);

        Coordinates coordinates = new Coordinates();
        coordinates.setLatitude(DEFAULT_LATITUDE_DOUBLE_VALUE);
        coordinates.setLatitudeDelta(DEFAULT_LATITUDE_DELTA_DOUBLE_VALUE);
        coordinates.setLongitude(DEFAULT_LONGITUDE_DOUBLE_VALUE);
        coordinates.setLongitudeDelta(DEFAULT_LONGITUDE_DELTA_DOUBLE_VALUE);
        properties.setCoordinates(coordinates);

        return properties;
    }

    private SearchAdvertListProperties createCommonProperties(SearchAdvertListProperties properties) {
        properties.setAvailableFrom(DEFAULT_AVAILABLE_FROM_VALUE);
        properties.setDaysOfWeekAvailable(EnumUtil.searchItemByValue(DaysOfWeek.values(), DEFAULT_DAYS_OF_WEEK_AVAILABLE_VALUE, DaysOfWeek.NOT_SET));
        properties.setFeatured(true);
        properties.setGayShare(true);
        properties.setGenderFilter(EnumUtil.searchItemByValue(GenderFilter.values(), DEFAULT_GENDER_FILTER_VALUE, GenderFilter.NOT_SET));
        properties.setIncludeHidden(false);
        properties.setMaxAge(DEFAULT_MAX_AGE_REQ_INT_VALUE);
        properties.maxMonthlyRent(DEFAULT_MONTHLY_MAX_RENT_INT_VALUE);
        properties.setMaxPerPage(DEFAULT_MAX_PER_PAGE_VALUE);
        properties.setMaxTerm(EnumUtil.searchItemByValue(MaxTerm.values(), DEFAULT_MAX_TERM_VALUE, MaxTerm.NOT_SET));
        properties.setMinAge(DEFAULT_MIN_AGE_REQ_INT_VALUE);
        properties.minMonthlyRent(DEFAULT_MONTHLY_MIN_RENT_INT_VALUE);
        properties.setMinTerm(EnumUtil.searchItemByValue(MinTerm.values(), DEFAULT_MIN_TERM_VALUE, MinTerm.NOT_SET));
        properties.setPage(DEFAULT_PAGE_VALUE);
        properties.setPhotoAdvertsOnly(true);
        properties.setRoomType(EnumUtil.searchItemByValue(RoomTypes.values(), DEFAULT_ROOM_TYPES_VALUE, RoomTypes.NOT_SET));
        properties.setShareType(EnumUtil.searchItemByValue(ShareType.values(), DEFAULT_SHARE_TYPE_VALUE, ShareType.NOT_SET));
        properties.setSmoking(EnumUtil.searchItemByValue(Smoking.values(), DEFAULT_SMOKING_VALUE, Smoking.NOT_SET));
        properties.setSortedBy(EnumUtil.searchItemByValue(SortedBy.values(), DEFAULT_SORTED_BY_VALUE, SortedBy.DEFAULT));
        properties.setWhere(DEFAULT_WHERE_VALUE);

        return properties;
    }

    public Parameters createFullSetOfOfferedParameters(boolean useCoordinates) {
        Parameters parameters = createFullSetOfCommonParameters();

        if (useCoordinates) {
            parameters.add(LATITUDE, DEFAULT_LATITUDE_STRING_VALUE);
            parameters.add(LATITUDE_DELTA, DEFAULT_LATITUDE_DELTA_STRING_VALUE);
            parameters.add(LONGITUDE, DEFAULT_LONGITUDE_STRING_VALUE);
            parameters.add(LONGITUDE_DELTA, DEFAULT_LONGITUDE_DELTA_STRING_VALUE);
        }

        parameters.add(BENEFITS, ApiParams.YES);
        parameters.add(BILLS_INC, DEFAULT_BILLS_INC_VALUE);
        parameters.add(DISABLED_ACCESS, ApiParams.YES);
        parameters.add(EN_SUITE, ApiParams.YES);
        parameters.add(LANDLORD, DEFAULT_LANDLORD_VALUE);
        parameters.add(LIVING_ROOM, SHARED);
        parameters.add(LOCATION_TYPE, DEFAULT_LOCATION_TYPE_VALUE);
        parameters.add(MAX_BEDS, DEFAULT_MAX_BEDS_VALUE);
        parameters.add(MILES_FROM_MAX, DEFAULT_MILES_FROM_MAX_VALUE);
        parameters.add(MIN_BEDS, DEFAULT_MIN_BEDS_VALUE);
        parameters.add(NO_OF_ROOMS, DEFAULT_NO_OF_ROOMS_VALUE);
        parameters.add(PARKING, ApiParams.YES);
        parameters.add(PETS, ApiParams.YES);
        parameters.add(POSTED_BY, DEFAULT_POSTED_BY_VALUE);
        parameters.add(ROOMS_FOR, DEFAULT_ROOMS_FOR_VALUE);
        parameters.add(SHORT_LETS_CONSIDERED, ApiParams.YES);
        parameters.add(SHOW_STUDIOS, ApiParams.YES);
        parameters.add(SHOW_WHOLE_PROPERTY, ApiParams.YES);
        parameters.add(VEGETARIANS, ApiParams.YES);
        parameters.add(SHOW_ROOMS, ApiParams.YES);
        return parameters;
    }

    public Parameters createFullSetOfWantedParameters() {
        Parameters parameters = createFullSetOfCommonParameters();

        parameters.add(COUPLES, DEFAULT_COUPLES_VALUE);
        parameters.add(USER_ID, DEFAULT_USER_ID_VALUE);
        return parameters;
    }

    private Parameters createFullSetOfCommonParameters() {
        Parameters parameters = new Parameters();

        parameters.add(AVAILABLE_FROM, DEFAULT_AVAILABLE_FROM_VALUE);
        parameters.add(DAYS_OF_WEEK_AVAILABLE, DEFAULT_DAYS_OF_WEEK_AVAILABLE_VALUE);
        parameters.add(FEATURED, DEFAULT_FEATURED_YES_VALUE);
        parameters.add(GAY_SHARE, ApiParams.YES);
        parameters.add(GENDER_FILTER, DEFAULT_GENDER_FILTER_VALUE);
        parameters.add(HIDE_UNSUITABLE, ApiParams.YES_INT);
        parameters.add(MAX_AGE_REQ, DEFAULT_MAX_AGE_REQ_VALUE);
        parameters.add(MAX_PER_PAGE, DEFAULT_MAX_PER_PAGE_VALUE);
        parameters.add(MAX_RENT, DEFAULT_WEEKLY_MAX_RENT_VALUE);
        parameters.add(MAX_TERM, DEFAULT_MAX_TERM_VALUE);
        parameters.add(MIN_AGE_REQ, DEFAULT_MIN_AGE_REQ_VALUE);
        parameters.add(MIN_RENT, DEFAULT_WEEKLY_MIN_RENT_VALUE);
        parameters.add(MIN_TERM, DEFAULT_MIN_TERM_VALUE);
        parameters.add(PAGE, DEFAULT_PAGE_VALUE);
        parameters.add(PER, SearchAdvertListProperties.PW);
        parameters.add(PHOTO_ADS_ONLY, ApiParams.YES);
        parameters.add(ROOM_TYPES, DEFAULT_ROOM_TYPES_VALUE);
        parameters.add(SHARE_TYPE, DEFAULT_SHARE_TYPE_VALUE);
        parameters.add(SMOKING, DEFAULT_SMOKING_VALUE);
        parameters.add(SORTING, DEFAULT_SORTING_VALUE);
        parameters.add(WHERE, DEFAULT_WHERE_VALUE);

        return parameters;
    }

    public Parameters createMinSetOfOfferedParameters(boolean useCoordinates) {
        Parameters parameters = createMinSetOfCommonParameters();

        if (useCoordinates) {
            parameters.add(LATITUDE_DELTA, DEFAULT_LATITUDE_DELTA_STRING_VALUE);
            parameters.add(LONGITUDE, DEFAULT_LONGITUDE_STRING_VALUE);
            parameters.add(LONGITUDE_DELTA, DEFAULT_LONGITUDE_DELTA_STRING_VALUE);
            parameters.add(LATITUDE, DEFAULT_LATITUDE_STRING_VALUE);
        }

        parameters.add(SHOW_ROOMS, ApiParams.YES);
        parameters.add(SHOW_STUDIOS, ApiParams.YES);
        parameters.add(SHOW_WHOLE_PROPERTY, ApiParams.YES);
        return parameters;
    }

    public Parameters createMinSetOfWantedParameters() {
        return createMinSetOfCommonParameters();
    }

    private Parameters createMinSetOfCommonParameters() {
        Parameters parameters = new Parameters();

        parameters.add(FEATURED, DEFAULT_FEATURED_YES_VALUE);
        parameters.add(MAX_PER_PAGE, DEFAULT_MAX_PER_PAGE_VALUE);
        parameters.add(PAGE, DEFAULT_PAGE_VALUE);
        parameters.add(SORTING, DEFAULT_SORTING_VALUE);
        parameters.add(WHERE, DEFAULT_WHERE_VALUE);

        return parameters;
    }

    public Parameters createFullSetOfParameters() {
        Parameters parameters = new Parameters();

        parameters.add(BENEFITS, ApiParams.YES);
        parameters.add(BILLS_INC, DEFAULT_BILLS_INC_VALUE);
        parameters.add(COUPLES, DEFAULT_COUPLES_VALUE);
        parameters.add(DAYS_OF_WEEK_AVAILABLE, DEFAULT_DAYS_OF_WEEK_AVAILABLE_VALUE);
        parameters.add(DISABLED_ACCESS, ApiParams.YES);
        parameters.add(EN_SUITE, ApiParams.YES);
        parameters.add(FEATURED, ApiParams.YES_INT);
        parameters.add(GAY_SHARE, ApiParams.YES);
        parameters.add(GENDER_FILTER, DEFAULT_GENDER_FILTER_VALUE);
        parameters.add(HIDE_UNSUITABLE, ApiParams.YES_INT);
        parameters.add(LANDLORD, DEFAULT_LANDLORD_VALUE);
        parameters.add(LATITUDE, DEFAULT_LATITUDE_STRING_VALUE);
        parameters.add(LATITUDE_DELTA, DEFAULT_LATITUDE_DELTA_STRING_VALUE);
        parameters.add(LIVING_ROOM, ApiParams.YES);
        parameters.add(LOCATION_TYPE, DEFAULT_LOCATION_TYPE_VALUE);
        parameters.add(LONGITUDE, DEFAULT_LONGITUDE_STRING_VALUE);
        parameters.add(LONGITUDE_DELTA, DEFAULT_LONGITUDE_DELTA_STRING_VALUE);
        parameters.add(MAX_AGE_REQ, DEFAULT_MAX_AGE_REQ_VALUE);
        parameters.add(MAX_BEDS, DEFAULT_MAX_BEDS_VALUE);
        parameters.add(MAX_OTHER_AREAS, DEFAULT_MAX_OTHER_AREAS_VALUE);
        parameters.add(MAX_PER_PAGE, DEFAULT_MAX_PER_PAGE_VALUE);
        parameters.add(MAX_RENT, DEFAULT_MONTHLY_MAX_RENT_STRING_VALUE);
        parameters.add(MAX_TERM, DEFAULT_MAX_TERM_VALUE);
        parameters.add(MAX_COMMUTE_TIME, DEFAULT_MAX_COMMUTE_TIME_VALUE);
        parameters.add(KEYWORD, DEFAULT_KEYWORD_VALUE);
        parameters.add(MILES_FROM_MAX, DEFAULT_MILES_FROM_MAX_VALUE);
        parameters.add(MIN_AGE_REQ, DEFAULT_MIN_AGE_REQ_VALUE);
        parameters.add(MIN_BEDS, DEFAULT_MIN_BEDS_VALUE);
        parameters.add(MIN_RENT, DEFAULT_MONTHLY_MIN_RENT_STRING_VALUE);
        parameters.add(MIN_TERM, DEFAULT_MIN_TERM_VALUE);
        parameters.add(NO_OF_ROOMS, DEFAULT_NO_OF_ROOMS_VALUE);
        parameters.add(PAGE, DEFAULT_PAGE_VALUE);
        parameters.add(PARKING, ApiParams.YES);
        parameters.add(PETS, ApiParams.YES);
        parameters.add(PHOTO_ADS_ONLY, ApiParams.YES);
        parameters.add(POSTED_BY, DEFAULT_POSTED_BY_VALUE);
        parameters.add(ROOM_TYPES, DEFAULT_ROOM_TYPES_VALUE);
        parameters.add(ROOMS_FOR, DEFAULT_ROOMS_FOR_VALUE);
        parameters.add(SHARE_TYPE, DEFAULT_SHARE_TYPE_VALUE);
        parameters.add(SHORT_LETS_CONSIDERED, ApiParams.YES);
        parameters.add(SHOW_ROOMS, ApiParams.YES);
        parameters.add(SHOW_STUDIOS, ApiParams.YES);
        parameters.add(SHOW_WHOLE_PROPERTY, ApiParams.YES);
        parameters.add(SMOKING, DEFAULT_SMOKING_VALUE);
        parameters.add(SORTING, DEFAULT_SORTED_BY_VALUE);
        parameters.add(USER_ID, DEFAULT_USER_ID_VALUE);
        parameters.add(VEGETARIANS, ApiParams.YES);
        parameters.add(WHERE, DEFAULT_WHERE_VALUE);

        return parameters;
    }

}
