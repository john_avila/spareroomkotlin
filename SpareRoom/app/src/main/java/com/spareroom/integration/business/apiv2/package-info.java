/**
 * APIv2 Factory and data accessors. APIv2 communicates with the server's API in a way where most of
 * business objects have a data accessor, and this one will request the data to the suitable
 * endpoints in order to get an ideal business structure. The design is done from business classes
 * to API endpoints (which may not match the business design).
 */
package com.spareroom.integration.business.apiv2;