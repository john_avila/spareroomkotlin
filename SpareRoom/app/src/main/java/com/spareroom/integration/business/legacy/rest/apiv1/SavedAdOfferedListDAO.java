package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.ISavedAdListDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.SavedAdOfferedListParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.service.AdRestService;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SearchType;

import javax.inject.Inject;

public class SavedAdOfferedListDAO implements ISavedAdListDAO {

    private final AdRestService adRestService;
    private final SavedAdOfferedListParser parser;

    @Inject
    public SavedAdOfferedListDAO(AdRestService adRestService, SavedAdOfferedListParser parser) {
        this.parser = parser;
        this.adRestService = adRestService;
    }

    public Object read(Parameters parameters) throws AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException {

        parameters.add("filter", "shortlist");
        parameters.add("hide_unsuitable", "1");

        String response = adRestService.getAdverts(parameters, SearchType.OFFERED);

        return parser.parse(response);
    }
}
