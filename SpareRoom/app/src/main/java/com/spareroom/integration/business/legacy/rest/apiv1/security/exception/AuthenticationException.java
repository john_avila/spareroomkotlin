package com.spareroom.integration.business.legacy.rest.apiv1.security.exception;

public class AuthenticationException extends Exception {

    private static final long serialVersionUID = -4615081067327149413L;

    public AuthenticationException() {
        super();
    }

    public AuthenticationException(String detailMessage) {
        super(detailMessage);
    }

    public AuthenticationException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public AuthenticationException(Throwable throwable) {
        super(throwable);
    }
}
