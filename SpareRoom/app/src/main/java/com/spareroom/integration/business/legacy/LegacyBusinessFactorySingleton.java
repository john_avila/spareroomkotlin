package com.spareroom.integration.business.legacy;

import com.spareroom.integration.business.legacy.rest.apiv1.RestAPIv1Factory;

/**
 * Singleton for the Factory that communicates with SpareRoom
 */
public class LegacyBusinessFactorySingleton {
    private static ILegacyBusinessAbstractFactory __instance;

    private LegacyBusinessFactorySingleton() {
    }

    /**
     * Gets an Instance of the Factory that communicates with SpareRoom
     *
     * @return the Factory that communicates with SpareRoom
     */
    public static synchronized ILegacyBusinessAbstractFactory getInstance() {
        if (__instance == null)
            __instance = new RestAPIv1Factory();
        return __instance;
    }
}
