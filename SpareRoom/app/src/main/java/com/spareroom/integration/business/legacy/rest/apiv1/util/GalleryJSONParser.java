package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.Picture;
import com.spareroom.model.business.PictureGallery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

public class GalleryJSONParser {
    /**
     * Parses a offered ad
     *
     * @return AdOffered object
     * @throws JSONException if the format of the JSON String is incorrect
     */
    public PictureGallery parse(String jsonResponse) throws InvalidJSONFormatException {
        try {
            JSONObject firstNode = new JSONObject(jsonResponse);

            PictureGallery gallery = new PictureGallery();

            LinkedList<Picture> lPictures = new LinkedList<>();

            if (firstNode.has("photos")) {
                JSONArray jsonvPhotos = firstNode.getJSONArray("photos");
                for (int i = 0; i < jsonvPhotos.length(); i++) {
                    JSONObject jsonPicture = jsonvPhotos.getJSONObject(i);
                    Picture picture = new Picture();
                    picture.setCaption(jsonPicture.getString("caption"));
                    if (picture.getCaption() != null && picture.getCaption().equals("null")) {
                        picture.setCaption(null);
                    }

                    picture.setLargeUrl(jsonPicture.getString("large_url"));
                    picture.setId(jsonPicture.getString("photo_id"));

                    lPictures.add(picture);
                }
            }

            gallery.set_gallery(lPictures);

            return gallery;

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

    }
}
