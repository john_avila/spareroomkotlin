package com.spareroom.integration.business.apiv2;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.SpareroomAbstractDaoImpl;
import com.spareroom.integration.business.apiv2.util.RegisterJsonParser;
import com.spareroom.integration.business.apiv2.util.RegisterWithFacebookResultJsonParser;
import com.spareroom.integration.business.legacy.rest.apiv1.security.Authenticator;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.*;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.service.LoginRestService;
import com.spareroom.model.business.*;
import com.spareroom.model.extra.RegisterExtra;

/**
 * DAO for the AccountCredentials business object.
 */
class RegisterFacebookTransactionDao extends SpareroomAbstractDaoImpl<AccountRegisterFormFacebook, RegisterExtra, AccountConnectionInterface> {
    private static final String PASSWORD = "password";
    private static final String PASSWORD2 = "password_again";
    private static final String IN_AGREEMENT = "inagreement";
    private static final String USER_TYPE = "usertype_new";
    private static final String MY_VERSION = "my_version";
    private static final String STEP = "step";
    private static final String STEP_COMPLETE = "complete_registration";
    private static final String PAGE_JOINED_FROM = "page_joined_from";
    private static final String FACEBOOK_ACCESS_TOKEN = "fb_access_token";
    private static final String FACEBOOK_USER_ID = "fb_user_id";
    private static final String FACEBOOK_TOKEN_EXPIRES = "fb_token_expires";
    private static final String CUSTOM_PASSWORD = "custom_password_required";
    private static final String CUSTOM_PASSWORD_YES = "Y";
    private static final String USER_TYPE_OFFERER = "haveashare";
    private static final String USER_TYPE_SEEKER = "lookingforashare";
    private static final String USER_TYPE_OFFERER_AND_SEEKER = "haveashare,lookingforashare";
    private static final String IN_AGREEMENT_YES = "Y";
    private static final String IN_AGREEMENT_NO = "N";

    private final LoginRestService loginRestService;

    RegisterFacebookTransactionDao(SpareroomContext spareroomContext) {
        super(spareroomContext);
        loginRestService = new LoginRestService();
    }

    /**
     * Creates the business object on the external system. Step 1 of the Register with Facebook
     * process.
     *
     * @return <code>Session</code> session if the log in was successful,
     * <code>SpareroomStatus</code> if there was a problem
     */
    @Override
    public Object create(AccountRegisterFormFacebook businessObject, RegisterExtra extra) throws
            UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {
        try {
            Parameters p = new Parameters();
            p.add(FACEBOOK_ACCESS_TOKEN, businessObject.getFacebookAccessToken());
            p.add(FACEBOOK_USER_ID, businessObject.getFacebookUserId());
            p.add(FACEBOOK_TOKEN_EXPIRES, businessObject.getFacebookTokenExpires());
            p.add(CUSTOM_PASSWORD, CUSTOM_PASSWORD_YES);

            p.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON);

            Authenticator authenticator = new Authenticator();
            p.add(ApiParams.API_SIGNATURE, authenticator.authenticate(p));
            p.add(ApiParams.API_KEY, authenticator.get_apiKey());

            String[] vNames = p.getParameterNames();
            String[] vValues = new String[p.size()];

            for (int i = 0; i < p.size(); i++) {
                vNames[i] = vNames[i];
                vValues[i] = p.get(vNames[i]);
            }

            String response = loginRestService.connectToFacebook(vNames, vValues);
            RegisterWithFacebookResultJsonParser parser = new RegisterWithFacebookResultJsonParser();

            Object o = parser.parse(response);

            if (o instanceof RegisterWithFacebookResult &&
                    !((RegisterWithFacebookResult) o).is_requireCustomPassword()) {
                _spareroomContext.saveUserSession(((RegisterWithFacebookResult) o).get_session());
            }
            return o;
        } catch (NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e);
        } catch (ClientErrorException | AuthenticationException e) {
            throw new InvalidArgumentException(e);
        } catch (ServerErrorException | InvalidJSONFormatException e) {
            throw new UnexpectedResultException(e);
        }
    }

    /**
     * Updates the business object. Step 2 of the Register with Facebook process.
     */
    @Override
    public Object update(AccountRegisterFormFacebook businessObject, RegisterExtra extra)
            throws UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {
        try {
            Parameters p = new Parameters();
            p.add(STEP, STEP_COMPLETE);
            p.add(MY_VERSION, extra.getVersion());
            p.add(PASSWORD, businessObject.getPassword());
            p.add(PASSWORD2, businessObject.getPassword());

            switch (businessObject.getAdvertiserType()) {
                case OFFERER:
                    p.add(USER_TYPE, USER_TYPE_OFFERER);
                    break;
                case SEEKER:
                    p.add(USER_TYPE, USER_TYPE_SEEKER);
                    break;
                case OFFERER_AND_SEEKER:
                    p.add(USER_TYPE, USER_TYPE_OFFERER_AND_SEEKER);
                    break;
            }

            p.add(IN_AGREEMENT, (businessObject.isInAgreement() ? IN_AGREEMENT_YES : IN_AGREEMENT_NO));
            if (extra.getPageJoinedFrom() != null)
                p.add(PAGE_JOINED_FROM, extra.getPageJoinedFrom());

            p.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON);

            Authenticator authenticator = new Authenticator();
            p.add(ApiParams.API_SIGNATURE, authenticator.authenticate(p));
            p.add(ApiParams.API_KEY, authenticator.get_apiKey());

            String[] vNames = p.getParameterNames();
            String[] vValues = new String[p.size()];

            for (int i = 0; i < p.size(); i++) {
                vNames[i] = vNames[i];
                vValues[i] = p.get(vNames[i]);
            }

            String response = loginRestService.connectToFacebook(vNames, vValues);
            RegisterJsonParser parser = new RegisterJsonParser();

            if (response != null) {
                Object o = parser.parse(response);

                if (o instanceof Session)
                    _spareroomContext.saveUserSession((Session) o);
            }

            return null;
        } catch (NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e);
        } catch (ClientErrorException | AuthenticationException e) {
            throw new InvalidArgumentException(e);
        } catch (ServerErrorException | InvalidJSONFormatException e) {
            throw new UnexpectedResultException(e);
        }
    }

}
