package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.ILocationDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.Point2PostcodeUnitParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.service.LocationRestService;
import com.spareroom.model.business.Parameters;

public class LocationDAO implements ILocationDAO {

    private final LocationRestService locationRestService;

    LocationDAO() {
        locationRestService = new LocationRestService();
    }

    public Object read(Parameters parameters) throws NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException {

        String response = locationRestService.getPostcodeFromCoordinates(parameters);

        Point2PostcodeUnitParser parser = new Point2PostcodeUnitParser();

        return parser.parse(response);
    }

}
