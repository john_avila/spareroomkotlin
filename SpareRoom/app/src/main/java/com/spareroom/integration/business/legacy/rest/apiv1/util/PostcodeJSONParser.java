package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.PlaceAdPostcodeArea;
import com.spareroom.model.business.PlaceAdPostcodeAreaList;
import com.spareroom.model.business.SpareroomStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

public class PostcodeJSONParser {

    public Object parse(String json) throws InvalidJSONFormatException {
        try {
            PlaceAdPostcodeAreaList postcodeAreaList = new PlaceAdPostcodeAreaList();

            JSONObject jsonRoot = new JSONObject(json);

            if (jsonRoot.has("response_type") && (jsonRoot.getString("response_type").equals("error"))) {
                SpareroomStatus im = new SpareroomStatus();
                if (jsonRoot.has("response_id"))
                    im.setCode(jsonRoot.getString("response_id"));
                if (jsonRoot.has("response"))
                    im.setMessage(jsonRoot.getString("response"));
                return im;
            }

            JSONObject jsonLocationData = jsonRoot.getJSONObject("location_data");
            String minPricePw = jsonLocationData.getString("expected_min_price_pw");
            String maxPricePw = jsonLocationData.getString("expected_max_price_pw");

            // BO for min max price
            postcodeAreaList.set_minPrice(Float.parseFloat(minPricePw));
            postcodeAreaList.set_maxPrice(Float.parseFloat(maxPricePw));

            JSONArray jsonArrayStation = jsonLocationData.getJSONArray("stations");

            // BO for stations

            LinkedList<PlaceAdPostcodeArea> lArea = new LinkedList<>();
            JSONArray jsonArrayNeighbourhoods = jsonLocationData.getJSONArray("neighbourhoods");
            for (int i = 0; i < jsonArrayNeighbourhoods.length(); i++) {
                String name = jsonArrayNeighbourhoods.getJSONObject(i).getString("neighbourhood_name");
                String id = jsonArrayNeighbourhoods.getJSONObject(i).getString("neighbourhood_id");

                PlaceAdPostcodeArea postcodeArea = new PlaceAdPostcodeArea();

                postcodeArea.set_id(id);
                postcodeArea.set_name(name);
                lArea.add(postcodeArea);
                // BO for neighbourhood
            }
            postcodeAreaList.set_lAreas(lArea);
            return postcodeAreaList;
        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }
    }

}
