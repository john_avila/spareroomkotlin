package com.spareroom.integration.json

import com.google.gson.GsonBuilder
import com.spareroom.model.business.date.IsoDate
import java.lang.reflect.Type
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class JsonParser @Inject constructor(isoDateAdapter: IsoDateAdapter) {

    private val gson = GsonBuilder()
        .excludeFieldsWithoutExposeAnnotation()
        .registerTypeAdapter(IsoDate::class.java, isoDateAdapter)
        .create()

    fun <T> fromJson(jsonString: String, classOfT: Class<T>): T = gson.fromJson(jsonString, classOfT)

    fun <T> fromJson(jsonString: String, typeOfT: Type): T = gson.fromJson(jsonString, typeOfT)
}