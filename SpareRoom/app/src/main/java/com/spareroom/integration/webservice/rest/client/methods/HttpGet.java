package com.spareroom.integration.webservice.rest.client.methods;

import android.app.Application;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.legacy.rest.apiv1.util.UpgradeSessionJSONParser;
import com.spareroom.ui.util.ConnectivityChecker;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class HttpGet extends HttpRequest {

    public HttpGet(SpareroomContext spareroomContext, UpgradeSessionJSONParser sessionParser, OkHttpClient okHttpClient,
                   String userAgent, ConnectivityChecker connectivityChecker, Application application, String url) {
        super(spareroomContext, sessionParser, okHttpClient, userAgent, connectivityChecker, application, url);
    }

    @Override
    public Request getRequest() {
        return new Request.Builder()
                .url(getUrl())
                .addHeader(HEADER_CHARSET, CHARSET_UTF8)
                .addHeader(HEADER_USER_AGENT, getUserAgent())
                .get()
                .build();
    }
}
