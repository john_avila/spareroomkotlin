package com.spareroom.integration.analytics.fabric;

public class AnalyticsTagsFabric {

    public class Event {
        static final String LOGOUT = "Logout";
        static final String RECOVER_PASSWORD = "Password recovered";
        static final String RECOVER_PASSWORD_FAILED = "Password recover - Failed";
        static final String FACEBOOK_REDIRECTION_LOGIN_TO_REGISTER = "Facebook redirection - Login to sign up";
        static final String FACEBOOK_REDIRECTION_REGISTER_TO_LOGIN = "Facebook redirection - Sign up to login";
        static final String MESSAGE_ADVERT = "Advert messaged";
        static final String CALL_ADVERT = "Advert called";
        static final String FAVOURITE = "Advert Favourited";
        static final String UN_FAVOURITE = "Advert Unfavourited";
        static final String SHARE = "Advert Shared";
        static final String REPORT = "Advert Reported";
        static final String MESSAGE = "Conversation - Manual Message Sent";
        static final String MESSAGE_AUTO_REJECT = "Conversation - Auto-Reject Message Sent";
        static final String CALL_CS = "Call Customer Services";
        static final String EMAIL_CS = "Email Customer Services";
        static final String PLACE_AD_STARTED = "Place Advert Started";
        static final String SEARCH_SAVED = "Search Saved";
        public static final String CONVERSATION_LIST_LOADED = "Conversations Loaded";
        static final String SINGLE_CONVERSATION_LOADED = "Single Conversation Loaded";
        public static final String OTHER_USER_ADVERTS_LOADED = "Other User Adverts Loaded";
        public static final String MY_ADVERTS_LOADED = "My Adverts Loaded";
        public static final String ADVERT_RENEWED = "Advert Renewed";
        public static final String ADVERT_ACTIVATED = "Advert Activated";
        public static final String ADVERT_DEACTIVATED = "Advert Deactivated";
        public static final String SINGLE_ADVERT_IN_MY_ADVERTS_LOADED = "Single Advert In My Adverts Loaded";
        static final String SEARCH_ADVERT_LIST_LOADED = "Search Advert List Loaded";
        static final String CUSTOM_TABS = "Custom Tabs";
        static final String USING_DEAD_FRAGMENT = "Using dead fragment";

        // rating popup
        static final String RATING_INITIAL_POPUP = "Rating popup - initial";
        static final String RATING_REMINDER_POPUP = "Rating popup - reminder";

        public static final String RETRIEVE_POSTCODE_BY_COORDINATES = "Retrieve post code by coordinates";
    }

    class Attribute {
        static final String KEY_REGISTER_FAILED = "Error Reason";
        static final String KEY_LOGIN_FAILED = "Error Reason";
        static final String KEY_RECOVER_PASSWORD_FAILED = "Error Reason";
        static final String KEY_REGISTER_USER_TYPE = "User Type";
        static final String KEY_FAILED_ERROR_CODE = "Error Code";

        static final String KEY_LOGIN_STATUS = "Log in Status";
        static final String VALUE_LOGIN_STATUS_LOGGED_IN = "Logged in";
        static final String VALUE_LOGIN_STATUS_LOGGED_OUT = "Logged out";

        // Register Method
        static final String VALUE_REGISTER_METHOD_SPAREROOM = "Email";
        static final String VALUE_REGISTER_METHOD_FACEBOOK = "Facebook";

        // Login Method
        static final String VALUE_LOGIN_METHOD_SPAREROOM = "Email";
        static final String VALUE_LOGIN_METHOD_FACEBOOK = "Facebook";

        // Register Errors
        static final String VALUE_REGISTER_FAILED_ERROR_INVALID_EMAIL = "Invalid Email";
        static final String VALUE_REGISTER_FAILED_ERROR_INVALID_EMAIL_LENGTH = "Invalid Password Format";
        static final String VALUE_REGISTER_FAILED_ERROR_CONFIRM_PASSWORD_FAILED = "Passwords Don't Match";
        static final String VALUE_REGISTER_FAILED_ERROR_AGREE_TERMS_N_CONDITIONS = "Not Agree With Terms and Conditions";
        static final String VALUE_REGISTER_FAILED_ERROR_USED_EMAIL = "Email Already Registered";
        static final String VALUE_REGISTER_FAILED_ERROR_MISSED_FIRST_NAME = "No First Name";
        static final String VALUE_REGISTER_FAILED_ERROR_MISSED_LAST_NAME = "No Last Name";
        static final String VALUE_REGISTER_FAILED_ERROR_MISSED_USER_TYPE = "No User Type";
        static final String VALUE_REGISTER_FAILED_OTHER_ERROR = "Other Error";
        static final String VALUE_REGISTER_FAILED_UNDEFINED_ERROR = "Undefined Error";

        // Login Errors
        static final String VALUE_LOGIN_FAILED_ERROR_NO_PASSWORD = "No Password";
        static final String VALUE_LOGIN_FAILED_ERROR_UNKNOWN_EMAIL = "Email Not Found";
        static final String VALUE_LOGIN_FAILED_ERROR_INVALID_EMAIL = "Invalid Email";
        static final String VALUE_LOGIN_FAILED_ERROR_WRONG_PASSWORD = "Invalid Password";
        static final String VALUE_LOGIN_FAILED_ERROR_ALREADY_LOGGED_IN = "Already LoggedIn";
        static final String VALUE_LOGIN_FAILED_ERROR_MISSED_EMAIL_AND_PASSWORD = "No Email Or Password";
        static final String VALUE_LOGIN_FAILED_ERROR_MISSED_EMAIL = "No Email";
        static final String VALUE_LOGIN_FAILED_OTHER_ERROR = "Other Error";
        static final String VALUE_LOGIN_FAILED_UNDEFINED_ERROR = "Undefined Error";

        // Recovering password Errors
        static final String VALUE_RECOVER_PASSWORD_FAILED_THREE_REMINDERS = "Three reminders has been sent";
        static final String VALUE_RECOVER_PASSWORD_FAILED_EMAIL = "Email not valid";
        static final String VALUE_RECOVER_PASSWORD_OTHER_ERROR = "Other Error";
        static final String VALUE_RECOVER_PASSWORD_UNDEFINED_ERROR = "Undefined Error";

        // Register - User type
        static final String VALUE_REGISTER_USER_TYPE_SEEKER_OFFERER = "Seeker Offerer";
        static final String VALUE_REGISTER_USER_TYPE_SEEKER = "Seeker";
        static final String VALUE_REGISTER_USER_TYPE_OFFERER = "Offerer";

        // Search
        static final String KEY_SEARCH_QUERY_TYPE = "Query Type";
        static final String KEY_SEARCH_METHOD = "Search Method";
        static final String KEY_SEARCH_TYPE = "Search Type";

        static final String VALUE_SEARCH_QUERY_TYPE_STRING = "Query String";
        static final String VALUE_SEARCH_QUERY_TYPE_ADVERT = "Advert Id";
        static final String VALUE_SEARCH_METHOD_RECENT = "Recent";
        static final String VALUE_SEARCH_METHOD_AUTOCOMPLETE = "Autocomplete";
        static final String VALUE_SEARCH_METHOD_MANUAL = "Return Key";
        static final String VALUE_SEARCH_METHOD_LOCATION = "Location Button";
        static final String VALUE_SEARCH_METHOD_SAVED_SEARCH = "Saved Search";

        static final String VALUE_SEARCH_TYPE_OFFERED = "Offered";
        static final String VALUE_SEARCH_TYPE_WANTED = "Wanted";

        // Message from Advert
        static final String KEY_ADVERT_TYPE = "Advert Type";
        static final String VALUE_ADVERT_TYPE_OFFERED = "Offered";
        static final String VALUE_ADVERT_TYPE_WANTED = "Wanted";

        // Message
        static final String KEY_MESSAGE_STATUS = "Delivery status";
        static final String VALUE_MESSAGE_STATUS_SUCCESS = "Sent";

        static final String DURATION = "Duration in seconds";
        static final String CUSTOM_TABS_HANDLED_PAGE = "Custom tabs handled page";

        // rating popup
        static final String RATING_BACK_BUTTON = "Back button";
        static final String RATING_OUTSIDE = "Outside popup";
        static final String RATING_MAYBE_LATER = "Maybe later";
        static final String RATING_RATE_APP = "Rate app";
        static final String RATING_DISMISS = "Dismiss";

        static final String FRAGMENT_NAME = "Fragment name";
    }

    class Ecommerce {
        static final String TYPE_UPGRADE = "Upgrade";
        static final String TYPE_EXTRA_LISTING = "Extra listing";

        static final String ATTRIBUTE_BILLING_RESULT_CODE = "Billing error code";
        static final String ATTRIBUTE_BILLING_RESULT_MESSAGE = "Billing error message";
        static final String ATTRIBUTE_UPGRADE_CONTEXT = "Upgrade context";

    }

}