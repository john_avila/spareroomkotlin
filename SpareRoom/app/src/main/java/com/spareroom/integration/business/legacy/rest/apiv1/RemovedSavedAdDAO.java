package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IRemovedSavedAdDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.RemovedSavedAdParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.AdSearchesRestService;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SpareroomStatus;

public class RemovedSavedAdDAO implements IRemovedSavedAdDAO {

    private final AdSearchesRestService adSearchesRestService;

    RemovedSavedAdDAO() {
        adSearchesRestService = new AdSearchesRestService();
    }

    public SpareroomStatus create(Parameters parameters) throws AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException {

        parameters.add("format", "json");

        String response = adSearchesRestService.removeSavedAd(parameters);

        RemovedSavedAdParser parser = new RemovedSavedAdParser();

        return parser.parse(response);

    }
}
