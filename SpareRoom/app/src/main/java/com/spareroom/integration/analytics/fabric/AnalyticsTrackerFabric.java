package com.spareroom.integration.analytics.fabric;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.*;
import com.spareroom.integration.analytics.AnalyticsTracker;
import com.spareroom.model.business.*;
import com.spareroom.ui.util.StringUtils;

import java.math.BigDecimal;
import java.util.Currency;

import androidx.fragment.app.Fragment;

import static com.spareroom.integration.analytics.fabric.AnalyticsTagsFabric.Attribute.*;
import static com.spareroom.integration.analytics.fabric.AnalyticsTagsFabric.Event.*;

/**
 * Tracker for Fabric
 * <p>
 * Created by manuel on 16/06/2016.
 */
public class AnalyticsTrackerFabric extends AnalyticsTracker {

    private static final String YES = "YES";
    private static final String NO = "NO";

    @Override
    public void trackEvent_AdvertOffered_MessageAdvertiser() {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.MESSAGE_ADVERT);

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_ADVERT_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_ADVERT_TYPE_OFFERED
        );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_AdvertWanted_MessageAdvertiser() {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.MESSAGE_ADVERT);

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_ADVERT_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_ADVERT_TYPE_WANTED
        );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_AdvertOffered_PhoneAdvertiser() {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.CALL_ADVERT);

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_ADVERT_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_ADVERT_TYPE_OFFERED
        );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_AdvertWanted_PhoneAdvertiser() {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.CALL_ADVERT);

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_ADVERT_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_ADVERT_TYPE_WANTED
        );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_AdvertOffered_Report() {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.REPORT);

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_ADVERT_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_ADVERT_TYPE_OFFERED
        );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_AdvertWanted_Report() {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.REPORT);

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_ADVERT_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_ADVERT_TYPE_WANTED
        );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_AdvertOffered_Share() {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.SHARE);

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_ADVERT_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_ADVERT_TYPE_OFFERED
        );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_AdvertWanted_Share() {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.SHARE);

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_ADVERT_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_ADVERT_TYPE_WANTED
        );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_Search_MarkFavouriteOffered(boolean isLoggedIn) {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.FAVOURITE);

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_OFFERED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_LOGIN_STATUS,
                isLoggedIn ?
                        AnalyticsTagsFabric.Attribute.VALUE_LOGIN_STATUS_LOGGED_IN :
                        AnalyticsTagsFabric.Attribute.VALUE_LOGIN_STATUS_LOGGED_OUT
        );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_Search_MarkFavouriteWanted(boolean isLoggedIn) {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.FAVOURITE);

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_WANTED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_LOGIN_STATUS,
                isLoggedIn ?
                        AnalyticsTagsFabric.Attribute.VALUE_LOGIN_STATUS_LOGGED_IN :
                        AnalyticsTagsFabric.Attribute.VALUE_LOGIN_STATUS_LOGGED_OUT
        );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_Search_RemoveShortlistOffered(boolean isLoggedIn) {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.UN_FAVOURITE);

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_OFFERED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_LOGIN_STATUS,
                isLoggedIn ?
                        AnalyticsTagsFabric.Attribute.VALUE_LOGIN_STATUS_LOGGED_IN :
                        AnalyticsTagsFabric.Attribute.VALUE_LOGIN_STATUS_LOGGED_OUT
        );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_Search_RemoveShortlistWanted(boolean isLoggedIn) {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.UN_FAVOURITE);

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_WANTED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_LOGIN_STATUS,
                isLoggedIn ?
                        AnalyticsTagsFabric.Attribute.VALUE_LOGIN_STATUS_LOGGED_IN :
                        AnalyticsTagsFabric.Attribute.VALUE_LOGIN_STATUS_LOGGED_OUT
        );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_SearchRooms_LaunchSavedSearch() {
        SearchEvent customEvent = new SearchEvent();

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_OFFERED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_QUERY_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_QUERY_TYPE_STRING
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_METHOD,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_METHOD_SAVED_SEARCH
        );

        Answers
                .getInstance()
                .logSearch(customEvent);
    }

    @Override
    public void trackEvent_SearchRooms_GetLocation() {
        SearchEvent customEvent = new SearchEvent();

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_OFFERED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_QUERY_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_QUERY_TYPE_STRING
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_METHOD,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_METHOD_LOCATION
        );

        Answers
                .getInstance()
                .logSearch(customEvent);

    }

    @Override
    public void trackEvent_SearchRooms_ManualSearch() {
        SearchEvent customEvent = new SearchEvent();

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_OFFERED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_QUERY_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_QUERY_TYPE_STRING
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_METHOD,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_METHOD_MANUAL
        );

        Answers
                .getInstance()
                .logSearch(customEvent);
    }

    @Override
    public void trackEvent_SearchRooms_AutocompleteSearch() {
        SearchEvent customEvent = new SearchEvent();

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_OFFERED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_QUERY_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_QUERY_TYPE_STRING
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_METHOD,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_METHOD_AUTOCOMPLETE
        );

        Answers
                .getInstance()
                .logSearch(customEvent);

    }

    @Override
    public void trackEvent_SearchRooms_HistorySearch() {
        SearchEvent customEvent = new SearchEvent();

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_OFFERED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_QUERY_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_QUERY_TYPE_STRING
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_METHOD,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_METHOD_RECENT
        );

        Answers
                .getInstance()
                .logSearch(customEvent);

    }

    @Override
    public void trackEvent_SearchRooms_ManualAdvertSearch() {
        SearchEvent customEvent = new SearchEvent();

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_OFFERED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_QUERY_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_QUERY_TYPE_ADVERT
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_METHOD,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_METHOD_MANUAL
        );

        Answers
                .getInstance()
                .logSearch(customEvent);

    }

    @Override
    public void trackEvent_SearchRooms_AutocompleteAdvertSearch() {
        SearchEvent customEvent = new SearchEvent();

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_OFFERED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_QUERY_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_QUERY_TYPE_ADVERT
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_METHOD,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_METHOD_AUTOCOMPLETE
        );

        Answers
                .getInstance()
                .logSearch(customEvent);

    }

    @Override
    public void trackEvent_SearchRooms_HistoryAdvertSearch() {
        SearchEvent customEvent = new SearchEvent();

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_OFFERED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_QUERY_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_QUERY_TYPE_ADVERT
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_METHOD,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_METHOD_RECENT
        );

        Answers
                .getInstance()
                .logSearch(customEvent);

    }

    @Override
    public void trackEvent_SearchFlatmates_GetLocation() {
        SearchEvent customEvent = new SearchEvent();

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_WANTED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_QUERY_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_QUERY_TYPE_STRING
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_METHOD,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_METHOD_LOCATION
        );

        Answers
                .getInstance()
                .logSearch(customEvent);

    }

    @Override
    public void trackEvent_SearchFlatmates_ManualSearch() {
        SearchEvent customEvent = new SearchEvent();

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_WANTED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_QUERY_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_QUERY_TYPE_STRING
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_METHOD,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_METHOD_MANUAL
        );

        Answers
                .getInstance()
                .logSearch(customEvent);

    }

    @Override
    public void trackEvent_SearchFlatmates_AutocompleteSearch() {
        SearchEvent customEvent = new SearchEvent();

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_WANTED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_QUERY_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_QUERY_TYPE_STRING
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_METHOD,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_METHOD_AUTOCOMPLETE
        );

        Answers
                .getInstance()
                .logSearch(customEvent);

    }

    @Override
    public void trackEvent_SearchFlatmates_HistorySearch() {
        SearchEvent customEvent = new SearchEvent();

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_WANTED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_QUERY_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_QUERY_TYPE_STRING
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_METHOD,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_METHOD_RECENT
        );

        Answers
                .getInstance()
                .logSearch(customEvent);

    }

    @Override
    public void trackEvent_SearchFlatmates_ManualAdvertSearch() {
        SearchEvent customEvent = new SearchEvent();

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_WANTED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_QUERY_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_QUERY_TYPE_ADVERT
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_METHOD,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_METHOD_MANUAL
        );

        Answers
                .getInstance()
                .logSearch(customEvent);

    }

    @Override
    public void trackEvent_SearchFlatmates_AutocompleteAdvertSearch() {
        SearchEvent customEvent = new SearchEvent();

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_WANTED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_QUERY_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_QUERY_TYPE_ADVERT
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_METHOD,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_METHOD_AUTOCOMPLETE
        );

        Answers
                .getInstance()
                .logSearch(customEvent);

    }

    @Override
    public void trackEvent_SearchFlatmates_HistoryAdvertSearch() {
        SearchEvent customEvent = new SearchEvent();

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_WANTED
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_QUERY_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_QUERY_TYPE_ADVERT
        );

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_METHOD,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_METHOD_RECENT
        );

        Answers
                .getInstance()
                .logSearch(customEvent);

    }

    @Override
    public void trackEvent_Message_Send(SpareroomStatus status) {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.MESSAGE);

        if ((status != null) && (status.getCode() != null))
            customEvent.putCustomAttribute(
                    AnalyticsTagsFabric.Attribute.KEY_MESSAGE_STATUS,
                    status.getCode().equals("1") ?
                            AnalyticsTagsFabric.Attribute.VALUE_MESSAGE_STATUS_SUCCESS :
                            status.getCode()
            );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_Message_Autoreject(SpareroomStatus status) {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.MESSAGE_AUTO_REJECT);

        if ((status != null) && (status.getCode() != null))
            customEvent.putCustomAttribute(
                    AnalyticsTagsFabric.Attribute.KEY_MESSAGE_STATUS,
                    status.getCode().equals("1") ?
                            AnalyticsTagsFabric.Attribute.VALUE_MESSAGE_STATUS_SUCCESS :
                            status.getCode()
            );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_PlaceAd_StartPlaceOfferedAd() {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.PLACE_AD_STARTED);

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_OFFERED
        );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_PlaceAd_StartPlaceWantedAd() {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.PLACE_AD_STARTED);

        customEvent.putCustomAttribute(
                AnalyticsTagsFabric.Attribute.KEY_SEARCH_TYPE,
                AnalyticsTagsFabric.Attribute.VALUE_SEARCH_TYPE_WANTED
        );

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEcommerce_Upgrade_Purchase(Upgrade u, String orderId, int productPosition) {
        PurchaseEvent pe = new PurchaseEvent()
                .putItemPrice(BigDecimal.valueOf(u.get_priceInMicros() / 1000000))
                .putCurrency(Currency.getInstance(u.get_currency()))
                .putItemName(u.get_title())
                .putItemType(AnalyticsTagsFabric.Ecommerce.TYPE_UPGRADE)
                .putItemId(u.get_code())
                .putSuccess(true);
        try {
            pe.putCustomAttribute(AnalyticsTagsFabric.Ecommerce.ATTRIBUTE_UPGRADE_CONTEXT,
                    UpgradeContext.getName(Integer.parseInt(u.get_context())));
        } catch (NumberFormatException e) {
            // we omit the custom attribute and we carry on logging the purchase
            // TODO: empty catch, don't do that!!!
        }

        Answers.getInstance().logPurchase(pe);
    }

    @Override
    public void trackEcommerce_Upgrade_Purchase_Fail(Upgrade u, int response, String message) {
        PurchaseEvent pe = new PurchaseEvent()
                .putItemPrice(BigDecimal.valueOf(u.get_priceInMicros() / 1000000))
                .putCurrency(Currency.getInstance(u.get_currency()))
                .putItemName(u.get_title())
                .putItemType(AnalyticsTagsFabric.Ecommerce.TYPE_UPGRADE)
                .putItemId(u.get_code())
                .putSuccess(false)
                .putCustomAttribute(AnalyticsTagsFabric.Ecommerce.ATTRIBUTE_BILLING_RESULT_CODE,
                        response)
                .putCustomAttribute(AnalyticsTagsFabric.Ecommerce.ATTRIBUTE_BILLING_RESULT_MESSAGE,
                        message);
        try {
            pe.putCustomAttribute(AnalyticsTagsFabric.Ecommerce.ATTRIBUTE_UPGRADE_CONTEXT,
                    UpgradeContext.getName(Integer.parseInt(u.get_context())));
        } catch (NumberFormatException e) {
            // we omit the custom attribute and we carry on logging the purchase
            // TODO: empty catch, don't do that!!!
        }

        Answers.getInstance().logPurchase(pe);
    }

    @Override
    public void trackEcommerce_ExtraListing_Purchase(Upgrade u, String orderId) {
        PurchaseEvent pe = new PurchaseEvent()
                .putItemPrice(BigDecimal.valueOf(u.get_priceInMicros() / 1000000))
                .putCurrency(Currency.getInstance(u.get_currency()))
                .putItemName(u.get_title())
                .putItemType(AnalyticsTagsFabric.Ecommerce.TYPE_EXTRA_LISTING)
                .putItemId(u.get_code())
                .putSuccess(true);
        try {
            pe.putCustomAttribute(AnalyticsTagsFabric.Ecommerce.ATTRIBUTE_UPGRADE_CONTEXT,
                    UpgradeContext.getName(Integer.parseInt(u.get_context())));
        } catch (NumberFormatException e) {
            // we omit the custom attribute and we carry on logging the purchase
            // TODO: empty catch, don't do that!!!
        }

        Answers.getInstance().logPurchase(pe);
    }

    @Override
    public void trackEcommerce_ExtraListing_Purchase_Fail(Upgrade u, int response, String message) {
        PurchaseEvent pe = new PurchaseEvent()
                .putItemPrice(BigDecimal.valueOf(u.get_priceInMicros() / 1000000))
                .putCurrency(Currency.getInstance(u.get_currency()))
                .putItemName(u.get_title())
                .putItemType(AnalyticsTagsFabric.Ecommerce.TYPE_EXTRA_LISTING)
                .putItemId(u.get_code())
                .putSuccess(false)
                .putCustomAttribute(AnalyticsTagsFabric.Ecommerce.ATTRIBUTE_BILLING_RESULT_CODE,
                        response)
                .putCustomAttribute(AnalyticsTagsFabric.Ecommerce.ATTRIBUTE_BILLING_RESULT_MESSAGE,
                        message);
        try {
            pe.putCustomAttribute(AnalyticsTagsFabric.Ecommerce.ATTRIBUTE_UPGRADE_CONTEXT,
                    UpgradeContext.getName(Integer.parseInt(u.get_context())));
        } catch (NumberFormatException e) {
            // we omit the custom attribute and we carry on logging the purchase
            // TODO: empty catch, don't do that!!!
        }

        Answers.getInstance().logPurchase(pe);
    }

    @Override
    public void trackEvent_Account_Register_Facebook(String userType) {
        String userTypeParam = null;

        switch (userType) {
            case "OFFERER_AND_SEEKER":
                userTypeParam = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_USER_TYPE_SEEKER_OFFERER;
                break;
            case "OFFERER":
                userTypeParam = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_USER_TYPE_OFFERER;
                break;
            case "SEEKER":
                userTypeParam = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_USER_TYPE_SEEKER;
                break;
        }

        Answers
                .getInstance()
                .logSignUp(
                        new SignUpEvent()
                                .putMethod(AnalyticsTagsFabric.Attribute.VALUE_REGISTER_METHOD_FACEBOOK)
                                .putSuccess(true)
                                .putCustomAttribute(AnalyticsTagsFabric.Attribute.KEY_REGISTER_USER_TYPE, userTypeParam));

    }

    @Override
    public void trackEvent_Account_Register_Spareroom(String userType) {
        String userTypeParam = null;

        switch (userType) {
            case "OFFERER_AND_SEEKER":
                userTypeParam = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_USER_TYPE_SEEKER_OFFERER;
                break;
            case "OFFERER":
                userTypeParam = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_USER_TYPE_OFFERER;
                break;
            case "SEEKER":
                userTypeParam = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_USER_TYPE_SEEKER;
                break;
        }

        Answers
                .getInstance()
                .logSignUp(
                        new SignUpEvent()
                                .putMethod(AnalyticsTagsFabric.Attribute.VALUE_REGISTER_METHOD_SPAREROOM)
                                .putSuccess(true)
                                .putCustomAttribute(AnalyticsTagsFabric.Attribute.KEY_REGISTER_USER_TYPE, userTypeParam));

    }

    @Override
    public void trackEvent_Account_FacebookLoginToRegisterRedirection() {
        Answers
                .getInstance()
                .logCustom(new CustomEvent(AnalyticsTagsFabric.Event.FACEBOOK_REDIRECTION_LOGIN_TO_REGISTER));
    }

    @Override
    public void trackEvent_Account_FacebookRegisterToLoginRedirection() {
        Answers
                .getInstance()
                .logCustom(new CustomEvent(AnalyticsTagsFabric.Event.FACEBOOK_REDIRECTION_REGISTER_TO_LOGIN));
    }

    @Override
    public void trackEvent_Account_Register_Facebook_Fail(SpareroomStatus failedRegistration) {
        String errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_UNDEFINED_ERROR;
        SignUpEvent signUpEvent = new SignUpEvent()
                .putMethod(AnalyticsTagsFabric.Attribute.VALUE_REGISTER_METHOD_FACEBOOK)
                .putSuccess(false);

        if (failedRegistration != null
                && failedRegistration.getCode() != null
                && !failedRegistration.getCode().isEmpty()) {

            switch (failedRegistration.getCode()) {
                case "652":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_INVALID_EMAIL;
                    break;
                case "655":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_INVALID_EMAIL_LENGTH;
                    break;
                case "654":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_CONFIRM_PASSWORD_FAILED;
                    break;
                case "657":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_AGREE_TERMS_N_CONDITIONS;
                    break;
                case "650":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_USED_EMAIL;
                    break;
                case "658":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_MISSED_FIRST_NAME;
                    break;
                case "659":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_MISSED_LAST_NAME;
                    break;
                case "661":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_MISSED_USER_TYPE;
                    break;
                default:
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_OTHER_ERROR;
                    signUpEvent.putCustomAttribute(errorLabel, failedRegistration.getMessage());
            }

            signUpEvent =
                    signUpEvent.putCustomAttribute(
                            AnalyticsTagsFabric.Attribute.KEY_FAILED_ERROR_CODE,
                            failedRegistration.getCode());

        }

        Answers
                .getInstance()
                .logSignUp(signUpEvent.putCustomAttribute(AnalyticsTagsFabric.Attribute.KEY_REGISTER_FAILED, errorLabel));

    }

    @Override
    public void trackEvent_Account_Register_Spareroom_Fail(SpareroomStatus failedRegistration) {
        String errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_UNDEFINED_ERROR;
        SignUpEvent signUpEvent = new SignUpEvent()
                .putMethod(AnalyticsTagsFabric.Attribute.VALUE_REGISTER_METHOD_SPAREROOM)
                .putSuccess(false);

        if (failedRegistration != null
                && failedRegistration.getCode() != null
                && !failedRegistration.getCode().isEmpty()) {

            switch (failedRegistration.getCode()) {
                case "652":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_INVALID_EMAIL;
                    break;
                case "655":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_INVALID_EMAIL_LENGTH;
                    break;
                case "654":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_CONFIRM_PASSWORD_FAILED;
                    break;
                case "657":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_AGREE_TERMS_N_CONDITIONS;
                    break;
                case "650":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_USED_EMAIL;
                    break;
                case "658":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_MISSED_FIRST_NAME;
                    break;
                case "659":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_MISSED_LAST_NAME;
                    break;
                case "661":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_ERROR_MISSED_USER_TYPE;
                    break;
                default:
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_REGISTER_FAILED_OTHER_ERROR;
                    signUpEvent.putCustomAttribute(errorLabel, failedRegistration.getMessage());

            }

            signUpEvent =
                    signUpEvent.putCustomAttribute(
                            AnalyticsTagsFabric.Attribute.KEY_FAILED_ERROR_CODE,
                            failedRegistration.getCode());

        }

        Answers
                .getInstance()
                .logSignUp(signUpEvent.putCustomAttribute(AnalyticsTagsFabric.Attribute.KEY_REGISTER_FAILED, errorLabel));

    }

    @Override
    public void trackEvent_Account_Login_Facebook() {
        Answers
                .getInstance()
                .logLogin(
                        new LoginEvent()
                                .putMethod(AnalyticsTagsFabric.Attribute.VALUE_LOGIN_METHOD_FACEBOOK)
                                .putSuccess(true));
    }

    @Override
    public void trackEvent_Account_Login_Spareroom() {
        Answers
                .getInstance()
                .logLogin(
                        new LoginEvent()
                                .putMethod(AnalyticsTagsFabric.Attribute.VALUE_LOGIN_METHOD_SPAREROOM)
                                .putSuccess(true));
    }

    @Override
    public void trackEvent_Account_Login_Facebook_Failed(SpareroomStatus failedRegistration) {
        String errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_UNDEFINED_ERROR;
        LoginEvent loginEvent =
                new LoginEvent()
                        .putMethod(AnalyticsTagsFabric.Attribute.VALUE_LOGIN_METHOD_FACEBOOK)
                        .putSuccess(false);

        if (failedRegistration != null
                && failedRegistration.getCode() != null
                && !failedRegistration.getCode().isEmpty()) {

            switch (failedRegistration.getCode()) {
                case "603":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_ERROR_NO_PASSWORD;
                    break;
                case "604":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_ERROR_UNKNOWN_EMAIL;
                    break;
                case "610":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_ERROR_INVALID_EMAIL;
                    break;
                case "605":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_ERROR_WRONG_PASSWORD;
                    break;
                case "601":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_ERROR_MISSED_EMAIL_AND_PASSWORD;
                    break;
                case "602":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_ERROR_MISSED_EMAIL;
                    break;
                case "651":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_ERROR_ALREADY_LOGGED_IN;
                    break;
                default:
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_OTHER_ERROR;
                    loginEvent = loginEvent.putCustomAttribute(errorLabel, failedRegistration.getMessage());
            }

            loginEvent =
                    loginEvent.putCustomAttribute(
                            AnalyticsTagsFabric.Attribute.KEY_FAILED_ERROR_CODE,
                            failedRegistration.getCode());

        }

        Answers
                .getInstance()
                .logLogin(loginEvent.putCustomAttribute(AnalyticsTagsFabric.Attribute.KEY_LOGIN_FAILED, errorLabel));

    }

    @Override
    public void trackEvent_Account_Login_Spareroom_Failed(SpareroomStatus failedRegistration) {
        String errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_UNDEFINED_ERROR;
        LoginEvent loginEvent = new LoginEvent()
                .putMethod(AnalyticsTagsFabric.Attribute.VALUE_LOGIN_METHOD_SPAREROOM)
                .putSuccess(false);

        if (failedRegistration != null
                && failedRegistration.getCode() != null
                && !failedRegistration.getCode().isEmpty()) {

            switch (failedRegistration.getCode()) {
                case "603":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_ERROR_NO_PASSWORD;
                    break;
                case "604":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_ERROR_UNKNOWN_EMAIL;
                    break;
                case "610":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_ERROR_INVALID_EMAIL;
                    break;
                case "605":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_ERROR_WRONG_PASSWORD;
                    break;
                case "601":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_ERROR_MISSED_EMAIL_AND_PASSWORD;
                    break;
                case "602":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_ERROR_MISSED_EMAIL;
                    break;
                case "651":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_ERROR_ALREADY_LOGGED_IN;
                    break;
                default:
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_LOGIN_FAILED_OTHER_ERROR;
                    loginEvent = loginEvent.putCustomAttribute(errorLabel, failedRegistration.getMessage());

            }

            loginEvent =
                    loginEvent.putCustomAttribute(
                            AnalyticsTagsFabric.Attribute.KEY_FAILED_ERROR_CODE,
                            failedRegistration.getCode());
        }

        Answers
                .getInstance()
                .logLogin(loginEvent.putCustomAttribute(AnalyticsTagsFabric.Attribute.KEY_LOGIN_FAILED, errorLabel));

    }

    @Override
    public void trackEvent_Account_Logout() {
        Answers
                .getInstance()
                .logCustom(new CustomEvent(AnalyticsTagsFabric.Event.LOGOUT));
    }

    @Override
    public void trackEvent_Account_ForgotPassword() {
        Answers
                .getInstance()
                .logCustom(new CustomEvent(AnalyticsTagsFabric.Event.RECOVER_PASSWORD));
    }

    @Override
    public void trackEvent_Account_ForgotPassword_Failed(SpareroomStatus failedRecover) {
        String errorLabel = AnalyticsTagsFabric.Attribute.VALUE_RECOVER_PASSWORD_UNDEFINED_ERROR;
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.RECOVER_PASSWORD_FAILED);

        if (failedRecover != null
                && failedRecover.getCode() != null
                && !failedRecover.getCode().isEmpty()) {

            switch (failedRecover.getCode()) {
                case "1102":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_RECOVER_PASSWORD_FAILED_THREE_REMINDERS;
                    break;
                case "1103":
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_RECOVER_PASSWORD_FAILED_EMAIL;
                    break;
                default:
                    errorLabel = AnalyticsTagsFabric.Attribute.VALUE_RECOVER_PASSWORD_OTHER_ERROR;

            }

            customEvent =
                    customEvent.putCustomAttribute(
                            AnalyticsTagsFabric.Attribute.KEY_FAILED_ERROR_CODE,
                            failedRecover.getCode());

        }

        Answers
                .getInstance()
                .logCustom(
                        customEvent.putCustomAttribute(
                                AnalyticsTagsFabric.Attribute.KEY_RECOVER_PASSWORD_FAILED,
                                errorLabel));

    }

    @Override
    public void trackEvent_ContactUs_Call() {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.CALL_CS);

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_ContactUs_Email() {
        CustomEvent customEvent = new CustomEvent(AnalyticsTagsFabric.Event.EMAIL_CS);

        Answers
                .getInstance()
                .logCustom(customEvent);
    }

    @Override
    public void trackEvent_SearchRoomsMenu_SaveSearch() {
        Answers.getInstance().logCustom(new CustomEvent(AnalyticsTagsFabric.Event.SEARCH_SAVED));
    }

    @Override
    public void trackSingleConversationLoadedEvent(float duration) {
        Answers.getInstance().logCustom(
                new CustomEvent(AnalyticsTagsFabric.Event.SINGLE_CONVERSATION_LOADED)
                        .putCustomAttribute(AnalyticsTagsFabric.Attribute.DURATION, duration));
    }

    @Override
    public void trackTaskDuration(String taskName, float duration) {
        Answers.getInstance().logCustom(new CustomEvent(taskName).putCustomAttribute(AnalyticsTagsFabric.Attribute.DURATION, duration));
    }

    @Override
    public void logHandledException(String action, Throwable throwable) {
        if (throwable != null) {
            StringBuilder errorMessage = new StringBuilder();
            errorMessage.append(action);

            String message = throwable.getMessage();
            if (!StringUtils.isNullOrEmpty(message))
                errorMessage.append(String.format(": %s", message));

            Crashlytics.logException(new Throwable(errorMessage.toString(), throwable));
        }
    }

    @Override
    public void trackOpenedCustomTab(boolean opened) {
        Answers.getInstance().logCustom(
                new CustomEvent(AnalyticsTagsFabric.Event.CUSTOM_TABS)
                        .putCustomAttribute(AnalyticsTagsFabric.Attribute.CUSTOM_TABS_HANDLED_PAGE, opened ? YES : NO));
    }

    // initial rating popup
    @Override
    public void trackInitialRatingPopupBackButton() {
        Answers.getInstance().logCustom(new CustomEvent(RATING_INITIAL_POPUP).putCustomAttribute(RATING_BACK_BUTTON, ""));
    }

    @Override
    public void trackInitialRatingPopupTapOutside() {
        Answers.getInstance().logCustom(new CustomEvent(RATING_INITIAL_POPUP).putCustomAttribute(RATING_OUTSIDE, ""));
    }

    @Override
    public void trackInitialRatingPopupMaybeLater() {
        Answers.getInstance().logCustom(new CustomEvent(RATING_INITIAL_POPUP).putCustomAttribute(RATING_MAYBE_LATER, ""));
    }

    @Override
    public void trackInitialRatingPopupRateApp() {
        Answers.getInstance().logCustom(new CustomEvent(RATING_INITIAL_POPUP).putCustomAttribute(RATING_RATE_APP, ""));
    }

    // reminder rating popup
    @Override
    public void trackReminderRatingPopupBackButton() {
        Answers.getInstance().logCustom(new CustomEvent(RATING_REMINDER_POPUP).putCustomAttribute(RATING_BACK_BUTTON, ""));
    }

    @Override
    public void trackReminderRatingPopupTapOutside() {
        Answers.getInstance().logCustom(new CustomEvent(RATING_REMINDER_POPUP).putCustomAttribute(RATING_OUTSIDE, ""));
    }

    @Override
    public void trackReminderRatingPopupDismiss() {
        Answers.getInstance().logCustom(new CustomEvent(RATING_REMINDER_POPUP).putCustomAttribute(RATING_DISMISS, ""));
    }

    @Override
    public void trackReminderRatingPopupRateApp() {
        Answers.getInstance().logCustom(new CustomEvent(RATING_REMINDER_POPUP).putCustomAttribute(RATING_RATE_APP, ""));
    }

    @Override
    public void trackSearchAdvertListLoadedEvent(float duration) {
        Answers.getInstance().logCustom(
                new CustomEvent(AnalyticsTagsFabric.Event.SEARCH_ADVERT_LIST_LOADED)
                        .putCustomAttribute(AnalyticsTagsFabric.Attribute.DURATION, duration));
    }

    @Override
    public void trackUsingDeadFragmentEvent(Fragment fragment) {
        Answers.getInstance().logCustom(new CustomEvent(USING_DEAD_FRAGMENT).putCustomAttribute(FRAGMENT_NAME, fragment.getClass().getSimpleName()));
    }

    @Override
    public void setCustomerUserId(String customerUserId) {
        Crashlytics.setUserIdentifier(customerUserId);
    }

    @Override
    public void leaveBreadcrumb(String message) {
        Crashlytics.log(message);
    }
}
