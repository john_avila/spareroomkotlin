package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.SpareroomStatus;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Parser for the video upload response
 * <p>
 * Created by miguel.rossi on 30/08/2016.
 */
public class VideoUploadedJSONParser {

    public Object parse(String json) throws InvalidJSONFormatException {

        try {
            JSONObject firstNode = new JSONObject(json);
            SpareroomStatus m = new SpareroomStatus();

            m.setCode(firstNode.getString("response_type"));
            m.setMessage(firstNode.getString("response"));

            return m;

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

    } //end parse(String json)

}
