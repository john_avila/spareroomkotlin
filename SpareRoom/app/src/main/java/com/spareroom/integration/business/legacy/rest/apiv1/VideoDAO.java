package com.spareroom.integration.business.legacy.rest.apiv1;

import android.util.Log;

import com.spareroom.integration.business.legacy.rest.IVideoDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.Authenticator;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.DeleteVideoJSONParser;
import com.spareroom.integration.business.legacy.rest.apiv1.util.VideoUploadedJSONParser;
import com.spareroom.integration.exception.InterruptedCommunicationException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.VideoRestService;
import com.spareroom.lib.core.ILongTaskObserver;
import com.spareroom.lib.core.exception.LongTaskInterruptedException;
import com.spareroom.lib.util.FileProcessor;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SpareroomStatus;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * DAO for videos
 * <p/>
 * Created by miguel.rossi on 25/08/2016.
 */
public class VideoDAO implements IVideoDAO {
    private static final int MAX_FAILED_UPLOADS = 10;

    private final VideoRestService videoRestService;

    VideoDAO() {
        videoRestService = new VideoRestService();
    }

    @Override
    public Object create(Parameters parameters, ILongTaskObserver progressObserver) throws
            NetworkConnectivityException,
            InvalidJSONFormatException,
            AuthenticationException,
            ClientErrorException,
            ServerErrorException,
            InterruptedCommunicationException {

        parameters.add("format", "json");
        parameters.add("function", "upload");
        parameters.add("name", "android_video.mp4");

        String[] vNamesMultipart = new String[1];
        String[] vValuesMultipart = new String[1];

        String sVideo = parameters.get("videoPath");
        if (sVideo != null) {
            vNamesMultipart[0] = "file";
            vValuesMultipart[0] = sVideo;
        }
        parameters.remove("videoPath");

        Authenticator authenticator = new Authenticator();
        parameters.add("api_sig", authenticator.authenticate(parameters));
        parameters.add("api_key", authenticator.get_apiKey());

        String[] vNames = new String[parameters.size() + 3];
        String[] vValues = new String[parameters.size() + 3];

        String[] vParamNames = parameters.getParameterNames();

        for (int i = 0; i < parameters.size(); i++) {
            vNames[i] = vParamNames[i];
            vValues[i] = parameters.get(vParamNames[i]);
        }

        Object o = null;

        String response;
        VideoUploadedJSONParser parser = new VideoUploadedJSONParser();

        FileProcessor fileProcessor = null;
        try {
            fileProcessor = new FileProcessor(sVideo);

            // split(sVideo);
            int numberChunks = fileProcessor.getNumberChunks();
            int intentsByFailedChunk = 0;
            long seconds, lastSecond, beginning;
            lastSecond = beginning = Calendar.getInstance().getTimeInMillis();

            vNames[parameters.size()] = "chunks";
            vValues[parameters.size()] = String.valueOf(numberChunks);
            // The `upload_session` is a random string, that should be the same for all the chunks
            //                  of one file, but different each time you upload a new file.
            // This would prevent problems in an odd but possible scenario where one user uses 2
            //      phones at the same time to upload files. So each phone must have a different
            //      `upload_session` for those chunks from both phones not be mixed when we join
            //      them together on the back-end.
            vNames[parameters.size() + 1] = "upload_session";
            vValues[parameters.size() + 1] = Calendar.getInstance().getTimeInMillis() + "_" + Math.random();
            vNames[parameters.size() + 2] = "chunk";

            FileProcessor.ChunkIterator iterator = fileProcessor.getIterator();

            for (int i = 0; i < fileProcessor.getNumberChunks(); i++) {
                // log
                seconds = Calendar.getInstance().getTimeInMillis();
                Log.d("chunk", String.format("%s: %d; %s: %d; %s: %d", "number", i, "time", seconds, "spent", seconds - lastSecond));
                lastSecond = Calendar.getInstance().getTimeInMillis();

                vValues[parameters.size() + 2] = String.valueOf(i);

                vValuesMultipart[0] = iterator.nextChunk();

                response = videoRestService.uploadVideo(vNames, vValues, vNamesMultipart, vValuesMultipart);

                o = parser.parse(response);
                if (o instanceof SpareroomStatus) {
                    if (!((SpareroomStatus) o).getCode().equals("success")) { // if not success
                        i--;
                        intentsByFailedChunk++;
                        if (intentsByFailedChunk >= MAX_FAILED_UPLOADS) {
                            throw new ClientErrorException("Too many failed attempts to upload");
                        }
                        Log.d("chunk", i + "/" + numberChunks + " FAILED ");

                    } else {
                        intentsByFailedChunk = 0;
                        Log.d("chunk", i + "/" + numberChunks + " ");
                    }
                } else {
                    throw new InvalidJSONFormatException();
                }

                progressObserver.notifyObserver(i / numberChunks);

            } // end for

            // log
            seconds = Calendar.getInstance().getTimeInMillis();
            Log.d(
                    "chunk",
                    String.format(
                            "%s: %s; %s: %d; %s: %d\n%s: %d min, %d sec",
                            "number",
                            "finish",
                            "time",
                            seconds,
                            "spent",
                            seconds - lastSecond,
                            "time spent",
                            TimeUnit.MILLISECONDS.toMinutes(seconds - beginning),
                            TimeUnit.MILLISECONDS.toSeconds(seconds - beginning) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(seconds - beginning))));

            return o;
        } catch (IOException e) {
            throw new ClientErrorException(e);
        } catch (LongTaskInterruptedException e) {
            throw new InterruptedCommunicationException(e);
        } finally {
            if (fileProcessor != null) {
                fileProcessor.deleteChunks();
                try {
                    fileProcessor.close();
                } catch (IOException e1) {
                    // We ignore it so the file upload is successful even it the is a potential
                    // memory leak from not closing buffers correctly with fileProcessor.close();
                }
            }
        }
    }

    @Override
    public SpareroomStatus delete(Parameters params) throws
            NetworkConnectivityException,
            InvalidJSONFormatException,
            AuthenticationException,
            ClientErrorException,
            ServerErrorException {
        // Delete
        DeleteVideoJSONParser parser = new DeleteVideoJSONParser();

        params.add("format", "json");
        params.add("function", "delete");

        String response = videoRestService.deleteVideo(params);

        return parser.parse(response);
    }

}
