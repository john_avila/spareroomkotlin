package com.spareroom.integration.analytics.googleanalytics;

/**
 * Custom tags for Google Analytics
 */
public class AnalyticsTagsGoogleAnalytics {
    public static final String BRAND = "SpareRoom UK";
    public static final String AFFILIATION = "Google Play In-app Billing";

    public class Category {
        public static final String HOMESCREEN = "Homescreen";
        public static final String SEARCH = "Search";
        public static final String ADVERT = "Advert";
        public static final String MESSAGES = "Messages";
        public static final String PLACEAD = "PlaceAdvert";
        public static final String PLACEDADVERTS = "Account/Placed";
        public static final String SAVEDSEARCHES = "Account/Saved-Searches";
        public static final String SHARE_TWITTER = "/share/twitter";

        public static final String PUSH = "Push";
        public static final String DEV = "Dev";

        // NEW ANALYTICS

        // public static final String CATEGORY_SAVED_SEARCHES_SEARCH = "Saved searches (Search)";
        // public static final String CATEGORY_SAVED_SEARCHES_MENU= "Saved searches (Menu)";
        public static final String MENU = "Menu";
        public static final String SEARCH_ROOMS = "Search Rooms";
        public static final String SEARCH_ROOMS_MENU = "Search Rooms Menu";
        public static final String SEARCH_FLATMATES = "Search Flatmates";
        public static final String SEARCH_FLATMATES_MENU = "Search Flatmates Menu";
        public static final String RESULTS = "Results";
        public static final String SAVED_SEARCHES = "Saved Searches";
        public static final String PLACE_ADVERT = "Place Advert";
        public static final String EDIT_ADVERT = "Edit Advert";
        public static final String UPGRADE = "Upgrade account";
        public static final String ACCOUNT = "Account";
        public static final String ADVERT_VIDEO = "Advert video";
    }

    public class Action {
        public static final String BROADEN_SEARCH = "Broaden search";
        public static final String REMOVE_SHORTLIST = "Remove shortlist";
        public static final String MARK_UNSUITABLE = "Mark as unsuitable";
        public static final String MARK_FAVOURITE = "Mark as favourite";
        public static final String MESSAGE_ADVERTISER = "Message Advertiser";
        public static final String PHONE_ADVERTISER = "Phone Advertiser";
        public static final String VIEW_YOUTUBE = "View on YouTube";
        public static final String CONTACT_ADVERTISER = "Contact Advertiser";
        public static final String SHARE_TWITTER = "Share on Twitter";
        public static final String SHARE_FACEBOOK = "Share on Facebook";
        public static final String VIEW_USERS_ADVERT = "View user's adverts";
        public static final String SAVE_SEARCH = "Save Search";
        public static final String FACEBOOK = "Facebook";
        public static final String TWITTER = "Twitter";
        public static final String GET_MY_LOCATION = "Get my location";
        public static final String DELETE = "Delete";
        public static final String LOAD_MORE = "Load more";
        public static final String UPDATE = "Update";
        public static final String PLACED_ADVERT = "Placed advert";
        public static final String ADD_EXTRA_LISTINGS = "Add Extra listings";
        public static final String SHARE_TWITTER_VIA_WEB = "Shared on Twitter via web";
        public static final String NO_TWITTER_ACCOUNT = "No Twitter account";

        public static final String PUSH_TOO_MANY_REQUESTS = "Too many requests";

        // NEW ANALYTICS
        public static final String MENU_SEARCH = "Select Search";
        public static final String MENU_MESSAGES = "Select Messages";
        public static final String SELECT_MY_ADVERTS = "Select My adverts";
        public static final String MENU_SAVED_ADS = "Select Saved ads";
        public static final String MENU_SAVEDSEARCHES = "Select Saved searches";
        public static final String MENU_EDIT_MY_DETAILS = "Select Edit my details";
        public static final String MENU_UPGRADE = "Select Upgrade my account";
        public static final String MENU_PLACE_ADVERT = "Select Place advert";
        public static final String MENU_BETA_PROGRAMME = "Select Beta Programme";
        public static final String MENU_CONTACT_US_PHONE = "Select Contact us - Phone";
        public static final String MENU_CONTACT_US_MAIL = "Select Contact us - eMail";
        public static final String MENU_LOGOUT = "Select Log out";

        public static final String LAUNCH_SAVED_SEARCH = "Search Rooms using Saved Search";
        public static final String DELETE_SAVED_SEARCH = "Delete Saved Search";

        public static final String SEARCH_ROOMS_MANUALLY = "Search Rooms manually";
        public static final String SEARCH_ROOMS_USING_AUTOCOMPLETE = "Search Rooms using autocomplete";
        public static final String SEARCH_ROOMS_USING_HISTORY = "Search Rooms using history";
        public static final String SEARCH_FLATMATES_MANUALLY = "Search Flatmates manually";
        public static final String SEARCH_FLATMATES_USING_AUTOCOMPLETE = "Search Flatmates using autocomplete";
        public static final String SEARCH_FLATMATES_USING_HISTORY = "Search Flatmates using history";

        public static final String SEARCH_ADVERT_MANUALLY = "Search Advert manually";
        public static final String SEARCH_ADVERT_USING_HISTORY = "Search Advert using history";
        public static final String SEARCH_ADVERT_USING_AUTOCOMPLETE = "Search Advert using autocomplete";

        public static final String RESULTS_SUGGESTION = "Search by correcting misspelled location";
        public static final String RESULTS_CANCEL_SUGGESTION = "Cancel correcting misspelled location";

        public static final String SEARCH_LOCATION = "Get location";

        public static final String PLACE_OFFERED_AD = "Place advert - offered";
        public static final String PLACE_WANTED_AD = "Place advert - wanted";
        public static final String PLACE_OFFERED_AD_ERRORS = "Receive rejected form - offered";
        public static final String PLACE_WANTED_AD_ERRORS = "Receive rejected form - wanted";

        public static final String READ_OFFERED_DRAFT = "Read advert to edit - offered";
        public static final String READ_WANTED_DRAFT = "Read advert to edit- wanted";
        public static final String READ_OFFERED_DRAFT_ERRORS = "Error reading advert to edit - offered";
        public static final String READ_WANTED_DRAFT_ERRORS = "Error reading advert to edit - wanted";

        public static final String EDIT_OFFERED_AD = "Edit advert - offered";
        public static final String EDIT_WANTED_AD = "Edit advert - wanted";
        public static final String EDIT_OFFERED_AD_ERRORS = "Receive rejected edit form - offered";
        public static final String EDIT_WANTED_AD_ERRORS = "Receive rejected edit form - wanted";

        public static final String SEARCH_MAP = "Select Map view";
        public static final String SEARCH_SAVE = "Select Save search";
        public static final String SEARCH_REFINE = "Select Refine";

        public static final String SEARCH_SORT_BY = "Select Sort by";
        public static final String SEARCH_SORT_BY_DEFAULT = "Sort by Default";
        public static final String SEARCH_SORT_BY_LAST_UPDATED = "Sort by Last updated";
        public static final String SEARCH_SORT_BY_NEWEST = "Sort by Newest";
        public static final String SEARCH_SORT_BY_MOST_EXPENSIVE = "Sort by Most expensive";
        public static final String SEARCH_SORT_BY_LEAST_EXPENSIVE = "Sort by Least expensive";

        public static final String UPGRADE_SELECT_PRODUCT = "Select upgrade product";
        public static final String UPGRADE_VIEW_PRODUCT = "View product details";
        public static final String UPGRADE_INITIATE_CHECKOUT = "Initiate upgrade checkout";
        public static final String UPGRADE_PURCHASE = "Purchase upgrade";
        public static final String UPGRADE_EXTRA_LISTING = "Purchase extra listing";
        public static final String UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE = "Billing unexpectedly unavailable";
        public static final String UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_EXTRA_LISTING =
                "Billing unexpectedly unavailable (extra listing)";

        public static final String ACCOUNT_REGISTER_FACEBOOK = "Register Account - Facebook";
        public static final String ACCOUNT_REGISTER_SPAREROOM = "Register Account - Spareroom";
        public static final String ACCOUNT_REGISTER_FAILED = "Register Account - Failed";

        public static final String ACCOUNT_LOGIN = "Login Account";
        public static final String ACCOUNT_LOGIN_FAILED = "Login Account - Failed";

        public static final String ACCOUNT_LOGOUT = "Logout";

        public static final String ACCOUNT_FORGOT_PASSWORD = "Recovered Password";
        public static final String ACCOUNT_FORGOT_PASSWORD_FAILED = "Recovered Password - Failed";

        public static final String ACCOUNT_FACEBOOK_REDIRECTION_LOGIN_TO_REGISTER = "Facebook redirection - login to sign up";
        public static final String ACCOUNT_FACEBOOK_REDIRECTION_REGISTER_TO_LOGIN = "Facebook redirection - sign up to login";

        public static final String VIDEO_UPLOAD_OFFERED_GALLERY = "Upload video - offered - gallery";
        public static final String VIDEO_UPLOAD_OFFERED_CAMERA = "Upload video - offered - camera";
        public static final String VIDEO_UPLOAD_WANTED_GALLERY = "Upload video - wanted - gallery";
        public static final String VIDEO_UPLOAD_WANTED_CAMERA = "Upload video - wanted - camera";
        public static final String VIDEO_UPLOAD_ERROR_UPLOADING_OFFERED_GALLERY = "Error uploading video - offered - gallery";
        public static final String VIDEO_UPLOAD_ERROR_UPLOADING_OFFERED_CAMERA = "Error uploading video - offered - camera";
        public static final String VIDEO_UPLOAD_ERROR_UPLOADING_WANTED_GALLERY = "Error uploading video - wanted - gallery";
        public static final String VIDEO_UPLOAD_ERROR_UPLOADING_WANTED_CAMERA = "Error uploading video - wanted - camera";

    }

    public class Label {
        public static final String LOGIN_OPENED = "Login screen opened";
        public static final String UPGRADE_OPEN = "Upgrade screen opened";
        //		public static final String LOGIN_SCREEN_APPEARED = "Login screen appeared"; the same thing as LABEL_LOGIN_OPENED
        public static final String INITIAL_UPGRADE_REQUIRED = "Initial upgrade required";

        // NEW ANALYTICS

        public static final String UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_CREATE = "Create";
        public static final String UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_DISABLE_DEBUG = "Disable debug";
        public static final String UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_SETUP = "Setup";
        public static final String UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_QUERY_INVENTORY = "Query inventory";
        public static final String UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_LAUNCH_PURCHASE = "Launch purchase flow";
        public static final String UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_HANDLE_PAYMENT_RESULT = "Handle payment result";
        public static final String UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_CONSUME = "Consume";

        // Register Errors
        public static final String REGISTER_FAILED_ERROR_INVALID_EMAIL = "Invalid email";
        public static final String REGISTER_FAILED_ERROR_INVALID_EMAIL_LENGTH = "Invalid email length";
        public static final String REGISTER_FAILED_ERROR_CONFIRM_PASSWORD_FAILED = "Password confirmation failed";
        public static final String REGISTER_FAILED_ERROR_AGREE_TERMS_N_CONDITIONS = "Not agree with terms and conditions";
        public static final String REGISTER_FAILED_ERROR_USED_EMAIL = "Email already in use";
        public static final String REGISTER_FAILED_ERROR_MISSED_FIRST_NAME = "Missed first name";
        public static final String REGISTER_FAILED_ERROR_MISSED_LAST_NAME = "Missed last name";
        public static final String REGISTER_FAILED_ERROR_MISSED_USER_TYPE = "No User Type";
        public static final String REGISTER_FAILED_OTHER_ERROR = "Other Error";
        public static final String REGISTER_FAILED_UNDEFINED_ERROR = "Undefined Error";

        // Login Errors
        public static final String LOGIN_FAILED_ERROR_NO_PASSWORD = "Password empty";
        public static final String LOGIN_FAILED_ERROR_UNKNOWN_EMAIL = "Unknown email";
        public static final String LOGIN_FAILED_ERROR_INVALID_EMAIL = "Invalid email";
        public static final String LOGIN_FAILED_ERROR_WRONG_PASSWORD = "Wrong password";
        public static final String LOGIN_FAILED_ERROR_ALREADY_LOGGED_IN = "Already LoggedIn";
        public static final String LOGIN_FAILED_ERROR_MISSED_EMAIL_AND_PASSWORD = "No Email Or Password";
        public static final String LOGIN_FAILED_ERROR_MISSED_EMAIL = "No Email";
        public static final String LOGIN_FAILED_OTHER_ERROR = "Other Error";
        public static final String LOGIN_FAILED_UNDEFINED_ERROR = "Undefined Error";

        // Recovering Password Errors
        public static final String VALUE_RECOVER_PASSWORD_FAILED_THREE_REMINDERS = "Three reminders has been sent";
        public static final String VALUE_RECOVER_PASSWORD_FAILED_EMAIL = "Email not valid";
        public static final String VALUE_RECOVER_PASSWORD_OTHER_ERROR = "Other Error";
        public static final String VALUE_RECOVER_PASSWORD_UNDEFINED_ERROR = "Undefined Error";

        // Login Methods
        public static final String ACCOUNT_LOGIN_METHOD_FACEBOOK = "Facebook";
        public static final String ACCOUNT_LOGIN_METHOD_SPAREROOM = "Spareroom";

        // Register - User type
        public static final String ACCOUNT_REGISTER_USER_TYPE_SEEKER_OFFERER = "Seeker Offerer";
        public static final String ACCOUNT_REGISTER_USER_TYPE_SEEKER = "Seeker";
        public static final String ACCOUNT_REGISTER_USER_TYPE_OFFERER = "Offerer";

        /**
         * Generic code label (code needs to be appended)
         */
        public static final String CODE = "Code:";
        /**
         * Generic exception label (exception name needs to be appended)
         */
        public static final String EXCEPTION = "Exception:";

        // Advert Video
        public static final String VIDEO_UPLOAD_ERROR_TOO_LONG = "Video uploaded is too big";
        public static final String VIDEO_UPLOAD_ERROR_TOO_BIG = "Video uploaded is too long";
        public static final String VIDEO_UPLOAD_ERROR_UNKNOWN = "Video uploaded - unknown error";

    }

    public class Ecommerce {
        public static final String CATEGORY_EARLY_BIRD = "Early bird access";
        public static final String CATEGORY_BOLD_ADVERTISING = "Bold advertising";
    }

}
