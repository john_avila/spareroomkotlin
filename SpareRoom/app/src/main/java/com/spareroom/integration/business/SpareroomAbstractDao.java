package com.spareroom.integration.business;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.exception.InvalidArgumentException;
import com.spareroom.integration.exception.UnavailableDataSourceException;
import com.spareroom.integration.exception.UnexpectedResultException;

/**
 * Dao template interface for a business object.
 *
 * @param <BO>   business object
 * @param <E>    extra data required by SpareRoom in order to perform the operation. Data provided here
 *               does not belong to the business object. Optional, as required by the SpareRoom per
 *               operation.
 * @param <BOID> id for the business object
 *               <p>
 *               Created by manuel on 27/06/2016.
 */
public abstract class SpareroomAbstractDao<BO, E, BOID> {
    protected final SpareroomContext _spareroomContext;

    public SpareroomAbstractDao(SpareroomContext spareroomContext) {
        _spareroomContext = spareroomContext;
    }

    /**
     * Creates the business object on the external system
     *
     * @param businessObject the business object
     * @param extra          extra data required by SpareRoom. Can be <code>null</code> if required.
     * @return depends on SpareRoom's API spec. Most of times it should return the business object
     * retrieved or SpareroomStatus if something went wrong.
     * @throws UnavailableDataSourceException if the data source is not ready
     * @throws InvalidArgumentException       if an argument cannot be processed
     * @throws UnexpectedResultException      if the result cannot be processed
     */
    public abstract Object create(BO businessObject, E extra)
            throws UnavailableDataSourceException,
            InvalidArgumentException,
            UnexpectedResultException;

    /**
     * Reads the business object from the external system
     *
     * @param id    the id for the business object
     * @param extra extra data required by SpareRoom. Can be <code>null</code> if required.
     * @return depends on SpareRoom's API spec. Most of times it should return the business object
     * retrieved or SpareroomStatus if something went wrong.
     * @throws UnavailableDataSourceException if the data source is not ready
     * @throws InvalidArgumentException       if an argument cannot be processed
     * @throws UnexpectedResultException      if the result cannot be processed
     */
    public abstract Object read(BOID id, E extra)
            throws UnavailableDataSourceException,
            InvalidArgumentException,
            UnexpectedResultException;

    /**
     * Updates the business object
     *
     * @param businessObject the business object that needs to be updated on the external system
     * @param extra          extra data required by SpareRoom. Can be <code>null</code> if required.
     * @throws UnavailableDataSourceException if the data source is not ready
     * @throws InvalidArgumentException       if an argument cannot be processed
     * @throws UnexpectedResultException      if the result cannot be processed
     */
    public abstract Object update(BO businessObject, E extra)
            throws UnavailableDataSourceException,
            InvalidArgumentException,
            UnexpectedResultException;

    /**
     * Deletes the business object
     *
     * @param businessObject the business object to be deleted on the external system
     * @param extra          extra data required by SpareRoom. Can be <code>null</code> if required.
     * @throws UnavailableDataSourceException if the data source is not ready
     * @throws InvalidArgumentException       if an argument cannot be processed
     * @throws UnexpectedResultException      if the result cannot be processed
     */
    public abstract Object delete(BO businessObject, E extra)
            throws UnavailableDataSourceException,
            InvalidArgumentException,
            UnexpectedResultException;
}
