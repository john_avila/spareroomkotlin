package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IUpgradeListDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.UpgradeListJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.UpgradePlanRestService;
import com.spareroom.model.business.Parameters;

import java.util.List;

public class UpgradeListDAO implements IUpgradeListDAO {

    private final UpgradePlanRestService upgradePlanRestService;

    UpgradeListDAO() {
        upgradePlanRestService = new UpgradePlanRestService();
    }

    @Override
    public List<String> read(Parameters p) throws AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException {

        String response = upgradePlanRestService.getAvailablePlans(p);

        UpgradeListJSONParser parser = new UpgradeListJSONParser();

        return parser.parse(response);
    }

}
