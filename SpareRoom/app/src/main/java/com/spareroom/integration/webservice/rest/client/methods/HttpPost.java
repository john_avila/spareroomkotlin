package com.spareroom.integration.webservice.rest.client.methods;

import android.app.Application;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.legacy.rest.apiv1.util.UpgradeSessionJSONParser;
import com.spareroom.ui.util.ConnectivityChecker;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import okhttp3.*;

public class HttpPost extends HttpRequest {

    private final String[] names;
    private final String[] values;

    public HttpPost(SpareroomContext spareroomContext, UpgradeSessionJSONParser sessionParser, OkHttpClient okHttpClient,
                    String userAgent, ConnectivityChecker connectivityChecker, Application application,
                    String url, String[] names, String[] values) {
        super(spareroomContext, sessionParser, okHttpClient, userAgent, connectivityChecker, application, url);
        this.names = names;
        this.values = values;
    }

    @Override
    public Request getRequest() throws Exception {
        String requestBody = getUrlEncodedRequestBody();

        return new Request.Builder()
                .url(getUrl())
                .addHeader(HEADER_CHARSET, CHARSET_UTF8)
                .addHeader(HEADER_USER_AGENT, getUserAgent())
                .cacheControl(CacheControl.FORCE_NETWORK)
                .post(RequestBody.create(MEDIA_TYPE_FORM_URL_ENCODED, requestBody))
                .build();
    }

    String getUrlEncodedRequestBody() throws UnsupportedEncodingException {
        StringBuilder requestBody = new StringBuilder();
        for (int i = 0; i < names.length; i++) {
            if (values[i] != null) {
                requestBody.append(names[i]);
                requestBody.append("=");
                requestBody.append(URLEncoder.encode(values[i], CHARSET_UTF8));
            }

            requestBody.append((i != (names.length - 1)) ? "&" : "");
        }

        return requestBody.toString();
    }
}
