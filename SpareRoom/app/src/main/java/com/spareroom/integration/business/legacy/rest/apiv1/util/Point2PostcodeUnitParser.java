package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.SpareroomStatus;

import org.json.JSONException;
import org.json.JSONObject;

public class Point2PostcodeUnitParser {
    /**
     * Parses a postcode
     *
     * @param json String containing a postcode
     * @return String containing the raw value for the postcode or SpareroomStatus if there was any problem with the request
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public Object parse(String json) throws InvalidJSONFormatException {
        try {
            JSONObject firstNode = new JSONObject(json);

            if (firstNode.has("response_type"))
                if (firstNode.getString("response_type").equals("error")) {
                    SpareroomStatus m = new SpareroomStatus();
                    m.setCode(firstNode.getString("response_id"));
                    m.setMessage(firstNode.getString("response"));
                    return m;
                }

            if (firstNode.has("success")) {
                if (firstNode.getString("success").equals("1")) {
                    if (firstNode.has("postcode_unit")) {
                        String postCode = firstNode.getString("postcode_unit");
                        return postCode;
                    }
                }
            }

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

        throw new InvalidJSONFormatException();
    }

}
