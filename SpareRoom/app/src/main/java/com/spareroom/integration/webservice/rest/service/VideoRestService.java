package com.spareroom.integration.webservice.rest.service;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.Parameters;

public class VideoRestService extends RestService {

    private static final String EXTENSION_MP4 = "mp4";
    private static final String MANAGE_VIDEO = "/flatshare/upload-video.pl";

    public String uploadVideo(String[] names, String[] values, String[] namesMultipart, Object[] valuesMultipart) throws NetworkConnectivityException, ClientErrorException, ServerErrorException {
        return postMultipart(MANAGE_VIDEO, names, values, namesMultipart, valuesMultipart, EXTENSION_MP4);
    }

    public String deleteVideo(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(MANAGE_VIDEO, parameters);
    }

    public String getVideos(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(MANAGE_VIDEO, parameters);
    }
}
