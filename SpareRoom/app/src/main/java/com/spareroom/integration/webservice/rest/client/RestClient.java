package com.spareroom.integration.webservice.rest.client;

import android.app.Application;

import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.legacy.rest.apiv1.util.UpgradeSessionJSONParser;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.client.methods.*;
import com.spareroom.ui.util.ConnectivityChecker;

import java.net.*;
import java.util.List;

import javax.inject.*;

import okhttp3.OkHttpClient;

import static com.spareroom.integration.webservice.rest.client.methods.HttpRequest.USER_AGENT_NAMED_PROVIDER;

@Singleton
public class RestClient {

    private final SpareroomContext spareroomContext;
    private final UpgradeSessionJSONParser sessionParser;
    private final OkHttpClient okHttpClient;
    private final String userAgent;
    private final ConnectivityChecker connectivityChecker;
    private final Application application;

    @Inject
    public RestClient(SpareroomContext spareroomContext, UpgradeSessionJSONParser sessionParser, OkHttpClient okHttpClient,
                      @Named(USER_AGENT_NAMED_PROVIDER) String userAgent, ConnectivityChecker connectivityChecker, Application application) {
        this.spareroomContext = spareroomContext;
        this.sessionParser = sessionParser;
        this.okHttpClient = okHttpClient;
        this.userAgent = userAgent;
        this.connectivityChecker = connectivityChecker;
        this.application = application;

        CookieHandler.setDefault(new CookieManager());
    }

    public String post(String url, String[] names, String[] values) throws NetworkConnectivityException, ClientErrorException, ServerErrorException {
        return new HttpPost(spareroomContext, sessionParser, okHttpClient, userAgent, connectivityChecker, application, url,
                names, values).execute();
    }

    public String postMultipart(String url, String[] names, String[] values, String[] namesMultipart, Object[] valuesMultipart, String extension) throws NetworkConnectivityException, ClientErrorException, ServerErrorException {
        return new HttpPostMultipart(spareroomContext, sessionParser, okHttpClient, userAgent, connectivityChecker, application, url,
                names, values, namesMultipart, valuesMultipart, extension).execute();
    }

    public String get(String url) throws NetworkConnectivityException, ClientErrorException, ServerErrorException {
        return new HttpGet(spareroomContext, sessionParser, okHttpClient, userAgent, connectivityChecker, application, url).execute();
    }

    public List<HttpCookie> getCookieStoreCookies() {
        return ((CookieManager) CookieHandler.getDefault()).getCookieStore().getCookies();
    }

    public void setCookieStoreCookies(List<HttpCookie> lCookies) {
        CookieStore cs = ((CookieManager) CookieHandler.getDefault()).getCookieStore();
        cs.removeAll();
        for (HttpCookie c : lCookies) {
            try {
                cs.add(new URI(AppVersion.flavor().getDomain() + "/"), c);
            } catch (URISyntaxException e) {
                // do nothing, it's ok
            }
        }
    }
}
