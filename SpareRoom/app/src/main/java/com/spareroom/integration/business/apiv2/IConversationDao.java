package com.spareroom.integration.business.apiv2;

import com.spareroom.integration.exception.*;
import com.spareroom.model.business.*;

public interface IConversationDao {

    Conversation getConversation(GetConversationRequest getConversationRequest)
            throws UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException;

    void deleteConversation(DeleteConversationRequest deleteConversationRequest)
            throws UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException;
}
