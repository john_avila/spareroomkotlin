package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.business.SpareroomStatusCode;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPasswordJSONParser {

    /**
     * Parses the response of a forgotten password request
     *
     * @param json String containing the response
     * @return <code>null</code> for successful response; SpareroomStatus if not. If the response
     * has the code 1102 then returns <code>null</code> (successful response).
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public Object parse(String json) throws InvalidJSONFormatException {
        try {
            JSONObject firstNode = new JSONObject(json);
            SpareroomStatus m = new SpareroomStatus();

            if (firstNode.getString("success").equalsIgnoreCase(SpareroomStatusCode.CODE_SUCCESS)) {
                return null;
            } else {
                m.setCode(firstNode.has("response_id")
                        ? firstNode.getString("response_id") : SpareroomStatusCode.CODE_0);
                m.setMessage(firstNode.has("response") ? firstNode.getString("response") : null);
            }

            // For us this response is a successful one. The server sends this response when an
            // email is registered in the DDBB and it has requested three times the reset but, if
            // the email is not in the DDBB this message is not sent ever. With this behaviour way
            // we are letting users know which emails are registered or not.
            // The API message: "We have already sent a password reminder to your email address.
            // If you have not received it yet, please check your junk mail folder in case it has
            // been filtered there by mistake (we advise you to add gemma.craft@spareroom.co.uk to
            // your address book to avoid this in the future)."
            if (m.getCode().equals(SpareroomStatusCode.CODE_1102))
                return null;

            return m;

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

    }
}
