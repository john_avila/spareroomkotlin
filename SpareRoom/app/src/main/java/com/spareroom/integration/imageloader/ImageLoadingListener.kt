package com.spareroom.integration.imageloader

import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import java.util.*

abstract class ImageLoadingListener {

    val requestId: UUID = UUID.randomUUID()
    val requestListener = object : RequestListener<String, GlideDrawable> {

        override fun onResourceReady(resource: GlideDrawable?, model: String?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean,
                                     isFirstResource: Boolean): Boolean {
            onLoadingComplete()
            return false
        }

        override fun onException(e: Exception?, model: String?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
            onLoadingFailed(e)
            return false
        }
    }

    protected open fun onLoadingComplete() {}

    protected open fun onLoadingFailed(exception: Exception?) {}
}
