package com.spareroom.integration.business.apiv2

import com.spareroom.integration.business.apiv2.ApiParams.FORMAT
import com.spareroom.integration.business.apiv2.ApiParams.FORMAT_JSON
import com.spareroom.integration.json.JsonParser
import com.spareroom.integration.webservice.rest.service.UserAdsRestService
import com.spareroom.model.business.MyAdverts
import com.spareroom.model.business.Parameters
import javax.inject.Inject

private const val MAX_PER_PAGE = "max_per_page"
private const val OFFSET = "offset"
private const val EDIT = "edit"
private const val AD_STATUS = "ad_status"
private const val ADVERT_ID = "advert_id"
private const val ADVERT_TYPE = "ad_type"
private const val NEW_STATUS = "new_status"
private const val ACTIVATED = "renewed"
private const val DEACTIVATED = "deactivated"
private const val RENEWED = "renewed"
private const val OFFERED = "offered"
private const val WANTED = "wanted"

class MyAdvertsDao @Inject constructor(private val userAdsRestService: UserAdsRestService, private val parser: JsonParser) : IMyAdvertsDao {

    @Throws(Exception::class)
    override fun getAdverts(offset: Int, maxPerPage: Int): MyAdverts {
        val parameters = Parameters()
        parameters.add(FORMAT, FORMAT_JSON)
        parameters.add(MAX_PER_PAGE, maxPerPage)
        parameters.add(OFFSET, offset)

        val response = userAdsRestService.getAds(parameters)
        return parser.fromJson(response, MyAdverts::class.java)
    }

    @Throws(Exception::class)
    override fun activateAdvert(advertId: String, offered: Boolean) = updateStatus(advertId, offered, ACTIVATED)

    @Throws(Exception::class)
    override fun renewAdvert(advertId: String, offered: Boolean) = updateStatus(advertId, offered, RENEWED)

    @Throws(Exception::class)
    override fun deactivateAdvert(advertId: String, offered: Boolean) = updateStatus(advertId, offered, DEACTIVATED)

    private fun updateStatus(advertId: String, offered: Boolean, newStatus: String): MyAdverts {
        val parameters = Parameters()
        parameters.add(FORMAT, FORMAT_JSON)
        parameters.add(EDIT, AD_STATUS)
        parameters.add(ADVERT_ID, advertId)
        parameters.add(ADVERT_TYPE, if (offered) OFFERED else WANTED)
        parameters.add(NEW_STATUS, newStatus)

        val response = userAdsRestService.updateActiveStatus(parameters)
        return parser.fromJson(response, MyAdverts::class.java)
    }

}