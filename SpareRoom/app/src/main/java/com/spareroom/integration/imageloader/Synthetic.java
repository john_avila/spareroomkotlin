package com.spareroom.integration.imageloader;

import java.lang.annotation.*;

/**
 * Indicates that target's visibility can be relaxed to avoid synthetic methods.
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.FIELD, ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.TYPE})
@interface Synthetic {
}