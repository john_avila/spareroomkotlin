package com.spareroom.integration.exception;

public class InvalidJSONFormatException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -5006379048149128859L;

    public InvalidJSONFormatException() {
        super();
    }

    public InvalidJSONFormatException(String detailMessage) {
        super(detailMessage);
    }

    public InvalidJSONFormatException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public InvalidJSONFormatException(Throwable throwable) {
        super(throwable);
    }

}
