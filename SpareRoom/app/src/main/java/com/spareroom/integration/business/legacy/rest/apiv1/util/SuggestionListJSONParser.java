package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.Suggestion;
import com.spareroom.model.business.SuggestionList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

public class SuggestionListJSONParser {
    /**
     * Parses a List of Suggestions from a JSON String
     *
     * @param json String containing a SuggestionList
     * @return SuggestionList object
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public SuggestionList parse(String json) throws InvalidJSONFormatException {
        SuggestionList suggestionList = new SuggestionList();
        LinkedList<Suggestion> suggestions = new LinkedList<>();

        try {
            JSONObject firstNode = new JSONObject(json);
            // String response_type = firstNode.getString("response_type");

            JSONArray results = firstNode.getJSONArray("options");

            for (int i = 0; i < results.length(); i++) {
                Suggestion suggestion = new Suggestion();
                suggestion.set_location(results.getString(i));
                suggestions.add(suggestion);
            }

            suggestionList.set_suggestion(suggestions);
            return suggestionList;

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

    }
}
