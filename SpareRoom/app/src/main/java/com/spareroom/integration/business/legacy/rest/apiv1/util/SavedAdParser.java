package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.SpareroomStatus;

import org.json.JSONException;
import org.json.JSONObject;

public class SavedAdParser {
    /**
     * Parses the response of saving an ad
     *
     * @param json String containing the response
     * @return a message stating whether the operation was successful or not
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public SpareroomStatus parse(String json) throws InvalidJSONFormatException {
        try {
            JSONObject firstNode = new JSONObject(json);

            if (firstNode.has("error")) { // error
                SpareroomStatus im = new SpareroomStatus();
                if (firstNode.has("error_id"))
                    im.setCode(firstNode.getString("error_id"));
                im.setMessage(firstNode.getString("error"));
                return im;
            } else if (firstNode.has("response_type") && firstNode.getString("response_type").equals("error")) { // error
                SpareroomStatus im = new SpareroomStatus();
                if (firstNode.has("response_id"))
                    im.setCode(firstNode.getString("response_id"));
                if (firstNode.has("response"))
                    im.setMessage(firstNode.getString("response"));
                return im;
            } else { // ad saved
                if (firstNode.has("result") && firstNode.getString("result").equals("added")) {
                    SpareroomStatus im = new SpareroomStatus();
                    if (firstNode.has("response"))
                        im.setMessage(firstNode.getString("response"));
                    return im;
                }
                throw new InvalidJSONFormatException();
            }

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }
    }
}
