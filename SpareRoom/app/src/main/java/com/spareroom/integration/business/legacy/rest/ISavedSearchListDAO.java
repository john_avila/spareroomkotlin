package com.spareroom.integration.business.legacy.rest;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SavedSearchList;

public interface ISavedSearchListDAO {

    SavedSearchList read(Parameters parameters) throws InvalidJSONFormatException, AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException;

}
