package com.spareroom.integration.business.legacy.rest.apiv1;

public class SearchParamEnum {
    public static final String PAGE = "page";
    public static final String MAX_PER_PAGE = "max_per_page";
    public static final String OFFSET = "offset";
    public static final String FEATURED = "show_featured_ads";
    public static final String SORTING = "sort_by";

    public static final String USER_ID = "user_id";
    public static final String FILTER = "filter";
    public static final String SHORTLIST = "shortlist";

    public static final String WHERE = "where";

    public static final String KEYWORD = "keyword";
    public static final String HIDE_UNSUITABLE = "hide_unsuitable";
    public static final String GAY_SHARE = "gayshare";
    public static final String PHOTO_ADS_ONLY = "photoadsonly";
    public static final String PER = "per";
    public static final String MIN_RENT = "min_rent";
    public static final String MAX_RENT = "max_rent";
    public static final String MIN_AGE_REQ = "min_age_req";
    public static final String MAX_AGE_REQ = "max_age_req";
    public static final String MIN_TERM = "min_term";
    public static final String MAX_TERM = "max_term";
    public static final String DAYS_OF_WEEK_AVAILABLE = "days_of_wk_available";
    public static final String SMOKING = "smoking";
    public static final String ROOM_TYPES = "room_types";
    public static final String GENDER_FILTER = "genderfilter";
    public static final String SHARE_TYPE = "share_type";
    public static final String AVAILABLE_FROM = "available_from";
    public static final String AVAILABLE_SEARCH = "available_search";

    public static final String LATITUDE = "latitude";
    public static final String LATITUDE_DELTA = "latitude_delta";
    public static final String LONGITUDE = "longitude";
    public static final String LONGITUDE_DELTA = "longitude_delta";
    public static final String SHOW_ROOMS = "showme_rooms";
    public static final String SHOW_STUDIOS = "showme_1beds";
    public static final String SHOW_WHOLE_PROPERTY = "showme_buddyup_properties";
    public static final String PARKING = "parking";
    public static final String DISABLED_ACCESS = "disabled_access";
    public static final String EN_SUITE = "ensuite";
    public static final String BENEFITS = "dss";
    public static final String PETS = "pets_req";
    public static final String LIVING_ROOM = "living_room";
    public static final String SHARED = "shared";
    public static final String LOCATION_TYPE = "location_type";
    public static final String BILLS_INC = "bills_inc";
    public static final String SHORT_LETS_CONSIDERED = "short_lets_considered";
    public static final String MILES_FROM_MAX = "miles_from_max";
    public static final String NO_OF_ROOMS = "no_of_rooms";
    public static final String MIN_BEDS = "min_beds";
    public static final String MAX_BEDS = "max_beds";
    public static final String LANDLORD = "landlord";
    public static final String ROOMS_FOR = "rooms_for";
    public static final String POSTED_BY = "posted_by";
    public static final String VEGETARIANS = "vegetarians";

    public static final String COUPLES = "couples";
    public static final String MAX_OTHER_AREAS = "max_other_areas";
    public static final String MAX_COMMUTE_TIME = "max_commute_time";

}
