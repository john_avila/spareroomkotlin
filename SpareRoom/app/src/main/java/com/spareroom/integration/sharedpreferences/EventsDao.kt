package com.spareroom.integration.sharedpreferences

import android.app.Application
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.spareroom.ui.util.DateUtils
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

data class UserEvents(
    val searchCount: Int,
    val saveAdvertCount: Int,
    val messageAdvertCount: Int,
    val daysOfUsage: Int,
    val initialRatingPopupShownDate: Calendar?,
    val reminderRatingPopupShownDate: Calendar?,
    val appRated: Boolean)

class EventsDao @Inject constructor(context: Application) {

    private val dateFormatter = SimpleDateFormat(DateUtils.YEAR_MONTH_DAY_FORMAT, Locale.US)
    private val prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE)

    companion object {
        const val PREFS_NAME = "events"
        const val SEARCH = "search"
        const val SAVE_ADVERT = "save_advert"
        const val MESSAGE_ADVERT = "message_advert"
        const val LAST_DATE_OF_USAGE = "last_date_of_usage"
        const val DAYS_OF_USAGE = "days_of_usage"
        const val INITIAL_RATING_POPUP_SHOWN_DATE = "initial_rating_popup_shown_date"
        const val REMINDER_RATING_POPUP_SHOWN_DATE = "reminder_rating_popup_shown_date"
        const val RATE_APP = "rate_app"
    }

    fun logSearchEvent() {
        updatePrefs { it.putInt(SEARCH, getIntFromPrefs(SEARCH).inc()) }
    }

    fun logSaveAdvertEvent() {
        updatePrefs { it.putInt(SAVE_ADVERT, getIntFromPrefs(SAVE_ADVERT).inc()) }
    }

    fun logMessageAdvertEvent() {
        updatePrefs { it.putInt(MESSAGE_ADVERT, getIntFromPrefs(MESSAGE_ADVERT).inc()) }
    }

    fun logShowInitialRatingPopupEvent(date: Calendar) {
        updatePrefs { it.putString(INITIAL_RATING_POPUP_SHOWN_DATE, formatDate(date)) }
    }

    fun logShowReminderRatingPopupEvent(date: Calendar) {
        updatePrefs { it.putString(REMINDER_RATING_POPUP_SHOWN_DATE, formatDate(date)) }
    }

    fun logRateAppEvent() {
        updatePrefs { it.putBoolean(RATE_APP, true) }
    }

    fun logDaysOfUseEvent(dateOfUsage: Calendar) {
        val isFirstUsage = LAST_DATE_OF_USAGE !in prefs
        val lastUsage = getDateFromPrefs(LAST_DATE_OF_USAGE, dateOfUsage)
        DateUtils.clearTime(dateOfUsage)
        DateUtils.clearTime(lastUsage)
        val timeDifferenceInDays = DateUtils.getTimeDifferenceInDays(dateOfUsage, lastUsage)

        if (isFirstUsage || timeDifferenceInDays > 0) {
            updatePrefs {
                it.putString(LAST_DATE_OF_USAGE, dateFormatter.format(dateOfUsage.time))
                it.putInt(DAYS_OF_USAGE, getIntFromPrefs(DAYS_OF_USAGE).inc())
            }
        }
    }

    fun getEvents(): UserEvents {
        return UserEvents(
            getIntFromPrefs(SEARCH),
            getIntFromPrefs(SAVE_ADVERT),
            getIntFromPrefs(MESSAGE_ADVERT),
            getIntFromPrefs(DAYS_OF_USAGE),
            getDateFromPrefs(INITIAL_RATING_POPUP_SHOWN_DATE),
            getDateFromPrefs(REMINDER_RATING_POPUP_SHOWN_DATE),
            getBooleanFromPrefs(RATE_APP))
    }

    private fun getDateFromPrefs(key: String, defaultDate: Calendar? = null): Calendar? {
        if (key !in prefs)
            return defaultDate

        val date = Calendar.getInstance()
        date.time = dateFormatter.parse(prefs.getString(key, ""))
        return date
    }

    private inline fun updatePrefs(editPrefs: (SharedPreferences.Editor) -> Unit) {
        val editor = prefs.edit()
        editPrefs(editor)
        editor.apply()
    }

    private fun getIntFromPrefs(key: String) = prefs.getInt(key, 0)
    private fun getBooleanFromPrefs(key: String) = prefs.getBoolean(key, false)
    private fun formatDate(date: Calendar) = dateFormatter.format(date.time)
}
