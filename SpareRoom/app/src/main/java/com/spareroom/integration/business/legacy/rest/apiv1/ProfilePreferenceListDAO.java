package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IProfilePreferenceListDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.Authenticator;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.ProfilePreferenceListJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.UserDetailsRestService;
import com.spareroom.model.business.Parameters;

public class ProfilePreferenceListDAO implements IProfilePreferenceListDAO {

    private final UserDetailsRestService userDetailsRestService;

    ProfilePreferenceListDAO() {
        userDetailsRestService = new UserDetailsRestService();
    }

    public Object read() throws NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException {

        Parameters parameters = new Parameters();
        parameters.add("format", "json");

        String response = userDetailsRestService.getUserDetails(parameters);

        ProfilePreferenceListJSONParser parser = new ProfilePreferenceListJSONParser();

        return parser.parse(response);
    }

    public Object update(Parameters params) throws NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException {
        Authenticator authenticator = new Authenticator();
        params.add("api_sig", authenticator.authenticate(params));
        params.add("api_key", authenticator.get_apiKey());
        params.add("format", "json");

        String[] names = params.getParameterNames();
        String[] values = new String[names.length];

        for (int i = 0; i < names.length; i++) {
            names[i] = names[i];
            values[i] = params.get(names[i]);
        }

        Parameters getParameters = new Parameters();
        getParameters.add("format", "json");
        getParameters.add(params);

        String response = userDetailsRestService.updateUserDetails(names, values);

        ProfilePreferenceListJSONParser parser = new ProfilePreferenceListJSONParser();

        return parser.parse(response);

    }
}
