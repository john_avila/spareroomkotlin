package com.spareroom.integration.sharedpreferences;

import android.content.Context;

import com.spareroom.model.business.SearchAutocompleteList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

/**
 * DAO for SearchSuggestionList. Stores SearchSuggestionList as serialised objects. All suggestions
 * are stored under the folder autocomplete/ with file name being the string searched
 *
 * @author manuel
 */
public class SearchSuggestionListDAO {
    private Context _context;

    public SearchSuggestionListDAO(Context context) {
        _context = context;
    }

    public void create(String search, SearchAutocompleteList lSearchSuggestion) {
        try {
            CharSequence cs = "\\/";
            if (search.contains(cs)) {
                _context.deleteFile(search);
                FileOutputStream fos = _context.openFileOutput(search, Context.MODE_PRIVATE);
                ObjectOutputStream os = new ObjectOutputStream(fos);
                os.writeObject(lSearchSuggestion);
                os.close();
                fos.close();
            }
        } catch (FileNotFoundException e) {
            // do nothing
        } catch (IOException e) {
            _context.deleteFile(search);
        }
    }

    public SearchAutocompleteList read(String search) {
        try {
            CharSequence cs = "\\/";
            if (search.contains(cs)) {
                FileInputStream fis = _context.openFileInput(search);
                ObjectInputStream is = new ObjectInputStream(fis);
                SearchAutocompleteList draft = (SearchAutocompleteList) is.readObject();
                is.close();
                fis.close();
                return draft;
            } // else { we ignore it }
        } catch (FileNotFoundException e) {
            // do nothing
        } catch (StreamCorruptedException e) {
            _context.deleteFile(search);
        } catch (IOException e) {
            _context.deleteFile(search);
        } catch (ClassNotFoundException e) {
            _context.deleteFile(search);
        }
        return null;
    }

    public void deleteAll() {
        File index = new File("autocomplete");
        String[] entries = index.list();
        if (entries != null)
            for (String s : entries) {
                File currentFile = new File(index.getPath(), s);
                currentFile.delete();
            }
    }
}
