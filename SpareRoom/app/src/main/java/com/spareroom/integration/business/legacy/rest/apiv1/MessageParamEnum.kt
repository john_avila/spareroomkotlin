package com.spareroom.integration.business.legacy.rest.apiv1

object MessageParamEnum {
    const val FORMAT = "format"
    const val DELETE_THREAD = "delete_thread"
    const val RE_LISTING = "re_listing"
    const val MESSAGE_ID = "message_id"
    const val MESSAGE = "message"
    const val FORM_ACTION = "formaction"
    const val CONFIRM = "confirm_ok_to_send"
    const val E_TEMPLATE = "etemplate"

    const val AD_ID = "advert_id"
    const val MAX_PER_PAGE = "max_per_page"
    const val AD_TYPE = "ad_type"
    const val MODE = "mode"
    const val SUBMODE = "submode"
}
