package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IPlaceAdAreaDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.PlaceAdAreaAJAXParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.LocationRestService;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.PlaceAdArea;

import java.util.Collection;

public class PlaceAdAreaDAO implements IPlaceAdAreaDAO {

    private final LocationRestService locationRestService;

    PlaceAdAreaDAO() {
        locationRestService = new LocationRestService();
    }
/*
1	London and surrounds
2	East Anglia
3	East Midlands
4	North England
5	North West England
6	South East England
7	South West England
8	West Midlands
9	Yorkshire and Humberside
10	Northern Ireland
11	    Scotland
12	Wales
13	Channel Islands / Isle of Man
*/

    public Collection<PlaceAdArea> read(Parameters parameters) throws NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException {
        Parameters ajaxParams = new Parameters();
        String level = parameters.get("level");
        String levelAjaxStr;

        // The Ajax call does not serve the root level (which is hard-coded in the back-end, so we
        // hard-code it here as well)
        if (level.equals("0")) {
            levelAjaxStr = "watch_gl";
            ajaxParams.add(levelAjaxStr, "1");
        } else if (level.equals("1")) {
            levelAjaxStr = "watch_gl_id";
            ajaxParams.add(levelAjaxStr, parameters.get("id"));
        } else if (level.equals("2")) {
            levelAjaxStr = "watch_gm_id";
            ajaxParams.add(levelAjaxStr, parameters.get("id"));
        }
        ajaxParams.add("ajax", "Y");

        String response = locationRestService.getAreas(ajaxParams);

        PlaceAdAreaAJAXParser parser = new PlaceAdAreaAJAXParser();

        return parser.parse(response, Integer.parseInt(level), parameters.get("parent"));

    }
}
