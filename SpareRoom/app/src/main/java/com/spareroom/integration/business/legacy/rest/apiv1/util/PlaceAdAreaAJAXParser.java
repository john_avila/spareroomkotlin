package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.PlaceAdArea;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class PlaceAdAreaAJAXParser {
    /**
     * Parses the AJAX response for areas in the place ad form
     *
     * @param json   the response
     * @param level  area level: 0 for gl, 1 for gm, 2 for gs
     * @param parent id of the area in the parent level
     * @return array of areas
     * @throws InvalidJSONFormatException
     */
    public Collection<PlaceAdArea> parse(String json, int level, String parent) throws InvalidJSONFormatException {
        String idJsonStr = null;
        String nameJsonStr = null;
        switch (level) {
            case 0:
                idJsonStr = "gl_id";
                nameJsonStr = "gl_name";
                break;
            case 1:
                idJsonStr = "gm_id";
                nameJsonStr = "gm_name";
                break;
            case 2:
                idJsonStr = "gs_id";
                nameJsonStr = "gs_name";
                break;
        }

        List<PlaceAdArea> lArea = new LinkedList<>();

        try {
            JSONArray firstNode = new JSONArray(json);
            for (int i = 0; i < firstNode.length(); i++) {
                JSONObject jsonArea = firstNode.getJSONObject(i);
                PlaceAdArea area = new PlaceAdArea();
                area.setId(jsonArea.getString(idJsonStr));
                area.setName(jsonArea.getString(nameJsonStr));
                area.setParent(parent);
                lArea.add(area);
            }
        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }
        return lArea;
    }
}
