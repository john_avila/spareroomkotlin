package com.spareroom.integration.webservice.rest.service;

import com.spareroom.controller.AppVersion;
import com.spareroom.integration.business.apiv2.ApiParams;
import com.spareroom.integration.business.legacy.rest.apiv1.URIBuilder;
import com.spareroom.integration.business.legacy.rest.apiv1.security.Authenticator;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.dependency.component.ComponentRepository;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.client.RestClient;
import com.spareroom.model.business.Parameters;

import java.net.HttpCookie;
import java.util.List;

import javax.inject.Inject;

public abstract class RestService {

    @Inject
    RestClient restClient;

    @Inject
    Authenticator authenticator;

    @Inject
    URIBuilder uriBuilder;

    RestService() {
        ComponentRepository.get().getAppComponent().inject(this);
    }

    private String getUrl(String uri) {
        return AppVersion.flavor().getDomain() + uri;
    }

    private String getUrl(String uri, String resource) {
        return getUrl(uri) + resource + "/";
    }

    protected String post(String uri, String[] names, String[] values) throws ServerErrorException, NetworkConnectivityException, ClientErrorException {
        return restClient.post(getUrl(uri), names, values);
    }

    protected String post(String uri, Parameters parameters) throws ServerErrorException, NetworkConnectivityException, ClientErrorException, AuthenticationException {
        parameters.add(ApiParams.API_SIGNATURE, authenticator.authenticate(parameters));
        parameters.add(ApiParams.API_KEY, authenticator.get_apiKey());
        return restClient.post(getUrl(uri), parameters.getParameterNames(), parameters.getParameterValues());
    }

    String postMultipart(String uri, String[] names, String[] values, String[] namesMultipart, Object[] valuesMultipart, String extension) throws ServerErrorException, NetworkConnectivityException, ClientErrorException {
        return restClient.postMultipart(getUrl(uri), names, values, namesMultipart, valuesMultipart, extension);
    }

    protected String get(String uri, Parameters parameters) throws AuthenticationException, ServerErrorException, NetworkConnectivityException, ClientErrorException {
        return restClient.get(uriBuilder.buildGetURI(getUrl(uri), parameters));
    }

    protected String get(String uri, String resource, Parameters parameters) throws AuthenticationException, ServerErrorException, NetworkConnectivityException, ClientErrorException {
        return restClient.get(uriBuilder.buildGetURI(getUrl(uri, resource), parameters));
    }

    List<HttpCookie> getCookieStoreCookies() {
        return restClient.getCookieStoreCookies();
    }

    void setCookieStoreCookies(List<HttpCookie> cookies) {
        restClient.setCookieStoreCookies(cookies);
    }

}