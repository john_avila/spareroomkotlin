package com.spareroom.integration.business.apiv2.util;

import com.spareroom.integration.business.apiv2.ApiParams;
import com.spareroom.lib.util.EnumUtil;
import com.spareroom.model.business.*;
import com.spareroom.model.business.SearchAdvertListProperties.*;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.*;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted.Couples;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted.MaxOtherAreas;
import com.spareroom.ui.util.AdvertUtils;
import com.spareroom.ui.util.StringUtils;

import javax.inject.Inject;

import androidx.annotation.*;

import static com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum.*;
import static com.spareroom.lib.util.NumberUtil.MIN_AGE;
import static com.spareroom.lib.util.NumberUtil.isLowerThanMinAge;

public class SearchAdvertListParametersAdapter {
    final static int MAX_PRICE = 99_999;
    final static int MAX_AGE = 99;

    private final AdvertUtils advertUtils;

    @Inject
    public SearchAdvertListParametersAdapter(AdvertUtils advertUtils) {
        this.advertUtils = advertUtils;
    }

    @NonNull
    public SearchAdvertListProperties adapt(@NonNull Parameters parameters, @NonNull SearchType searchType) {
        SearchAdvertListProperties searchAdvertListProperties =
                (searchType == SearchType.OFFERED)
                        ? new SearchAdvertListPropertiesOffered()
                        : new SearchAdvertListPropertiesWanted();

        searchAdvertListProperties.setFeatured(ApiParams.YES_INT.equalsIgnoreCase(parameters.get(FEATURED)));

        searchAdvertListProperties.setWhere(parameters.get(WHERE));
        searchAdvertListProperties.setMaxPerPage(parameters.get(MAX_PER_PAGE));
        searchAdvertListProperties.setPage(parameters.get(PAGE));
        searchAdvertListProperties.setAvailableFrom(parameters.get(AVAILABLE_FROM));
        searchAdvertListProperties.setKeyword(parameters.get(KEYWORD));

        if (parameters.has(MIN_AGE_REQ))
            searchAdvertListProperties.setMinAge(checkAge(parameters.get(MIN_AGE_REQ)));
        if (parameters.has(MAX_AGE_REQ))
            searchAdvertListProperties.setMaxAge(checkAge(parameters.get(MAX_AGE_REQ)));

        searchAdvertListProperties.setGayShare(parameters.has(GAY_SHARE));
        searchAdvertListProperties.setPhotoAdvertsOnly(parameters.has(PHOTO_ADS_ONLY));
        searchAdvertListProperties.setIncludeHidden(!parameters.has(HIDE_UNSUITABLE));

        if (SearchAdvertListProperties.PW.equals(parameters.get(PER))) {
            if (parameters.has(MIN_RENT))
                searchAdvertListProperties.minWeeklyRent(advertUtils, checkPrice(parameters.get(MIN_RENT)));
            if (parameters.has(MAX_RENT))
                searchAdvertListProperties.maxWeeklyRent(advertUtils, checkPrice(parameters.get(MAX_RENT)));
        } else {
            if (parameters.has(MIN_RENT))
                searchAdvertListProperties.minMonthlyRent(checkPrice(parameters.get(MIN_RENT)));
            if (parameters.has(MAX_RENT))
                searchAdvertListProperties.maxMonthlyRent(checkPrice(parameters.get(MAX_RENT)));
        }

        searchAdvertListProperties.setSortedBy(EnumUtil.searchItemByValue(SortedBy.values(), parameters.get(SORTING), SortedBy.DEFAULT));
        searchAdvertListProperties.setMinTerm(EnumUtil.searchItemByValue(MinTerm.values(), parameters.get(MIN_TERM), MinTerm.NOT_SET));
        searchAdvertListProperties.setMaxTerm(EnumUtil.searchItemByValue(MaxTerm.values(), parameters.get(MAX_TERM), MaxTerm.NOT_SET));
        searchAdvertListProperties.setDaysOfWeekAvailable(EnumUtil.searchItemByValue(DaysOfWeek.values(), parameters.get(DAYS_OF_WEEK_AVAILABLE), DaysOfWeek.NOT_SET));
        searchAdvertListProperties.setSmoking(EnumUtil.searchItemByValue(Smoking.values(), parameters.get(SMOKING), Smoking.NOT_SET));
        searchAdvertListProperties.setRoomType(EnumUtil.searchItemByValue(RoomTypes.values(), parameters.get(ROOM_TYPES), RoomTypes.NOT_SET));
        searchAdvertListProperties.setShareType(EnumUtil.searchItemByValue(ShareType.values(), parameters.get(SHARE_TYPE), ShareType.NOT_SET));
        searchAdvertListProperties.setGenderFilter(EnumUtil.searchItemByValue(GenderFilter.values(), parameters.get(GENDER_FILTER), GenderFilter.NOT_SET));

        if (searchAdvertListProperties instanceof SearchAdvertListPropertiesOffered) {
            SearchAdvertListPropertiesOffered properties = (SearchAdvertListPropertiesOffered) searchAdvertListProperties;
            Coordinates coordinates = new Coordinates();

            if (parameters.has(MILES_FROM_MAX))
                properties.setMilesFromMax(Integer.parseInt(parameters.get(MILES_FROM_MAX)));
            if (parameters.has(MAX_COMMUTE_TIME))
                properties.setMaxCommuteTime(parameters.get(MAX_COMMUTE_TIME));
            if (parameters.has(LATITUDE))
                coordinates.setLatitude(Double.parseDouble(parameters.get(LATITUDE)));
            if (parameters.has(LATITUDE_DELTA))
                coordinates.setLatitudeDelta(Double.parseDouble(parameters.get(LATITUDE_DELTA)));
            if (parameters.has(LONGITUDE))
                coordinates.setLongitude(Double.parseDouble(parameters.get(LONGITUDE)));
            if (parameters.has(LONGITUDE_DELTA))
                coordinates.setLongitudeDelta(Double.parseDouble(parameters.get(LONGITUDE_DELTA)));
            properties.setCoordinates(coordinates);

            properties.setShowRooms(parameters.has(SHOW_ROOMS));
            properties.setShowStudios(parameters.has(SHOW_STUDIOS));
            properties.setParking(parameters.has(PARKING));
            properties.setDisabledAccess(parameters.has(DISABLED_ACCESS));
            properties.setEnSuite(parameters.has(EN_SUITE));
            properties.setBenefits(parameters.has(BENEFITS));
            properties.setPets(parameters.has(PETS));
            properties.setShowWholeProperty(parameters.has(SHOW_WHOLE_PROPERTY));
            properties.setLivingRoom(parameters.has(LIVING_ROOM));
            properties.billsInc(parameters.has(BILLS_INC));
            properties.setVegetarians(parameters.has(VEGETARIANS));
            properties.setShortLets(parameters.has(SHORT_LETS_CONSIDERED));

            properties.setLocationType(EnumUtil.searchItemByValue(LocationType.values(), parameters.get(LOCATION_TYPE), LocationType.NOT_SET));
            properties.setNoOfRooms(EnumUtil.searchItemByValue(NoOfRooms.values(), parameters.get(NO_OF_ROOMS), NoOfRooms.NOT_SET));
            properties.setMinFlatmates(EnumUtil.searchItemByValue(MinFlatmates.values(), parameters.get(MIN_BEDS), MinFlatmates.NOT_SET));
            properties.maxFlatmates(EnumUtil.searchItemByValue(MaxFlatmates.values(), parameters.get(MAX_BEDS), MaxFlatmates.NOT_SET));
            properties.setLandlord(EnumUtil.searchItemByValue(Landlord.values(), parameters.get(LANDLORD), Landlord.NOT_SET));
            properties.setRoomsFor(EnumUtil.searchItemByValue(RoomsFor.values(), parameters.get(ROOMS_FOR), RoomsFor.NOT_SET));
            properties.setPostedBy(EnumUtil.searchItemByValue(PostedBy.values(), parameters.get(POSTED_BY), PostedBy.NOT_SET));

        } else {
            SearchAdvertListPropertiesWanted properties = (SearchAdvertListPropertiesWanted) searchAdvertListProperties;

            properties.setUserId(parameters.get(USER_ID));
            properties.setCouples(EnumUtil.searchItemByValue(Couples.values(), parameters.get(COUPLES), Couples.NOT_SET));
            properties.setMaxOtherAreas(EnumUtil.searchItemByValue(MaxOtherAreas.values(), parameters.get(MAX_OTHER_AREAS), MaxOtherAreas.NOT_SET));
        }

        return searchAdvertListProperties;
    }

    @VisibleForTesting
    @Nullable
    Integer checkPrice(@Nullable String parsedPrice) {
        Double price = StringUtils.toDouble(parsedPrice);

        if (price == null || price <= 0)
            return null;
        if (price > MAX_PRICE)
            return MAX_PRICE;
        return price.intValue();
    }

    @VisibleForTesting
    @Nullable
    Integer checkAge(@Nullable String parsedAge) {
        Double age = StringUtils.toDouble(parsedAge);

        if (age == null || age <= 0)
            return null;
        if (age > MAX_AGE)
            return MAX_AGE;
        if (isLowerThanMinAge(age.intValue()))
            return MIN_AGE;
        return age.intValue();
    }

}
