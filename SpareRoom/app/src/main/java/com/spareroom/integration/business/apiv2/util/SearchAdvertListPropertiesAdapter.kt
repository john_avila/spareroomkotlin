package com.spareroom.integration.business.apiv2.util

import com.spareroom.integration.business.apiv2.ApiParams
import com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum.*
import com.spareroom.model.business.*
import com.spareroom.model.business.Coordinates.Constants.COORDINATES_DEFAULT_VALUE
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.*
import com.spareroom.ui.util.StringUtils

class SearchAdvertListPropertiesAdapter {

    fun adapt(searchAdvertListProperties: SearchAdvertListProperties, useCoordinates: Boolean = false): Parameters {
        val parameters = Parameters()

        parameters.add(MIN_RENT, searchAdvertListProperties.minRent)
        parameters.add(MAX_RENT, searchAdvertListProperties.maxRent)
        parameters.add(MIN_AGE_REQ, searchAdvertListProperties.minAge)
        parameters.add(MAX_AGE_REQ, searchAdvertListProperties.maxAge)
        parameters.add(KEYWORD, searchAdvertListProperties.keyword)

        parameters.add(FEATURED, if (searchAdvertListProperties.featured) ApiParams.YES_INT else null)
        parameters.add(GAY_SHARE, if (searchAdvertListProperties.gayShare) ApiParams.YES else null)
        parameters.add(PHOTO_ADS_ONLY, if (searchAdvertListProperties.photoAdvertsOnly) ApiParams.YES else null)
        parameters.add(WHERE, searchAdvertListProperties.where)
        parameters.add(MAX_PER_PAGE, searchAdvertListProperties.maxPerPage)
        parameters.add(PAGE, searchAdvertListProperties.page)
        parameters.add(SORTING, searchAdvertListProperties.sortedBy.value)
        parameters.add(PER, searchAdvertListProperties.per())
        parameters.add(MIN_TERM, searchAdvertListProperties.minTerm.value)
        parameters.add(DAYS_OF_WEEK_AVAILABLE, searchAdvertListProperties.daysOfWeekAvailable.value)
        parameters.add(MAX_TERM, searchAdvertListProperties.maxTerm.value)
        parameters.add(SMOKING, searchAdvertListProperties.smoking.value)
        parameters.add(ROOM_TYPES, searchAdvertListProperties.roomType.value)
        parameters.add(SHARE_TYPE, searchAdvertListProperties.shareType.value)
        parameters.add(HIDE_UNSUITABLE, if (!searchAdvertListProperties.includeHidden) ApiParams.YES_INT else null)
        parameters.add(AVAILABLE_SEARCH, if (!StringUtils.isNullOrEmpty(searchAdvertListProperties.availableFrom)) ApiParams.YES else null)
        parameters.add(AVAILABLE_FROM, searchAdvertListProperties.availableFrom)
        parameters.add(GENDER_FILTER, searchAdvertListProperties.genderFilter.value)

        if (searchAdvertListProperties is SearchAdvertListPropertiesOffered) {

            parameters.add(SHOW_ROOMS, if (searchAdvertListProperties.showRooms) ApiParams.YES else null)
            parameters.add(SHOW_STUDIOS, if (searchAdvertListProperties.showStudios) ApiParams.YES else null)
            parameters.add(SHOW_WHOLE_PROPERTY, if (searchAdvertListProperties.showWholeProperty) ApiParams.YES else null)
            parameters.add(PARKING, if (searchAdvertListProperties.parking) ApiParams.YES else null)
            parameters.add(DISABLED_ACCESS, if (searchAdvertListProperties.disabledAccess) ApiParams.YES else null)
            parameters.add(EN_SUITE, if (searchAdvertListProperties.enSuite) ApiParams.YES else null)
            parameters.add(BENEFITS, if (searchAdvertListProperties.benefits) ApiParams.YES else null)
            parameters.add(PETS, if (searchAdvertListProperties.pets) ApiParams.YES else null)
            parameters.add(LIVING_ROOM, if (searchAdvertListProperties.livingRoom) SHARED else null)
            parameters.add(LOCATION_TYPE, searchAdvertListProperties.locationType.value)
            parameters.add(BILLS_INC, if (searchAdvertListProperties.billsInc()) BillsInc.YES.value else null)
            parameters.add(SHORT_LETS_CONSIDERED, if (searchAdvertListProperties.shortLets) ApiParams.YES else null)
            parameters.add(MAX_COMMUTE_TIME, searchAdvertListProperties.maxCommuteTime)
            parameters.add(MILES_FROM_MAX, searchAdvertListProperties.milesFromMax)
            parameters.add(NO_OF_ROOMS, searchAdvertListProperties.noOfRooms.value)
            parameters.add(LANDLORD, searchAdvertListProperties.landlord.value)
            parameters.add(ROOMS_FOR, searchAdvertListProperties.roomsFor.value)
            parameters.add(POSTED_BY, searchAdvertListProperties.postedBy.value)
            parameters.add(VEGETARIANS, if (searchAdvertListProperties.vegetarians) ApiParams.YES else null)

            if (useCoordinates) {
                val coordinates: Coordinates = searchAdvertListProperties.coordinates

                if (coordinates.latitude != COORDINATES_DEFAULT_VALUE)
                    parameters.add(LATITUDE, coordinates.latitude.toString())
                if (coordinates.latitudeDelta != COORDINATES_DEFAULT_VALUE)
                    parameters.add(LATITUDE_DELTA, coordinates.latitudeDelta.toString())
                if (coordinates.longitude != COORDINATES_DEFAULT_VALUE)
                    parameters.add(LONGITUDE, coordinates.longitude.toString())
                if (coordinates.longitudeDelta != COORDINATES_DEFAULT_VALUE)
                    parameters.add(LONGITUDE_DELTA, coordinates.longitudeDelta.toString())
            }

            if (searchAdvertListProperties.minFlatmates != MinFlatmates.NOT_SET)
                parameters.add(MIN_BEDS, searchAdvertListProperties.minFlatmates.value)
            if (searchAdvertListProperties.maxFlatmates() != MaxFlatmates.MORE_THAN_SIX)
                parameters.add(MAX_BEDS, searchAdvertListProperties.maxFlatmates().value)

        } else {
            val properties = searchAdvertListProperties as SearchAdvertListPropertiesWanted

            parameters.add(USER_ID, properties.userId)
            parameters.add(COUPLES, properties.couples.value)
            parameters.add(MAX_OTHER_AREAS, properties.maxOtherAreas.value)

        }

        return parameters
    }

}
