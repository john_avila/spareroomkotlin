package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IPictureDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.DeletePictureJSONParser;
import com.spareroom.integration.business.legacy.rest.apiv1.util.UpdatePictureJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.PictureRestService;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SpareroomStatus;

class PictureDAO implements IPictureDAO {

    private final PictureRestService pictureRestService;

    PictureDAO() {
        pictureRestService = new PictureRestService();
    }

    public SpareroomStatus update(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException {

        params.add("format", "json");
        params.add("function", "save");

        String response = pictureRestService.updatePicture(params);

        UpdatePictureJSONParser parser = new UpdatePictureJSONParser();

        return parser.parse(response);
    }

    public SpareroomStatus delete(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException {
        Parameters p = new Parameters();
        p.add("format", "json");
        p.add("function", "delete");

        if (params.get("photo_id") != null)
            p.add("photo_id", params.get("photo_id"));

        if (params.get("flatshare_id") != null)
            p.add("flatshare_id", params.get("flatshare_id"));

        if (params.get("flatshare_type") != null)
            p.add("flatshare_type", params.get("flatshare_type"));

        String response = pictureRestService.deletePicture(p);

        DeletePictureJSONParser parser = new DeletePictureJSONParser();

        return parser.parse(response);
    }

}
