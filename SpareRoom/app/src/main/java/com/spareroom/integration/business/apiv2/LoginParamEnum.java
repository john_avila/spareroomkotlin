package com.spareroom.integration.business.apiv2;

/**
 * Enum of parameters used for Parameter object
 */
class LoginParamEnum {

    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String REMEMBER_ME = "remember_me";
    public static final String PAGE_JOINED_FROM = "page_joined_from";
    public static final String MY_VERSION = "my_version";

}
