package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IAdOfferedCompleteDao;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.AdDetailsSheetJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.json.JsonParser;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.service.AdRestService;
import com.spareroom.model.business.*;

import javax.inject.Inject;

public class AdOfferedCompleteDao implements IAdOfferedCompleteDao {
    private final AdRestService adRestService;
    private final JsonParser parser;

    @Inject
    AdOfferedCompleteDao(AdRestService adRestService, JsonParser parser) {
        this.adRestService = adRestService;
        this.parser = parser;
    }

    public void create(DraftAd draft) {

    }

    public AdOffered read(String advertId) throws AuthenticationException, NetworkConnectivityException, InvalidJSONFormatException, ClientErrorException, ServerErrorException {
        Parameters p = new Parameters();
        p.add("format", "json");

        String response = adRestService.getAdDetails(advertId, p, SearchType.OFFERED);

        AdOffered adComplete = parser.fromJson(response, AdOfferedCompleteResponse.class).getAdvert();

        if (adComplete != null) {
            AdDetailsSheetJSONParser sheetParser = new AdDetailsSheetJSONParser();
            adComplete.setSheet(sheetParser.parse(response, false));
        }

        return adComplete;
    }

}
