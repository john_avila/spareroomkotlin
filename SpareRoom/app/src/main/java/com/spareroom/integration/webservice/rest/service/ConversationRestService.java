package com.spareroom.integration.webservice.rest.service;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.model.business.Parameters;

import javax.inject.Inject;

public class ConversationRestService extends RestService {

    private static final String CONVERSATIONS = "/flatshare/mythreads.pl";
    private static final String MESSAGE = "/flatshare/myemailreply.pl";

    @Inject
    public ConversationRestService() {

    }

    public String getConversation(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(CONVERSATIONS, parameters);
    }

    public String deleteConversation(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(CONVERSATIONS, parameters);
    }

    public String getConversationsList(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(CONVERSATIONS, parameters);
    }

    public String sendMessage(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return post(MESSAGE, parameters);
    }
}
