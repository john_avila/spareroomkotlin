package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.ISavedSearchDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.SavedSearchParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.AdSearchesRestService;
import com.spareroom.model.business.Parameters;

public class SavedSearchDAO implements ISavedSearchDAO {

    private final AdSearchesRestService adSearchesRestService;

    SavedSearchDAO() {
        adSearchesRestService = new AdSearchesRestService();
    }

    public Object create(Parameters parameters) throws AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException {

        parameters.add("format", "json");
        parameters.add("override_default_alert_preferences", "Y");

        String response = adSearchesRestService.updateSavedSearch(parameters);

        SavedSearchParser parser = new SavedSearchParser();

        return parser.parse(response);

    }

    public Object update(Parameters parameters) throws AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException {
        return create(parameters);
    }

    public Object delete(Parameters parameters) throws AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException {
        return create(parameters);
    }
}
