package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SavedSearch;
import com.spareroom.model.business.SavedSearchList;
import com.spareroom.model.business.SearchType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import static com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum.LOCATION_TYPE;
import static com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum.MAX_OTHER_AREAS;
import static com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum.MILES_FROM_MAX;

public class SavedSearchListParser {

    public SavedSearchList parse(String json) throws InvalidJSONFormatException {
        try {
            JSONObject firstNode = new JSONObject(json);
            SavedSearchList lSavedSearch = new SavedSearchList();

            if (firstNode.has("saved_searches")) {
                JSONArray jsonSavedSearchList = firstNode.getJSONArray("saved_searches");

                for (int i = 0; i < jsonSavedSearchList.length(); i++) {
                    JSONObject jsonSavedSearch = jsonSavedSearchList.getJSONObject(i);

                    SavedSearch savedSearch = new SavedSearch();

                    if (jsonSavedSearch.has("where"))
                        savedSearch.set_location(jsonSavedSearch.getString("where"));
                    if (jsonSavedSearch.has("search_name")) {
                        savedSearch.set_name(jsonSavedSearch.getString("search_name"));
                    }

                    if (jsonSavedSearch.has("params")) {
                        JSONArray jsonParamDesc = jsonSavedSearch.getJSONArray("params");
                        LinkedList<String> paramDesc = new LinkedList<>();
                        for (int j = 0; j < jsonParamDesc.length(); j++) {
                            if (jsonParamDesc.getString(j) != null && (!jsonParamDesc.getString(j).equals("null")))
                                paramDesc.add(jsonParamDesc.getString(j));
                        }
                        savedSearch.set_paramDesc(paramDesc);
                    }

                    Parameters parameters = new Parameters();
                    if (jsonSavedSearch.has("refine_options")) {
                        JSONArray jsonRefineOptions = jsonSavedSearch.getJSONArray("refine_options");
                        for (int j = 0; j < jsonRefineOptions.length(); j++) {
                            JSONObject refineOption = jsonRefineOptions.getJSONObject(j);
                            if (refineOption.has("additional_params")) {
                                JSONObject jsonAdditionalParameters = refineOption.getJSONObject("additional_params");
                                JSONArray jsonNames = jsonAdditionalParameters.names();
                                for (int k = 0; k < jsonNames.length(); k++) {
                                    String value = jsonAdditionalParameters.optString(jsonNames.getString(k));
                                    parameters.add(jsonNames.getString(k), "null".equals(value) ? null : value);
                                }
                            }
                        }
                    }
                    if (jsonSavedSearch.has("location_options")) {
                        JSONObject jsonObject = jsonSavedSearch.getJSONObject("location_options");
                        savedSearch.setMilesFromMax(jsonObject.optString(MILES_FROM_MAX));
                        parameters.add(LOCATION_TYPE, jsonObject.optString(LOCATION_TYPE));
                        parameters.add(MAX_OTHER_AREAS, jsonObject.optString(MAX_OTHER_AREAS));
                    }
                    savedSearch.set_parameters(parameters);

                    if (jsonSavedSearch.has("search_id")) {
                        savedSearch.set_searchId(jsonSavedSearch.getString("search_id"));
                    }

                    if (jsonSavedSearch.has("daily_email_alerts_activated")) {
                        savedSearch.set_dailyEmails(jsonSavedSearch.getString("daily_email_alerts_activated").equals("Y"));
                    }

                    if (jsonSavedSearch.has("instant_email_alerts_activated")) {
                        savedSearch.set_instantEmails((jsonSavedSearch.getString("instant_email_alerts_activated")).equals("Y"));
                    }

                    if (jsonSavedSearch.has("search_type")) {
                        savedSearch.set_searchType((jsonSavedSearch.getString("search_type").equals("offered")) ? SearchType.OFFERED : SearchType.WANTED);
                    }

                    lSavedSearch.add(savedSearch);
                }
            }

            return lSavedSearch;

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }
    }

}
