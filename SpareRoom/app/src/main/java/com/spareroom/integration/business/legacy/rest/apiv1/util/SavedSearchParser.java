package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.SavedSearchList;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.business.SpareroomStatusCode;

import org.json.JSONException;
import org.json.JSONObject;

public class SavedSearchParser {
    /**
     * Parses the response of saving an ad
     *
     * @param json String containing the response
     * @return a message stating whether the operation was successful or not
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public Object parse(String json) throws InvalidJSONFormatException {
        try {
            JSONObject firstNode = new JSONObject(json);

            if (firstNode.has("response_type")) {
                String responseType = firstNode.getString("response_type");
                if (responseType.equals("success")) {
                    if (firstNode.has("saved_searches")) {
                        SavedSearchListParser listParser = new SavedSearchListParser();

                        SavedSearchList list = listParser.parse(json);
                        return list;
                    } else {
                        return new SavedSearchList();
                    }

                    //} else if (responseType.equals("warning")) {

                } else if (responseType.equals("error")) {
                    SpareroomStatus im = new SpareroomStatus();
                    if (firstNode.has("error_id")) {
                        im.setCode(SpareroomStatusCode.CODE_ERROR_ID);
                    }
                    if (firstNode.has("error")) {
                        im.setMessage("error");
                    }
                    return im;
                }
            }

            throw new InvalidJSONFormatException();

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }
    }
}
