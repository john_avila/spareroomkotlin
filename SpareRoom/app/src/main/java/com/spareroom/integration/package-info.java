/**
 * Integration Tier. Contains All artifacts for communication with external systems.
 */
package com.spareroom.integration;