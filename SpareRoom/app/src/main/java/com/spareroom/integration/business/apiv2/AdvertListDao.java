package com.spareroom.integration.business.apiv2;

import android.app.Application;

import com.spareroom.App;
import com.spareroom.integration.business.apiv2.util.SearchAdvertListPropertiesAdapter;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.AdBoardJSONParser;
import com.spareroom.integration.business.legacy.rest.apiv1.util.IAdBoardJSONParser;
import com.spareroom.integration.exception.*;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.service.AdRestService;
import com.spareroom.model.business.*;
import com.spareroom.model.business.SearchAdvertListResponse.ResponseTypes;

import javax.inject.Inject;

public class AdvertListDao implements IAdvertListDao {

    private final AdRestService adRestService;
    private Application application;

    @Inject
    AdvertListDao(AdRestService adRestService) {
        this.adRestService = adRestService;
    }

    AdvertListDao(AdRestService adRestService, Application application) {
        this.adRestService = adRestService;
        this.application = application;
    }

    @Override
    public SearchAdvertListResponse getAdvertList(SearchAdvertListProperties searchAdvertListProperties, SearchType searchType, boolean useCoordinates)
            throws UnexpectedResultException, UnavailableDataSourceException, InvalidArgumentException {

        try {
            String response = adRestService.getAdverts(new SearchAdvertListPropertiesAdapter().adapt(searchAdvertListProperties, useCoordinates), searchType);
            if (application == null)
                application = App.get();
            IAdBoardJSONParser parser = new AdBoardJSONParser(searchType, application);
            SearchAdvertListResponse searchAdvertListResponse = new SearchAdvertListResponse();

            searchAdvertListResponse.setResponse(parser.parse(response));
            searchAdvertListResponse.setResponseType(parser.responseType());
            searchAdvertListResponse.setError(parser.responseCode(), parser.responseMessage());

            if (searchAdvertListResponse.getResponseType() == ResponseTypes.SUGGESTION_LIST)
                ((SuggestionList) searchAdvertListResponse.getResponse()).set_location(searchAdvertListProperties.getWhere());

            return searchAdvertListResponse;

        } catch (NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e);
        } catch (ClientErrorException | AuthenticationException e) {
            throw new InvalidArgumentException(e);
        } catch (ServerErrorException | InvalidJSONFormatException e) {
            throw new UnexpectedResultException(e);
        }

    }

}
