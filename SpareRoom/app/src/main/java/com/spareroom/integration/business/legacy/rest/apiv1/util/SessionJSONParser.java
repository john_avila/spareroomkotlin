package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.*;

import org.json.*;

public class SessionJSONParser {
    /**
     * Parses a Session object from a JSON String
     *
     * @param json String containing a Session
     * @return Session object
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public Session parse(String json) throws InvalidJSONFormatException {
        Session s = new Session();

        JSONObject firstNode;
        try {
            firstNode = new JSONObject(json);
            s.set_sessionId(firstNode.getString("session_id"));
            s.set_sessionKey(firstNode.getString("session_key"));
            s.set_sessionType(firstNode.getString("session_type"));

            s.set_facebookUserId(firstNode.getString("facebook_user_id"));
            s.set_facebookSessionKey(firstNode.getString("facebook_session_key"));
            s.set_loggedIn(firstNode.getString("loggedin"));
            s.set_messageCount(firstNode.getString("message_count"));

            if (firstNode.has("profile_photo_url")) {
                s.set_profilePictureUrl(firstNode.getString("profile_photo_url"));
            }
            if (firstNode.has("current_upgrade_product")) {
                s.set_upgradeProduct(firstNode.getString("current_upgrade_product"));
            }
            if (firstNode.has("access_expires_eng")) {
                s.set_upgradeExpire(firstNode.getString("access_expires_eng"));
            }
            if (firstNode.has("upgrade_days_remaining")) {
                s.set_upgradeTimeLeft(firstNode.getString("upgrade_days_remaining"));
            }
            if (firstNode.has("myads_offered_count")) {
                s.set_upgradeNoAdsOffered(firstNode.getString("myads_offered_count"));
            }
            if (firstNode.has("myads_wanted_count")) {
                s.set_upgradeNoAdsWanted(firstNode.getString("myads_wanted_count"));
            }
            if (firstNode.has("paid")) {
                s.set_paid((firstNode.getString("paid").equals("yes")));
            }
            if (firstNode.has("email")) {
                s.set_email(firstNode.getString("email"));
            }
            if (firstNode.has("upgrade_options")) {
                UpgradeOptionList optionList = new UpgradeOptionList();
                JSONObject jsonUpgradeOptions = firstNode.getJSONObject("upgrade_options");

                if (jsonUpgradeOptions.has("105")) {
                    JSONObject jsonOption = jsonUpgradeOptions.getJSONObject("105");
                    optionList.add("105", parseUpgradeOption(jsonOption));
                }

                if (jsonUpgradeOptions.has("46")) {
                    JSONObject jsonOption = jsonUpgradeOptions.getJSONObject("46");
                    optionList.add("46", parseUpgradeOption(jsonOption));
                }

                if (jsonUpgradeOptions.has("47")) {
                    JSONObject jsonOption = jsonUpgradeOptions.getJSONObject("47");
                    optionList.add("47", parseUpgradeOption(jsonOption));
                }

                if (jsonUpgradeOptions.has("99")) {
                    JSONObject jsonOption = jsonUpgradeOptions.getJSONObject("99");
                    optionList.add("99", parseUpgradeOption(jsonOption));
                }

                s.set_upgradeOptions(optionList);
            }
            if (firstNode.has("photo_upload_limit")) {
                s.setPhotoUploadLimit(firstNode.getInt("photo_upload_limit"));
            }
            if (firstNode.has("unix_access_exp")) {
                s.setUnixAccessExp(firstNode.getString("unix_access_exp"));
            }

            s.setUserType(firstNode.optString("usertype"));

            UserJSONParser userParser = new UserJSONParser();
            User u = userParser.parse(firstNode);

            s.set_user(u);

            return s;
        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

    }

    private UpgradeOption parseUpgradeOption(JSONObject jsonOption) throws JSONException {

        UpgradeOption uo = new UpgradeOption();
        if (jsonOption.has("call_to_action")) {
            String callToAction = jsonOption.getString("call_to_action");
            uo.set_callToAction(callToAction);
        }
        if (jsonOption.has("notice")) {
            String notice = jsonOption.getString("notice");
            uo.set_notice(notice);
        }
        if (jsonOption.has("benefits_of_upgrading")) {
            JSONArray jsonBenefits = jsonOption.getJSONArray("benefits_of_upgrading");

            String[][] vBenefits = new String[jsonBenefits.length()][];

            for (int i = 0; i < jsonBenefits.length(); i++) {
                String content = jsonBenefits.getJSONObject(i).getString("content");
                String title = jsonBenefits.getJSONObject(i).getString("title");

                String[] benefit = new String[2];
                benefit[0] = content;
                benefit[1] = title;

                vBenefits[i] = benefit;
            }
            uo.set_benefits(vBenefits);
        }

        return uo;
    }

}
