package com.spareroom.integration.business.apiv2

import com.spareroom.integration.business.apiv2.ApiParams.MAX_PER_PAGE
import com.spareroom.integration.business.apiv2.ApiParams.OFFSET
import com.spareroom.integration.json.JsonParser
import com.spareroom.integration.webservice.rest.service.ConversationRestService
import com.spareroom.model.business.ConversationListRestResponse
import com.spareroom.model.business.Parameters
import javax.inject.Inject

private const val ONE_SIDED_CONVERSATIONS = "show_one_sided_conversations"

class ConversationListDao @Inject constructor(
    private val conversationRestService: ConversationRestService,
    private val parser: JsonParser
) : IConversationListDao {

    @Throws(Exception::class)
    override fun getConversationList(offset: Int, maxPerPage: Int): ConversationListRestResponse {
        val parameters = Parameters()
        parameters.add(MAX_PER_PAGE, maxPerPage)
        parameters.add(OFFSET, offset)
        parameters.add(ONE_SIDED_CONVERSATIONS, ApiParams.YES_INT) // Includes conversations without response
        parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON)

        val response = conversationRestService.getConversationsList(parameters)
        return parser.fromJson(response, ConversationListRestResponse::class.java)
    }

}
