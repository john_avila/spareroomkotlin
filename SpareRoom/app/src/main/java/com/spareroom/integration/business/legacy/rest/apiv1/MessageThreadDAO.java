package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IMessageThreadDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.NewMessageThreadJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.MarkAdRestService;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SpareroomStatus;

public class MessageThreadDAO implements IMessageThreadDAO {
    private final MarkAdRestService markAdRestService;

    MessageThreadDAO() {
        markAdRestService = new MarkAdRestService();
    }

    @Override
    public SpareroomStatus create(Parameters parameters) throws NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException {

        parameters.add("format", "json");

        String response = markAdRestService.markAsContacted(parameters);

        NewMessageThreadJSONParser parser = new NewMessageThreadJSONParser();

        return parser.parse(response);
    }

}
