package com.spareroom.integration.business.apiv2;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.SpareroomAbstractDaoImpl;
import com.spareroom.integration.business.apiv2.util.RegisterJsonParser;
import com.spareroom.integration.business.legacy.rest.apiv1.security.Authenticator;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.*;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.service.LoginRestService;
import com.spareroom.model.business.*;
import com.spareroom.model.extra.RegisterExtra;

/**
 * DAO for the LoginTransaction Business Object (does not exists on the app)
 */
class RegisterTransactionDao extends SpareroomAbstractDaoImpl<AccountRegisterForm, RegisterExtra, AccountConnectionInterface> {

    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    private static final String PASSWORD2 = "password_again";
    private static final String INAGREEMENT = "inagreement";
    private static final String USERTYPE = "usertype_new";
    private static final String MYVERSION = "my_version";
    private static final String PAGE_JOINED_FROM = "page_joined_from";
    private static final String USER_TYPE_OFFERER = "haveashare";
    private static final String USER_TYPE_SEEKER = "lookingforashare";
    private static final String USER_TYPE_OFFERER_AND_SEEKER = "haveashare,lookingforashare";
    private static final String INAGREEMENT_YES = "Y";
    private static final String INAGREEMENT_NO = "N";

    private final LoginRestService loginRestService;

    RegisterTransactionDao(SpareroomContext spareroomContext) {
        super(spareroomContext);
        loginRestService = new LoginRestService();
    }

    /**
     * @return <code>Session</code> session if the log in was successful,
     * <code>SpareroomStatus</code> if there was a problem
     */
    @Override
    public Object create(AccountRegisterForm businessObject, RegisterExtra extra) throws
            UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {

        Parameters parameters = new Parameters();
        Authenticator authenticator = new Authenticator();

        String response;
        try {
            if (businessObject.getEmail() != null)
                parameters.add(EMAIL, businessObject.getEmail());
            if (businessObject.getFirstName() != null)
                parameters.add(FIRST_NAME, businessObject.getFirstName());
            if (businessObject.getLastName() != null)
                parameters.add(LAST_NAME, businessObject.getLastName());
            if (businessObject.getPassword() != null) {
                parameters.add(PASSWORD, businessObject.getPassword());
                parameters.add(PASSWORD2, businessObject.getPassword());
            }

            switch (businessObject.getAdvertiserType()) {
                case OFFERER:
                    parameters.add(USERTYPE, USER_TYPE_OFFERER);
                    break;
                case SEEKER:
                    parameters.add(USERTYPE, USER_TYPE_SEEKER);
                    break;
                case OFFERER_AND_SEEKER:
                    parameters.add(USERTYPE, USER_TYPE_OFFERER_AND_SEEKER);
                    break;
                default:
                    throw new InvalidArgumentException("User type needs to be specified");
            }

            parameters.add(INAGREEMENT,
                    businessObject.isInAgreement() ? INAGREEMENT_YES : INAGREEMENT_NO);

            parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON);

            String pageJoinedFrom = extra.getPageJoinedFrom();
            if (pageJoinedFrom != null)
                parameters.add(PAGE_JOINED_FROM, pageJoinedFrom);

            // if my_version is missing, the server simply defaults it to 1.0 This is not a critical
            // error
            parameters.add(MYVERSION, extra.getVersion());
            parameters.add(ApiParams.API_SIGNATURE, authenticator.authenticate(parameters));
            parameters.add(ApiParams.API_KEY, authenticator.get_apiKey());

            String[] vParametersName = parameters.getParameterNames();
            String[] vName = new String[vParametersName.length];
            String[] vValue = new String[vParametersName.length];
            for (int i = 0; i < vParametersName.length; i++) {
                vName[i] = vParametersName[i];
                vValue[i] = parameters.get(vParametersName[i]);
            }

            response = loginRestService.registerAccount(vName, vValue);

            return (new RegisterJsonParser()).parse(response);

        } catch (NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e);
        } catch (ClientErrorException | AuthenticationException e) {
            throw new InvalidArgumentException(e);
        } catch (ServerErrorException | InvalidJSONFormatException e) {
            throw new UnexpectedResultException(e);
        }
    }

}
