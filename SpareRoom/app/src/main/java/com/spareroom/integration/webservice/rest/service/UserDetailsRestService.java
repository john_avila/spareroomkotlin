package com.spareroom.integration.webservice.rest.service;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.Parameters;

public class UserDetailsRestService extends RestService {

    private static final String MANAGE_USER_DETAILS = "/flatshare/mydetails.pl";

    public String getUserDetails(Parameters parameters) throws ClientErrorException, ServerErrorException, NetworkConnectivityException, AuthenticationException {
        return get(MANAGE_USER_DETAILS, parameters);
    }

    public String updateUserDetails(String[] names, String[] values) throws NetworkConnectivityException, ClientErrorException, ServerErrorException {
        return post(MANAGE_USER_DETAILS, names, values);
    }
}
