package com.spareroom.integration.webservice.exception

class NotLoggedInException(message: String = "") : Exception(message)