package com.spareroom.integration.webservice.rest.service;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.model.business.Parameters;

import javax.inject.Inject;

public class UserAdsRestService extends RestService {

    private static final String MANAGE_OFFERED_AD = "/flatshare/offered-advert3.pl";
    private static final String MANAGE_WANTED_AD = "/flatshare/wanted_listing_step2.pl";
    private static final String MANAGE_USER_ADS = "/flatshare/mylistings.pl";
    private static final String MANAGE_VIEWING_ADS = "/flatshare/myviewings.pl";

    @Inject
    public UserAdsRestService() {

    }

    public String updateActiveStatus(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException {
        return post(MANAGE_USER_ADS, parameters.getParameterNames(), parameters.getParameterValues());
    }

    public String getAds(Parameters parameters) throws ClientErrorException, ServerErrorException, NetworkConnectivityException, AuthenticationException {
        return get(MANAGE_USER_ADS, parameters);
    }

    public String getOfferedAd(Parameters parameters) throws ClientErrorException, ServerErrorException, NetworkConnectivityException, AuthenticationException {
        return get(MANAGE_VIEWING_ADS, parameters);
    }

    public String getWantedAd(Parameters parameters) throws ClientErrorException, ServerErrorException, NetworkConnectivityException, AuthenticationException {
        return get(MANAGE_VIEWING_ADS, parameters);
    }

    public String updateWantedAd(String[] names, String[] values) throws NetworkConnectivityException, ClientErrorException, ServerErrorException {
        return post(MANAGE_WANTED_AD, names, values);
    }

    public String updateOfferedAd(String[] names, String[] values) throws NetworkConnectivityException, ClientErrorException, ServerErrorException {
        return post(MANAGE_OFFERED_AD, names, values);
    }

}
