package com.spareroom.integration.business.apiv2

import com.spareroom.model.business.*

interface IAdvertsDao<T : AbstractAd> {

    @Throws(Exception::class)
    fun getUserAdverts(userId: String, page: Int, maxPerPage: Int): AdvertsListRestResponse<T>

    @Throws(Exception::class)
    fun getAdvert(advertId: String): SingleAdvertRestResponse<T>

    @Throws(Exception::class)
    fun getSavedAdverts(offset: Int, maxPerPage: Int): AdvertsListRestResponse<T>
}