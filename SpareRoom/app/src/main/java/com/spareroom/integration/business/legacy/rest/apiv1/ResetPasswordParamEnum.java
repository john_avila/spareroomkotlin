package com.spareroom.integration.business.legacy.rest.apiv1;

/**
 * Enum of parameters used for Parameter object.
 * <p>
 * Created by miguel.rossi on 09/08/2016.
 */
public class ResetPasswordParamEnum {
    public static final String EMAIL = "email";
}
