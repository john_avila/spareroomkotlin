package com.spareroom.integration.webservice.exception

class NotEnoughListingsException(message: String = "") : Exception(message)