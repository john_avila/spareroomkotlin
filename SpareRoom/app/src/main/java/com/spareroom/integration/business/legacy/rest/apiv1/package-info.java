/**
 * APIv1 Factory and data accessors. APIv1 communicates with the server's API in a way where most of
 * the endpoints match a data accessor. In most occasions, the design of classes is done from API
 * endpoints to business classes rather than from business classes to API endpoints.
 */
package com.spareroom.integration.business.legacy.rest.apiv1;