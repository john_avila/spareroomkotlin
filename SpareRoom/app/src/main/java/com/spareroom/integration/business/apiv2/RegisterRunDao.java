package com.spareroom.integration.business.apiv2;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.SpareroomAbstractDaoImpl;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.*;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.service.LoginRestService;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.RegisterParameters;

/**
 * DAO for the RegisterTransactionDAO Business Object
 */

class RegisterRunDao extends SpareroomAbstractDaoImpl<RegisterParameters, Void, Void> {

    private final LoginRestService loginRestService;

    RegisterRunDao(SpareroomContext spareroomContext) {
        super(spareroomContext);
        loginRestService = new LoginRestService();
    }

    @Override
    public Object create(RegisterParameters businessObject, Void extra)
            throws UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {

        Parameters parameters = new Parameters();

        parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON);
        parameters.add("first_open", businessObject.getFirstOpen());
        parameters.add("my_version", businessObject.getMyVersion());

        try {

            loginRestService.registerApp(parameters);

        } catch (NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e);
        } catch (ClientErrorException | AuthenticationException e) {
            throw new InvalidArgumentException(e);
        } catch (ServerErrorException e) {
            throw new UnexpectedResultException(e);
        }

        return null;
    }

}
