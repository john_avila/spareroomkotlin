package com.spareroom.integration.business.apiv2.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.SpareroomStatus;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Parser for the FirebaseIdTransaction API response.
 * <p/>
 * Created by miguel.rossi on 06/01/2017.
 */

public class FirebaseIdTransactionJsonParser {

    public static SpareroomStatus parse(String json) throws InvalidJSONFormatException {
        SpareroomStatus spareroomStatus = new SpareroomStatus();

        try {
            JSONObject jsonFirstNode = new JSONObject(json);

            if (jsonFirstNode.has("success"))
                return null;

            if (jsonFirstNode.has("response_id"))
                spareroomStatus.setCode(jsonFirstNode.getString("response_id"));

            if (jsonFirstNode.has("response"))
                spareroomStatus.setMessage(jsonFirstNode.getString("response"));

            return spareroomStatus;

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }
    }

}
