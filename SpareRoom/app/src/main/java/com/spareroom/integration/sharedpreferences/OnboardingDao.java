package com.spareroom.integration.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class OnboardingDao {
    private static final String PREFS_NAME = "onboarding";
    private static final String FIRST_TIME = "firstTime";

    private OnboardingDao() {
    }

    public static void create(Context context, boolean showItAgain) {
        write(context, showItAgain);
    }

    /**
     * @param context ...you know
     * @return - <code>true</code> for showing it again; <code>false</code> otherwise
     */
    public static boolean read(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        return prefs.getBoolean(FIRST_TIME, true);
    }

    public static void update(Context context, boolean showItAgain) {
        write(context, showItAgain);
    }

    public static void delete(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.clear();
        editor.apply();

    }

    private static void write(Context context, boolean showItAgain) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putBoolean(FIRST_TIME, showItAgain);
        editor.apply();

    }

}
