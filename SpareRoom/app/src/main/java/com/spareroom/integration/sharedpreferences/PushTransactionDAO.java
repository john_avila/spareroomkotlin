package com.spareroom.integration.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

import static com.spareroom.integration.sharedpreferences.PushTransactionDAO.Status.SUCCESSFUL;
import static com.spareroom.integration.sharedpreferences.PushTransactionDAO.Status.UNSUCCESSFUL;

public class PushTransactionDAO {
    private static final String KEY_PREFS = "pushStatus";

    public enum Status {SUCCESSFUL, UNSUCCESSFUL}

    public enum Process {
        REGISTER {
            @Override
            public String toString() {
                return "status_register";
            }
        },
        UNREGISTER {
            @Override
            public String toString() {
                return "status_unregister";
            }
        }

    }

    /**
     * Tells us if the register/unregister process was ok.
     *
     * @param context the context
     * @param process {@link Process}
     * @return if the process was ok or not; KO by default
     */
    public static Status read(Context context, Process process) {
        SharedPreferences prefs = context.getSharedPreferences(KEY_PREFS, Context.MODE_PRIVATE);
        int statusOrdinal = prefs.getInt(process.toString(), Status.UNSUCCESSFUL.ordinal());
        return (statusOrdinal == SUCCESSFUL.ordinal()) ? SUCCESSFUL : UNSUCCESSFUL;
    }

    public static boolean update(Context context, Process process, Status status) {
        SharedPreferences prefs = context.getSharedPreferences(KEY_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putInt(process.toString(), status.ordinal());
        return editor.commit();
    }

}
