package com.spareroom.integration.dependency.module;

import com.spareroom.ui.filters.screen.*;
import com.spareroom.ui.screen.*;
import com.spareroom.ui.screen.legacy.*;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@SuppressWarnings("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract ConversationFragment contributeConversationFragment();

    @ContributesAndroidInjector
    abstract OtherUserAdvertsFragment contributeOtherUserAdvertsFragment();

    @ContributesAndroidInjector
    abstract FilterMapsFragment contributeFilterMapsFragment();

    @ContributesAndroidInjector
    abstract FilterDetailFragment contributeFilterSpecificFragment();

    @ContributesAndroidInjector
    abstract MinFlatmatesDialog contributeMinFlatmatesDialog();

    @ContributesAndroidInjector
    abstract MinTermDialog contributeMinTermDialog();

    @ContributesAndroidInjector
    abstract MaxFlatmatesDialog contributeMaxFlatmatesDialog();

    @ContributesAndroidInjector
    abstract MaxTermDialog contributeMaxTermDialog();

    @ContributesAndroidInjector
    abstract MyAdvertsListFragment contributeMyAdvertsListFragment();

    @ContributesAndroidInjector
    abstract MyAdvertsListHostFragment contributeMyAdvertsListHostFragment();

    @ContributesAndroidInjector
    abstract DeleteConversationDialog contributeDeleteConversationDialog();

    @ContributesAndroidInjector
    abstract DeclineConversationDialog contributeDeclineConversationDialog();

    @ContributesAndroidInjector
    abstract SendMessageConfirmationDialog contributeSendMessageConfirmationDialog();

    @ContributesAndroidInjector
    abstract FilterSummaryFragment contributeFilterSummaryFragment();

    @ContributesAndroidInjector
    abstract DateDialog contributeDateDialog();

    @ContributesAndroidInjector
    abstract DrawerLoggedInFragment contributeDrawerLoggedInFragment();

    @ContributesAndroidInjector
    abstract DrawerLoggedOutFragment contributeDrawerLoggedOutFragment();

    @ContributesAndroidInjector
    abstract HomeFragment contributeHomeFragment();

    @ContributesAndroidInjector
    abstract SearchMapAdOfferedFragment contributeSearchMapAdOfferedFragment();

    @ContributesAndroidInjector
    abstract PlaceAdPhotoChosenFragment contributePlaceAdPhotoChosenFragment();

    @ContributesAndroidInjector
    abstract ConversationListFragment contributeConversationListFragment();

    @ContributesAndroidInjector
    abstract RatingInitialDialog contributeRatingInitialDialog();

    @ContributesAndroidInjector
    abstract RatingReminderDialog contributeRatingReminderDialog();

    @ContributesAndroidInjector
    abstract DrawerFragment contributeDrawerFragment();

    @ContributesAndroidInjector
    abstract PlaceAdOfferedAreaFragment contributePlaceAdOfferedAreaFragment();

    @ContributesAndroidInjector
    abstract PlaceAdOfferedPostcodeAreaFragment contributePlaceAdOfferedPostcodeAreaFragment();

    @ContributesAndroidInjector
    abstract PlaceAdTitleDescriptionFragment contributePlaceAdTitleDescriptionFragment();

    @ContributesAndroidInjector
    abstract PlaceAdWantedAreasLookingInFragment contributePlaceAdWantedAreasLookingInFragment();

    @ContributesAndroidInjector
    abstract NewSavedSearchFragment contributeNewSavedSearchFragment();

    @ContributesAndroidInjector
    abstract SearchFragment contributeSearchFragment();

}
