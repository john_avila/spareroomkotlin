package com.spareroom.integration.business;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.exception.InvalidArgumentException;
import com.spareroom.integration.exception.UnavailableDataSourceException;
import com.spareroom.integration.exception.UnexpectedResultException;

public abstract class SpareroomAbstractDaoImpl<BO, E, BOID> extends SpareroomAbstractDao<BO, E, BOID> {

    public SpareroomAbstractDaoImpl(SpareroomContext spareroomContext) {
        super(spareroomContext);
    }

    public Object create(BO businessObject, E extra) throws UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {
        return null;
    }

    public Object read(BOID id, E extra) throws UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {
        return null;
    }

    public Object update(BO businessObject, E extra) throws UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {
        return null;
    }

    public Object delete(BO businessObject, E extra) throws UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {
        return null;
    }

}
