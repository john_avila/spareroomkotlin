package com.spareroom.integration.business.apiv2.util;

import com.spareroom.integration.business.legacy.rest.apiv1.util.SessionJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.RegisterWithFacebookResult;
import com.spareroom.model.business.SpareroomStatus;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterWithFacebookResultJsonParser {

    /**
     * Parses the response after registering a new user
     *
     * @param json String containing the response
     * @return an User object if the the user could be registered, SpareroomStatus if there were
     * problems in the request
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public Object parse(String json) throws InvalidJSONFormatException {

        if (json == null)
            throw new InvalidJSONFormatException("null object");

        try {
            JSONObject firstNode = new JSONObject(json);
            int success = firstNode.getInt("success");

            if (success == 1) {
                RegisterWithFacebookResult result = new RegisterWithFacebookResult();

                if (firstNode.has("require_custom_password")) {
                    result.set_requireCustomPassword(firstNode.getInt("require_custom_password") == 1);

                } else {
                    result.set_requireCustomPassword(false);

                    // This if shouldn't be needed, but the API is returning either
                    // require_custom_password = 1 or omitting the field and returning the
                    // session instead.
                    if (firstNode.has("session")) {
                        SessionJSONParser sessionParser = new SessionJSONParser();
                        result.set_session(sessionParser.parse(firstNode.getString("session")));

                    }

                }

                return result;

            } else {

                if (firstNode.has("response_id") && firstNode.has("response")) {
                    SpareroomStatus errorMessage = new SpareroomStatus();

                    errorMessage.setCode(firstNode.getString("response_id"));
                    errorMessage.setMessage(firstNode.getString("response"));

                    return errorMessage;

                }

                SpareroomStatus e = new SpareroomStatus();

                e.setCode("9999");
                e.setMessage("Could not connect with Facebook");

                return e;

            }

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();

        }

    }

}
