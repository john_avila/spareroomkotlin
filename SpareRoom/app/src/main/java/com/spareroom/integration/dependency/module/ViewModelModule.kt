package com.spareroom.integration.dependency.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.spareroom.integration.dependency.ViewModelKey
import com.spareroom.viewmodel.*
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ConversationViewModel::class)
    abstract fun bindConversationViewModel(conversationViewModel: ConversationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ResultsViewModel::class)
    abstract fun bindResultsViewModel(resultsViewModel: ResultsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FilterViewModel::class)
    abstract fun bindFilterViewModel(filterViewModel: FilterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MyAdvertsListViewModel::class)
    abstract fun bindMyAdvertsFragmentViewModel(myAdvertsListViewModel: MyAdvertsListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MyAdvertViewModel::class)
    abstract fun bindMyAdvertViewModel(myAdvertViewModel: MyAdvertViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ConversationListViewModel::class)
    abstract fun bindConversationListViewModel(conversationListViewModel: ConversationListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    abstract fun bindSearchViewModel(searchViewModel: SearchViewModel): ViewModel
}
