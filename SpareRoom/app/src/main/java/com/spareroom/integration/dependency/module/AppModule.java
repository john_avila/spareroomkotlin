package com.spareroom.integration.dependency.module;

import android.app.Application;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.integration.webservice.rest.client.methods.HttpRequest;
import com.spareroom.model.business.Session;

import java.util.Locale;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

@Module
public class AppModule {

    // this needs to be such high number to get proper network traffic encoding when in debug mode (server checks numbers in user-agent header)
    private static final String DEBUG_USER_AGENT_VERSION_NAME = "1000.0.0.0";

    private final Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Singleton
    @Provides
    Application providesApplication() {
        return application;
    }

    @Singleton
    @Provides
    SpareroomApplication providesSpareroomFactory() {
        return SpareroomApplication.getInstance();
    }

    @Singleton
    @Provides
    SpareroomContext providesSpareroomContext(SpareroomApplication spareroomFactory) {
        return spareroomFactory.getSpareroomContext();
    }

    @Provides
    Session providesSession(SpareroomContext spareroomContext) {
        return spareroomContext.getSession();
    }

    @Provides
    Locale providesLocale() {
        return AppVersion.flavor().getLocale();
    }

    @Provides
    Animation providesImageLoadingAnimation(Application application) {
        return AnimationUtils.loadAnimation(application, R.anim.fade_in_out);
    }

    @Singleton
    @Provides
    OkHttpClient providesHttpClient() {
        return new OkHttpClient();
    }

    @Singleton
    @Provides
    @Named(HttpRequest.USER_AGENT_NAMED_PROVIDER)
    String providesUserAgent() {
        String userAgent = AppVersion.flavor().getUserAgentAppId();
        userAgent += ("/" + (AppVersion.isDevRelease() ? DEBUG_USER_AGENT_VERSION_NAME : AppVersion.versionName()));
        userAgent += " (Android ";
        userAgent += android.os.Build.VERSION.RELEASE;
        userAgent += ";";
        userAgent += android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL;
        userAgent += ")";

        return userAgent;
    }

}
