package com.spareroom.integration.exception;

public class UnexpectedResultException extends Exception {

    private static final long serialVersionUID = -7680127637031287215L;

    public UnexpectedResultException() {
        super();
    }

    public UnexpectedResultException(String detailMessage) {
        super(detailMessage);
    }

    public UnexpectedResultException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public UnexpectedResultException(Throwable throwable) {
        super(throwable);
    }

}

