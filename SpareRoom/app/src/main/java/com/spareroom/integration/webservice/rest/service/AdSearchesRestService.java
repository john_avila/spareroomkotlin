package com.spareroom.integration.webservice.rest.service;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.Parameters;

public class AdSearchesRestService extends RestService {

    private static final String SEARCH_AD = "/flatshare/shortlist.pl";
    private static final String SAVED_SEARCH = "/flatshare/savesearch.pl";

    public String saveAd(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(SEARCH_AD, parameters);
    }

    public String removeSavedAd(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(SEARCH_AD, parameters);
    }

    public String getSavedSearches(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(SAVED_SEARCH, parameters);
    }

    public String updateSavedSearch(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(SAVED_SEARCH, parameters);
    }
}
