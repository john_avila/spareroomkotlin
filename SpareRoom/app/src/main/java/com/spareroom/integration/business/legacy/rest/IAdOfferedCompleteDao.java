package com.spareroom.integration.business.legacy.rest;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.AdOffered;
import com.spareroom.model.business.DraftAd;

public interface IAdOfferedCompleteDao {

    void create(DraftAd draft);

    AdOffered read(String advertId) throws AuthenticationException, NetworkConnectivityException, InvalidJSONFormatException, ClientErrorException, ServerErrorException;
}
