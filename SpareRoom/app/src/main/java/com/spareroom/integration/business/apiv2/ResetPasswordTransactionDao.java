package com.spareroom.integration.business.apiv2;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.SpareroomAbstractDaoImpl;
import com.spareroom.integration.business.legacy.rest.apiv1.ResetPasswordParamEnum;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.ForgotPasswordJSONParser;
import com.spareroom.integration.exception.*;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.service.LoginRestService;
import com.spareroom.model.business.*;
import com.spareroom.model.extra.ResetPasswordExtra;

/**
 * DAO for the ResetPasswordTransaction Business Object (does not exists on the app)
 */
class ResetPasswordTransactionDao extends SpareroomAbstractDaoImpl<AccountCredentials, ResetPasswordExtra, AccountConnectionInterface> {

    private final LoginRestService loginRestService;

    ResetPasswordTransactionDao(SpareroomContext spareroomContext) {
        super(spareroomContext);
        loginRestService = new LoginRestService();
    }

    /**
     * @return <code>Session</code> session if the log in was successful,
     * <code>SpareroomStatus</code> if there was a problem
     */
    @Override
    public Object create(AccountCredentials businessObject, ResetPasswordExtra extra) throws
            UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {

        Parameters parameters = new Parameters();
        parameters.add(ResetPasswordParamEnum.EMAIL, businessObject.getUsername());
        parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON);

        try {
            String response = loginRestService.resetPassword(parameters);

            return (new ForgotPasswordJSONParser()).parse(response);
        } catch (NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e);
        } catch (ClientErrorException | AuthenticationException e) {
            throw new InvalidArgumentException(e);
        } catch (ServerErrorException | InvalidJSONFormatException e) {
            throw new UnexpectedResultException(e);
        }

    }

}
