package com.spareroom.integration.business.apiv2;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.DeleteThreadJSONParser;
import com.spareroom.integration.exception.*;
import com.spareroom.integration.json.JsonParser;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.service.ConversationRestService;
import com.spareroom.model.business.*;

import javax.inject.Inject;

import static com.spareroom.integration.business.legacy.rest.apiv1.MessageParamEnum.DELETE_THREAD;
import static com.spareroom.integration.business.legacy.rest.apiv1.MessageParamEnum.RE_LISTING;

public class ConversationDao implements IConversationDao {
    private static final String OTHER_USER_ID = "conversation_with";
    private static final String ADVERT_ID = "re_listing";

    private final ConversationRestService conversationRestService;
    private final JsonParser jsonParser;

    @Inject
    ConversationDao(ConversationRestService conversationRestService, JsonParser jsonParser) {
        this.conversationRestService = conversationRestService;
        this.jsonParser = jsonParser;
    }

    @Override
    public Conversation getConversation(GetConversationRequest getConversationRequest)
            throws UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {

        Parameters parameters = new Parameters();
        parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON);
        parameters.add(OTHER_USER_ID, getConversationRequest.getOtherUserId());
        parameters.add(ADVERT_ID, getConversationRequest.getAdvertId());

        try {
            String response = conversationRestService.getConversation(parameters);

            return jsonParser.fromJson(response, Conversation.class);
        } catch (NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e);
        } catch (ClientErrorException | AuthenticationException e) {
            throw new InvalidArgumentException(e);
        } catch (ServerErrorException e) {
            throw new UnexpectedResultException(e);
        }

    }

    @Override
    public void deleteConversation(DeleteConversationRequest deleteConversationRequest)
            throws UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {

        Parameters parameters = new Parameters();
        DeleteThreadJSONParser parser = new DeleteThreadJSONParser();

        parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON);
        parameters.add(RE_LISTING, deleteConversationRequest.getAdvertId());
        parameters.add(DELETE_THREAD, deleteConversationRequest.getOtherUserId());

        try {
            String response = conversationRestService.deleteConversation(parameters);
            parser.parse(response);
        } catch (NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e);
        } catch (ClientErrorException | AuthenticationException e) {
            throw new InvalidArgumentException(e);
        } catch (ServerErrorException | InvalidJSONFormatException e) {
            throw new UnexpectedResultException(e);
        }
    }

}
