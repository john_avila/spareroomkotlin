package com.spareroom.integration.business.apiv2.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.NewMessageResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Parses the response when sending a new message
 * <p/>
 * Created by miguel.rossi on 21/02/2017.
 */
public class NewMessageJsonParser {

    public NewMessageResponse parse(String json) throws InvalidJSONFormatException {
        NewMessageResponse newMessageResponse = new NewMessageResponse();
        JSONObject firstNode;
        String responseType;

        try {
            firstNode = new JSONObject(json);

            if (firstNode.has("response_type")) {
                responseType = firstNode.getString("response_type");

                switch (responseType) {
                    case "success":
                        newMessageResponse.setStatusCode(firstNode.getString("success"));
                        newMessageResponse.setResponseType(firstNode.getString("response_type"));
                        newMessageResponse.setResponse(firstNode.getString("response"));
                        newMessageResponse.setMessageSent(firstNode.getString("message_sent"));
                        break;

                    case "action":
                    case "error":
                        newMessageResponse.setStatusCode(firstNode.getString("response_id"));
                        newMessageResponse.setResponse(firstNode.getString("response"));
                        break;

                    default:
                        throw new InvalidJSONFormatException();

                }

            } else { // AutoReply successful response
                newMessageResponse.setMessageSent(firstNode.getString("message"));

            }

            return newMessageResponse;

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

    }

}
