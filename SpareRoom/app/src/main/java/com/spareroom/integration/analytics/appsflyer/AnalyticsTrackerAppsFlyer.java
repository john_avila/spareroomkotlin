package com.spareroom.integration.analytics.appsflyer;

import android.app.Application;

import com.appsflyer.*;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.spareroom.R;
import com.spareroom.integration.analytics.AnalyticsTracker;
import com.spareroom.integration.analytics.firebase.AnalyticsTagsFirebase;
import com.spareroom.integration.analytics.googleanalytics.AnalyticsTagsGoogleAnalytics;
import com.spareroom.model.business.AccountRegisterForm;
import com.spareroom.model.business.Upgrade;
import com.spareroom.ui.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class AnalyticsTrackerAppsFlyer extends AnalyticsTracker {

    private final Application app;

    public AnalyticsTrackerAppsFlyer(Application app) {
        this.app = app;
        AppsFlyerLib.getInstance().init(app.getString(R.string.apps_flyer_dev_key), null, app);
        AppsFlyerLib.getInstance().startTracking(app);
        AppsFlyerLib.getInstance().setCurrencyCode(app.getString(R.string.currency_code));
    }

    @Override
    public void setCustomerUserId(String customerId) {
        AppsFlyerLib.getInstance().setCustomerUserId(customerId);
    }

    @Override
    public void trackEvent_Upgrade_Purchase(Upgrade u, String orderId, int productPosition) {
        Map<String, Object> eventValue = new HashMap<>();
        eventValue.put(AFInAppEventParameterName.REVENUE, u.get_price());
        eventValue.put(AFInAppEventParameterName.CONTENT_TYPE, AnalyticsTagsGoogleAnalytics.Category.UPGRADE);
        eventValue.put(AFInAppEventParameterName.CONTENT_ID, u.get_code());
        eventValue.put(AFInAppEventParameterName.CURRENCY, u.get_currency());
        AppsFlyerLib.getInstance().trackEvent(app, AFInAppEventType.PURCHASE, eventValue);
    }

    @Override
    public void trackEvent_Account_Register_Facebook(String userType) {
        trackRegistration(userType, AnalyticsTagsFirebase.Param.VALUE_REGISTER_METHOD_FACEBOOK);
    }

    @Override
    public void trackEvent_Account_Register_Spareroom(String userType) {
        trackRegistration(userType, AnalyticsTagsFirebase.Param.VALUE_REGISTER_METHOD_EMAIL);
    }

    @Override
    public void trackEvent_Account_Login_Facebook() {
        trackLogIn(AnalyticsTagsFirebase.Param.VALUE_LOGIN_METHOD_FACEBOOK);
    }

    @Override
    public void trackEvent_Account_Login_Spareroom() {
        trackLogIn(AnalyticsTagsFirebase.Param.VALUE_LOGIN_METHOD_EMAIL);
    }

    private void trackRegistration(String method, String userType) {
        Map<String, Object> eventValues = new HashMap<>();
        eventValues.put(AnalyticsTagsFirebase.Param.KEY_REGISTER_USER_TYPE, getUserType(userType));
        eventValues.put(FirebaseAnalytics.Param.SIGN_UP_METHOD, method);
        AppsFlyerLib.getInstance().trackEvent(app, FirebaseAnalytics.Event.SIGN_UP, eventValues);
    }

    private void trackLogIn(String method) {
        Map<String, Object> eventValues = new HashMap<>();
        eventValues.put(AnalyticsTagsFirebase.Param.KEY_LOGIN_METHOD, method);
        AppsFlyerLib.getInstance().trackEvent(app, FirebaseAnalytics.Event.LOGIN, eventValues);
    }

    private String getUserType(String userType) {
        if (AccountRegisterForm.AdvertiserType.OFFERER_AND_SEEKER.toString().equals(userType))
            return AnalyticsTagsFirebase.Param.VALUE_REGISTER_USER_TYPE_SEEKER_OFFERER;

        if (AccountRegisterForm.AdvertiserType.OFFERER.toString().equals(userType))
            return AnalyticsTagsFirebase.Param.VALUE_REGISTER_USER_TYPE_OFFERER;

        if (AccountRegisterForm.AdvertiserType.SEEKER.toString().equals(userType))
            return AnalyticsTagsFirebase.Param.VALUE_REGISTER_USER_TYPE_SEEKER;

        return StringUtils.EMPTY_STRING;
    }
}
