package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.business.SpareroomStatusCode;

import org.json.JSONException;
import org.json.JSONObject;

public class DeleteThreadJSONParser {
    /**
     * Parses the response after deleting a conversation
     *
     * @param json String containing the response
     * @return SpareroomStatus indicating whether the response was successful or not
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */

    public SpareroomStatus parse(String json) throws InvalidJSONFormatException {
        try {
            JSONObject firstNode = new JSONObject(json);

            String success = firstNode.getString("success");

            if (success.equals(SpareroomStatusCode.CODE_SUCCESS)) {
                SpareroomStatus m = new SpareroomStatus();
                m.setCode(success);
                m.setMessage(firstNode.getString("response"));
                return m;
            }

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

        throw new InvalidJSONFormatException();
    }
}
