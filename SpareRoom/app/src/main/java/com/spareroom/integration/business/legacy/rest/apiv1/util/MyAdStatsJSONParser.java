package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.DayStats;
import com.spareroom.model.business.Stats;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;

public class MyAdStatsJSONParser {

    public Stats parse(String json) throws InvalidJSONFormatException {
        try {
            JSONObject result = new JSONObject(json);
            if (result.has("success") && result.getString("success").equals("1")) {
                if (result.has("stats")) {
                    Stats stats = new Stats();

                    JSONObject jsonStats = result.getJSONObject("stats");
                    Iterator it = jsonStats.keys();
                    while (it.hasNext()) {
                        DayStats dayStats = new DayStats();

                        String key = (String) it.next();
                        JSONObject jsonDayStats = jsonStats.getJSONObject(key);
                        dayStats.set_emailCount(Integer.parseInt(jsonDayStats.getString("emailcount")));
                        dayStats.set_interestCount(Integer.parseInt(jsonDayStats.getString("interestcount")));
                        dayStats.set_telCount(Integer.parseInt(jsonDayStats.getString("telcount")));
                        dayStats.set_viewCount(Integer.parseInt(jsonDayStats.getString("viewcount")));

                        String sDate = jsonDayStats.getString("summarydate");
                        GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.UK);
                        calendar.set(Integer.parseInt(sDate.substring(0, 4)),
                                Integer.parseInt(sDate.substring(5, 7)) - 1,
                                Integer.parseInt(sDate.substring(8, 10)));

                        dayStats.set_date(calendar);

                        stats.add(dayStats);
                    }

                    return stats;
                }
            }
        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

        return null;
    }
}
