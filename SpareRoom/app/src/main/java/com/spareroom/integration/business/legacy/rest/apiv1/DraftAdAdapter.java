package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.controller.AppVersion;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.lib.util.Json2HttpUtil;
import com.spareroom.model.business.*;
import com.spareroom.model.business.DraftAdOffered.AdvertiserType;
import com.spareroom.ui.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class DraftAdAdapter {

    private String adaptPropertyType(DraftAdOffered.PropertyType type) {
        switch (type) {
            case FLAT:
                return DraftAdHttpMap.VALUE_OFFERED_PROPERTY_FLAT;
            case HOUSE:
                return DraftAdHttpMap.VALUE_OFFERED_PROPERTY_HOUSE;
            case OTHER:
                return DraftAdHttpMap.VALUE_OFFERED_PROPERTY_OTHER;
            default:
                return null;
        }
    }

    private String adaptAdvertiserType(DraftAdOffered.AdvertiserType type) {
        switch (type) {
            case LIVE_IN_LANDLORD:
                return DraftAdHttpMap.VALUE_OFFERED_LIL;
            case LIVE_OUT_LANDLORD:
                return DraftAdHttpMap.VALUE_OFFERED_LOL;
            case CURRENT_FLATMATE:
                return DraftAdHttpMap.VALUE_OFFERED_ADVERTISER_CURRENT_FLATMATE;
            case AGENT:
                return DraftAdHttpMap.VALUE_OFFERED_ADVERTISER_AGENT;
            case FORMER_FLATMATE:
                return DraftAdHttpMap.VALUE_OFFERED_ADVERTISER_FORMER_FLATMATE;
            default:
                return null;
        }
    }

    private String adaptRoomSize(int roomSize) {
        switch (roomSize) {
            case Room.ROOM_SIZE_SINGLE:
                return DraftAdHttpMap.VALUE_OFFERED_ROOM_SIZE_SINGLE;
            case Room.ROOM_SIZE_DOUBLE:
                return DraftAdHttpMap.VALUE_OFFERED_ROOM_SIZE_DOUBLE;
            default:
                return null;
        }
    }

    private String adaptRoomStatus(int roomStatus) {
        switch (roomStatus) {
            case Room.ROOM_STATUS_AVAILABLE:
                return DraftAdHttpMap.VALUE_OFFERED_STATUS_AVAILABLE;
            case Room.ROOM_STATUS_OCCUPIED:
                return DraftAdHttpMap.VALUE_OFFERED_STATUS_OCCUPIED;
            case Room.ROOM_STATUS_TAKEN:
                return DraftAdHttpMap.VALUE_OFFERED_STATUS_TAKEN;
            default:
                return null;
        }
    }

    private String adaptBillsIncluded(DraftAdOffered.BillsIncluded billsIncluded) {
        switch (billsIncluded) {
            case NO:
                return DraftAdHttpMap.VALUE_NO_FULL_TEXT;
            case YES:
                return DraftAdHttpMap.VALUE_YES_FULL_TEXT;
            case SOME:
                return DraftAdHttpMap.VALUE_SOME_FULL_TEXT;
            default:
                return null;
        }
    }

    private String adaptGender(int numMale, int numFemale) {
        StringBuilder maleFemaleStr = new StringBuilder();

        if ((numMale + numFemale) > 5 || (numMale == 1 && numFemale == 1) || (numMale < numFemale)) {
            for (int i = 0; i < numFemale; i++)
                maleFemaleStr.append(DraftAdHttpMap.VALUE_FEMALE);
            for (int i = 0; i < numMale; i++)
                maleFemaleStr.append(DraftAdHttpMap.VALUE_MALE);

        } else {
            for (int i = 0; i < numMale; i++)
                maleFemaleStr.append(DraftAdHttpMap.VALUE_MALE);
            for (int i = 0; i < numFemale; i++)
                maleFemaleStr.append(DraftAdHttpMap.VALUE_FEMALE);
        }

        return maleFemaleStr.toString();
    }

    private String adaptAdvertiserTypeWanted(DraftAdWanted.AdvertiserType at) {
        switch (at) {
            case MALE:
                return DraftAdHttpMap.VALUE_WANTED_WE_ARE_ONE_MALE;
            case FEMALE:
                return DraftAdHttpMap.VALUE_WANTED_WE_ARE_ONE_FEMALE;
            case TWO_MALES:
                return DraftAdHttpMap.VALUE_WANTED_WE_ARE_TWO_MALES;
            case TWO_FEMALES:
                return DraftAdHttpMap.VALUE_WANTED_WE_ARE_TWO_FEMALES;
            case MALE_FEMALE:
                return DraftAdHttpMap.VALUE_WANTED_WE_ARE_ONE_MALE_ONE_FEMALE;
        }
        return null;
    }

    private String adaptOccupationWanted(DraftAdWanted.Occupation o) {
        switch (o) {
            case STUDENT:
                return DraftAdHttpMap.VALUE_STUDENT;
            case PROFESSIONAL:
                return DraftAdHttpMap.VALUE_PROFESSIONAL;
            case OTHER:
                return DraftAdHttpMap.VALUE_OCCUPATION_OTHER;
            case MIXED:
                return DraftAdHttpMap.VALUE_OCCUPATION_MIXED;
        }
        return null;
    }

    private String adaptRoom(DraftAdWanted.RoomType r) {
        switch (r) {
            case SINGLE_OR_DOUBLE:
                return DraftAdHttpMap.VALUE_WANTED_ROOM_TYPE_SINGLE_OR_DOUBLE;
            case DOUBLE:
                return DraftAdHttpMap.VALUE_WANTED_ROOM_TYPE_DOUBLE;
            case DOUBLE_COUPLE:
                return DraftAdHttpMap.VALUE_WANTED_ROOM_TYPE_DOUBLE_COUPLE;
            case TWO_ROOMS:
                return DraftAdHttpMap.VALUE_WANTED_ROOM_TYPE_2_ROOMS;
            case TWIN_OR_TWO_ROOMS:
                return DraftAdHttpMap.VALUE_WANTED_ROOM_TYPE_TWIN_OR_2_ROOMS;
        }
        return null;
    }

    private String adaptDaysOfWeek(DraftAdWanted.DaysOfWeek s) {
        switch (s) {
            case MONDAY_TO_FRIDAY:
                return DraftAdHttpMap.VALUE_WANTED_DAYS_OF_WEEK_MON_FRI;
            case SEVEN_A_DAY:
                return DraftAdHttpMap.VALUE_WANTED_DAYS_OF_WEEK_7_DAYS;
            case WEEKENDS_ONLY:
                return DraftAdHttpMap.VALUE_WANTED_DAYS_OF_WEEK_WEEKENDS;
        }
        return null;
    }

    public DraftAdHttpMap adapt(DraftAd draft) throws InvalidJSONFormatException {
        if (draft instanceof DraftAdOffered) {
            return adaptOffered((DraftAdOffered) draft);
        } else if (draft instanceof DraftAdWanted) {
            return adaptWanted((DraftAdWanted) draft);
        }
        return null;
    }

    private DraftAdHttpMap adaptOffered(DraftAdOffered draftOffered) throws InvalidJSONFormatException {
        DraftAdHttpMap httpMap = new DraftAdHttpMap();

        // page 1
        if (draftOffered.get_firstName() != null)
            httpMap.put(DraftAdHttpMap.KEY_FIRST_NAME, draftOffered.get_firstName());
        if (draftOffered.get_lastName() != null)
            httpMap.put(DraftAdHttpMap.KEY_LAST_NAME, draftOffered.get_lastName());
        if (draftOffered.get_email() != null)
            httpMap.put(DraftAdHttpMap.KEY_EMAIL, draftOffered.get_email());

        // page 2
        if (draftOffered.get_propertyType() != null)
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_PROPERTY_TYPE, adaptPropertyType(draftOffered.get_propertyType()));

        // page 3
        if (draftOffered.get_advertiserType() != null)
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_ADVERTISER_TYPE, adaptAdvertiserType(draftOffered.get_advertiserType()));

        if (draftOffered.get_companyName() != null)
            if (draftOffered.get_advertiserType() == AdvertiserType.AGENT) {
                httpMap.put(DraftAdHttpMap.KEY_OFFERED_COMPANY_NAME, draftOffered.get_companyName());
            }

        // page 4
        // _isAtProperty is for internal use
        if (draftOffered.get_postCode() != null)
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_POST_CODE, draftOffered.get_postCode());
        if (draftOffered.get_area() != null)
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_NEIGHBOURHOOD_ID, draftOffered.get_area());

        // page 5
        // _firstPictureUri not used on the http form
        // _firstPicturePath not used on the http form

        // page 6
        if (draftOffered.get_rooms() != null) {
            int sizeRoomList = draftOffered.get_rooms().size();
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_ROOMS_COUNT, Integer.toString(sizeRoomList));

            for (int i = 0; i < sizeRoomList; i++) {
                Room room = draftOffered.get_rooms().get(i);
                httpMap.put(DraftAdHttpMap.KEY_OFFERED_ROOM_PRICE + Integer.toString(i + 1), room.get_rent().get_amount());
                httpMap.put(DraftAdHttpMap.KEY_OFFERED_ROOM_PER + Integer.toString(i + 1), room.get_rent().get_periodicity());
                // the values for periodicity match valuePerMonth and valuePerWeek, so nothing else to do
                httpMap.put(DraftAdHttpMap.KEY_OFFERED_ROOM_SIZE + Integer.toString(i + 1),
                        adaptRoomSize(room.get_roomSize()));
                httpMap.put(DraftAdHttpMap.KEY_OFFERED_ROOM_STATUS + Integer.toString(i + 1), adaptRoomStatus(room.get_roomStatus()));

                if (!StringUtils.isNullOrEmpty(room.deposit()))
                    httpMap.put(DraftAdHttpMap.KEY_OFFERED_ROOM_DEPOSIT + Integer.toString(i + 1), room.deposit());
            }
        }

        if (!StringUtils.isNullOrEmpty(draftOffered.wholePropertyDeposit()))
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_DEPOSIT, draftOffered.wholePropertyDeposit());

        // page 7
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);
        if (draftOffered.get_availableFrom() != null)
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_DATE_AVAILABLE, sdf.format(draftOffered.get_availableFrom().getTime()));

        if (draftOffered.is_isMonFriLet())
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_MON_FRI, DraftAdHttpMap.VALUE_OFFERED_DAYS_OF_WEEK);

        if (draftOffered.get_areBillsIncluded() != null)
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_BILLS_INCLUDED, adaptBillsIncluded(draftOffered.get_areBillsIncluded()));

        httpMap.put(DraftAdHttpMap.KEY_OFFERED_EXISTING_GENDER, adaptGender(draftOffered.get_numExistingGuys(), draftOffered.get_numExistingGirls()));

        if (draftOffered.get_rangeExistingAges() != null) {
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_MINIMUM_AGE, Integer.toString(draftOffered.get_rangeExistingAges().getKey()));
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_MAXIMUM_AGE, Integer.toString(draftOffered.get_rangeExistingAges().getValue()));
        }

        httpMap.put(DraftAdHttpMap.KEY_OFFERED_ROOMS_IN_PROPERTY, Integer.toString(draftOffered.get_numBedroom()));

        if (draftOffered.is_isTherePet() != null)
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_PETS, draftOffered.is_isTherePet() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT);

        if (draftOffered.is_areThereSmokers() != null)
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_SMOKING, draftOffered.is_areThereSmokers() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT);

        PreferenceList list = draftOffered.get_preferenceList();

        List<PreferenceItem> lPreference = list.getList();

        boolean isGuyPreferred = false;
        boolean isGirlPreferred = false;
        boolean isStudentPreferred = false;
        boolean isProfessionalPreferred = false;
        for (PreferenceItem item : lPreference) {
            if (item.getId() == PlaceAdOfferedPreferenceEnum.GUYS.ordinal()) {
                if (item.isChecked())
                    isGuyPreferred = true;
            } else if (item.getId() == PlaceAdOfferedPreferenceEnum.GIRLS.ordinal()) {
                if (item.isChecked())
                    isGirlPreferred = true;
            } else if (item.getId() == PlaceAdOfferedPreferenceEnum.COUPLES.ordinal()) {
                httpMap.put(DraftAdHttpMap.KEY_OFFERED_COUPLES_ALLOWED,
                        (item.isChecked()) ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );
            } else if (item.getId() == PlaceAdOfferedPreferenceEnum.SMOKERS.ordinal()) {
                httpMap.put(DraftAdHttpMap.KEY_OFFERED_SMOKING_ALLOWED,
                        (item.isChecked()) ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );
            } else if (item.getId() == PlaceAdOfferedPreferenceEnum.STUDENTS.ordinal()) {
                if (item.isChecked())
                    isStudentPreferred = true;
            } else if (item.getId() == PlaceAdOfferedPreferenceEnum.PROFESSIONALS.ordinal()) {
                if (item.isChecked())
                    isProfessionalPreferred = true;
            } else if (item.getId() == PlaceAdOfferedPreferenceEnum.PETS.ordinal()) {
                httpMap.put(DraftAdHttpMap.KEY_OFFERED_PETS_ALLOWED,
                        (item.isChecked()) ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );
            } else if (item.getId() == PlaceAdOfferedPreferenceEnum.DSS.ordinal()
                    && (AppVersion.isUk())) {
                httpMap.put(DraftAdHttpMap.KEY_OFFERED_DSS_WELCOME,
                        (item.isChecked()) ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );
            }
        }

        if (isGuyPreferred) {
            if (isGirlPreferred) {
                httpMap.put(DraftAdHttpMap.KEY_OFFERED_GENDER_REQUIRED, DraftAdHttpMap.VALUE_N);
            } else {
                httpMap.put(DraftAdHttpMap.KEY_OFFERED_GENDER_REQUIRED, DraftAdHttpMap.VALUE_MALE);
            }
        } else if (isGirlPreferred) {
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_GENDER_REQUIRED, DraftAdHttpMap.VALUE_FEMALE);
        } else {
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_GENDER_REQUIRED, DraftAdHttpMap.VALUE_N);
        }

        if (AppVersion.isUk())
            if (draftOffered.is_feesApply() != null) {
                httpMap.put(
                        DraftAdHttpMap.KEY_OFFERED_FEES_APPLY,
                        (draftOffered.is_feesApply()) ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );
            }

        if (isStudentPreferred) {
            if (isProfessionalPreferred) {
                httpMap.put(DraftAdHttpMap.KEY_OFFERED_SHARE_TYPE_REQUIRED, DraftAdHttpMap.VALUE_OFFERED_M);
            } else {
                httpMap.put(DraftAdHttpMap.KEY_OFFERED_SHARE_TYPE_REQUIRED, DraftAdHttpMap.VALUE_STUDENT);
            }
        } else if (isProfessionalPreferred) {
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_SHARE_TYPE_REQUIRED, DraftAdHttpMap.VALUE_PROFESSIONAL);
        } else {
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_SHARE_TYPE_REQUIRED, DraftAdHttpMap.VALUE_OFFERED_M);
        }

        if (draftOffered.getRangeDesiredAge() != null) {
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_REQUIRED_MINIMUM_AGE, Integer.toString(draftOffered.getRangeDesiredAge().getKey()));
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_REQUIRED_MAXIMUM_AGE, Integer.toString(draftOffered.getRangeDesiredAge().getValue()));
        }

        if (draftOffered.getPhoneNumber() == null || !draftOffered.getDisplayPhone()) {
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_DISPLAY_PHONE_NUMBER, DraftAdHttpMap.VALUE_NO_SHORT);
        } else {
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_DISPLAY_PHONE_NUMBER, DraftAdHttpMap.VALUE_YES_SHORT);
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_PHONE_NUMBER, draftOffered.getPhoneNumber());
        }

        if (draftOffered.getTitle() != null)
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_ADVERT_TITLE, draftOffered.getTitle());
        if (draftOffered.getDescription() != null)
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_ADVERT_DESCRIPTION, draftOffered.getDescription());

        // Broadband
        if (draftOffered.isBroadband() != null) {
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_BROADBAND_AVAILABLE, (draftOffered.isBroadband() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT));
        }

        // Amenities
        PreferenceList amenitiesList = draftOffered.getAmenitiesList();
        for (PreferenceItem item : amenitiesList.getList()) {

            if (item.getName().equals(EditAdOfferedAmenityEnum.PARKING.toString())) {

                httpMap.put(
                        DraftAdHttpMap.KEY_OFFERED_PARKING_OFFERED,
                        item.isChecked() ? DraftAdHttpMap.VALUE_SHARED : DraftAdHttpMap.VALUE_OFFERED_NONE
                );

            } else if (item.getName().equals(EditAdOfferedAmenityEnum.BALCONY.toString())) {

                httpMap.put(
                        DraftAdHttpMap.KEY_OFFERED_BALCONY_OFFERED,
                        item.isChecked() ? DraftAdHttpMap.VALUE_SHARED : DraftAdHttpMap.VALUE_OFFERED_NONE
                );

            } else if (item.getName().equals(EditAdOfferedAmenityEnum.GARDEN.toString())) {

                httpMap.put(
                        DraftAdHttpMap.KEY_OFFERED_GARDEN_OFFERED,
                        item.isChecked() ? DraftAdHttpMap.VALUE_SHARED : DraftAdHttpMap.VALUE_OFFERED_NONE
                );

            } else if (item.getName().equals(EditAdOfferedAmenityEnum.DISABLED_ACCESS.toString())) {

                httpMap.put(
                        DraftAdHttpMap.KEY_OFFERED_DISABLED_ACCESS_OFFERED,
                        item.isChecked() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );

            } else if (item.getName().equals(EditAdOfferedAmenityEnum.GARAGE.toString())) {

                httpMap.put(
                        DraftAdHttpMap.KEY_OFFERED_GARAGE_OFFERED,
                        item.isChecked() ? DraftAdHttpMap.VALUE_SHARED : DraftAdHttpMap.VALUE_OFFERED_NONE
                );

            }
        }

        // Street name
        if (draftOffered.getStreetName() != null) {
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_STREET_NAME, draftOffered.getStreetName());
        }

        // Shared living room
        if (draftOffered.isSharedLivingRoom() != null) {
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_LIVING_ROOM_AVAILABLE, draftOffered.isSharedLivingRoom() ? DraftAdHttpMap.VALUE_SHARED : DraftAdHttpMap.VALUE_OFFERED_NONE);
        }

        // Interests
        if (draftOffered.getInterests() != null) {
            httpMap.put(DraftAdHttpMap.KEY_OFFERED_INTERESTS_OFFERED, draftOffered.getInterests());
        }

        // JSON Extras
        if (draftOffered.getJsonExtras() != null) {
            Json2HttpUtil util = new Json2HttpUtil();
            Parameters extras = util.transform(draftOffered.getJsonExtras());
            httpMap.add(extras);
        }

        return httpMap;
    } //end adaptOffered(DraftAdOffered draftOffered)

    private DraftAdHttpMap adaptWanted(DraftAdWanted draftWanted) {
        DraftAdHttpMap httpMap = new DraftAdHttpMap();

        if (draftWanted.get_firstName() != null)
            httpMap.put(DraftAdHttpMap.KEY_FIRST_NAME, draftWanted.get_firstName());
        if (draftWanted.get_lastName() != null)
            httpMap.put(DraftAdHttpMap.KEY_LAST_NAME, draftWanted.get_lastName());
        if (draftWanted.get_email() != null)
            httpMap.put(DraftAdHttpMap.KEY_EMAIL, draftWanted.get_email());
        if (draftWanted.get_advertiserType() != null)
            httpMap.put(DraftAdHttpMap.KEY_WANTED_WE_ARE, adaptAdvertiserTypeWanted(draftWanted.get_advertiserType()));

        if ( // if it's only one advertiser, his age is set by minimum age
                (draftWanted.get_advertiserType() == DraftAdWanted.AdvertiserType.MALE) ||
                        (draftWanted.get_advertiserType() == DraftAdWanted.AdvertiserType.FEMALE)) {

            httpMap.put(DraftAdHttpMap.KEY_WANTED_MINIMUM_AGE, Integer.toString(draftWanted.get_age()));

        } else { // for more than one advertiser, age is set by minimum and maximum values

            httpMap.put(
                    DraftAdHttpMap.KEY_WANTED_MINIMUM_AGE,
                    Integer.toString(draftWanted.get_ageMin())
            );

            httpMap.put(
                    DraftAdHttpMap.KEY_WANTED_MAXIMUM_AGE,
                    Integer.toString(draftWanted.get_ageMax())
            );

        }

        if (draftWanted.isCouple() != null) {
            httpMap.put(
                    DraftAdHttpMap.KEY_WANTED_COUPLE,
                    draftWanted.isCouple() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
            );
        }

        if (draftWanted.get_occupation() != null)
            httpMap.put(DraftAdHttpMap.KEY_WANTED_OCCUPATION, adaptOccupationWanted(draftWanted.get_occupation()));
        if (draftWanted.is_hasPets() != null)
            httpMap.put(DraftAdHttpMap.KEY_WANTED_PETS, draftWanted.is_hasPets() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT);

        if (draftWanted.get_preferenceList() != null) {
            PreferenceList list = draftWanted.get_preferenceList();

            List<PreferenceItem> lPreference = list.getList();

            boolean isGuyPreferred = false;
            boolean isGirlPreferred = false;
            boolean isStudentPreferred = false;
            boolean isProfessionalPreferred = false;
            for (PreferenceItem item : lPreference) {
                if (item.getId() == PlaceAdWantedPreferenceEnum.GUYS.ordinal()) {
                    if (item.isChecked())
                        isGuyPreferred = true;
                } else if (item.getId() == PlaceAdWantedPreferenceEnum.GIRLS.ordinal()) {
                    if (item.isChecked())
                        isGirlPreferred = true;
                } else if (item.getId() == PlaceAdWantedPreferenceEnum.SMOKERS.ordinal()) {
                    httpMap.put(
                            DraftAdHttpMap.KEY_WANTED_SMOKING_ALLOWED,
                            (item.isChecked()) ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                    );

                } else if (item.getId() == PlaceAdWantedPreferenceEnum.STUDENTS.ordinal()) {
                    if (item.isChecked())
                        isStudentPreferred = true;
                } else if (item.getId() == PlaceAdWantedPreferenceEnum.PROFESSIONALS.ordinal()) {
                    if (item.isChecked())
                        isProfessionalPreferred = true;
                } else if (item.getId() == PlaceAdWantedPreferenceEnum.PETS.ordinal()) {
                    httpMap.put(DraftAdHttpMap.KEY_WANTED_PETS_ALLOWED,
                            (item.isChecked()) ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                    );
                }
            }

            if (isGuyPreferred) {
                if (isGirlPreferred) {
                    httpMap.put(DraftAdHttpMap.KEY_WANTED_GENDER_REQUIRED, DraftAdHttpMap.VALUE_N);
                } else {
                    httpMap.put(DraftAdHttpMap.KEY_WANTED_GENDER_REQUIRED, DraftAdHttpMap.VALUE_MALE);
                }
            } else if (isGirlPreferred) {
                httpMap.put(DraftAdHttpMap.KEY_WANTED_GENDER_REQUIRED, DraftAdHttpMap.VALUE_FEMALE);
            } else {
                httpMap.put(DraftAdHttpMap.KEY_WANTED_GENDER_REQUIRED, DraftAdHttpMap.VALUE_N);
            }

            if (isStudentPreferred) {
                if (isProfessionalPreferred) {
                    httpMap.put(DraftAdHttpMap.KEY_WANTED_SHARE_TYPE_REQUIRED, DraftAdHttpMap.VALUE_OCCUPATION_MIXED);
                } else {
                    httpMap.put(DraftAdHttpMap.KEY_WANTED_SHARE_TYPE_REQUIRED, DraftAdHttpMap.VALUE_STUDENT);
                }
            } else if (isProfessionalPreferred) {
                httpMap.put(DraftAdHttpMap.KEY_WANTED_SHARE_TYPE_REQUIRED, DraftAdHttpMap.VALUE_PROFESSIONAL);
            } else {
                httpMap.put(DraftAdHttpMap.KEY_WANTED_SHARE_TYPE_REQUIRED, DraftAdHttpMap.VALUE_OCCUPATION_MIXED);
            }

        }

        if (AppVersion.isUk()) {
            if (draftWanted.isDss() != null) {
                httpMap.put(DraftAdHttpMap.KEY_WANTED_DSS,
                        (draftWanted.isDss()) ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );
            }
        }

        if (draftWanted.getRangeDesiredAge() != null) {
            httpMap.put(DraftAdHttpMap.KEY_WANTED_REQUIRED_MINIMUM_AGE, Integer.toString(draftWanted.getRangeDesiredAge().getKey()));
            httpMap.put(DraftAdHttpMap.KEY_WANTED_REQUIRED_MAXIMUM_AGE, Integer.toString(draftWanted.getRangeDesiredAge().getValue()));
        }

        if (draftWanted.getRoomType() != null)
            httpMap.put(DraftAdHttpMap.KEY_WANTED_ROOM_TYPE, adaptRoom(draftWanted.getRoomType()));
        if (draftWanted.is_isInterestedInBuddyingUp() != null)
            httpMap.put(DraftAdHttpMap.KEY_WANTED_BUDDY_UP, draftWanted.is_isInterestedInBuddyingUp() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT);
        if (draftWanted.get_budget() != null) {
            httpMap.put(DraftAdHttpMap.KEY_WANTED_COMBINED_BUDGET, draftWanted.get_budget().get_amount());
            httpMap.put(DraftAdHttpMap.KEY_WANTED_PER, draftWanted.get_budget().get_periodicity());
        }

        if (draftWanted.get_areas() != null) {
            PlaceAdFullAreaSet areas = draftWanted.get_areas();
            StringBuilder strBuilderAreas = new StringBuilder("");

            for (PlaceAdArea area : areas.getLevel(2)) {
                strBuilderAreas.append(area.getId());
                strBuilderAreas.append(",");
            }

            if (strBuilderAreas.length() > 0)
                strBuilderAreas.deleteCharAt(strBuilderAreas.length() - 1);

            httpMap.put(DraftAdHttpMap.KEY_WANTED_AREAS_WATCH, strBuilderAreas.toString());

        }

        if (draftWanted.get_areas().getLevel(0).iterator().next() == null)
            httpMap.put(DraftAdHttpMap.KEY_GL_ID_SELECTED, draftWanted.getGlIdSelected());
        else
            httpMap.put(DraftAdHttpMap.KEY_GL_ID_SELECTED, draftWanted.get_areas().getLevel(0).iterator().next().getId());

        if (draftWanted.get_availableFrom() != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy", Locale.UK);
            httpMap.put(DraftAdHttpMap.KEY_WANTED_DATE_AVAILABLE, sdf.format(draftWanted.get_availableFrom().getTime()));
        }
        httpMap.put(DraftAdHttpMap.KEY_WANTED_ACCOMMODATION_MIN, Integer.toString(draftWanted.get_minMonths()));
        httpMap.put(DraftAdHttpMap.KEY_WANTED_ACCOMMODATION_MAX, Integer.toString(draftWanted.get_maxMonths()));
        if (draftWanted.getDaysOfWeek() != null)
            httpMap.put(DraftAdHttpMap.KEY_WANTED_DAYS_OF_WEEK, adaptDaysOfWeek(draftWanted.getDaysOfWeek()));

        if (draftWanted.getPhoneNumber() == null || !draftWanted.getDisplayPhone()) {
            httpMap.put(DraftAdHttpMap.KEY_WANTED_ALLOW_PHONE_NUMBER, DraftAdHttpMap.VALUE_NO_SHORT);
        } else {
            httpMap.put(DraftAdHttpMap.KEY_WANTED_ALLOW_PHONE_NUMBER, DraftAdHttpMap.VALUE_YES_SHORT);
            httpMap.put(DraftAdHttpMap.KEY_WANTED_PHONE_NUMBER, draftWanted.getPhoneNumber());
        }
        if (draftWanted.getTitle() != null)
            httpMap.put(DraftAdHttpMap.KEY_WANTED_ADVERT_TITLE, draftWanted.getTitle());
        if (draftWanted.getDescription() != null)
            httpMap.put(DraftAdHttpMap.KEY_WANTED_ADVERT_DESCRIPTION, draftWanted.getDescription());

        // Interests
        if (draftWanted.getInterests() != null)
            httpMap.put(DraftAdHttpMap.KEY_WANTED_INTERESTS, draftWanted.getInterests());

        // Amenities
        PreferenceList amenitiesList = draftWanted.getAmenitiesList();
        for (PreferenceItem item : amenitiesList.getList()) {

            if (item.getName().equals(EditAdWantedAmenityEnum.FURNITURE.toString())) {

                httpMap.put(
                        DraftAdHttpMap.KEY_WANTED_FURNISHED,
                        item.isChecked() ? DraftAdHttpMap.VALUE_WANTED_FURNISHED_YES : DraftAdHttpMap.VALUE_NO_SHORT
                );

            } else if (item.getName().equals(EditAdWantedAmenityEnum.SHARED_LIVING_ROOM.toString())) {

                httpMap.put(
                        DraftAdHttpMap.KEY_WANTED_SHARED_LIVING_ROOM_AVAILABLE,
                        item.isChecked() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );

            } else if (item.getName().equals(EditAdWantedAmenityEnum.GARAGE.toString())) {

                httpMap.put(
                        DraftAdHttpMap.KEY_WANTED_GARAGE,
                        item.isChecked() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );

            } else if (item.getName().equals(EditAdWantedAmenityEnum.WASHING_MACHINE.toString())) {

                httpMap.put(
                        DraftAdHttpMap.KEY_WANTED_WASHING_MACHINE,
                        item.isChecked() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );

            } else if (item.getName().equals(EditAdWantedAmenityEnum.DISABLE_ACCESS.toString())) {

                httpMap.put(
                        DraftAdHttpMap.KEY_WANTED_DISABLED_ACCESS,
                        item.isChecked() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );

            } else if (item.getName().equals(EditAdWantedAmenityEnum.GARDEN.toString())) {

                httpMap.put(
                        DraftAdHttpMap.KEY_WANTED_GARDEN,
                        item.isChecked() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );

            } else if (item.getName().equals(EditAdWantedAmenityEnum.BROADBAND.toString())) {

                httpMap.put(
                        DraftAdHttpMap.KEY_WANTED_BROADBAND,
                        item.isChecked() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );

            } else if (item.getName().equals(EditAdWantedAmenityEnum.BALCONY.toString())) {

                httpMap.put(
                        DraftAdHttpMap.KEY_WANTED_BALCONY,
                        item.isChecked() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );

            } else if (item.getName().equals(EditAdWantedAmenityEnum.EN_SUITE.toString())) {

                httpMap.put(
                        DraftAdHttpMap.KEY_WANTED_EN_SUITE,
                        item.isChecked() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );

            } else if (item.getName().equals(EditAdWantedAmenityEnum.PARKING.toString())) {

                httpMap.put(
                        DraftAdHttpMap.KEY_WANTED_PARKING,
                        item.isChecked() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT
                );

            }

        }

        if (draftWanted.is_isSmoker() != null)
            httpMap.put(DraftAdHttpMap.KEY_WANTED_SMOKING, draftWanted.is_isSmoker() ? DraftAdHttpMap.VALUE_YES_SHORT : DraftAdHttpMap.VALUE_NO_SHORT);

        return httpMap;
    } //end adaptWanted(DraftAdWanted draftWanted)
}
