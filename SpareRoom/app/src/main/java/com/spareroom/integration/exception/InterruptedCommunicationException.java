package com.spareroom.integration.exception;

/**
 * Created by miguel.rossi on 28/09/2016.
 */
public class InterruptedCommunicationException extends Exception {
    private static final long serialVersionUID = 5681800516150741066L;

    public InterruptedCommunicationException() {
        super();
    }

    public InterruptedCommunicationException(String message) {
        super(message);
    }

    public InterruptedCommunicationException(Exception e) {
        super(e);
    }

    public InterruptedCommunicationException(Throwable throwable) {
        super(throwable);
    }

}
