package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.ISavedAdListDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.SavedAdWantedListParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.service.AdRestService;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SearchType;

import javax.inject.Inject;

public class SavedAdWantedListDAO implements ISavedAdListDAO {

    private final AdRestService adRestService;
    private final SavedAdWantedListParser parser;

    @Inject
    SavedAdWantedListDAO(AdRestService adRestService, SavedAdWantedListParser parser) {
        this.parser = parser;
        this.adRestService = adRestService;
    }

    public Object read(Parameters parameters) throws AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException {

        parameters.add("filter", "shortlist");
        parameters.add("hide_unsuitable", "1");

        String response = adRestService.getAdverts(parameters, SearchType.WANTED);

        return parser.parse(response);

    }
}
