package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.ISearchSuggestionListDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.SearchSuggestionListParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.SearchSuggestionRestService;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SearchAutocompleteList;

import java.util.Calendar;

public class SearchSuggestionListDAO implements ISearchSuggestionListDAO {

    private final SearchSuggestionRestService searchSuggestionRestService;

    SearchSuggestionListDAO() {
        searchSuggestionRestService = new SearchSuggestionRestService();
    }

    public SearchAutocompleteList read(Parameters parameters) throws AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException {
        parameters.add("limit", "20");
        parameters.add("timestamp", Long.toString(Calendar.getInstance().getTimeInMillis()));
        parameters.add("campus", "off");
        parameters.add("format", "json");

        String response = searchSuggestionRestService.getSuggestions(parameters);
        try {
            SearchSuggestionListParser parser = new SearchSuggestionListParser();

            return parser.parse(response);
        } catch (InvalidJSONFormatException e) {
            return null;
        }

    }

}
