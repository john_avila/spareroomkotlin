package com.spareroom.integration.analytics.firebase;

/**
 * Custom tags for Firebase
 * <p>
 * Created by miguel.rossi on 30/06/2016.
 */
public class AnalyticsTagsFirebase {

    public class Event {
        public static final String SIGN_UP_FAILED = "sign_up_failed";
        public static final String LOGIN_FAILED = "login_failed";
        public static final String LOGOUT = "logout";
        public static final String RECOVER_PASSWORD = "password_recovered";
        public static final String RECOVER_PASSWORD_FAILED = "password_recover_failed";
        public static final String FACEBOOK_REDIRECTION_LOGIN_TO_REGISTER = "fb_redirect_login_to_signup";
        public static final String FACEBOOK_REDIRECTION_REGISTER_TO_LOGIN = "fb_redirect_signup_to_login";
        public static final String SHOW_HOME_FRAGMENT_ON_BACK_PRESS_FAIL = "show_home_on_back_press_fail";

    }

    public class Param {
        public static final String KEY_LOGIN_METHOD = "login_method";
        public static final String KEY_REGISTER_FAILED = "sign_up_error";
        public static final String KEY_LOGIN_FAILED = "login_error";
        public static final String KEY_RECOVER_PASSWORD_FAILED = "recovered_password_fail";
        public static final String KEY_REGISTER_USER_TYPE = "user_type";
        public static final String KEY_FAILED_ERROR_CODE = "error_code";

        // Register Methods
        public static final String VALUE_REGISTER_METHOD_EMAIL = "email";
        public static final String VALUE_REGISTER_METHOD_FACEBOOK = "facebook";

        // Login Methods
        public static final String VALUE_LOGIN_METHOD_EMAIL = "email";
        public static final String VALUE_LOGIN_METHOD_FACEBOOK = "facebook";

        // Upgrades Categories
        public static final String VALUE_ITEM_CATEGORY_UPGRADE = "upgrade";
        public static final String VALUE_ITEM_CATEGORY_EXTRA_LISTING = "extra_listing";

        // Register Errors
        public static final String VALUE_REGISTER_FAILED_ERROR_INVALID_EMAIL = "invalid_email";
        public static final String VALUE_REGISTER_FAILED_ERROR_INVALID_EMAIL_LENGTH = "invalid_email_length";
        public static final String VALUE_REGISTER_FAILED_ERROR_CONFIRM_PASSWORD_FAILED = "password_confirmation_failed";
        public static final String VALUE_REGISTER_FAILED_ERROR_AGREE_TERMS_N_CONDITIONS = "not_agree_with_terms_and_conditions";
        public static final String VALUE_REGISTER_FAILED_ERROR_USED_EMAIL = "email_already_in_use";
        public static final String VALUE_REGISTER_FAILED_ERROR_MISSED_FIRST_NAME = "missed_first_name";
        public static final String VALUE_REGISTER_FAILED_ERROR_MISSED_LAST_NAME = "missed_last_name";
        public static final String VALUE_REGISTER_FAILED_ERROR_MISSED_USER_TYPE = "no_user_type";
        public static final String VALUE_REGISTER_FAILED_OTHER_ERROR = "other_error";
        public static final String VALUE_REGISTER_FAILED_UNDEFINED_ERROR = "undefined_error";

        // Login Errors
        public static final String VALUE_LOGIN_FAILED_ERROR_NO_PASSWORD = "password_empty";
        public static final String VALUE_LOGIN_FAILED_ERROR_UNKNOWN_EMAIL = "unknown_email";
        public static final String VALUE_LOGIN_FAILED_ERROR_INVALID_EMAIL = "invalid_email";
        public static final String VALUE_LOGIN_FAILED_ERROR_WRONG_PASSWORD = "wrong_password";
        public static final String VALUE_LOGIN_FAILED_ERROR_ALREADY_LOGGED_IN = "already_logged_in";
        public static final String VALUE_LOGIN_FAILED_ERROR_MISSED_EMAIL_AND_PASSWORD = "no_email_or_password";
        public static final String VALUE_LOGIN_FAILED_ERROR_MISSED_EMAIL = "no_email";
        public static final String VALUE_LOGIN_FAILED_OTHER_ERROR = "other_error";
        public static final String VALUE_LOGIN_FAILED_UNDEFINED_ERROR = "undefined_error";

        // Recovering password Errors
        public static final String VALUE_RECOVER_PASSWORD_FAILED_THREE_REMINDERS = "three_reminders_sent";
        public static final String VALUE_RECOVER_PASSWORD_FAILED_EMAIL = "email_not_valid";
        public static final String VALUE_RECOVER_PASSWORD_OTHER_ERROR = "other_error";
        public static final String VALUE_RECOVER_PASSWORD_UNDEFINED_ERROR = "undefined_error";

        // Register - User type
        public static final String VALUE_REGISTER_USER_TYPE_SEEKER_OFFERER = "seeker_offerer";
        public static final String VALUE_REGISTER_USER_TYPE_SEEKER = "seeker";
        public static final String VALUE_REGISTER_USER_TYPE_OFFERER = "offerer";

    }

}
