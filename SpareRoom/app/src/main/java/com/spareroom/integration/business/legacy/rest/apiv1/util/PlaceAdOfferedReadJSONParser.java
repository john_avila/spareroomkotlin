package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.controller.AppVersion;
import com.spareroom.integration.business.apiv2.ApiParams;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.lib.core.Pair;
import com.spareroom.model.business.*;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

public class PlaceAdOfferedReadJSONParser {

    //region PRIVATE METHODS

    private DraftAdOffered.PropertyType adaptPropertyType(String type) {
        switch (type) {
            case DraftAdHttpMap.VALUE_OFFERED_PROPERTY_FLAT:
                return DraftAdOffered.PropertyType.FLAT;
            case DraftAdHttpMap.VALUE_OFFERED_PROPERTY_HOUSE:
                return DraftAdOffered.PropertyType.HOUSE;
            case DraftAdHttpMap.VALUE_OFFERED_PROPERTY_OTHER:
                return DraftAdOffered.PropertyType.OTHER;
            default:
                return null;
        }
    }

    private DraftAdOffered.AdvertiserType adaptAdvertiserType(String type) {
        switch (type) {
            case DraftAdHttpMap.VALUE_OFFERED_LIL:
                return DraftAdOffered.AdvertiserType.LIVE_IN_LANDLORD;
            case DraftAdHttpMap.VALUE_OFFERED_LOL:
                return DraftAdOffered.AdvertiserType.LIVE_OUT_LANDLORD;
            case DraftAdHttpMap.VALUE_OFFERED_ADVERTISER_CURRENT_FLATMATE:
                return DraftAdOffered.AdvertiserType.CURRENT_FLATMATE;
            case DraftAdHttpMap.VALUE_OFFERED_ADVERTISER_AGENT:
                return DraftAdOffered.AdvertiserType.AGENT;
            case DraftAdHttpMap.VALUE_OFFERED_ADVERTISER_FORMER_FLATMATE:
                return DraftAdOffered.AdvertiserType.FORMER_FLATMATE;
            default:
                return null;
        }
    }

    private int adaptRoomSize(String roomSize) {
        switch (roomSize) {
            case DraftAdHttpMap.VALUE_OFFERED_ROOM_SIZE_SINGLE:
                return Room.ROOM_SIZE_SINGLE;
            case DraftAdHttpMap.VALUE_OFFERED_ROOM_SIZE_DOUBLE:
                return Room.ROOM_SIZE_DOUBLE;
            default:
                return -1;
        }
    }

    private int adaptRoomStatus(String roomStatus) {
        switch (roomStatus) {
            case DraftAdHttpMap.VALUE_OFFERED_STATUS_AVAILABLE:
                return Room.ROOM_STATUS_AVAILABLE;
            case DraftAdHttpMap.VALUE_OFFERED_STATUS_OCCUPIED:
                return Room.ROOM_STATUS_OCCUPIED;
            case DraftAdHttpMap.VALUE_OFFERED_STATUS_TAKEN:
                return Room.ROOM_STATUS_TAKEN;
            default:
                return -1;
        }
    }

    private int adaptGenderMale(String sMaleFemale) {
        int numMale = 0;
        for (int i = 0; i < sMaleFemale.length(); i++) {
            if (sMaleFemale.charAt(i) == 'M') {
                numMale += 1;
            }
        }
        return numMale;
    }

    private int adaptGenderFemale(String sMaleFemale) {
        int numFemale = 0;
        for (int i = 0; i < sMaleFemale.length(); i++) {
            if (sMaleFemale.charAt(i) == 'F') {
                numFemale += 1;
            }
        }
        return numFemale;
    }

    //endregion PRIVATE METHODS

    //region PUBLIC METHODS

    /**
     * Parses the response after registering a new user
     *
     * @param json String containing the response
     * @return an User object if the the user could be registered, SpareroomStatus if there were problems in the request
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public Object parse(String json) throws InvalidJSONFormatException {

        try {
            JSONObject firstNode = new JSONObject(json);

            if (firstNode.has("response_type")) {
                String responseType = firstNode.getString("response_type");
                if (responseType.equals("error")) {
                    SpareroomStatus im = new SpareroomStatus();
                    if (firstNode.has("response_id")) {
                        // We assume API spec is correct and response_id is an int
                        int responseId = firstNode.getInt("response_id");
                        im.setCode(Integer.toString(responseId));
                    } else
                        im.setCode(SpareroomStatusCode.CODE_0);
                    if (firstNode.has("error")) {
                        im.setMessage(firstNode.getString("error"));
                    }
                    return im;
                }
            }

            if (firstNode.has("existing_advert_data")) {
                JSONObject jsonAdvert = firstNode.getJSONObject("existing_advert_data");

                DraftAdOffered draftOffered = new DraftAdOffered();
                // page 1
                if (jsonAdvert.has(DraftAdHttpMap.KEY_FIRST_NAME)) {
                    draftOffered.set_firstName(jsonAdvert.getString(DraftAdHttpMap.KEY_FIRST_NAME));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_FIRST_NAME);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_LAST_NAME)) {
                    draftOffered.set_lastName(jsonAdvert.getString(DraftAdHttpMap.KEY_LAST_NAME));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_LAST_NAME);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_EMAIL)) {
                    draftOffered.set_email(jsonAdvert.getString(DraftAdHttpMap.KEY_EMAIL));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_EMAIL);
                }

                // page 2
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_PROPERTY_TYPE)) {
                    draftOffered.set_propertyType(adaptPropertyType(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_PROPERTY_TYPE)));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_PROPERTY_TYPE);
                }
                // page 3
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_ADVERTISER_TYPE)) {
                    draftOffered.set_advertiserType(adaptAdvertiserType(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_ADVERTISER_TYPE)));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_ADVERTISER_TYPE);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_COMPANY_NAME)) {
                    if (draftOffered.get_advertiserType() == DraftAdOffered.AdvertiserType.AGENT) {
                        draftOffered.set_companyName(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_COMPANY_NAME));
                    }
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_COMPANY_NAME);
                }
                // page 4
                // _isAtProperty is for internal use
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_POST_CODE)) {
                    draftOffered.set_postCode(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_POST_CODE));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_POST_CODE);
                }
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_NEIGHBOURHOOD_ID)) {
                    draftOffered.set_area(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_NEIGHBOURHOOD_ID));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_NEIGHBOURHOOD_ID);
                }

                draftOffered.wholeProperty(ApiParams.YES.equals(jsonAdvert.optString(DraftAdHttpMap.KEY_OFFERED_WHOLE_PROPERTY)));
                // page 5
                // _firstPictureUri not used on the http form
                // _firstPicturePath not used on the http form
                // page 6
                // DELETE int sizeRoomList = draftOffered.get_rooms().size();
                // DELETE httpMap.put(DraftAdHttpMap.keyRoomsCount, Integer.toString(sizeRoomList));
                RoomList roomList = new RoomList();
                if ((jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_ROOMS_COUNT))) {
                    int sizeRoomList = Integer.parseInt(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_ROOMS_COUNT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_ROOMS_COUNT);

                    for (int i = 0; i < sizeRoomList; i++) {
                        Room room = new Room();
                        Price price = new Price();
                        price.set_amount(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_ROOM_PRICE + Integer.toString(i + 1)));
                        jsonAdvert.remove((DraftAdHttpMap.KEY_OFFERED_ROOM_PRICE + Integer.toString(i + 1)));
                        price.set_periodicity(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_ROOM_PER + Integer.toString(i + 1)));
                        jsonAdvert.remove((DraftAdHttpMap.KEY_OFFERED_ROOM_PER + Integer.toString(i + 1)));
                        room.set_rent(price);
                        room.set_roomSize(adaptRoomSize(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_ROOM_SIZE + Integer.toString(i + 1))));
                        jsonAdvert.remove((DraftAdHttpMap.KEY_OFFERED_ROOM_SIZE + Integer.toString(i + 1)));
                        room.set_roomStatus(adaptRoomStatus(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_ROOM_STATUS + Integer.toString(i + 1))));
                        jsonAdvert.remove((DraftAdHttpMap.KEY_OFFERED_ROOM_STATUS + Integer.toString(i + 1)));
                        room.deposit((String) jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_ROOM_DEPOSIT + Integer.toString(i + 1)));
                        roomList.add(room);
                    }
                }
                draftOffered.set_rooms(roomList);
                draftOffered.wholePropertyDeposit((String) jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_DEPOSIT));

                // page 7
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_DATE_AVAILABLE)) {
                    GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.UK);
                    String jsonDate = jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_DATE_AVAILABLE);
                    calendar.set(Integer.parseInt(jsonDate.substring(6, 10)),
                            Integer.parseInt(jsonDate.substring(3, 5)) - 1,
                            Integer.parseInt(jsonDate.substring(0, 2)));
                    draftOffered.set_availableFrom(calendar);
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_DATE_AVAILABLE);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_MON_FRI)) {
                    draftOffered.set_isMonFriLet(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_MON_FRI).equals(DraftAdHttpMap.VALUE_OFFERED_DAYS_OF_WEEK));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_MON_FRI);
                }

                // Bills included
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_BILLS_INCLUDED)) {
                    String billsIncluded = jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_BILLS_INCLUDED);
                    switch (billsIncluded) {
                        case DraftAdHttpMap.VALUE_YES_FULL_TEXT:
                            draftOffered.set_areBillsIncluded(DraftAdOffered.BillsIncluded.YES);
                            break;
                        case DraftAdHttpMap.VALUE_SOME_FULL_TEXT:
                            draftOffered.set_areBillsIncluded(DraftAdOffered.BillsIncluded.SOME);
                            break;
                        default: // DraftAdHttpMap.VALUE_NO_FULL_TEXT:
                            draftOffered.set_areBillsIncluded(DraftAdOffered.BillsIncluded.NO);
                            break;
                    }
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_BILLS_INCLUDED);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_EXISTING_GENDER)) {
                    draftOffered.set_numExistingGuys(adaptGenderMale(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_EXISTING_GENDER)));
                    draftOffered.set_numExistingGirls(adaptGenderFemale(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_EXISTING_GENDER)));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_EXISTING_GENDER);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_MINIMUM_AGE) &&
                        (!jsonAdvert.isNull(DraftAdHttpMap.KEY_OFFERED_MINIMUM_AGE))) {
                    Integer minAge = Integer.parseInt(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_MINIMUM_AGE));
                    Integer maxAge =
                            jsonAdvert.isNull(DraftAdHttpMap.KEY_OFFERED_MAXIMUM_AGE)
                                    ? Integer.parseInt(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_MINIMUM_AGE))
                                    : Integer.parseInt(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_MAXIMUM_AGE));

                    Pair<Integer, Integer> rangeExistingAges = new Pair<>(minAge, maxAge);

                    draftOffered.set_rangeExistingAges(rangeExistingAges);
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_MINIMUM_AGE);
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_MAXIMUM_AGE);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_ROOMS_IN_PROPERTY)) {
                    draftOffered.set_numBedroom(Integer.parseInt(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_ROOMS_IN_PROPERTY)));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_ROOMS_IN_PROPERTY);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_PETS)) {
                    draftOffered.set_isTherePet(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_PETS).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_PETS);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_SMOKING)) {
                    draftOffered.set_areThereSmokers(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_SMOKING).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_SMOKING);
                }

                // Happy to live with
                PreferenceList list = new PreferenceList();

                PreferenceItem itemGuys = new PreferenceItem(PlaceAdOfferedPreferenceEnum.GUYS.ordinal());
                PreferenceItem itemGirls = new PreferenceItem(PlaceAdOfferedPreferenceEnum.GIRLS.ordinal());
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_GENDER_REQUIRED)) {
                    itemGuys.setChecked(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_GENDER_REQUIRED).equals(DraftAdHttpMap.VALUE_MALE));
                    itemGirls.setChecked(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_GENDER_REQUIRED).equals(DraftAdHttpMap.VALUE_FEMALE));

                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_GENDER_REQUIRED);
                }
                // If both of them are false is the same than both true, so for make it more visual....
                if (!itemGuys.isChecked() && !itemGirls.isChecked()) {
                    itemGuys.setChecked(true);
                    itemGirls.setChecked(true);
                }
                list.add(itemGuys);
                list.add(itemGirls);

                PreferenceItem itemProfessional = new PreferenceItem(PlaceAdOfferedPreferenceEnum.STUDENTS.ordinal());
                PreferenceItem itemStudent = new PreferenceItem(PlaceAdOfferedPreferenceEnum.PROFESSIONALS.ordinal());
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_SHARE_TYPE_REQUIRED) &&
                        (!jsonAdvert.isNull(DraftAdHttpMap.KEY_OFFERED_SHARE_TYPE_REQUIRED))) {
                    itemProfessional.setChecked(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_SHARE_TYPE_REQUIRED).equals(DraftAdHttpMap.VALUE_STUDENT));
                    itemStudent.setChecked(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_SHARE_TYPE_REQUIRED).equals(DraftAdHttpMap.VALUE_PROFESSIONAL));

                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_SHARE_TYPE_REQUIRED);
                }
                // If both of them are false is the same than both true, so for make it more visual....
                if (!itemProfessional.isChecked() && !itemStudent.isChecked()) {
                    itemProfessional.setChecked(true);
                    itemStudent.setChecked(true);
                }
                list.add(itemProfessional);
                list.add(itemStudent);

                PreferenceItem itemSmoking = new PreferenceItem(PlaceAdOfferedPreferenceEnum.SMOKERS.ordinal());
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_SMOKING_ALLOWED)) {
                    itemSmoking.setChecked(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_SMOKING_ALLOWED).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_SMOKING_ALLOWED);
                }
                list.add(itemSmoking);

                PreferenceItem itemPets = new PreferenceItem(PlaceAdOfferedPreferenceEnum.PETS.ordinal());
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_PETS_ALLOWED)) {
                    itemPets.setChecked(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_PETS_ALLOWED).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_PETS_ALLOWED);
                }
                list.add(itemPets);

                if (AppVersion.isUk()) {
                    PreferenceItem itemDss = new PreferenceItem(PlaceAdOfferedPreferenceEnum.DSS.ordinal());
                    if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_DSS_WELCOME)) {
                        itemDss.setChecked(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_DSS_WELCOME).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                        jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_DSS_WELCOME);
                    }
                    list.add(itemDss);
                }

                PreferenceItem itemCouples = new PreferenceItem(PlaceAdOfferedPreferenceEnum.COUPLES.ordinal());
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_COUPLES_ALLOWED)) {
                    itemCouples.setChecked(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_COUPLES_ALLOWED).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_COUPLES_ALLOWED);
                }
                list.add(itemCouples);

                draftOffered.set_preferenceList(list);

                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_REQUIRED_MINIMUM_AGE) &&
                        jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_REQUIRED_MAXIMUM_AGE) &&
                        !jsonAdvert.isNull(DraftAdHttpMap.KEY_OFFERED_REQUIRED_MINIMUM_AGE) &&
                        !jsonAdvert.isNull(DraftAdHttpMap.KEY_OFFERED_REQUIRED_MAXIMUM_AGE)
                        ) {
                    Pair<Integer, Integer> rangeDesiredAges = new Pair<>(
                            Integer.parseInt(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_REQUIRED_MINIMUM_AGE)),
                            Integer.parseInt(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_REQUIRED_MAXIMUM_AGE)));
                    draftOffered.setRangeDesiredAge(rangeDesiredAges);
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_REQUIRED_MINIMUM_AGE);
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_REQUIRED_MAXIMUM_AGE);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_DISPLAY_PHONE_NUMBER)) {
                    draftOffered.setDisplayPhone(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_DISPLAY_PHONE_NUMBER).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_DISPLAY_PHONE_NUMBER);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_PHONE_NUMBER)) {
                    draftOffered.setPhoneNumber(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_PHONE_NUMBER));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_PHONE_NUMBER);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_ADVERT_TITLE)) {
                    draftOffered.setTitle(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_ADVERT_TITLE));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_ADVERT_TITLE);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_ADVERT_DESCRIPTION)) {
                    draftOffered.setDescription(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_ADVERT_DESCRIPTION));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_ADVERT_DESCRIPTION);
                }

                // Shared living room
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_LIVING_ROOM_AVAILABLE)) {
                    draftOffered.setSharedLivingRoom(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_LIVING_ROOM_AVAILABLE).equals(DraftAdHttpMap.VALUE_SHARED));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_LIVING_ROOM_AVAILABLE);
                }

                // Broadband
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_BROADBAND_AVAILABLE)) {
                    draftOffered.setBroadband(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_BROADBAND_AVAILABLE).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_BROADBAND_AVAILABLE);
                }

                // Street name
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_STREET_NAME)) {
                    draftOffered.setStreetName(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_STREET_NAME));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_STREET_NAME);
                }

                // Interests
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_INTERESTS_OFFERED)) {
                    draftOffered.setInterests(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_INTERESTS_OFFERED));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_INTERESTS_OFFERED);
                }

                // Amenities
                PreferenceList amenitiesList = new PreferenceList();
                PreferenceItem item;

                // Amenities: parking
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_PARKING_OFFERED) && !jsonAdvert.isNull(DraftAdHttpMap.KEY_OFFERED_PARKING_OFFERED)) {
                    item = new PreferenceItem(
                            EditAdOfferedAmenityEnum.PARKING.ordinal(),
                            EditAdOfferedAmenityEnum.PARKING.toString(),
                            jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_PARKING_OFFERED).equals(DraftAdHttpMap.VALUE_SHARED)
                    );
                } else {
                    item = new PreferenceItem(
                            EditAdOfferedAmenityEnum.PARKING.ordinal(),
                            EditAdOfferedAmenityEnum.PARKING.toString(),
                            false
                    );
                }
                jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_PARKING_OFFERED);
                amenitiesList.add(item);

                // Amenities: balcony
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_BALCONY_OFFERED) && !jsonAdvert.isNull(DraftAdHttpMap.KEY_OFFERED_BALCONY_OFFERED)) {
                    item = new PreferenceItem(
                            EditAdOfferedAmenityEnum.BALCONY.ordinal(),
                            EditAdOfferedAmenityEnum.BALCONY.toString(),
                            jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_BALCONY_OFFERED).equals(DraftAdHttpMap.VALUE_SHARED)
                    );
                } else {
                    item = new PreferenceItem(
                            EditAdOfferedAmenityEnum.BALCONY.ordinal(),
                            EditAdOfferedAmenityEnum.BALCONY.toString(),
                            false
                    );
                }
                jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_BALCONY_OFFERED);
                amenitiesList.add(item);

                // Amenities: garden
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_GARDEN_OFFERED) && !jsonAdvert.isNull(DraftAdHttpMap.KEY_OFFERED_GARDEN_OFFERED)) {
                    item = new PreferenceItem(
                            EditAdOfferedAmenityEnum.GARDEN.ordinal(),
                            EditAdOfferedAmenityEnum.GARDEN.toString(),
                            jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_GARDEN_OFFERED).equals(DraftAdHttpMap.VALUE_SHARED)
                    );
                } else {
                    item = new PreferenceItem(
                            EditAdOfferedAmenityEnum.GARDEN.ordinal(),
                            EditAdOfferedAmenityEnum.GARDEN.toString(),
                            false
                    );
                }
                jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_GARDEN_OFFERED);
                amenitiesList.add(item);

                // Amenities: disabled access
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_DISABLED_ACCESS_OFFERED) && !jsonAdvert.isNull(DraftAdHttpMap.KEY_OFFERED_DISABLED_ACCESS_OFFERED)) {
                    item = new PreferenceItem(
                            EditAdOfferedAmenityEnum.DISABLED_ACCESS.ordinal(),
                            EditAdOfferedAmenityEnum.DISABLED_ACCESS.toString(),
                            jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_DISABLED_ACCESS_OFFERED).equals(DraftAdHttpMap.VALUE_YES_SHORT)
                    );
                } else {
                    item = new PreferenceItem(
                            EditAdOfferedAmenityEnum.DISABLED_ACCESS.ordinal(),
                            EditAdOfferedAmenityEnum.DISABLED_ACCESS.toString(),
                            false
                    );
                }
                jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_DISABLED_ACCESS_OFFERED);
                amenitiesList.add(item);

                // Amenities: garage
                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_GARAGE_OFFERED) && !jsonAdvert.isNull(DraftAdHttpMap.KEY_OFFERED_GARAGE_OFFERED)) {
                    item = new PreferenceItem(
                            EditAdOfferedAmenityEnum.GARAGE.ordinal(),
                            EditAdOfferedAmenityEnum.GARAGE.toString(),
                            jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_GARAGE_OFFERED).equals(DraftAdHttpMap.VALUE_SHARED)
                    );
                } else {
                    item = new PreferenceItem(
                            EditAdOfferedAmenityEnum.GARAGE.ordinal(),
                            EditAdOfferedAmenityEnum.GARAGE.toString(),
                            false
                    );
                }
                jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_GARAGE_OFFERED);
                amenitiesList.add(item);

                draftOffered.setAmenitiesList(amenitiesList);

                if (jsonAdvert.has(DraftAdHttpMap.KEY_OFFERED_FEES_APPLY)) {
                    draftOffered.set_feesApply(jsonAdvert.getString(DraftAdHttpMap.KEY_OFFERED_FEES_APPLY).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_OFFERED_FEES_APPLY);
                }

                // Everything else that cannot be populated into the object is saved in raw JSON
                draftOffered.setJsonExtras(jsonAdvert.toString());

                return draftOffered;
            }

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

        throw new InvalidJSONFormatException();
    } // parse(String) end

    //endregion PUBLIC METHODS

}
