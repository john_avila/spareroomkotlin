package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.ISavedSearchListDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.SavedSearchListParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.AdSearchesRestService;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SavedSearchList;

public class SavedSearchListDAO implements ISavedSearchListDAO {

    private final AdSearchesRestService adSearchesRestService;

    SavedSearchListDAO() {
        adSearchesRestService = new AdSearchesRestService();
    }

    public SavedSearchList read(Parameters parameters) throws InvalidJSONFormatException, AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException {
        parameters.add("format", "json");

        String response = adSearchesRestService.getSavedSearches(parameters);

        SavedSearchListParser parser = new SavedSearchListParser();

        return parser.parse(response);
    }
}
