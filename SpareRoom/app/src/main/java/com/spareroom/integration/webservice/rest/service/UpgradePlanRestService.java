package com.spareroom.integration.webservice.rest.service;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.Parameters;

public class UpgradePlanRestService extends RestService {

    private static final String UPGRADE_PLAN = "/flatshare/upgrade.pl";

    public String getAvailablePlans(Parameters parameters) throws ClientErrorException, ServerErrorException, NetworkConnectivityException, AuthenticationException {
        return get(UPGRADE_PLAN, parameters);
    }

    public String upgradePlan(String[] names, String[] values) throws NetworkConnectivityException, ClientErrorException, ServerErrorException {
        return post(UPGRADE_PLAN, names, values);
    }
}
