package com.spareroom.integration.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.spareroom.controller.AppVersion;

import static android.content.Context.MODE_PRIVATE;

public class UpgradeFirstRunDao {

    public static boolean read(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(AppVersion.appId(), MODE_PRIVATE);
        return sharedPreferences.getBoolean("firstRun", true);
    }

    public static boolean update(Context context, boolean b) {
        SharedPreferences prefs = context.getSharedPreferences(AppVersion.appId(), MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        return editor.putBoolean("firstRun", b).commit();
    }

}
