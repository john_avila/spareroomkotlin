package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.apiv1.security.Authenticator;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.model.business.Parameters;

import java.net.URLEncoder;

import javax.inject.Inject;

import androidx.annotation.NonNull;

public class URIBuilder {
    public final static String URL_ENCODING = "UTF-8";
    public final static String API_KEY = "api_key=";
    public final static String API_SIG = "&api_sig=";
    public final static String EQUALS = "=";
    public final static String AMPERSAND = "&";
    public final static String QUESTION_MARK = "?";

    private final Authenticator authenticator;

    @Inject
    public URIBuilder(Authenticator authenticator) {
        this.authenticator = authenticator;
    }

    /**
     * Builds an authenticated HTTP GET URL following API v1 specifications.
     *
     * @param baseUri    base address
     * @param parameters contains the parameters needed for performing the search. Location required, pages, max per page and featured ads are optional.
     * @return valid URL for API v1
     * @throws AuthenticationException if the URL could not be authenticated
     */
    @NonNull
    public String buildGetURI(@NonNull String baseUri, @NonNull Parameters parameters) throws AuthenticationException {
        StringBuilder uri = new StringBuilder((baseUri + QUESTION_MARK));
        String[] parameterNames = parameters.getParameterNames();

        try {
            for (String name : parameterNames) {
                uri.append(URLEncoder.encode(name, URL_ENCODING))
                        .append(EQUALS)
                        .append(URLEncoder.encode(parameters.get(name), URL_ENCODING))
                        .append(AMPERSAND);
            }

            uri.append(API_KEY)
                    .append(authenticator.get_apiKey())
                    .append(API_SIG)
                    .append(authenticator.authenticate(parameters));

        } catch (Exception e) {
            throw new AuthenticationException(e);
        }

        return uri.toString();
    }

}
