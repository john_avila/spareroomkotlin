package com.spareroom.integration.business.apiv2

import com.spareroom.model.business.*

interface IOfferedAdvertsDao : IAdvertsDao<AdOffered> {

    @Throws(Exception::class)
    override fun getUserAdverts(userId: String, page: Int, maxPerPage: Int): AdvertsListRestResponse<AdOffered>

    @Throws(Exception::class)
    override fun getAdvert(advertId: String): SingleAdvertRestResponse<AdOffered>

    @Throws(Exception::class)
    override fun getSavedAdverts(offset: Int, maxPerPage: Int): AdvertsListRestResponse<AdOffered>
}