package com.spareroom.integration.business.legacy;

import com.spareroom.integration.business.legacy.rest.*;
import com.spareroom.model.business.SearchType;

public interface ILegacyBusinessAbstractFactory {
    IAdBoardDAO getAdBoardDAO(SearchType searchType);

    IMessageThreadDAO getMessageThreadDAO();

    ILocationDAO getLocationDAO();

    ISavedAdDAO getSavedAdDAO();

    IMarkAdDAO getMarkedAdDAO();

    IClockCalledDAO getClockCalledDAO();

    IClockFeaturedDAO getClockFeaturedDAO();

    IRemovedSavedAdDAO getRemovedSavedAdDAO();

    IUpgradeListDAO getUpgradeListDAO();

    IUpgradeDAO getUpgradeDAO();

    ISavedSearchDAO getSavedSearchDAO();

    ISavedSearchListDAO getSavedSearchListDAO();

    IProfilePreferenceListDAO getProfilePreferenceListDAO();

    IMyOfferedAdDAO getMyOfferedAdDAO();

    IMyWantedAdDAO getMyWantedAdDAO();

    IGalleryDAO getGalleryDAO();

    IPictureDAO getPictureDAO();

    IPostcodeFinderDAO getPostcodeFinderDAO();

    IPlaceAdAreaDAO getPlaceAdAreaDAO();

    IPlaceAdOfferedDAO getPlaceAdOfferedDAO();

    IPlaceAdWantedDAO getPlaceAdWantedDAO();

    ISearchSuggestionListDAO getSearchSuggestionListDAO();

    IVideoDAO getVideoDAO();

    IVideoGalleryDAO getVideoGalleryDAO();

}
