package com.spareroom.integration.webservice.rest.service;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.model.business.Parameters;

import javax.inject.Inject;

public class LocationRestService extends RestService {

    private static final String MANAGE_WANTED_AD = "/flatshare/wanted_listing_step2.pl";
    private static final String MANAGE_OFFERED_AD = "/flatshare/offered-advert3.pl";
    private static final String POSTCODE = "/point_to_postcode_unit.pl";

    @Inject
    public LocationRestService() {
    }

    public String findPostcode(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(MANAGE_OFFERED_AD, parameters);
    }

    public String getPostcodeFromCoordinates(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(POSTCODE, parameters);
    }

    public String getAreas(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(MANAGE_WANTED_AD, parameters);
    }
}
