package com.spareroom.integration.business.apiv2

import com.spareroom.model.business.PostCodeRestResponse

interface ILocationDao {

    @Throws(Exception::class)
    fun getPostCode(latitude: Double, longitude: Double): PostCodeRestResponse
}