package com.spareroom.integration.webservice.rest.client.methods;

import android.app.Application;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.legacy.rest.apiv1.util.UpgradeSessionJSONParser;
import com.spareroom.ui.util.ConnectivityChecker;

import java.io.File;

import okhttp3.*;

public class HttpPostMultipart extends HttpRequest {

    private final String[] names;
    private final String[] values;
    private final String[] namesMultipart;
    private final Object[] valuesMultipart;
    private final String extension;

    public HttpPostMultipart(SpareroomContext spareroomContext, UpgradeSessionJSONParser sessionParser, OkHttpClient okHttpClient,
                             String userAgent, ConnectivityChecker connectivityChecker, Application application,
                             String url, String[] names, String[] values, String[] namesMultipart, Object[] valuesMultipart, String extension) {
        super(spareroomContext, sessionParser, okHttpClient, userAgent, connectivityChecker, application, url);
        this.names = names;
        this.values = values;
        this.namesMultipart = namesMultipart;
        this.valuesMultipart = valuesMultipart;
        this.extension = extension;
    }

    @Override
    public Request getRequest() {
        return new Request.Builder()
                .url(getUrl())
                .addHeader(HEADER_CHARSET, CHARSET_UTF8)
                .addHeader(HEADER_CONNECTION, CONNECTION_KEEP_ALIVE)
                .addHeader(HEADER_USER_AGENT, getUserAgent())
                .cacheControl(CacheControl.FORCE_NETWORK)
                .post(getRequestBody())
                .build();
    }

    MultipartBody getRequestBody() {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        for (int i = 0; i < names.length; i++) {
            builder.addFormDataPart(names[i], values[i]);
        }

        for (int i = 0; i < namesMultipart.length; i++) {
            builder.addFormDataPart(namesMultipart[i], "file." + extension, RequestBody.create(MEDIA_TYPE_ALL, new File((String) valuesMultipart[i])));
        }

        return builder.build();
    }
}
