package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.lib.core.Pair;
import com.spareroom.model.business.DraftAdHttpMap;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.model.business.EditAdWantedAmenityEnum;
import com.spareroom.model.business.PlaceAdArea;
import com.spareroom.model.business.PlaceAdFullArea;
import com.spareroom.model.business.PlaceAdFullAreaSet;
import com.spareroom.model.business.PlaceAdWantedPreferenceEnum;
import com.spareroom.model.business.PreferenceItem;
import com.spareroom.model.business.PreferenceList;
import com.spareroom.model.business.Price;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.business.SpareroomStatusCode;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;

public class PlaceAdWantedReadJSONParser {

    private DraftAdWanted.AdvertiserType adaptAdvertiserTypeWanted(String at) {
        switch (at) {
            case DraftAdHttpMap.VALUE_WANTED_WE_ARE_ONE_MALE:
                return DraftAdWanted.AdvertiserType.MALE;
            case DraftAdHttpMap.VALUE_WANTED_WE_ARE_ONE_FEMALE:
                return DraftAdWanted.AdvertiserType.FEMALE;
            case DraftAdHttpMap.VALUE_WANTED_WE_ARE_TWO_MALES:
                return DraftAdWanted.AdvertiserType.TWO_MALES;
            case DraftAdHttpMap.VALUE_WANTED_WE_ARE_TWO_FEMALES:
                return DraftAdWanted.AdvertiserType.TWO_FEMALES;
            case DraftAdHttpMap.VALUE_WANTED_WE_ARE_ONE_MALE_ONE_FEMALE:
                return DraftAdWanted.AdvertiserType.MALE_FEMALE;
        }
        return null;
    }

    private DraftAdWanted.Occupation adaptOccupationWanted(String o) {
        switch (o) {
            case DraftAdHttpMap.VALUE_STUDENT:
                return DraftAdWanted.Occupation.STUDENT;
            case DraftAdHttpMap.VALUE_PROFESSIONAL:
                return DraftAdWanted.Occupation.PROFESSIONAL;
            case DraftAdHttpMap.VALUE_OCCUPATION_OTHER:
                return DraftAdWanted.Occupation.OTHER;
            case DraftAdHttpMap.VALUE_OCCUPATION_MIXED:
                return DraftAdWanted.Occupation.MIXED;
        }
        return null;
    }

    private DraftAdWanted.RoomType adaptRoom(String r) {
        switch (r) {
            case DraftAdHttpMap.VALUE_WANTED_ROOM_TYPE_SINGLE_OR_DOUBLE:
                return DraftAdWanted.RoomType.SINGLE_OR_DOUBLE;
            case DraftAdHttpMap.VALUE_WANTED_ROOM_TYPE_DOUBLE:
                return DraftAdWanted.RoomType.DOUBLE;
            case DraftAdHttpMap.VALUE_WANTED_ROOM_TYPE_DOUBLE_COUPLE:
                return DraftAdWanted.RoomType.DOUBLE_COUPLE;
            case DraftAdHttpMap.VALUE_WANTED_ROOM_TYPE_2_ROOMS:
                return DraftAdWanted.RoomType.TWO_ROOMS;
            case DraftAdHttpMap.VALUE_WANTED_ROOM_TYPE_TWIN_OR_2_ROOMS:
                return DraftAdWanted.RoomType.TWIN_OR_TWO_ROOMS;
        }
        return null;
    }

    private DraftAdWanted.DaysOfWeek adaptDaysOfWeek(String s) {
        switch (s) {
            case DraftAdHttpMap.VALUE_WANTED_DAYS_OF_WEEK_MON_FRI:
                return DraftAdWanted.DaysOfWeek.MONDAY_TO_FRIDAY;
            case DraftAdHttpMap.VALUE_WANTED_DAYS_OF_WEEK_7_DAYS:
                return DraftAdWanted.DaysOfWeek.SEVEN_A_DAY;
            case DraftAdHttpMap.VALUE_WANTED_DAYS_OF_WEEK_WEEKENDS:
                return DraftAdWanted.DaysOfWeek.WEEKENDS_ONLY;
        }
        return null;
    }

    /**
     * Parses the response after registering a new user
     *
     * @param json String containing the response
     * @return an User object if the the user could be registered, SpareroomStatus if there were problems in the request
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public Object parse(String json) throws InvalidJSONFormatException {

        try {
            JSONObject firstNode = new JSONObject(json);

            if (firstNode.has("response_type")) {
                String responseType = firstNode.getString("response_type");
                if (responseType.equals("error")) {
                    SpareroomStatus im = new SpareroomStatus();
                    if (firstNode.has("response_id")) {
                        // We assume API spec is correct and response_id is an int
                        int responseId = firstNode.getInt("response_id");
                        im.setCode(Integer.toString(responseId));
                    } else
                        im.setCode(SpareroomStatusCode.CODE_0);
                    if (firstNode.has("error")) {
                        im.setMessage(firstNode.getString("error"));
                    }
                    return im;
                }
            }

            if (firstNode.has("existing_advert_data")) {
                JSONObject jsonAdvert = firstNode.getJSONObject("existing_advert_data");

                DraftAdWanted draftWanted = new DraftAdWanted();

                if (jsonAdvert.has(DraftAdHttpMap.KEY_FIRST_NAME)) {
                    draftWanted.set_firstName(jsonAdvert.getString(DraftAdHttpMap.KEY_FIRST_NAME));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_FIRST_NAME);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_LAST_NAME)) {
                    draftWanted.set_lastName(jsonAdvert.getString(DraftAdHttpMap.KEY_LAST_NAME));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_LAST_NAME);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_EMAIL)) {
                    draftWanted.set_email(jsonAdvert.getString(DraftAdHttpMap.KEY_EMAIL));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_EMAIL);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_WE_ARE)) {
                    draftWanted.set_advertiserType(adaptAdvertiserTypeWanted(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_WE_ARE)));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_WE_ARE);
                }

                // if it's only one advertiser, his age is set by minimum age
                if ((draftWanted.get_advertiserType() == DraftAdWanted.AdvertiserType.MALE) ||
                        (draftWanted.get_advertiserType() == DraftAdWanted.AdvertiserType.FEMALE)) {

                    if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_MINIMUM_AGE) &&
                            (!jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_MINIMUM_AGE))) {
                        draftWanted.set_age(Integer.parseInt(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_MINIMUM_AGE)));
                        jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_MINIMUM_AGE);
                    }

                } else { // for more than one advertiser, age is set by minimum and maximum values

                    if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_MINIMUM_AGE) &&
                            (!jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_MINIMUM_AGE))) {
                        draftWanted.set_ageMin(Integer.parseInt(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_MINIMUM_AGE)));
                        jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_MINIMUM_AGE);
                    }

                    if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_MAXIMUM_AGE) &&
                            (!jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_MAXIMUM_AGE))) {
                        draftWanted.set_ageMax(Integer.parseInt(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_MAXIMUM_AGE)));
                        jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_MAXIMUM_AGE);
                    }

                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_COUPLE)) {
                    draftWanted.setIsCouple(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_COUPLE).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_COUPLE);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_OCCUPATION)) {
                    draftWanted.set_occupation(adaptOccupationWanted(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_OCCUPATION)));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_OCCUPATION);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_PETS)) {
                    draftWanted.set_hasPets(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_PETS).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_PETS);
                }

                // Happy to live with
                PreferenceList list = new PreferenceList();

                PreferenceItem itemGuys = new PreferenceItem(
                        PlaceAdWantedPreferenceEnum.GUYS.ordinal(),
                        PlaceAdWantedPreferenceEnum.GUYS.toString(),
                        false
                );
                PreferenceItem itemGirls = new PreferenceItem(
                        PlaceAdWantedPreferenceEnum.GIRLS.ordinal(),
                        PlaceAdWantedPreferenceEnum.GIRLS.toString(),
                        false
                );
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_GENDER_REQUIRED)) {
                    itemGuys.setChecked(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_GENDER_REQUIRED).equals(DraftAdHttpMap.VALUE_MALE));
                    itemGirls.setChecked(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_GENDER_REQUIRED).equals(DraftAdHttpMap.VALUE_FEMALE));

                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_GENDER_REQUIRED);
                }
                // If both of them are false is the same than both true, so for make it more visual....
                if (!itemGuys.isChecked() && !itemGirls.isChecked()) {
                    itemGuys.setChecked(true);
                    itemGirls.setChecked(true);
                }
                list.add(itemGuys);
                list.add(itemGirls);

                PreferenceItem itemProfessional = new PreferenceItem(
                        PlaceAdWantedPreferenceEnum.STUDENTS.ordinal(),
                        PlaceAdWantedPreferenceEnum.STUDENTS.toString(),
                        false
                );
                list.add(itemProfessional);
                PreferenceItem itemStudent = new PreferenceItem(
                        PlaceAdWantedPreferenceEnum.PROFESSIONALS.ordinal(),
                        PlaceAdWantedPreferenceEnum.PROFESSIONALS.toString(),
                        false
                );
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_SHARE_TYPE_REQUIRED) &&
                        (!jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_SHARE_TYPE_REQUIRED))) {
                    itemProfessional.setChecked(!jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_SHARE_TYPE_REQUIRED).equals(DraftAdHttpMap.VALUE_PROFESSIONAL));
                    itemStudent.setChecked(!jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_SHARE_TYPE_REQUIRED).equals(DraftAdHttpMap.VALUE_STUDENT));

                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_SHARE_TYPE_REQUIRED);
                }
                // If both of them are false is the same than both true, so for make it more visual....
                if (!itemProfessional.isChecked() && !itemStudent.isChecked()) {
                    itemProfessional.setChecked(true);
                    itemStudent.setChecked(true);
                }
                list.add(itemStudent);

                PreferenceItem itemSmoking = new PreferenceItem(
                        PlaceAdWantedPreferenceEnum.SMOKERS.ordinal(),
                        PlaceAdWantedPreferenceEnum.SMOKERS.toString(),
                        false
                );
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_SMOKING_ALLOWED)) {
                    itemSmoking.setChecked(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_SMOKING_ALLOWED).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_SMOKING_ALLOWED);
                }
                list.add(itemSmoking);

                PreferenceItem itemPets = new PreferenceItem(
                        PlaceAdWantedPreferenceEnum.PETS.ordinal(),
                        PlaceAdWantedPreferenceEnum.PETS.toString(),
                        false
                );
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_PETS_ALLOWED)) {
                    itemPets.setChecked(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_PETS_ALLOWED).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_PETS_ALLOWED);
                }
                list.add(itemPets);

                draftWanted.set_preferenceList(list);

                // Dss
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_DSS)) {
                    draftWanted.setDss(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_DSS).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_DSS);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_REQUIRED_MINIMUM_AGE) &&
                        jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_REQUIRED_MAXIMUM_AGE) &&
                        !jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_REQUIRED_MINIMUM_AGE) &&
                        !jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_REQUIRED_MAXIMUM_AGE)) {
                    Pair<Integer, Integer> rangeDesiredAges = new Pair<>(
                            Integer.parseInt(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_REQUIRED_MINIMUM_AGE)),
                            Integer.parseInt(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_REQUIRED_MAXIMUM_AGE)));
                    draftWanted.setRangeDesiredAge(rangeDesiredAges);
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_REQUIRED_MINIMUM_AGE);
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_REQUIRED_MAXIMUM_AGE);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_ROOM_TYPE)) {
                    draftWanted.setRoomType(adaptRoom(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_ROOM_TYPE)));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_ROOM_TYPE);
                }

                if ((jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_COMBINED_BUDGET)) &&
                        (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_PER))) {
                    Price budget = new Price();
                    budget.set_amount(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_COMBINED_BUDGET));
                    budget.set_periodicity(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_PER));
                    draftWanted.set_budget(budget);
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_COMBINED_BUDGET);
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_PER);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_BUDDY_UP)) {
                    draftWanted.set_isInterestedInBuddyingUp(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_BUDDY_UP).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_BUDDY_UP);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_SELECTED_AREAS)) {
                    PlaceAdFullAreaSet areas = new PlaceAdFullAreaSet();
                    JSONObject jsonAreas = jsonAdvert.getJSONObject(DraftAdHttpMap.KEY_WANTED_SELECTED_AREAS);
                    Iterator<String> iAreas = jsonAreas.keys();

                    while (iAreas.hasNext()) {
                        String key = iAreas.next();
                        PlaceAdFullArea area = new PlaceAdFullArea();
                        area.set(2, new PlaceAdArea(key, jsonAreas.getString(key), null));
                        areas.add(area);
                    }

                    draftWanted.set_areas(areas);
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_AREAS_WATCH);
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_SELECTED_AREAS);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_GL_ID_SELECTED))
                    draftWanted.setGlIdSelected(jsonAdvert.getString(DraftAdHttpMap.KEY_GL_ID_SELECTED));

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_DATE_AVAILABLE)) {
                    GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.UK);
                    String jsonDate = jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_DATE_AVAILABLE);
                    calendar.set(Integer.parseInt(jsonDate.substring(6, 10)),
                            Integer.parseInt(jsonDate.substring(3, 5)) - 1,
                            Integer.parseInt(jsonDate.substring(0, 2)));
                    draftWanted.set_availableFrom(calendar);
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_DATE_AVAILABLE);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_ACCOMMODATION_MIN)) {
                    draftWanted.set_minMonths(Integer.parseInt(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_ACCOMMODATION_MIN)));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_ACCOMMODATION_MIN);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_ACCOMMODATION_MAX)) {
                    draftWanted.set_maxMonths(Integer.parseInt(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_ACCOMMODATION_MAX)));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_ACCOMMODATION_MAX);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_DAYS_OF_WEEK)) {
                    draftWanted.setDaysOfWeek(adaptDaysOfWeek(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_DAYS_OF_WEEK)));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_DAYS_OF_WEEK);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_ALLOW_PHONE_NUMBER)) {
                    draftWanted.setDisplayPhone(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_ALLOW_PHONE_NUMBER).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_ALLOW_PHONE_NUMBER);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_PHONE_NUMBER)) {
                    draftWanted.setPhoneNumber(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_PHONE_NUMBER));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_PHONE_NUMBER);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_ADVERT_TITLE)) {
                    draftWanted.setTitle(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_ADVERT_TITLE));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_ADVERT_TITLE);
                }

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_ADVERT_DESCRIPTION)) {
                    draftWanted.setDescription(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_ADVERT_DESCRIPTION));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_ADVERT_DESCRIPTION);
                }

                // Interests
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_INTERESTS)) {
                    draftWanted.setInterests(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_INTERESTS));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_INTERESTS);
                }

                // Amenities
                PreferenceList amenitiesList = new PreferenceList();
                PreferenceItem item;

                // Amenities: Furniture
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_FURNISHED) && !jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_FURNISHED)) {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.FURNITURE.ordinal(),
                            EditAdWantedAmenityEnum.FURNITURE.toString(),
                            jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_FURNISHED).equals(DraftAdHttpMap.VALUE_WANTED_FURNISHED_YES)
                    );
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_FURNISHED);
                } else {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.FURNITURE.ordinal(),
                            EditAdWantedAmenityEnum.FURNITURE.toString(),
                            false
                    );
                }
                amenitiesList.add(item);

                // Amenities: Shared living room
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_SHARED_LIVING_ROOM_AVAILABLE) && !jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_SHARED_LIVING_ROOM_AVAILABLE)) {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.SHARED_LIVING_ROOM.ordinal(),
                            EditAdWantedAmenityEnum.SHARED_LIVING_ROOM.toString(),
                            jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_SHARED_LIVING_ROOM_AVAILABLE).equals(DraftAdHttpMap.VALUE_YES_SHORT)
                    );
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_SHARED_LIVING_ROOM_AVAILABLE);
                } else {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.SHARED_LIVING_ROOM.ordinal(),
                            EditAdWantedAmenityEnum.SHARED_LIVING_ROOM.toString(),
                            false
                    );
                }
                amenitiesList.add(item);

                // Amenities: Garage
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_GARAGE) && !jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_GARAGE)) {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.GARAGE.ordinal(),
                            EditAdWantedAmenityEnum.GARAGE.toString(),
                            jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_GARAGE).equals(DraftAdHttpMap.VALUE_YES_SHORT)
                    );
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_GARAGE);
                } else {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.GARAGE.ordinal(),
                            EditAdWantedAmenityEnum.GARAGE.toString(),
                            false
                    );
                }
                amenitiesList.add(item);

                // Amenities: Washing machine
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_WASHING_MACHINE) && !jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_WASHING_MACHINE)) {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.WASHING_MACHINE.ordinal(),
                            EditAdWantedAmenityEnum.WASHING_MACHINE.toString(),
                            jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_WASHING_MACHINE).equals(DraftAdHttpMap.VALUE_YES_SHORT)
                    );
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_WASHING_MACHINE);
                } else {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.WASHING_MACHINE.ordinal(),
                            EditAdWantedAmenityEnum.WASHING_MACHINE.toString(),
                            false
                    );
                }
                amenitiesList.add(item);

                // Amenities: Disabled access
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_DISABLED_ACCESS) && !jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_DISABLED_ACCESS)) {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.DISABLE_ACCESS.ordinal(),
                            EditAdWantedAmenityEnum.DISABLE_ACCESS.toString(),
                            jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_DISABLED_ACCESS).equals(DraftAdHttpMap.VALUE_YES_SHORT)
                    );
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_DISABLED_ACCESS);
                } else {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.DISABLE_ACCESS.ordinal(),
                            EditAdWantedAmenityEnum.DISABLE_ACCESS.toString(),
                            false
                    );
                }
                amenitiesList.add(item);

                // Amenities: Garden
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_GARDEN) && !jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_GARDEN)) {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.GARDEN.ordinal(),
                            EditAdWantedAmenityEnum.GARDEN.toString(),
                            jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_GARDEN).equals(DraftAdHttpMap.VALUE_YES_SHORT)
                    );
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_GARDEN);
                } else {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.GARDEN.ordinal(),
                            EditAdWantedAmenityEnum.GARDEN.toString(),
                            false
                    );
                }
                amenitiesList.add(item);

                // Amenities: BroadBand
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_BROADBAND) && !jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_BROADBAND)) {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.BROADBAND.ordinal(),
                            EditAdWantedAmenityEnum.BROADBAND.toString(),
                            jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_BROADBAND).equals(DraftAdHttpMap.VALUE_YES_SHORT)
                    );
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_BROADBAND);
                } else {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.BROADBAND.ordinal(),
                            EditAdWantedAmenityEnum.BROADBAND.toString(),
                            false
                    );
                }
                amenitiesList.add(item);

                // Amenities: Balcony
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_BALCONY) && !jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_BALCONY)) {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.BALCONY.ordinal(),
                            EditAdWantedAmenityEnum.BALCONY.toString(),
                            jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_BALCONY).equals(DraftAdHttpMap.VALUE_YES_SHORT)
                    );
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_BALCONY);
                } else {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.BALCONY.ordinal(),
                            EditAdWantedAmenityEnum.BALCONY.toString(),
                            false
                    );
                }
                amenitiesList.add(item);

                // Amenities: En suite
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_EN_SUITE) && !jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_EN_SUITE)) {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.EN_SUITE.ordinal(),
                            EditAdWantedAmenityEnum.EN_SUITE.toString(),
                            jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_EN_SUITE).equals(DraftAdHttpMap.VALUE_YES_SHORT)
                    );
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_EN_SUITE);
                } else {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.EN_SUITE.ordinal(),
                            EditAdWantedAmenityEnum.EN_SUITE.toString(),
                            false
                    );
                }
                amenitiesList.add(item);

                // Amenities: Parking
                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_PARKING) && !jsonAdvert.isNull(DraftAdHttpMap.KEY_WANTED_PARKING)) {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.PARKING.ordinal(),
                            EditAdWantedAmenityEnum.PARKING.toString(),
                            jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_PARKING).equals(DraftAdHttpMap.VALUE_YES_SHORT)
                    );
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_PARKING);
                } else {
                    item = new PreferenceItem(
                            EditAdWantedAmenityEnum.PARKING.ordinal(),
                            EditAdWantedAmenityEnum.PARKING.toString(),
                            false
                    );
                }
                amenitiesList.add(item);

                draftWanted.setAmenitiesList(amenitiesList);

                if (jsonAdvert.has(DraftAdHttpMap.KEY_WANTED_SMOKING)) {
                    draftWanted.set_isSmoker(jsonAdvert.getString(DraftAdHttpMap.KEY_WANTED_SMOKING).equals(DraftAdHttpMap.VALUE_YES_SHORT));
                    jsonAdvert.remove(DraftAdHttpMap.KEY_WANTED_SMOKING);
                }

                // Everything else that cannot be populated into the object is saved in raw JSON
                draftWanted.setJsonExtras(jsonAdvert.toString());

                return draftWanted;

            }

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

        throw new InvalidJSONFormatException();
    } //end parse(String json)

}
