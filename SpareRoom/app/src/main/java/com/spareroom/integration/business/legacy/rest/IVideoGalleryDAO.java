package com.spareroom.integration.business.legacy.rest;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.VideoGallery;

/**
 * Interface for the Video Gallery DAO.
 * <p>
 * Created by miguel.rossi on 25/08/2016.
 */
public interface IVideoGalleryDAO {

    VideoGallery read(Parameters parameters) throws NetworkConnectivityException,
            InvalidJSONFormatException, AuthenticationException, ClientErrorException,
            ServerErrorException;

}
