package com.spareroom.integration.dependency.component;

import com.spareroom.App;
import com.spareroom.integration.dependency.module.*;
import com.spareroom.integration.webservice.rest.service.RestService;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted;
import com.spareroom.model.legacy.SearchFacade;
import com.spareroom.platform.push.PushNotificationsMessagingService;
import com.spareroom.ui.adapter.legacy.*;
import com.spareroom.ui.filters.provider.AbstractItemProvider;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class, DaoModule.class, ActivityModule.class, ViewModelModule.class})
public interface AppComponent {

    void inject(ManagePhotosAdapter managePhotosAdapter);

    void inject(PushNotificationsMessagingService pushNotificationsMessagingService);

    void inject(SavedAdWantedListAdapter savedAdWantedListAdapter);

    void inject(SavedAdOfferedListAdapter savedAdOfferedListAdapter);

    void inject(AdListAdapter adListAdapter);

    void inject(ManagePhotoProfileAdapter managePhotoProfileAdapter);

    void inject(RestService restService);

    void inject(App app);

    void inject(SearchFacade searchFacade);

    void inject(SearchAdvertListPropertiesWanted searchAdvertListPropertiesWanted);

    void inject(SearchAdvertListPropertiesOffered searchAdvertListPropertiesOffered);

    void inject(AbstractItemProvider abstractItemProvider);
}
