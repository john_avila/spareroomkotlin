package com.spareroom.integration.imageloader;

import android.content.Context;
import android.graphics.Bitmap;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

class CropCircleTransformation extends BitmapTransformation {

    CropCircleTransformation(Context context) {
        super(context);
    }

    @Override
    protected Bitmap transform(BitmapPool pool, Bitmap bitmap, int outWidth, int outHeight) {
        return TransformationUtils.circleCrop(bitmap, outWidth, outHeight);
    }

    @Override
    public String getId() {
        return CropCircleTransformation.class.getName() + "Id";
    }

}