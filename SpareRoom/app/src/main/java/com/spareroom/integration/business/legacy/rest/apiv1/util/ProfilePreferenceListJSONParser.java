package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.ProfilePreference;
import com.spareroom.model.business.ProfilePreferenceList;
import com.spareroom.model.business.SpareroomStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProfilePreferenceListJSONParser {

    public Object parse(String json) throws InvalidJSONFormatException {
        try {
            JSONObject firstNode = new JSONObject(json);

            if (firstNode.has("response_type") && firstNode.getString("response_type").equals("error")) {
                SpareroomStatus im = new SpareroomStatus();
                if (firstNode.has("response_id")) {
                    im.setCode(firstNode.getString("response_id"));
                }
                if (firstNode.has("response")) {
                    im.setMessage(firstNode.getString("response"));
                }
                return im;

            } else {
                if (firstNode.has("email_preferences")) {
                    JSONArray vJsonEmail = firstNode.getJSONArray("email_preferences");
                    ProfilePreferenceList ppl = new ProfilePreferenceList();
                    for (int i = 0; i < vJsonEmail.length(); i++) {
                        ProfilePreference pp = new ProfilePreference();
                        JSONObject jsonEmail = vJsonEmail.getJSONObject(i);
                        pp.set_description(jsonEmail.getString("desc"));
                        if (jsonEmail.has("permission"))
                            pp.set_permission(jsonEmail.getString("permission"));
                        pp.set_type(jsonEmail.getString("type"));
                        if (jsonEmail.has("data_type")) {
                            pp.set_dataType(jsonEmail.getString("data_type"));
                            if (jsonEmail.has("options")) {
                                JSONArray jsonAvailableValues = jsonEmail.getJSONArray("options");
                                for (int j = 0; j < jsonAvailableValues.length(); j++) {
                                    JSONObject jsonOption = jsonAvailableValues.getJSONObject(j);
                                    pp.addAvailableOption(
                                            jsonOption.getString("value"),
                                            jsonOption.getString("desc"),
                                            jsonOption.getString("data_value")
                                    );
                                }
                            }
                        }
                        ppl.add(pp);
                    }
                    return ppl;
                }
            }
            return null;

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }
    }

}
