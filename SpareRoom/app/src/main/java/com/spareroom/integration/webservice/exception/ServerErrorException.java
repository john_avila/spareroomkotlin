package com.spareroom.integration.webservice.exception;

public class ServerErrorException extends Exception {

    private static final long serialVersionUID = 4097326059323603163L;

    public ServerErrorException() {
        super();
    }

    public ServerErrorException(String message) {
        super(message);
    }

    public ServerErrorException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ServerErrorException(Throwable throwable) {
        super(throwable);
    }

}
