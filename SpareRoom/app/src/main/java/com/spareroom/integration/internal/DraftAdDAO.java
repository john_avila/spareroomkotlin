package com.spareroom.integration.internal;

import android.content.Context;

import com.spareroom.model.business.DraftAd;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class DraftAdDAO {
    public static final String FILENAME = "temp";

    public static void save(Context context, DraftAd draft) throws IOException {
        FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(draft);
        os.close();
        fos.close();
    }

    public static DraftAd load(Context context) throws IOException, ClassNotFoundException {
        FileInputStream fis = context.openFileInput(FILENAME);
        ObjectInputStream is = new ObjectInputStream(fis);
        DraftAd draft;
        try {
            draft = (DraftAd) is.readObject();
        } catch (Exception e) {
            throw new IOException();
        }
        is.close();
        fis.close();

        return draft;
    }

    public void delete(Context context) {
        context.deleteFile(FILENAME);
    }
}
