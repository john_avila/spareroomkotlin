package com.spareroom.integration.business;

import com.spareroom.model.business.*;
import com.spareroom.model.extra.*;

/**
 * Abstract Factory that provides a family of data accessors for SpareRoom's business objects
 */
public interface BusinessAbstractFactory {
    SpareroomAbstractDao<Void, Void, Void> getSessionDao();

    SpareroomAbstractDao<AccountCredentials, LoginExtra, AccountConnectionInterface> getLoginTransactionDao();

    SpareroomAbstractDao<AccountRegisterForm, RegisterExtra, AccountConnectionInterface> getRegisterTransactionDao();

    SpareroomAbstractDao<AccountRegisterFormFacebook, RegisterExtra, AccountConnectionInterface> getRegisterFacebookTransactionDao();

    SpareroomAbstractDao<AccountCredentials, ResetPasswordExtra, AccountConnectionInterface> getResetPasswordTransactionDao();

    SpareroomAbstractDao<Void, Void, Void> getLogOutTransactionDao();

    SpareroomAbstractDao<String, Void, Void> getFirebaseIdTransactionDao();

    SpareroomAbstractDao<RegisterParameters, Void, Void> getRegisterRunDao();

    SpareroomAbstractDao<NewPicture, Void, Void> getPictureDao();

}
