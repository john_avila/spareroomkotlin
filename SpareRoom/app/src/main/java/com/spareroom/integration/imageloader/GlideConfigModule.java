package com.spareroom.integration.imageloader;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.module.GlideModule;

public class GlideConfigModule implements GlideModule {

    private static final int MAX_IMAGES_DISK_CACHE_SIZE_IN_BYTES = 50 * 1024 * 1024; // 50MB

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        builder.setDiskCache(new InternalCacheDiskCacheFactory(context, MAX_IMAGES_DISK_CACHE_SIZE_IN_BYTES));
        builder.setMemoryCache(new LruResourceCache(0));
        builder.setBitmapPool(new LruBitmapPool(0));
    }

    @Override
    public void registerComponents(Context context, Glide glide) {

    }
}
