package com.spareroom.integration.analytics.googleanalytics;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.spareroom.integration.analytics.AnalyticsTags;
import com.spareroom.integration.analytics.AnalyticsTracker;
import com.spareroom.integration.analytics.googleanalytics.AnalyticsTagsGoogleAnalytics.Action;
import com.spareroom.integration.analytics.legacy.GoogleAnalyticsTracker;
import com.spareroom.model.business.*;

import androidx.fragment.app.Fragment;

public class AnalyticsTrackerGoogleAnalytics extends AnalyticsTracker {
    private final Context __applicationContext;

    public AnalyticsTrackerGoogleAnalytics(Context c) {
        __applicationContext = c;
    }

    @Override
    public void trackScreen_Activity(Activity activity) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.setScreenName(activity.getClass().getSimpleName());
        t.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void trackScreen_Fragment(Fragment fragment) {
        Activity activity = fragment.getActivity();
        String activityName = activity != null ? activity.getClass().getSimpleName() + "/" : "";

        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.setScreenName(activityName + fragment.getClass().getSimpleName());
        t.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void trackEvent_AdvertOffered_MessageAdvertiser() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.MESSAGE_ADVERTISER)
                .build());
    }

    @Override
    public void trackEvent_AdvertWanted_MessageAdvertiser() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.MESSAGE_ADVERTISER)
                .build());
    }

    @Override
    public void trackEvent_AdvertOffered_PhoneAdvertiser() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.PHONE_ADVERTISER)
                .build());
    }

    @Override
    public void trackEvent_AdvertWanted_PhoneAdvertiser() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.PHONE_ADVERTISER)
                .build());
    }

    @Override
    public void trackEvent_Menu_Search(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.MENU_SEARCH)
                .build());
    }

    @Override
    public void trackEvent_Menu_Messages(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.MENU_MESSAGES)
                .build());
    }

    @Override
    public void trackEvent_Menu_MyAdverts(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SELECT_MY_ADVERTS)
                .build());
    }

    @Override
    public void trackEvent_Menu_SavedAds(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.MENU_SAVED_ADS)
                .build());
    }

    @Override
    public void trackEvent_Menu_SavedSearches(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.MENU_SAVEDSEARCHES)
                .build());
    }

    @Override
    public void trackEvent_Menu_EditMyDetails(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.MENU_EDIT_MY_DETAILS)
                .build());
    }

    @Override
    public void trackEvent_Menu_Upgrade(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.MENU_UPGRADE)
                .build());
    }

    @Override
    public void trackEvent_Menu_PlaceAdvert(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.MENU_PLACE_ADVERT)
                .build());
    }

    @Override
    public void trackEvent_Menu_BetaProgramme() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.MENU_BETA_PROGRAMME)
                .build());
    }

    @Override
    public void trackEvent_Menu_ContactUsPhone(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.MENU_CONTACT_US_PHONE)
                .build());
    }

    @Override
    public void trackEvent_Menu_ContactUsMail(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.MENU_CONTACT_US_MAIL)
                .build());
    }

    @Override
    public void trackEvent_Menu_LogOut(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.MENU_LOGOUT)
                .build());
    }

    @Override
    public void trackEvent_Push_TooManyRequests(Long runs) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.PUSH)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.PUSH_TOO_MANY_REQUESTS)
                .setValue(runs)
                .build());
    }

    @Override
    public void trackEvent_SavedSearch_LaunchSavedSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SAVED_SEARCHES)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.LAUNCH_SAVED_SEARCH)
                .build());
    }

    @Override
    public void trackEvent_SavedSearch_DeleteSavedSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SAVED_SEARCHES)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.DELETE_SAVED_SEARCH)
                .build());
    }

    @Override
    public void trackEvent_Search_BroadenSearch(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        HitBuilders.EventBuilder eb =
                new HitBuilders.EventBuilder()
                        .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH)
                        .setAction(AnalyticsTagsGoogleAnalytics.Action.BROADEN_SEARCH);
        if (!isLoggedIn)
            eb.setLabel(AnalyticsTagsGoogleAnalytics.Label.LOGIN_OPENED);
        t.send(eb.build());
    }

    @Override
    public void trackEvent_Search_MarkFavouriteOffered(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        HitBuilders.EventBuilder eb =
                new HitBuilders.EventBuilder()
                        .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH)
                        .setAction(AnalyticsTagsGoogleAnalytics.Action.MARK_FAVOURITE);
        if (!isLoggedIn)
            eb.setLabel(AnalyticsTagsGoogleAnalytics.Label.LOGIN_OPENED);
        t.send(eb.build());
    }

    @Override
    public void trackEvent_Search_MarkFavouriteWanted(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        HitBuilders.EventBuilder eb =
                new HitBuilders.EventBuilder()
                        .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH)
                        .setAction(AnalyticsTagsGoogleAnalytics.Action.MARK_FAVOURITE);
        if (!isLoggedIn)
            eb.setLabel(AnalyticsTagsGoogleAnalytics.Label.LOGIN_OPENED);
        t.send(eb.build());
    }

    @Override
    public void trackEvent_Search_RemoveShortlistOffered(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        HitBuilders.EventBuilder eb =
                new HitBuilders.EventBuilder()
                        .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH)
                        .setAction(AnalyticsTagsGoogleAnalytics.Action.REMOVE_SHORTLIST);
        if (!isLoggedIn)
            eb.setLabel(AnalyticsTagsGoogleAnalytics.Label.LOGIN_OPENED);
        t.send(eb.build());
    }

    @Override
    public void trackEvent_Search_RemoveShortlistWanted(boolean isLoggedIn) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext)
                .getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        HitBuilders.EventBuilder eb =
                new HitBuilders.EventBuilder()
                        .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH)
                        .setAction(AnalyticsTagsGoogleAnalytics.Action.REMOVE_SHORTLIST);
        if (!isLoggedIn)
            eb.setLabel(AnalyticsTagsGoogleAnalytics.Label.LOGIN_OPENED);
        t.send(eb.build());
    }

    @Override
    public void trackEvent_SearchRooms_LaunchSavedSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.LAUNCH_SAVED_SEARCH)
                .build());
    }

    @Override
    public void trackEvent_SearchRooms_DeleteSavedSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.DELETE_SAVED_SEARCH)
                .build());
    }

    @Override
    public void trackEvent_SearchRooms_ManualSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_ROOMS_MANUALLY)
                .build());
    }

    @Override
    public void trackEvent_SearchRooms_AutocompleteSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_ROOMS_USING_AUTOCOMPLETE)
                .build());
    }

    @Override
    public void trackEvent_SearchRooms_HistorySearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_ROOMS_USING_HISTORY)
                .build());
    }

    @Override
    public void trackEvent_SearchRooms_ManualAdvertSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_ADVERT_MANUALLY)
                .build());
    }

    @Override
    public void trackEvent_SearchRooms_HistoryAdvertSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_ADVERT_USING_HISTORY)
                .build());
    }

    @Override
    public void trackEvent_SearchRooms_AutocompleteAdvertSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_ADVERT_USING_AUTOCOMPLETE)
                .build());
    }

    @Override
    public void trackEvent_SearchFlatmates_ManualSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_FLATMATES)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_FLATMATES_MANUALLY)
                .build());
    }

    @Override
    public void trackEvent_SearchFlatmates_AutocompleteSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_FLATMATES)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_FLATMATES_USING_AUTOCOMPLETE)
                .build());
    }

    @Override
    public void trackEvent_SearchFlatmates_HistorySearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_FLATMATES)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_FLATMATES_USING_HISTORY)
                .build());
    }

    @Override
    public void trackEvent_SearchFlatmates_ManualAdvertSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_FLATMATES)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_ADVERT_MANUALLY)
                .build());
    }

    @Override
    public void trackEvent_SearchFlatmates_HistoryAdvertSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_FLATMATES)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_ADVERT_USING_HISTORY)
                .build());
    }

    @Override
    public void trackEvent_SearchFlatmates_AutocompleteAdvertSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_FLATMATES)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_ADVERT_USING_AUTOCOMPLETE)
                .build());
    }

    @Override
    public void trackEvent_Results_SuggestionAdvertSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.RESULTS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.RESULTS_SUGGESTION)
                .build());
    }

    @Override
    public void trackEvent_Results_CancelSuggestionAdvertSearch() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.RESULTS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.RESULTS_CANCEL_SUGGESTION)
                .build());
    }

    @Override
    public void trackEvent_SearchRooms_GetLocation() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.GET_MY_LOCATION)
                .build());
    }

    @Override
    public void trackEvent_SearchFlatmates_GetLocation() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_FLATMATES)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.GET_MY_LOCATION)
                .build());
    }

    @Override
    public void trackEvent_PlaceAd_PlaceOfferedAd() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.PLACE_ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.PLACE_OFFERED_AD)
                .build());
    }

    @Override
    public void trackEvent_PlaceAd_PlaceWantedAd() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.PLACE_ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.PLACE_WANTED_AD)
                .build());
    }

    @Override
    public void trackEvent_PlaceAd_PlaceOfferedAdWithErrors() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.PLACE_ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.PLACE_OFFERED_AD_ERRORS)
                .build());
    }

    @Override
    public void trackEvent_PlaceAd_PlaceWantedAdWithErrors() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.PLACE_ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.PLACE_WANTED_AD_ERRORS)
                .build());
    }

    @Override
    public void trackEvent_EditAd_ReadOfferedAd() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.EDIT_ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.READ_OFFERED_DRAFT)
                .build());
    }

    @Override
    public void trackEvent_EditAd_ReadWantedAd() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.EDIT_ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.READ_WANTED_DRAFT)
                .build());
    }

    @Override
    public void trackEvent_EditAd_ReadOfferedAdWithErrors(SpareroomStatus status) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.EDIT_ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.READ_OFFERED_DRAFT_ERRORS)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.CODE + status.getCode())
                .build());
    }

    @Override
    public void trackEvent_EditAd_ReadWantedAdWithErrors(SpareroomStatus status) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.EDIT_ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.READ_WANTED_DRAFT_ERRORS)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.CODE + status.getCode())
                .build());
    }

    @Override
    public void trackEvent_EditAd_EditOfferedAd() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.EDIT_ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.EDIT_OFFERED_AD)
                .build());
    }

    @Override
    public void trackEvent_EditAd_EditWantedAd() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.EDIT_ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.EDIT_WANTED_AD)
                .build());
    }

    @Override
    public void trackEvent_EditAd_EditOfferedAdWithErrors(SpareroomStatus status) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.EDIT_ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.EDIT_OFFERED_AD_ERRORS)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.CODE + status.getCode())
                .build());
    }

    @Override
    public void trackEvent_EditAd_EditWantedAdWithErrors(SpareroomStatus status) {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.EDIT_ADVERT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.EDIT_WANTED_AD_ERRORS)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.CODE + status.getCode())
                .build());
    }

    @Override
    public void trackEvent_Upgrade_SelectProduct(Upgrade u, int contextId, int productPosition) {
        Tracker tApp = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        tApp.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_SELECT_PRODUCT)
                .setLabel(u.get_code())
                .build());
    }

    @Override
    public void trackEvent_Upgrade_Purchase(Upgrade u, String orderId, int productPosition) {
        Tracker tApp = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        tApp.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_PURCHASE)
                .setLabel(u.get_code())
                .build());
    }

    @Override
    public void trackEvent_ExtraListing_Purchase(Upgrade u, String orderId) {
        Tracker tApp = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        tApp.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_EXTRA_LISTING)
                .setLabel(u.get_code())
                .build());
    }

    @Override
    public void trackEvent_SearchRoomsMenu_Map() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS_MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_MAP)
                .build());
    }

    @Override
    public void trackEvent_SearchRoomsMenu_Refine() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS_MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_REFINE)
                .build());
    }

    @Override
    public void trackEvent_SearchFlatmatesMenu_Refine() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_FLATMATES_MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_REFINE)
                .build());
    }

    @Override
    public void trackEvent_SearchRoomsMenu_SortOption() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS_MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_SORT_BY)
                .build());
    }

    @Override
    public void trackEvent_SearchRooms_SortByDefault() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_SORT_BY_DEFAULT)
                .build());
    }

    @Override
    public void trackEvent_SearchRooms_SortByLastUpdated() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_SORT_BY_LAST_UPDATED)
                .build());
    }

    @Override
    public void trackEvent_SearchRooms_SortByNewest() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_SORT_BY_NEWEST)
                .build());
    }

    @Override
    public void trackEvent_SearchRooms_SortByMostExpensive() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_SORT_BY_MOST_EXPENSIVE)
                .build());
    }

    @Override
    public void trackEvent_SearchRooms_SortByLeastExpensive() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_ROOMS)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_SORT_BY_LEAST_EXPENSIVE)
                .build());
    }

    @Override
    public void trackEvent_SearchFlatmatesMenu_SortOption() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_FLATMATES_MENU)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_SORT_BY)
                .build());
    }

    @Override
    public void trackEvent_SearchFlatmates_SortByDefault() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_FLATMATES)
                .setAction(Action.SEARCH_SORT_BY_DEFAULT)
                .build());
    }

    @Override
    public void trackEvent_SearchFlatmates_SortByLastUpdated() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_FLATMATES)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_SORT_BY_LAST_UPDATED)
                .build());
    }

    @Override
    public void trackEvent_SearchFlatmates_SortByNewest() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_FLATMATES)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_SORT_BY_NEWEST)
                .build());
    }

    @Override
    public void trackEvent_SearchFlatmates_SortByMostExpensive() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_FLATMATES)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_SORT_BY_MOST_EXPENSIVE)
                .build());
    }

    @Override
    public void trackEvent_SearchFlatmates_SortByLeastExpensive() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.SEARCH_FLATMATES)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.SEARCH_SORT_BY_LEAST_EXPENSIVE)
                .build());
    }

    @Override
    public void trackEcommerce_Upgrade_SelectProduct(Upgrade u, int contextId, int productPosition) {
        Tracker tracker =
                GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.ECOMMERCE_TRACKER);

        Product product =
                new Product()
                        .setId(u.get_code())
                        .setPrice((double) u.get_priceInMicros())
                        .setName(u.get_title())
                        .setCategory(
                                u.get_upgradeType() == Upgrade.BOLD_ADVERTISING ?
                                        AnalyticsTagsGoogleAnalytics.Ecommerce.CATEGORY_BOLD_ADVERTISING :
                                        AnalyticsTagsGoogleAnalytics.Ecommerce.CATEGORY_EARLY_BIRD)
                        .setBrand(AnalyticsTagsGoogleAnalytics.BRAND)
                        .setPosition(productPosition);

        ProductAction selectProduct =
                new ProductAction(ProductAction.ACTION_CLICK)
                        .setTransactionRevenue((double) u.get_priceInMicros())
                        .setProductActionList(UpgradeContext.getName(contextId));

        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder().addProduct(product).setProductAction(selectProduct);

        tracker.send(builder.build());

    }

    @Override
    public void trackEcommerce_Upgrade_Purchase(Upgrade u, String orderId, int productPosition) {
        Tracker tracker =
                GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.ECOMMERCE_TRACKER);

        Product product =
                new Product()
                        .setId(u.get_code())
                        .setName(u.get_title())
                        .setPrice((double) u.get_priceInMicros())
                        .setCategory(
                                u.get_upgradeType() == Upgrade.BOLD_ADVERTISING
                                        ? AnalyticsTagsGoogleAnalytics.Ecommerce.CATEGORY_BOLD_ADVERTISING
                                        : AnalyticsTagsGoogleAnalytics.Ecommerce.CATEGORY_EARLY_BIRD)
                        .setBrand(AnalyticsTagsGoogleAnalytics.BRAND)
                        .setPosition(productPosition)
                        .setQuantity(1);

        ProductAction productAction =
                new ProductAction(ProductAction.ACTION_PURCHASE)
                        .setTransactionId(orderId)
                        .setTransactionRevenue((double) u.get_priceInMicros())
                        .setTransactionAffiliation(AnalyticsTagsGoogleAnalytics.AFFILIATION);

        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder().addProduct(product).setProductAction(productAction);

        if (u.get_priceInMicros() != null)
            product.setPrice(u.get_priceInMicros() / Math.pow(10, 6));

        tracker.set("&cu", u.get_currency());
        tracker.send(builder.build());

    }

    @Override
    public void trackEcommerce_ExtraListing_Purchase(Upgrade u, String orderId) {
        Tracker tracker =
                GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.ECOMMERCE_TRACKER);

        Product product =
                new Product()
                        .setId(u.get_code())
                        .setName(u.get_title())
                        .setPrice((double) u.get_priceInMicros())
                        .setCategory(
                                (u.get_upgradeType() == Upgrade.BOLD_ADVERTISING)
                                        ? AnalyticsTagsGoogleAnalytics.Ecommerce.CATEGORY_BOLD_ADVERTISING
                                        : AnalyticsTagsGoogleAnalytics.Ecommerce.CATEGORY_EARLY_BIRD)
                        .setBrand(AnalyticsTagsGoogleAnalytics.BRAND)
                        .setQuantity(1);

        ProductAction productAction =
                new ProductAction(ProductAction.ACTION_PURCHASE)
                        .setTransactionId(orderId)
                        .setTransactionRevenue((double) u.get_priceInMicros())
                        .setTransactionAffiliation(AnalyticsTagsGoogleAnalytics.AFFILIATION);

        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder().addProduct(product).setProductAction(productAction);

        if (u.get_priceInMicros() != null)
            product.setPrice(u.get_priceInMicros() / Math.pow(10, 6));

        tracker.set("&cu", u.get_currency());
        tracker.send(builder.build());
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailable_Create() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_CREATE)
                .build());
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailable_DisableDebug() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_DISABLE_DEBUG)
                .build());
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailable_Setup() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_SETUP)
                .build());
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailable_QueryInventory() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_QUERY_INVENTORY)
                .build());
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailable_LaunchPurchaseFlow() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_LAUNCH_PURCHASE)
                .build());
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailable_HandlePaymentResult() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_HANDLE_PAYMENT_RESULT)
                .build());
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailable_Consume() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_CONSUME)
                .build());
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_Create() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_EXTRA_LISTING)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_CREATE)
                .build());
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_DisableDebug() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_EXTRA_LISTING)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_DISABLE_DEBUG)
                .build());
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_Setup() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_EXTRA_LISTING)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_SETUP)
                .build());
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_QueryInventory() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_EXTRA_LISTING)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_QUERY_INVENTORY)
                .build());
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_LaunchPurchaseFlow() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_EXTRA_LISTING)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_LAUNCH_PURCHASE)
                .build());
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_HandlePaymentResult() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_EXTRA_LISTING)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_HANDLE_PAYMENT_RESULT)
                .build());
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_Consume() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.UPGRADE)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_EXTRA_LISTING)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.UPGRADE_BILLING_UNEXPECTEDLY_UNAVAILABLE_CONSUME)
                .build());
    }

    @Override
    public void trackEvent_Account_Register_Facebook(String userType) {
        String userTypeParam = null;

        switch (userType) {
            case "OFFERER_AND_SEEKER":
                userTypeParam = AnalyticsTagsGoogleAnalytics.Label.ACCOUNT_REGISTER_USER_TYPE_SEEKER_OFFERER;
                break;
            case "OFFERER":
                userTypeParam = AnalyticsTagsGoogleAnalytics.Label.ACCOUNT_REGISTER_USER_TYPE_OFFERER;
                break;
            case "SEEKER":
                userTypeParam = AnalyticsTagsGoogleAnalytics.Label.ACCOUNT_REGISTER_USER_TYPE_SEEKER;
                break;
        }

        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ACCOUNT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.ACCOUNT_REGISTER_FACEBOOK)
                .setLabel(userTypeParam)
                .build());
    }

    @Override
    public void trackEvent_Account_Register_Spareroom(String userType) {
        String userTypeParam = null;

        switch (userType) {
            case "OFFERER_AND_SEEKER":
                userTypeParam = AnalyticsTagsGoogleAnalytics.Label.ACCOUNT_REGISTER_USER_TYPE_SEEKER_OFFERER;
                break;
            case "OFFERER":
                userTypeParam = AnalyticsTagsGoogleAnalytics.Label.ACCOUNT_REGISTER_USER_TYPE_OFFERER;
                break;
            case "SEEKER":
                userTypeParam = AnalyticsTagsGoogleAnalytics.Label.ACCOUNT_REGISTER_USER_TYPE_SEEKER;
                break;
        }

        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ACCOUNT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.ACCOUNT_REGISTER_SPAREROOM)
                .setLabel(userTypeParam)
                .build());
    }

    @Override
    public void trackEvent_Account_FacebookLoginToRegisterRedirection() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ACCOUNT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.ACCOUNT_FACEBOOK_REDIRECTION_LOGIN_TO_REGISTER)
                .build());
    }

    @Override
    public void trackEvent_Account_FacebookRegisterToLoginRedirection() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ACCOUNT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.ACCOUNT_FACEBOOK_REDIRECTION_REGISTER_TO_LOGIN)
                .build());
    }

    @Override
    public void trackEvent_Account_Register_Facebook_Fail(SpareroomStatus failedRegistration) {
        String errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_UNDEFINED_ERROR;

        if (failedRegistration != null && failedRegistration.getCode() != null && !failedRegistration.getCode().isEmpty()) {

            switch (failedRegistration.getCode()) {
                case "652":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_INVALID_EMAIL;
                    break;
                case "655":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_INVALID_EMAIL_LENGTH;
                    break;
                case "654":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_CONFIRM_PASSWORD_FAILED;
                    break;
                case "657":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_AGREE_TERMS_N_CONDITIONS;
                    break;
                case "650":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_USED_EMAIL;
                    break;
                case "658":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_MISSED_FIRST_NAME;
                    break;
                case "659":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_MISSED_LAST_NAME;
                    break;
                case "661":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_MISSED_USER_TYPE;
                    break;
                default:
                    errorLabel = String.format(
                            "%s_%s",
                            AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_OTHER_ERROR,
                            failedRegistration.getCode());

            }

        }

        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ACCOUNT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.ACCOUNT_REGISTER_FAILED)
                .setLabel(errorLabel)
                .build());

    }

    @Override
    public void trackEvent_Account_Register_Spareroom_Fail(SpareroomStatus failedRegistration) {
        String errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_UNDEFINED_ERROR;

        if (failedRegistration != null && failedRegistration.getCode() != null && !failedRegistration.getCode().isEmpty()) {

            switch (failedRegistration.getCode()) {
                case "652":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_INVALID_EMAIL;
                    break;
                case "655":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_INVALID_EMAIL_LENGTH;
                    break;
                case "654":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_CONFIRM_PASSWORD_FAILED;
                    break;
                case "657":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_AGREE_TERMS_N_CONDITIONS;
                    break;
                case "650":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_USED_EMAIL;
                    break;
                case "658":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_MISSED_FIRST_NAME;
                    break;
                case "659":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_MISSED_LAST_NAME;
                    break;
                case "661":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_ERROR_MISSED_USER_TYPE;
                    break;
                default:
                    errorLabel = String.format(
                            "%s_%s",
                            AnalyticsTagsGoogleAnalytics.Label.REGISTER_FAILED_OTHER_ERROR,
                            failedRegistration.getCode());

            }

        }

        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ACCOUNT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.ACCOUNT_REGISTER_FAILED)
                .setLabel(errorLabel)
                .build());

    }

    @Override
    public void trackEvent_Account_Login_Facebook() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ACCOUNT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.ACCOUNT_LOGIN)
                .setLabel(AnalyticsTagsGoogleAnalytics.Label.ACCOUNT_LOGIN_METHOD_FACEBOOK)
                .build());
    }

    @Override
    public void trackEvent_Account_Login_Spareroom() {
        Tracker t =
                GoogleAnalyticsTracker
                        .getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(
                new HitBuilders
                        .EventBuilder()
                        .setCategory(AnalyticsTagsGoogleAnalytics.Category.ACCOUNT)
                        .setAction(AnalyticsTagsGoogleAnalytics.Action.ACCOUNT_LOGIN)
                        .setLabel(AnalyticsTagsGoogleAnalytics.Label.ACCOUNT_LOGIN_METHOD_SPAREROOM)
                        .build()
        );
    }

    @Override
    public void trackEvent_Account_Login_Facebook_Failed(SpareroomStatus failedRegistration) {
        String errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_UNDEFINED_ERROR;

        if (failedRegistration != null
                && failedRegistration.getCode() != null
                && !failedRegistration.getCode().isEmpty()) {

            switch (failedRegistration.getCode()) {
                case "603":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_ERROR_NO_PASSWORD;
                    break;
                case "604":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_ERROR_UNKNOWN_EMAIL;
                    break;
                case "610":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_ERROR_INVALID_EMAIL;
                    break;
                case "605":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_ERROR_WRONG_PASSWORD;
                    break;
                case "601":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_ERROR_MISSED_EMAIL_AND_PASSWORD;
                    break;
                case "602":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_ERROR_MISSED_EMAIL;
                    break;
                case "651":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_ERROR_ALREADY_LOGGED_IN;
                    break;
                default:
                    errorLabel = String.format(
                            "%s_%s",
                            AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_OTHER_ERROR,
                            failedRegistration.getCode());
            }

        }

        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);

        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ACCOUNT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.ACCOUNT_LOGIN_FAILED)
                .setLabel(errorLabel)
                .build());
    }

    @Override
    public void trackEvent_Account_Login_Spareroom_Failed(SpareroomStatus failedRegistration) {
        String errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_UNDEFINED_ERROR;

        if (failedRegistration != null && failedRegistration.getCode() != null && !failedRegistration.getCode().isEmpty()) {

            switch (failedRegistration.getCode()) {
                case "603":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_ERROR_NO_PASSWORD;
                    break;
                case "604":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_ERROR_UNKNOWN_EMAIL;
                    break;
                case "610":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_ERROR_INVALID_EMAIL;
                    break;
                case "605":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_ERROR_WRONG_PASSWORD;
                    break;
                case "601":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_ERROR_MISSED_EMAIL_AND_PASSWORD;
                    break;
                case "602":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_ERROR_MISSED_EMAIL;
                    break;
                case "651":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_ERROR_ALREADY_LOGGED_IN;
                    break;
                default:
                    errorLabel = String.format(
                            "%s_%s",
                            AnalyticsTagsGoogleAnalytics.Label.LOGIN_FAILED_OTHER_ERROR,
                            failedRegistration.getCode());
            }

        }

        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ACCOUNT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.ACCOUNT_LOGIN_FAILED)
                .setLabel(errorLabel)
                .build());

    }

    @Override
    public void trackEvent_Account_Logout() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ACCOUNT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.ACCOUNT_LOGOUT)
                .build());
    }

    @Override
    public void trackEvent_Account_ForgotPassword() {
        Tracker t =
                GoogleAnalyticsTracker
                        .getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(
                new HitBuilders
                        .EventBuilder()
                        .setCategory(AnalyticsTagsGoogleAnalytics.Category.ACCOUNT)
                        .setAction(AnalyticsTagsGoogleAnalytics.Action.ACCOUNT_FORGOT_PASSWORD)
                        .build()
        );
    }

    @Override
    public void trackEvent_Account_ForgotPassword_Failed(SpareroomStatus failedRecover) {
        String errorLabel = AnalyticsTagsGoogleAnalytics.Label.VALUE_RECOVER_PASSWORD_UNDEFINED_ERROR;

        if (failedRecover != null
                && failedRecover.getCode() != null
                && !failedRecover.getCode().isEmpty()) {

            switch (failedRecover.getCode()) {
                case "1102":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.VALUE_RECOVER_PASSWORD_FAILED_THREE_REMINDERS;
                    break;
                case "1103":
                    errorLabel = AnalyticsTagsGoogleAnalytics.Label.VALUE_RECOVER_PASSWORD_FAILED_EMAIL;
                    break;
                default:
                    errorLabel = String.format(
                            "%s_%s",
                            AnalyticsTagsGoogleAnalytics.Label.VALUE_RECOVER_PASSWORD_OTHER_ERROR,
                            failedRecover.getCode());

            }

        }

        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ACCOUNT)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.ACCOUNT_FORGOT_PASSWORD_FAILED)
                .setLabel(errorLabel)
                .build());

    }

    @Override
    public void trackEvent_AdvertVideo_UploadOfferedCamera() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ADVERT_VIDEO)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.VIDEO_UPLOAD_OFFERED_CAMERA)
                .build());
    }

    @Override
    public void trackEvent_AdvertVideo_UploadOfferedGallery() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ADVERT_VIDEO)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.VIDEO_UPLOAD_OFFERED_GALLERY)
                .build());
    }

    @Override
    public void trackEvent_AdvertVideo_UploadWantedCamera() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ADVERT_VIDEO)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.VIDEO_UPLOAD_WANTED_CAMERA)
                .build());

    }

    @Override
    public void trackEvent_AdvertVideo_UploadWantedGallery() {
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);
        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ADVERT_VIDEO)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.VIDEO_UPLOAD_WANTED_GALLERY)
                .build());
    }

    @Override
    public void trackEvent_AdvertVideo_ErrorUploadingOfferedCamera(int cause) {
        String label;
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);

        switch (cause) {
            case AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_BIG:
                label = AnalyticsTagsGoogleAnalytics.Label.VIDEO_UPLOAD_ERROR_TOO_BIG;
                break;
            case AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_LONG:
                label = AnalyticsTagsGoogleAnalytics.Label.VIDEO_UPLOAD_ERROR_TOO_LONG;
                break;
            default:
                label = AnalyticsTagsGoogleAnalytics.Label.VIDEO_UPLOAD_ERROR_UNKNOWN;
                break;
        }

        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ADVERT_VIDEO)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.VIDEO_UPLOAD_ERROR_UPLOADING_OFFERED_CAMERA)
                .setLabel(label)
                .build());
    }

    @Override
    public void trackEvent_AdvertVideo_ErrorUploadingOfferedGallery(int cause) {
        String label;
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);

        switch (cause) {
            case AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_BIG:
                label = AnalyticsTagsGoogleAnalytics.Label.VIDEO_UPLOAD_ERROR_TOO_BIG;
                break;
            case AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_LONG:
                label = AnalyticsTagsGoogleAnalytics.Label.VIDEO_UPLOAD_ERROR_TOO_LONG;
                break;
            default:
                label = AnalyticsTagsGoogleAnalytics.Label.VIDEO_UPLOAD_ERROR_UNKNOWN;
                break;
        }

        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ADVERT_VIDEO)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.VIDEO_UPLOAD_ERROR_UPLOADING_OFFERED_GALLERY)
                .setLabel(label)
                .build());
    }

    @Override
    public void trackEvent_AdvertVideo_ErrorUploadingWantedCamera(int cause) {
        String label;
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);

        switch (cause) {
            case AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_BIG:
                label = AnalyticsTagsGoogleAnalytics.Label.VIDEO_UPLOAD_ERROR_TOO_BIG;
                break;
            case AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_LONG:
                label = AnalyticsTagsGoogleAnalytics.Label.VIDEO_UPLOAD_ERROR_TOO_LONG;
                break;
            default:
                label = AnalyticsTagsGoogleAnalytics.Label.VIDEO_UPLOAD_ERROR_UNKNOWN;
                break;
        }

        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ADVERT_VIDEO)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.VIDEO_UPLOAD_ERROR_UPLOADING_WANTED_CAMERA)
                .setLabel(label)
                .build());
    }

    @Override
    public void trackEvent_AdvertVideo_ErrorUploadingWantedGallery(int cause) {
        String label;
        Tracker t = GoogleAnalyticsTracker.getInstance(__applicationContext).getTracker(GoogleAnalyticsTracker.TrackerName.APP_TRACKER);

        switch (cause) {
            case AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_BIG:
                label = AnalyticsTagsGoogleAnalytics.Label.VIDEO_UPLOAD_ERROR_TOO_BIG;
                break;
            case AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_LONG:
                label = AnalyticsTagsGoogleAnalytics.Label.VIDEO_UPLOAD_ERROR_TOO_LONG;
                break;
            default:
                label = AnalyticsTagsGoogleAnalytics.Label.VIDEO_UPLOAD_ERROR_UNKNOWN;
                break;
        }

        t.send(new HitBuilders
                .EventBuilder()
                .setCategory(AnalyticsTagsGoogleAnalytics.Category.ADVERT_VIDEO)
                .setAction(AnalyticsTagsGoogleAnalytics.Action.VIDEO_UPLOAD_ERROR_UPLOADING_WANTED_GALLERY)
                .setLabel(label)
                .build());
    }
}
