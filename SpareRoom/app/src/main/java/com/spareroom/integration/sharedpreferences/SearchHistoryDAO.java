package com.spareroom.integration.sharedpreferences;

import android.content.Context;

import com.spareroom.model.legacy.SearchHistory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SearchHistoryDAO {
    private static final String FILENAME = "search_history";

    public static void save(Context context, SearchHistory sh) throws IOException {
        FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(sh);
        os.close();
        fos.close();
    }

    public static SearchHistory load(Context context) throws IOException, ClassNotFoundException {
        FileInputStream fis = context.openFileInput(FILENAME);
        ObjectInputStream is = new ObjectInputStream(fis);
        SearchHistory draft = (SearchHistory) is.readObject();
        is.close();
        fis.close();
        return draft;
    }

}
