package com.spareroom.integration.business.apiv2.util;

import com.spareroom.integration.business.legacy.rest.apiv1.util.SessionJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.SpareroomStatus;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterJsonParser {

    /**
     * Parses the response after registering a new user
     *
     * @param json String containing the response
     * @return an User object if the the user could be registered, SpareroomStatus if there were problems in the request
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public Object parse(String json) throws InvalidJSONFormatException {

        if (json == null) // sometimes happens when an unexpected http status code is returned (302, 500)
            throw new InvalidJSONFormatException();

        try {
            JSONObject firstNode = new JSONObject(json);
            String response_type = firstNode.getString("response_type");

            if (response_type.equals("success")) {
                String jsonSession = firstNode.getString("session");
                SessionJSONParser sessionParser = new SessionJSONParser();

                return sessionParser.parse(jsonSession);

            } else if (response_type.equals("error")) {
                SpareroomStatus e = new SpareroomStatus();

                if (firstNode.has("response_id"))
                    e.setCode(firstNode.getString("response_id"));
                else
                    e.setCode("0");

                if (firstNode.has("error_element")) // This field is optional in the response
                    e.setCause(firstNode.getString("error_element"));

                e.setMessage(firstNode.getString("response"));

                return e;

            }

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();

        }

        throw new InvalidJSONFormatException();
    }

}
