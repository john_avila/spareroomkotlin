package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IUpgradeDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.Authenticator;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.UpgradeJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.UpgradePlanRestService;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SpareroomStatus;

public class UpgradeDAO implements IUpgradeDAO {

    private final UpgradePlanRestService upgradePlanRestService;

    UpgradeDAO() {
        upgradePlanRestService = new UpgradePlanRestService();
    }

    @Override
    public SpareroomStatus create(Parameters p) throws AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException {

        Authenticator authenticator = new Authenticator();
        p.add("api_sig", authenticator.authenticate(p));
        p.add("api_key", authenticator.get_apiKey());

        String[] names = p.getParameterNames();
        String[] values = new String[names.length];

        for (int i = 0; i < names.length; i++) {
            names[i] = names[i];
            values[i] = p.get(names[i]);
        }

        String response = upgradePlanRestService.upgradePlan(names, values);

        UpgradeJSONParser parser = new UpgradeJSONParser();

        return parser.parse(response);
    }

}
