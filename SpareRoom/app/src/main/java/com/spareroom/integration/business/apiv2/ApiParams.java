package com.spareroom.integration.business.apiv2;

public class ApiParams {
    public static final String FORMAT = "format";
    public static final String FORMAT_JSON = "json";
    public static final String API_SIGNATURE = "api_sig";
    public static final String API_KEY = "api_key";

    public final static String YES = "Y";
    public final static String YES_INT = "1";

    public static final String OFFSET = "offset";
    public static final String MAX_PER_PAGE = "max_per_page";

}
