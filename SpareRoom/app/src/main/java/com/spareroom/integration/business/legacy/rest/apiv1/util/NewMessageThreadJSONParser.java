package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.SpareroomStatus;

import org.json.JSONException;
import org.json.JSONObject;

public class NewMessageThreadJSONParser {
    /**
     * Parses the response after sending a new message (first message)
     *
     * @param json String containing the response
     * @return a message stating whether the operation was successful or not
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public SpareroomStatus parse(String json) throws InvalidJSONFormatException {
        try {
            JSONObject firstNode = new JSONObject(json);

            if (firstNode.has("success")) {
                if (firstNode.getString("success").equals("1")) {
                    SpareroomStatus m = new SpareroomStatus();
                    m.setCode(firstNode.getString("success"));
                    return m;
                } else {
                    SpareroomStatus m = new SpareroomStatus();
                    if (firstNode.has("response_id"))
                        m.setCode(firstNode.getString("response_id"));
                    if (firstNode.has("response"))
                        m.setCode(firstNode.getString("response"));
                    return m;
                }
            }

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

        throw new InvalidJSONFormatException();
    }
}
