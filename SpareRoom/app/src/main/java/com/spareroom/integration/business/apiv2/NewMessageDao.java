package com.spareroom.integration.business.apiv2;

import com.spareroom.integration.business.apiv2.util.NewMessageJsonParser;
import com.spareroom.integration.business.legacy.rest.apiv1.MessageParamEnum;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.*;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.service.ConversationRestService;
import com.spareroom.model.business.*;

import javax.inject.Inject;

/**
 * DAO for sending a new message
 */
public class NewMessageDao implements INewMessageDao {

    private final ConversationRestService conversationRestService;

    @Inject
    NewMessageDao(ConversationRestService conversationRestService) {
        this.conversationRestService = conversationRestService;
    }

    @Override
    public NewMessageResponse getDeclineMessageText(String messageId, String template)
            throws UnexpectedResultException, UnavailableDataSourceException, InvalidArgumentException {

        return sendMessage(messageId, "", template, null, null);
    }

    @Override
    public NewMessageResponse decline(String messageId, String declineText, String template)
            throws UnexpectedResultException, UnavailableDataSourceException, InvalidArgumentException {

        return sendMessage(messageId, declineText, template, NewMessage.FORM_ACTION_REPLY, null);
    }

    @Override
    public NewMessageResponse sendNewMessage(NewMessage message)
            throws UnexpectedResultException, UnavailableDataSourceException, InvalidArgumentException {

        return sendMessage(message.getMessageId(), message.getMessage(), null, message.getFormAction(), message.getConfirm());
    }

    private NewMessageResponse sendMessage(String messageId, String message, String template, String action, String confirm)
            throws UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {

        Parameters parameters = new Parameters();
        parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON);
        parameters.add(MessageParamEnum.MESSAGE_ID, messageId);
        parameters.add(MessageParamEnum.MESSAGE, message);
        if (action != null)
            parameters.add(MessageParamEnum.FORM_ACTION, action);
        if (template != null)
            parameters.add(MessageParamEnum.E_TEMPLATE, template);
        if (confirm != null)
            parameters.add(MessageParamEnum.CONFIRM, confirm);

        try {
            String response = conversationRestService.sendMessage(parameters);

            NewMessageJsonParser parser = new NewMessageJsonParser();
            return parser.parse(response);

        } catch (NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e);
        } catch (ClientErrorException | AuthenticationException e) {
            throw new InvalidArgumentException(e);
        } catch (ServerErrorException | InvalidJSONFormatException e) {
            throw new UnexpectedResultException(e);
        }

    }
}
