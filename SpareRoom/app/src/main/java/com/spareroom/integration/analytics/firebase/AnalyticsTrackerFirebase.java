package com.spareroom.integration.analytics.firebase;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.spareroom.integration.analytics.AnalyticsTracker;
import com.spareroom.model.business.SpareroomStatus;

/**
 * Tracker for Firebase
 * <p>
 * Created by miguel.rossi on 08/06/2016.
 */
public class AnalyticsTrackerFirebase extends AnalyticsTracker {
    private FirebaseAnalytics _firebaseAnalytics;

    public AnalyticsTrackerFirebase(Context c) {
        _firebaseAnalytics = FirebaseAnalytics.getInstance(c);
    }

    @Override
    public void trackEvent_Account_Register_Facebook(String userType) {
        Bundle bundle = new Bundle();
        String userTypeParam = null;

        switch (userType) {
            case "OFFERER_AND_SEEKER":
                userTypeParam = AnalyticsTagsFirebase.Param.VALUE_REGISTER_USER_TYPE_SEEKER_OFFERER;
                break;
            case "OFFERER":
                userTypeParam = AnalyticsTagsFirebase.Param.VALUE_REGISTER_USER_TYPE_OFFERER;
                break;
            case "SEEKER":
                userTypeParam = AnalyticsTagsFirebase.Param.VALUE_REGISTER_USER_TYPE_SEEKER;
                break;
        }

        bundle.putString(
                AnalyticsTagsFirebase.Param.KEY_REGISTER_USER_TYPE,
                userTypeParam
        );

        bundle.putString(
                FirebaseAnalytics.Param.SIGN_UP_METHOD,
                AnalyticsTagsFirebase.Param.VALUE_REGISTER_METHOD_FACEBOOK
        );

        _firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);
    }

    @Override
    public void trackEvent_Account_Register_Spareroom(String userType) {
        Bundle bundle = new Bundle();
        String userTypeParam = null;

        switch (userType) {
            case "OFFERER_AND_SEEKER":
                userTypeParam = AnalyticsTagsFirebase.Param.VALUE_REGISTER_USER_TYPE_SEEKER_OFFERER;
                break;
            case "OFFERER":
                userTypeParam = AnalyticsTagsFirebase.Param.VALUE_REGISTER_USER_TYPE_OFFERER;
                break;
            case "SEEKER":
                userTypeParam = AnalyticsTagsFirebase.Param.VALUE_REGISTER_USER_TYPE_SEEKER;
                break;
        }

        bundle.putString(
                AnalyticsTagsFirebase.Param.KEY_REGISTER_USER_TYPE,
                userTypeParam
        );

        bundle.putString(
                FirebaseAnalytics.Param.SIGN_UP_METHOD,
                AnalyticsTagsFirebase.Param.VALUE_REGISTER_METHOD_EMAIL
        );

        _firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);
    }

    @Override
    public void trackEvent_Account_FacebookLoginToRegisterRedirection() {
        Bundle bundle = new Bundle();
        _firebaseAnalytics.logEvent(AnalyticsTagsFirebase.Event.FACEBOOK_REDIRECTION_LOGIN_TO_REGISTER, bundle);
    }

    @Override
    public void trackEvent_Account_FacebookRegisterToLoginRedirection() {
        Bundle bundle = new Bundle();
        _firebaseAnalytics.logEvent(AnalyticsTagsFirebase.Event.FACEBOOK_REDIRECTION_REGISTER_TO_LOGIN, bundle);
    }

    @Override
    public void trackEvent_Account_Register_Facebook_Fail(SpareroomStatus failedRegistration) {
        Bundle bundle = new Bundle();
        String errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_UNDEFINED_ERROR;

        if (failedRegistration != null
                && failedRegistration.getCode() != null
                && !failedRegistration.getCode().isEmpty()) {

            switch (failedRegistration.getCode()) {
                case "652":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_INVALID_EMAIL;
                    break;
                case "655":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_INVALID_EMAIL_LENGTH;
                    break;
                case "654":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_CONFIRM_PASSWORD_FAILED;
                    break;
                case "657":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_AGREE_TERMS_N_CONDITIONS;
                    break;
                case "650":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_USED_EMAIL;
                    break;
                case "658":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_MISSED_FIRST_NAME;
                    break;
                case "659":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_MISSED_LAST_NAME;
                    break;
                case "661":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_MISSED_USER_TYPE;
                    break;
                default:
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_OTHER_ERROR;
            }

            bundle.putString(
                    AnalyticsTagsFirebase.Param.KEY_FAILED_ERROR_CODE,
                    failedRegistration.getCode());
        }

        bundle.putString(
                FirebaseAnalytics.Param.SIGN_UP_METHOD,
                AnalyticsTagsFirebase.Param.VALUE_REGISTER_METHOD_FACEBOOK);

        bundle.putString(
                AnalyticsTagsFirebase.Param.KEY_REGISTER_FAILED,
                errorLabel);

        _firebaseAnalytics.logEvent(AnalyticsTagsFirebase.Event.SIGN_UP_FAILED, bundle);
    }

    @Override
    public void trackEvent_Account_Register_Spareroom_Fail(SpareroomStatus failedRegistration) {
        Bundle bundle = new Bundle();
        String errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_UNDEFINED_ERROR;

        if (failedRegistration != null
                && failedRegistration.getCode() != null
                && !failedRegistration.getCode().isEmpty()) {

            switch (failedRegistration.getCode()) {
                case "652":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_INVALID_EMAIL;
                    break;
                case "655":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_INVALID_EMAIL_LENGTH;
                    break;
                case "654":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_CONFIRM_PASSWORD_FAILED;
                    break;
                case "657":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_AGREE_TERMS_N_CONDITIONS;
                    break;
                case "650":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_USED_EMAIL;
                    break;
                case "658":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_MISSED_FIRST_NAME;
                    break;
                case "659":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_MISSED_LAST_NAME;
                    break;
                case "661":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_ERROR_MISSED_USER_TYPE;
                    break;
                default:
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_REGISTER_FAILED_OTHER_ERROR;

            }

            bundle.putString(
                    AnalyticsTagsFirebase.Param.KEY_FAILED_ERROR_CODE,
                    failedRegistration.getCode());

        }

        bundle.putString(
                FirebaseAnalytics.Param.SIGN_UP_METHOD,
                AnalyticsTagsFirebase.Param.VALUE_REGISTER_METHOD_EMAIL);

        bundle.putString(
                AnalyticsTagsFirebase.Param.KEY_REGISTER_FAILED,
                errorLabel);

        _firebaseAnalytics.logEvent(AnalyticsTagsFirebase.Event.SIGN_UP_FAILED, bundle);
    }

    @Override
    public void trackEvent_Account_Login_Facebook() {
        Bundle bundle = new Bundle();
        bundle.putString(
                AnalyticsTagsFirebase.Param.KEY_LOGIN_METHOD,
                AnalyticsTagsFirebase.Param.VALUE_LOGIN_METHOD_FACEBOOK
        );
        _firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);
    }

    @Override
    public void trackEvent_Account_Login_Spareroom() {
        Bundle bundle = new Bundle();
        bundle.putString(
                AnalyticsTagsFirebase.Param.KEY_LOGIN_METHOD,
                AnalyticsTagsFirebase.Param.VALUE_LOGIN_METHOD_EMAIL
        );
        _firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);
    }

    @Override
    public void trackEvent_Account_Login_Facebook_Failed(SpareroomStatus failedRegistration) {
        Bundle bundle = new Bundle();
        String errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_UNDEFINED_ERROR;

        if (failedRegistration != null
                && failedRegistration.getCode() != null
                && !failedRegistration.getCode().isEmpty()) {

            switch (failedRegistration.getCode()) {
                case "603":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_ERROR_NO_PASSWORD;
                    break;
                case "604":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_ERROR_UNKNOWN_EMAIL;
                    break;
                case "610":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_ERROR_INVALID_EMAIL;
                    break;
                case "605":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_ERROR_WRONG_PASSWORD;
                    break;
                case "601":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_ERROR_MISSED_EMAIL_AND_PASSWORD;
                    break;
                case "602":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_ERROR_MISSED_EMAIL;
                    break;
                case "651":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_ERROR_ALREADY_LOGGED_IN;
                    break;
                default:
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_OTHER_ERROR;

            }

            bundle.putString(
                    AnalyticsTagsFirebase.Param.KEY_FAILED_ERROR_CODE,
                    failedRegistration.getCode());

        }

        bundle.putString(
                AnalyticsTagsFirebase.Param.KEY_LOGIN_METHOD,
                AnalyticsTagsFirebase.Param.VALUE_LOGIN_METHOD_FACEBOOK);

        bundle.putString(
                AnalyticsTagsFirebase.Param.KEY_LOGIN_FAILED,
                errorLabel);

        _firebaseAnalytics.logEvent(AnalyticsTagsFirebase.Event.LOGIN_FAILED, bundle);
    }

    @Override
    public void trackEvent_Account_Login_Spareroom_Failed(SpareroomStatus failedRegistration) {
        Bundle bundle = new Bundle();
        String errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_UNDEFINED_ERROR;

        if (failedRegistration != null
                && failedRegistration.getCode() != null
                && !failedRegistration.getCode().isEmpty()) {

            switch (failedRegistration.getCode()) {
                case "603":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_ERROR_NO_PASSWORD;
                    break;
                case "604":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_ERROR_UNKNOWN_EMAIL;
                    break;
                case "610":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_ERROR_INVALID_EMAIL;
                    break;
                case "605":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_ERROR_WRONG_PASSWORD;
                    break;
                case "601":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_ERROR_MISSED_EMAIL_AND_PASSWORD;
                    break;
                case "602":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_ERROR_MISSED_EMAIL;
                    break;
                case "651":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_ERROR_ALREADY_LOGGED_IN;
                    break;
                default:
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_LOGIN_FAILED_OTHER_ERROR;

            }

            bundle.putString(
                    AnalyticsTagsFirebase.Param.KEY_FAILED_ERROR_CODE,
                    failedRegistration.getCode());

        }

        bundle.putString(
                AnalyticsTagsFirebase.Param.KEY_LOGIN_METHOD,
                AnalyticsTagsFirebase.Param.VALUE_LOGIN_METHOD_EMAIL);

        bundle.putString(
                AnalyticsTagsFirebase.Param.KEY_LOGIN_FAILED,
                errorLabel);

        _firebaseAnalytics.logEvent(AnalyticsTagsFirebase.Event.LOGIN_FAILED, bundle);
    }

    @Override
    public void trackEvent_Account_Logout() {
        _firebaseAnalytics.logEvent(AnalyticsTagsFirebase.Event.LOGOUT, null);
    }

    @Override
    public void trackEvent_Account_ForgotPassword() {
        _firebaseAnalytics.logEvent(AnalyticsTagsFirebase.Event.RECOVER_PASSWORD, null);
    }

    @Override
    public void trackEvent_Account_ForgotPassword_Failed(SpareroomStatus failedRecover) {
        Bundle bundle = new Bundle();
        String errorLabel = AnalyticsTagsFirebase.Param.VALUE_RECOVER_PASSWORD_UNDEFINED_ERROR;

        if (failedRecover != null
                && failedRecover.getCode() != null
                && !failedRecover.getCode().isEmpty()) {

            switch (failedRecover.getCode()) {
                case "1102":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_RECOVER_PASSWORD_FAILED_THREE_REMINDERS;
                    break;
                case "1103":
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_RECOVER_PASSWORD_FAILED_EMAIL;
                    break;
                default:
                    errorLabel = AnalyticsTagsFirebase.Param.VALUE_RECOVER_PASSWORD_OTHER_ERROR;

            }

            bundle.putString(
                    AnalyticsTagsFirebase.Param.KEY_FAILED_ERROR_CODE,
                    failedRecover.getCode());

        }

        bundle.putString(
                AnalyticsTagsFirebase.Param.KEY_RECOVER_PASSWORD_FAILED,
                errorLabel);

        _firebaseAnalytics.logEvent(AnalyticsTagsFirebase.Event.RECOVER_PASSWORD_FAILED, bundle);
    }

    @Override
    public void trackFailedToShowHomeFragmentOnBackPressEvent() {
        _firebaseAnalytics.logEvent(AnalyticsTagsFirebase.Event.SHOW_HOME_FRAGMENT_ON_BACK_PRESS_FAIL, new Bundle());
    }
}
