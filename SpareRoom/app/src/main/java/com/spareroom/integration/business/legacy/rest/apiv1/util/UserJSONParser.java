package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.model.business.User;

import org.json.JSONException;
import org.json.JSONObject;

public class UserJSONParser {
    /**
     * Parses a user from a json String, typically this would be a session object
     *
     * @param jsonObject containing user data
     * @return User object
     * @throws JSONException if the format of the JSON String is incorrect
     */
    public User parse(JSONObject jsonObject) throws JSONException {
        User u = new User();

        u.set_userId(jsonObject.getString("user_id"));
        u.set_firstName(jsonObject.getString("first_name"));
        u.set_lastName(jsonObject.getString("last_name"));
        u.set_profilePhotoUrl(jsonObject.getString("profile_photo_url"));

        return u;
    }

}
