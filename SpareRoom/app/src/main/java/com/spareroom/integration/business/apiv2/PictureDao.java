package com.spareroom.integration.business.apiv2;

import android.net.Uri;

import com.spareroom.R;
import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.SpareroomAbstractDaoImpl;
import com.spareroom.integration.business.legacy.rest.apiv1.security.Authenticator;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.PictureUploadedJSONParser;
import com.spareroom.integration.exception.*;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.service.PictureRestService;
import com.spareroom.model.business.*;
import com.spareroom.ui.util.FileUtils;

import java.io.File;

class PictureDao extends SpareroomAbstractDaoImpl<NewPicture, Void, Void> {

    private final PictureRestService pictureRestService;

    PictureDao(SpareroomContext srContext) {
        super(srContext);
        this.pictureRestService = new PictureRestService();
    }

    @Override
    public Object create(NewPicture picture, Void extra) throws UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {
        File tempPicture = null;
        try {

            Uri pictureUri = Uri.parse(picture.getUri());

            // check if Uri is of image type
            if (!FileUtils.isUriImageType(pictureUri)) {
                SpareroomStatus spareroomStatus = new SpareroomStatus();
                spareroomStatus.setMessage(_spareroomContext.get_applicationContext().getString(R.string.errorPhoto_incorrectType));
                return spareroomStatus;
            }

            // copy image to internal storage
            tempPicture = FileUtils.copyImageToInternalStorage(_spareroomContext.get_applicationContext(), pictureUri);

            String[] vNamesMultipart = new String[]{"file"};
            String[] vValuesMultipart = new String[]{tempPicture.getPath()};

            Authenticator authenticator = new Authenticator();
            Parameters parameters = new Parameters();
            parameters.add("format", "json");
            parameters.add("function", "upload");
            parameters.add("api_sig", authenticator.authenticate(parameters));
            parameters.add("api_key", authenticator.get_apiKey());
            if (picture.getFlatshareId() != null)
                parameters.add("flatshare_id", picture.getFlatshareId());

            String[] vNames = new String[parameters.size()];
            String[] vValues = new String[parameters.size()];

            String[] vParamNames = parameters.getParameterNames();

            for (int i = 0; i < parameters.size(); i++) {
                vNames[i] = vParamNames[i];
                vValues[i] = parameters.get(vParamNames[i]);
            }

            String response = pictureRestService.uploadPicture(vNames, vValues, vNamesMultipart, vValuesMultipart);

            PictureUploadedJSONParser parser = new PictureUploadedJSONParser();

            return parser.parse(response);
        } catch (NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e);
        } catch (ClientErrorException | AuthenticationException e) {
            throw new InvalidArgumentException(e);
        } catch (ServerErrorException | InvalidJSONFormatException e) {
            throw new UnexpectedResultException(e);
        } finally {
            if (tempPicture != null)
                tempPicture.delete();
        }
    }

}
