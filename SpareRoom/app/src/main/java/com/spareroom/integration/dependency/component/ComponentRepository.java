package com.spareroom.integration.dependency.component;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import com.spareroom.integration.dependency.Injectable;

import androidx.fragment.app.*;
import dagger.android.AndroidInjection;
import dagger.android.HasActivityInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.HasSupportFragmentInjector;

public class ComponentRepository {

    private static ComponentRepository componentRepository;
    private AppComponent appComponent;

    private ComponentRepository() {

    }

    public static ComponentRepository get() {
        if (componentRepository == null)
            componentRepository = new ComponentRepository();

        return componentRepository;
    }

    public void setAppComponent(AppComponent appComponent, Application application) {
        this.appComponent = appComponent;

        application.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                handleActivity(activity);
            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    private void handleActivity(Activity activity) {
        if (activity instanceof HasSupportFragmentInjector || activity instanceof HasActivityInjector)
            AndroidInjection.inject(activity);

        if (activity instanceof FragmentActivity) {
            ((FragmentActivity) activity).getSupportFragmentManager().registerFragmentLifecycleCallbacks(new FragmentManager.FragmentLifecycleCallbacks() {

                @Override
                public void onFragmentAttached(FragmentManager fm, Fragment f, Context context) {
                    if (f instanceof Injectable)
                        AndroidSupportInjection.inject(f);
                }
            }, true);
        }
    }
}
