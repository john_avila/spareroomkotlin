package com.spareroom.integration.business.apiv2;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.BusinessAbstractFactory;
import com.spareroom.integration.business.SpareroomAbstractDao;
import com.spareroom.model.business.*;
import com.spareroom.model.extra.*;

public class BusinessAPIv2Factory implements BusinessAbstractFactory {
    private final SpareroomContext _srContext;

    /**
     * Constructor
     *
     * @param srContext the current <code>SpareroomContext</code>
     */
    public BusinessAPIv2Factory(SpareroomContext srContext) {
        _srContext = srContext;
    }

    @Override
    public SpareroomAbstractDao<Void, Void, Void> getSessionDao() {
        return new SessionDao(_srContext);
    }

    @Override
    public SpareroomAbstractDao<AccountCredentials, LoginExtra, AccountConnectionInterface> getLoginTransactionDao() {
        return new LoginTransactionDao(_srContext);
    }

    @Override
    public SpareroomAbstractDao<AccountRegisterForm, RegisterExtra, AccountConnectionInterface> getRegisterTransactionDao() {
        return new RegisterTransactionDao(_srContext);
    }

    @Override
    public SpareroomAbstractDao<AccountRegisterFormFacebook, RegisterExtra, AccountConnectionInterface> getRegisterFacebookTransactionDao() {
        return new RegisterFacebookTransactionDao(_srContext);
    }

    @Override
    public SpareroomAbstractDao<AccountCredentials, ResetPasswordExtra, AccountConnectionInterface> getResetPasswordTransactionDao() {
        return new ResetPasswordTransactionDao(_srContext);
    }

    @Override
    public SpareroomAbstractDao<Void, Void, Void> getLogOutTransactionDao() {
        return new LogOutTransactionDao(_srContext);
    }

    @Override
    public SpareroomAbstractDao<String, Void, Void> getFirebaseIdTransactionDao() {
        return new FirebaseIdTransactionDao(_srContext);
    }

    @Override
    public SpareroomAbstractDao<RegisterParameters, Void, Void> getRegisterRunDao() {
        return new RegisterRunDao(_srContext);
    }

    @Override
    public SpareroomAbstractDao<NewPicture, Void, Void> getPictureDao() {
        return new PictureDao(_srContext);
    }

}
