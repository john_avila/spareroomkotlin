package com.spareroom.integration.business.apiv2

import com.spareroom.model.business.ConversationListRestResponse

interface IConversationListDao {

    @Throws(Exception::class)
    fun getConversationList(offset: Int, maxPerPage: Int): ConversationListRestResponse
}
