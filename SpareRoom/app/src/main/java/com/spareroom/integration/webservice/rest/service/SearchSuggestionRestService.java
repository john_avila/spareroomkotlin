package com.spareroom.integration.webservice.rest.service;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.Parameters;

public class SearchSuggestionRestService extends RestService {

    private static final String AUTOCOMPLETE = "/autocompleter.pl";

    public String getSuggestions(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(AUTOCOMPLETE, parameters);
    }
}
