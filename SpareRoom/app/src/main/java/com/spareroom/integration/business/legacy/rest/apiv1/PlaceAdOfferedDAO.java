package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IPlaceAdOfferedDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.Authenticator;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.PlaceAdEditJsonParser;
import com.spareroom.integration.business.legacy.rest.apiv1.util.PlaceAdJSONParser;
import com.spareroom.integration.business.legacy.rest.apiv1.util.PlaceAdOfferedReadJSONParser;
import com.spareroom.integration.exception.InvalidArgumentException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.exception.UnavailableDataSourceException;
import com.spareroom.integration.exception.UnexpectedResultException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.AdRestService;
import com.spareroom.integration.webservice.rest.service.UserAdsRestService;
import com.spareroom.model.business.DraftAd;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SearchType;
import com.spareroom.model.business.SpareroomStatus;

class PlaceAdOfferedDAO implements IPlaceAdOfferedDAO {

    private final AdRestService adRestService;
    private final UserAdsRestService userAdsRestService;

    PlaceAdOfferedDAO() {
        adRestService = new AdRestService();
        userAdsRestService = new UserAdsRestService();
    }

    public SpareroomStatus create(DraftAdOffered draft) throws AuthenticationException,
            NetworkConnectivityException, ClientErrorException, ServerErrorException,
            InvalidJSONFormatException {
        DraftAdAdapter adapter = new DraftAdAdapter();

        Parameters parameters = adapter.adapt(draft);

        parameters.add("format", "json");
        parameters.add("function", "placelisting");

        Authenticator authenticator = new Authenticator();
        parameters.add("api_sig", authenticator.authenticate(parameters));
        parameters.add("api_key", authenticator.get_apiKey());

        String[] names = parameters.getParameterNames();// params.getParameterNames();
        String[] values = new String[names.length];

        for (int i = 0; i < names.length; i++) {
            names[i] = names[i];
            String value = parameters.get(names[i]);
            values[i] = (value != null) ? value : "";
        }

        String response = adRestService.placeAd(names, values, SearchType.OFFERED);

        PlaceAdJSONParser parser = new PlaceAdJSONParser();
        return parser.parse(response);
    }

    public Object read(String advertId)
            throws UnavailableDataSourceException,
            InvalidArgumentException,
            UnexpectedResultException {

        try {
            Parameters parameters = new Parameters();
            DraftAd draftAd;

            parameters.add("advert_id", advertId);
            parameters.add("format", "json");
            parameters.add("function", "getlisting");

            String response = adRestService.getDraftAdvert(parameters, SearchType.OFFERED);

            draftAd = (DraftAd) (new PlaceAdOfferedReadJSONParser()).parse(response);
            draftAd.setId(advertId);

            return draftAd;
        } catch (ServerErrorException | NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e.getMessage(), e);
        } catch (InvalidJSONFormatException e) {
            throw new UnexpectedResultException(e.getMessage(), e);
        } catch (AuthenticationException | ClientErrorException e) {
            throw new InvalidArgumentException(e.getMessage(), e);
        }

    } //end read(String advertId)

    @Override
    public SpareroomStatus update(DraftAdOffered draft)
            throws UnavailableDataSourceException,
            InvalidArgumentException,
            UnexpectedResultException {

        try {

            DraftAdAdapter adapter = new DraftAdAdapter();
            Parameters parameters = adapter.adapt(draft);
            Authenticator authenticator = new Authenticator();
            PlaceAdEditJsonParser parser = new PlaceAdEditJsonParser();
            String names[], values[], response;

            parameters.add("format", "json");
            // Lets know to the API we want edit the advert
            parameters.add("function", "editlisting");
            // Adds missed data
            parameters.add("advert_id", draft.getId());
            // Sets the authentication parameters
            parameters.add("api_sig", authenticator.authenticate(parameters));
            parameters.add("api_key", authenticator.get_apiKey());
            // Sets the parameters for make the API allow us to make changes in the data
            parameters.add("theadvert", "yes");
            parameters.add("therooms", "yes");
            parameters.add("thehousehold", "yes");
            parameters.add("theproperty", "yes");

            names = parameters.getParameterNames();
            values = new String[names.length];

            for (int i = 0; i < names.length; i++) {
                String value = parameters.get(names[i]);
                values[i] = (value != null) ? value : "";
            }

            response = userAdsRestService.updateOfferedAd(names, values);

            return parser.parse(response);

        } catch (ServerErrorException | NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e.getMessage(), e);
        } catch (InvalidJSONFormatException e) {
            throw new UnexpectedResultException(e.getMessage(), e);
        } catch (AuthenticationException | ClientErrorException e) {
            throw new InvalidArgumentException(e.getMessage(), e);
        }

    } //end update(DraftAdOffered draft)

}
