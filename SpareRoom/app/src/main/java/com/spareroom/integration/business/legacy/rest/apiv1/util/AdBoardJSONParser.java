package com.spareroom.integration.business.legacy.rest.apiv1.util;

import android.app.Application;

import com.spareroom.App;
import com.spareroom.controller.AppVersion;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.json.IsoDateAdapter;
import com.spareroom.integration.json.JsonParser;
import com.spareroom.model.business.*;
import com.spareroom.model.business.SearchAdvertListResponse.ResponseTypes;
import com.spareroom.ui.util.DateUtils;

import org.json.*;

import java.util.LinkedList;

public class AdBoardJSONParser implements IAdBoardJSONParser {
    private final static String RESPONSE = "response";
    private final static String RESPONSE_ID = "response_id";
    private final static String SUCCESS = "success";
    private final static String WARNING = "warning";
    private final static String RESPONSE_TYPE = "response_type";

    private String responseType;
    private ResponseTypes typeOfResponse;
    private String responseCode;
    private String responseMessage;

    private final SearchType searchType;
    private final JsonParser jsonParser;

    public AdBoardJSONParser(SearchType searchType) {
        this(searchType, App.get());
    }

    public AdBoardJSONParser(SearchType searchType, Application application) {
        this.searchType = searchType;
        jsonParser = new JsonParser(new IsoDateAdapter(new DateUtils(AppVersion.flavor().getLocale(), application)));
    }

    @Override
    public Object parse(String json) throws InvalidJSONFormatException {
        AdBoard board = new AdBoard();
        LinkedList<AbstractAd> ads = new LinkedList<>();

        try {
            JSONObject firstNode = new JSONObject(json);

            if ("1".equals(firstNode.optString(SUCCESS))) { // Successful response
                typeOfResponse = ResponseTypes.SUCCESS;
                if (WARNING.equals(firstNode.optString(RESPONSE_TYPE))) {
                    typeOfResponse = ResponseTypes.WARNING;
                    responseMessage = firstNode.optString(RESPONSE);
                }

                if (firstNode.has("featured_ads"))
                    createAdvert(ads, firstNode.getJSONArray("featured_ads"));

                if (firstNode.has("results"))
                    createAdvert(ads, firstNode.getJSONArray("results"));

                if (SearchType.OFFERED == searchType) {
                    JSONObject jsonLocation = null;
                    if (firstNode.has("location_info")) {
                        jsonLocation = firstNode.getJSONObject("location_info");
                    }

                    if (jsonLocation != null &&
                            (jsonLocation.has("latitude")
                                    && jsonLocation.has("longitude")
                                    && jsonLocation.has("latitude_delta")
                                    && jsonLocation.has("longitude_delta")) &&
                            (!jsonLocation.getString("latitude").equals("null")
                                    && !jsonLocation.getString("longitude").equals("null")
                                    && !jsonLocation.getString("latitude_delta").equals("null")
                                    && !jsonLocation.getString("longitude_delta").equals("null"))

                            ) {
                        MapReference mapReference = new MapReference();
                        mapReference.set_latitude(Float.parseFloat(jsonLocation.getString("latitude")));
                        mapReference.set_longitude(Float.parseFloat(jsonLocation.getString("longitude")));
                        mapReference.set_latitudeDelta(Float.parseFloat(jsonLocation.getString("latitude_delta")));
                        mapReference.set_longitudeDelta(Float.parseFloat(jsonLocation.getString("longitude_delta")));
                        board.set_mapReference(mapReference);

                    } else {
                        board.set_mapReference(new MapReference());
                    }
                }

                board.set_total(Integer.parseInt(firstNode.getString("count")));
                board.set_board(ads);

            } else {
                responseType = firstNode.optString(RESPONSE_TYPE);
                responseMessage = firstNode.optString(RESPONSE);
                responseCode = firstNode.optString(RESPONSE_ID);

                if (ResponseTypes.NO_MATCHES.getValue().equals(responseType)) {
                    typeOfResponse = ResponseTypes.NO_MATCHES;
                    return new AdBoard(new LinkedList<>());

                } else if (ResponseTypes.SUGGESTION_LIST.getValue().equals(responseType)) {
                    typeOfResponse = ResponseTypes.SUGGESTION_LIST;
                    SuggestionListJSONParser suggestionParser = new SuggestionListJSONParser();
                    return suggestionParser.parse(json);

                } else if (ResponseTypes.WARNING.getValue().equals(responseType)) {
                    typeOfResponse = ResponseTypes.WARNING;
                    responseCode = firstNode.optString(SUCCESS);
                    // do nothing, will fill broaden options if suitable

                } else {
                    throw new JSONException("Unexpected response type: " + responseType);
                }
            }

            BroadenSearches relatedSearches = new BroadenSearches();
            if (firstNode.has("broaden_search_options")) {
                JSONArray jsonBroadenSearchOptions = firstNode.getJSONArray("broaden_search_options");
                for (int i = 0; i < jsonBroadenSearchOptions.length(); i++) {
                    JSONObject jsonOption = jsonBroadenSearchOptions.getJSONObject(i);
                    BroadenSearch option = new BroadenSearch();
                    if (jsonOption.has("criteria_to_add_string")) {
                        option.set_criteriaToAdd(jsonOption.getString("criteria_to_add_string"));
                    }
                    if (jsonOption.has("criteria_to_lose_string")) {
                        option.set_criteriaToLose(jsonOption.getString("criteria_to_lose_string"));
                    }
                    if (jsonOption.has("criteria_to_keep_string")) {
                        option.set_criteriaToKeep(jsonOption.getString("criteria_to_keep_string"));
                    }
                    if (jsonOption.has("add_params")) {
                        JSONObject jsonAddParams = jsonOption.getJSONObject("add_params");
                        JSONArray jsonParamNames = jsonAddParams.names();
                        Parameters addParams = new Parameters();
                        for (int j = 0; j < jsonParamNames.length(); j++) {
                            String param = jsonParamNames.getString(j);
                            addParams.add(param, jsonAddParams.getString(param));
                        }
                        option.set_paramsToAdd(addParams);
                    }
                    if (jsonOption.has("remove_params")) {
                        JSONArray jsonRemoveParams = jsonOption.getJSONArray("remove_params");
                        Parameters removeParams = new Parameters();
                        for (int j = 0; j < jsonRemoveParams.length(); j++) {
                            String param = jsonRemoveParams.getString(j);
                            // The "1" value is only set like that in order to avoid the `Parameters` object to delete the param with the
                            // `StringUtils.isNullOrEmpty(value)` check when it is added. Has no other function
                            removeParams.add(param, "1");
                        }
                        option.set_paramsToLose(removeParams);
                    }
                    if (jsonOption.has("num_matches")) {
                        option.set_numMatches(Integer.parseInt(jsonOption.getString("num_matches")));
                    }
                    if (jsonOption.has("url")) {
                        option.set_url(jsonOption.getString("url"));
                    }
                    relatedSearches.add(option);
                }
            }

            board.set_broadenSearch(relatedSearches);
            return board;

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

    }

    public ResponseTypes responseType() {
        return typeOfResponse;
    }

    public String responseCode() {
        return responseCode;
    }

    public String responseMessage() {
        return responseMessage;
    }

    public String responseCause() {
        return responseType;
    }

    private void createAdvert(LinkedList<AbstractAd> ads, JSONArray jsonArray) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject result = jsonArray.getJSONObject(i);

            AbstractAd ad = searchType == SearchType.OFFERED ? jsonParser.fromJson(result.toString(), AdOffered.class) : jsonParser.fromJson(result.toString(), AdWanted.class);
            if (ad != null)
                ads.add(ad);
        }
    }

}
