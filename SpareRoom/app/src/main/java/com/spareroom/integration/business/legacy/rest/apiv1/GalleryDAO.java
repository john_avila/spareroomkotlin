package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IGalleryDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.GalleryJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.PictureRestService;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.PictureGallery;

public class GalleryDAO implements IGalleryDAO {

    private final PictureRestService pictureRestService;

    GalleryDAO() {
        pictureRestService = new PictureRestService();
    }

    @Override
    public PictureGallery read(Parameters parameters) throws NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException {

        parameters.add("format", "json");
        parameters.add("function", "get_photos");

        String response = pictureRestService.getPictures(parameters);

        GalleryJSONParser parser = new GalleryJSONParser();

        return parser.parse(response);
    }

}
