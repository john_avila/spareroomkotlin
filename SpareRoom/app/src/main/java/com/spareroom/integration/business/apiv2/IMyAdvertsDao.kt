package com.spareroom.integration.business.apiv2

import com.spareroom.model.business.MyAdverts

interface IMyAdvertsDao {

    @Throws(Exception::class)
    fun getAdverts(offset: Int, maxPerPage: Int): MyAdverts

    @Throws(Exception::class)
    fun activateAdvert(advertId: String, offered: Boolean): MyAdverts

    @Throws(Exception::class)
    fun renewAdvert(advertId: String, offered: Boolean): MyAdverts

    @Throws(Exception::class)
    fun deactivateAdvert(advertId: String, offered: Boolean): MyAdverts
}