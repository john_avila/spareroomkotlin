package com.spareroom.integration.business.apiv2

import com.google.gson.reflect.TypeToken
import com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum
import com.spareroom.integration.json.JsonParser
import com.spareroom.integration.webservice.rest.service.AdRestService
import com.spareroom.model.business.*
import javax.inject.Inject

class OfferedAdvertsDao @Inject constructor(
    private val adRestService: AdRestService,
    private val parser: JsonParser
) : IOfferedAdvertsDao {

    @Throws(Exception::class)
    override fun getUserAdverts(userId: String, page: Int, maxPerPage: Int): AdvertsListRestResponse<AdOffered> {
        val parameters = Parameters()
        parameters.add(SearchParamEnum.USER_ID, userId)
        parameters.add(SearchParamEnum.PAGE, page)
        parameters.add(SearchParamEnum.MAX_PER_PAGE, maxPerPage)
        parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON)

        val response = adRestService.getAdverts(parameters, SearchType.OFFERED)
        return parser.fromJson(response, object : TypeToken<AdvertsListRestResponse<AdOffered>>() {}.type)
    }

    @Throws(Exception::class)
    override fun getAdvert(advertId: String): SingleAdvertRestResponse<AdOffered> {
        val parameters = Parameters()
        parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON)

        val response = adRestService.getSingleAdvert(advertId, parameters, SearchType.OFFERED)
        return parser.fromJson(response, object : TypeToken<SingleAdvertRestResponse<AdOffered>>() {}.type)
    }

    @Throws(Exception::class)
    override fun getSavedAdverts(offset: Int, maxPerPage: Int): AdvertsListRestResponse<AdOffered> {
        val parameters = Parameters()
        parameters.add(SearchParamEnum.FILTER, SearchParamEnum.SHORTLIST)
        parameters.add(SearchParamEnum.HIDE_UNSUITABLE, ApiParams.YES_INT)
        parameters.add(SearchParamEnum.OFFSET, offset)
        parameters.add(SearchParamEnum.MAX_PER_PAGE, maxPerPage)
        parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON)

        val response = adRestService.getAdverts(parameters, SearchType.OFFERED)
        return parser.fromJson(response, object : TypeToken<AdvertsListRestResponse<AdOffered>>() {}.type)
    }

}