package com.spareroom.integration.business.apiv2;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.SpareroomAbstractDaoImpl;
import com.spareroom.integration.business.apiv2.util.FirebaseIdTransactionJsonParser;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InvalidArgumentException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.exception.UnavailableDataSourceException;
import com.spareroom.integration.exception.UnexpectedResultException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.FirebaseRestService;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.business.SpareroomStatusCode;

/**
 * CRUD for the Firebase Id in the SpareRoom server.
 */
class FirebaseIdTransactionDao extends SpareroomAbstractDaoImpl<String, Void, Void> {

    private final FirebaseRestService firebaseRestService;

    FirebaseIdTransactionDao(SpareroomContext spareroomContext) {
        super(spareroomContext);
        firebaseRestService = new FirebaseRestService();
    }

    @Override
    public Object create(String firebaseToken, Void extra) throws UnavailableDataSourceException,
            InvalidArgumentException, UnexpectedResultException {

        Parameters parameters = new Parameters();
        SpareroomStatus result;

        parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON);
        parameters.add(FirebaseIdParamEnum.ACTION, FirebaseIdParamEnum.ACTION_REGISTER);
        parameters.add(FirebaseIdParamEnum.PLATFORM, FirebaseIdParamEnum.PLATFORM_GCM);
        parameters.add(FirebaseIdParamEnum.TOKEN, firebaseToken);

        try {
            String response = firebaseRestService.registerToken(parameters);
            result = FirebaseIdTransactionJsonParser.parse(response);

            if (result != null) {
                switch (result.getCode()) {
                    case SpareroomStatusCode.CODE_400:
                    case SpareroomStatusCode.NOT_LOGGED_IN:
                        throw new ClientErrorException(result.getMessage());
                    case SpareroomStatusCode.CODE_500:
                    case SpareroomStatusCode.CODE_1151:
                    case SpareroomStatusCode.CODE_1152:
                    case SpareroomStatusCode.CODE_1153:
                        throw new ServerErrorException(result.getMessage());
                }
            }

            return result;
        } catch (NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e);
        } catch (ClientErrorException | AuthenticationException e) {
            throw new InvalidArgumentException(e);
        } catch (ServerErrorException | InvalidJSONFormatException e) {
            throw new UnexpectedResultException(e);
        }

    }

    @Override
    public Object delete(String firebaseToken, Void extra) throws UnavailableDataSourceException,
            InvalidArgumentException, UnexpectedResultException {

        Parameters parameters = new Parameters();
        SpareroomStatus result;

        parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON);
        parameters.add(FirebaseIdParamEnum.ACTION, FirebaseIdParamEnum.ACTION_UNREGISTER);
        parameters.add(FirebaseIdParamEnum.PLATFORM, FirebaseIdParamEnum.PLATFORM_GCM);
        parameters.add(FirebaseIdParamEnum.TOKEN, firebaseToken);

        try {
            String response = firebaseRestService.deleteToken(parameters);
            result = FirebaseIdTransactionJsonParser.parse(response);

            if (result != null) {
                switch (result.getCode()) {
                    case SpareroomStatusCode.CODE_400:
                    case SpareroomStatusCode.NOT_LOGGED_IN:
                        throw new ClientErrorException(result.getMessage());
                    case SpareroomStatusCode.CODE_500:
                    case SpareroomStatusCode.CODE_1151:
                    case SpareroomStatusCode.CODE_1152:
                    case SpareroomStatusCode.CODE_1153:
                        throw new ServerErrorException(result.getMessage());
                }
            }

            return null;
        } catch (NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e);
        } catch (ClientErrorException | AuthenticationException e) {
            throw new InvalidArgumentException(e);
        } catch (ServerErrorException | InvalidJSONFormatException e) {
            throw new UnexpectedResultException(e);
        }

    }

}
