package com.spareroom.integration.business.apiv2;

/**
 * Enum of parameters used for Parameter object
 */

class FirebaseIdParamEnum {
    static final String ACTION = "action";
    static final String ACTION_REGISTER = "register";
    static final String ACTION_UNREGISTER = "unregister";
    static final String PLATFORM = "platform";
    static final String PLATFORM_GCM = "GCM";
    static final String TOKEN = "token";

}
