package com.spareroom.integration.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.spareroom.model.business.*;

public class SessionDAO {

    private final static String PREFS_NAME = "session";

    public boolean create(Context c, Session s) {
        SharedPreferences sp = c.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor spe = sp.edit();

        spe.putString("facebookSessionKey", s.get_facebookSessionKey());
        spe.putString("facebookUserId", s.get_facebookUserId());
        spe.putString("loggedIn", s.get_loggedIn());
        spe.putString("sessionId", s.get_sessionId());
        spe.putString("sessionKey", s.get_sessionKey());
        spe.putString("sessionType", s.get_sessionType());

        spe.putString("upgradeProduct", s.get_upgradeProduct());
        spe.putString("upgradeExpire", s.get_upgradeExpire());
        spe.putString("upgradeTimeLeft", s.get_upgradeTimeLeft());
        spe.putString("upgradeNoAdsOffered", s.get_upgradeNoAdsOffered());
        spe.putString("upgradeNoAdsWanted", s.get_upgradeNoAdsWanted());
        spe.putInt("photoUploadLimit", s.getPhotoUploadLimit());
        spe.putString("unixAccessExp", s.getUnixAccessExp());

        spe.putBoolean("paid", s.is_paid());
        spe.putString("email", s.get_email());

        spe.putString("messageCount", s.get_messageCount());

        spe.putString("usertype", s.getUserType());

        User u = s.get_user();
        spe.putString("user_firstName", u.get_firstName());
        spe.putString("user_lastName", u.get_lastName());
        spe.putString("user_userId", u.get_userId());
        spe.putString("user_profilePicture", u.get_profilePhotoUrl());
        return spe.commit();
    }

    public Session read(Context c) {
        Session s = new Session();
        SharedPreferences sp = c.getSharedPreferences(PREFS_NAME, 0);

        String sessionId = sp.getString("sessionId", "");
        if (sessionId.equals(""))
            return null;

        s.set_facebookSessionKey(sp.getString("facebookSessionKey", ""));
        s.set_facebookUserId(sp.getString("facebookUserId", ""));
        s.set_loggedIn(sp.getString("loggedIn", ""));
        s.set_sessionId(sessionId);
        s.set_sessionKey(sp.getString("sessionKey", ""));
        s.set_sessionType(sp.getString("sessionType", ""));

        s.set_upgradeProduct(sp.getString("upgradeProduct", ""));
        s.set_upgradeExpire(sp.getString("upgradeExpire", ""));
        s.set_upgradeTimeLeft(sp.getString("upgradeTimeLeft", ""));
        s.set_upgradeNoAdsOffered(sp.getString("upgradeNoAdsOffered", ""));
        s.set_upgradeNoAdsWanted(sp.getString("upgradeNoAdsWanted", ""));
        s.setPhotoUploadLimit(sp.getInt("photoUploadLimit", 5));
        s.setUnixAccessExp(sp.getString("unixAccessExp", ""));

        s.set_paid(sp.getBoolean("paid", false));
        s.set_email(sp.getString("email", ""));

        s.set_messageCount(sp.getString("messageCount", "0"));

        s.setUserType(sp.getString("usertype", ""));

        User u = new User();
        u.set_firstName(sp.getString("user_firstName", ""));
        u.set_lastName(sp.getString("user_lastName", ""));
        u.set_userId(sp.getString("user_userId", ""));
        u.set_profilePhotoUrl(sp.getString("user_profilePicture", ""));

        s.set_upgradeOptions(upgradeOptions());
        s.set_user(u);
        return s;
    }

    public boolean delete(Context c) {
        SharedPreferences sp = c.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor spe = sp.edit();

        spe.clear();
        return spe.commit();
    }

    private static UpgradeOptionList upgradeOptions() {
        UpgradeOptionList listOptions = new UpgradeOptionList();

        UpgradeOption option105 = new UpgradeOption();
        String[][] benefits105 = new String[][]{
                {"Contact all Room Wanted adverts for free", "Access to the latest adverts ahead of the rest"},
                {"Your Room for Rent advert will be made bold and free for all to contact", "Give your own advert a boost"}
        };
        option105.set_benefits(benefits105);
        option105.set_callToAction("Upgrade to Early Bird/Bold Advertising from just 75p per day");
        option105.set_notice("This is a new non-bold advert (less than 7 days old), so Early Bird access/ Bold Advertising is required to contact the advertiser (tap to read more about how the site works)");
        listOptions.add("105", option105);

        UpgradeOption option46 = new UpgradeOption();
        String[][] benefits46 = new String[][]{
                {"Rooms go fast - give yourself a head start with Early Bird, giving you full access to the newest adverts. Regular users can only contact Bold adverts and adverts over 7 days old; as an Early Bird user all contact restrictions are lifted, allowing you to contact everyone", "Access to the latest adverts ahead of the rest"},
                {"Your Room Wanted advert will be made bold and free for all to contact", "Give your own advert a boost"}
        };
        option46.set_benefits(benefits46);
        option46.set_callToAction("Renew your Early Bird access from just 75p per day");
        listOptions.add("46", option46);

        UpgradeOption option47 = new UpgradeOption();
        String[][] benefits47 = new String[][]{
                {"Your advert will be made bold and prioritised on the search results. Non-bold adverts are subject to our 7 day Early Bird phase, reserving your advert for our Early Bird users - upgrading skips this stage and lets anyone contact you immediately", "Your advert will be Bold & free for all to contact"},
                {"Upgrading gives you Early Bird access so you can contact all adverts on the site", "Contact all Room Wanted adverts"}
        };
        option47.set_benefits(benefits47);
        option47.set_callToAction("Renew your Bold / Early Bird access from just 75p per day");
        listOptions.add("47", option47);

        UpgradeOption option99 = new UpgradeOption();
        String[][] benefits99 = new String[][]{
                {"Your advert will be made bold and prioritised on the search results. Non-bold adverts are subject to our 7 day Early Bird phase, reserving your advert for our Early Bird users - upgrading skips this stage and lets anyone contact you immediately", "Your advert will be Bold & free for all to contact"},
                {"Upgrading gives you Early Bird access so you can contact all adverts on the site", "Contact all Room Wanted adverts"}
        };
        option99.set_benefits(benefits99);
        option99.set_callToAction("Renew your Bold / Early Bird access from just 75p per day");
        option99.set_notice("This is a new non-bold advert (less than 7 days old), so Early Bird access is required to contact the advertiser (tap to read more about how the site works)");
        listOptions.add("99", option99);

        return listOptions;
    }

}
