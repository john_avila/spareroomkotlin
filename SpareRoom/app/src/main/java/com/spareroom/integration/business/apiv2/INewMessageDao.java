package com.spareroom.integration.business.apiv2;

import com.spareroom.integration.exception.*;
import com.spareroom.model.business.NewMessage;
import com.spareroom.model.business.NewMessageResponse;

public interface INewMessageDao {

    NewMessageResponse getDeclineMessageText(String messageId, String template) throws UnexpectedResultException, UnavailableDataSourceException, InvalidArgumentException;

    NewMessageResponse decline(String messageId, String declineText, String template) throws UnexpectedResultException, UnavailableDataSourceException, InvalidArgumentException;

    NewMessageResponse sendNewMessage(NewMessage message) throws UnexpectedResultException, UnavailableDataSourceException, InvalidArgumentException;
}
