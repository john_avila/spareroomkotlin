package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IAdBoardDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.AdBoardJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.AdRestService;
import com.spareroom.model.business.AdBoard;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SearchType;

class AdBoardDAO implements IAdBoardDAO {
    private final SearchType searchType;
    private final AdRestService adRestService = new AdRestService();

    AdBoardDAO(SearchType searchType) {
        this.searchType = searchType;
    }

    @Override
    public AdBoard read(Parameters parameters) throws NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException {
        String response = adRestService.getAdverts(parameters, searchType);
        Object result = new AdBoardJSONParser(searchType).parse(response);

        return result instanceof AdBoard ? (AdBoard) result : new AdBoard();
    }

}
