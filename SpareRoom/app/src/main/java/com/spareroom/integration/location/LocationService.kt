package com.spareroom.integration.location

import android.annotation.SuppressLint
import android.app.Application
import android.content.Intent
import android.content.IntentSender
import android.provider.Settings
import androidx.lifecycle.*
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.spareroom.livedata.LocationLiveData
import com.spareroom.ui.screen.Activity
import com.spareroom.ui.screen.Fragment
import com.spareroom.ui.util.Launcher
import javax.inject.Inject

private const val REQUEST_EXPIRATION_TIME_IN_MILLIS = 120000L
private const val INTERVAL_LOCATION_UPDATES_TIME_IN_MILLIS = 0L
private const val AMOUNT_OF_UPDATES = 1

class LocationService @Inject constructor(application: Application) : LifecycleObserver {

    private val fusedLocationClient = LocationServices.getFusedLocationProviderClient(application)
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private lateinit var builder: LocationSettingsRequest.Builder

    var requested = false
        private set

    fun init(lifecycleOwner: LifecycleOwner, requested: Boolean, locationLiveData: LocationLiveData) {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                stopLocationUpdates()
                locationLiveData.location(locationResult.lastLocation)
            }

            override fun onLocationAvailability(locationAvailability: LocationAvailability) {
                super.onLocationAvailability(locationAvailability)

                if (!locationAvailability.isLocationAvailable) {
                    stopLocationUpdates()
                    locationLiveData.failed()
                }
            }
        }

        locationRequest = LocationRequest().apply {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            interval = INTERVAL_LOCATION_UPDATES_TIME_IN_MILLIS
            numUpdates = AMOUNT_OF_UPDATES
            setExpirationDuration(REQUEST_EXPIRATION_TIME_IN_MILLIS)
        }

        builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)

        this.requested = requested
        lifecycleOwner.lifecycle.addObserver(this)
    }

    @SuppressLint("MissingPermission")
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private fun start() {
        if (requested)
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private fun stop() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    fun startLocationUpdates() {
        requested = true
        start()
    }

    fun stopLocationUpdates() {
        requested = false
        stop()
    }

    fun openLocationSettings(fragment: Fragment) {
        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        if (Launcher.canHandleIntent(fragment.context, intent))
            fragment.startActivity(intent)
    }

    fun checkLocationSettings(activity: Activity, requestCode: Int, resolveFailureObserver: () -> Unit, failureObserver: () -> Unit) {
        val client = LocationServices.getSettingsClient(activity)
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener { _ -> startLocationUpdates() }
        task.addOnFailureListener { exception -> fixSettingsFailure(activity, exception, requestCode, resolveFailureObserver, failureObserver) }
    }

    private fun fixSettingsFailure(activity: Activity, exception: Exception, requestCode: Int, resolveFailureObserver: () -> Unit, failureObserver: () -> Unit) {
        if (exception is ResolvableApiException) {
            try {
                exception.startResolutionForResult(activity, requestCode)
                resolveFailureObserver()
            } catch (ignore: IntentSender.SendIntentException) {
                failureObserver()
            }
        } else {
            failureObserver()
        }
    }

}
