package com.spareroom.integration.business.legacy.rest.apiv1.security;

import com.spareroom.controller.AppVersion;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.model.business.Parameters;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.inject.Inject;

/**
 * Class that authenticates API calls
 *
 * @author manuel
 */
public class Authenticator {
    private final String _apiKey = AppVersion.flavor().getApiId();
    private final String _secretKey = AppVersion.flavor().getApiSecret();

    @Inject
    public Authenticator() {

    }

    /***
     * Generates a new api signature for the parameters provided
     * @param parameters parameters needed for producing a new api signature
     * @return api signature for the provided parameters, null if null parameters are given
     * @throws AuthenticationException if MD5 algorithm cannot be applied
     */
    public String authenticate(Parameters parameters) throws AuthenticationException {

        if (parameters == null)
            return null;

        String message = _secretKey;
        if (parameters.getParameterNames().length != 0) {
            // Step 1: Sort the parameters
            String[] parameterNames = parameters.getParameterNames();
            //     Keys should be sorted alphabetically as they come from a TreeMap (which implements SortedMap)

            // Step 2: Build the message (Secret key + key-value params)

            for (String param : parameterNames) {
                message += (param + parameters.get(param));
            }
        }

        // Step 3: MD5 of the previous message
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            // String apiSignature = new String(md.digest(message.getBytes()));
            byte[] digest = md.digest(message.getBytes());

            String apiSignature = (new BigInteger(1, digest)).toString(16);

            while (apiSignature.length() < 32) {
                apiSignature = "0" + apiSignature;
            }

            return apiSignature;
        } catch (NoSuchAlgorithmException e) {
            throw new AuthenticationException("Hashing algorithm not found");
        }

    }

    public String get_apiKey() {
        return _apiKey;
    }

}
