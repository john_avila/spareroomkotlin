package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

public class UpgradeListJSONParser {
    /**
     * Parses a list of upgrades from a JSON String
     *
     * @param json list of upgrades in JSON format
     * @return a list containing the product ids for upgrades
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public List<String> parse(String json) throws InvalidJSONFormatException {
        JSONObject firstNode;
        try {
            firstNode = new JSONObject(json);
            if (firstNode.has("product_ids")) {
                JSONArray jsonProductIds = firstNode.getJSONArray("product_ids");
                LinkedList<String> lProductIds = new LinkedList<>();
                for (int i = 0; i < jsonProductIds.length(); i++) {
                    String productId = jsonProductIds.getString(i);
                    lProductIds.add(productId);
                }
                return lProductIds;
            } else {
                // TODO error - throw Internal Message?
                throw new InvalidJSONFormatException();
            }
        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }
    }
}
