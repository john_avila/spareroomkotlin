package com.spareroom.integration.business;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.apiv2.BusinessAPIv2Factory;

/**
 * Singleton for the Factory that communicates with SpareRoom
 */
public class BusinessFactorySingleton {
    private static BusinessAbstractFactory __instance;

    private BusinessFactorySingleton() {
    }

    /**
     * Gets an Instance of the Factory that communicates with SpareRoom
     *
     * @param spareroomContext the SpareRoom Context where this factory will run
     * @return the Factory that communicates with SpareRoom
     */
    public static synchronized BusinessAbstractFactory getInstance(
            SpareroomContext spareroomContext) {
        if (__instance == null)
            __instance = new BusinessAPIv2Factory(spareroomContext);
        return __instance;
    }
}
