package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.ILegacyBusinessAbstractFactory;
import com.spareroom.integration.business.legacy.rest.*;
import com.spareroom.model.business.SearchType;

public class RestAPIv1Factory implements ILegacyBusinessAbstractFactory {

    @Override
    public IAdBoardDAO getAdBoardDAO(SearchType searchType) {
        return new AdBoardDAO(searchType);
    }

    @Override
    public ILocationDAO getLocationDAO() {
        return new LocationDAO();
    }

    @Override
    public ISavedAdDAO getSavedAdDAO() {
        return new SavedAdDAO();
    }

    @Override
    public IMarkAdDAO getMarkedAdDAO() {
        return new MarkAdDAO();
    }

    @Override
    public IClockCalledDAO getClockCalledDAO() {
        return new ClockCalledDAO();
    }

    @Override
    public IClockFeaturedDAO getClockFeaturedDAO() {
        return new ClockFeaturedDAO();
    }

    @Override
    public IRemovedSavedAdDAO getRemovedSavedAdDAO() {
        return new RemovedSavedAdDAO();
    }

    @Override
    public IUpgradeListDAO getUpgradeListDAO() {
        return new UpgradeListDAO();
    }

    @Override
    public IMessageThreadDAO getMessageThreadDAO() {
        return new MessageThreadDAO();
    }

    @Override
    public IUpgradeDAO getUpgradeDAO() {
        return new UpgradeDAO();
    }

    @Override
    public ISavedSearchDAO getSavedSearchDAO() {
        return new SavedSearchDAO();
    }

    @Override
    public ISavedSearchListDAO getSavedSearchListDAO() {
        return new SavedSearchListDAO();
    }

    @Override
    public IProfilePreferenceListDAO getProfilePreferenceListDAO() {
        return new ProfilePreferenceListDAO();
    }

    @Override
    public IMyOfferedAdDAO getMyOfferedAdDAO() {
        return new MyOfferedAdDAO();
    }

    @Override
    public IMyWantedAdDAO getMyWantedAdDAO() {
        return new MyWantedAdDAO();
    }

    @Override
    public IGalleryDAO getGalleryDAO() {
        return new GalleryDAO();
    }

    @Override
    public IPictureDAO getPictureDAO() {
        return new PictureDAO();
    }

    @Override
    public IPlaceAdAreaDAO getPlaceAdAreaDAO() {
        return new PlaceAdAreaDAO();
    }

    @Override
    public IPostcodeFinderDAO getPostcodeFinderDAO() {
        return new PostcodeFinderDAO();
    }

    @Override
    public IPlaceAdOfferedDAO getPlaceAdOfferedDAO() {
        return new PlaceAdOfferedDAO();
    }

    @Override
    public IPlaceAdWantedDAO getPlaceAdWantedDAO() {
        return new PlaceAdWantedDAO();
    }

    @Override
    public ISearchSuggestionListDAO getSearchSuggestionListDAO() {
        return new SearchSuggestionListDAO();
    }

    @Override
    public IVideoDAO getVideoDAO() {
        return new VideoDAO();
    }

    @Override
    public IVideoGalleryDAO getVideoGalleryDAO() {
        return new VideoGalleryDAO();
    }

}
