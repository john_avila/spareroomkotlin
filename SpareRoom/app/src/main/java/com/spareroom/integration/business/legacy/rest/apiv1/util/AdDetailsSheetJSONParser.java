package com.spareroom.integration.business.legacy.rest.apiv1.util;

import android.util.Pair;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.AdDetailsSheet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Vector;

public class AdDetailsSheetJSONParser {
    /**
     * Parses a wanted ad details sheet
     *
     * @param json String containing a wanted ad details sheet
     * @return AdWantedDetailsSheet object
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public AdDetailsSheet parse(String json, boolean checkAreasLookingIn) throws InvalidJSONFormatException {

        AdDetailsSheet sheet = new AdDetailsSheet();

        try {
            JSONObject firstNode = new JSONObject(json);

            String success = firstNode.getString("success");
            if (!success.equals("1"))
                return null;

            JSONArray jsonAdDetails = firstNode.getJSONArray("advert_details");
            for (int i = 0; i < jsonAdDetails.length(); i++) {
                JSONObject jsonEntry = jsonAdDetails.getJSONObject(i);
                String title = jsonEntry.getString("desc");
                if (title.equals("Description") || (checkAreasLookingIn && title.equals("Areas looking in"))) {
                    String jsonDescription = jsonEntry.getString("content");
                    sheet.add(new Pair<>(title, jsonDescription));
                } else {
                    JSONArray jsonDetailsBlock = jsonEntry.getJSONArray("content");

                    Vector<Pair<String, String>> vPairs = new Vector<>();

                    for (int j = 0; j < jsonDetailsBlock.length(); j++) {
                        JSONObject jsonPair = jsonDetailsBlock.getJSONObject(j);
                        JSONArray jsonNames = jsonPair.names();
                        for (int k = 0; k < jsonNames.length(); k++) {
                            String name = jsonNames.getString(k);
                            String value = jsonPair.getString(name);
                            if (!value.equals("null"))
                                vPairs.add(new Pair<>(name, value));
                        }
                    }
                    if (vPairs.size() > 0)
                        sheet.add(new Pair<>(title, vPairs));
                }
            }
            return sheet;

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

    }
}
