package com.spareroom.integration.webservice.rest.service;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SearchType;

import javax.inject.Inject;

public class AdRestService extends RestService {
    private static final String MANAGE_OFFERED_AD = "/flatshare/offered-advert3.pl";
    public static final String SEARCH_OFFERED_AD = "/flatshares/";
    private static final String MANAGE_WANTED_AD = "/flatshare/wanted_listing_step2.pl";
    public static final String SEARCH_WANTED_AD = "/flatmates/";

    @Inject
    public AdRestService() {

    }

    public String placeAd(String[] names, String[] values, SearchType searchType) throws NetworkConnectivityException, ClientErrorException, ServerErrorException {
        return post(getManageUri(searchType), names, values);
    }

    public String getDraftAdvert(Parameters parameters, SearchType searchType) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(getManageUri(searchType), parameters);
    }

    public String getAdDetails(String resource, Parameters parameters, SearchType searchType) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(getSearchUri(searchType), resource, parameters);
    }

    public String getAdverts(Parameters parameters, SearchType searchType) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(getSearchUri(searchType), parameters);
    }

    public String getSingleAdvert(String uri, Parameters parameters, SearchType searchType) throws Exception {
        return get(getSearchUri(searchType) + uri, parameters);
    }

    private String getSearchUri(SearchType searchType) {
        return searchType == SearchType.OFFERED ? SEARCH_OFFERED_AD : SEARCH_WANTED_AD;
    }

    private String getManageUri(SearchType searchType) {
        return searchType == SearchType.OFFERED ? MANAGE_OFFERED_AD : MANAGE_WANTED_AD;
    }

}
