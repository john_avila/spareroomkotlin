package com.spareroom.integration.imageloader;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.spareroom.R;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.DrawableRes;
import androidx.annotation.WorkerThread;
import androidx.fragment.app.Fragment;

@Singleton
public class ImageLoader {

    private final Context context;

    @Inject
    public ImageLoader(Application context) {
        this.context = context;
    }

    public void loadImage(String url, ImageView imageView) {
        Glide.with(imageView.getContext()).load(url).into(imageView);
    }

    public void loadImage(String url, ImageView imageView, Fragment fragment) {
        Glide.with(fragment).load(url).fitCenter().into(imageView);
    }

    public void loadImage(@DrawableRes int drawableRes, ImageView imageView, Fragment fragment) {
        Glide.with(fragment).load(drawableRes).fitCenter().into(imageView);
    }

    public void loadImage(String url, ImageView imageView, @DrawableRes int placeholder) {
        Glide.with(imageView.getContext()).load(url).placeholder(placeholder).error(placeholder).into(imageView);
    }

    public void loadImage(String url, ImageView imageView, ImageLoadingListener listener, Fragment fragment) {
        setRequestIdTag(imageView, listener);
        Glide.with(fragment).load(url).listener(listener.getRequestListener()).into(imageView);
    }

    public void loadImage(String url, ImageView imageView, ImageLoadingListener listener) {
        loadImage(url, imageView, listener, false);
    }

    public void loadImage(String url, ImageView imageView, ImageLoadingListener listener, boolean cacheOriginal) {
        setRequestIdTag(imageView, listener);
        DrawableRequestBuilder<String> request = Glide.with(imageView.getContext()).load(url).listener(listener.getRequestListener());
        if (cacheOriginal)
            request.diskCacheStrategy(DiskCacheStrategy.SOURCE);

        request.into(imageView);
    }

    public void loadCenterCropImageFromFragment(String url, ImageView imageView, ImageLoadingListener listener, Fragment fragment) {
        loadCenterCropImageFromFragment(url, imageView, listener, fragment, false);
    }

    public void loadCenterCropImageFromFragment(String url, ImageView imageView, ImageLoadingListener listener, Fragment fragment, boolean cacheOriginal) {
        DrawableRequestBuilder<String> requestBuilder = Glide.with(fragment).load(url).centerCrop();
        if (cacheOriginal)
            requestBuilder.diskCacheStrategy(DiskCacheStrategy.SOURCE);

        if (listener != null) {
            setRequestIdTag(imageView, listener);
            requestBuilder.listener(listener.getRequestListener());
        }

        requestBuilder.into(imageView);
    }

    public void loadCenterCropImage(String url, ImageView imageView, ImageLoadingListener listener, boolean cacheOriginal) {
        setRequestIdTag(imageView, listener);
        DrawableRequestBuilder<String> request = Glide.with(imageView.getContext()).load(url).centerCrop().listener(listener.getRequestListener());
        if (cacheOriginal)
            request.diskCacheStrategy(DiskCacheStrategy.SOURCE);

        request.into(imageView);
    }

    public void loadCircularImage(String url, ImageView imageView) {
        Glide.with(imageView.getContext()).load(url).fitCenter().transform(new CropCircleTransformation(imageView.getContext())).into(imageView);
    }

    public void loadCircularImage(String url, ImageView imageView, Fragment fragment) {
        loadCircularImage(url, imageView, null, fragment);
    }

    public void loadCircularImage(String url, ImageView imageView, ImageLoadingListener listener, Fragment fragment) {
        DrawableRequestBuilder<String> requestBuilder = Glide.with(fragment).load(url).fitCenter().transform(new CropCircleTransformation(imageView.getContext()));

        if (listener != null) {
            setRequestIdTag(imageView, listener);
            requestBuilder.listener(listener.getRequestListener());
        }
        requestBuilder.into(imageView);
    }

    public void clear(View view) {
        Glide.clear(view);
    }

    @WorkerThread
    public Bitmap getBitmap(String url) {
        try {
            return Glide.with(context).load(url).asBitmap().into(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).get();
        } catch (Exception e) {
            return null;
        }
    }

    private void setRequestIdTag(ImageView imageView, ImageLoadingListener listener) {
        imageView.setTag(R.id.request_id_tag, listener.getRequestId());
    }
}
