package com.spareroom.integration.business.apiv2;

import com.spareroom.integration.exception.*;
import com.spareroom.model.business.*;

public interface IAdvertListDao {

    SearchAdvertListResponse getAdvertList(SearchAdvertListProperties searchAdvertListProperties, SearchType searchType, boolean useCoordinates)
            throws UnexpectedResultException, UnavailableDataSourceException, InvalidArgumentException;

}
