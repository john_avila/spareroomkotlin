package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.Video;
import com.spareroom.model.business.VideoGallery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

public class VideoGalleryJSONParser {
    /**
     * Parses the response of an auto-reply request
     *
     * @param response String containing the response
     * @return SpareroomStatus stating whether the response was successful or not
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public VideoGallery parse(String response) throws InvalidJSONFormatException {

        try {
            JSONObject firstNode = new JSONObject(response);
            VideoGallery gallery = new VideoGallery();
            LinkedList<Video> lVideos = new LinkedList<>();

            if (firstNode.has("videos")) {
                JSONArray jsonArray = firstNode.getJSONArray("videos");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Video video = new Video();
                    String s = jsonObject.getString("status");
                    Video.Status status = null;

                    video.setCaption(jsonObject.getString("caption"));
                    if (video.getCaption() != null && video.getCaption().equals("null")) {
                        video.setCaption(null);
                    }

                    if (s.equals(Video.Status.LIVE.toString()))
                        status = Video.Status.LIVE;
                    if (s.equals(Video.Status.PENDING.toString()))
                        status = Video.Status.PENDING;
                    if (s.equals(Video.Status.REJECTED.toString()))
                        status = Video.Status.REJECTED;

                    video.setStatus(status);
                    video.setId(jsonObject.getString("video_id"));

                    lVideos.add(video);
                }

            }

            gallery.setGallery(lVideos);

            return gallery;

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

    }

}
