package com.spareroom.integration.exception;

public class InvalidArgumentException extends Exception {

    private static final long serialVersionUID = -7680127637031287215L;

    public InvalidArgumentException() {
        super();
    }

    public InvalidArgumentException(String detailMessage) {
        super(detailMessage);
    }

    public InvalidArgumentException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public InvalidArgumentException(Throwable throwable) {
        super(throwable);
    }

}

