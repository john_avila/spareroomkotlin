package com.spareroom.integration.business.legacy.rest;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InterruptedCommunicationException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.lib.core.ILongTaskObserver;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SpareroomStatus;

/**
 * Interface for the Video DAO.
 * <p>
 * Created by miguel.rossi on 25/08/2016.
 */
public interface IVideoDAO {

    Object create(Parameters parameters, ILongTaskObserver progressObserver)
            throws NetworkConnectivityException, InvalidJSONFormatException,
            AuthenticationException, ClientErrorException, ServerErrorException,
            InterruptedCommunicationException;

    SpareroomStatus delete(Parameters params) throws NetworkConnectivityException,
            InvalidJSONFormatException, AuthenticationException, ClientErrorException,
            ServerErrorException;

}
