package com.spareroom.integration.webservice.exception;

public class NetworkConnectivityException extends Exception {

    private static final long serialVersionUID = -5464531808307031867L;

    public NetworkConnectivityException() {
        super();
    }

    public NetworkConnectivityException(String detailMessage) {
        super(detailMessage);
    }

    public NetworkConnectivityException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public NetworkConnectivityException(Throwable throwable) {
        super(throwable);
    }

}
