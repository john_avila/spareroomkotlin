package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IClockFeaturedDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.MarkAdRestService;
import com.spareroom.model.business.Parameters;

public class ClockFeaturedDAO implements IClockFeaturedDAO {

    private final MarkAdRestService markAdRestService;

    ClockFeaturedDAO() {
        markAdRestService = new MarkAdRestService();
    }

    public boolean create(Parameters parameters) throws AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException {

        parameters.add("format", "json");

        String response = markAdRestService.reportAsViewed(parameters);

        return response.equals("1");

    }
}
