package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IMyWantedAdDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.MyAdStatsJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.UserAdsRestService;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.Stats;

public class MyWantedAdDAO implements IMyWantedAdDAO {

    private final UserAdsRestService userAdsRestService;

    MyWantedAdDAO() {
        userAdsRestService = new UserAdsRestService();
    }

    @Override
    public Stats read(Parameters parameters)
            throws NetworkConnectivityException, InvalidJSONFormatException,
            AuthenticationException, ClientErrorException, ServerErrorException {
        parameters.add("format", "json");
        parameters.add("flatshare_id", parameters.get("advert_id"));
        parameters.add("flatshare_type", "wanted");

        String response = userAdsRestService.getWantedAd(parameters);

        MyAdStatsJSONParser parser = new MyAdStatsJSONParser();

        return parser.parse(response);
    }
}
