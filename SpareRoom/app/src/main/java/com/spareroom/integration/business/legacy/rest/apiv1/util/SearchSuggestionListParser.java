package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.SearchAutocomplete;
import com.spareroom.model.business.SearchAutocompleteList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SearchSuggestionListParser {

    public SearchAutocompleteList parse(String json) throws InvalidJSONFormatException {
        try {
            JSONObject firstNode = new JSONObject(json);

            JSONArray listJsonArray = firstNode.getJSONArray("list");

            SearchAutocompleteList listSearchSuggestion = new SearchAutocompleteList();
            for (int i = 0; i < listJsonArray.length(); i++) {
                JSONObject searchSuggestion = listJsonArray.getJSONObject(i);
                SearchAutocomplete suggestion = new SearchAutocomplete();
                suggestion.set_suggestion(searchSuggestion.getString("search_string"));
                listSearchSuggestion.add(suggestion);
            }
            return listSearchSuggestion;

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }
    }
}
