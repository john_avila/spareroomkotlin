package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IVideoGalleryDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.VideoGalleryJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.VideoRestService;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.VideoGallery;

/**
 * DAO for the video gallery
 * <p>
 * Created by miguel.rossi on 25/08/2016.
 */
public class VideoGalleryDAO implements IVideoGalleryDAO {

    private final VideoRestService videoRestService;

    VideoGalleryDAO() {
        videoRestService = new VideoRestService();
    }

    @Override
    public VideoGallery read(Parameters parameters) throws
            NetworkConnectivityException,
            InvalidJSONFormatException,
            AuthenticationException,
            ClientErrorException,
            ServerErrorException {

        VideoGalleryJSONParser parser = new VideoGalleryJSONParser();

        parameters.add("format", "json");
        parameters.add("function", "get_videos");

        String response = videoRestService.getVideos(parameters);

        return parser.parse(response);
    }

}
