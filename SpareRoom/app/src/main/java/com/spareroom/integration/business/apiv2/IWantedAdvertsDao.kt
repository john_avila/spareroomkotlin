package com.spareroom.integration.business.apiv2

import com.spareroom.model.business.*

interface IWantedAdvertsDao : IAdvertsDao<AdWanted> {

    @Throws(Exception::class)
    override fun getUserAdverts(userId: String, page: Int, maxPerPage: Int): AdvertsListRestResponse<AdWanted>

    @Throws(Exception::class)
    override fun getAdvert(advertId: String): SingleAdvertRestResponse<AdWanted>

    @Throws(Exception::class)
    override fun getSavedAdverts(offset: Int, maxPerPage: Int): AdvertsListRestResponse<AdWanted>
}