package com.spareroom.integration.webservice.rest.service;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.Parameters;

import java.net.HttpCookie;
import java.util.List;

public class LoginRestService extends RestService {

    private static final String LOGIN_URI = "/flatshare/logon.pl";
    private static final String FACEBOOK_REGISTER = "/flatshare/facebook_connect.pl";
    private static final String REGISTER_ACCOUNT = "/flatshare/reg.pl";
    private static final String LOGOUT = "/flatshare/logout.pl";
    private static final String REGISTER_APP = "/app_checkin.pl";
    private static final String SESSION = "/session_info.pl";
    private static final String RESET_PASSWORD = "/flatshare/passwordreminder.pl";

    public String login(String[] names, String[] values) throws NetworkConnectivityException, ClientErrorException, ServerErrorException {
        return post(LOGIN_URI, names, values);
    }

    public String connectToFacebook(String[] names, String[] values) throws NetworkConnectivityException, ClientErrorException, ServerErrorException {
        return post(FACEBOOK_REGISTER, names, values);
    }

    public String registerAccount(String[] names, String[] values) throws NetworkConnectivityException, ClientErrorException, ServerErrorException {
        return post(REGISTER_ACCOUNT, names, values);
    }

    public String logOut(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(LOGOUT, parameters);
    }

    public void registerApp(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        get(REGISTER_APP, parameters);
    }

    public String getSession(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(SESSION, parameters);
    }

    public String resetPassword(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(RESET_PASSWORD, parameters);
    }

    public List<HttpCookie> getCookies() {
        return getCookieStoreCookies();
    }

    public void setCookies(List<HttpCookie> cookies) {
        setCookieStoreCookies(cookies);
    }

}
