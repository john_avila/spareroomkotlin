package com.spareroom.integration.webservice.rest.service;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.Parameters;

public class PictureRestService extends RestService {

    private static final String EXTENSION_JPG = "jpg";
    private static final String MANAGE_PHOTOS = "/flatshare/upload-photos.pl";

    public String uploadPicture(String[] names, String[] values, String[] namesMultipart, Object[] valuesMultipart) throws NetworkConnectivityException, ClientErrorException, ServerErrorException {
        return postMultipart(MANAGE_PHOTOS, names, values, namesMultipart, valuesMultipart, EXTENSION_JPG);
    }

    public String updatePicture(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(MANAGE_PHOTOS, parameters);
    }

    public String deletePicture(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(MANAGE_PHOTOS, parameters);
    }

    public String getPictures(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(MANAGE_PHOTOS, parameters);
    }

}
