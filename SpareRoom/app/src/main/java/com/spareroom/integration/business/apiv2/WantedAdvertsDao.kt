package com.spareroom.integration.business.apiv2

import com.google.gson.reflect.TypeToken
import com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum
import com.spareroom.integration.json.JsonParser
import com.spareroom.integration.webservice.rest.service.AdRestService
import com.spareroom.model.business.*
import javax.inject.Inject

class WantedAdvertsDao @Inject constructor(
    private val adRestService: AdRestService,
    private val parser: JsonParser
) : IWantedAdvertsDao {

    @Throws(Exception::class)
    override fun getUserAdverts(userId: String, page: Int, maxPerPage: Int): AdvertsListRestResponse<AdWanted> {
        val parameters = Parameters()
        parameters.add(SearchParamEnum.USER_ID, userId)
        parameters.add(SearchParamEnum.PAGE, page)
        parameters.add(SearchParamEnum.MAX_PER_PAGE, maxPerPage)
        parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON)

        val response = adRestService.getAdverts(parameters, SearchType.WANTED)
        return parser.fromJson(response, object : TypeToken<AdvertsListRestResponse<AdWanted>>() {}.type)
    }

    @Throws(Exception::class)
    override fun getAdvert(advertId: String): SingleAdvertRestResponse<AdWanted> {
        val parameters = Parameters()
        parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON)

        val response = adRestService.getSingleAdvert(advertId, parameters, SearchType.WANTED)
        return parser.fromJson(response, object : TypeToken<SingleAdvertRestResponse<AdWanted>>() {}.type)
    }

    @Throws(Exception::class)
    override fun getSavedAdverts(offset: Int, maxPerPage: Int): AdvertsListRestResponse<AdWanted> {
        val parameters = Parameters()
        parameters.add(SearchParamEnum.FILTER, SearchParamEnum.SHORTLIST)
        parameters.add(SearchParamEnum.HIDE_UNSUITABLE, ApiParams.YES_INT)
        parameters.add(SearchParamEnum.OFFSET, offset)
        parameters.add(SearchParamEnum.MAX_PER_PAGE, maxPerPage)
        parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON)

        val response = adRestService.getAdverts(parameters, SearchType.WANTED)
        return parser.fromJson(response, object : TypeToken<AdvertsListRestResponse<AdWanted>>() {}.type)
    }

}