package com.spareroom.integration.webservice.rest.service;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.Parameters;

public class MarkAdRestService extends RestService {

    private static final String SHORTLIST = "/flatshare/shortlist.pl";
    private static final String ADVERT_DETAIL = "/flatshare/advert_detail.pl";
    private static final String FEATURED_AD = "/flatshare/fad_click.pl";

    public String markAsContacted(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(ADVERT_DETAIL, parameters);
    }

    public String reportAsViewed(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(FEATURED_AD, parameters);
    }

    public String markAsFavourite(Parameters parameters) throws NetworkConnectivityException, ClientErrorException, ServerErrorException, AuthenticationException {
        return get(SHORTLIST, parameters);
    }

}
