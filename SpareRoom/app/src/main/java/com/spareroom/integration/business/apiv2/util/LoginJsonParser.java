package com.spareroom.integration.business.apiv2.util;

import com.spareroom.integration.business.legacy.rest.apiv1.util.SessionJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.SpareroomStatus;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginJsonParser {
    /**
     * Parses the response of loggin in
     *
     * @param json String containing the response after carrying out the operation
     * @return User that has been logged in, SpareroomStatus stating the error occurred otherwise
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public Object parse(String json) throws InvalidJSONFormatException {

        if (json == null) // this should not happen
            throw new InvalidJSONFormatException();

        try {
            JSONObject firstNode = new JSONObject(json);
            String response_type = firstNode.getString("response_type");

            if (response_type.equals("success")) {
                String jsonSession = firstNode.getString("session");
                SessionJSONParser sessionParser = new SessionJSONParser();

                return sessionParser.parse(jsonSession);

            } else if (response_type.equals("error")) {
                SpareroomStatus e = new SpareroomStatus();
                e.setCode(firstNode.getString("response_id"));
                e.setMessage(firstNode.getString("response"));
                return e;

            }

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();

        }

        throw new InvalidJSONFormatException();

    }

}
