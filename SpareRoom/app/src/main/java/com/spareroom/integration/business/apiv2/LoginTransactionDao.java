package com.spareroom.integration.business.apiv2;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.SpareroomAbstractDaoImpl;
import com.spareroom.integration.business.apiv2.util.LoginJsonParser;
import com.spareroom.integration.business.legacy.rest.apiv1.security.Authenticator;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.*;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.service.LoginRestService;
import com.spareroom.model.business.*;
import com.spareroom.model.extra.LoginExtra;

/**
 * DAO for the LoginTransaction Business Object (does not exists on the app)
 */
class LoginTransactionDao extends SpareroomAbstractDaoImpl<AccountCredentials, LoginExtra, AccountConnectionInterface> {
    private static final String REMEMBER_ME_YES = "Y";

    private final LoginRestService loginRestService;

    LoginTransactionDao(SpareroomContext spareroomContext) {
        super(spareroomContext);
        loginRestService = new LoginRestService();
    }

    /**
     * @return <code>Session</code> session if the log in was successful <code>SpareroomStatus</code> if there was a problem
     */
    @Override
    public Object create(AccountCredentials businessObject, LoginExtra extra) throws
            UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {

        Parameters parameters = new Parameters();
        Authenticator authenticator = new Authenticator();
        String response;

        try {
            parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON);
            parameters.add(LoginParamEnum.REMEMBER_ME, REMEMBER_ME_YES);
            if (businessObject.getUsername() != null)
                parameters.add(LoginParamEnum.EMAIL, businessObject.getUsername());
            if (businessObject.getPassword() != null)
                parameters.add(LoginParamEnum.PASSWORD, businessObject.getPassword());
            if (extra.getPageJoinedFrom() != null)
                parameters.add(LoginParamEnum.PAGE_JOINED_FROM, extra.getPageJoinedFrom());
            // if my_version is missing, the server simply defaults it to 1.0 This is not a critical error
            if (extra.getVersion() != null)
                parameters.add(LoginParamEnum.MY_VERSION, extra.getVersion());
            if (authenticator.authenticate(parameters) != null)
                parameters.add(ApiParams.API_SIGNATURE, authenticator.authenticate(parameters));
            if (authenticator.get_apiKey() != null)
                parameters.add(ApiParams.API_KEY, authenticator.get_apiKey());

            String[] vParametersName = parameters.getParameterNames();
            String[] vName = new String[vParametersName.length];
            String[] vValue = new String[vParametersName.length];
            for (int i = 0; i < vParametersName.length; i++) {
                vName[i] = vParametersName[i];
                vValue[i] = parameters.get(vParametersName[i]);
            }

            response = loginRestService.login(vName, vValue);

            return (new LoginJsonParser()).parse(response);

        } catch (NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e);
        } catch (ClientErrorException | AuthenticationException e) {
            throw new InvalidArgumentException(e);
        } catch (ServerErrorException | InvalidJSONFormatException e) {
            throw new UnexpectedResultException(e);
        }
    }

}
