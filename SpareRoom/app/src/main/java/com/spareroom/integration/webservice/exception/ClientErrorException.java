package com.spareroom.integration.webservice.exception;

public class ClientErrorException extends Exception {

    public ClientErrorException() {
        super();
    }

    public ClientErrorException(String detailMessage) {
        super(detailMessage);
    }

    public ClientErrorException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ClientErrorException(Throwable throwable) {
        super(throwable);
    }

}
