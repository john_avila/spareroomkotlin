package com.spareroom.integration.business.apiv2

import com.spareroom.integration.json.JsonParser
import com.spareroom.integration.webservice.rest.service.LocationRestService
import com.spareroom.model.business.Parameters
import com.spareroom.model.business.PostCodeRestResponse
import javax.inject.Inject

private const val LATITUDE = "latitude"
private const val LONGITUDE = "longitude"

class LocationDao @Inject constructor(
    private val service: LocationRestService,
    private val parser: JsonParser
) : ILocationDao {

    @Throws(Exception::class)
    override fun getPostCode(latitude: Double, longitude: Double): PostCodeRestResponse {
        val parameters = Parameters()
        parameters.apply {
            add(LATITUDE, latitude)
            add(LONGITUDE, longitude)
        }

        val response = service.getPostcodeFromCoordinates(parameters)
        return parser.fromJson(response, PostCodeRestResponse::class.java)
    }

}
