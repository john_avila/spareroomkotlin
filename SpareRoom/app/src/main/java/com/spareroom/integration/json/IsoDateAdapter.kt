package com.spareroom.integration.json

import com.google.gson.*
import com.spareroom.model.business.date.IsoDate
import com.spareroom.ui.util.DateUtils
import java.lang.reflect.Type
import javax.inject.Inject

class IsoDateAdapter @Inject constructor(private val dateUtils: DateUtils) : JsonDeserializer<IsoDate> {

    override fun deserialize(json: JsonElement?, typeOfT: Type, context: JsonDeserializationContext): IsoDate {
        return if (json == null || json.isJsonNull) IsoDate() else IsoDate(dateUtils.parseIsoDate(json.asString))
    }
}