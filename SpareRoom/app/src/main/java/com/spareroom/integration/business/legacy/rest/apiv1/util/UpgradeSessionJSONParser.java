package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.Session;

import org.json.JSONObject;

import javax.inject.Inject;

public class UpgradeSessionJSONParser {

    @Inject
    public UpgradeSessionJSONParser() {

    }

    public Session parse(String json) throws Exception {
        JSONObject jsonObject = new JSONObject(json);
        if (!jsonObject.has("session"))
            throw new InvalidJSONFormatException();

        String jsonSession = jsonObject.getString("session");
        SessionJSONParser sessionParser = new SessionJSONParser();
        return sessionParser.parse(jsonSession);
    }
}