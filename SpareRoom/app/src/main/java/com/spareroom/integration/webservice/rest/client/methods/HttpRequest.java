package com.spareroom.integration.webservice.rest.client.methods;

import android.app.Application;
import android.os.Build;

import com.google.android.gms.security.ProviderInstaller;
import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.business.legacy.rest.apiv1.util.UpgradeSessionJSONParser;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.client.Tls12SocketFactory;
import com.spareroom.integration.webservice.rest.service.FirebaseRestService;
import com.spareroom.model.business.Session;
import com.spareroom.model.business.SpareroomStatusCode;
import com.spareroom.ui.util.ConnectivityChecker;
import com.spareroom.ui.util.FileUtils;

import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.SocketTimeoutException;
import java.security.KeyStore;
import java.util.*;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.*;

import androidx.annotation.VisibleForTesting;
import okhttp3.*;

public abstract class HttpRequest {

    public static final String USER_AGENT_NAMED_PROVIDER = "user_agent_named_provider";

    public static final String TLS_12 = "TLSv1.2";

    static final String HEADER_CHARSET = "Charset";
    static final String HEADER_USER_AGENT = "User-Agent";
    static final String HEADER_CONNECTION = "Connection";

    static final String CHARSET_UTF8 = "UTF-8";
    static final String CONNECTION_KEEP_ALIVE = "Keep-Alive";

    private static final String QUERY_PARAM_NAME_ACTION = "action";
    private static final String QUERY_PARAM_VALUE_UNREGISTER = "unregister";

    static final MediaType MEDIA_TYPE_FORM_URL_ENCODED = MediaType.parse("application/x-www-form-urlencoded");
    static final MediaType MEDIA_TYPE_ALL = MediaType.parse("*/*");

    private static final int TIMEOUT = 30;

    private final String url;

    private final SpareroomContext spareroomContext;
    private final UpgradeSessionJSONParser sessionParser;
    private final OkHttpClient okHttpClient;
    private final String userAgent;
    private final ConnectivityChecker connectivityChecker;
    private final Application application;
    private final AnalyticsTrackerComposite analyticsTracker;

    HttpRequest(SpareroomContext spareroomContext, UpgradeSessionJSONParser sessionParser, OkHttpClient okHttpClient,
                String userAgent, ConnectivityChecker connectivityChecker, Application application, String url) {
        this.spareroomContext = spareroomContext;
        this.sessionParser = sessionParser;
        this.okHttpClient = okHttpClient;
        this.userAgent = userAgent;
        this.connectivityChecker = connectivityChecker;
        this.application = application;
        this.url = url;
        this.analyticsTracker = AnalyticsTrackerComposite.getInstance();
    }

    protected abstract Request getRequest() throws Exception;

    public String execute() throws NetworkConnectivityException, ClientErrorException, ServerErrorException {

        checkNetworkConnection();

        Response response = null;
        ResponseBody responseBody = null;
        try {
            updateSecurityProvider();

            Request request = getRequest();
            response = execute(request);

            checkResponseCode(response);
            responseBody = response.body();
            if (responseBody != null) {
                String responseAsString = responseBody.string();
                updateSession(responseAsString, request);

                return responseAsString;
            }

            throw new ServerErrorException();

        } catch (SSLHandshakeException exception) {
            throw new ClientErrorException(exception.getMessage());
        } catch (SocketTimeoutException exception) {
            throw new ServerErrorException(exception.getMessage());
        } catch (ClientErrorException | ServerErrorException exception) {
            throw exception;
        } catch (Exception exception) {
            throw new NetworkConnectivityException(exception.getMessage());
        } finally {
            if (responseBody != null)
                FileUtils.closeResource(response);
        }
    }

    String getUrl() {
        return url;
    }

    @VisibleForTesting
    CookieJar getCookieJar() {
        return new JavaNetCookieJar(CookieHandler.getDefault());
    }

    @VisibleForTesting
    void checkNetworkConnection() throws NetworkConnectivityException {
        if (!connectivityChecker.isConnected()) {
            final NetworkConnectivityException noConnectionException = new NetworkConnectivityException("No connection");
            analyticsTracker.logHandledException("Performing network call when offline", noConnectionException);
            throw noConnectionException;
        }
    }

    String getUserAgent() {
        return userAgent;
    }

    @VisibleForTesting
    void updateSession(String response, Request request) {
        try {
            saveSession(response, request);
        } catch (Exception e) {
            deleteSessionIfUserLoggedOut(response);
        }
    }

    private Response execute(Request request) throws Exception {
        try {
            return newClient().newCall(request).execute();
        } catch (Exception exception) {
            checkNetworkConnectionAfterFailure(exception);

            analyticsTracker.logHandledException("Performing network call", exception);
            throw exception;
        }
    }

    private void checkResponseCode(Response response) throws ClientErrorException, ServerErrorException {
        if (response.isSuccessful())
            return;

        int responseCode = response.code();
        if (responseCode >= 400 && responseCode < 500)
            throw new ClientErrorException(Integer.toString(responseCode));

        if (responseCode >= 500 && responseCode < 600)
            throw new ServerErrorException(Integer.toString(responseCode));
    }

    private void updateSecurityProvider() {
        try {
            ProviderInstaller.installIfNeeded(application);
        } catch (Exception e) {
            analyticsTracker.logHandledException("Installing security provider", e);
        }
    }

    @VisibleForTesting
    void checkNetworkConnectionAfterFailure(Exception exception) throws Exception {
        if (!connectivityChecker.isConnected()) {
            final NetworkConnectivityException noConnectionException = new NetworkConnectivityException("No connection in execute", exception);
            analyticsTracker.logHandledException("Network lost while performing network call", noConnectionException);
            throw noConnectionException;
        }
    }

    @VisibleForTesting
    OkHttpClient newClient() {
        OkHttpClient.Builder builder = okHttpClient.newBuilder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .followRedirects(true)
                .followSslRedirects(true)
                .cookieJar(getCookieJar());

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1)
            enableTls12(builder);

        return builder.build();
    }

    @VisibleForTesting
    void enableTls12(OkHttpClient.Builder builder) {
        try {
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init((KeyStore) null);
            TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
            if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager))
                throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));

            X509TrustManager trustManager = (X509TrustManager) trustManagers[0];

            SSLContext sslContext = SSLContext.getInstance(TLS_12);
            sslContext.init(null, null, null);
            builder.sslSocketFactory(new Tls12SocketFactory(sslContext.getSocketFactory()), trustManager);

            ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                    .tlsVersions(TlsVersion.TLS_1_2)
                    .build();

            List<ConnectionSpec> specs = new ArrayList<>();
            specs.add(cs);
            specs.add(ConnectionSpec.COMPATIBLE_TLS);
            specs.add(ConnectionSpec.CLEARTEXT);

            builder.connectionSpecs(specs);
        } catch (Exception e) {
            analyticsTracker.logHandledException("Setting SSLSocketFactory on pre-lollipop mr1 device", e);
        }
    }

    @VisibleForTesting
    void deleteSessionIfUserLoggedOut(String response) {
        try {
            JSONObject jsonResponse = new JSONObject(response);
            String userLoggedOutErrorCode = SpareroomStatusCode.NOT_LOGGED_IN;
            if (userLoggedOutErrorCode.equals(jsonResponse.optString(SpareroomStatusCode.CODE_ERROR_ID)) ||
                    userLoggedOutErrorCode.equals(jsonResponse.optString(SpareroomStatusCode.CODE_RESPONSE_ID)))
                spareroomContext.deleteUserSession();
        } catch (Exception e) {
            // do not throw, give callers chance to handle response anyway
        }

    }

    @VisibleForTesting
    void saveSession(String response, Request request) throws Exception {
        Session session = sessionParser.parse(response);
        if (session != null && !isUnregisteringFirebaseToken(request))
            spareroomContext.saveUserSession(session);
    }

    @VisibleForTesting
    boolean isUnregisteringFirebaseToken(Request request) {
        HttpUrl url = request.url();
        HttpUrl baseUrl = HttpUrl.parse(AppVersion.flavor().getDomain());
        return (baseUrl != null && baseUrl.host().equalsIgnoreCase(url.host()))
                && FirebaseRestService.FIREBASE_TOKEN.equalsIgnoreCase(url.encodedPath())
                && QUERY_PARAM_VALUE_UNREGISTER.equalsIgnoreCase(url.queryParameter(QUERY_PARAM_NAME_ACTION));
    }
}
