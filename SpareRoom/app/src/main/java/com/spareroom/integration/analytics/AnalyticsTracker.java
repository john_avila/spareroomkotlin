package com.spareroom.integration.analytics;

import android.app.Activity;

import com.spareroom.integration.googleplay.billing.IabHelper;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.business.Upgrade;

import androidx.fragment.app.Fragment;

/**
 * Facade for an Analytics Tracker. Any tracker should provide the ability of tracking these actions
 * which are required by the business. The design decision of passing no arguments and creating as
 * many methods as events was taken in order to make easier to find code references and control of
 * the events logged.
 *
 * @author manuel
 */
public abstract class AnalyticsTracker {

    public void trackScreen_Activity(Activity activity) {
    }

    public void trackScreen_Fragment(Fragment fragment) {
    }

    /**
     * Tracks the event of messaging an advertiser for the category Advert
     */
    public void trackEvent_AdvertOffered_MessageAdvertiser() {
    }

    /**
     * Tracks the event of messaging an advertiser for the category Advert
     */
    public void trackEvent_AdvertWanted_MessageAdvertiser() {
    }

    /**
     * Tracks the event of calling an advertiser for the category Advert
     */
    public void trackEvent_AdvertOffered_PhoneAdvertiser() {
    }

    /**
     * Tracks the event of calling an advertiser for the category Advert
     */
    public void trackEvent_AdvertWanted_PhoneAdvertiser() {
    }

    /**
     * Tracks the event of reporting an offered advert
     */
    public void trackEvent_AdvertOffered_Report() {
    }

    /**
     * Tracks the event of reporting a wanted advert
     */
    public void trackEvent_AdvertWanted_Report() {
    }

    /**
     * Tracks the event of sharing an offered advert
     */
    public void trackEvent_AdvertOffered_Share() {
    }

    /**
     * Tracks the event of sharing a wanted advert
     */
    public void trackEvent_AdvertWanted_Share() {
    }

    /**
     * Tracks the event of choosing to search from the menu
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Menu_Search(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of choosing to messages from the menu
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Menu_Messages(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of choosing to view the user's own adverts from the menu
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Menu_MyAdverts(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of choosing to view the saved adverts from the menu
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Menu_SavedAds(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of choosing to view the saved searches from the menu
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Menu_SavedSearches(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of choosing to edit user's details from the menu
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Menu_EditMyDetails(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of choosing to view upgrades from the menu
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Menu_Upgrade(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of choosing to place an advert from the menu
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Menu_PlaceAdvert(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of having too many requests for registering Push notifications
     *
     * @param runs the number of times that tried to register
     */
    public void trackEvent_Push_TooManyRequests(Long runs) {
    }

    public void trackEvent_Menu_BetaProgramme() {
    }

    /**
     * Tracks the event of choosing to contact SpareRoom from the menu
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Menu_ContactUsPhone(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of choosing to contact SpareRoom from the menu
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Menu_ContactUsMail(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of choosing to log out from the menu
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Menu_LogOut(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of launching a saved search from the saved searches screen
     */
    public void trackEvent_SavedSearch_LaunchSavedSearch() {
    }

    /**
     * Tracks the event of deleting a saved search from the saved searches screen
     */
    public void trackEvent_SavedSearch_DeleteSavedSearch() {
    }

    /**
     * Tracks the event of choosing broaden the search with one of the options provided on the
     * Results screen
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Search_BroadenSearch(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of marking an offered advert as favourite
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Search_MarkFavouriteOffered(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of marking a wanted advert as favourite
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Search_MarkFavouriteWanted(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of removing the favourite mark from an offered advert
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Search_RemoveShortlistOffered(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of removing the favourite mark from a wanted advert
     *
     * @param isLoggedIn <code>true</code> if the user is logged in, <code>false</code> otherwise
     */
    public void trackEvent_Search_RemoveShortlistWanted(boolean isLoggedIn) {
    }

    /**
     * Tracks the event of launching a saved search from the search rooms screen
     */
    public void trackEvent_SearchRooms_LaunchSavedSearch() {
    }

    /**
     * Tracks the event of deleting a saved search from the search rooms screen
     */
    public void trackEvent_SearchRooms_DeleteSavedSearch() {
    }

    /**
     * Tracks the event of getting the user's location when searching for rooms
     */
    public void trackEvent_SearchRooms_GetLocation() {
    }

    /**
     * Tracks the event of launching a search manually from the search rooms screen
     */
    public void trackEvent_SearchRooms_ManualSearch() {
    }

    /**
     * Tracks the event of launching a search by choosing a suggested term (autocomplete) from the
     * search rooms screen
     */
    public void trackEvent_SearchRooms_AutocompleteSearch() {
    }

    /**
     * Tracks the event of launching a search from the history in the search rooms screen
     */
    public void trackEvent_SearchRooms_HistorySearch() {
    }

    /**
     * Tracks the event of launching an advert number search in the search rooms screen
     */
    public void trackEvent_SearchRooms_ManualAdvertSearch() {
    }

    /**
     * Tracks the event of launching a search by choosing suggested advert number search
     * (autocomplete) from the search rooms screen
     */
    public void trackEvent_SearchRooms_AutocompleteAdvertSearch() {
    }

    /**
     * Tracks the event of launching an advert number search from the history in the search rooms
     * screen
     */
    public void trackEvent_SearchRooms_HistoryAdvertSearch() {
    }

    /**
     * Tracks the event of getting the user's location when searching for flatmates
     */
    public void trackEvent_SearchFlatmates_GetLocation() {
    }

    /**
     * Tracks the event of launching a search manually from the search flatmates screen
     */
    public void trackEvent_SearchFlatmates_ManualSearch() {
    }

    /**
     * Tracks the event of launching a search by choosing a suggested term (autocomplete) from the
     * search flatmates screen
     */
    public void trackEvent_SearchFlatmates_AutocompleteSearch() {
    }

    /**
     * Tracks the event of launching a search from the history in the search rooms screen
     */
    public void trackEvent_SearchFlatmates_HistorySearch() {
    }

    /**
     * Tracks the event of launching an advert number search in the search advert screen
     */
    public void trackEvent_SearchFlatmates_ManualAdvertSearch() {
    }

    /**
     * Tracks the event of launching a search by choosing suggested advert number search
     * (autocomplete) from the search flatmates screen
     */
    public void trackEvent_SearchFlatmates_AutocompleteAdvertSearch() {
    }

    /**
     * Tracks the event of launching an advert number search from the history in the search
     * flatmates screen
     */
    public void trackEvent_SearchFlatmates_HistoryAdvertSearch() {
    }

    /**
     * Tracks the event of searching for a suggested area after the search has yielded no
     * results
     */
    public void trackEvent_Results_SuggestionAdvertSearch() {
    }

    /**
     * Tracks the event of dismissing the suggested areas after the search has yielded no
     * results
     */
    public void trackEvent_Results_CancelSuggestionAdvertSearch() {
    }

    /**
     * Tracks the event of sending a message
     *
     * @param status the resulting status after performing the operation
     */
    public void trackEvent_Message_Send(SpareroomStatus status) {
    }

    /**
     * Tracks the event of sending a message
     *
     * @param status the resulting status after performing the operation
     */
    public void trackEvent_Message_Autoreject(SpareroomStatus status) {
    }

    /**
     * Tracks the event of start placing an offered advert at the end of the place ad process
     */
    public void trackEvent_PlaceAd_StartPlaceOfferedAd() {
    }

    /**
     * Tracks the event of start placing a wanted advert at the end of the place ad process
     */
    public void trackEvent_PlaceAd_StartPlaceWantedAd() {
    }

    /**
     * Tracks the event of placing an offered advert at the end of the place ad process
     */
    public void trackEvent_PlaceAd_PlaceOfferedAd() {
    }

    /**
     * Tracks the event of placing a wanted advert at the end of the place ad process
     */
    public void trackEvent_PlaceAd_PlaceWantedAd() {
    }

    /**
     * Tracks the event of attempting to place an offered advert that contains errors at the end of
     * the place ad process (place advert rejected by the server)
     */
    public void trackEvent_PlaceAd_PlaceOfferedAdWithErrors() {
    }

    /**
     * Tracks the event of attempting to place a wanted advert that contains errors at the end of
     * the place ad process (place advert rejected by the server)
     */
    public void trackEvent_PlaceAd_PlaceWantedAdWithErrors() {
    }

    public void trackEvent_EditAd_ReadOfferedAd() {
    }

    public void trackEvent_EditAd_ReadWantedAd() {
    }

    public void trackEvent_EditAd_ReadOfferedAdWithErrors(SpareroomStatus status) {
    }

    public void trackEvent_EditAd_ReadWantedAdWithErrors(SpareroomStatus status) {
    }

    public void trackEvent_EditAd_EditOfferedAd() {
    }

    public void trackEvent_EditAd_EditWantedAd() {
    }

    public void trackEvent_EditAd_EditOfferedAdWithErrors(SpareroomStatus status) {
    }

    public void trackEvent_EditAd_EditWantedAdWithErrors(SpareroomStatus status) {
    }

    /**
     * Tracks the event of selecting an upgrade product form the upgrade screen
     *
     * @param u               the upgrade selected
     * @param contextId       context (source) that fired this upgrade process
     * @param productPosition position of the product on the list displayed to the user
     */
    public void trackEvent_Upgrade_SelectProduct(Upgrade u, int contextId, int productPosition) {
    }

    /**
     * Tracks the event of purchasing an upgrade from the upgrade screen
     *
     * @param u               the upgrade purchased
     * @param orderId         the order id for Google Play (obtained after finishing the purchase process)
     * @param productPosition position of the product on the list displayed to the user
     */
    public void trackEvent_Upgrade_Purchase(Upgrade u, String orderId, int productPosition) {
    }

    /**
     * Tracks the event of purchasing an extra listing
     *
     * @param u       the upgrade purchased
     * @param orderId the order id for Google Play (obtained after finishing the purchase process)
     */
    public void trackEvent_ExtraListing_Purchase(Upgrade u, String orderId) {
    }

    /**
     * Tracks the event of choosing to sort search results on the results screen (offered)
     */
    public void trackEvent_SearchRoomsMenu_SortOption() {
    }

    /**
     * Tracks the event of choosing to sort search results by default on the results screen
     * (offered)
     */
    public void trackEvent_SearchRooms_SortByDefault() {

    }

    /**
     * Tracks the event of choosing to sort search results by last updated on the results screen
     * (offered)
     */
    public void trackEvent_SearchRooms_SortByLastUpdated() {
    }

    /**
     * Tracks the event of choosing to sort search by newest results on the results screen (offered)
     */
    public void trackEvent_SearchRooms_SortByNewest() {
    }

    /**
     * Tracks the event of choosing to sort search results by most expensive on the results screen
     * (offered)
     */
    public void trackEvent_SearchRooms_SortByMostExpensive() {
    }

    /**
     * Tracks the event of choosing to sort search results by least expensive on the results screen
     * (offered)
     */
    public void trackEvent_SearchRooms_SortByLeastExpensive() {
    }

    /**
     * Tracks the event of choosing to sort search results on the results screen (wanted)
     */
    public void trackEvent_SearchFlatmatesMenu_SortOption() {
    }

    /**
     * Tracks the event of choosing to sort search results by last updated on the results screen
     * (wanted)
     */
    public void trackEvent_SearchFlatmates_SortByDefault() {
    }

    /**
     * Tracks the event of choosing to sort search results by last updated on the results screen
     * (wanted)
     */
    public void trackEvent_SearchFlatmates_SortByLastUpdated() {
    }

    /**
     * Tracks the event of choosing to sort search by newest results on the results screen (wanted)
     */
    public void trackEvent_SearchFlatmates_SortByNewest() {
    }

    /**
     * Tracks the event of choosing to sort search results by most expensive on the results screen
     * (wanted)
     */
    public void trackEvent_SearchFlatmates_SortByMostExpensive() {
    }

    /**
     * Tracks the event of choosing to sort search results by least expensive on the results screen
     * (wanted)
     */
    public void trackEvent_SearchFlatmates_SortByLeastExpensive() {
    }

    /**
     * Tracks the event of choosing the option of searching using the map view from the results
     * screen (only applicable to offered)
     */
    public void trackEvent_SearchRoomsMenu_Map() {
    }

    /**
     * Tracks the event of choosing to save a search from the results screen (offered)
     */
    public void trackEvent_SearchRoomsMenu_SaveSearch() {
    }

    /**
     * Tracks the event of choosing to refine the search from the results screen (offered)
     */
    public void trackEvent_SearchRoomsMenu_Refine() {
    }

    /**
     * Tracks the event of choosing to refine the search from the results screen (wanted)
     */
    public void trackEvent_SearchFlatmatesMenu_Refine() {
    }

    /**
     * Tracks the ecommerce event of selecting an upgrade product form the upgrade screen
     *
     * @param u               the upgrade selected
     * @param contextId       context (source) that fired this upgrade process
     * @param productPosition position of the product on the list displayed to the user
     */
    public void trackEcommerce_Upgrade_SelectProduct(Upgrade u, int contextId, int productPosition) {
    }

    /**
     * Tracks the ecommerce event of purchasing an upgrade from the upgrade screen
     *
     * @param u               the upgrade purchased
     * @param orderId         the order id for Google Play (obtained after finishing the purchase process)
     * @param productPosition position of the product on the list displayed to the user
     */
    public void trackEcommerce_Upgrade_Purchase(Upgrade u, String orderId, int productPosition) {
    }

    /**
     * Tracks the ecommerce event of purchasing an upgrade from the upgrade screen
     *
     * @param u        the upgrade purchased
     * @param response response code for the billing result as documented in constants
     *                 BILLING_RESPONSE* in
     *                 {@link IabHelper}
     * @param message  message from the billing result
     */
    public void trackEcommerce_Upgrade_Purchase_Fail(Upgrade u, int response, String message) {
    }

    /**
     * Tracks the ecommerce event of purchasing an extra listing
     *
     * @param u       the upgrade purchased
     * @param orderId the order id for Google Play (obtained after finishing the purchase process)
     */
    public void trackEcommerce_ExtraListing_Purchase(Upgrade u, String orderId) {
    }

    /**
     * Tracks the ecommerce event of purchasing an extra listing
     *
     * @param u        the upgrade purchased
     * @param response response code for the billing result as documented in constants
     *                 BILLING_RESPONSE* in
     *                 {@link IabHelper}
     * @param message  message from the billing result
     */
    public void trackEcommerce_ExtraListing_Purchase_Fail(Upgrade u, int response, String message) {
    }

    /**
     * Tracks unexpected billing issue when creating the billing helper for upgrades
     */
    public void trackEvent_Upgrade_BillingUnavailable_Create() {
    }

    /**
     * Tracks unexpected billing issue when disabling debug for upgrades
     */
    public void trackEvent_Upgrade_BillingUnavailable_DisableDebug() {
    }

    /**
     * Tracks unexpected billing issue when setting up for upgrades
     */
    public void trackEvent_Upgrade_BillingUnavailable_Setup() {
    }

    /**
     * Tracks unexpected billing issue when querying inventory for upgrades
     */
    public void trackEvent_Upgrade_BillingUnavailable_QueryInventory() {
    }

    /**
     * Tracks unexpected billing issue when launching the purchase flow for upgrades
     */
    public void trackEvent_Upgrade_BillingUnavailable_LaunchPurchaseFlow() {
    }

    /**
     * Tracks unexpected billing issue when handling the result of a payment process for upgrades
     */
    public void trackEvent_Upgrade_BillingUnavailable_HandlePaymentResult() {
    }

    /**
     * Tracks unexpected billing issue when consuming purchases for upgrades
     */
    public void trackEvent_Upgrade_BillingUnavailable_Consume() {
    }

    /**
     * Tracks unexpected billing issue when creating the billing helper for extra listings
     */
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_Create() {
    }

    /**
     * Tracks unexpected billing issue when disabling debug for extra listings
     */
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_DisableDebug() {
    }

    /**
     * Tracks unexpected billing issue when setting up for extra listings
     */
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_Setup() {
    }

    /**
     * Tracks unexpected billing issue when querying inventory for extra listings
     */
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_QueryInventory() {
    }

    /**
     * Tracks unexpected billing issue when launching the purchase flow for extra listings
     */
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_LaunchPurchaseFlow() {
    }

    /**
     * Tracks unexpected billing issue when handling the result of a payment process for extra
     * listings
     */
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_HandlePaymentResult() {
    }

    /**
     * Tracks unexpected billing issue when consuming purchases for extra listings
     */
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_Consume() {
    }

    /**
     * Tracks a successful event of registering with Facebook
     *
     * @param userType the type of user. Valid values are "Offerer", "Seeker", "Offerer,Seeker".
     *                 Any other value will track the event without this information.
     */
    public void trackEvent_Account_Register_Facebook(String userType) {
    }

    /**
     * Tracks a successful event of registering with Spareroom
     *
     * @param userType the type of user. Valid values are "Offerer", "Seeker", "Offerer,Seeker".
     *                 Any other value will track the event without this information.
     */
    public void trackEvent_Account_Register_Spareroom(String userType) {
    }

    /**
     * Tracks a Facebook redirection from login to register
     */
    public void trackEvent_Account_FacebookLoginToRegisterRedirection() {
    }

    /**
     * Tracks a Facebook redirection from login to register
     */
    public void trackEvent_Account_FacebookRegisterToLoginRedirection() {
    }

    /**
     * Tracks a failed event of registering with Facebook
     *
     * @param failedRegistration the result of failing to register the account. If null or no code
     *                           is provided, the events get reported as an undefined error.
     */
    public void trackEvent_Account_Register_Facebook_Fail(SpareroomStatus failedRegistration) {
    }

    /**
     * Tracks a failed event of registering with Spareroom
     *
     * @param failedRegistration the result of failing to register the account. If null or no code
     *                           is provided, the events get reported as an undefined error.
     */
    public void trackEvent_Account_Register_Spareroom_Fail(SpareroomStatus failedRegistration) {
    }

    /**
     * Tracks a successful event of login with Facebook
     */
    public void trackEvent_Account_Login_Facebook() {
    }

    /**
     * Tracks a successful event of login with Spareroom
     */
    public void trackEvent_Account_Login_Spareroom() {
    }

    /**
     * Tracks a failed event of login with Facebook
     *
     * @param failedRegistration the result of failing to login the account
     */
    public void trackEvent_Account_Login_Facebook_Failed(SpareroomStatus failedRegistration) {
    }

    /**
     * Tracks a failed event of login with Spareroom
     *
     * @param failedRegistration the result of failing to login the account
     */
    public void trackEvent_Account_Login_Spareroom_Failed(SpareroomStatus failedRegistration) {
    }

    /**
     * Tracks a successful event of logout
     */
    public void trackEvent_Account_Logout() {
    }

    /**
     * Tracks a successful event of requesting a new password
     */
    public void trackEvent_Account_ForgotPassword() {
    }

    /**
     * Tracks a failed event of requesting a new password
     *
     * @param failedRecover the result of failing to request a new password. If null or no code
     *                      is provided, the events get reported as an undefined error.
     */
    public void trackEvent_Account_ForgotPassword_Failed(SpareroomStatus failedRecover) {
    }

    /**
     * Tracks a successful event of uploading a video for an advert from the gallery
     */
    public void trackEvent_AdvertVideo_UploadOfferedCamera() {
    }

    /**
     * Tracks a successful event of uploading a video for an advert from the gallery
     */
    public void trackEvent_AdvertVideo_UploadOfferedGallery() {
    }

    /**
     * Tracks a successful event of uploading a video for an advert from the gallery
     */
    public void trackEvent_AdvertVideo_UploadWantedCamera() {
    }

    /**
     * Tracks a successful event of uploading a video for an advert from the gallery
     */
    public void trackEvent_AdvertVideo_UploadWantedGallery() {
    }

    /**
     * Tracks a failed event of uploading a video for an advert from the gallery
     *
     * @param cause 1: too big; 2: too long;
     */
    public void trackEvent_AdvertVideo_ErrorUploadingOfferedCamera(int cause) {
    }

    /**
     * Tracks a failed event of uploading a video for an advert from the gallery
     *
     * @param cause 1: too big; 2: too long;
     */
    public void trackEvent_AdvertVideo_ErrorUploadingOfferedGallery(int cause) {
    }

    /**
     * Tracks a failed event of uploading a video for an advert from the gallery
     *
     * @param cause 1: too big; 2: too long;
     */
    public void trackEvent_AdvertVideo_ErrorUploadingWantedCamera(int cause) {
    }

    /**
     * Tracks a failed event of uploading a video for an advert from the gallery
     *
     * @param cause 1: too big; 2: too long;
     */
    public void trackEvent_AdvertVideo_ErrorUploadingWantedGallery(int cause) {
    }

    /**
     * Tracks calling to Customer Services
     */
    public void trackEvent_ContactUs_Call() {
    }

    /**
     * Tracks emailing Customer Services
     */
    public void trackEvent_ContactUs_Email() {
    }

    public void trackFailedToShowHomeFragmentOnBackPressEvent() {
    }

    public void setCustomerUserId(String customerUserId) {

    }

    public void trackSingleConversationLoadedEvent(float duration) {
    }

    public void trackTaskDuration(String taskName, float duration) {

    }

    public void logHandledException(String action, Throwable throwable) {

    }

    public void trackOpenedCustomTab(boolean opened) {

    }

    public void trackInitialRatingPopupBackButton() {

    }

    public void trackInitialRatingPopupTapOutside() {

    }

    public void trackInitialRatingPopupMaybeLater() {

    }

    public void trackInitialRatingPopupRateApp() {

    }

    public void trackReminderRatingPopupBackButton() {

    }

    public void trackReminderRatingPopupTapOutside() {

    }

    public void trackReminderRatingPopupDismiss() {

    }

    public void trackReminderRatingPopupRateApp() {

    }

    public void trackSearchAdvertListLoadedEvent(float duration) {
    }

    public void trackUsingDeadFragmentEvent(Fragment fragment) {
    }

    public void leaveBreadcrumb(String message) {

    }

}