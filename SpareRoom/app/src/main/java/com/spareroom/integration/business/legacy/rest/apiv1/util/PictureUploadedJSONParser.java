package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.Picture;
import com.spareroom.model.business.SpareroomStatus;

import org.json.JSONException;
import org.json.JSONObject;

public class PictureUploadedJSONParser {
    /**
     * Parses the response of an auto-reply request
     *
     * @param json String containing the response
     * @return SpareroomStatus stating whether the response was successful or not
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public Object parse(String json) throws InvalidJSONFormatException {
        try {
            JSONObject firstNode = new JSONObject(json);

            String response_type = null;
            try {
                response_type = firstNode.getString("response_type");

                if (response_type.equals("error")) {
                    SpareroomStatus m = new SpareroomStatus();
                    m.setCode(firstNode.getString("response_id"));
                    m.setMessage(firstNode.getString("response"));
                    return m;
                }
            } catch (JSONException e) {
            }

            if (response_type == null) {
                SpareroomStatus m = new SpareroomStatus();
                m.setMessage(firstNode.getString("message"));
                return m;
            }

            if (firstNode.has("photo")) {
                Picture p = new Picture();
                JSONObject jsonPicture = firstNode.getJSONObject("photo");
                if (jsonPicture.has("caption") && !jsonPicture.getString("caption").equals(null))
                    p.setCaption(jsonPicture.getString("caption"));
                p.setId(jsonPicture.getString("photo_id"));
                p.setLargeUrl(jsonPicture.getString("large_url"));

                return p;
            }

            throw new InvalidJSONFormatException();

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

    }
}
