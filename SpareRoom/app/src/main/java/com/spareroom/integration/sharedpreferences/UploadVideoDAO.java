package com.spareroom.integration.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by miguel.rossi on 26/09/2016.
 */
public class UploadVideoDAO {
    private static final String PREFS_NAME = "VideoUpload";

    public static final int VIDEO_UPLOAD_FAILED = 2;
    public static final int VIDEO_UPLOAD_NONE = -1;

    public static void create(Context c, String videoKey) {
        SharedPreferences sharedPreferences =
                c.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(videoKey, VIDEO_UPLOAD_FAILED);
        editor.apply();
    }

    public static int read(Context c, String videoKey) {
        SharedPreferences sharedPreferences =
                c.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        return sharedPreferences.getInt(videoKey, VIDEO_UPLOAD_NONE);
    }

    public static void update(Context c, int uploadState, String videoKey) {
        SharedPreferences sharedPreferences =
                c.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(videoKey, uploadState);
        editor.apply();
    }

    public static void delete(Context c, String videoKey) {
        SharedPreferences sharedPreferences =
                c.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.remove(videoKey);
        editor.apply();
    }

    public static void saveVideoPath(Context c, String videoKey, String videoPath) {
        SharedPreferences sharedPreferences =
                c.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(videoKey + "_path", videoPath);
        editor.apply();
    }

    public static String retrieveVideoPath(Context c, String videoKey) {
        SharedPreferences sharedPreferences =
                c.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(videoKey + "_path", null);
    }

    public static void removeVideoPath(Context c, String videoKey) {
        SharedPreferences sharedPreferences =
                c.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.remove(videoKey + "_path");
        editor.apply();
    }

}
