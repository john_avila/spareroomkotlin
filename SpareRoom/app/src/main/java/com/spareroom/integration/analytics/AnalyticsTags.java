package com.spareroom.integration.analytics;

/**
 * Standard tags for Analytics. Specific platforms may either reuse these standard ones or define
 * their own.
 */
public class AnalyticsTags {
    public final static int VIDEO_UPLOAD_ERROR_TOO_BIG = 1;
    public final static int VIDEO_UPLOAD_ERROR_TOO_LONG = 2;

}
