package com.spareroom.integration.analytics.legacy;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.spareroom.R;

import java.util.HashMap;

public class GoogleAnalyticsTracker {
    private static GoogleAnalyticsTracker __instance;
    private Context _appContext;
    private HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();

    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        // GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    private GoogleAnalyticsTracker(Context applicationContext) {
        __instance = this;
        _appContext = applicationContext;
    }

    public static GoogleAnalyticsTracker getInstance(Context applicationContext) {
        if (__instance == null)
            __instance = new GoogleAnalyticsTracker(applicationContext);

        return __instance;
    }

    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(_appContext);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(R.xml.app_tracker)
                    // : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.app_tracker);
                    : analytics.newTracker(R.xml.ecommerce_tracker);

            // analytics.setDryRun(true);
            // analytics.getLogger().setLogLevel(LogLevel.VERBOSE);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }

}
