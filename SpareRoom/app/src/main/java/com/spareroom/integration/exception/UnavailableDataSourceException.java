package com.spareroom.integration.exception;

public class UnavailableDataSourceException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = -7680127637031287215L;

    public UnavailableDataSourceException() {
        super();
    }

    public UnavailableDataSourceException(String detailMessage) {
        super(detailMessage);
    }

    public UnavailableDataSourceException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public UnavailableDataSourceException(Throwable throwable) {
        super(throwable);
    }

}

