package com.spareroom.integration.business.legacy.rest;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.model.business.AdWanted;

public interface IAdWantedCompleteDAO {

    AdWanted read(String advertId) throws AuthenticationException, NetworkConnectivityException, InvalidJSONFormatException, ClientErrorException, ServerErrorException;
}