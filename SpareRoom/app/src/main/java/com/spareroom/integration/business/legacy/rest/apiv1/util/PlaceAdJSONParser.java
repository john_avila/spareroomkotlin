package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.Session;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.business.SpareroomStatusCode;

import org.json.JSONException;
import org.json.JSONObject;

public class PlaceAdJSONParser {

    /**
     * Parses the response after registering a new user
     *
     * @param json String containing the response
     * @return an User object if the the user could be registered, SpareroomStatus if there were problems in the request
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public SpareroomStatus parse(String json) throws InvalidJSONFormatException {
        try {
            JSONObject firstNode = new JSONObject(json);

            String response_type = null;

            try {

                if (firstNode.has("success")) {
                    if (firstNode.getInt("success") == 1) {
                        SpareroomStatus im = new SpareroomStatus();
                        im.setCode(Integer.toString(firstNode.getInt("success")));
                        im.setCause(null);
                        if (firstNode.has("advert_id"))
                            im.setMessage(firstNode.getString("advert_id"));
                        return im;
                    }
                }

                response_type = firstNode.getString("response_type");

                if (response_type.equals("success")) {
                    String jsonSession = firstNode.getString("session");

                    SessionJSONParser sessionParser = new SessionJSONParser();
                    Session s = sessionParser.parse(jsonSession);

                    SpareroomStatus im = new SpareroomStatus();
                    im.setCode(SpareroomStatusCode.CODE_SUCCESS);
                    im.setCause(null);
                    im.setMessage("Success");

                    return im;
                } else if (response_type.equals("error")) {
                    SpareroomStatus im = new SpareroomStatus();
                    if (firstNode.has("response_id") &&
                            (firstNode.getString("response_id") != null) &&
                            !firstNode.getString("response_id").isEmpty())
                        im.setCode(firstNode.getString("response_id"));
                    if (firstNode.has("error_element")) {
                        im.setCause(firstNode.getString("error_element"));
                    }
                    if (firstNode.has("error"))
                        im.setMessage(firstNode.getString("error"));
                    else if (firstNode.has("response"))
                        im.setMessage(firstNode.getString("response"));
                    return im;
                }
            } catch (JSONException e) {
                // response_type is always returned for this web service, even if it is successful
                throw new InvalidJSONFormatException();
            }

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

        throw new InvalidJSONFormatException();
    }
}
