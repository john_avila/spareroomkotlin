package com.spareroom.integration.dependency.module;

import com.spareroom.ui.filters.screen.FilterActivity;
import com.spareroom.ui.screen.*;
import com.spareroom.ui.screen.legacy.*;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@SuppressWarnings("unused")
@Module
public abstract class ActivityModule {

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract ConversationActivity contributeConversationActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract PictureGalleryActivity contributePictureGalleryActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract AdActivity contributeAdActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract MyAdActivity contributeMyAdActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract ResultsActivity contributeResultsActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract FilterActivity contributeFilterActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract ResultsMapActivity contributeResultsMapActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract EditProfileActivity contributeEditProfileActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract PlaceAdActivity contributePlaceAdActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract EditAdActivity contributeEditAdActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract SavedSearchesAddActivity contributeSavedSearchesAddActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract PlaceAdAreasActivity contributePlaceAdAreasActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract VideoPlayerActivity contributeVideoPlayerActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract SearchActivity contributeSearchActivity();

}
