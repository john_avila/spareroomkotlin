package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IAdWantedCompleteDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.AdDetailsSheetJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.json.JsonParser;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.integration.webservice.rest.service.AdRestService;
import com.spareroom.model.business.*;

import javax.inject.Inject;

public class AdWantedCompleteDAO implements IAdWantedCompleteDAO {
    private final AdRestService adRestService;
    private final JsonParser jsonParser;

    @Inject
    public AdWantedCompleteDAO(AdRestService adRestService, JsonParser jsonParser) {
        this.adRestService = adRestService;
        this.jsonParser = jsonParser;
    }

    public AdWanted read(String advertId) throws AuthenticationException, NetworkConnectivityException, InvalidJSONFormatException, ClientErrorException, ServerErrorException {
        Parameters p = new Parameters();
        p.add("format", "json");

        String response = adRestService.getAdDetails(advertId, p, SearchType.WANTED);

        AdWanted adComplete = jsonParser.fromJson(response, AdWantedCompleteResponse.class).getAdvert();

        if (adComplete != null) {
            AdDetailsSheetJSONParser sheetParser = new AdDetailsSheetJSONParser();
            adComplete.setSheet(sheetParser.parse(response, true));
        }

        return adComplete;
    }

}
