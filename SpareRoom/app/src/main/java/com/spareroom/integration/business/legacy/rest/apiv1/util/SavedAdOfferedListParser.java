package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.json.JsonParser;
import com.spareroom.model.business.*;

import org.json.*;

import java.util.LinkedList;

import javax.inject.Inject;

public class SavedAdOfferedListParser {

    private final JsonParser jsonParser;

    @Inject
    SavedAdOfferedListParser(JsonParser jsonParser) {
        this.jsonParser = jsonParser;
    }

    public Object parse(String json) throws InvalidJSONFormatException {
        try {
            JSONObject firstNode = new JSONObject(json);

            if (firstNode.has("response_type") && firstNode.getString("response_type").equals("error")) {
                SpareroomStatus im = new SpareroomStatus();
                if (firstNode.has("response"))
                    im.setMessage(firstNode.getString("response"));
                if (firstNode.has("response_id"))
                    im.setCode(firstNode.getString("response_id"));
                return im;
            }

            if (firstNode.has("success") && firstNode.getString("success").equals("1")) {
                AdBoard board = new AdBoard();
                if (firstNode.has("results")) {
                    JSONArray jsonResults = firstNode.getJSONArray("results");

                    if (firstNode.has("count"))
                        board.set_total(Integer.parseInt(firstNode.getString("count")));
                    board.set_total(Integer.parseInt(firstNode.getString("count")));
                    LinkedList<AbstractAd> _lBoard = new LinkedList<>();
                    for (int i = 0; i < jsonResults.length(); i++) {
                        AdOffered ad = jsonParser.fromJson(jsonResults.getString(i), AdOffered.class);
                        _lBoard.add(ad);
                    }
                    board.set_board(_lBoard);
                }
                return board;
            }

            throw new InvalidJSONFormatException();

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }
    }
}
