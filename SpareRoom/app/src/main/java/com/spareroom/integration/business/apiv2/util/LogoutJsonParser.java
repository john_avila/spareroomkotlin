package com.spareroom.integration.business.apiv2.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.SpareroomStatus;

import org.json.JSONException;
import org.json.JSONObject;

public class LogoutJsonParser {
    /**
     * Parses the response after logging out
     *
     * @param json String containing the response
     * @return SpareroomStatus indicating whether the operation was performed successfully or not
     * @throws InvalidJSONFormatException if the format of the JSON String is incorrect
     */
    public SpareroomStatus parse(String json) throws InvalidJSONFormatException {
        try {
            JSONObject firstNode = new JSONObject(json);
            String response_type = firstNode.getString("response_type");
            SpareroomStatus e = new SpareroomStatus();

            if (response_type.equals("success")) {
                e.setCode(firstNode.getString("success"));
                e.setMessage(firstNode.getString("response"));

            } else if (response_type.equals("error")) {
                e.setCode(firstNode.getString("response_id"));
                e.setMessage(firstNode.getString("response"));

            }

            return e;

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();

        }

    }

}
