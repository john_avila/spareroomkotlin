package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.ISavedAdDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.SavedAdParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.AdSearchesRestService;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SpareroomStatus;

public class SavedAdDAO implements ISavedAdDAO {

    private final AdSearchesRestService adSearchesRestService;

    SavedAdDAO() {
        adSearchesRestService = new AdSearchesRestService();
    }

    public SpareroomStatus create(Parameters parameters) throws AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException {

        parameters.add("format", "json");

        String response = adSearchesRestService.saveAd(parameters);

        SavedAdParser parser = new SavedAdParser();

        return parser.parse(response);

    }
}
