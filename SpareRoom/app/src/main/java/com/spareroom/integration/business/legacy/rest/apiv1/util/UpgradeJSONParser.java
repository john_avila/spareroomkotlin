package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.business.SpareroomStatusCode;

import org.json.JSONException;
import org.json.JSONObject;

public class UpgradeJSONParser {
    public SpareroomStatus parse(String json) throws InvalidJSONFormatException {
        try {
            JSONObject firstNode = new JSONObject(json);

            String response_type = null;

            try {

                if (firstNode.has("success") && firstNode.getString("success").equals(SpareroomStatusCode.CODE_SUCCESS)) {
                    SpareroomStatus im = new SpareroomStatus();
                    im.setCode(SpareroomStatusCode.CODE_SUCCESS);
                    if (firstNode.has("order_ref"))
                        im.setMessage(firstNode.getString("order_ref"));
                    return im;

					/*String jsonSession = firstNode.getString("session");

					SessionJSONParser sessionParser = new SessionJSONParser();
					Session s = sessionParser.parse(jsonSession);
					*/
                } else if (firstNode.has("response_type") && firstNode.getString("response_type").equals("error")) {
                    SpareroomStatus im = new SpareroomStatus();
                    if (firstNode.has("response_id"))
                        im.setCode(firstNode.getString("response_id"));
                    if (firstNode.has("response"))
                        im.setMessage(firstNode.getString("response"));
                    return im;
                }
                throw new InvalidJSONFormatException();

            } catch (JSONException e) {
                // response_type is always returned for this web service, even if it is successful
                throw new InvalidJSONFormatException();
            }

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }

    }
}