package com.spareroom.integration.dependency.module;

import com.spareroom.integration.business.apiv2.*;
import com.spareroom.integration.business.legacy.rest.*;
import com.spareroom.integration.business.legacy.rest.apiv1.*;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;

@SuppressWarnings("unused")
@Module
public abstract class DaoModule {

    public static final String OFFERED_SAVED_ADS_LIST_DAO = "OFFERED_SAVED_ADS_LIST_DAO";
    public static final String WANTED_SAVED_ADS_LIST_DAO = "WANTED_SAVED_ADS_LIST_DAO";

    @Binds
    abstract IConversationDao bindConversationDao(ConversationDao conversationDao);

    @Binds
    abstract INewMessageDao bindMessageDao(NewMessageDao newMessageDao);

    @Binds
    abstract IAdOfferedCompleteDao bindAdOfferedCompleteDao(AdOfferedCompleteDao adOfferedCompleteDao);

    @Binds
    abstract IOfferedAdvertsDao bindOfferedAdvertsDao(OfferedAdvertsDao offeredAdvertsDao);

    @Binds
    abstract IWantedAdvertsDao bindWantedAdvertsDao(WantedAdvertsDao wantedAdvertsDao);

    @Binds
    abstract IMyAdvertsDao bindMyAdvertsDao(MyAdvertsDao myAdvertsDao);

    @Binds
    abstract IAdvertListDao bindAdvertListDao(AdvertListDao advertListDao);

    @Binds
    abstract IConversationListDao bindConversationListDao(ConversationListDao conversationListDao);

    @Binds
    abstract IAdWantedCompleteDAO bindAdWantedCompleteDao(AdWantedCompleteDAO adWantedCompleteDao);

    @Binds
    @Named(OFFERED_SAVED_ADS_LIST_DAO)
    abstract ISavedAdListDAO bindSavedAdOfferedListDAO(SavedAdOfferedListDAO savedAdOfferedListDAO);

    @Binds
    @Named(WANTED_SAVED_ADS_LIST_DAO)
    abstract ISavedAdListDAO bindSavedAdWantedListDAO(SavedAdWantedListDAO savedAdWantedListDAO);

    @Binds
    abstract ILocationDao bindLocationDao(LocationDao locationDao);
}