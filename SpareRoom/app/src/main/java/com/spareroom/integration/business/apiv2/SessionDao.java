package com.spareroom.integration.business.apiv2;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.SpareroomAbstractDaoImpl;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InvalidArgumentException;
import com.spareroom.integration.exception.UnavailableDataSourceException;
import com.spareroom.integration.exception.UnexpectedResultException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.LoginRestService;
import com.spareroom.model.business.Parameters;

/**
 * DAO for retrieving the Session
 */
class SessionDao extends SpareroomAbstractDaoImpl<Void, Void, Void> {

    private final LoginRestService loginRestService;

    SessionDao(SpareroomContext spareroomContext) {
        super(spareroomContext);
        loginRestService = new LoginRestService();
    }

    @Override
    public Object read(Void id, Void extra) throws
            UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {
        Parameters parameters = new Parameters();

        parameters.add(ApiParams.FORMAT, ApiParams.FORMAT_JSON);

        try {
            loginRestService.getSession(parameters);

            return null;
        } catch (NetworkConnectivityException e) {
            throw new UnavailableDataSourceException(e);
        } catch (ClientErrorException | AuthenticationException e) {
            throw new InvalidArgumentException(e);
        } catch (ServerErrorException e) {
            throw new UnexpectedResultException(e);
        }
    }

}
