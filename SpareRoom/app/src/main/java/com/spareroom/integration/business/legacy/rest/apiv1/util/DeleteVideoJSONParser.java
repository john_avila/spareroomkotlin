package com.spareroom.integration.business.legacy.rest.apiv1.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.SpareroomStatus;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Parses the response of an auto-reply request
 * </p>
 * Created by miguel.rossi on 26/08/2016.
 */
public class DeleteVideoJSONParser {

    /**
     * "Parses" the response after deleting a conversation, actually it crashes if it wasn't ok,
     * otherwise return <code>null</code>
     *
     * @param json String containing the response
     * @return SpareroomStatus <code>null</code> if ok
     * @throws InvalidJSONFormatException if it wasn't successful or any other thing happened
     */
    public SpareroomStatus parse(String json) throws InvalidJSONFormatException {

        try {
            JSONObject firstNode = new JSONObject(json);
            firstNode.getString("success");

            return null;

        } catch (JSONException e) {
            throw new InvalidJSONFormatException();

        }

    }

}
