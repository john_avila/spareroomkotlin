package com.spareroom.integration.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Calendar;

public class AutocompleteConfigDAO {
    private final Context _context;
    private static final String PREFS_NAME = "autocomplete";
    private static final String DATE = "date";

    public AutocompleteConfigDAO(Context context) {
        _context = context;
    }

    public boolean create() {
        final SharedPreferences prefs = _context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.edit().putString(DATE, Long.toString(Calendar.getInstance().getTimeInMillis())).commit();
    }

    public String read() {
        final SharedPreferences prefs = _context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getString(DATE, "");
    }

    public boolean update() {
        final SharedPreferences prefs = _context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.edit().putString(DATE, Long.toString(Calendar.getInstance().getTimeInMillis())).commit();
    }

    public boolean delete() {
        final SharedPreferences prefs = _context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.edit().clear().commit();
    }
}
