package com.spareroom.integration.analytics;

import android.app.Activity;

import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.business.Upgrade;

import java.util.LinkedList;

import androidx.fragment.app.Fragment;

/**
 * Composite tracker that will fire actions on all registered trackers. Singleton.
 */
public class AnalyticsTrackerComposite extends AnalyticsTracker {
    private static AnalyticsTrackerComposite __instance;

    private LinkedList<AnalyticsTracker> _lTracker;

    /**
     * Constructor
     */
    private AnalyticsTrackerComposite() {
        _lTracker = new LinkedList<>();
        // Turn on the following line if we wanted to disable dependency injection (add method)
        // _lTracker.add(new AnalyticsTrackerGoogleAnalytics(__applicationContext));
    }

    /**
     * Gets an instance of this class (Singleton)
     *
     * @return an instance of the <code>AnalyticsTrackerComposite</code>
     */
    public static AnalyticsTrackerComposite getInstance() {
        if (__instance == null)
            __instance = new AnalyticsTrackerComposite();
        return __instance;
    }

    /**
     * Adds a specific tracker to this Composite tracker. Good for Unit testing / dependency
     * injection. Initialise this Composite tracker and add all trackers on first run. Avoid
     * adding any trackers during the lifetime of this Singleton.
     *
     * @param tracker a tracker for a specific platform
     */
    public void add(AnalyticsTracker tracker) {
        _lTracker.add(tracker);
    }

    @Override
    public void trackScreen_Activity(Activity activity) {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackScreen_Activity(activity);
    }

    @Override
    public void trackScreen_Fragment(Fragment fragment) {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackScreen_Fragment(fragment);
    }

    @Override
    public void trackEvent_AdvertOffered_MessageAdvertiser() {
        super.trackEvent_AdvertOffered_MessageAdvertiser();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertOffered_MessageAdvertiser();
    }

    @Override
    public void trackEvent_AdvertWanted_MessageAdvertiser() {
        super.trackEvent_AdvertWanted_MessageAdvertiser();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertWanted_MessageAdvertiser();
    }

    @Override
    public void trackEvent_AdvertOffered_PhoneAdvertiser() {
        super.trackEvent_AdvertOffered_PhoneAdvertiser();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertOffered_PhoneAdvertiser();
    }

    @Override
    public void trackEvent_AdvertWanted_PhoneAdvertiser() {
        super.trackEvent_AdvertWanted_PhoneAdvertiser();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertWanted_PhoneAdvertiser();
    }

    @Override
    public void trackEvent_AdvertOffered_Report() {
        super.trackEvent_AdvertOffered_Report();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertOffered_Report();
    }

    @Override
    public void trackEvent_AdvertWanted_Report() {
        super.trackEvent_AdvertWanted_Report();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertWanted_Report();
    }

    public void trackEvent_AdvertOffered_Share() {
        super.trackEvent_AdvertOffered_Share();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertOffered_Share();
    }

    public void trackEvent_AdvertWanted_Share() {
        super.trackEvent_AdvertWanted_Share();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertWanted_Share();
    }

    @Override
    public void trackEvent_Menu_Search(boolean isLoggedIn) {
        super.trackEvent_Menu_Search(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Menu_Search(isLoggedIn);
    }

    @Override
    public void trackEvent_Menu_Messages(boolean isLoggedIn) {
        super.trackEvent_Menu_Messages(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Menu_Messages(isLoggedIn);
    }

    @Override
    public void trackEvent_Menu_MyAdverts(boolean isLoggedIn) {
        super.trackEvent_Menu_MyAdverts(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Menu_MyAdverts(isLoggedIn);
    }

    @Override
    public void trackEvent_Menu_SavedAds(boolean isLoggedIn) {
        super.trackEvent_Menu_SavedAds(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Menu_SavedAds(isLoggedIn);
    }

    @Override
    public void trackEvent_Menu_SavedSearches(boolean isLoggedIn) {
        super.trackEvent_Menu_SavedSearches(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Menu_SavedSearches(isLoggedIn);
    }

    @Override
    public void trackEvent_Menu_EditMyDetails(boolean isLoggedIn) {
        super.trackEvent_Menu_EditMyDetails(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Menu_EditMyDetails(isLoggedIn);
    }

    @Override
    public void trackEvent_Menu_Upgrade(boolean isLoggedIn) {
        super.trackEvent_Menu_Upgrade(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Menu_Upgrade(isLoggedIn);
    }

    @Override
    public void trackEvent_Menu_PlaceAdvert(boolean isLoggedIn) {
        super.trackEvent_Menu_PlaceAdvert(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Menu_PlaceAdvert(isLoggedIn);
    }

    @Override
    public void trackEvent_Menu_BetaProgramme() {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Menu_BetaProgramme();
    }

    @Override
    public void trackEvent_Menu_ContactUsPhone(boolean isLoggedIn) {
        super.trackEvent_Menu_ContactUsPhone(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Menu_ContactUsPhone(isLoggedIn);
    }

    @Override
    public void trackEvent_Menu_ContactUsMail(boolean isLoggedIn) {
        super.trackEvent_Menu_ContactUsMail(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Menu_ContactUsMail(isLoggedIn);
    }

    @Override
    public void trackEvent_Menu_LogOut(boolean isLoggedIn) {
        super.trackEvent_Menu_LogOut(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Menu_LogOut(isLoggedIn);
    }

    @Override
    public void trackEvent_Push_TooManyRequests(Long runs) {
        super.trackEvent_Push_TooManyRequests(runs);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Push_TooManyRequests(runs);
    }

    @Override
    public void trackEvent_SavedSearch_LaunchSavedSearch() {
        super.trackEvent_SavedSearch_LaunchSavedSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SavedSearch_LaunchSavedSearch();
    }

    @Override
    public void trackEvent_SavedSearch_DeleteSavedSearch() {
        super.trackEvent_SavedSearch_DeleteSavedSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SavedSearch_DeleteSavedSearch();
    }

    @Override
    public void trackEvent_Search_BroadenSearch(boolean isLoggedIn) {
        super.trackEvent_Search_BroadenSearch(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Search_BroadenSearch(isLoggedIn);
    }

    @Override
    public void trackEvent_Search_MarkFavouriteOffered(boolean isLoggedIn) {
        super.trackEvent_Search_MarkFavouriteOffered(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Search_MarkFavouriteOffered(isLoggedIn);
    }

    @Override
    public void trackEvent_Search_MarkFavouriteWanted(boolean isLoggedIn) {
        super.trackEvent_Search_MarkFavouriteWanted(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Search_MarkFavouriteWanted(isLoggedIn);
    }

    @Override
    public void trackEvent_Search_RemoveShortlistOffered(boolean isLoggedIn) {
        super.trackEvent_Search_RemoveShortlistOffered(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Search_RemoveShortlistOffered(isLoggedIn);
    }

    @Override
    public void trackEvent_Search_RemoveShortlistWanted(boolean isLoggedIn) {
        super.trackEvent_Search_RemoveShortlistWanted(isLoggedIn);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Search_RemoveShortlistWanted(isLoggedIn);
    }

    @Override
    public void trackEvent_SearchRooms_LaunchSavedSearch() {
        super.trackEvent_SearchRooms_LaunchSavedSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRooms_LaunchSavedSearch();
    }

    @Override
    public void trackEvent_SearchRooms_DeleteSavedSearch() {
        super.trackEvent_SearchRooms_DeleteSavedSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRooms_DeleteSavedSearch();
    }

    @Override
    public void trackEvent_SearchRooms_ManualSearch() {
        super.trackEvent_SearchRooms_ManualSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRooms_ManualSearch();
    }

    @Override
    public void trackEvent_SearchRooms_AutocompleteSearch() {
        super.trackEvent_SearchRooms_AutocompleteSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRooms_AutocompleteSearch();
    }

    @Override
    public void trackEvent_SearchRooms_HistorySearch() {
        super.trackEvent_SearchRooms_HistorySearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRooms_HistorySearch();
    }

    @Override
    public void trackEvent_SearchRooms_ManualAdvertSearch() {
        super.trackEvent_SearchRooms_ManualAdvertSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRooms_ManualAdvertSearch();
    }

    @Override
    public void trackEvent_SearchRooms_HistoryAdvertSearch() {
        super.trackEvent_SearchRooms_HistoryAdvertSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRooms_HistoryAdvertSearch();
    }

    @Override
    public void trackEvent_SearchRooms_AutocompleteAdvertSearch() {
        super.trackEvent_SearchRooms_AutocompleteAdvertSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRooms_AutocompleteAdvertSearch();
    }

    @Override
    public void trackEvent_SearchFlatmates_ManualSearch() {
        super.trackEvent_SearchFlatmates_ManualSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchFlatmates_ManualSearch();
    }

    @Override
    public void trackEvent_SearchFlatmates_AutocompleteSearch() {
        super.trackEvent_SearchFlatmates_AutocompleteSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchFlatmates_AutocompleteSearch();
    }

    @Override
    public void trackEvent_SearchFlatmates_HistorySearch() {
        super.trackEvent_SearchFlatmates_HistorySearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchFlatmates_HistorySearch();
    }

    @Override
    public void trackEvent_SearchFlatmates_ManualAdvertSearch() {
        super.trackEvent_SearchFlatmates_ManualAdvertSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchFlatmates_ManualAdvertSearch();
    }

    @Override
    public void trackEvent_SearchFlatmates_HistoryAdvertSearch() {
        super.trackEvent_SearchFlatmates_HistoryAdvertSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchFlatmates_HistoryAdvertSearch();
    }

    @Override
    public void trackEvent_SearchFlatmates_AutocompleteAdvertSearch() {
        super.trackEvent_SearchFlatmates_AutocompleteAdvertSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchFlatmates_AutocompleteAdvertSearch();
    }

    @Override
    public void trackEvent_Results_SuggestionAdvertSearch() {
        super.trackEvent_Results_SuggestionAdvertSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Results_SuggestionAdvertSearch();
    }

    @Override
    public void trackEvent_Results_CancelSuggestionAdvertSearch() {
        super.trackEvent_Results_CancelSuggestionAdvertSearch();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Results_CancelSuggestionAdvertSearch();
    }

    @Override
    public void trackEvent_Message_Send(SpareroomStatus status) {
        super.trackEvent_Message_Send(status);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Message_Send(status);
    }

    @Override
    public void trackEvent_Message_Autoreject(SpareroomStatus status) {
        super.trackEvent_Message_Autoreject(status);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Message_Autoreject(status);
    }

    @Override
    public void trackEvent_SearchRooms_GetLocation() {
        super.trackEvent_SearchRooms_GetLocation();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRooms_GetLocation();
    }

    @Override
    public void trackEvent_SearchFlatmates_GetLocation() {
        super.trackEvent_SearchFlatmates_GetLocation();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchFlatmates_GetLocation();
    }

    @Override
    public void trackEvent_PlaceAd_StartPlaceOfferedAd() {
        super.trackEvent_PlaceAd_StartPlaceOfferedAd();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_PlaceAd_StartPlaceOfferedAd();
    }

    @Override
    public void trackEvent_PlaceAd_StartPlaceWantedAd() {
        super.trackEvent_PlaceAd_StartPlaceWantedAd();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_PlaceAd_StartPlaceWantedAd();
    }

    @Override
    public void trackEvent_PlaceAd_PlaceOfferedAd() {
        super.trackEvent_PlaceAd_PlaceOfferedAd();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_PlaceAd_PlaceOfferedAd();
    }

    @Override
    public void trackEvent_PlaceAd_PlaceWantedAd() {
        super.trackEvent_PlaceAd_PlaceWantedAd();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_PlaceAd_PlaceWantedAd();
    }

    @Override
    public void trackEvent_PlaceAd_PlaceOfferedAdWithErrors() {
        super.trackEvent_PlaceAd_PlaceOfferedAdWithErrors();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_PlaceAd_PlaceOfferedAdWithErrors();
    }

    @Override
    public void trackEvent_PlaceAd_PlaceWantedAdWithErrors() {
        super.trackEvent_PlaceAd_PlaceWantedAdWithErrors();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_PlaceAd_PlaceWantedAdWithErrors();
    }

    @Override
    public void trackEvent_EditAd_ReadOfferedAd() {
        super.trackEvent_EditAd_ReadOfferedAd();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_EditAd_ReadOfferedAd();
    }

    @Override
    public void trackEvent_EditAd_ReadWantedAd() {
        super.trackEvent_EditAd_ReadWantedAd();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_EditAd_ReadWantedAd();
    }

    @Override
    public void trackEvent_EditAd_ReadOfferedAdWithErrors(SpareroomStatus status) {
        super.trackEvent_EditAd_ReadOfferedAdWithErrors(status);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_EditAd_ReadOfferedAdWithErrors(status);
    }

    @Override
    public void trackEvent_EditAd_ReadWantedAdWithErrors(SpareroomStatus status) {
        super.trackEvent_EditAd_ReadWantedAdWithErrors(status);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_EditAd_ReadWantedAdWithErrors(status);
    }

    @Override
    public void trackEvent_EditAd_EditOfferedAd() {
        super.trackEvent_EditAd_EditOfferedAd();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_EditAd_EditOfferedAd();
    }

    @Override
    public void trackEvent_EditAd_EditWantedAd() {
        super.trackEvent_EditAd_EditWantedAd();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_EditAd_EditWantedAd();
    }

    @Override
    public void trackEvent_EditAd_EditOfferedAdWithErrors(SpareroomStatus status) {
        super.trackEvent_EditAd_EditOfferedAdWithErrors(status);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_EditAd_EditOfferedAdWithErrors(status);
    }

    @Override
    public void trackEvent_EditAd_EditWantedAdWithErrors(SpareroomStatus status) {
        super.trackEvent_EditAd_EditWantedAdWithErrors(status);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_EditAd_EditWantedAdWithErrors(status);
    }

    @Override
    public void trackEvent_Upgrade_SelectProduct(Upgrade u, int contextId, int productPosition) {
        super.trackEvent_Upgrade_SelectProduct(u, contextId, productPosition);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_SelectProduct(u, contextId, productPosition);
    }

    @Override
    public void trackEvent_Upgrade_Purchase(Upgrade u, String orderId, int productPosition) {
        super.trackEvent_Upgrade_Purchase(u, orderId, productPosition);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_Purchase(u, orderId, productPosition);
    }

    @Override
    public void trackEvent_ExtraListing_Purchase(Upgrade u, String orderId) {
        super.trackEvent_ExtraListing_Purchase(u, orderId);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_ExtraListing_Purchase(u, orderId);
    }

    @Override
    public void trackEvent_SearchRoomsMenu_Map() {
        super.trackEvent_SearchRoomsMenu_Map();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRoomsMenu_Map();
    }

    @Override
    public void trackEvent_SearchRoomsMenu_SaveSearch() {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRoomsMenu_SaveSearch();
    }

    @Override
    public void trackEvent_SearchRoomsMenu_Refine() {
        super.trackEvent_SearchRoomsMenu_Refine();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRoomsMenu_Refine();
    }

    @Override
    public void trackEvent_SearchFlatmatesMenu_Refine() {
        super.trackEvent_SearchFlatmatesMenu_Refine();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchFlatmatesMenu_Refine();
    }

    @Override
    public void trackEvent_SearchRoomsMenu_SortOption() {
        super.trackEvent_SearchRoomsMenu_SortOption();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRoomsMenu_SortOption();
    }

    @Override
    public void trackEvent_SearchRooms_SortByDefault() {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRooms_SortByDefault();
    }

    @Override
    public void trackEvent_SearchRooms_SortByLastUpdated() {
        super.trackEvent_SearchRooms_SortByLastUpdated();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRooms_SortByLastUpdated();
    }

    @Override
    public void trackEvent_SearchRooms_SortByNewest() {
        super.trackEvent_SearchRooms_SortByNewest();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRooms_SortByNewest();
    }

    @Override
    public void trackEvent_SearchRooms_SortByMostExpensive() {
        super.trackEvent_SearchRooms_SortByMostExpensive();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRooms_SortByMostExpensive();
    }

    @Override
    public void trackEvent_SearchRooms_SortByLeastExpensive() {
        super.trackEvent_SearchRooms_SortByLeastExpensive();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchRooms_SortByLeastExpensive();
    }

    @Override
    public void trackEvent_SearchFlatmatesMenu_SortOption() {
        super.trackEvent_SearchFlatmatesMenu_SortOption();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchFlatmatesMenu_SortOption();
    }

    @Override
    public void trackEvent_SearchFlatmates_SortByDefault() {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchFlatmates_SortByDefault();
    }

    @Override
    public void trackEvent_SearchFlatmates_SortByLastUpdated() {
        super.trackEvent_SearchFlatmates_SortByLastUpdated();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchFlatmates_SortByLastUpdated();
    }

    @Override
    public void trackEvent_SearchFlatmates_SortByNewest() {
        super.trackEvent_SearchFlatmates_SortByNewest();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchFlatmates_SortByNewest();
    }

    @Override
    public void trackEvent_SearchFlatmates_SortByMostExpensive() {
        super.trackEvent_SearchFlatmates_SortByMostExpensive();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchFlatmates_SortByMostExpensive();
    }

    @Override
    public void trackEvent_SearchFlatmates_SortByLeastExpensive() {
        super.trackEvent_SearchFlatmates_SortByLeastExpensive();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_SearchFlatmates_SortByLeastExpensive();
    }

    @Override
    public void trackEcommerce_Upgrade_SelectProduct(Upgrade u, int contextId,
                                                     int productPosition) {
        super.trackEcommerce_Upgrade_SelectProduct(u, contextId, productPosition);
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEcommerce_Upgrade_SelectProduct(u, contextId, productPosition);

    }

    @Override
    public void trackEcommerce_Upgrade_Purchase(Upgrade u, String orderId, int productPosition) {
        super.trackEcommerce_Upgrade_Purchase(u, orderId, productPosition);
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEcommerce_Upgrade_Purchase(u, orderId, productPosition);
    }

    @Override
    public void trackEcommerce_Upgrade_Purchase_Fail(Upgrade u, int response, String message) {
        super.trackEcommerce_Upgrade_Purchase_Fail(u, response, message);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEcommerce_Upgrade_Purchase_Fail(u, response, message);
    }

    @Override
    public void trackEcommerce_ExtraListing_Purchase(Upgrade u, String orderId) {
        super.trackEcommerce_ExtraListing_Purchase(u, orderId);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEcommerce_ExtraListing_Purchase(u, orderId);
    }

    @Override
    public void trackEcommerce_ExtraListing_Purchase_Fail(Upgrade u, int response, String message) {
        super.trackEcommerce_ExtraListing_Purchase_Fail(u, response, message);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEcommerce_ExtraListing_Purchase_Fail(u, response, message);
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailable_Create() {
        super.trackEvent_Upgrade_BillingUnavailable_Create();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_BillingUnavailable_Create();
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailable_DisableDebug() {
        super.trackEvent_Upgrade_BillingUnavailable_DisableDebug();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_BillingUnavailable_DisableDebug();
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailable_Setup() {
        super.trackEvent_Upgrade_BillingUnavailable_Setup();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_BillingUnavailable_Setup();
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailable_QueryInventory() {
        super.trackEvent_Upgrade_BillingUnavailable_QueryInventory();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_BillingUnavailable_QueryInventory();
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailable_LaunchPurchaseFlow() {
        super.trackEvent_Upgrade_BillingUnavailable_LaunchPurchaseFlow();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_BillingUnavailable_LaunchPurchaseFlow();
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailable_HandlePaymentResult() {
        super.trackEvent_Upgrade_BillingUnavailable_HandlePaymentResult();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_BillingUnavailable_HandlePaymentResult();
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailable_Consume() {
        super.trackEvent_Upgrade_BillingUnavailable_Consume();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_BillingUnavailable_Consume();
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_Create() {
        super.trackEvent_Upgrade_BillingUnavailableExtraListing_Create();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_BillingUnavailableExtraListing_Create();
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_DisableDebug() {
        super.trackEvent_Upgrade_BillingUnavailableExtraListing_DisableDebug();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_BillingUnavailableExtraListing_DisableDebug();
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_Setup() {
        super.trackEvent_Upgrade_BillingUnavailableExtraListing_Setup();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_BillingUnavailableExtraListing_Setup();
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_QueryInventory() {
        super.trackEvent_Upgrade_BillingUnavailableExtraListing_QueryInventory();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_BillingUnavailableExtraListing_QueryInventory();
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_LaunchPurchaseFlow() {
        super.trackEvent_Upgrade_BillingUnavailableExtraListing_LaunchPurchaseFlow();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_BillingUnavailableExtraListing_LaunchPurchaseFlow();
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_HandlePaymentResult() {
        super.trackEvent_Upgrade_BillingUnavailableExtraListing_HandlePaymentResult();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_BillingUnavailableExtraListing_HandlePaymentResult();
    }

    @Override
    public void trackEvent_Upgrade_BillingUnavailableExtraListing_Consume() {
        super.trackEvent_Upgrade_BillingUnavailableExtraListing_Consume();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Upgrade_BillingUnavailableExtraListing_Consume();
    }

    @Override
    public void trackEvent_Account_Register_Facebook(String userType) {
        super.trackEvent_Account_Register_Facebook(userType);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Account_Register_Facebook(userType);
    }

    @Override
    public void trackEvent_Account_Register_Spareroom(String userType) {
        super.trackEvent_Account_Register_Spareroom(userType);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Account_Register_Spareroom(userType);
    }

    @Override
    public void trackEvent_Account_FacebookLoginToRegisterRedirection() {
        super.trackEvent_Account_FacebookLoginToRegisterRedirection();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Account_FacebookLoginToRegisterRedirection();
    }

    @Override
    public void trackEvent_Account_FacebookRegisterToLoginRedirection() {
        super.trackEvent_Account_FacebookRegisterToLoginRedirection();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Account_FacebookRegisterToLoginRedirection();
    }

    @Override
    public void trackEvent_Account_Register_Facebook_Fail(SpareroomStatus failedRegistration) {
        super.trackEvent_Account_Register_Facebook_Fail(failedRegistration);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Account_Register_Facebook_Fail(failedRegistration);
    }

    @Override
    public void trackEvent_Account_Register_Spareroom_Fail(SpareroomStatus failedRegistration) {
        super.trackEvent_Account_Register_Spareroom_Fail(failedRegistration);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Account_Register_Spareroom_Fail(failedRegistration);
    }

    @Override
    public void trackEvent_Account_Login_Facebook() {
        super.trackEvent_Account_Login_Facebook();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Account_Login_Facebook();
    }

    @Override
    public void trackEvent_Account_Login_Spareroom() {
        super.trackEvent_Account_Login_Spareroom();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Account_Login_Spareroom();
    }

    @Override
    public void trackEvent_Account_Login_Facebook_Failed(SpareroomStatus failedRegistration) {
        super.trackEvent_Account_Login_Facebook_Failed(failedRegistration);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Account_Login_Facebook_Failed(failedRegistration);
    }

    @Override
    public void trackEvent_Account_Login_Spareroom_Failed(SpareroomStatus failedRegistration) {
        super.trackEvent_Account_Login_Spareroom_Failed(failedRegistration);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Account_Login_Spareroom_Failed(failedRegistration);
    }

    @Override
    public void trackEvent_Account_Logout() {
        super.trackEvent_Account_Logout();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Account_Logout();
    }

    @Override
    public void trackEvent_Account_ForgotPassword() {
        super.trackEvent_Account_ForgotPassword();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Account_ForgotPassword();
    }

    @Override
    public void trackEvent_Account_ForgotPassword_Failed(SpareroomStatus failedRecover) {
        super.trackEvent_Account_ForgotPassword_Failed(failedRecover);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_Account_ForgotPassword_Failed(failedRecover);
    }

    @Override
    public void trackEvent_AdvertVideo_UploadOfferedCamera() {
        super.trackEvent_AdvertVideo_UploadOfferedCamera();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertVideo_UploadOfferedCamera();
    }

    @Override
    public void trackEvent_AdvertVideo_UploadOfferedGallery() {
        super.trackEvent_AdvertVideo_UploadOfferedGallery();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertVideo_UploadOfferedGallery();
    }

    @Override
    public void trackEvent_AdvertVideo_UploadWantedCamera() {
        super.trackEvent_AdvertVideo_UploadWantedCamera();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertVideo_UploadWantedCamera();
    }

    @Override
    public void trackEvent_AdvertVideo_UploadWantedGallery() {
        super.trackEvent_AdvertVideo_UploadWantedGallery();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertVideo_UploadWantedGallery();
    }

    @Override
    public void trackEvent_AdvertVideo_ErrorUploadingOfferedCamera(int cause) {
        super.trackEvent_AdvertVideo_ErrorUploadingOfferedCamera(cause);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertVideo_ErrorUploadingOfferedCamera(cause);
    }

    @Override
    public void trackEvent_AdvertVideo_ErrorUploadingOfferedGallery(int cause) {
        super.trackEvent_AdvertVideo_ErrorUploadingOfferedGallery(cause);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertVideo_ErrorUploadingOfferedGallery(cause);
    }

    @Override
    public void trackEvent_AdvertVideo_ErrorUploadingWantedCamera(int cause) {
        super.trackEvent_AdvertVideo_ErrorUploadingWantedCamera(cause);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertVideo_ErrorUploadingWantedCamera(cause);
    }

    @Override
    public void trackEvent_AdvertVideo_ErrorUploadingWantedGallery(int cause) {
        super.trackEvent_AdvertVideo_ErrorUploadingWantedGallery(cause);

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_AdvertVideo_ErrorUploadingWantedGallery(cause);
    }

    @Override
    public void trackEvent_ContactUs_Call() {
        super.trackEvent_ContactUs_Call();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_ContactUs_Call();
    }

    @Override
    public void trackEvent_ContactUs_Email() {
        super.trackEvent_ContactUs_Email();

        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackEvent_ContactUs_Email();
    }

    @Override
    public void trackFailedToShowHomeFragmentOnBackPressEvent() {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackFailedToShowHomeFragmentOnBackPressEvent();
    }

    @Override
    public void setCustomerUserId(String customerId) {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.setCustomerUserId(customerId);
    }

    @Override
    public void trackSingleConversationLoadedEvent(float duration) {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackSingleConversationLoadedEvent(duration);
    }

    @Override
    public void trackTaskDuration(String taskName, float duration) {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackTaskDuration(taskName, duration);
    }

    @Override
    public void logHandledException(String action, Throwable throwable) {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.logHandledException(action, throwable);
    }

    @Override
    public void trackOpenedCustomTab(boolean opened) {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackOpenedCustomTab(opened);
    }

    @Override
    public void trackInitialRatingPopupBackButton() {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackInitialRatingPopupBackButton();
    }

    @Override
    public void trackInitialRatingPopupTapOutside() {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackInitialRatingPopupTapOutside();
    }

    @Override
    public void trackInitialRatingPopupMaybeLater() {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackInitialRatingPopupMaybeLater();
    }

    @Override
    public void trackInitialRatingPopupRateApp() {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackInitialRatingPopupRateApp();
    }

    @Override
    public void trackReminderRatingPopupBackButton() {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackReminderRatingPopupBackButton();
    }

    @Override
    public void trackReminderRatingPopupTapOutside() {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackReminderRatingPopupTapOutside();
    }

    @Override
    public void trackReminderRatingPopupDismiss() {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackReminderRatingPopupDismiss();
    }

    @Override
    public void trackReminderRatingPopupRateApp() {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackReminderRatingPopupRateApp();
    }

    @Override
    public void trackSearchAdvertListLoadedEvent(float duration) {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackSearchAdvertListLoadedEvent(duration);
    }

    @Override
    public void trackUsingDeadFragmentEvent(Fragment fragment) {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.trackUsingDeadFragmentEvent(fragment);
    }

    @Override
    public void leaveBreadcrumb(String message) {
        for (AnalyticsTracker tracker : _lTracker)
            tracker.leaveBreadcrumb(message);
    }

}
