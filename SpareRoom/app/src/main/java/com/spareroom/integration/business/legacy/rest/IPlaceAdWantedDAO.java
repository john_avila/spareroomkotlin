package com.spareroom.integration.business.legacy.rest;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InvalidArgumentException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.exception.UnavailableDataSourceException;
import com.spareroom.integration.exception.UnexpectedResultException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.model.business.SpareroomStatus;

public interface IPlaceAdWantedDAO {

    SpareroomStatus create(DraftAdWanted draft)
            throws
            AuthenticationException,
            NetworkConnectivityException,
            ClientErrorException,
            ServerErrorException,
            InvalidJSONFormatException;

    Object read(String advertId)
            throws
            UnavailableDataSourceException,
            InvalidArgumentException,
            UnexpectedResultException;

    SpareroomStatus update(DraftAdWanted draft)
            throws
            UnavailableDataSourceException,
            InvalidArgumentException,
            UnexpectedResultException;

}
