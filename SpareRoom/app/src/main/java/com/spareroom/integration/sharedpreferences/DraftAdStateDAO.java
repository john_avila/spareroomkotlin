package com.spareroom.integration.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.spareroom.model.business.DraftAdState;
import com.spareroom.ui.screen.legacy.PlaceAdTitleDescriptionFragment;

public class DraftAdStateDAO {
    private final Context _context;
    private final static String PREFS_NAME = "draftAdState";
    private final static String OLD_WANTED_TITLE_DESCRIPTION_SCREEN = "com.spareroom.ui.screen.legacy.PlaceAdWantedTitleDescriptionFragment";
    private final static String OLD_OFFERED_TITLE_DESCRIPTION_SCREEN = "com.spareroom.ui.screen.legacy.PlaceAdOfferedTitleDescriptionFragment";

    public DraftAdStateDAO(Context context) {
        _context = context;
    }

    public DraftAdState read() {
        SharedPreferences prefs = _context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        DraftAdState draftAdState = new DraftAdState();
        draftAdState.set_placeAdState(prefs.getString("state", null));
        String screen = prefs.getString("screen", null);
        draftAdState.set_screen(isUsingOldTitleDescriptionName(screen) ? PlaceAdTitleDescriptionFragment.class.getName() : screen);

        return draftAdState;
    }

    public boolean update(DraftAdState state) {
        SharedPreferences prefs = _context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("state", state.get_placeAdState());
        editor.putString("screen", state.get_screen());
        return editor.commit();
    }

    public boolean delete() {
        SharedPreferences prefs = _context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        return editor.commit();
    }

    private boolean isUsingOldTitleDescriptionName(String screen) {
        return OLD_OFFERED_TITLE_DESCRIPTION_SCREEN.equalsIgnoreCase(screen) || OLD_WANTED_TITLE_DESCRIPTION_SCREEN.equalsIgnoreCase(screen);
    }
}
