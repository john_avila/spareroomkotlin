package com.spareroom.integration.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

import java.net.HttpCookie;
import java.util.LinkedList;
import java.util.List;

public class CookieDAO {

    private static final String PREFS_NAME = "cookie";

    public boolean save(Context context, List<HttpCookie> cookie) {
        SharedPreferences sp = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor spe = sp.edit();
        boolean result = true;

        spe.putInt("num_cookies", cookie.size());
        for (int i = 0; i < cookie.size(); i++) {
            spe.putString("comment" + Integer.toString(i), cookie.get(i).getComment());
            spe.putString("comment_url" + Integer.toString(i), cookie.get(i).getCommentURL());
            spe.putString("domain" + Integer.toString(i), cookie.get(i).getDomain());
            spe.putString("name" + Integer.toString(i), cookie.get(i).getName());
            spe.putString("path" + Integer.toString(i), cookie.get(i).getPath());
            spe.putString("value" + Integer.toString(i), cookie.get(i).getValue());
            spe.putLong("expiry" + Integer.toString(i), cookie.get(i).getMaxAge());
            spe.putInt("version" + Integer.toString(i), cookie.get(i).getVersion());

            if (!spe.commit())
                result = false;

        }

        return result;

    }

    public List<HttpCookie> load(Context context) {
        SharedPreferences sp = context.getSharedPreferences(PREFS_NAME, 0);
        int numCookies = sp.getInt("num_cookies", 0);
        List<HttpCookie> lCookies = new LinkedList<>();

        for (int i = 0; i < numCookies; i++) {
            String name = sp.getString("name" + Integer.toString(i), "");
            String value = sp.getString("value" + Integer.toString(i), "");

            if (name.equals("") || value.equals(""))
                return null;

            HttpCookie c = new HttpCookie(name, value);
            c.setComment(sp.getString("comment" + Integer.toString(i), ""));
            c.setDomain(sp.getString("domain" + Integer.toString(i), ""));
            c.setPath(sp.getString("path" + Integer.toString(i), ""));
            c.setMaxAge(sp.getLong("expiry" + Integer.toString(i), 0));
            c.setVersion(sp.getInt("version" + Integer.toString(i), 0));

            lCookies.add(c);
        }

        return lCookies;
    }

    public boolean delete(Context context) {
        SharedPreferences sp = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor spe = sp.edit();
        spe.clear();
        return spe.commit();
    }
}
