package com.spareroom.integration.business.legacy.rest.apiv1;

import com.spareroom.integration.business.legacy.rest.IPostcodeFinderDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.business.legacy.rest.apiv1.util.PostcodeJSONParser;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.integration.webservice.rest.service.LocationRestService;
import com.spareroom.model.business.Parameters;

public class PostcodeFinderDAO implements IPostcodeFinderDAO {

    private final LocationRestService locationRestService;

    PostcodeFinderDAO() {
        locationRestService = new LocationRestService();
    }

    public Object read(Parameters parameters) throws AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException {
        parameters.add("format", "json");
        parameters.add("function", "lookuppostcode");

        String response = locationRestService.findPostcode(parameters);

        PostcodeJSONParser parser = new PostcodeJSONParser();

        return parser.parse(response);
    }
}
