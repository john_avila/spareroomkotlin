package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import java.util.LinkedList;

@SuppressWarnings("rawtypes")
public class AsyncTaskManager {

    private LinkedList<AsyncTask> _runningTasks;
    private LinkedList<IEmptyAsyncTaskManagerListener> _listenersEmptyManager;

    public AsyncTaskManager() {
        _runningTasks = new LinkedList<>();
        _listenersEmptyManager = new LinkedList<>();
    }

    public void register(AsyncTask t) {
        _runningTasks.add(t);
    }

    public void remove(AsyncTask t) {
        _runningTasks.remove(t);
        if (_runningTasks.size() == 0)
            notifyListeners();
    }

    public void cancelAll() {
        for (AsyncTask t : _runningTasks)
            t.cancel(true);
    }

    public void registerSizeListener(IEmptyAsyncTaskManagerListener listener) {
        _listenersEmptyManager.add(listener);
    }

    private void notifyListeners() {
        for (IEmptyAsyncTaskManagerListener listener : _listenersEmptyManager) {
            listener.notifyEmptyManager();
        }
    }
}
