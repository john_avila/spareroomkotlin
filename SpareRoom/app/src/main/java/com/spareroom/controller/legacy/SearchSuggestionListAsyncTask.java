package com.spareroom.controller.legacy;

import android.content.Context;
import android.os.AsyncTask;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.SearchAutocompleteList;
import com.spareroom.model.exception.InvalidUserException;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;
import com.spareroom.model.legacy.ISearchFacade;

public class SearchSuggestionListAsyncTask extends AsyncTask<Object, Integer, Object> {
    private ISearchFacade _sf;
    private IAsyncResult _targetActivity;
    private SpareroomContext _srContext;

    public SearchSuggestionListAsyncTask(ISearchFacade mf, SpareroomContext srContext, IAsyncResult a) {
        _sf = mf;
        _targetActivity = a;
        _srContext = srContext;
    }

    @Override
    protected Object doInBackground(Object... params) {

        try {
            return _sf.autocomplete((Context) params[0], (String) params[1]);
        } catch (AuthenticationException e) {
            return e;
        } catch (NetworkConnectivityException e) {
            return e;
        } catch (ClientErrorException e) {
            return e;
        } catch (ServerErrorException e) {
            return e;
        } catch (InvalidJSONFormatException e) {
            return e;
        }

    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);
        if ((result != null) && (result instanceof SearchAutocompleteList)) {
            _targetActivity.update(result);
        } else if (result instanceof ServiceUnavailableException) {
            _targetActivity.handleServiceUnavailableException(((ServiceUnavailableException) result).getMessage());
        } else if (result instanceof NetworkConnectivityException) {
            _targetActivity.handleServiceUnavailableException(((NetworkConnectivityException) result).getMessage());
        } else if (result instanceof MissingSystemFeatureException) {
            _targetActivity.handleMissingSystemFeatureException(null);
        } else if (result instanceof InconsistentStateException) {
            _targetActivity.handleInconsistentStateException(null);
        } else if (result instanceof InvalidUserException) {
            _targetActivity.handleInvalidUserException(null);
        } else if (result instanceof InvalidJSONFormatException) {
            _targetActivity.handleInconsistentStateException(null);
        } else if (result instanceof AuthenticationException) {
            _targetActivity.handleInconsistentStateException(null);
        } else if (result instanceof ClientErrorException) {
            _targetActivity.handleServiceUnavailableException(null);
        } else if (result instanceof ServerErrorException) {
            _targetActivity.handleServiceUnavailableException(null);
        }

    }

}
