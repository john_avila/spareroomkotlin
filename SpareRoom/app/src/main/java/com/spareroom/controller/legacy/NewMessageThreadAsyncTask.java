package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.controller.interceptor.Interceptor;
import com.spareroom.controller.interceptor.LoggedInInterceptor;
import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.exception.InvalidUserException;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;
import com.spareroom.model.legacy.IMessageFacade;

public class NewMessageThreadAsyncTask extends AsyncTask<Parameters, Integer, Object> {
    private IMessageFacade _mf;
    private IAsyncResult _targetActivity;
    private SpareroomContext _srContext;

    public NewMessageThreadAsyncTask(IMessageFacade mf, SpareroomContext srContext, IAsyncResult a) {
        _mf = mf;
        _targetActivity = a;
        _srContext = srContext;
    }

    @Override
    protected Object doInBackground(Parameters... params) {

        Interceptor i = new LoggedInInterceptor(_srContext);
        if (i.execute((Object[]) null) != Interceptor.SUCCESS) {
            return new InvalidUserException();
        }

        try {
            return _mf.newMessageThread(params[0]);
        } catch (ServiceUnavailableException e) {
            return e;
        } catch (InconsistentStateException e) {
            return e;
        } catch (MissingSystemFeatureException e) {
            return e;
        }

    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);

        if (!(result instanceof Exception)) {
            _targetActivity.update(result);
        } else if (result instanceof ServiceUnavailableException) {
            _targetActivity.handleServiceUnavailableException(null);
        } else if (result instanceof InconsistentStateException) {
            _targetActivity.handleInconsistentStateException(null);
        } else if (result instanceof MissingSystemFeatureException) {
            _targetActivity.handleMissingSystemFeatureException(null);
        } else if (result instanceof InvalidUserException) {
            _targetActivity.handleInvalidUserException(null);
        }

    }

}
