package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;
import com.spareroom.model.legacy.ILocationFacade;

public class Point2PostcodeAsyncTask extends AsyncTask<Parameters, Integer, Object> {
    private ILocationFacade _lf;
    private IAsyncResult _targetActivity;
    private SpareroomContext _srContext;

    public Point2PostcodeAsyncTask(ILocationFacade lf, SpareroomContext srContext, IAsyncResult a) {
        _lf = lf;
        _targetActivity = a;
        _srContext = srContext;
    }

    @Override
    protected Object doInBackground(Parameters... params) {

        try {
            return _lf.getPostcodeUnit(params[0]);
        } catch (ServiceUnavailableException e) {
            return e;
        } catch (InconsistentStateException e) {
            return e;
        } catch (MissingSystemFeatureException e) {
            return e;
        }

    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);
        if (result instanceof SpareroomStatus) {
            _targetActivity.handleInconsistentStateException(null);
        } else if (!(result instanceof Exception)) {
            _targetActivity.update(result);
        } else if (result instanceof ServiceUnavailableException) {
            _targetActivity.handleServiceUnavailableException(null);
        } else if (result instanceof InconsistentStateException) {
            _targetActivity.handleInconsistentStateException(null);
        } else if (result instanceof MissingSystemFeatureException) {
            _targetActivity.handleMissingSystemFeatureException(null);
        }

    }
}
