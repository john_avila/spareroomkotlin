package com.spareroom.controller;

import com.spareroom.model.UploadPictureModelAction;

public class UploadPictureControllerAction extends ControllerActionImpl {

    private UploadPictureControllerAction(SpareroomContext spareroomContext, ControllerActionObserver observer) {
        super(spareroomContext, new UploadPictureModelAction(spareroomContext), observer);
    }

    public static UploadPictureControllerAction get(ControllerActionObserver observer) {
        return new UploadPictureControllerAction(SpareroomApplication.getSpareRoomContext(), observer);
    }
}
