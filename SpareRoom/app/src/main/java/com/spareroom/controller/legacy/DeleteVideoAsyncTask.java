package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.legacy.IAccountFacade;

/**
 * Async for remove a video in the advert
 * </p>
 * Created by miguel.rossi on 26/08/2016.
 */
public class DeleteVideoAsyncTask extends AsyncTask<Parameters, Integer, Object> {
    private final IAccountFacade _iAccountFacade;
    private final IAsyncResult _targetActivity;

    public DeleteVideoAsyncTask(IAccountFacade iAccountFacade, IAsyncResult iAsyncResult) {
        _iAccountFacade = iAccountFacade;
        _targetActivity = iAsyncResult;
    }

    @Override
    protected Object doInBackground(Parameters... parameters) {

        try {
            _iAccountFacade.removeVideo(parameters[0]);
            return null;

        } catch (NetworkConnectivityException e) {
            return e;
        } catch (InvalidJSONFormatException e) {
            return e;
        } catch (AuthenticationException e) {
            return e;
        } catch (ClientErrorException e) {
            return e;
        } catch (ServerErrorException e) {
            return e;
        }

    }

    @Override
    protected void onPostExecute(Object businessObject) {
        super.onPostExecute(businessObject);

        if (!(businessObject instanceof Exception)) {
            _targetActivity.update(businessObject);

        } else if (businessObject instanceof NetworkConnectivityException) {
            _targetActivity.handleServiceUnavailableException(
                    ((NetworkConnectivityException) businessObject).getMessage());

        } else if (businessObject instanceof InvalidJSONFormatException) {
            _targetActivity.handleInconsistentStateException(
                    ((InvalidJSONFormatException) businessObject).getMessage());

        } else if (businessObject instanceof AuthenticationException) {
            _targetActivity.handleInconsistentStateException(
                    ((AuthenticationException) businessObject).getMessage());

        } else if (businessObject instanceof ClientErrorException) {
            _targetActivity.handleInconsistentStateException(
                    ((ClientErrorException) businessObject).getMessage());

        } else if (businessObject instanceof ServerErrorException) {
            _targetActivity.handleServiceUnavailableException(
                    ((ServerErrorException) businessObject).getMessage());

        }
    }

}
