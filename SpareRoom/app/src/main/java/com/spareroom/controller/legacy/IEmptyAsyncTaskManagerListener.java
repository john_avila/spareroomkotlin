package com.spareroom.controller.legacy;

public interface IEmptyAsyncTaskManagerListener {
    void notifyEmptyManager();
}
