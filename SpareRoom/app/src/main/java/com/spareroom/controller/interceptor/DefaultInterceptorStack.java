package com.spareroom.controller.interceptor;

/**
 * Default stack of interceptors that checks matching number of arguments, type and whether they
 * can be null or not.
 * <p>
 * Created by manuel on 23/06/2016.
 */
public class DefaultInterceptorStack extends InterceptorStack {

    public DefaultInterceptorStack(Class[] vExpectedParameter, boolean[] vIsNullableParameter) {
        add(new ArgumentNumberInterceptor(vExpectedParameter));
        add(new ArgumentTypeInterceptor(vExpectedParameter));
        add(new ArgumentNullInterceptor(vExpectedParameter, vIsNullableParameter));
    }
}
