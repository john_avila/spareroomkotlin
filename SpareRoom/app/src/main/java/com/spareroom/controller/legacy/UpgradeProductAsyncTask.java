package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.controller.interceptor.Interceptor;
import com.spareroom.controller.interceptor.LoggedInInterceptor;
import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.exception.MissingParameterException;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;
import com.spareroom.model.legacy.IAccountFacade;

public class UpgradeProductAsyncTask extends AsyncTask<Parameters, Integer, Object> {

    private IAccountFacade _modelFacade;
    private IAsyncResult _activity;
    private SpareroomContext _srContext;

    public UpgradeProductAsyncTask(IAccountFacade modelFacade, SpareroomContext srContext, IAsyncResult a) {
        _modelFacade = modelFacade;
        _activity = a;
        _srContext = srContext;
    }

    @Override
    protected Object doInBackground(Parameters... params) {

        Interceptor i = new LoggedInInterceptor(_srContext);
        if (i.execute(params[0]) != Interceptor.SUCCESS) {
            return new MissingParameterException();
        }

        try {
            return _modelFacade.upgrade(params[0]);
        } catch (MissingSystemFeatureException e) {
            return e;
        } catch (ServiceUnavailableException e) {
            return e;
        } catch (InconsistentStateException e) {
            return e;
        }

    }

    @Override
    protected void onPostExecute(Object result) {
        if (result instanceof SpareroomStatus) {
            _activity.update(result);
        } else if (result instanceof ServiceUnavailableException) {
            _activity.handleServiceUnavailableException(null);
        } else if (result instanceof InconsistentStateException) {
            _activity.handleInconsistentStateException(null);
        } else if (result instanceof MissingSystemFeatureException) {
            _activity.handleMissingSystemFeatureException(null);
        } else if (result instanceof MissingParameterException) {
            _activity.handleMissingParameterException(null);
        }
    }

}
