package com.spareroom.controller.interceptor;

/**
 * Interceptor that checks that the number of parameters expected and provided match.
 * <p>
 * Created by manuel on 23/06/2016.
 */
public class ArgumentNumberInterceptor extends Interceptor {
    private Class[] _vExpectedParameter;

    /**
     * Contructor.
     *
     * @param vExpectedParameter array with expected kind of parameters
     */
    public ArgumentNumberInterceptor(Class... vExpectedParameter) {
        _vExpectedParameter = vExpectedParameter;
    }

    /**
     * Executes the interceptor
     *
     * @param parameters array of parameters that need to comply with the expected definition in the
     *                   constructor.
     * @return <code>Interceptor.SUCCESS</code> if succeeds,
     * <code>Interceptor.ERROR</code> or other custom value (greater than 1) if there was an error
     */
    @Override
    public int execute(Object... parameters) {
        if ((_vExpectedParameter == null) && ((parameters == null) || (parameters.length == 0)))
            return SUCCESS;
        else if (_vExpectedParameter.length == parameters.length)
            return SUCCESS;

        return ERROR;
    }
}
