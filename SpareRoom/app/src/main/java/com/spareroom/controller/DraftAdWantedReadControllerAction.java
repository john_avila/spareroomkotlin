package com.spareroom.controller;

import com.spareroom.controller.interceptor.DefaultInterceptorStack;
import com.spareroom.lib.core.Action;
import com.spareroom.model.DraftAdWantedReadModelAction;

/**
 * Created by manuel on 08/06/2016.
 */
public class DraftAdWantedReadControllerAction extends ControllerAction {

    /**
     * Constructor.
     *
     * @param sc SpareRoom app platform resources
     */
    public DraftAdWantedReadControllerAction(final SpareroomContext sc) {

        addInterceptor(new DefaultInterceptorStack(
                new Class[]{String.class},
                new boolean[]{false}
        ));

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {

                DraftAdWantedReadModelAction draftAdWantedReadModelAction
                        = new DraftAdWantedReadModelAction(sc);
                observe(draftAdWantedReadModelAction);
                draftAdWantedReadModelAction.execute(params);

                return null;

            } //end execute(Object... params) throws Exception
        });

    } //DraftAdOfferedReadControllerAction(final SpareroomContext sc)

}
