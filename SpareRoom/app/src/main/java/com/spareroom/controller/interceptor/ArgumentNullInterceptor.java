package com.spareroom.controller.interceptor;

/**
 * Interceptor that checks whether the parameters provided comply with the null constraints
 * specified in the Constructor.
 * <p>
 * Created by manuel on 23/06/2016.
 */
public class ArgumentNullInterceptor extends Interceptor {
    private Class[] _vExpectedParameters;
    private boolean[] _vIsNullableParameter;

    /**
     * Contructor.
     *
     * @param expectedParameters array with expected kind of parameters
     * @param nullableParameters array with nullable parameters
     */
    public ArgumentNullInterceptor(Class[] expectedParameters, boolean[] nullableParameters) {
        _vExpectedParameters = expectedParameters;
        _vIsNullableParameter = nullableParameters;
    }

    /**
     * Executes the interceptor
     *
     * @param parameters array of parameters that need to comply with the expected definition in the
     *                   constructor.
     * @return <code>Interceptor.SUCCESS</code> if succeeds,
     * <code>Interceptor.ERROR</code> or other custom value (greater than 1) if there was an error
     */
    @Override
    public int execute(Object... parameters) {
        if ((_vExpectedParameters == null) && (_vIsNullableParameter == null))
            return Interceptor.SUCCESS;
        if (_vExpectedParameters.length != _vIsNullableParameter.length)
            return Interceptor.ERROR;
        else if (parameters.length != _vIsNullableParameter.length)
            return Interceptor.ERROR;
        else {
            for (int i = 0; i < parameters.length; i++) {
                if ((parameters[i] == null) && (_vIsNullableParameter[i] == false))
                    return Interceptor.ERROR;
            }
            return Interceptor.SUCCESS;
        }
    }
}
