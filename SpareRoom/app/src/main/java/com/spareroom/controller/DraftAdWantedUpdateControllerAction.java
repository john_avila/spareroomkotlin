package com.spareroom.controller;

import com.spareroom.controller.interceptor.DefaultInterceptorStack;
import com.spareroom.lib.core.Action;
import com.spareroom.model.DraftAdWantedUpdateModelAction;
import com.spareroom.model.business.DraftAdWanted;

/**
 * Created by miguel.rossi on 18/05/2016.
 */
public class DraftAdWantedUpdateControllerAction extends ControllerAction {

    /**
     * Constructor.
     *
     * @param sc SpareRoom app platform resources
     */
    public DraftAdWantedUpdateControllerAction(final SpareroomContext sc) {

        addInterceptor(new DefaultInterceptorStack(
                new Class[]{DraftAdWanted.class},
                new boolean[]{false}
        ));

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {
                DraftAdWantedUpdateModelAction draftAdWantedUpdateModelAction = new DraftAdWantedUpdateModelAction(sc);
                observe(draftAdWantedUpdateModelAction);
                draftAdWantedUpdateModelAction.execute(params);

                return null;
            }
        });

    }

}
