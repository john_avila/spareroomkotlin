package com.spareroom.controller;

import com.spareroom.App;
import com.spareroom.controller.interceptor.DefaultInterceptorStack;
import com.spareroom.lib.core.Action;
import com.spareroom.model.LogOutModelAction;
import com.spareroom.ui.util.NotificationUtils;

/**
 * <code>ControllerAction</code> for log out in to an account.
 */

public class LogOutControllerAction extends ControllerAction {

    public LogOutControllerAction(final SpareroomContext spareroomContext) {

        spareroomContext.deleteUserSession();

        FirebaseIdController.unregister(spareroomContext);

        NotificationUtils.closeNotifications(App.get());

        addInterceptor(new DefaultInterceptorStack(null, null));

        setAction(new Action() {
            @Override
            public Object execute(Object... params) {

                LogOutModelAction modelAction = new LogOutModelAction(spareroomContext);
                observe(modelAction);
                modelAction.execute(params);

                return null;
            }
        });

    }
}
