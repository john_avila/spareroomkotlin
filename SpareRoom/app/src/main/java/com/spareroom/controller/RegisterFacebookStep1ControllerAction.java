package com.spareroom.controller;

import com.spareroom.controller.interceptor.DefaultInterceptorStack;
import com.spareroom.lib.core.Action;
import com.spareroom.model.RegisterFacebookStep1ModelAction;
import com.spareroom.model.business.AccountRegisterFormFacebook;
import com.spareroom.model.extra.RegisterExtra;

/**
 * <code>ControllerAction</code> for registering a new account
 * <p>
 * Created by manuel on 29/06/2016.
 */
public class RegisterFacebookStep1ControllerAction extends ControllerAction {

    /**
     * Constructor.
     *
     * @param spareroomContext SpareRoom app platform resources
     */
    public RegisterFacebookStep1ControllerAction(final SpareroomContext spareroomContext) {

        addInterceptor(new DefaultInterceptorStack(
                new Class[]{AccountRegisterFormFacebook.class, RegisterExtra.class},
                new boolean[]{false, true}
        ));

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {
                RegisterFacebookStep1ModelAction modelAction =
                        new RegisterFacebookStep1ModelAction(spareroomContext);
                observe(modelAction);
                modelAction.execute(params);

                return null;
            }
        });
    }

}
