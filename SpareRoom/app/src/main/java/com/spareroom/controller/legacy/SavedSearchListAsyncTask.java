package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.controller.interceptor.Interceptor;
import com.spareroom.controller.interceptor.LoggedInInterceptor;
import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SavedSearchList;
import com.spareroom.model.exception.InvalidUserException;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;
import com.spareroom.model.legacy.ISearchFacade;

public class SavedSearchListAsyncTask extends AsyncTask<Parameters, Integer, Object> {
    private ISearchFacade _modelFacade;
    private IAsyncResult _activity;
    private SpareroomContext _srContext;

    public SavedSearchListAsyncTask(ISearchFacade modelFacade, SpareroomContext srContext, IAsyncResult a) {
        _modelFacade = modelFacade;
        _activity = a;
        _srContext = srContext;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(Parameters... params) {
        Interceptor i = new LoggedInInterceptor(_srContext);
        if (i.execute((Object[]) null) != Interceptor.SUCCESS) {
            return new InvalidUserException();
        }

        try {
            return _modelFacade.getSavedSearches(params[0]);
        } catch (ServiceUnavailableException e) {
            return e;
        } catch (InconsistentStateException e) {
            return e;
        } catch (MissingSystemFeatureException e) {
            return e;
        }

    }

    @Override
    protected void onPostExecute(Object result) {
        if (result instanceof SavedSearchList) {
            _activity.update(result);
        } else if (result instanceof ServiceUnavailableException) {
            _activity.handleServiceUnavailableException(((ServiceUnavailableException) result).getMessage());
        } else if (result instanceof InconsistentStateException) {
            _activity.handleInconsistentStateException(null);
        } else if (result instanceof MissingSystemFeatureException) {
            _activity.handleMissingSystemFeatureException(null);
        } else if (result instanceof InvalidUserException) {
            _activity.handleInvalidUserException(null);
        }
    }
}