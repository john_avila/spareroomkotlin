package com.spareroom.controller;

import com.spareroom.lib.core.LongTask;
import com.spareroom.model.ModelActionObserver;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.legacy.VideoUploadModelAction;

/**
 * Created by miguel.rossi on 22/09/2016.
 */
public class UploadVideoTask extends LongTask {
    public final static int ID = 321231;

    private ModelActionObserver _modelActionObserver = (observable, resultType, result) -> {
        switch (resultType) {
            case RESULT:
                notifyObservers(null);
                break;
            case SPAREROOM_STATUS:
                notifyObservers(result);
                break;
            case EXCEPTION:
                notifyObservers(result);
                break;
        }

    };

    public UploadVideoTask(final SpareroomContext srContext, final Parameters parameters) {

        setRunnable(new ModelRunnable(srContext, parameters));

    }

    @Override
    public int getId() {
        return ID;
    }

    private class ModelRunnable implements Runnable {
        private SpareroomContext _srContext;
        private Parameters _parameters;

        public ModelRunnable(final SpareroomContext srContext, final Parameters parameters) {
            _srContext = srContext;
            _parameters = parameters;

        }

        @Override
        public void run() {
            VideoUploadModelAction modelAction = new VideoUploadModelAction(_srContext);
            modelAction.addModelActionObserver(_modelActionObserver);
            modelAction.execute(
                    _parameters,
                    new InterruptionObserver());
        }
    }

}
