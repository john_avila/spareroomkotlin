package com.spareroom.controller;

import android.content.Context;

import com.google.firebase.iid.FirebaseInstanceId;
import com.spareroom.integration.sharedpreferences.PushTransactionDAO;
import com.spareroom.model.business.SpareroomStatus;

/**
 * This class implement the management for the Firebase Id.
 * <p/>
 * Created by miguel.rossi on 05/01/2017.
 */

public class FirebaseIdController {

    /**
     * Registers the Firebase Id in the SpareRoom server
     */
    public static void register(SpareroomContext spareroomContext) {
        String firebaseId = FirebaseInstanceId.getInstance().getToken();

        // It can be null if the token has not yet been generated.
        if (firebaseId != null) {
            FirebaseIdRegisterControllerAction firebaseIdRegisterControllerAction =
                    new FirebaseIdRegisterControllerAction(spareroomContext);
            ControllerActionObserver controllerActionObserver =
                    new FirebaseIdControllerActionObserver(
                            spareroomContext.get_applicationContext(),
                            PushTransactionDAO.Process.REGISTER);

            firebaseIdRegisterControllerAction.addControllerActionObserver(controllerActionObserver);
            firebaseIdRegisterControllerAction.execute(firebaseId);
        }

    }

    /**
     * Unregisters the Firebase Id in the SpareRoom server
     */
    public static void unregister(SpareroomContext spareroomContext) {
        String firebaseId = FirebaseInstanceId.getInstance().getToken();

        // It can be null if the token has not yet been generated.
        if (firebaseId != null) {
            FirebaseIdUnregisterControllerAction firebaseIdUnregisterControllerAction =
                    new FirebaseIdUnregisterControllerAction(spareroomContext);
            ControllerActionObserver controllerActionObserver =
                    new FirebaseIdControllerActionObserver(
                            spareroomContext.get_applicationContext(),
                            PushTransactionDAO.Process.UNREGISTER);

            firebaseIdUnregisterControllerAction.addControllerActionObserver(controllerActionObserver);
            firebaseIdUnregisterControllerAction.execute(firebaseId);
        }

    }

    private static class FirebaseIdControllerActionObserver implements ControllerActionObserver {
        private Context _context;
        private PushTransactionDAO.Process _process;

        FirebaseIdControllerActionObserver(Context context, PushTransactionDAO.Process process) {
            _context = context;
            _process = process;
        }

        @Override
        public void onResult(Object o) {
            PushTransactionDAO.update(_context, _process, PushTransactionDAO.Status.SUCCESSFUL);
        }

        @Override
        public void onSpareroomStatus(SpareroomStatus status) {
            PushTransactionDAO.update(_context, _process, PushTransactionDAO.Status.UNSUCCESSFUL);
        }

        @Override
        public void onException(Exception exception) {
            PushTransactionDAO.update(_context, _process, PushTransactionDAO.Status.UNSUCCESSFUL);
        }
    }

}
