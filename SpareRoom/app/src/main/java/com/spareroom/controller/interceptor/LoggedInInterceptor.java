package com.spareroom.controller.interceptor;

import com.spareroom.controller.SpareroomContext;

public class LoggedInInterceptor extends Interceptor {

    protected SpareroomContext _spareroomContext;

    public LoggedInInterceptor(SpareroomContext spareroomContext) {
        _spareroomContext = spareroomContext;
    }

    @Override
    public int execute(Object... parameters) {
        if (_spareroomContext.getSession() != null)
            return SUCCESS;
        return ERROR;
    }
}
