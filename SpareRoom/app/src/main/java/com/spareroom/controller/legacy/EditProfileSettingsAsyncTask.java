package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.controller.interceptor.Interceptor;
import com.spareroom.controller.interceptor.LoggedInInterceptor;
import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.ProfilePreferenceList;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.business.SpareroomStatusCode;
import com.spareroom.model.exception.InvalidUserException;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;
import com.spareroom.model.legacy.IAccountFacade;

public class EditProfileSettingsAsyncTask extends AsyncTask<Parameters, Integer, Object> {
    private IAccountFacade _af;
    private IAsyncResult _targetActivity;
    private SpareroomContext _srContext;

    public EditProfileSettingsAsyncTask(IAccountFacade af, SpareroomContext srContext, IAsyncResult a) {
        _af = af;
        _targetActivity = a;
        _srContext = srContext;
    }

    @Override
    protected Object doInBackground(Parameters... params) {
        Interceptor i = new LoggedInInterceptor(_srContext);
        if (i.execute((Object[]) null) != Interceptor.SUCCESS) {
            return new InvalidUserException();
        }

        try {
            return _af.getEmailPreferences();
        } catch (MissingSystemFeatureException e) {
            return e;
        } catch (ServiceUnavailableException e) {
            return e;
        } catch (InconsistentStateException e) {
            return e;
        }

    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);

        if (result instanceof ProfilePreferenceList) {
            _targetActivity.update(result);
        } else if (result instanceof SpareroomStatus && ((SpareroomStatus) result).getCode().equals(SpareroomStatusCode.NOT_LOGGED_IN)) {
            _targetActivity.handleInvalidUserException(null);
        } else if (result instanceof ServiceUnavailableException) {
            _targetActivity.handleServiceUnavailableException(((ServiceUnavailableException) result).getMessage());
        } else if (result instanceof MissingSystemFeatureException) {
            _targetActivity.handleMissingSystemFeatureException(null);
        } else if (result instanceof InconsistentStateException) {
            _targetActivity.handleInconsistentStateException(null);
        } else if (result instanceof InvalidUserException) {
            _targetActivity.handleInvalidUserException(null);
        }
    }
}
