package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.model.business.Parameters;
import com.spareroom.model.legacy.ISearchFacade;

public class ClockFeaturedAsyncTask extends AsyncTask<Parameters, Integer, Object> {
    private ISearchFacade _modelFacade;

    public ClockFeaturedAsyncTask(ISearchFacade modelFacade) {
        _modelFacade = modelFacade;
    }

    @Override
    protected Object doInBackground(Parameters... params) {
        try {
            return _modelFacade.clockFeatured(params[0]);
        } catch (Exception e) {
            return e;
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        // do nothing
    }
}
