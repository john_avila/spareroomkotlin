package com.spareroom.controller;

import com.spareroom.controller.interceptor.DefaultInterceptorStack;
import com.spareroom.lib.core.Action;
import com.spareroom.model.DraftAdOfferedReadModelAction;

/**
 * Created by manuel on 08/06/2016.
 */
public class DraftAdOfferedReadControllerAction extends ControllerAction {

    /**
     * Constructor.
     *
     * @param sc SpareRoom app platform resources
     */
    public DraftAdOfferedReadControllerAction(final SpareroomContext sc) {

        addInterceptor(new DefaultInterceptorStack(
                new Class[]{String.class},
                new boolean[]{false}
        ));

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {

                DraftAdOfferedReadModelAction draftAdOfferedReadModelAction
                        = new DraftAdOfferedReadModelAction(sc);
                observe(draftAdOfferedReadModelAction);
                draftAdOfferedReadModelAction.execute(params);

                return null;

            } //end execute(Object... params) throws Exception
        });

    } //DraftAdOfferedReadControllerAction(final SpareroomContext sc)

}
