package com.spareroom.controller.interceptor;

/**
 * Class that runs checks on data provided and determines whether they are valid or not.
 */
public abstract class Interceptor {

    public final static int SUCCESS = 0;
    public final static int ERROR = 1;

    /**
     * Executes the interceptor
     *
     * @param parameters passed to the task
     * @return 0 if succeeds, greater than 0 if there was an error (1 standard error)
     */
    abstract public int execute(Object... parameters);
}
