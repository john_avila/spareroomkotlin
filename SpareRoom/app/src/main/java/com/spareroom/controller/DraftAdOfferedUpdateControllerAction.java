package com.spareroom.controller;

import com.spareroom.controller.interceptor.DefaultInterceptorStack;
import com.spareroom.lib.core.Action;
import com.spareroom.model.DraftAdOfferedUpdateModelAction;
import com.spareroom.model.business.DraftAdOffered;

/**
 * Created by miguel.rossi on 18/05/2016.
 */
public class DraftAdOfferedUpdateControllerAction extends ControllerAction {

    /**
     * Constructor.
     *
     * @param sc SpareRoom app platform resources
     */
    public DraftAdOfferedUpdateControllerAction(final SpareroomContext sc) {

        addInterceptor(new DefaultInterceptorStack(
                new Class[]{DraftAdOffered.class},
                new boolean[]{false}
        ));

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {
                DraftAdOfferedUpdateModelAction draftAdOfferedUpdateModelAction = new DraftAdOfferedUpdateModelAction(sc);
                observe(draftAdOfferedUpdateModelAction);
                draftAdOfferedUpdateModelAction.execute(params);

                return null;
            }
        });

    }

}
