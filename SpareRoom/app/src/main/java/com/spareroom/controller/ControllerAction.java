package com.spareroom.controller;

import android.app.Activity;
import android.os.AsyncTask;

import com.spareroom.controller.exception.ControllerInvalidArgumentException;
import com.spareroom.controller.interceptor.Interceptor;
import com.spareroom.lib.core.Action;
import com.spareroom.lib.core.ObserverLifecycleInterface;
import com.spareroom.model.ModelAction;
import com.spareroom.model.ModelActionObserver;
import com.spareroom.model.business.SpareroomStatus;

import java.util.LinkedList;

/**
 * Operation performed by Controller in order to invoke the Model. This implementation is based on
 * AsyncTask from the Android library and it additionally implements the Observer pattern. Extend
 * this class for each Controller action. Implement Parameter handling and Controller logic (Model
 * invocation) in <code>doInBackground</code> and result handling on <code>onPostExecute</code>.
 * Always call <code>super</code> for the previous methods.
 * <p>
 * Created by manuel on 17/05/2016.
 */
public abstract class ControllerAction
        extends AsyncTask<Object, Integer, Void>
        implements ObserverLifecycleInterface {

    /**
     * List of interceptors to be run before executing this <code>ControllerAction</code>
     */
    private LinkedList<Interceptor> _listInterceptors = new LinkedList<>();
    /**
     * List of observers currently observing <code>ControllerAction</code>
     */
    private LinkedList<ControllerActionObserver> _listObservers =
            new LinkedList<>();

    /**
     * Kind of result returned by the Model
     */
    private ModelAction.ResultType _resultType;

    /**
     * Action to be performed.
     */
    private Action _action = null;

    /**
     * Kind of result returned by the Model
     */
    private Object _result;

    /**
     * Observer that will receive the result from performing an operation on the Model
     */
    private ModelActionObserver _modelActionObserver = new ModelActionObserver() {
        @Override
        public void notifyModelActionResult(ModelAction observable,
                                            ModelAction.ResultType resultType,
                                            Object result) {
            _resultType = resultType;
            _result = result;
        }
    };

    protected final void observe(ModelAction modelAction) {
        modelAction.addModelActionObserver(_modelActionObserver);
    }

    /**
     * Sets the <code>Action</code> containing the Controller logic for this
     * <code>ControllerAction</code>. The action must return result of the operation,
     * <code>null</code> if there was no result from performing the operation (procedure / void
     * method). Exceptions should be thrown.
     *
     * @param action the operation containing the controller logic for this
     *               <code>ControllerAction</code>
     */
    protected void setAction(Action action) {
        _action = action;
    }

    /**
     * Adds an interceptor for checking integrity aspects in the request (not business logic)
     *
     * @param i the interceptor
     */
    protected void addInterceptor(Interceptor i) {
        _listInterceptors.add(i);
    }

    @Override
    protected final void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * Performs the Controller logic in a background process. Do parameter handling and Model
     * invocation here. If Model is invoked, add  <code>_modelActionObserver</code> to the
     * <code>ModelAction</code> through <code>ModelAction.addObserver</code>
     *
     * @param params data needed to perform this operation
     * @return <code>null</code>, as results from the operations should be handled via
     * ModelActionObserver.
     */
    @Override
    protected final Void doInBackground(Object... params) {
        try {
            for (Interceptor i : _listInterceptors) {
                int code = i.execute(params);
                if (code != Interceptor.SUCCESS)
                    throw new ControllerInvalidArgumentException("Failed when executing " +
                            i.getClass().getSimpleName() + ": " + Integer.toString(code)
                    );
            }
            _action.execute(params);
        } catch (Exception e) { // Exception from the Controller: Param handling or model invocation
            _resultType = ModelAction.ResultType.EXCEPTION;
            _result = e;
        }
        return null; // the return type is Void!
    }

    /**
     * Notifies the result from performing this operation to the observers
     *
     * @param v <code>null</code>, <code>_modelResultType</code> and <code>_modelResult</code> are
     */
    @Override
    protected final void onPostExecute(Void v) {
        super.onPostExecute(v);
        notifyObservers(_resultType, _result);
    }

    /**
     * Adds an observer that would be notified of the result of the action
     *
     * @param observer the observer to be notified
     */
    public void addControllerActionObserver(ControllerActionObserver observer) {
        _listObservers.add(observer);
    }

    /**
     * Notifies all observers that this <code>ControllerAction</code> has finished
     *
     * @param resultType the type of result generated by the Model
     * @param result     the result from performing the operation by the Model
     */
    private void notifyObservers(ModelAction.ResultType resultType, Object result) {
        for (ControllerActionObserver observer : _listObservers) {
            switch (resultType) {
                case RESULT:
                    observer.onResult(result);
                    break;
                case SPAREROOM_STATUS:
                    observer.onSpareroomStatus((SpareroomStatus) result);
                    break;
                case EXCEPTION:
                    observer.onException((Exception) result);
                    break;
            }
        }
    } //end notifyObservers(ModelAction.ResultType resultType, Object result)

    /**
     * Notifies to the observers when the {@link Activity#onStop() onStop()} is called
     */
    @Override
    public void notifyOnStop() {
        this.cancel(true);
    } //end notifyOnStopped()

}
