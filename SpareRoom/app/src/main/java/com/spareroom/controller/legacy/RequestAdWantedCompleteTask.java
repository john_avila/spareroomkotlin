package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.model.business.AdWanted;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;
import com.spareroom.model.legacy.ISearchFacade;

public class RequestAdWantedCompleteTask extends AsyncTask<Object, Integer, Object> {
    private ISearchFacade _modelFacade;
    private IAsyncResult _target;

    public RequestAdWantedCompleteTask(ISearchFacade modelFacade, IAsyncResult a) {
        _modelFacade = modelFacade;
        _target = a;
    }

    @Override
    protected Object doInBackground(Object... params) {

        try {
            return _modelFacade.getAdWanted((String) params[0]);
        } catch (ServiceUnavailableException e) {
            return e;
        } catch (InconsistentStateException e) {
            return e;
        } catch (MissingSystemFeatureException e) {
            return e;
        }

    }

    @Override
    protected void onPostExecute(Object result) {
        if ((result instanceof AdWanted) || (result == null)) {
            _target.update(result);
        } else if (result instanceof ServiceUnavailableException) {
            _target.handleServiceUnavailableException(null);
        } else if (result instanceof InconsistentStateException) {
            _target.handleInconsistentStateException(null);
        } else if (result instanceof MissingSystemFeatureException) {
            _target.handleMissingSystemFeatureException(null);
        }
    }

}