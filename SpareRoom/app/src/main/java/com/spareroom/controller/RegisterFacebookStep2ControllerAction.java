package com.spareroom.controller;

import com.spareroom.controller.interceptor.DefaultInterceptorStack;
import com.spareroom.lib.core.Action;
import com.spareroom.model.RegisterFacebookStep2ModelAction;
import com.spareroom.model.business.AccountRegisterFormFacebook;
import com.spareroom.model.extra.RegisterExtra;

/**
 * <code>ControllerAction</code> for registering a new account
 * <p>
 * Created by manuel on 29/06/2016.
 */
public class RegisterFacebookStep2ControllerAction extends ControllerAction {

    /**
     * Constructor.
     *
     * @param spareroomContext SpareRoom app platform resources
     */
    public RegisterFacebookStep2ControllerAction(final SpareroomContext spareroomContext) {

        addInterceptor(new DefaultInterceptorStack(
                new Class[]{AccountRegisterFormFacebook.class, RegisterExtra.class},
                new boolean[]{false, false}
        ));

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {
                RegisterFacebookStep2ModelAction modelAction =
                        new RegisterFacebookStep2ModelAction(spareroomContext);
                observe(modelAction);
                modelAction.execute(params);

                return null;
            }
        });
    }
}
