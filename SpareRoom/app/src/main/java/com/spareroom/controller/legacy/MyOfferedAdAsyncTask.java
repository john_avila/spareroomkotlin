package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.legacy.IAccountFacade;

public class MyOfferedAdAsyncTask extends AsyncTask<Parameters, Integer, Object> {
    private IAccountFacade _modelFacade;
    private IAsyncResult _activity;

    public MyOfferedAdAsyncTask(IAccountFacade modelFacade, IAsyncResult a) {
        _modelFacade = modelFacade;
        _activity = a;
    }

    @Override
    protected Object doInBackground(Parameters... params) {
        try {
            return _modelFacade.getMyOfferedAdStats(params[0]);
        } catch (Exception e) {
            return e;
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        if (!(result instanceof Exception))
            _activity.update(result);
        else if (result instanceof InvalidJSONFormatException)
            _activity.handleInconsistentStateException(null);
    }
}
