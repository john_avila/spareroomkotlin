package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.business.SpareroomStatusCode;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;
import com.spareroom.model.legacy.ISearchFacade;

public class SaveAdAsyncTaskV2 extends AsyncTask<Parameters, Integer, Object> {
    private ISearchFacade _modelFacade;
    private IAsyncResult _target;

    public SaveAdAsyncTaskV2(ISearchFacade modelFacade, IAsyncResult t) {
        _modelFacade = modelFacade;
        _target = t;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(Parameters... params) {
        // LoginInterceptor should be used, but for the moment API returns an error message
        try {
            return _modelFacade.saveSearch(params[0]);
        } catch (ServiceUnavailableException e) {
            return e;
        } catch (InconsistentStateException e) {
            return e;
        } catch (MissingSystemFeatureException e) {
            return e;
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        if (result instanceof SpareroomStatus) {
            if ((((SpareroomStatus) result).getCode() != null) &&
                    (((SpareroomStatus) result).getCode().equals(SpareroomStatusCode.NOT_LOGGED_IN)))
                _target.handleInvalidUserException(null);
            else
                _target.update(result);
        } else if (result instanceof ServiceUnavailableException) {
            _target.handleServiceUnavailableException(null);
        } else if (result instanceof InconsistentStateException) {
            _target.handleInconsistentStateException(null);
        } else if (result instanceof MissingSystemFeatureException) {
            _target.handleMissingSystemFeatureException(null);
        }
    }
}
