package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.legacy.ISearchFacade;

public class ClockCallAsyncTask extends AsyncTask<Parameters, Integer, Object> {
    private ISearchFacade _modelFacade;
    private IAsyncResult _activity;
    private SpareroomContext _srContext;

    public ClockCallAsyncTask(ISearchFacade modelFacade, SpareroomContext srContext, IAsyncResult a) {
        _modelFacade = modelFacade;
        _activity = a;
        _srContext = srContext;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(Parameters... params) {
        try {

            // LoginInterceptor should be used, but for the moment API returns an error message

            return _modelFacade.clockCall(params[0]);
        } catch (Exception e) {
            return e;
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        if (result instanceof Boolean) {
            _activity.update(result);
        } else {
            // Do nothing, this is a silent response
            // Exceptions thrown: ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException
        }
    }
}
