package com.spareroom.controller;

import com.spareroom.controller.interceptor.DefaultInterceptorStack;
import com.spareroom.lib.core.Action;
import com.spareroom.model.RegisterRunModelAction;
import com.spareroom.model.business.RegisterParameters;

/**
 * Controller for registering the device in the SpareRoom database
 * <p/>
 * Created by miguel.rossi on 26/01/2017.
 */

public class RegisterRunControllerAction extends ControllerAction {

    public RegisterRunControllerAction(final SpareroomContext spareroomContext) {

        addInterceptor(
                new DefaultInterceptorStack(
                        new Class[]{RegisterParameters.class},
                        new boolean[]{false}));

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {
                RegisterRunModelAction registerRunModelAction =
                        new RegisterRunModelAction(spareroomContext);

                observe(registerRunModelAction);
                registerRunModelAction.execute(params);

                return null;
            }
        });
    }

}
