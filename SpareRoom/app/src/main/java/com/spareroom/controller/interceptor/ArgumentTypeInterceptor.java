package com.spareroom.controller.interceptor;

/**
 * Interceptor that checks that the type (class) of the parameters provided are what can be expected
 * <p>
 * Created by manuel on 23/06/2016.
 */
public class ArgumentTypeInterceptor extends Interceptor {
    private Class[] _vExpectedParameter;

    /**
     * Contructor.
     *
     * @param vExpectedParameter an array with the class expected for every parameter. Order
     *                           sensitive.
     */
    public ArgumentTypeInterceptor(Class... vExpectedParameter) {
        _vExpectedParameter = vExpectedParameter;
    }

    /**
     * Executes the interceptor
     *
     * @param parameters array of objects which type needs to be checked. Order sensitive.
     * @return <code>Interceptor.SUCCESS</code> if succeeds,
     * <code>Interceptor.ERROR</code> or other custom value (greater than 1) if there was an error
     */
    @Override
    public int execute(Object... parameters) {
        if ((_vExpectedParameter == null) && ((parameters == null) || (parameters.length == 0)))
            return Interceptor.SUCCESS;
        else if (_vExpectedParameter.length != parameters.length)
            return Interceptor.ERROR;
        else {
            for (int i = 0; i < _vExpectedParameter.length; i++) {
                if (_vExpectedParameter[i] == null) { // null cannot be a required Type
                    return Interceptor.ERROR;
                }
                // The argument can be null. It is not responsibility of this interceptor to
                // determine whether a field can be provided null or not. null is a valid value for
                // an object of any class.
                if ((parameters[i] != null) &&
                        (!_vExpectedParameter[i].getName().equals(parameters[i].getClass().getName())))
                    return ERROR;
            }
            return Interceptor.SUCCESS;
        }
    }
}
