package com.spareroom.controller.legacy;

public abstract class IAsyncResultImpl implements IAsyncResult {

    @Override
    public void update(Object o) {

    }

    @Override
    public void handleInvalidUserException(String message) {

    }

    @Override
    public void handleMissingParameterException(String message) {

    }

    @Override
    public void handleMissingSystemFeatureException(String message) {

    }

    @Override
    public void handleServiceUnavailableException(String message) {

    }

    @Override
    public void handleInconsistentStateException(String message) {

    }
}
