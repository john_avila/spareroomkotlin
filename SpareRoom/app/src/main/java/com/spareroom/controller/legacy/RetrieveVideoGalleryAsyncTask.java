package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.VideoGallery;
import com.spareroom.model.exception.InvalidUserException;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;
import com.spareroom.model.legacy.IAccountFacade;

public class RetrieveVideoGalleryAsyncTask extends AsyncTask<Parameters, Integer, Object> {
    private final IAccountFacade _modelFacade;
    private final IAsyncResult _activity;

    public RetrieveVideoGalleryAsyncTask(
            IAccountFacade modelFacade,
            IAsyncResult a) {

        _modelFacade = modelFacade;
        _activity = a;

    }

    @Override
    protected Object doInBackground(Parameters... params) {
        try {
            return _modelFacade.getVideoGallery(params[0]);
        } catch (Exception e) {
            return e;
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        if (result instanceof VideoGallery) {
            _activity.update(result);
        } else if (result instanceof ServiceUnavailableException) {
            _activity.handleServiceUnavailableException(null);
        } else if (result instanceof MissingSystemFeatureException) {
            _activity.handleMissingSystemFeatureException(null);
        } else if (result instanceof InconsistentStateException) {
            _activity.handleInconsistentStateException(null);
        } else if (result instanceof InvalidUserException) {
            _activity.handleInvalidUserException(null);
        }
    }
}
