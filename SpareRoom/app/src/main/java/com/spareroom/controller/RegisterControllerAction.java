package com.spareroom.controller;

import com.spareroom.controller.interceptor.DefaultInterceptorStack;
import com.spareroom.lib.core.Action;
import com.spareroom.model.RegisterModelAction;
import com.spareroom.model.business.AccountRegisterForm;
import com.spareroom.model.extra.RegisterExtra;

/**
 * <code>ControllerAction</code> for registering a new account
 * <p>
 * Created by manuel on 29/06/2016.
 */
public class RegisterControllerAction extends ControllerAction {

    /**
     * Constructor.
     *
     * @param spareroomContext SpareRoom app platform resources
     */
    public RegisterControllerAction(final SpareroomContext spareroomContext) {

        addInterceptor(new DefaultInterceptorStack(
                new Class[]{AccountRegisterForm.class, RegisterExtra.class},
                new boolean[]{false, false}
        ));

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {

                RegisterModelAction modelAction = new RegisterModelAction(spareroomContext);
                observe(modelAction);
                modelAction.execute(params);

                return null;
            }
        });
    }
}
