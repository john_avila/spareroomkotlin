package com.spareroom.controller.legacy;

public interface IAsyncResult {
    void update(Object o);

    void handleInvalidUserException(String message);

    void handleMissingParameterException(String message);

    void handleMissingSystemFeatureException(String message);

    void handleServiceUnavailableException(String message);

    void handleInconsistentStateException(String message);
}
