package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.model.business.AdBoard;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SearchType;
import com.spareroom.model.exception.MissingParameterException;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;
import com.spareroom.model.legacy.ISearchFacade;

public class SearchAdAsyncTask extends AsyncTask<Parameters, Integer, Object> {
    private final ISearchFacade _modelFacade;
    private final IAsyncResult _activity;
    private final SearchType searchType;

    public SearchAdAsyncTask(ISearchFacade modelFacade, IAsyncResult a, SearchType searchType) {
        _modelFacade = modelFacade;
        _activity = a;
        this.searchType = searchType;
    }

    @Override
    protected Object doInBackground(Parameters... params) {
        try {
            return _modelFacade.searchForRent(params[0], searchType);
        } catch (ServiceUnavailableException e) {
            return e;
        } catch (InconsistentStateException e) {
            return e;
        } catch (MissingSystemFeatureException e) {
            return e;
        }

    }

    @Override
    protected void onPostExecute(Object result) {
        if (result instanceof AdBoard) {
            _activity.update(result);
        } else if (result instanceof ServiceUnavailableException) {
            _activity.handleServiceUnavailableException(((ServiceUnavailableException) result).getMessage());
        } else if (result instanceof InconsistentStateException) {
            _activity.handleInconsistentStateException(null);
        } else if (result instanceof MissingSystemFeatureException) {
            _activity.handleMissingSystemFeatureException(null);
        } else if (result instanceof MissingParameterException) {
            _activity.handleMissingParameterException(null);
        }
    }
}