package com.spareroom.controller

import com.spareroom.BuildConfig
import com.spareroom.FlavoredValue
import com.spareroom.spareroomuk.FlavoredValueUk
import com.spareroom.spareroomus.FlavoredValueUs

// build types
private const val DEBUG = "debug"
private const val RELEASE = "release"

// flavors
private const val FLAVOR_UK = "spareroomuk"
private const val FLAVOR_US = "spareroomus"

object AppVersion {

    private val flavoredValueUk = FlavoredValueUk()
    private val flavoredValueUs = FlavoredValueUs()

    @JvmStatic
    fun isProductionRelease() = BuildConfig.BUILD_TYPE == RELEASE

    @JvmStatic
    fun isDevRelease() = BuildConfig.BUILD_TYPE == DEBUG

    @JvmStatic
    fun isUk() = BuildConfig.FLAVOR == FLAVOR_UK

    @JvmStatic
    fun isUs() = BuildConfig.FLAVOR == FLAVOR_US

    @JvmStatic
    fun appId() = BuildConfig.APPLICATION_ID

    @JvmStatic
    fun versionName() = BuildConfig.VERSION_NAME

    @JvmStatic
    fun flavor(): FlavoredValue {
        if (isUk())
            return flavoredValueUk

        if (isUs())
            return flavoredValueUs

        throw IllegalStateException("Unsupported flavor")
    }
}
