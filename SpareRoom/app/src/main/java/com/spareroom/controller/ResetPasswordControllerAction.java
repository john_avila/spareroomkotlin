package com.spareroom.controller;

import com.spareroom.controller.interceptor.DefaultInterceptorStack;
import com.spareroom.lib.core.Action;
import com.spareroom.model.ResetPasswordModelAction;
import com.spareroom.model.business.AccountCredentials;
import com.spareroom.model.extra.ResetPasswordExtra;

/**
 * <code>ControllerAction</code> for logging in to an account.
 *
 * @see ControllerAction
 * <p>
 * Created by miguel.rossi on 09/08/2016.
 */
public class ResetPasswordControllerAction extends ControllerAction {

    public ResetPasswordControllerAction(final SpareroomContext spareroomContext) {

        addInterceptor(
                new DefaultInterceptorStack(
                        new Class[]{AccountCredentials.class, ResetPasswordExtra.class},
                        new boolean[]{false, true}
                ));

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {

                ResetPasswordModelAction modelAction = new ResetPasswordModelAction(spareroomContext);
                observe(modelAction);
                modelAction.execute(params);

                return null;
            }
        });

    }
}
