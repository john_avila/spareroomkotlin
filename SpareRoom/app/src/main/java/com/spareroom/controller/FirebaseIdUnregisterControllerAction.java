package com.spareroom.controller;

import com.spareroom.controller.interceptor.DefaultInterceptorStack;
import com.spareroom.lib.core.Action;
import com.spareroom.model.FirebaseIdUnregisterModelAction;

/**
 * Controller for unregister the Firebase Id in the SpareRoom database
 * <p/>
 * Created by miguel.rossi on 09/01/2017.
 */

class FirebaseIdUnregisterControllerAction extends ControllerAction {

    FirebaseIdUnregisterControllerAction(final SpareroomContext spareroomContext) {

        addInterceptor(
                new DefaultInterceptorStack(
                        new Class[]{String.class},
                        new boolean[]{false}));

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {
                FirebaseIdUnregisterModelAction firebaseIdUnregisterModelAction =
                        new FirebaseIdUnregisterModelAction(spareroomContext);

                observe(firebaseIdUnregisterModelAction);
                firebaseIdUnregisterModelAction.execute(params);

                return null;
            }
        });

    }
}
