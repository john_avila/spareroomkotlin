package com.spareroom.controller;

import com.spareroom.model.business.SpareroomStatus;

/**
 * Created by manuel on 17/05/2016.
 */
public interface ControllerActionObserver {

    void onResult(Object o);

    void onSpareroomStatus(SpareroomStatus status);

    void onException(Exception exception);

}
