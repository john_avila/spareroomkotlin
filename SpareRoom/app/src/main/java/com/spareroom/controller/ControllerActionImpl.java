package com.spareroom.controller;

import com.spareroom.controller.interceptor.Interceptor;
import com.spareroom.lib.core.Action;
import com.spareroom.model.ModelAction;

abstract class ControllerActionImpl extends ControllerAction {

    ControllerActionImpl(final SpareroomContext spareroomContext, final ModelAction modelAction, ControllerActionObserver observer, Interceptor... interceptors) {

        for (Interceptor interceptor : interceptors) {
            addInterceptor(interceptor);
        }

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {
                observe(modelAction);
                modelAction.execute(params);

                return null;
            }
        });

        addControllerActionObserver(observer);
    }
}
