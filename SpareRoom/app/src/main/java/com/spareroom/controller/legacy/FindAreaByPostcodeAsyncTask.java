package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.model.legacy.IPlaceAdFacade;

public class FindAreaByPostcodeAsyncTask extends AsyncTask<String, Integer, Object> {
    private IAsyncResult _activity;
    private IPlaceAdFacade _modelFacade;

    public FindAreaByPostcodeAsyncTask(IPlaceAdFacade modelFacade, IAsyncResult a) {
        _modelFacade = modelFacade;
        _activity = a;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(String... postcode) {
        try {
            return _modelFacade.getNeightbourhoods(postcode[0]);
        } catch (Exception e) {
            return e;
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        if (!(result instanceof Exception)) {
            _activity.update(result);
        } else {
            _activity.handleServiceUnavailableException(((Exception) result).getMessage());
        }
    }
}
