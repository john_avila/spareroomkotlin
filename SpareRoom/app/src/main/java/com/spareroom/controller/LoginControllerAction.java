package com.spareroom.controller;

import com.spareroom.controller.interceptor.DefaultInterceptorStack;
import com.spareroom.lib.core.Action;
import com.spareroom.model.LoginModelAction;
import com.spareroom.model.business.AccountCredentials;
import com.spareroom.model.extra.LoginExtra;

/**
 * <code>ControllerAction</code> for logging in to an account
 * <p>
 * Created by manuel on 22/06/2016.
 */
public class LoginControllerAction extends ControllerAction {

    /**
     * Constructor.
     *
     * @param spareroomContext SpareRoom app platform resources
     */
    public LoginControllerAction(final SpareroomContext spareroomContext) {

        addInterceptor(new DefaultInterceptorStack(
                new Class[]{AccountCredentials.class, LoginExtra.class},
                new boolean[]{false, false}
        ));

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {

                LoginModelAction modelAction = new LoginModelAction(spareroomContext);
                observe(modelAction);
                modelAction.execute(params);

                return null;
            }
        });
    }
}
