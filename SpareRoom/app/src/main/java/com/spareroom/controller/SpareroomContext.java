package com.spareroom.controller;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.sharedpreferences.CookieDAO;
import com.spareroom.integration.sharedpreferences.SessionDAO;
import com.spareroom.integration.webservice.rest.service.LoginRestService;
import com.spareroom.lib.core.IObservable;
import com.spareroom.lib.core.IObserver;
import com.spareroom.model.business.*;
import com.spareroom.ui.util.StringUtils;

import java.net.HttpCookie;
import java.util.LinkedList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Context object holding dynamic data that only concerns to this application. These are global resources that do not belong to any
 * particular Activity and can be used and shared at any point within the scope of this application.
 *
 * @author manuel
 */
public class SpareroomContext {
    private Parameters _currentSearch;
    private Session _session;
    private DeleteSessionObserver _deleteSessionObserver = new DeleteSessionObserver();
    private SaveSessionObserver _saveSessionObserver = new SaveSessionObserver();
    private Context _applicationContext;
    private float _screenHeight;

    public SpareroomContext(Context applicationContext) {
        _applicationContext = applicationContext;
        _currentSearch = new Parameters();

        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) _applicationContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metrics);
        _screenHeight = metrics.heightPixels / (metrics.densityDpi / 160f);

    }

    public float get_screenHeight() {
        return _screenHeight;
    }

    public Parameters getCurrentSearch() {
        return _currentSearch;
    }

    public void updateCurrentSearch(Parameters updatedSearch) {
        _currentSearch = updatedSearch;
    }

    public void cleanCurrentSearch() {
        _currentSearch = new Parameters();
    }

    @Nullable
    public Session getSession() {
        if (_session == null)
            return loadUserSession();
        return _session;
    }

    public boolean isUserLoggedIn() {
        return getSession() != null;
    }

    public void saveUserSession(Session s) {
        _session = s;
        setCustomerUserId(_session);

        CookieDAO cookieDAO = new CookieDAO();
        cookieDAO.save(_applicationContext, new LoginRestService().getCookies());

        SessionDAO sessionDAO = new SessionDAO();
        sessionDAO.create(_applicationContext, _session);

        _saveSessionObserver.notifySessionObservers();
    }

    @Nullable
    public Session loadUserSession() {
        CookieDAO dao = new CookieDAO();
        List<HttpCookie> cookie = dao.load(_applicationContext);
        Session session = null;

        if ((cookie != null) && !cookie.isEmpty()) {
            new LoginRestService().setCookies(cookie);

            SessionDAO sessionDAO = new SessionDAO();
            session = sessionDAO.read(_applicationContext);
            if (session != null)
                _session = session;
        }

        setCustomerUserId(session);
        return session;
    }

    public void deleteUserSession() {
        CookieDAO cookieDAO = new CookieDAO();
        cookieDAO.delete(_applicationContext);

        SessionDAO sessionDAO = new SessionDAO();
        sessionDAO.delete(_applicationContext);

        _session = null;
        setCustomerUserId(null);

        _deleteSessionObserver.notifySessionObservers();
    }

    public Context get_applicationContext() {
        return _applicationContext;
    }

    public void addDeleteSessionObserver(IObserver observer) {
        _deleteSessionObserver.addObserver(observer);

    }

    public void removeDeleteSessionObserver(IObserver observer) {
        _deleteSessionObserver.removeObserver(observer);
    }

    public void addSaveSessionObserver(IObserver observer) {
        _saveSessionObserver.addObserver(observer);

    }

    public void removeSaveSessionObserver(IObserver observer) {
        _saveSessionObserver.removeObserver(observer);
    }

    @NonNull
    public String getUserId() {
        Session session = getSession();
        if (session == null)
            return "";

        User user = session.get_user();
        String userId = user == null ? "" : user.get_userId();
        return userId == null ? "" : userId;
    }

    public boolean isUserUpgraded() {
        Session session = getSession();
        return session != null && !StringUtils.isNullOrEmpty(session.get_upgradeProduct(), true);
    }

    //region INNER CLASSES

    private class DeleteSessionObserver implements IObservable {
        private LinkedList<IObserver> _listObserver = new LinkedList<>();

        @Override
        public void addObserver(IObserver observer) {
            _listObserver.add(observer);
        }

        @Override
        public void removeObserver(IObserver observer) {
            _listObserver.remove(observer);
        }

        void notifySessionObservers() {
            for (IObserver o : _listObserver)
                o.notifyObserver(null);
        }

    }

    private class SaveSessionObserver implements IObservable {
        private LinkedList<IObserver> _listObserver = new LinkedList<>();

        @Override
        public void addObserver(IObserver observer) {
            _listObserver.add(observer);
        }

        @Override
        public void removeObserver(IObserver observer) {
            _listObserver.remove(observer);
        }

        void notifySessionObservers() {
            for (IObserver o : _listObserver)
                o.notifyObserver(null);
        }

    }

    private void setCustomerUserId(@Nullable Session session) {
        AnalyticsTrackerComposite.getInstance().setCustomerUserId(session != null ? getUserId() : null);
    }

    //endregion INNER CLASSES

}
