package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.model.business.Parameters;
import com.spareroom.model.legacy.IPlaceAdFacade;

public class PlaceAdWantedAreasAsyncTask extends AsyncTask<Parameters, Integer, Object> {
    private IPlaceAdFacade _modelFacade;
    private IAsyncResult _asyncResult;

    public PlaceAdWantedAreasAsyncTask(IPlaceAdFacade modelFacade, IAsyncResult a) {
        _modelFacade = modelFacade;
        _asyncResult = a;
    }

    @Override
    protected Object doInBackground(Parameters... params) {
        try {
            return _modelFacade.getAreasAjax(params[0]);
        } catch (Exception e) {
            return e;
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        if (!(result instanceof Exception)) {
            _asyncResult.update(result);
        } else {
            _asyncResult.handleServiceUnavailableException(((Exception) result).getMessage());
        }
    }

}
