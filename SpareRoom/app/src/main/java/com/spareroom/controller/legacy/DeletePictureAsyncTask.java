package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.controller.interceptor.Interceptor;
import com.spareroom.controller.interceptor.LoggedInInterceptor;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.exception.InvalidUserException;
import com.spareroom.model.legacy.IAccountFacade;

public class DeletePictureAsyncTask extends AsyncTask<Parameters, Integer, Object> {
    private IAccountFacade _af;
    private IAsyncResult _targetActivity;
    private SpareroomContext _srContext;

    public DeletePictureAsyncTask(IAccountFacade af, SpareroomContext srContext, IAsyncResult a) {
        _af = af;
        _targetActivity = a;
        _srContext = srContext;
    }

    @Override
    protected Object doInBackground(Parameters... params) {

        Interceptor i = new LoggedInInterceptor(_srContext);
        if (i.execute((Object[]) null) != Interceptor.SUCCESS) {
            return new InvalidUserException();
        }

        try {
            _af.removePicture(params[0]);
            return null;
        } catch (NetworkConnectivityException e) {
            return e;
        } catch (InvalidJSONFormatException e) {
            return e;
        } catch (AuthenticationException e) {
            return e;
        } catch (ClientErrorException e) {
            return e;
        } catch (ServerErrorException e) {
            return e;
        }

    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);
        if (!(result instanceof Exception)) {
            _targetActivity.update(result);
        } else if (result instanceof NetworkConnectivityException) {
            _targetActivity.handleServiceUnavailableException(null);
        } else if (result instanceof InvalidJSONFormatException) {
            _targetActivity.handleInconsistentStateException(null);
        } else if (result instanceof AuthenticationException) {
            _targetActivity.handleInconsistentStateException(null);
        } else if (result instanceof ClientErrorException) {
            _targetActivity.handleInconsistentStateException(null);
        } else if (result instanceof ServerErrorException) {
            _targetActivity.handleServiceUnavailableException(null);
        }

    }

}
