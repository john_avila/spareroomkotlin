package com.spareroom.controller.legacy;

import android.os.AsyncTask;

import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.model.business.DraftAd;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.legacy.IPlaceAdFacade;

public class PlaceAdUploadAsyncTask extends AsyncTask<DraftAd, Integer, Object> {
    private IPlaceAdFacade _modelFacade;
    private IAsyncResult _activity;

    public PlaceAdUploadAsyncTask(IPlaceAdFacade modelFacade, IAsyncResult a) {
        _modelFacade = modelFacade;
        _activity = a;
    }

    @Override
    protected Object doInBackground(DraftAd... params) {
        SpareroomStatus im;
        try {
            DraftAd draft = params[0];

            if (draft instanceof DraftAdWanted)
                im = _modelFacade.placeWantedAdvert((DraftAdWanted) draft);
            else
                im = _modelFacade.placeOfferedAdvert((DraftAdOffered) draft);

        } catch (Exception e) {
            return e;
        }
        return im;
    }

    @Override
    protected void onPostExecute(Object result) {
        if (result instanceof SpareroomStatus) {
            _activity.update(result);
        } else if (result instanceof NetworkConnectivityException) {
            _activity.handleServiceUnavailableException(((NetworkConnectivityException) result).getMessage());
        }
    }
}
