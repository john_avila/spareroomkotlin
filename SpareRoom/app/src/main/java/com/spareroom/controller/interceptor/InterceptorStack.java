package com.spareroom.controller.interceptor;

import java.util.LinkedList;

/**
 * Stack of Interceptors. Abstract. Extend this class and add interceptors in the constructor via
 * <code>add(Interceptor i)</code> to create a stack. Executes as an Interceptor, as it extends that
 * class.
 * <p>
 * Created by manuel on 23/06/2016.
 */
public abstract class InterceptorStack extends Interceptor {
    private LinkedList<Interceptor> _listInterceptor = new LinkedList<>();

    protected void add(Interceptor i) {
        _listInterceptor.add(i);
    }

    public final int execute(Object... parameters) {
        for (Interceptor i : _listInterceptor) {
            int code = i.execute(parameters);
            if (code != Interceptor.SUCCESS)
                return code;
        }
        return Interceptor.SUCCESS;
    }
}
