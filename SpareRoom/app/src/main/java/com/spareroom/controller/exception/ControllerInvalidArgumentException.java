package com.spareroom.controller.exception;

/**
 * Created by miguel.rossi on 23/05/2016.
 */
public class ControllerInvalidArgumentException extends Exception {

    private static final long serialVersionUID = 8399253297732206852L;

    public ControllerInvalidArgumentException() {
    }

    public ControllerInvalidArgumentException(String detailMessage) {
        super(detailMessage);
    }

    public ControllerInvalidArgumentException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ControllerInvalidArgumentException(Throwable throwable) {
        super(throwable);
    }
}
