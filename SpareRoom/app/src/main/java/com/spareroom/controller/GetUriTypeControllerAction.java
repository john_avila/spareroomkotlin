package com.spareroom.controller;

import com.spareroom.model.GetUriTypeModelAction;

public class GetUriTypeControllerAction extends ControllerActionImpl {

    private GetUriTypeControllerAction(SpareroomContext spareroomContext, ControllerActionObserver observer) {
        super(spareroomContext, new GetUriTypeModelAction(spareroomContext), observer);
    }

    public static GetUriTypeControllerAction get(ControllerActionObserver observer) {
        return new GetUriTypeControllerAction(SpareroomApplication.getSpareRoomContext(), observer);
    }
}
