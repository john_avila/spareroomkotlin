package com.spareroom.controller;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.spareroom.App;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.analytics.appsflyer.AnalyticsTrackerAppsFlyer;
import com.spareroom.integration.analytics.fabric.AnalyticsTrackerFabric;
import com.spareroom.integration.analytics.firebase.AnalyticsTrackerFirebase;
import com.spareroom.integration.analytics.googleanalytics.AnalyticsTrackerGoogleAnalytics;
import com.spareroom.integration.business.legacy.LegacyBusinessFactorySingleton;
import com.spareroom.integration.sharedpreferences.*;
import com.spareroom.model.business.RegisterParameters;
import com.spareroom.model.legacy.*;

import java.util.Calendar;

import io.fabric.sdk.android.Fabric;

import static android.content.Context.MODE_PRIVATE;

/**
 * Overrides Application providing a single instance for every model facade.
 */
public class SpareroomApplication {
    private static SpareroomApplication __instance;
    private ISearchFacade _searchFacade;
    private IAccountFacade _accountFacade;
    private IMessageFacade _messageFacade;
    private ILocationFacade _locationFacade;
    private IPlaceAdFacade _placeAdFacade;

    private SpareroomContext _context;

    private static final String CACHE_1 = "EIY0XLbJBf+uogeRoU";
    private static final String CACHE_2 = "piPFcBVGuCacaafV+odd";
    private static final String CACHE_3 = "YiTEBpfrccscBrbnuJVFtK";
    private static final String CACHE_4 = "uxnKDusxxwGhPl/b0gcue";
    private static final String CACHE_5 = "lmsYXgqJmpjY+UN/pD/VJWo";
    private static final String CACHE_6 = "YioCRvzxgyKqeiZfHxawF";
    private static final String CACHE_7 = "QqvaRYpNAmsX+oQIDAQAB";

    private static final int FACEBOOK_REQUEST_CODE = 32763;

    private SpareroomApplication() {
    }

    @Deprecated
    public static synchronized SpareroomApplication getInstance(Context applicationContext) {
        return getInstance();
    }

    public static synchronized SpareroomApplication getInstance() {
        if (__instance == null) {
            __instance = new SpareroomApplication();
            __instance.initApp(App.get());
        }
        return __instance;
    }

    private void initApp(Application applicationContext) {

        Crashlytics crashlytics = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(AppVersion.isDevRelease()).build())
                .build();

        Fabric.with(applicationContext, crashlytics);

        AnalyticsTrackerComposite trackerComposite = AnalyticsTrackerComposite.getInstance();

        if (AppVersion.isProductionRelease()) {
            trackerComposite.add(new AnalyticsTrackerGoogleAnalytics(applicationContext));
            trackerComposite.add(new AnalyticsTrackerFirebase(applicationContext));
            trackerComposite.add(new AnalyticsTrackerFabric());
            trackerComposite.add(new AnalyticsTrackerAppsFlyer(applicationContext));
        }

        _context = new SpareroomContext(applicationContext);

        _searchFacade = new SearchFacade(LegacyBusinessFactorySingleton.getInstance());

        AutocompleteConfigDAO dao = new AutocompleteConfigDAO(applicationContext);
        String autocompleteTime = dao.read();

        if (autocompleteTime == null || autocompleteTime.isEmpty()) {
            dao.delete();
            dao.create();
            autocompleteTime = dao.read();
        }

        if (autocompleteTime != null && !autocompleteTime.isEmpty()) {
            // If we started caching longer than a month ago, we
            Calendar lastMonthCalendar = Calendar.getInstance();
            Calendar autocompleteCalendar = Calendar.getInstance();
            lastMonthCalendar.roll(Calendar.MONTH, -1);
            lastMonthCalendar.getTime();
            autocompleteCalendar.setTimeInMillis(Long.parseLong(autocompleteTime));

            if (autocompleteCalendar.before(lastMonthCalendar)) {
                dao.delete();
                dao.create();
                SearchSuggestionListDAO searchSuggestionDao = new SearchSuggestionListDAO(applicationContext);
                searchSuggestionDao.deleteAll();
            }
        }

        retryRegisterFirebaseId();
        register();
    }

    /**
     * Initialize Facebook library on demand for the login and register process. It checks if it is
     * already initialized for avoiding have more than one instance.
     */
    public void initializeFacebook(Application application) {
        if (!FacebookSdk.isInitialized()) {
            FacebookSdk.sdkInitialize(application.getApplicationContext(), FACEBOOK_REQUEST_CODE);
            AppEventsLogger.activateApp(application);
        }
    }

    /**
     * Gets the model for this application
     *
     * @return the model
     */
    public ISearchFacade getSearchFacade() {
        return _searchFacade;
    }

    public IAccountFacade getAccountFacade() {
        if (_accountFacade == null)
            _accountFacade = new AccountFacade(LegacyBusinessFactorySingleton.getInstance());
        return _accountFacade;
    }

    public IMessageFacade getMessageFacade() {
        if (_messageFacade == null)
            _messageFacade = new MessageFacade(LegacyBusinessFactorySingleton.getInstance());
        return _messageFacade;
    }

    public ILocationFacade getLocationFacade() {
        if (_locationFacade == null)
            _locationFacade = new LocationFacade(LegacyBusinessFactorySingleton.getInstance(), this);
        return _locationFacade;
    }

    public IPlaceAdFacade getPlaceAdFacade() {
        if (_placeAdFacade == null)
            _placeAdFacade = new PlaceAdFacade(LegacyBusinessFactorySingleton.getInstance(), this);
        return _placeAdFacade;
    }

    public SpareroomContext getSpareroomContext() {
        return _context;
    }

    public static SpareroomContext getSpareRoomContext() {
        return getInstance().getSpareroomContext();
    }

    public String getCache() {
        return (CACHE_1 + CACHE_2 + CACHE_3 + CACHE_4 + CACHE_5 + CACHE_6 + CACHE_7);
    }

    /**
     * Try to register/unregister the FirebaseId only if the previous try failed
     */
    private void retryRegisterFirebaseId() {
        PushTransactionDAO.Status status;

        if (_context.getSession() != null) {
            status = PushTransactionDAO.read(_context.get_applicationContext(), PushTransactionDAO.Process.REGISTER);

            if (status == PushTransactionDAO.Status.UNSUCCESSFUL)
                FirebaseIdController.register(_context);

        } else {
            status = PushTransactionDAO.read(_context.get_applicationContext(), PushTransactionDAO.Process.UNREGISTER);

            if (status == PushTransactionDAO.Status.UNSUCCESSFUL)
                FirebaseIdController.unregister(_context);
        }

    }

    /**
     * Registers the device in the server.
     */
    private void register() {
        RegisterParameters registerParameters = new RegisterParameters();
        SharedPreferences sharedPreferences =
                _context.get_applicationContext().getSharedPreferences(AppVersion.appId(), MODE_PRIVATE);
        RegisterRunControllerAction registerRunControllerAction =
                new RegisterRunControllerAction(SpareroomApplication.getInstance().getSpareroomContext());
        registerParameters.setFirstOpen(sharedPreferences.getBoolean("firstOpen", true) ? "Y" : "N");
        registerParameters.setMyVersion(AppVersion.versionName());

        sharedPreferences.edit().putBoolean("firstOpen", false).apply();

        registerRunControllerAction.execute(registerParameters);

    }

}
