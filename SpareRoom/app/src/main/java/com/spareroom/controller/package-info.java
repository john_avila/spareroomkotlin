/**
 * Controller of the Model-View-Controller. Classes here prepare parameters to invoke the Model,
 * invoke the Model, and deliver the result.
 */
package com.spareroom.controller;