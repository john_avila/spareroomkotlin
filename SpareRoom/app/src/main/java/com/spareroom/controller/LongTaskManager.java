package com.spareroom.controller;

import com.spareroom.lib.core.LongTask;

import java.util.TreeMap;

/**
 * Created by manuel on 27/09/2016.
 */
public class LongTaskManager {
    private static LongTaskManager __instance;
    private TreeMap<Integer, LongTask> _longTaskTreeMap = new TreeMap<>();

    private LongTaskManager() {

    }

    public static synchronized LongTaskManager getInstance() {
        if (__instance == null)
            __instance = new LongTaskManager();
        return __instance;
    }

    public void addLongTask(LongTask longTask) {
        _longTaskTreeMap.put(longTask.getId(), longTask);
    }

    public void removeLongTask(int longTaskId) {
        _longTaskTreeMap.remove(longTaskId);
    }

    public LongTask getLongTask(Integer longTaskId) {
        return _longTaskTreeMap.get(longTaskId);
    }
}
