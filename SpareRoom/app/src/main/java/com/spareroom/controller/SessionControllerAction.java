package com.spareroom.controller;

import com.spareroom.controller.interceptor.DefaultInterceptorStack;
import com.spareroom.lib.core.Action;
import com.spareroom.model.SessionModelAction;

/**
 * <code>ControllerAction</code> for retrieve the Session
 *
 * @see ControllerAction
 * <p/>
 * Created by miguel.rossi on 23/02/2017.
 */
public class SessionControllerAction extends ControllerAction {

    public SessionControllerAction(final SpareroomContext spareroomContext) {

        addInterceptor(
                new DefaultInterceptorStack(
                        new Class[]{},
                        new boolean[]{}
                ));

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {
                SessionModelAction conversationModelAction =
                        new SessionModelAction(spareroomContext);

                observe(conversationModelAction);
                conversationModelAction.execute(params);

                return null;
            }
        });
    }

}
