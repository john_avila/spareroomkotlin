package com.spareroom.controller.legacy;

public interface IRetriableActivity {

    void retry();

}
