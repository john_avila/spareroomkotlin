package com.spareroom.controller;

import com.spareroom.controller.interceptor.DefaultInterceptorStack;
import com.spareroom.lib.core.Action;
import com.spareroom.model.FirebaseIdRegisterModelAction;

/**
 * Controller for registering the Firebase Id in the SpareRoom database
 * <p/>
 * Created by miguel.rossi on 06/01/2017.
 */

class FirebaseIdRegisterControllerAction extends ControllerAction {

    FirebaseIdRegisterControllerAction(final SpareroomContext sc) {

        addInterceptor(
                new DefaultInterceptorStack(
                        new Class[]{String.class},
                        new boolean[]{false}));

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {
                FirebaseIdRegisterModelAction firebaseIdRegisterModelAction =
                        new FirebaseIdRegisterModelAction(sc);

                observe(firebaseIdRegisterModelAction);
                firebaseIdRegisterModelAction.execute(params);

                return null;
            }
        });

    }
}
