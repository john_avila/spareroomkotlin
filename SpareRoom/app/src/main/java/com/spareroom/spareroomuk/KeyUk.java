package com.spareroom.spareroomuk;

import com.spareroom.KInterface;

import java.util.Collections;
import java.util.LinkedList;

public class KeyUk implements KInterface {

    private class Cache {
        private final String cache1 = "EIY0XLbJBf+uogeRoU";
        private final String cache2 = "piPFcBVGuCacaafV+odd";
        private final String cache3 = "YiTEBpfrccscBrbnuJVFtK";
        private final String cache4 = "uxnKDusxxwGhPl/b0gcue";
        private final String cache5 = "lmsYXgqJmpjY+UN/pD/VJWo";
        private final String cache6 = "YioCRvzxgyKqeiZfHxawF";
        private final String cache7 = "QqvaRYpNAmsX+oQIDAQAB";

        String getCache() {
            return (cache1 + cache2 + cache3 + cache4 + cache5 + cache6 + cache7);
        }
    }

    private static final Integer[] mem = {67, 81, 112, 119, 121, 122, 127, 138, 143};

    public String getA() {
        String str1 = "MIIBIjANBgkqhki";
        String str2 = "Guw0BAQEFAAO";
        String str3 = "CAQyAMIIBCgKCA";
        String str4 = "QEA0VXbxqCDdW";
        String str5 = "qzata+CxAKuyKMjI";
        String str6 = "xkhl/gmsdgiIg+HqSs";
        String str7 = "QnkFeLiwHRsIXKjgze";
        String str8 = "sLu+mTdcWwfcSQbvag";
        String str9 = "u/AhPXCrqzhpvDFkgR";
        String str10 = "pduJe+qyaLRcBzo0KW";
        String str11 = "FHfmzd/DoxpTBs+YZYT";
        String str12 = "TdHAkPgoeJfMJCnVKP/x";
        String str13 = "lS+htlfhmUaOKhk/apnRxFs";
        String str14 = "NMZXtguEQ00LiKAclayTgtZy";

        String key = (str1 + str2 + str3 + str4 + str5 + str6 + str7 + str8 + str9 + str10 + str11 + str12 + str13 + str14);

        return getKey(key);
    }

    private String getKey(String o) {
        Integer s454564 = 98836165;
        Integer s6246435 = 8137124;
        Long s783 = 199315875L;
        Long s6757 = 725541745L;
        Integer s17 = 694283;

        Float[] p2 = {145f, 155f, 157f, 164f, 168f, 173f, 178f, 185f, 186f, 197f, 199f, 207f, 236f, 238f, 264f, 272f};
        String[] p3 = {"277", "288", "290", "303", "309", "315", "316", "319", "323", "341", "344", "358", "376", "377", "379", "380", "394", "400", "408", "409", "410", "412", "414"};

        o += (new Cache()).getCache();

        Long address = 855711165L;
        String s = (s454564.toString() + s6246435.toString() + s17.toString() + s6757.toString() + s783.toString() + address.toString());
        LinkedList<Integer> lp = new LinkedList<>();
        Collections.addAll(lp, mem);

        for (Float f : p2) {
            lp.add(f.intValue());
        }
        for (String ss : p3) {
            lp.add(Integer.parseInt(ss));
        }
        String no = "";
        int beginIndex = 0;
        for (int i = 0; i < lp.size(); i++) {
            Integer target = lp.get(i) - 51;
            if (beginIndex > (target)) {
                no += s.charAt(i);
                beginIndex = target + 1;
            } else {
                no += o.substring(beginIndex, target);
                no += s.charAt(i);
                beginIndex = target + 1;
            }
        }
        no += o.substring(beginIndex, o.length());

        return no;
    }
}
