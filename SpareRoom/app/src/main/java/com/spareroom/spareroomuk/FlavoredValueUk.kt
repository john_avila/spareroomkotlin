package com.spareroom.spareroomuk

import com.spareroom.FlavoredValue
import com.spareroom.controller.AppVersion
import java.util.*

private const val DOMAIN = "https://androidapp.spareroom.co.uk"
private const val WEB_DOMAIN = "http://www.spareroom.co.uk"
private const val TERMS_DOMAIN = "https://androidapp.spareroom.co.uk/content/padded/terms-uk"
private const val PRIVACY_DOMAIN = "https://androidapp.spareroom.co.uk/content/padded/privacy-uk/"
private const val TWITTER_USERNAME = "SpareRoomUK"
private const val PHONE = "01617681162"
private const val API_KEY = "502"
private const val API_S_1 = "JH"
private const val API_S_4 = "Qaidbh*!jdhmmmm"
private const val API_S_5 = 74
private const val API_S_2 = "zHGAS"
private const val API_SECRET_3 = 981
private const val USER_AGENT_APP_ID = "SpareRoomAppUK"
private const val CS_EMAIL = "customerservices@spareroom.co.uk"
private const val DATE_HOUR_REPRESENTATION = "dd/MM/yyy h:mm a"
private const val DATE_REPRESENTATION = "dd MMMM yyyy"
private const val UPGRADE_1_BOLD = "On average Bold Adverts get double the enquiries - find your room or flatmate faster"
private const val CURRENCY = "£"
private const val GOOGLE_PLAY_APP_NAME = "SpareRoom"

private val BETA_URL = "https://play.google.com/apps/testing/" + AppVersion.appId()
private val LOCALE = Locale.UK!!

class FlavoredValueUk : FlavoredValue {

    override val domain = DOMAIN

    override val webDomain = WEB_DOMAIN

    override val termsDomain = TERMS_DOMAIN

    override val privacyDomain = PRIVACY_DOMAIN

    override val twitterUsername = TWITTER_USERNAME

    override val currency = CURRENCY

    override val phone = PHONE

    override val apiId = API_KEY

    override val apiSecret = API_S_1 + API_S_5 + API_S_4 + API_SECRET_3 + API_S_2

    override val userAgentAppId = USER_AGENT_APP_ID

    override val csEmail = CS_EMAIL

    override val locale = LOCALE

    override val dateHourRepresentation = DATE_HOUR_REPRESENTATION

    override val dateRepresentation = DATE_REPRESENTATION

    override val key = KeyUk()

    override val upgrade1Bold = UPGRADE_1_BOLD

    override val betaUrl = BETA_URL

    override val googlePlayAppName = GOOGLE_PLAY_APP_NAME
}