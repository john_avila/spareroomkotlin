package com.spareroom;

import android.app.Activity;
import android.os.StrictMode;

import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.dependency.component.*;
import com.spareroom.integration.dependency.module.AppModule;
import com.spareroom.ui.util.NotificationUtils;
import com.spareroom.ui.util.UiUtils;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDexApplication;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

public class App extends MultiDexApplication implements HasActivityInjector {

    private static App instance;
    private RefWatcher refWatcher;

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        initStrictMode();

        // This process is dedicated to LeakCanary for heap analysis. Do not init app in this process.
        if (AppVersion.isDevRelease() && LeakCanary.isInAnalyzerProcess(this))
            return;

        initLeakCanary();

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        instance = this;
        setAppComponent();

        SpareroomApplication.getInstance();
        setCustomerId();
        NotificationUtils.createNotificationChannels(this);
    }

    public static App get() {
        return instance;
    }

    public RefWatcher getRefWatcher() {
        return refWatcher;
    }

    private void setAppComponent() {
        AppComponent appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        appComponent.inject(this);
        ComponentRepository.get().setAppComponent(appComponent, this);
    }

    private void initStrictMode() {
        if (AppVersion.isDevRelease()) {

            // set thread policy
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .penaltyFlashScreen()
                    .build());

            // set VM policy
            StrictMode.VmPolicy.Builder vmPolicyBuilder = new StrictMode.VmPolicy.Builder()
                    .detectActivityLeaks()
                    .detectLeakedClosableObjects()
                    .detectLeakedRegistrationObjects()
                    .penaltyLog();

            if (UiUtils.isJellyBeanMr2OrNewer())
                vmPolicyBuilder.detectFileUriExposure();

            if (UiUtils.isMarshmallowOrNewer())
                vmPolicyBuilder.detectCleartextNetwork();

            if (UiUtils.isOreoOrNewer()) {
                vmPolicyBuilder.detectContentUriWithoutPermission();

                // ok http doesn't tag them (neither in our app nor in libraries)
                //vmPolicyBuilder.detectUntaggedSockets();
            }

            StrictMode.setVmPolicy(vmPolicyBuilder.build());
        }
    }

    private void initLeakCanary() {
        refWatcher = AppVersion.isDevRelease() ? LeakCanary.install(this) : RefWatcher.DISABLED;
    }

    private void setCustomerId() {
        AnalyticsTrackerComposite.getInstance().setCustomerUserId(SpareroomApplication.getInstance().getSpareroomContext().getUserId());
    }

}
