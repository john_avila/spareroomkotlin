package com.spareroom.model.business;

public enum SearchType {OFFERED, WANTED}
