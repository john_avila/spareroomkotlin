package com.spareroom.model.exception;

public class ServiceUnavailableException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -2142714443997773672L;
    private String _message;

    public ServiceUnavailableException() {
        // TODO Auto-generated constructor stub
    }

    public ServiceUnavailableException(String message) {
        _message = message;
    }

    @Override
    public String getMessage() {
        return _message;
    }
}
