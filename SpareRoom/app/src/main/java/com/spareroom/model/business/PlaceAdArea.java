package com.spareroom.model.business;

import java.io.Serializable;

public class PlaceAdArea implements Serializable, Comparable<PlaceAdArea> {
    /**
     *
     */
    private static final long serialVersionUID = 6602602421576335296L;
    private String _id;
    private String _name;
    private String _parent = null;

    public PlaceAdArea() {
    }

    public PlaceAdArea(String id, String name, String parent) {
        _id = id;
        _name = name;
        _parent = parent;
    }

    public String getId() {
        return _id;
    }

    public void setId(String id) {
        this._id = id;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        this._name = name;
    }

    public String getParent() {
        return _parent;
    }

    public void setParent(String parent) {
        this._parent = parent;
    }

    @Override
    public int compareTo(PlaceAdArea another) {
        return Integer.valueOf(_id).compareTo(Integer.valueOf(another._id));
    }

    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return Integer.parseInt(_id + Integer.toString(_name.hashCode()));
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        return _id.equals(((PlaceAdArea) o)._id) &&
                _name.equals(((PlaceAdArea) o)._name);
    }

}
