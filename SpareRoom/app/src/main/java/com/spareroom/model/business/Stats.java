package com.spareroom.model.business;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Locale;
import java.util.TimeZone;

public class Stats {
    private Hashtable<String, DayStats> _lStats;

    /*
    private Integer _viewsToday;
    private Integer _viewsPastWeek;
    private Integer _contactsToday;
    private Integer contactsPastWeek;
    */
    private String date2StringId(Calendar date) {
        String sMonth = Integer.toString(date.get(Calendar.MONTH) + 1);
        String sDate = Integer.toString(date.get(Calendar.DATE));

        while (sMonth.length() < 2) {
            sMonth = ("0" + sMonth);
        }

        while (sDate.length() < 2) {
            sDate = ("0" + sDate);
        }

        return Integer.toString(date.get(Calendar.YEAR)) + "-" + sMonth + "-" + sDate;
    }

    private String date2StringId(int date, int month, int year) {
        String sMonth = Integer.toString(month);
        String sDate = Integer.toString(date);

        while (sMonth.length() < 2) {
            sMonth = ("0" + sMonth);
        }

        while (sDate.length() < 2) {
            sDate = ("0" + sDate);
        }
        return Integer.toString(year) + "-" + sMonth + "-" + sDate;
    }

    public Stats() {
        _lStats = new Hashtable<>();
    }

    public DayStats getStats(int date, int month, int year) {
        return _lStats.get(date2StringId(date, month, year));
    }

    public void add(DayStats stats) {
        _lStats.put(date2StringId(stats.get_date()), stats);
    }

    public int size() {
        return _lStats.size();
    }

    public Hashtable<String, DayStats> get_lStats() {
        return _lStats;
    }

    public void set_lStats(Hashtable<String, DayStats> _lStats) {
        this._lStats = _lStats;
    }

    public Integer get_viewsToday() {
        // Date today = new Date();
        GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.UK);

        String keyToday = date2StringId(calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR));

        DayStats todayStats = _lStats.get(keyToday);
        if (todayStats == null)
            return 0;

        return todayStats.get_viewCount();
    }

    public Integer get_viewsPastWeek() {
        // Date today = new Date();
        GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.UK);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        // String keyToday = date2StringId(calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR));

        GregorianCalendar calendar1WeekAgo = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.UK);
        calendar1WeekAgo.add(Calendar.DATE, -7);
        /* String key1WeekAgo = date2StringId(
                calendar1WeekAgo.get(Calendar.DATE),
				calendar1WeekAgo.get(Calendar.MONTH) + 1,
				calendar1WeekAgo.get(Calendar.YEAR));
				*/
        Integer viewsPastWeek = 0;

        for (String key : _lStats.keySet()) {
            if (_lStats.get(key).get_date().getTimeInMillis() > (calendar1WeekAgo.getTimeInMillis()) &&
                    _lStats.get(key).get_date().getTimeInMillis() < (calendar.getTimeInMillis()))
                viewsPastWeek += _lStats.get(key).get_viewCount();
            // if (key != keyToday) {

            // }
        }

        return viewsPastWeek;
    }

    public Integer get_contactsToday() {
        // Date today = new Date();
        GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.UK);

        String keyToday = date2StringId(calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR));

        DayStats todayStats = _lStats.get(keyToday);
        if (todayStats == null)
            return 0;

        return todayStats.get_telCount() + todayStats.get_emailCount();
    }

    public Integer get_contactsPastWeek() {
        // Date today = new Date();
        GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.UK);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        String keyToday = date2StringId(calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR));
        GregorianCalendar calendar1WeekAgo = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.UK);
        calendar1WeekAgo.add(Calendar.DATE, -7);

        Integer contactsPastWeek = 0;

        for (String key : _lStats.keySet()) {
            if (_lStats.get(key).get_date().getTimeInMillis() > (calendar1WeekAgo.getTimeInMillis()) &&
                    _lStats.get(key).get_date().getTimeInMillis() < (calendar.getTimeInMillis())) {
                contactsPastWeek += _lStats.get(key).get_telCount();
                contactsPastWeek += _lStats.get(key).get_emailCount();
            }
        }

        return contactsPastWeek;
    }

	/*
    public remove
	public DayStats[] getAll();
	public void empty();
	*/
}
