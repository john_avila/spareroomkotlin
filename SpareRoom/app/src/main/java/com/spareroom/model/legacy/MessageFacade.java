package com.spareroom.model.legacy;

import com.spareroom.integration.business.legacy.ILegacyBusinessAbstractFactory;
import com.spareroom.integration.business.legacy.rest.IMessageThreadDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;

public class MessageFacade implements IMessageFacade {
    private ILegacyBusinessAbstractFactory _webServiceFactory;

    /**
     * Constructor
     *
     * @param webServiceFactory Web Service Factory used for communicating with the server
     */
    public MessageFacade(ILegacyBusinessAbstractFactory webServiceFactory) {
        _webServiceFactory = webServiceFactory;
    }

    public SpareroomStatus newMessageThread(Parameters p) throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException {
        try {
            IMessageThreadDAO dao = _webServiceFactory.getMessageThreadDAO();
            return dao.create(p);
        } catch (NetworkConnectivityException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (ClientErrorException e) {
            throw new ServiceUnavailableException();
        } catch (ServerErrorException e) {
            throw new ServiceUnavailableException();
        }
    }
}
