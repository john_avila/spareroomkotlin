package com.spareroom.model;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.BusinessFactorySingleton;
import com.spareroom.integration.business.SpareroomAbstractDao;
import com.spareroom.integration.exception.*;
import com.spareroom.lib.core.Action;
import com.spareroom.model.business.RegisterParameters;
import com.spareroom.model.exception.*;

/**
 * ModelAction for for registering the device in the SpareRoom database
 */

public class RegisterRunModelAction extends ModelAction {

    public RegisterRunModelAction(final SpareroomContext srContext) {
        super(srContext);

        setAction(new Action() {
            @Override
            public Object execute(Object... params)
                    throws ModelOperationCannotBePerformedException,
                    ModelInvalidArgumentException,
                    ModelUnexpectedInternalIssueException {

                SpareroomAbstractDao<RegisterParameters, Void, Void> dao =
                        BusinessFactorySingleton.getInstance(srContext).getRegisterRunDao();

                try {

                    return dao.create((RegisterParameters) params[0], null);

                } catch (UnavailableDataSourceException e) {
                    throw new ModelOperationCannotBePerformedException(e.getMessage(), e);
                } catch (InvalidArgumentException e) {
                    throw new ModelInvalidArgumentException(e.getMessage(), e);
                } catch (UnexpectedResultException e) {
                    throw new ModelUnexpectedInternalIssueException(e.getMessage(), e);
                }

            }
        });
    }

    public RegisterRunModelAction(SpareroomContext srContext, Action action) {
        super(srContext, action);
    }

}
