package com.spareroom.model.business;

import java.util.LinkedList;

/**
 * Video gallery...
 * <p>
 * Created by miguel.rossi on 24/08/2016.
 */
public class VideoGallery {
    private LinkedList<Video> _gallery;

    public VideoGallery() {
        _gallery = new LinkedList<>();
    }

    public Video get(int position) {
        return _gallery.get(position);
    }

    public LinkedList<Video> getGallery() {
        return _gallery;
    }

    public void setGallery(LinkedList<Video> gallery) {
        this._gallery = gallery;
    }

    public int size() {
        return _gallery.size();
    }

}
