package com.spareroom.model.business

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.spareroom.integration.business.apiv2.ApiParams

data class PostCodeRestResponse(
    @Expose @SerializedName("postcode_unit") private val postcodeField: String?,
    @Expose @SerializedName("success") private val success: String?
) {
    val postcode get() = postcodeField ?: ""
    val failed get() = ApiParams.YES_INT != success
}