package com.spareroom.model.business;

/**
 * Data for register the device in the server
 * <p/>
 * Created by miguel.rossi on 26/01/2017.
 */
public class RegisterParameters {
    private String _myVersion;
    private String _firstOpen;

    public String getMyVersion() {
        return _myVersion;
    }

    public void setMyVersion(String myVersion) {
        _myVersion = myVersion;
    }

    public String getFirstOpen() {
        return _firstOpen;
    }

    public void setFirstOpen(String firstOpen) {
        _firstOpen = firstOpen;
    }

}
