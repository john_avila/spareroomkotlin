package com.spareroom.model.exception;

/**
 * Created by miguel.rossi on 23/05/2016.
 */
public class ModelInvalidArgumentException extends Exception {

    private static final long serialVersionUID = 4364680952246485488L;

    public ModelInvalidArgumentException() {
        super();
    }

    public ModelInvalidArgumentException(String detailMessage) {
        super(detailMessage);
    }

    public ModelInvalidArgumentException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ModelInvalidArgumentException(Throwable throwable) {
        super(throwable);
    }

}
