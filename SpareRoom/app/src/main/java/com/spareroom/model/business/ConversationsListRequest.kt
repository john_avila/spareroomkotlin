package com.spareroom.model.business

sealed class ConversationsListRequest

object RefreshConversationList : ConversationsListRequest()

class OpenConversation(val conversationInfo: ConversationInfo) : ConversationsListRequest()

