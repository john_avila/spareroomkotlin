package com.spareroom.model.legacy;

import android.content.Context;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.model.business.*;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;

import java.io.IOException;

public interface IAccountFacade {

    UpgradeList getUpgrades(Parameters params) throws MissingSystemFeatureException, ServiceUnavailableException, InconsistentStateException;

    SpareroomStatus upgrade(Parameters params) throws MissingSystemFeatureException, ServiceUnavailableException, InconsistentStateException;

    Object getEmailPreferences() throws MissingSystemFeatureException, ServiceUnavailableException, InconsistentStateException;

    Object updateName(Parameters params) throws MissingSystemFeatureException, ServiceUnavailableException, InconsistentStateException;

    Object updateEmail(Parameters params) throws MissingSystemFeatureException, ServiceUnavailableException, InconsistentStateException;

    Object updatePassword(Parameters params) throws MissingSystemFeatureException, ServiceUnavailableException, InconsistentStateException;

    Object updateEmailPreferences(Parameters params) throws MissingSystemFeatureException, ServiceUnavailableException, InconsistentStateException;

    Stats getMyOfferedAdStats(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException;

    Stats getMyWantedAdStats(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException;

    PictureGallery getPictures(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException;

    void removePicture(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException;

    void setOrderPicture(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException;

    void saveDraftAd(Context applicationContext, DraftAd ad) throws IOException;

    DraftAd loadDraftAd(Context applicationContext) throws IOException;

    VideoGallery getVideoGallery(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException;

    void removeVideo(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException;

}
