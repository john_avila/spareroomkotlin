package com.spareroom.model.business;

/**
 * An alive, validated link to an Account.
 * <p>
 * Created by manuel on 22/06/2016.
 */
public interface AccountConnectionInterface {
// TODO review whether this should be an interface or a class. This would affect Session DAOs
// This class is not developed any further for now for compatibility with legacy classes
}
