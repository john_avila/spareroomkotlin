package com.spareroom.model.business;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * List of preferences that can be selected.
 */
public class PreferenceList implements Serializable {

    //region FIELDS

    private static final long serialVersionUID = 7595118059031619459L;

    private List<PreferenceItem> _list = new LinkedList<>();
    private Enum[] _listEnum = null;

    //endregion FIELDS

    //region CONSTRUCTORS

    public PreferenceList() {
    }

    /**
     * Constructor.<br/>
     * Assumes that all preferences are checked by default.
     *
     * @param preferenceEnum <code>Enum</code> containing all preferences available for this <code>PreferenceList</code>.
     * @deprecated use {@link #PreferenceList(Enum[] preferenceEnum, boolean defaultChecked)} instead.
     */
    @Deprecated
    public PreferenceList(Enum[] preferenceEnum) {
        this(preferenceEnum, true);
    } //end PreferenceList(Enum[] preferenceEnum)

    /**
     * Constructor.
     *
     * @param preferenceEnum <code>Enum</code> containing all preferences available for this <code>PreferenceList</code>.
     * @param defaultChecked default value for all preferences in <code>preferenceEnum</code>.
     */
    public PreferenceList(Enum[] preferenceEnum, boolean defaultChecked) {
        _listEnum = preferenceEnum;

        for (Enum enumItem : preferenceEnum)
            add(
                    new PreferenceItem(
                            enumItem.ordinal(),
                            enumItem.toString(),
                            defaultChecked
                    )
            );
    } //end PreferenceList(Enum[] preferenceEnum, boolean defaultChecked)

    //endregion CONSTRUCTORS

    /**
     * Returns the enum that populates this list. <code>null</code> if it was not populated through
     * <code>PreferenceList(Enum[] preferenceEnum)</code>
     *
     * @return enum that populated this list
     */
    public Enum[] getEnum() {
        return _listEnum;
    }

    /**
     * Override the list with items.
     *
     * @param _list list with items.
     */
    public void setList(List<PreferenceItem> _list) {
        this._list = _list;
    }

    /**
     * Gets the list with items.
     *
     * @return the list.
     */
    public List<PreferenceItem> getList() {
        return _list;
    }

    /**
     * Adds an item to the list.
     *
     * @param item item to add.
     */
    public void add(PreferenceItem item) {
        _list.add(item);
    }

    /**
     * Gives the item in a determinate position.
     *
     * @param i position of the item.
     * @return the item in the given position.
     */
    public PreferenceItem get(int i) {
        return _list.get(i);
    }

    /**
     * Gives the size of the list.
     *
     * @return the size.
     */
    public int size() {
        return _list.size();
    }

}
