package com.spareroom.model.business;

/**
 * Video POJO
 * <p>
 * Created by miguel.rossi on 24/08/2016.
 */
public class Video {
    public static final int MAX_SIZE_IN_BYTES = 52428800;
    public static final int RECOMMENDED_LENGTH_IN_MILLIS = 180000;
    public static final int MAX_LENGTH_IN_MILLIS = 300000;
    private String _caption;
    private Status _status;
    private String _id;

    public enum Status {
        PENDING,
        LIVE,
        REJECTED,
        UPLOADING,
        UPLOAD_FAILED,
        BLANK;

        public String toString() {
            String ret;

            if (ordinal() == PENDING.ordinal())
                ret = "Pending";
            else if (ordinal() == LIVE.ordinal())
                ret = "Live";
            else if (ordinal() == REJECTED.ordinal())
                ret = "Rejected";
            else if (ordinal() == UPLOADING.ordinal())
                ret = "Uploading";
            else if (ordinal() == UPLOAD_FAILED.ordinal())
                ret = "Upload failed";
            else if (ordinal() == BLANK.ordinal())
                ret = "Video doesn't exist";
            else
                ret = null;

            return ret;
        }

    } //end enum Status

    public String getCaption() {
        return _caption;
    }

    public void setCaption(String caption) {
        _caption = caption;
    }

    public Status getStatus() {
        return _status;
    }

    public void setStatus(Status status) {
        _status = status;
    }

    public String getId() {
        return _id;
    }

    public void setId(String id) {
        _id = id;
    }

}
