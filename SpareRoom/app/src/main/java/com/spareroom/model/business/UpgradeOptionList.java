package com.spareroom.model.business;

import java.util.TreeMap;

public class UpgradeOptionList {
    private TreeMap<String, UpgradeOption> _options;

    public UpgradeOptionList() {
        _options = new TreeMap<>();
    }

    public TreeMap<String, UpgradeOption> get_options() {
        return _options;
    }

    public void set_options(TreeMap<String, UpgradeOption> _options) {
        this._options = _options;
    }

    public void add(String key, UpgradeOption value) {
        _options.put(key, value);
    }

}
