package com.spareroom.model.business;

import android.content.Context;

import com.spareroom.R;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.ui.util.StringUtils;

public abstract class Response<T> {

    private T response;
    private Throwable throwable;
    private String errorCode;
    private String errorMessage;

    public Response() {

    }

    public Response(T response, Throwable throwable) {
        this.response = response;
        this.throwable = throwable;
    }

    public boolean isSuccessful() {
        return response != null;
    }

    public boolean hasError() {
        return !StringUtils.isNullOrEmpty(errorCode);
    }

    String getErrorCode() {
        return errorCode;
    }

    public boolean hasThrowable() {
        return throwable != null;
    }

    String getErrorMessage() {
        return errorMessage;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public void setError(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }

    public String getErrorMessage(Context context, String defaultErrorMessage) {
        if (isNoConnectionException())
            return context.getString(R.string.no_connection);
        return !StringUtils.isNullOrEmpty(getErrorMessage()) ? getErrorMessage() : defaultErrorMessage;
    }

    public boolean isNoConnectionException() {
        return hasThrowable() && (throwable instanceof NetworkConnectivityException || throwable.getCause() instanceof NetworkConnectivityException);
    }
}
