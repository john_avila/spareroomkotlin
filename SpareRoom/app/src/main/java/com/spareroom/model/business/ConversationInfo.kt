package com.spareroom.model.business

import java.io.Serializable

data class ConversationInfo(
    val threadIdField: String?,
    val advertIdField: String?,
    val otherUserIdField: String?,
    val otherUserFirstNamesField: String?,
    val otherUserLastNameField: String?,
    val otherUserAvatarUrlField: String?
) : Serializable {

    companion object {
        const val TAG = "ConversationStateTag"
    }

    var threadId = threadIdField ?: "${otherUserIdField}_$advertIdField"
    var advertId = advertIdField ?: ""
    var otherUserId = otherUserIdField ?: ""
    var otherUserFirstNames = otherUserFirstNamesField ?: ""
    var otherUserLastName = otherUserLastNameField ?: ""
    var otherUserAvatarUrl = otherUserAvatarUrlField ?: ""
}
