package com.spareroom.model.business;

public class DraftAdState {
    private String _screen;
    private String _placeAdState;

    public String get_screen() {
        return _screen;
    }

    public void set_screen(String _screen) {
        this._screen = _screen;
    }

    public String get_placeAdState() {
        return _placeAdState;
    }

    public void set_placeAdState(String _placeAdState) {
        this._placeAdState = _placeAdState;
    }

}
