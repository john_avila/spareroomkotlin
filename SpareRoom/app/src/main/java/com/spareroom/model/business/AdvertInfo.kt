package com.spareroom.model.business

import com.spareroom.model.business.date.IsoDate
import java.io.Serializable

data class AdvertInfo(val id: String, val title: String, val status: String, val datePlaced: String, val upgradeApplied: String,
                      val upgradeDaysLeft: String, val dateLastRenewed: String, val dateLastLive: IsoDate
) : Serializable {

    companion object {

        @JvmStatic
        fun fromAdvert(advert: AbstractAd): AdvertInfo {
            return AdvertInfo(advert.id, advert.title, advert.status, advert.datePlaced, advert.upgradeApplied, advert.upgradeDaysLeft,
                advert.dateLastRenewed, IsoDate(advert.dateLastLive))
        }
    }
}