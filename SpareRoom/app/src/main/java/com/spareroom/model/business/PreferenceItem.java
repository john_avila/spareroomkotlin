package com.spareroom.model.business;

import java.io.Serializable;

/**
 * A preference that can be chosen
 */
public class PreferenceItem implements Serializable {

    //region FIELDS

    private static final long serialVersionUID = -795296248260680192L;

    private int _id;
    private String _name;
    private boolean _isChecked;

    //endregion FIELDS

    //region CONSTRUCTORS

    public PreferenceItem(int id) {
        _id = id;
    }

    public PreferenceItem(int id, String name, boolean checked) {
        _id = id;
        _name = name;
        _isChecked = checked;
    }

    //endregion CONSTRUCTORS

    //region GETTERS AND SETTERS

    /**
     * Gets the id of the item.
     *
     * @return the id.
     */
    public int getId() {
        return _id;
    }

    /**
     * Sets the id of the item.
     *
     * @param id the new id.
     */
    public void setId(int id) {
        _id = id;
    }

    /**
     * Gets the name of  the item.
     *
     * @return the name.
     */
    public String getName() {
        return _name;
    }

    /**
     * Sets the new name of the item.
     *
     * @param name the new name.
     */
    public void setName(String name) {
        _name = name;
    }

    /**
     * Returns whether the item is selected or not.
     *
     * @return <code>true</code> if the user selected it, <code>false</code> otherwise.
     */
    public boolean isChecked() {
        return _isChecked;
    }

    /**
     * Sets if the item is selected.
     *
     * @param _isChecked <code>true</code> if the user selected it, <code>false</code> otherwise.
     */
    public void setChecked(boolean _isChecked) {
        this._isChecked = _isChecked;
    }

    //endregion GETTERS AND SETTERS

}
