package com.spareroom.model.business;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;

public class Conversation {

    private static final String AUTO_DECLINE = "auto-decline";
    private static final String ADVERT_WANTED = "wanted";
    private static final String ADVERT_OFFERED = "offered";

    @Expose
    @SerializedName("success")
    private String success;

    @Expose
    @SerializedName("response_id")
    private String errorCode;

    @Expose
    @SerializedName("error")
    private String errorMessage;

    @Expose
    @SerializedName("conversation_count")
    private int conversationCount;

    @Expose
    @SerializedName("conversation_with")
    private String otherUserId;

    @Expose
    @SerializedName("advert_id")
    private String advertId;

    @Expose
    @SerializedName("subject")
    private String subject;

    @Expose
    @SerializedName("advert_placed_by")
    private String advertPlacedBy;

    @Expose
    @SerializedName("thread_status")
    private String threadStatus;

    @Expose
    @SerializedName("ad_type")
    private String advertType;

    @Expose
    @SerializedName("conversation_with_first_name")
    private String otherUserFirstNames;

    @Expose
    @SerializedName("conversation_with_last_name")
    private String otherUserLastName;

    @Expose
    @SerializedName("conversation_with_last_online_date")
    private String otherUserLastOnline;

    @Expose
    @SerializedName("conversation")
    private List<Message> messages;

    @Expose
    @SerializedName("conversation_with_wanted_advert_count")
    private int otherUserWantedAdvertCount;

    @Expose
    @SerializedName("conversation_with_offered_advert_count")
    private int otherUserOfferedAdvertCount;

    public List<Message> getMessages() {
        return messages != null ? messages : new ArrayList<>();
    }

    public String getSubject() {
        return subject;
    }

    public String getAdvertId() {
        return advertId;
    }

    public String getAdvertPlacedBy() {
        return advertPlacedBy;
    }

    public boolean isClosed() {
        return messages != null && messages.size() > 0 && AUTO_DECLINE.equals(messages.get(messages.size() - 1).getResponseType());
    }

    public boolean isAdvertWanted() {
        return ADVERT_WANTED.equalsIgnoreCase(advertType);
    }

    public boolean isAdvertOffered() {
        return ADVERT_OFFERED.equalsIgnoreCase(advertType);
    }

    public String getOtherUserFirstNames() {
        return otherUserFirstNames;
    }

    public String getOtherUserLastName() {
        return otherUserLastName;
    }

    public String getOtherUserLastOnline() {
        return otherUserLastOnline;
    }

    public boolean isOtherUserAgent(String otherUserId) {
        if (messages == null || messages.isEmpty())
            return false;

        for (Message message : messages) {
            if (Message.OFFERED.equalsIgnoreCase(message.getAdType()) && otherUserId.equalsIgnoreCase(message.getAdvertPlacedBy()))
                return AbstractAd.ADVERTISER_TYPE_AGENT.equalsIgnoreCase(message.getRegardingAdvertAdvertiserType());
        }

        return false;
    }

    public int getOtherUserWantedAdvertCount() {
        return otherUserWantedAdvertCount;
    }

    public int getOtherUserOfferedAdvertCount() {
        return otherUserOfferedAdvertCount;
    }

    public boolean isSuccessful() {
        return SpareroomStatusCode.CODE_SUCCESS.equalsIgnoreCase(success);
    }

    @NonNull
    public String errorCode() {
        return errorCode != null ? errorCode : "";
    }

    @NonNull
    public String errorMessage() {
        return errorMessage != null ? errorMessage : "";
    }

}
