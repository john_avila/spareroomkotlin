package com.spareroom.model.business;

public class MapReference {
    private Float _latitudeDelta;
    private Float _longitudeDelta;
    private Float _latitude;
    private Float _longitude;

    public MapReference() {
        _latitude = 0f;
        _longitude = 0f;
        _latitudeDelta = 1f;
        _longitudeDelta = 1f;
    }

    public Float get_latitudeDelta() {
        return _latitudeDelta;
    }

    public void set_latitudeDelta(Float _latitudeDelta) {
        this._latitudeDelta = _latitudeDelta;
    }

    public Float get_longitudeDelta() {
        return _longitudeDelta;
    }

    public void set_longitudeDelta(Float _longitudeDelta) {
        this._longitudeDelta = _longitudeDelta;
    }

    public Float get_latitude() {
        return _latitude;
    }

    public void set_latitude(Float _latitude) {
        this._latitude = _latitude;
    }

    public Float get_longitude() {
        return _longitude;
    }

    public void set_longitude(Float _longitude) {
        this._longitude = _longitude;
    }

}
