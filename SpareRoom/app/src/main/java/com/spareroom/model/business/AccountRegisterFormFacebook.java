package com.spareroom.model.business;

/**
 * Form with data needed to register a new Account
 * <p>
 * Created by manuel on 22/06/2016.
 */
public class AccountRegisterFormFacebook extends AccountRegisterForm {
    private String _facebookAccessToken;
    private String _facebookUserId;
    private String _facebookTokenExpires;

    public String getFacebookAccessToken() {
        return _facebookAccessToken;
    }

    public void setFacebookAccessToken(String facebookAccessToken) {
        _facebookAccessToken = facebookAccessToken;
    }

    public String getFacebookUserId() {
        return _facebookUserId;
    }

    public void setFacebookUserId(String facebookfbUserId) {
        _facebookUserId = facebookfbUserId;
    }

    public String getFacebookTokenExpires() {
        return _facebookTokenExpires;
    }

    public void setFacebookTokenExpires(String facebookTokenExpires) {
        _facebookTokenExpires = facebookTokenExpires;
    }

}
