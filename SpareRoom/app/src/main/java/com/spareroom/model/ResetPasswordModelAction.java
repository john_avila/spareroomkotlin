package com.spareroom.model;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.BusinessFactorySingleton;
import com.spareroom.integration.business.SpareroomAbstractDao;
import com.spareroom.integration.exception.*;
import com.spareroom.lib.core.Action;
import com.spareroom.model.business.AccountConnectionInterface;
import com.spareroom.model.business.AccountCredentials;
import com.spareroom.model.exception.*;
import com.spareroom.model.extra.ResetPasswordExtra;

/**
 * Model operation for Reset Password.
 */
public class ResetPasswordModelAction extends ModelAction {

    public ResetPasswordModelAction(final SpareroomContext srContext) {
        super(srContext);

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {
                AccountCredentials credentials = (AccountCredentials) params[0];
                ResetPasswordExtra extra = (ResetPasswordExtra) params[1];
                SpareroomAbstractDao<AccountCredentials, ResetPasswordExtra, AccountConnectionInterface> dao =
                        BusinessFactorySingleton.getInstance(srContext).getResetPasswordTransactionDao();

                try {
                    return dao.create(credentials, extra);

                } catch (UnavailableDataSourceException e) {
                    throw new ModelOperationCannotBePerformedException(e.getMessage(), e);
                } catch (InvalidArgumentException e) {
                    throw new ModelInvalidArgumentException(e.getMessage(), e);
                } catch (UnexpectedResultException e) {
                    throw new ModelUnexpectedInternalIssueException(e.getMessage(), e);
                }

            }
        });
    }

    /**
     * Constructor. Do not use unless Unit Testing. Instead, extend this class and override
     * {@link #ResetPasswordModelAction(SpareroomContext srContext)} specifying the <code>Action</code>
     * within the constructor to promote encapsulation.
     *
     * @param srContext the current <code>SpareroomContext</code>
     * @param action    model logic
     */
    public ResetPasswordModelAction(SpareroomContext srContext, Action action) {
        super(srContext, action);
    }
}
