package com.spareroom.model.business;

import java.util.LinkedList;
import java.util.List;

public class ProfilePreference {
    public static final String DATA_TYPE_BOOLEAN = "boolean";
    public static final String DATA_TYPE_ENUM = "enum";
    private String _description;
    private String _permission;
    private String _type;
    private String _dataType;
    private List<AvailableOption> _listAvailableOption =
            new LinkedList<>();

    public static class AvailableOption {
        public String _value;
        public String _description;
        public String _dataValue;

        public AvailableOption(String value, String description, String dataValue) {
            _value = value;
            _description = description;
            _dataValue = dataValue;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final AvailableOption other = (AvailableOption) obj;
            return this._value.equals(other._value);
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 97 * hash + _value.hashCode();
            return hash;
        }
    }

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }

    public String get_permission() {
        return _permission;
    }

    public void set_permission(String _permission) {
        this._permission = _permission;
    }

    public String get_type() {
        return _type;
    }

    public void set_type(String _type) {
        this._type = _type;
    }

    public List<AvailableOption> get_listAvailableOption() {
        return _listAvailableOption;
    }

    public void set_listAvailableOption(List<AvailableOption> listAvailableOption) {
        _listAvailableOption = listAvailableOption;
    }

    public void add(AvailableOption option) {
        _listAvailableOption.add(option);
    }

    public void addAvailableOption(String key, String value, String dataValue) {
        _listAvailableOption.add(new AvailableOption(key, value, dataValue));
    }

    public boolean hasAvailableOption() {
        return !_listAvailableOption.isEmpty();
    }

    public String get_dataType() {
        return _dataType;
    }

    public void set_dataType(String _dataType) {
        this._dataType = _dataType;
    }

}
