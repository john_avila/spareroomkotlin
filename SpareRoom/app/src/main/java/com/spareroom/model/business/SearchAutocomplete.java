package com.spareroom.model.business;

import java.io.Serializable;

public class SearchAutocomplete implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -7837026325819347801L;
    private String _suggestion;

    public SearchAutocomplete() {

    }

    public SearchAutocomplete(String suggestion) {
        _suggestion = suggestion;
    }

    public String get_suggestion() {
        return _suggestion;
    }

    public void set_suggestion(String _suggestion) {
        this._suggestion = _suggestion;
    }

}
