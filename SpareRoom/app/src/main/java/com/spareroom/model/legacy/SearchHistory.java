package com.spareroom.model.legacy;

import com.spareroom.controller.AppVersion;

import java.io.Serializable;

public class SearchHistory implements Serializable {
    private static final long serialVersionUID = 505789466756525364L;
    private String[] _history = new String[3];

    public SearchHistory() {
        _history = new String[3];
    }

    /**
     * Adds an item to the beginning of the history
     */
    public void add(String search) {
        int posSearch = -1;
        for (int i = 0; i < _history.length - 1; i++) {
            if ((_history[i] != null) &&
                    _history[i]
                            .trim()
                            .toLowerCase(AppVersion.flavor().getLocale())
                            .replaceAll("\\s+", " ")
                            .equals(search.trim().toLowerCase(AppVersion.flavor().getLocale()).replaceAll("\\s+", " ")))
                posSearch = i;
        }
        if (posSearch == 0)
            return;
        if (posSearch > -1) {
            System.arraycopy(_history, posSearch + 1, _history, posSearch, _history.length - 1 - posSearch);
        }

        System.arraycopy(_history, 0, _history, 1, _history.length - 1);
        _history[0] = search.trim().replaceAll("\\s+", " ");
    }

    public String[] getAll() {
        return _history;
    }

    public boolean isEmpty() {
        for (String s : _history)
            if (s != null)
                return false;
        return true;
    }
}
