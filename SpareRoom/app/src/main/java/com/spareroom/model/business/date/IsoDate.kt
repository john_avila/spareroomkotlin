package com.spareroom.model.business.date

import java.io.Serializable
import java.util.*

data class IsoDate(val date: Calendar? = null) : Serializable