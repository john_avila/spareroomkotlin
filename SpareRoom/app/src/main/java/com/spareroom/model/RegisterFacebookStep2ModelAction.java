package com.spareroom.model;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.BusinessFactorySingleton;
import com.spareroom.integration.business.SpareroomAbstractDao;
import com.spareroom.integration.exception.*;
import com.spareroom.lib.core.Action;
import com.spareroom.model.business.AccountConnectionInterface;
import com.spareroom.model.business.AccountRegisterFormFacebook;
import com.spareroom.model.exception.*;
import com.spareroom.model.extra.RegisterExtra;

/**
 * Model operation for Logging in.
 */
public class RegisterFacebookStep2ModelAction extends ModelAction {

    /**
     * Constructor.
     *
     * @param srContext the current <code>SpareroomContext</code>
     */
    public RegisterFacebookStep2ModelAction(final SpareroomContext srContext) {
        super(srContext);
        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {
                // no checking params here, the ControllerAction should had made sure this
                // ModelAction is invoked correctly

                AccountRegisterFormFacebook registerForm = (AccountRegisterFormFacebook) params[0];
                RegisterExtra extra = (RegisterExtra) params[1];
                SpareroomAbstractDao<AccountRegisterFormFacebook, RegisterExtra, AccountConnectionInterface> dao =
                        BusinessFactorySingleton.getInstance(srContext).getRegisterFacebookTransactionDao();

                try {

                    dao.update(registerForm, extra);
                    return null;

                } catch (UnavailableDataSourceException e) {
                    throw new ModelOperationCannotBePerformedException(e.getMessage(), e);
                } catch (InvalidArgumentException e) {
                    throw new ModelInvalidArgumentException(e.getMessage(), e);
                } catch (UnexpectedResultException e) {
                    throw new ModelUnexpectedInternalIssueException(e.getMessage(), e);
                }
            }

        });
    }

    /**
     * Constructor. Do not use unless Unit Testing. Instead, extend this class and override
     * {@link #RegisterFacebookStep2ModelAction(SpareroomContext srContext)} specifying the
     * <code>Action</code> within the constructor to promote encapsulation.
     *
     * @param srContext the current <code>SpareroomContext</code>
     * @param action    model logic
     */
    public RegisterFacebookStep2ModelAction(SpareroomContext srContext, Action action) {
        super(srContext, action);
    }
}
