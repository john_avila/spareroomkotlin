package com.spareroom.model.business

import com.spareroom.lib.util.IEnum
import com.spareroom.ui.util.AdvertUtils
import java.io.Serializable

interface SearchAdvertListProperties : Serializable {
    var where: String?
    var page: String?
    var maxPerPage: String?
    var keyword: String?
    var featured: Boolean
    var sortedBy: SearchAdvertListProperties.SortedBy

    var gayShare: Boolean
    var photoAdvertsOnly: Boolean
    var includeHidden: Boolean
    var availableFrom: String?
    var minRent: Int?
    var maxRent: Int?
    var minAge: Int?
    var maxAge: Int?

    var minTerm: SearchAdvertListProperties.MinTerm
    var maxTerm: SearchAdvertListProperties.MaxTerm
    var daysOfWeekAvailable: SearchAdvertListProperties.DaysOfWeek
    var smoking: SearchAdvertListProperties.Smoking
    var roomType: SearchAdvertListProperties.RoomTypes
    var shareType: SearchAdvertListProperties.ShareType
    var genderFilter: SearchAdvertListProperties.GenderFilter

    companion object {
        const val DEFAULT_MAX_PER_PAGE = "10"
        const val DEFAULT_PAGE = "1"
        const val PW = "pw"
        const val PCM = "pcm"
    }

    enum class SortedBy(private val value: String?) : IEnum {
        DEFAULT(null),
        LAST_UPDATED("last_updated"),
        DAYS_SINCE_PLACED("days_since_placed"),
        PRICE_LOW_TO_HIGH("price_low_to_high"),
        PRICE_HIGH_TO_LOW("price_high_to_low");

        override fun getValue() = value
    }

    enum class MinTerm(private val value: String?) : IEnum {
        NOT_SET(null),
        ONE("1"),
        TWO("2"),
        THREE("3"),
        FOUR("4"),
        FIVE("5"),
        SIX("6"),
        SEVEN("7"),
        EIGHT("8"),
        NINE("9"),
        TEN("10"),
        ELEVEN("11"),
        TWELVE("12"),
        FIFTEEN("15"),
        EIGHTEEN("18"),
        TWENTY_ONE("21"),
        TWENTY_FOUR("24");

        val intValue
            get() = if (value == null) NOT_SET.ordinal else Integer.parseInt(value)

        override fun getValue() = value
    }

    enum class MaxTerm(private val value: String?) : IEnum {
        NOT_SET(null),
        ONE("1"),
        TWO("2"),
        THREE("3"),
        FOUR("4"),
        FIVE("5"),
        SIX("6"),
        SEVEN("7"),
        EIGHT("8"),
        NINE("9"),
        TEN("10"),
        ELEVEN("11"),
        TWELVE("12"),
        FIFTEEN("15"),
        EIGHTEEN("18"),
        TWENTY_ONE("21"),
        TWENTY_FOUR("24"),
        THIRTY_SIX("36");

        val intValue
            get() = if (value == null) NOT_SET.ordinal else Integer.parseInt(value)

        override fun getValue() = value
    }

    enum class DaysOfWeek(private val value: String?) : IEnum {
        NOT_SET(null),
        WEEKDAYS("Mon to Fri only"),
        WEEKENDS("Weekends only"),
        SEVEN_DAYS("7 days a week");

        override fun getValue() = value
    }

    enum class Smoking(private val value: String?) : IEnum {
        NOT_SET(null),
        YES("Y"),
        NO("N");

        override fun getValue() = value
    }

    enum class RoomTypes(private val value: String?) : IEnum {
        NOT_SET(null),
        DOUBLE("double"),
        SINGLE("single");

        override fun getValue() = value
    }

    enum class ShareType(private val value: String?) : IEnum {
        NOT_SET(null),
        PROFESSIONALS("P"),
        STUDENTS("S");

        override fun getValue() = value
    }

    enum class GenderFilter(private val value: String?) : IEnum {
        NOT_SET(null),
        MALES("males"),
        FEMALES("females"),
        MIXED("mixed");

        override fun getValue() = value
    }

    fun per() = if (minRent != null || maxRent != null) PCM else null

    fun minWeeklyRent(advertUtils: AdvertUtils, rent: Int?) {
        this.minRent = if (rent != null) advertUtils.fromPwToPcmRent(rent) else null
    }

    fun minMonthlyRent(rent: Int?) {
        this.minRent = rent
    }

    fun maxWeeklyRent(advertUtils: AdvertUtils, rent: Int?) {
        this.maxRent = if (rent != null) advertUtils.fromPwToPcmRent(rent) else null
    }

    fun maxMonthlyRent(rent: Int?) {
        this.maxRent = rent
    }

}
