package com.spareroom.model.business

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.spareroom.lib.util.paging.PageItem

private const val OUT = "out"
private const val READ = "read"
private const val UNREAD = "unread"

data class ConversationPreview(
    @Expose @SerializedName("thread_id") private val threadIdField: String? = "",
    @Expose @SerializedName("advert_id") private val advertIdField: String? = "",
    @Expose @SerializedName("conversation_with") private val otherUserIdField: String? = "",
    @Expose @SerializedName("conversation_with_first_name") private val otherUserNameField: String? = "",
    @Expose @SerializedName("conversation_with_last_name") private val otherUserLastNameField: String? = "",
    @Expose @SerializedName("conversation_with_profile_photo_url") private val otherUserAvatarUrlField: String? = "",
    @Expose @SerializedName("date_last_activity") private val dateLastActivityField: String? = "",
    @Expose @SerializedName("direction") private val direction: String? = "",
    @Expose @SerializedName("snippet_long") private val snippetField: String? = "",
    @Expose @SerializedName("subject") private val advertTitleField: String? = "",
    @Expose @SerializedName("thread_status") private var conversationStatusField: String? = ""
) : PageItem {

    override val uniqueId get() = threadId
    val threadId get() = threadIdField ?: ""
    val advertId get() = advertIdField ?: ""
    val otherUserId get() = otherUserIdField ?: ""
    val otherUserName get() = otherUserNameField ?: ""
    val otherUserLastName get() = otherUserLastNameField ?: ""
    val otherUserAvatarUrl get() = otherUserAvatarUrlField ?: ""
    val advertTitle get() = advertTitleField ?: ""
    val dateLastActivity get() = dateLastActivityField ?: ""
    val snippet get() = snippetField ?: ""

    val isOut get() = OUT == direction
    val isUnread get() = UNREAD == conversationStatusField

    fun setRead() {
        conversationStatusField = READ
    }

}
