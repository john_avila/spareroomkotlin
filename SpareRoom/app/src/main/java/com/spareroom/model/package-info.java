/**
 * Model of the Model-View-Controller. Application logic.
 */
package com.spareroom.model;