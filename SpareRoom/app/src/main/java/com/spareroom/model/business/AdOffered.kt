package com.spareroom.model.business

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.spareroom.model.business.advertiser.Advertiser
import com.spareroom.model.business.advertiser.OfferedAdvertiser
import com.spareroom.model.business.date.IsoDate

data class AdOffered(

    // offered
    @Expose @SerializedName("accom_type") private val accommodationTypeField: String? = "",
    @Expose @SerializedName("neighbourhood_name") private val areaField: String? = "",
    @Expose @SerializedName("rent_options") private val rentOptionField: String? = "",
    @Expose @SerializedName("min_rent") private val minRentField: String? = "",
    @Expose @SerializedName("postcode") private val postcodeField: String? = "",
    @Expose @SerializedName("bills_inc") private val billsIncludedField: String? = "",
    @Expose @SerializedName("security_deposit") private val securityDepositField: String? = "",
    @Expose @SerializedName("rooms") private val roomsField: List<AdRoom>? = emptyList(),
    @Expose @SerializedName("property_type") private val propertyTypeField: String? = "",
    @Expose @SerializedName("rooms_in_property") private val roomsInPropertyField: String? = "",
    @Expose @SerializedName("latitude") private val latitudeField: String? = "",
    @Expose @SerializedName("longitude") private val longitudeField: String? = "",
    @Expose @SerializedName("advertiser_type") private val advertiserTypeField: String? = "",
    @Expose @SerializedName("company_name") private val companyNameField: String? = "",

    // common
    @Expose @SerializedName("available_from") override val availableFromField: String? = "",
    @Expose @SerializedName("per") override val pricePeriodField: String? = "",
    @Expose @SerializedName("advert_id") override val idField: String? = "",
    @Expose @SerializedName("new") override val newnessStatusField: String? = "",
    @Expose @SerializedName("photos") override val photosField: List<Picture>? = emptyList(),
    @Expose @SerializedName("upgrade_applied") override val upgradeAppliedField: String? = "",
    @Expose @SerializedName("days_left") override val upgradeDaysLeftField: String? = "",
    @Expose @SerializedName("date_last_renewed") override val dateLastRenewedField: String? = "",
    @Expose @SerializedName("date_ad_placed") override val datePlacedField: String? = "",
    @Expose @SerializedName("date_live_iso8601") override val dateLastLiveField: IsoDate? = null,
    @Expose @SerializedName("ad_title") override val titleField: String? = "",
    @Expose @SerializedName("ad_status") override val statusField: String? = "",
    @Expose @SerializedName("ad_text_255") override val shortDescriptionField: String? = "",
    @Expose @SerializedName("ad_text") override val fullDescriptionField: String? = "",
    @Expose @SerializedName("featured") override val featuredField: String? = "",
    @Expose @SerializedName("bold_ad") override val boldField: String? = "",
    @Expose @SerializedName("youtube_id") override val youtubeIdField: String? = "",
    @Expose @SerializedName("advertiser_id") override val advertiserIdField: String? = "",
    @Expose @SerializedName("tel_formatted") override val advertiserPhoneNumberField: String? = "",
    @Expose @SerializedName("early_bird_required") override val earlyBirdRequiredField: String? = "",
    @Expose @SerializedName("contactable_by_tel") override val contactableByPhoneField: String? = "",
    @Expose @SerializedName("free_to_contact") override val freeToContactField: String? = "",
    @Expose @SerializedName("profession") override val professionField: String? = "",
    @Expose @SerializedName("advertiser_name") override val advertiserNameField: String? = "",
    @Expose @SerializedName("couples") override val couplesField: String? = "",
    @Expose @SerializedName("advertiser_profile_photo_url") override val advertiserProfilePhotoUrlField: String? = "",
    @Expose @SerializedName("context") override var contextField: String? = "",
    @Expose @SerializedName("main_image_large_url") override var largePictureUrlField: String? = ""

) : AbstractAd() {

    val accommodationType get() = accommodationTypeField ?: ""
    val area get() = areaField ?: ""
    val rentOption get() = rentOptionField ?: ""
    val minRent get() = minRentField ?: ""
    val postcode get() = postcodeField ?: ""
    val billsIncluded get() = billsIncludedField ?: ""
    val securityDeposit get() = securityDepositField ?: ""
    val rooms get() = roomsField ?: emptyList()
    val propertyType get() = propertyTypeField ?: ""
    val advertiserType get() = advertiserTypeField ?: ""

    val latitude get() = latitudeField?.toFloatOrNull() ?: 0F
    val longitude get() = longitudeField?.toFloatOrNull() ?: 0F
    val roomsInProperty get() = roomsInPropertyField?.toIntOrNull() ?: 0

    override val price get() = getPrice(minRent, pricePeriod)

    override val advertiser: Advertiser
        get() = OfferedAdvertiser(advertiserProfilePhotoUrl, advertiserName, companyNameField ?: "", advertiserType, profession, couples)

    fun isWholePropertyForRent() = RENT_OPTION_WHOLE.equals(rentOption, ignoreCase = true)

    fun isFlat() = PROPERTY_TYPE_FLAT.equals(propertyType, ignoreCase = true)

    fun isHouse() = PROPERTY_TYPE_HOUSE.equals(propertyType, ignoreCase = true)

    fun isProperty() = PROPERTY_TYPE_PROPERTY.equals(propertyType, ignoreCase = true)

    fun isStudioFlat() = isStudio() && isFlat()

    fun isStudioHouse() = isStudio() && isHouse()

    fun isStudioProperty() = isStudio() && isProperty()

    fun isStudio() = isWholePropertyForRent() && ACCOMMODATION_STUDIO.equals(accommodationType, ignoreCase = true)

}