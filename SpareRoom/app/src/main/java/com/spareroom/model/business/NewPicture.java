package com.spareroom.model.business;

public class NewPicture {

    private String uri;
    private String photoId;
    private String flatshareId;
    private String flatshareType;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getFlatshareId() {
        return flatshareId;
    }

    public void setFlatshareId(String flatshareId) {
        this.flatshareId = flatshareId;
    }

    public String getFlatshareType() {
        return flatshareType;
    }

    public void setFlatshareType(String flatshareType) {
        this.flatshareType = flatshareType;
    }
}
