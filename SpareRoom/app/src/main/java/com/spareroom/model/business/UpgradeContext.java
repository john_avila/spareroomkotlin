package com.spareroom.model.business;

/**
 * Created by manuel on 03/03/2016.
 */
public class UpgradeContext {
    public static final int OFFERED_AD = 99;
    public static final int WANTED_AD = 105;
    public static final int MY_ACCOUNT = 46;
    public static final int EXTRA_LISTINGS = 550;

    private static final String NAME_OFFERED_AD = "Upgrade from offered advert";
    private static final String NAME_WANTED_AD = "Upgrade from wanted advert";
    private static final String NAME_MY_ACCOUNT = "Upgrade from My Account";
    private static final String NAME_EXTRA_LISTINGS = "Upgrade from Extra Listings";

    /**
     * Gets the name of the context
     *
     * @param contextId id for the context
     * @return the name of the context. <code>null</code> if contextId cannot be matched to any
     * context
     */
    public static String getName(int contextId) {
        switch (contextId) {
            case OFFERED_AD:
                return NAME_OFFERED_AD;
            case WANTED_AD:
                return NAME_WANTED_AD;
            case MY_ACCOUNT:
                return NAME_MY_ACCOUNT;
            case EXTRA_LISTINGS:
                return NAME_EXTRA_LISTINGS;
            default:
                return null;
        }
    }
}
