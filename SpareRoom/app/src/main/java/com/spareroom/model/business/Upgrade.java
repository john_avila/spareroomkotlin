package com.spareroom.model.business;

import com.spareroom.controller.AppVersion;
import com.spareroom.lib.core.Pair;

import java.io.Serializable;
import java.util.LinkedList;

public class Upgrade implements Serializable {

    private static final long serialVersionUID = 9024375539388119151L;

    public static final int EARLY_BIRD = 0;
    public static final int BOLD_ADVERTISING = 1;

    private String _code;
    private String _context;
    private String _title;
    private String _price;
    private Long _priceInMicros;
    private String _currency;
    private LinkedList<Pair> _features;
    private int _upgradeType = BOLD_ADVERTISING;

    private final String upgrade_1Bold_title = "1 Bold Advert";
    private final String upgrade_1Bold = AppVersion.flavor().getUpgrade1Bold();
    private final String upgrade_earlybird_title = "Early Bird Access";
    private final String upgrade_earlybird = "More than 12,000 new adverts currently available to contact with Early Bird Access";
    private final String upgrade_moneyback_title = "Money Back Guarantee";
    private final String upgrade_moneyback = "If you are not satisfied with the service we will give you your money back! Simply contact us within 3 days of your upgrade expiring to claim (please note this money back guarantee can only be claimed once)";
    private final String upgrade_pause_title = "Pause Your Upgrade";
    private final String upgrade_pause_1 = "If you manage to find ";
    private final String upgrade_pause_2 = " in say 2 weeks, simply pause your upgrade for another time (your access can only be paused once and must be used within 1 year)";

    private void initFeatures() {
        String appId = AppVersion.appId();
        if (_code.contains(appId + ".eba")) {
            _upgradeType = EARLY_BIRD;
        }
        _features.clear();
        if (_code.equals(appId + ".eba7days")
                || _code.equals(appId + ".b7days")) {

            _features.add(new Pair(upgrade_1Bold_title, upgrade_1Bold));
            _features.add(new Pair(upgrade_earlybird_title, upgrade_earlybird));
        } else if (_code.equals(appId + ".eba14days")
                || _code.equals(appId + ".bad14days")
                || _code.equals(appId + ".b14days")) {

            _features.add(new Pair(upgrade_1Bold_title, upgrade_1Bold));
            _features.add(new Pair(upgrade_earlybird_title, upgrade_earlybird));
        } else if (_code.equals(appId + ".eba30days")
                || _code.equals(appId + ".b30days")) {

            _features.add(new Pair(upgrade_1Bold_title, upgrade_1Bold));
            _features.add(new Pair(upgrade_earlybird_title, upgrade_earlybird));
            _features.add(new Pair(upgrade_moneyback_title, upgrade_moneyback));
            _features.add(new Pair(upgrade_pause_title, upgrade_pause_1 + "someone" + upgrade_pause_2));
        } else if (_code.equals(appId + ".eba6months")
                || _code.equals(appId + ".b6months")) {

            _features.add(new Pair(upgrade_1Bold_title, upgrade_1Bold));
            _features.add(new Pair(upgrade_earlybird_title, upgrade_earlybird));
        } else if (_code.equals(appId + ".eba1year")
                || _code.equals(appId + ".b1year")) {

            _features.add(new Pair(upgrade_1Bold_title, upgrade_1Bold));
            _features.add(new Pair(upgrade_earlybird_title, upgrade_earlybird));
        }
    }

    public Upgrade() {
        _features = new LinkedList<>();
    }

    public String get_code() {
        return _code;
    }

    public void set_code(String _code) {
        this._code = _code;

        //if (_context != null)
        initFeatures();

    }

    public String get_title() {
        return _title;
    }

    public void set_title(String _title) {
        this._title = _title;
    }

    public String get_price() {
        return _price;
    }

    public void set_price(String _price) {
        this._price = _price;
    }

    public LinkedList<Pair> get_features() {
        return _features;
    }

    public void set_features(LinkedList<Pair> _features) {
        this._features = _features;
    }

    public String get_context() {
        return _context;
    }

    public void set_context(String _context) {
        this._context = _context;
        // if (_code != null)
        initFeatures();
    }

    public String get_currency() {
        return _currency;
    }

    public void set_currency(String _currency) {
        this._currency = _currency;
    }

    public int get_upgradeType() {
        return _upgradeType;
    }

    public Long get_priceInMicros() {
        return _priceInMicros;
    }

    public void set_priceInMicros(Long _priceInMicros) {
        this._priceInMicros = _priceInMicros;
    }
}
