package com.spareroom.model.business;

import java.util.LinkedList;

public class UpgradeList {
    private LinkedList<Upgrade> _upgradeList;

    public LinkedList<Upgrade> get_upgradeList() {
        return _upgradeList;
    }

    public void set_upgradeList(LinkedList<Upgrade> _upgradeList) {
        this._upgradeList = _upgradeList;
    }

    public UpgradeList() {
        _upgradeList = new LinkedList<>();
    }

    public void add(Upgrade u) {
        _upgradeList.add(u);
    }

    public int getSize() {
        return _upgradeList.size();
    }

    public Upgrade get(int pos) {
        return _upgradeList.get(pos);
    }
}
