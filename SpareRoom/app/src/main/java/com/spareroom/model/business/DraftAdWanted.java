package com.spareroom.model.business;

import com.spareroom.lib.core.Pair;
import com.spareroom.lib.util.NumberUtil;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static com.spareroom.lib.util.NumberUtil.isLowerThanMinAge;

/**
 * Wanted advert. Advert that has not been placed yet or is being modified.
 */
public class DraftAdWanted extends DraftAd {

    private static final long serialVersionUID = 2143685840354077685L;

    public enum AdvertiserType {
        MALE,
        FEMALE,
        MALE_FEMALE,
        TWO_MALES,
        TWO_FEMALES
    }

    public enum Occupation {
        STUDENT,
        PROFESSIONAL,
        OTHER,
        MIXED
    }

    public enum RoomType {
        SINGLE_OR_DOUBLE,
        DOUBLE,
        DOUBLE_COUPLE,
        TWO_ROOMS,
        TWIN_OR_TWO_ROOMS,
        SINGLE
    }

    public enum DaysOfWeek {
        SEVEN_A_DAY,
        MONDAY_TO_FRIDAY,
        WEEKENDS_ONLY
    }

    private String _glIdSelected = ""; // gl area for statistics

    // page 2
    private AdvertiserType _advertiserType;

    // page 3 - picturePath

    // page 4
    // private Pair<Integer, Integer> _ageRange;
    private Boolean _isCouple = null;
    private int _age = 0;

    private int _ageMin = 0;
    private int _ageMax = 0;

    // page 5
    private Occupation _occupation;

    //page 6
    private Boolean _hasPets;

    // page 7
    private Boolean _isSmoker = null;

    // page 8
    private PreferenceList _preferenceList = new PreferenceList(PlaceAdWantedPreferenceEnum.class.getEnumConstants());

    //page 9
    private Pair<Integer, Integer> _rangeDesiredAge = null;

    // page 10
    private RoomType _roomType;

    //page 11
    private Boolean _isInterestedInBuddyingUp;

    // page 12
    private PlaceAdFullAreaSet _areas;

    // page 13
    private Price _budget;

    //page 14
    private Calendar _availableFrom = new GregorianCalendar();
    private int _minMonths = 3;
    private int _maxMonths = 12;

    // page 15
    private DaysOfWeek _daysOfWeek;

    private String _phoneNumber = "";

    // page 16

    // page 17 - title, description

    // Amenities
    private PreferenceList _amenitiesList =
            new PreferenceList(EditAdWantedAmenityEnum.class.getEnumConstants(), false);

    // Dss
    private Boolean _dss;

    // Not in the form
    private Boolean _displayPhone;

    public AdvertiserType get_advertiserType() {
        return _advertiserType;
    }

    public void set_advertiserType(AdvertiserType _advertiserType) {
        this._advertiserType = _advertiserType;
    }

    public int get_age() {
        return _age != 0 && isLowerThanMinAge(_age) ? NumberUtil.MIN_AGE : _age;
    }

    public void set_age(int _age) {
        this._age = _age;
    }

    public Occupation get_occupation() {
        return _occupation;
    }

    public void set_occupation(Occupation _occupation) {
        this._occupation = _occupation;
    }

    public Boolean is_hasPets() {
        return _hasPets;
    }

    public void set_hasPets(Boolean _hasPets) {
        this._hasPets = _hasPets;
    }

    public Boolean is_isSmoker() {
        return _isSmoker;
    }

    public void set_isSmoker(Boolean _isSmoker) {
        this._isSmoker = _isSmoker;
    }

    public PreferenceList get_preferenceList() {
        return _preferenceList;
    }

    public void set_preferenceList(PreferenceList _preferenceList) {
        this._preferenceList = _preferenceList;
    }

    public Pair<Integer, Integer> getRangeDesiredAge() {
        if (_rangeDesiredAge != null)
            sanitiseAgeRange(_rangeDesiredAge);

        return _rangeDesiredAge;
    }

    public void setRangeDesiredAge(Pair<Integer, Integer> _rangeDesiredAge) {
        this._rangeDesiredAge = _rangeDesiredAge;
    }

    public RoomType getRoomType() {
        return _roomType;
    }

    public void setRoomType(RoomType _roomType) {
        this._roomType = _roomType;
    }

    public Boolean is_isInterestedInBuddyingUp() {
        return _isInterestedInBuddyingUp;
    }

    public void set_isInterestedInBuddyingUp(Boolean _isInterestedInBuddyingUp) {
        this._isInterestedInBuddyingUp = _isInterestedInBuddyingUp;
    }

    public Price get_budget() {
        return _budget;
    }

    public void set_budget(Price _budget) {
        this._budget = _budget;
    }

    public Calendar get_availableFrom() {
        return _availableFrom;
    }

    public void set_availableFrom(Calendar _availableFrom) {
        this._availableFrom = _availableFrom;
    }

    public int get_minMonths() {
        return _minMonths;
    }

    public void set_minMonths(int _minMonths) {
        this._minMonths = _minMonths;
    }

    public int get_maxMonths() {
        return _maxMonths;
    }

    public void set_maxMonths(int _maxMonths) {
        this._maxMonths = _maxMonths;
    }

    public DaysOfWeek getDaysOfWeek() {
        return _daysOfWeek;
    }

    public void setDaysOfWeek(DaysOfWeek _daysOfWeek) {
        this._daysOfWeek = _daysOfWeek;
    }

    public String getPhoneNumber() {
        return _phoneNumber;
    }

    public void setPhoneNumber(String _phoneNumber) {
        this._phoneNumber = _phoneNumber;
    }

    public Boolean isCouple() {
        return _isCouple;
    }

    public void setIsCouple(boolean _isCouple) {
        this._isCouple = _isCouple;
    }

    public PlaceAdFullAreaSet get_areas() {
        return _areas;
    }

    public void set_areas(PlaceAdFullAreaSet _areas) {
        this._areas = _areas;
    }

    public int get_ageMin() {
        return _ageMin != 0 && isLowerThanMinAge(_ageMin) ? NumberUtil.MIN_AGE : _ageMin;
    }

    public void set_ageMin(int _ageMin) {
        this._ageMin = _ageMin;
    }

    public int get_ageMax() {
        return _ageMax != 0 && isLowerThanMinAge(_ageMax) ? NumberUtil.MIN_AGE : _ageMax;
    }

    public void set_ageMax(int _ageMax) {
        this._ageMax = _ageMax;
    }

    public Boolean getDisplayPhone() {
        return _displayPhone;
    }

    public void setDisplayPhone(Boolean _displayPhone) {
        this._displayPhone = _displayPhone;
    }

    public PreferenceList getAmenitiesList() {
        return _amenitiesList;
    }

    public void setAmenitiesList(PreferenceList amenitiesList) {
        _amenitiesList = amenitiesList;
    }

    public Boolean isDss() {
        return _dss;
    }

    public void setDss(Boolean dss) {
        _dss = dss;
    }

    public String getGlIdSelected() {
        return _glIdSelected;
    }

    public void setGlIdSelected(String glIdSelected) {
        this._glIdSelected = glIdSelected;
    }
}
