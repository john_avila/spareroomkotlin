package com.spareroom.model.business;

import java.util.LinkedList;

public class SavedSearch {
    private String _searchId;
    private String _name;
    private String _location;
    private SearchType _searchType;
    private boolean _dailyEmails;
    private boolean _instantEmails;
    private String _milesFromMax;

    /**
     * params on the API
     */
    private LinkedList<String> _paramDesc;

    private Parameters _parameters;

    public String get_searchId() {
        return _searchId;
    }

    public void set_searchId(String _searchId) {
        this._searchId = _searchId;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public LinkedList<String> get_paramDesc() {
        return _paramDesc;
    }

    public void set_paramDesc(LinkedList<String> _paramDesc) {
        this._paramDesc = _paramDesc;
    }

    public Parameters get_parameters() {
        return _parameters;
    }

    public void set_parameters(Parameters _parameters) {
        this._parameters = _parameters;
    }

    public String get_location() {
        return _location;
    }

    public void set_location(String _location) {
        this._location = _location;
    }

    public SearchType get_searchType() {
        return _searchType;
    }

    public void set_searchType(SearchType _searchType) {
        this._searchType = _searchType;
    }

    public boolean is_dailyEmails() {
        return _dailyEmails;
    }

    public void set_dailyEmails(boolean _dailyEmails) {
        this._dailyEmails = _dailyEmails;
    }

    public boolean is_instantEmails() {
        return _instantEmails;
    }

    public void set_instantEmails(boolean _instantEmails) {
        this._instantEmails = _instantEmails;
    }

    public String getMilesFromMax() {
        return _milesFromMax;
    }

    public void setMilesFromMax(String milesFromMax) {
        _milesFromMax = milesFromMax;
    }

}
