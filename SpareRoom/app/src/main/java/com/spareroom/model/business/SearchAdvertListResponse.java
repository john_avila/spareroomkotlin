package com.spareroom.model.business;

public class SearchAdvertListResponse<T> extends Response<T> {
    private ResponseTypes responseType;

    public enum ResponseTypes {
        /**
         * When no matches are found and there are not similar searches
         */
        NO_MATCHES("error"),
        /**
         * When the search for a place that doesn't match (like "Mamchestre")
         */
        SUGGESTION_LIST("action"),
        /**
         * When there are 0 results in the search
         */
        WARNING("warning"),
        /**
         * When the response is ok
         */
        SUCCESS("success");

        private String value;

        ResponseTypes(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public void setResponseType(ResponseTypes responseType) {
        this.responseType = responseType;
    }

    public ResponseTypes getResponseType() {
        return responseType;
    }

}
