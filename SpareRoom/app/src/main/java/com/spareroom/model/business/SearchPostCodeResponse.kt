package com.spareroom.model.business

sealed class SearchPostCodeResponse

class SuccessfulPostCodeSearch(val postCode: String) : SearchPostCodeResponse()

class FailedPostCodeSearch(val throwable: Throwable) : SearchPostCodeResponse()
