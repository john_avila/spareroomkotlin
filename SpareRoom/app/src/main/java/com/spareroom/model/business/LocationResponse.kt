package com.spareroom.model.business

import android.location.Location

sealed class LocationResponse

class SuccessfulLocation(val location: Location) : LocationResponse()

class FailedLocation : LocationResponse()
