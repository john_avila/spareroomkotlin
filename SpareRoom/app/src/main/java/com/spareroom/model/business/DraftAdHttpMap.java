package com.spareroom.model.business;

import java.util.Set;

/**
 * List of all keywords used for the communication with the API regarding placing an advert
 */
public class DraftAdHttpMap extends Parameters {

    //region CONSTANTS FIELDS

    //region OFFERED AND WANTED

    public static final String KEY_FIRST_NAME = "first_name";
    public static final String KEY_LAST_NAME = "last_name";
    public static final String KEY_EMAIL = "email";

    //endregion OFFERED AND WANTED

    //region OFFERED KEYs

    public static final String KEY_OFFERED_COMPANY_NAME = "company_name";
    public static final String KEY_OFFERED_ADVERTISER_TYPE = "advertiser_role";
    public static final String KEY_OFFERED_POST_CODE = "full_postcode";
    public static final String KEY_OFFERED_STREET_NAME = "street_name";
    public static final String KEY_OFFERED_PROPERTY_TYPE = "property_type";
    public static final String KEY_OFFERED_NEIGHBOURHOOD_ID = "neighbourhood_id";
    public static final String KEY_OFFERED_ROOMS_IN_PROPERTY = "rooms_in_property";
    public static final String KEY_OFFERED_ROOMS_COUNT = "rooms_for_rent";
    public static final String KEY_OFFERED_MON_FRI = "days_of_wk_available";
    public static final String KEY_OFFERED_BILLS_INCLUDED = "bills_inc";
    public static final String KEY_OFFERED_LIVING_ROOM_AVAILABLE = "living_room";
    public static final String KEY_OFFERED_BROADBAND_AVAILABLE = "broadband";
    public static final String KEY_OFFERED_DATE_AVAILABLE = "date_available";
    public static final String KEY_OFFERED_EXISTING_GENDER = "gender";
    public static final String KEY_OFFERED_PETS = "pets";
    public static final String KEY_OFFERED_SMOKING = "smoking_current";
    public static final String KEY_OFFERED_MINIMUM_AGE = "min_age";
    public static final String KEY_OFFERED_MAXIMUM_AGE = "max_age";
    public static final String KEY_OFFERED_REQUIRED_MINIMUM_AGE = "min_age_req";
    public static final String KEY_OFFERED_REQUIRED_MAXIMUM_AGE = "max_age_req";
    public static final String KEY_OFFERED_GENDER_REQUIRED = "gender_req";
    public static final String KEY_OFFERED_COUPLES_ALLOWED = "couples";
    public static final String KEY_OFFERED_SMOKING_ALLOWED = "smoking";
    public static final String KEY_OFFERED_SHARE_TYPE_REQUIRED = "share_type_req";
    public static final String KEY_OFFERED_PETS_ALLOWED = "pets_req";
    public static final String KEY_OFFERED_DSS_WELCOME = "dss";
    public static final String KEY_OFFERED_PARKING_OFFERED = "off_street_parking";
    public static final String KEY_OFFERED_BALCONY_OFFERED = "balcony";
    public static final String KEY_OFFERED_GARDEN_OFFERED = "garden";
    public static final String KEY_OFFERED_DISABLED_ACCESS_OFFERED = "disabled_access";
    public static final String KEY_OFFERED_GARAGE_OFFERED = "garage";
    public static final String KEY_OFFERED_ADVERT_TITLE = "ad_title";
    public static final String KEY_OFFERED_ADVERT_DESCRIPTION = "ad_text";
    public static final String KEY_OFFERED_PHONE_NUMBER = "tel";
    public static final String KEY_OFFERED_DISPLAY_PHONE_NUMBER = "display_tel";
    public static final String KEY_OFFERED_INTERESTS_OFFERED = "interests";
    public static final String KEY_OFFERED_ROOM_PRICE = "roomprice";
    public static final String KEY_OFFERED_ROOM_PER = "roomper";
    public static final String KEY_OFFERED_ROOM_SIZE = "roomtype";
    public static final String KEY_OFFERED_ROOM_STATUS = "roomstatus";
    public static final String KEY_OFFERED_ROOM_DEPOSIT = "roomsecurity_deposit";
    public static final String KEY_OFFERED_DEPOSIT = "security_deposit";
    public static final String KEY_OFFERED_FEES_APPLY = "fees_apply";
    public static final String KEY_OFFERED_WHOLE_PROPERTY = "scp";

//    public static final String keyAtLocation = "at_location";
//    public static final String keyRooms = "rooms";
//    public static final String keyRoomCount = "rooms_for_rent";
//    public static final String keyDayAvailable = "day_avail";
//    public static final String keyMonthAvailable = "mon_avail";
//    public static final String keyYearAvailable = "year_avail";
//    public static final String keyOccupants = "occupants";
//    public static final String keyExistingMales = "existing_males_count";
//    public static final String keyExistingFemales = "existing_females_count";
//    public static final String keyRequiredAge = "age_req";
//    public static final String keyMaleGenderRequired = "gender_req_male";
//    public static final String keyFemaleGenderRequired = "gender_req_female";
//    public static final String keyShareTypeStudent = "share_type_req_student";
//    public static final String keyShareTypeProfessional = "share_type_req_professional";
//    public static final String keyExpectedMinPrice = "expected_min_price_pw";
//    public static final String keyExpectedMaxPrice = "expected_max_price_pw";
//    public static final String keyAllowPhoneNumber = "tel_allowed";
//    public static final String keyWholeProperty = "scp";
//    public static final String keyTheAdvert = "theadvert";
//    public static final String keyTheHousehold = "thehousehold";
//    public static final String keyTheProperty = "theproperty";
//    public static final String keyTheRooms = "therooms";

    //endregion OFFERED KEYs

    //region OFFERED VALUES

    public static final String VALUE_OFFERED_NONE = "none";
    public static final String VALUE_OFFERED_M = "M";

    public static final String VALUE_OFFERED_LIL = "live in landlord";
    public static final String VALUE_OFFERED_LOL = "live out landlord";

    public static final String VALUE_OFFERED_ADVERTISER_CURRENT_FLATMATE = "current flatmate";
    public static final String VALUE_OFFERED_ADVERTISER_AGENT = "agent";
    public static final String VALUE_OFFERED_ADVERTISER_FORMER_FLATMATE = "former flatmate";

    public static final String VALUE_OFFERED_PROPERTY_HOUSE = "House";
    public static final String VALUE_OFFERED_PROPERTY_FLAT = "Flat";
    public static final String VALUE_OFFERED_PROPERTY_OTHER = "Property";

    public static final String VALUE_OFFERED_DAYS_OF_WEEK = "Mon to Fri only";

    public static final String VALUE_OFFERED_ROOM_SIZE_SINGLE = "single";
    public static final String VALUE_OFFERED_ROOM_SIZE_DOUBLE = "double";

    public static final String VALUE_OFFERED_STATUS_AVAILABLE = "available";
    public static final String VALUE_OFFERED_STATUS_TAKEN = "taken";
    public static final String VALUE_OFFERED_STATUS_OCCUPIED = "occupied";

//	public static final String valuePerMonth = "pcm";
//	public static final String valuePerWeek = "pw";

    //endregion OFFERED VALUES

    //region WANTED KEYs

    public static final String KEY_WANTED_WE_ARE = "NumberAndSexOfSeekers";
    public static final String KEY_WANTED_COUPLE = "couples";
    public static final String KEY_WANTED_MINIMUM_AGE = "min_age";
    public static final String KEY_WANTED_MAXIMUM_AGE = "max_age";
    public static final String KEY_WANTED_ROOM_TYPE = "RoomReq";
    public static final String KEY_WANTED_OCCUPATION = "share_type";
    public static final String KEY_WANTED_PETS = "pets";
    public static final String KEY_WANTED_BUDDY_UP = "interested_meeting_other_seekers";
    public static final String KEY_WANTED_SMOKING = "smoking_current";
    public static final String KEY_WANTED_COMBINED_BUDGET = "combined_budget";
    public static final String KEY_WANTED_PER = "per";
    public static final String KEY_WANTED_DATE_AVAILABLE = "date_available";
    public static final String KEY_WANTED_ACCOMMODATION_MIN = "min_term";
    public static final String KEY_WANTED_ACCOMMODATION_MAX = "max_term";
    public static final String KEY_WANTED_DAYS_OF_WEEK = "days_of_wk_available";
    public static final String KEY_WANTED_ADVERT_TITLE = "ad_title";
    public static final String KEY_WANTED_ADVERT_DESCRIPTION = "ad_text";
    public static final String KEY_WANTED_PHONE_NUMBER = "tel";
    public static final String KEY_WANTED_ALLOW_PHONE_NUMBER = "display_tel";
    public static final String KEY_WANTED_GENDER_REQUIRED = "gender_req";
    public static final String KEY_WANTED_SMOKING_ALLOWED = "smoking";
    public static final String KEY_WANTED_SHARE_TYPE_REQUIRED = "share_type_req";
    public static final String KEY_WANTED_PETS_ALLOWED = "pets_req";
    public static final String KEY_WANTED_DSS = "dss";
    public static final String KEY_WANTED_REQUIRED_MINIMUM_AGE = "min_age_req";
    public static final String KEY_WANTED_REQUIRED_MAXIMUM_AGE = "max_age_req";
    public static final String KEY_WANTED_AREAS_WATCH = "watch";
    public static final String KEY_GL_ID_SELECTED = "gl_id_selected";
    public static final String KEY_WANTED_SELECTED_AREAS = "selected_areas";
    public static final String KEY_WANTED_INTERESTS = "interests";
    public static final String KEY_WANTED_FURNISHED = "roomfurnishings";
    public static final String KEY_WANTED_WASHING_MACHINE = "washing_machine";
    public static final String KEY_WANTED_BROADBAND = "broadband";
    public static final String KEY_WANTED_EN_SUITE = "ensuite";
    public static final String KEY_WANTED_DISABLED_ACCESS = "disabled_access";
    public static final String KEY_WANTED_GARDEN = "garden";
    public static final String KEY_WANTED_BALCONY = "balcony";
    public static final String KEY_WANTED_PARKING = "off_street_parking";
    public static final String KEY_WANTED_GARAGE = "garage";
    public static final String KEY_WANTED_SHARED_LIVING_ROOM_AVAILABLE = "living_room";

//    public static final String keyMaleGenderRequired = "gender_req_male";
//    public static final String keyFemaleGenderRequired = "gender_req_female";
//    public static final String keyShareTypeStudent = "share_type_req_student";
//    public static final String keyShareTypeProfessional = "share_type_req_professional";
//    public static final String keyRequiredAge = "age_req";
//    public static final String keyAreas = "areas";

    //endregion WANTED KEYs

    //region WANTED VALUES

    public static final String VALUE_WANTED_WE_ARE_ONE_MALE = "M";
    public static final String VALUE_WANTED_WE_ARE_ONE_FEMALE = "F";
    public static final String VALUE_WANTED_WE_ARE_ONE_MALE_ONE_FEMALE = "FM";
    public static final String VALUE_WANTED_WE_ARE_TWO_MALES = "MM";
    public static final String VALUE_WANTED_WE_ARE_TWO_FEMALES = "FF";

    public static final String VALUE_WANTED_ROOM_TYPE_SINGLE_OR_DOUBLE = "a single or double room";
    public static final String VALUE_WANTED_ROOM_TYPE_DOUBLE = "a double room";
    public static final String VALUE_WANTED_ROOM_TYPE_DOUBLE_COUPLE = "a double room (we are partners)";
    public static final String VALUE_WANTED_ROOM_TYPE_2_ROOMS = "2 rooms";
    public static final String VALUE_WANTED_ROOM_TYPE_TWIN_OR_2_ROOMS = "a twin room or 2 rooms";

    public static final String VALUE_WANTED_FURNISHED_YES = "furnished";

    public static final String VALUE_WANTED_DAYS_OF_WEEK_7_DAYS = "7 days a week";
    public static final String VALUE_WANTED_DAYS_OF_WEEK_MON_FRI = "Mon to Fri only";
    public static final String VALUE_WANTED_DAYS_OF_WEEK_WEEKENDS = "Weekends only";

//    public static final String valuePetsYes = "Y";
//    public static final String valuePetsNo = "N";
//
//    public static final String valueBuddyUpYes = "Y";
//    public static final String valueBuddyUpNo = "N";
//
//    public static final String valueRequiredAgeYes = "Y";
//    public static final String valueRequiredAgeNo = "N";
//
//    public static final String valueCoupleYes = "Y";
//    public static final String valueCoupleNo = "N";
//
//    public static final String valueAllowPhoneNumberYes = "Y";
//    public static final String valueAllowPhoneNumberNo = "N";
//
//    public static final String valuePerMonth = "pcm";
//    public static final String valuePerWeek = "pw";
//
//    public static final String valuePetsAllowedYes = "Y";
//    public static final String valuePetsAllowedNo = "N";
//
//    public static final String valueGenderRequiredMale = "M";
//    public static final String valueGenderRequiredFemale = "F";
//    public static final String valueGenderRequiredNone = "N";
//
//    public static final String valueShareTypeProfessional = "P";
//    public static final String valueShareTypeStudent = "S";
//    public static final String valueShareTypeNone = "M";
//
//
//    public static final String valueTheAdvert = "theadvert";
//    public static final String valueTheHousehold = "thehousehold";
//    public static final String valueTheProperty = "theproperty";
//    public static final String valueTheRooms = "therooms";

    //endregion WANTED VALUES

    //region STANDARD VALUES

    public static final String VALUE_YES_SHORT = "Y";
    public static final String VALUE_NO_SHORT = "N";

    public static final String VALUE_YES_FULL_TEXT = "Yes";
    public static final String VALUE_SOME_FULL_TEXT = "Some";
    public static final String VALUE_NO_FULL_TEXT = "No";

    public static final String VALUE_SHARED = "shared";

    public static final String VALUE_STUDENT = "S";
    public static final String VALUE_PROFESSIONAL = "P";
    public static final String VALUE_OCCUPATION_OTHER = "O";
    public static final String VALUE_OCCUPATION_MIXED = "M";

    public static final String VALUE_MALE = "M";
    public static final String VALUE_FEMALE = "F";

    public static final String VALUE_N = "N";

    //endregion STANDARD VALUES

    //endregion CONSTANTS FIELDS

    //region PRIVATE FIELDS

    // private TreeMap<String, String> _map = new TreeMap<String, String>();
    private String _extra = null;

    //endregion PRIVATE FIELDS

    //region PUBLIC METHODS

    public void put(String key, String value) {
        _parameters.put(key, value);
    }

    public void setExtraHttpParams(String extra) {
        _extra = extra;
    }

    //endregion PUBLIC METHODS

    //region OVERRIDDEN METHODS

    @Override
    public String toString() {
        StringBuilder httpString = new StringBuilder();
        Set<String> sKey = _parameters.keySet();
        for (String k : sKey) {
            httpString.append(k);
            httpString.append("=");
            httpString.append(_parameters.get(k));
            httpString.append("&");
        }

        if (_extra != null && !_extra.isEmpty())
            httpString.append(_extra);
        else
            httpString.deleteCharAt(httpString.length() - 1); // remove last & written by the previous 'for'

        return httpString.toString();
    }

    //endregion OVERRIDDEN METHODS

}
