package com.spareroom.model.business.advertiser

data class WantedAdvertiser(
        override val avatarUrl: String,
        override val fullName: String,
        val profession: String,
        val couples: Boolean,
        val numberOfSeekers: Int
) : Advertiser