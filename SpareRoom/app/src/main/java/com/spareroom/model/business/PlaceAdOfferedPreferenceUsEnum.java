package com.spareroom.model.business;

public enum PlaceAdOfferedPreferenceUsEnum {
    GUYS,
    GIRLS,
    COUPLES,
    SMOKERS,
    STUDENTS,
    PROFESSIONALS,
    PETS;

    public String toString() {
        if (ordinal() == GUYS.ordinal())
            return "Guys";
        else if (ordinal() == GIRLS.ordinal())
            return "Girls";
        else if (ordinal() == COUPLES.ordinal())
            return "Couples";
        else if (ordinal() == SMOKERS.ordinal())
            return "Smokers";
        else if (ordinal() == STUDENTS.ordinal())
            return "Students";
        else if (ordinal() == PROFESSIONALS.ordinal())
            return "Professionals";
        else if (ordinal() == PETS.ordinal())
            return "People with pets";
        return null;
    }
}
