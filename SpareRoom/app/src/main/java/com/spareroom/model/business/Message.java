package com.spareroom.model.business;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.spareroom.ui.util.StringUtils;

import java.util.Arrays;

public class Message {

    public static final String OFFERED = "offered";

    private static final String MESSAGE_READ = "read";
    private static final String WANTED = "wanted";
    private static final String POSSESSION_MINE = "mine";

    @Expose
    @SerializedName("message_id")
    private String id;

    @Expose
    @SerializedName("message_plaintext")
    private String message;

    @Expose
    @SerializedName("date_sent_eng")
    private String dateSent;

    @Expose
    @SerializedName("direction")
    private String direction;

    @Expose
    @SerializedName("response_type")
    private String responseType;

    @Expose
    @SerializedName("ad_type")
    private String adType;

    @Expose
    @SerializedName("advert_id")
    private String advertId;

    @Expose
    @SerializedName("user_id_from")
    private String userId;

    @Expose
    @SerializedName("date_of_contact")
    private String dateOfContact;

    @Expose
    @SerializedName("view_status")
    private String viewStatus;

    @Expose
    @SerializedName("advert_possession")
    private String advertPossesion;

    @Expose
    @SerializedName("user_from_first_name")
    private String firstName;

    @Expose
    @SerializedName("user_from_last_name")
    private String lastName;

    @Expose
    @SerializedName("advert_placed_by")
    private String advertPlacedBy;

    @Expose
    @SerializedName("advertiser_type")
    private String regardingAdvertAdvertiserType;

    public String getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String direction() {
        return direction;
    }

    public String getAdvertId() {
        return advertId;
    }

    public String getUserId() {
        return userId;
    }

    public String getDateOfContact() {
        return dateOfContact;
    }

    public boolean isRead() {
        return MESSAGE_READ.equalsIgnoreCase(viewStatus);
    }

    public String getDeclineTemplate() {
        if (isRegardingMyAd())
            return OFFERED.equals(getAdType()) ? OFFERED : WANTED;

        return OFFERED.equals(getAdType()) ? WANTED : OFFERED;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Message))
            return false;

        Message messageToCompare = (Message) obj;

        return StringUtils.equals(getId(), messageToCompare.getId())
                && StringUtils.equals(getMessage(), messageToCompare.getMessage())
                && StringUtils.equals(getDateSent(), messageToCompare.getDateSent())
                && StringUtils.equals(getAdvertId(), messageToCompare.getAdvertId())
                && StringUtils.equals(getUserId(), messageToCompare.getUserId())
                && StringUtils.equals(getDateOfContact(), messageToCompare.getDateOfContact());
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[]{getId(), getMessage(), getDateSent(), getAdvertId(), getUserId(), getDateOfContact()});
    }

    String getAdType() {
        return adType;
    }

    String getResponseType() {
        return responseType;
    }

    String getFirstName() {
        return firstName;
    }

    String getLastName() {
        return lastName;
    }

    String getRegardingAdvertAdvertiserType() {
        return regardingAdvertAdvertiserType;
    }

    String getAdvertPlacedBy() {
        return advertPlacedBy;
    }

    private boolean isRegardingMyAd() {
        return POSSESSION_MINE.equalsIgnoreCase(advertPossesion);
    }

    private String getDateSent() {
        return dateSent;
    }

}
