package com.spareroom.model.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PicturesList implements Serializable {

    private final List<Picture> pictures;

    public PicturesList(List<Picture> pictures) {
        this.pictures = new ArrayList<>();
        this.pictures.addAll(pictures);
    }

    public List<Picture> getPictures() {
        return pictures;
    }
}
