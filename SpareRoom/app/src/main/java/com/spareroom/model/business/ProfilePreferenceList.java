package com.spareroom.model.business;

import java.util.LinkedList;

public class ProfilePreferenceList {
    private LinkedList<ProfilePreference> _lProfilePreference;

    public ProfilePreferenceList() {
        _lProfilePreference = new LinkedList<>();
    }

    public void add(ProfilePreference pp) {
        _lProfilePreference.add(pp);

    }

    public LinkedList<ProfilePreference> get_lProfilePreference() {
        return _lProfilePreference;
    }

    public void set_lProfilePreference(
            LinkedList<ProfilePreference> _lProfilePreference) {
        this._lProfilePreference = _lProfilePreference;
    }

}
