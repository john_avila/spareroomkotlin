package com.spareroom.model.business

import androidx.recyclerview.widget.DiffUtil

sealed class SearchResponse

class NewListSearch(val diff: DiffUtil.DiffResult, val searchItemList: List<SearchListItem>) : SearchResponse()

class FailedNewListSearch(val throwable: Throwable) : SearchResponse()
