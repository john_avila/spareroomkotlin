package com.spareroom.model.business

import android.os.Parcel
import java.io.Serializable

data class Coordinates(
    var longitude: Double = Constants.COORDINATES_DEFAULT_VALUE,
    var latitude: Double = Constants.COORDINATES_DEFAULT_VALUE,
    var longitudeDelta: Double = Constants.COORDINATES_DEFAULT_VALUE,
    var latitudeDelta: Double = Constants.COORDINATES_DEFAULT_VALUE
) : Serializable {

    constructor(parcel: Parcel) : this() {
        longitude = parcel.readDouble()
        latitude = parcel.readDouble()
        longitudeDelta = parcel.readDouble()
        latitudeDelta = parcel.readDouble()
    }

    object Constants {
        const val COORDINATES_DEFAULT_VALUE = 0.0
    }

}
