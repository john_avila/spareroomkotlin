package com.spareroom.model.business;

import android.util.Pair;

import java.util.Vector;

public class AdDetailsSheet {
    private Vector<Pair<String, Object>> _entries;

    public AdDetailsSheet() {
        _entries = new Vector<>();
    }

    public void add(Pair<String, Object> e) {
        _entries.add(e);
    }

    public Vector<Pair<String, Object>> get_entries() {
        return _entries;
    }
}
