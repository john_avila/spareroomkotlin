package com.spareroom.model.business;

public class UpgradeOption {
    private String _callToAction;
    private String _notice;
    private String[][] _benefits;

    public UpgradeOption() {

    }

    public String get_callToAction() {
        return _callToAction;
    }

    public void set_callToAction(String _callToAction) {
        this._callToAction = _callToAction;
    }

    public String get_notice() {
        return _notice;
    }

    public void set_notice(String _notice) {
        this._notice = _notice;
    }

    public String[][] get_benefits() {
        return _benefits;
    }

    public void set_benefits(String[][] _benefits) {
        this._benefits = _benefits;
    }

}
