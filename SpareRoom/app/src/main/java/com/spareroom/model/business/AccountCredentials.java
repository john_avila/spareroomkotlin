package com.spareroom.model.business;

/**
 * Credentials used for loggin in to an Account
 * <p>
 * Created by manuel on 22/06/2016.
 */
public class AccountCredentials {
    private String _username;
    private String _password;

    public String getUsername() {
        return _username;
    }

    public void setUsername(String username) {
        _username = username;
    }

    public String getPassword() {
        return _password;
    }

    public void setPassword(String password) {
        _password = password;
    }
}
