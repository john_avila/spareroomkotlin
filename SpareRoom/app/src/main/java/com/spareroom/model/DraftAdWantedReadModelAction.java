package com.spareroom.model;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.legacy.LegacyBusinessFactorySingleton;
import com.spareroom.integration.exception.InvalidArgumentException;
import com.spareroom.integration.exception.UnavailableDataSourceException;
import com.spareroom.integration.exception.UnexpectedResultException;
import com.spareroom.lib.core.Action;
import com.spareroom.model.business.DraftAd;
import com.spareroom.model.exception.ModelInvalidArgumentException;
import com.spareroom.model.exception.ModelOperationCannotBePerformedException;
import com.spareroom.model.exception.ModelUnexpectedInternalIssueException;

public class DraftAdWantedReadModelAction extends ModelAction {

    public DraftAdWantedReadModelAction(SpareroomContext sc) {
        super(sc);
        setAction(new Action() {
            @Override
            public Object execute(Object... params)
                    throws ModelOperationCannotBePerformedException,
                    ModelInvalidArgumentException,
                    ModelUnexpectedInternalIssueException {

                String advertId = (String) params[0];

                try {

                    Object result = LegacyBusinessFactorySingleton.getInstance().getPlaceAdWantedDAO().read(advertId);

                    if (result instanceof DraftAd) {
                        ((DraftAd) result).set_firstName(getSpareroomContext().getSession().get_user().get_firstName());
                        ((DraftAd) result).set_lastName(getSpareroomContext().getSession().get_user().get_lastName());
                    }

                    return result;

                } catch (UnavailableDataSourceException e) {
                    throw new ModelOperationCannotBePerformedException(e.getMessage(), e);
                } catch (InvalidArgumentException e) {
                    throw new ModelInvalidArgumentException(e.getMessage(), e);
                } catch (UnexpectedResultException e) {
                    throw new ModelUnexpectedInternalIssueException(e.getMessage(), e);
                }

            } //end execute(Object... params)
        });

    } //end DraftAdOfferedReadModelAction(SpareroomContext sc)

} //end class DraftAdOfferedReadModelAction
