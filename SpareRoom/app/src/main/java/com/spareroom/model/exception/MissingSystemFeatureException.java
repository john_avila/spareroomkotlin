package com.spareroom.model.exception;

public class MissingSystemFeatureException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -2047188744012305934L;

    public MissingSystemFeatureException(String name) {
        super(name);
    }
}
