package com.spareroom.model.business;

public class ErrorCause {

    public static final String NEW_EXISTING_USER = "new_or_existing_user";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
}
