package com.spareroom.model.business

import com.spareroom.lib.util.IEnum

data class SearchAdvertListPropertiesWanted(
    override var where: String? = null,
    override var page: String? = SearchAdvertListProperties.DEFAULT_PAGE,
    override var maxPerPage: String? = SearchAdvertListProperties.DEFAULT_MAX_PER_PAGE,
    override var keyword: String? = null,
    override var featured: Boolean = true,
    override var sortedBy: SearchAdvertListProperties.SortedBy = SearchAdvertListProperties.SortedBy.DEFAULT,

    override var gayShare: Boolean = false,
    override var photoAdvertsOnly: Boolean = false,
    override var includeHidden: Boolean = false,
    override var availableFrom: String? = null,
    override var minRent: Int? = null,
    override var maxRent: Int? = null,
    override var minAge: Int? = null,
    override var maxAge: Int? = null,

    override var minTerm: SearchAdvertListProperties.MinTerm = SearchAdvertListProperties.MinTerm.NOT_SET,
    override var maxTerm: SearchAdvertListProperties.MaxTerm = SearchAdvertListProperties.MaxTerm.NOT_SET,
    override var daysOfWeekAvailable: SearchAdvertListProperties.DaysOfWeek = SearchAdvertListProperties.DaysOfWeek.NOT_SET,
    override var smoking: SearchAdvertListProperties.Smoking = SearchAdvertListProperties.Smoking.NOT_SET,
    override var roomType: SearchAdvertListProperties.RoomTypes = SearchAdvertListProperties.RoomTypes.NOT_SET,
    override var shareType: SearchAdvertListProperties.ShareType = SearchAdvertListProperties.ShareType.NOT_SET,
    override var genderFilter: SearchAdvertListProperties.GenderFilter = SearchAdvertListProperties.GenderFilter.NOT_SET,

    var userId: String? = null,
    var couples: Couples = Couples.NOT_SET,
    var maxOtherAreas: MaxOtherAreas = MaxOtherAreas.NOT_SET
) : SearchAdvertListProperties {

    enum class Couples(private val value: String?) : IEnum {
        NOT_SET(null),
        YES("Y"),
        NO("N");

        override fun getValue() = value
    }

    enum class MaxOtherAreas(private val value: String?) : IEnum {
        NOT_SET(null),
        ONLY_THIS("zero"),
        UP_TO_TWO("2"),
        UP_TO_THREE("3"),
        UP_TO_FOUR("4"),
        UP_TO_FIVE("5");

        override fun getValue() = value
    }

    fun duplicate() = copy()

}
