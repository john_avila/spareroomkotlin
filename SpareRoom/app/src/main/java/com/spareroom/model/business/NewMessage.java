package com.spareroom.model.business;

public class NewMessage {
    public static final String FORM_ACTION_REPLY = "reply";
    public static final String E_TEMPLATE_DECLINE = "decline-";
    public static final String CONFIRMED = "1";

    private String _messageId;
    private String _message;
    private String _formAction;
    private String _confirm;

    public String getMessageId() {
        return _messageId;
    }

    public void setMessageId(String messageId) {
        _messageId = messageId;
    }

    public String getMessage() {
        return _message;
    }

    public void setMessage(String message) {
        _message = message;
    }

    public String getConfirm() {
        return _confirm;
    }

    public void setConfirm(String confirm) {
        _confirm = confirm;
    }

    public String getFormAction() {
        return _formAction;
    }

    public void setFormAction(String formAction) {
        _formAction = formAction;
    }
}
