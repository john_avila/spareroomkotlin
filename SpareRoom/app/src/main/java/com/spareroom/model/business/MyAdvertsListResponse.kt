package com.spareroom.model.business

import androidx.recyclerview.widget.DiffUtil

sealed class MyAdvertsListResponse

class MyAdvertsListFirstPage(
    val wantedAdverts: List<AdvertItem>,
    val wantedAdvertsDiff: DiffUtil.DiffResult,
    val offeredAdverts: List<AdvertItem>,
    val offeredAdvertsDiff: DiffUtil.DiffResult
) : MyAdvertsListResponse()

class MyAdvertsListNextPage(
    val wantedAdverts: List<AdvertItem>,
    val wantedAdvertsDiff: DiffUtil.DiffResult,
    val offeredAdverts: List<AdvertItem>,
    val offeredAdvertsDiff: DiffUtil.DiffResult
) : MyAdvertsListResponse()

class MyAdvertUpdated(
    val wantedAdverts: List<AdvertItem>,
    val wantedAdvertsDiff: DiffUtil.DiffResult,
    val offeredAdverts: List<AdvertItem>,
    val offeredAdvertsDiff: DiffUtil.DiffResult
) : MyAdvertsListResponse()

class FailedToLoadMyAdvertsList(val throwable: Throwable) : MyAdvertsListResponse()