package com.spareroom.model.business.advertiser

object AdvertiserType {
    const val AGENT = "agent"
    const val LIVE_IN_LANDLORD = "live in landlord"
    const val LIVE_OUT_LANDLORD = "live out landlord"
    const val CURRENT_FLATMATE = "current flatmate"
    const val FORMER_FLATMATE = "former flatmate"
    const val CURRENT_TENANTS = "current tenants"
}