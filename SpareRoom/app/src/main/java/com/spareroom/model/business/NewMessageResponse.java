package com.spareroom.model.business;

/**
 * Response from the API to an outgoing message in a conversation
 * <p/>
 * Created by miguel.rossi on 22/02/2017.
 */
public class NewMessageResponse {
    private String _statusCode;
    private String _responseType;
    private String _messageSent;
    private String _response;

    public String getStatusCode() {
        return _statusCode;
    }

    /**
     * Status code of the API response ("success" or "response_id" values)
     *
     * @param statusCode - the code.
     */
    public void setStatusCode(String statusCode) {
        _statusCode = statusCode;
    }

    public String getResponseType() {
        return _responseType;
    }

    public void setResponseType(String responseType) {
        _responseType = responseType;
    }

    public String getMessageSent() {
        return _messageSent;
    }

    public void setMessageSent(String messageSent) {
        _messageSent = messageSent;
    }

    public String getResponse() {
        return _response;
    }

    public void setResponse(String response) {
        _response = response;
    }

    public SpareroomStatus toSpareroomStatus() {
        SpareroomStatus spareroomStatus = new SpareroomStatus();
        spareroomStatus.setCode(getStatusCode());
        spareroomStatus.setMessage(getResponse());

        return spareroomStatus;
    }
}
