package com.spareroom.model.business.advertiser

data class OfferedAdvertiser(
        override val avatarUrl: String,
        override val fullName: String,
        val companyName: String,
        val advertiserType: String,
        val profession: String,
        val couples: Boolean
) : Advertiser {

    fun agent() = advertiserType == AdvertiserType.AGENT
}