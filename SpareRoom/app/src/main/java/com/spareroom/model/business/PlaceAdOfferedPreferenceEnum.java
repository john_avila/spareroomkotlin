package com.spareroom.model.business;

public enum PlaceAdOfferedPreferenceEnum {
    GUYS,
    GIRLS,
    COUPLES,
    SMOKERS,
    STUDENTS,
    PROFESSIONALS,
    PETS,
    DSS
}
