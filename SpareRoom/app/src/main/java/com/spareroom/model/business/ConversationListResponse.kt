package com.spareroom.model.business

import androidx.recyclerview.widget.DiffUtil

sealed class ConversationListResponse

class NextPageConversationList(val diff: DiffUtil.DiffResult, val conversations: List<ConversationPreview>, val hadDuplicates: Boolean) : ConversationListResponse()

class FirstPageConversationList(val diff: DiffUtil.DiffResult, val conversations: List<ConversationPreview>) : ConversationListResponse()

class ConversationReadResponse(val diff: DiffUtil.DiffResult, val conversations: List<ConversationPreview>) : ConversationListResponse()

class ConversationDeletedResponse(val diff: DiffUtil.DiffResult, val conversations: List<ConversationPreview>) : ConversationListResponse()

class EmptyConversationListResponse(val diff: DiffUtil.DiffResult, val conversations: List<ConversationPreview>) : ConversationListResponse()

class FailedConversationListResponse(val throwable: Throwable) : ConversationListResponse()
