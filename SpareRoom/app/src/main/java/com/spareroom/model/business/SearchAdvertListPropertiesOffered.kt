package com.spareroom.model.business

import com.spareroom.lib.util.IEnum

data class SearchAdvertListPropertiesOffered(
    override var where: String? = null,
    override var page: String? = SearchAdvertListProperties.DEFAULT_PAGE,
    override var maxPerPage: String? = SearchAdvertListProperties.DEFAULT_MAX_PER_PAGE,
    override var keyword: String? = null,
    override var featured: Boolean = true,
    override var sortedBy: SearchAdvertListProperties.SortedBy = SearchAdvertListProperties.SortedBy.DEFAULT,

    override var gayShare: Boolean = false,
    override var photoAdvertsOnly: Boolean = false,
    override var includeHidden: Boolean = false,
    override var availableFrom: String? = null,
    override var minRent: Int? = null,
    override var maxRent: Int? = null,
    override var minAge: Int? = null,
    override var maxAge: Int? = null,

    override var minTerm: SearchAdvertListProperties.MinTerm = SearchAdvertListProperties.MinTerm.NOT_SET,
    override var maxTerm: SearchAdvertListProperties.MaxTerm = SearchAdvertListProperties.MaxTerm.NOT_SET,
    override var daysOfWeekAvailable: SearchAdvertListProperties.DaysOfWeek = SearchAdvertListProperties.DaysOfWeek.NOT_SET,
    override var smoking: SearchAdvertListProperties.Smoking = SearchAdvertListProperties.Smoking.NOT_SET,
    override var roomType: SearchAdvertListProperties.RoomTypes = SearchAdvertListProperties.RoomTypes.NOT_SET,
    override var shareType: SearchAdvertListProperties.ShareType = SearchAdvertListProperties.ShareType.NOT_SET,
    override var genderFilter: SearchAdvertListProperties.GenderFilter = SearchAdvertListProperties.GenderFilter.NOT_SET,

    var showRooms: Boolean = false,
    var showStudios: Boolean = false,
    var showWholeProperty: Boolean = false,
    var parking: Boolean = false,
    var disabledAccess: Boolean = false,
    var enSuite: Boolean = false,
    var benefits: Boolean = false,
    var pets: Boolean = false,
    var livingRoom: Boolean = false,
    var vegetarians: Boolean = false,
    var shortLets: Boolean = false,

    var milesFromMax: Int? = null,
    var maxCommuteTime: String? = null,

    var locationType: LocationType = LocationType.NOT_SET,
    private var billsInc: BillsInc = BillsInc.NOT_SET,
    var noOfRooms: NoOfRooms = NoOfRooms.NOT_SET,
    var minFlatmates: MinFlatmates = MinFlatmates.NOT_SET,
    private var maxFlatmates: MaxFlatmates = MaxFlatmates.NOT_SET,
    var landlord: Landlord = Landlord.NOT_SET,
    var roomsFor: RoomsFor = RoomsFor.NOT_SET,
    var postedBy: PostedBy = PostedBy.NOT_SET,

    var coordinates: Coordinates = Coordinates()
) : SearchAdvertListProperties {

    enum class LocationType(private val value: String?) : IEnum {
        NOT_SET(null),
        ZONE("zone"),
        COMMUTER("commuter"),
        TUBE("tube"),
        AREA("area"),
        POST_TOWN("posttown");

        override fun getValue() = value
    }

    enum class BillsInc(private val value: String?) : IEnum {
        NOT_SET(null),
        SOME("Some"),
        YES("Yes"),
        NO("No");

        override fun getValue() = value
    }

    enum class NoOfRooms(private val value: String?) : IEnum {
        NOT_SET(null),
        MIN_TWO("2"),
        MIN_THREE("3");

        override fun getValue() = value
    }

    enum class MinFlatmates(private val value: String?) : IEnum {
        NOT_SET(null),
        TWO("2"),
        THREE("3"),
        FOUR("4"),
        FIVE("5"),
        SIX("6");

        override fun getValue() = value
    }

    enum class MaxFlatmates(private val value: String?) : IEnum {
        NOT_SET(null),
        TWO("2"),
        THREE("3"),
        FOUR("4"),
        FIVE("5"),
        SIX("6"),
        MORE_THAN_SIX("6+");

        override fun getValue() = value
    }

    enum class Landlord(private val value: String?) : IEnum {
        NOT_SET(null),
        LIVE_IN("live_in"),
        LIVE_OUT("live_out");

        override fun getValue() = value
    }

    enum class RoomsFor(private val value: String?) : IEnum {
        NOT_SET(null),
        MALES("males"),
        FEMALES("females"),
        COUPLES("couples");

        override fun getValue() = value
    }

    enum class PostedBy(private val value: String?) : IEnum {
        NOT_SET(null),
        AGENTS("agents"),
        PRIVATE_LANDLORDS("private_landlords");

        override fun getValue() = value
    }

    fun billsInc() = billsInc == BillsInc.YES

    fun billsInc(billsInc: Boolean) {
        this.billsInc = if (billsInc) BillsInc.YES else BillsInc.NOT_SET
    }

    fun maxFlatmates() = maxFlatmates

    fun maxFlatmates(maxBeds: MaxFlatmates) {
        this.maxFlatmates = if (MaxFlatmates.MORE_THAN_SIX != maxBeds) maxBeds else MaxFlatmates.NOT_SET
    }

    fun duplicate() = copy(coordinates = coordinates.copy())

}
