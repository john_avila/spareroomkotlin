package com.spareroom.model.business

import androidx.recyclerview.widget.DiffUtil

sealed class OtherUserAdvertsResponse(adverts: List<AdvertItem>? = null, throwable: Throwable? = null) : Response<List<AdvertItem>>(adverts, throwable)

class OtherUserAdvertsResponseSuccess(
    val diffResult: DiffUtil.DiffResult,
    val adverts: List<AdvertItem>,
    val otherUserFirstName: String
) : OtherUserAdvertsResponse(adverts = adverts)

class OtherUserAdvertsResponseFailure(throwable: Throwable) : OtherUserAdvertsResponse(throwable = throwable)