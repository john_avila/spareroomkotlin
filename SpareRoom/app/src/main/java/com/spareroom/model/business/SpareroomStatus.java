package com.spareroom.model.business;

/**
 * The message given for the API as a response for a request
 */
public class SpareroomStatus {

    private String _code;
    private String _cause;
    private String _message;

    /**
     * The response code given by the API to our request
     *
     * @return the response code
     */
    public String getCode() {
        return _code;
    }

    public void setCode(String _code) {
        this._code = _code;
    }

    public String getCause() {
        return _cause;
    }

    public void setCause(String _cause) {
        this._cause = _cause;
    }

    public String getMessage() {
        return _message;
    }

    public void setMessage(String _message) {
        this._message = _message;
    }

}
