package com.spareroom.model.business;

import java.util.LinkedList;
import java.util.List;

public class BroadenSearches {
    private List<BroadenSearch> _lBroadenSearch;

    public BroadenSearches() {
        _lBroadenSearch = new LinkedList<>();
    }

    public void add(BroadenSearch bs) {
        _lBroadenSearch.add(bs);
    }

    public List<BroadenSearch> get_lBroadenSearch() {
        return _lBroadenSearch;
    }

    public void set_lBroadenSearch(List<BroadenSearch> _lBroadenSearch) {
        this._lBroadenSearch = _lBroadenSearch;
    }

    public BroadenSearch get(int position) {
        return _lBroadenSearch.get(position);
    }

    public int size() {
        return _lBroadenSearch.size();
    }

}
