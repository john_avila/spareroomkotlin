package com.spareroom.model.extra;

/**
 * Extra information required from the API to perform a log in
 * Created by manuel on 24/06/2016.
 */
public class LoginExtra extends Extra {
    private String _pageJoinedFrom;
    private String _version;

    public String getPageJoinedFrom() {
        return _pageJoinedFrom;
    }

    public void setPageJoinedFrom(String pageJoinedFrom) {
        _pageJoinedFrom = pageJoinedFrom;
    }

    public String getVersion() {
        return _version;
    }

    public void setVersion(String version) {
        _version = version;
    }
}
