package com.spareroom.model.business;

import java.util.LinkedList;
import java.util.List;

public class SuggestionList {
    private String _location;

    private List<Suggestion> _suggestion;

    public SuggestionList() {
        _suggestion = new LinkedList<>();
    }

    public List<Suggestion> get_suggestion() {
        return _suggestion;
    }

    public void set_suggestion(List<Suggestion> _suggestion) {
        this._suggestion = _suggestion;
    }

    public String get_location() {
        return _location;
    }

    public void set_location(String _location) {
        this._location = _location;
    }

}
