package com.spareroom.model.business;

public class BroadenSearch {
    private Parameters _paramsToAdd;
    private Parameters _paramsToLose;
    private String _criteriaToAdd;
    private String _criteriaToLose;
    private String _criteriaToKeep;
    private int _numMatches;
    private String _url;

    public Parameters get_paramsToAdd() {
        return _paramsToAdd;
    }

    public void set_paramsToAdd(Parameters _paramsToAdd) {
        this._paramsToAdd = _paramsToAdd;
    }

    public Parameters get_paramsToLose() {
        return _paramsToLose;
    }

    public void set_paramsToLose(Parameters _paramsToLose) {
        this._paramsToLose = _paramsToLose;
    }

    public String get_criteriaToAdd() {
        return _criteriaToAdd;
    }

    public void set_criteriaToAdd(String _criteriaToAdd) {
        this._criteriaToAdd = _criteriaToAdd;
    }

    public String get_criteriaToLose() {
        return _criteriaToLose;
    }

    public void set_criteriaToLose(String _criteriaToLose) {
        this._criteriaToLose = _criteriaToLose;
    }

    public String get_criteriaToKeep() {
        return _criteriaToKeep;
    }

    public void set_criteriaToKeep(String _criteriaToKeep) {
        this._criteriaToKeep = _criteriaToKeep;
    }

    public int get_numMatches() {
        return _numMatches;
    }

    public void set_numMatches(int _numMatches) {
        this._numMatches = _numMatches;
    }

    public String get_url() {
        return _url;
    }

    public void set_url(String _url) {
        this._url = _url;
    }

}
