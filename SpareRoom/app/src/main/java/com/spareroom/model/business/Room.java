package com.spareroom.model.business;

import java.io.Serializable;

public class Room implements Serializable {
    private static final long serialVersionUID = 6220330478460475643L;

    public static final int ROOM_SIZE_SINGLE = 1;
    public static final int ROOM_SIZE_DOUBLE = 2;

    public static final int ROOM_STATUS_AVAILABLE = 0;
    public static final int ROOM_STATUS_TAKEN = 1;
    public static final int ROOM_STATUS_OCCUPIED = 2;
    private Price _rent;
    private String deposit;
    private int _roomSize;
    private int _roomStatus = ROOM_STATUS_AVAILABLE;

    public Price get_rent() {
        return _rent;
    }

    public void set_rent(Price _rent) {
        this._rent = _rent;
    }

    public int get_roomSize() {
        return _roomSize;
    }

    public void set_roomSize(int _roomSize) {
        this._roomSize = _roomSize;
    }

    public int get_roomStatus() {
        return _roomStatus;
    }

    public void set_roomStatus(int _roomStatus) {
        this._roomStatus = _roomStatus;
    }

    public String deposit() {
        return deposit;
    }

    public void deposit(String deposit) {
        this.deposit = deposit;
    }
}
