package com.spareroom.model;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.legacy.LegacyBusinessFactorySingleton;
import com.spareroom.integration.exception.InvalidArgumentException;
import com.spareroom.integration.exception.UnavailableDataSourceException;
import com.spareroom.integration.exception.UnexpectedResultException;
import com.spareroom.lib.core.Action;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.model.exception.ModelInvalidArgumentException;
import com.spareroom.model.exception.ModelOperationCannotBePerformedException;
import com.spareroom.model.exception.ModelUnexpectedInternalIssueException;

public class DraftAdOfferedUpdateModelAction extends ModelAction {

    public DraftAdOfferedUpdateModelAction(SpareroomContext sc) {
        super(sc);
        setAction(new Action() {
            @Override
            public Object execute(Object... params)
                    throws ModelOperationCannotBePerformedException,
                    ModelInvalidArgumentException,
                    ModelUnexpectedInternalIssueException {

                try {

                    return LegacyBusinessFactorySingleton.getInstance().getPlaceAdOfferedDAO().update((DraftAdOffered) params[0]);

                } catch (UnavailableDataSourceException e) {
                    throw new ModelOperationCannotBePerformedException(e.getMessage(), e);
                } catch (InvalidArgumentException e) {
                    throw new ModelInvalidArgumentException(e.getMessage(), e);
                } catch (UnexpectedResultException e) {
                    throw new ModelUnexpectedInternalIssueException(e.getMessage(), e);
                }

            } //end execute(Object... params)
        });

    } //en DraftAdOfferedUpdateModelAction(SpareroomContext sc)

} //end class DraftAdOfferedUpdateModelAction
