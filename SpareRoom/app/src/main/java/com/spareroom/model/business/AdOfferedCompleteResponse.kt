package com.spareroom.model.business

import com.google.gson.annotations.*

data class AdOfferedCompleteResponse(@Expose @SerializedName("advert_summary") val advert: AdOffered?)