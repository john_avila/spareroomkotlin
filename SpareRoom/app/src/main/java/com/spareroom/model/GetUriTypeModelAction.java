package com.spareroom.model;

import android.net.Uri;

import com.spareroom.App;
import com.spareroom.R;
import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.exception.InvalidArgumentException;
import com.spareroom.integration.exception.UnavailableDataSourceException;
import com.spareroom.integration.exception.UnexpectedResultException;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.ui.util.FileUtils;

public class GetUriTypeModelAction extends ModelActionImpl {

    public GetUriTypeModelAction(final SpareroomContext spareroomContext) {
        super(spareroomContext);
    }

    @Override
    public Object internalExecuteAction(final SpareroomContext spareroomContext, Object uri) throws UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {
        // Checks if Uri is of image type
        if (!FileUtils.isUriImageType(Uri.parse((String) uri))) {
            SpareroomStatus spareroomStatus = new SpareroomStatus();
            spareroomStatus.setMessage(App.get().getString(R.string.errorPhoto_incorrectType));
            return spareroomStatus;
        }
        return true;
    }
}
