package com.spareroom.model.business;

import java.util.LinkedList;

public class PictureGallery {
    private LinkedList<Picture> _gallery;

    public PictureGallery() {
        _gallery = new LinkedList<>();
    }

    public Picture get(int position) {
        return _gallery.get(position);
    }

    public LinkedList<Picture> get_gallery() {
        return _gallery;
    }

    public void set_gallery(LinkedList<Picture> _gallery) {
        this._gallery = _gallery;
    }

    public int size() {
        return _gallery.size();
    }

}
