package com.spareroom.model.legacy;

import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;

/**
 * Model Facade for location services
 *
 * @author manuel
 */
public interface ILocationFacade {

    Object getPostcodeUnit(Parameters p) throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;
}
