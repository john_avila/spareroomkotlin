package com.spareroom.model.legacy;

import android.content.Context;

import com.spareroom.integration.business.legacy.ILegacyBusinessAbstractFactory;
import com.spareroom.integration.business.legacy.rest.*;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.internal.DraftAdDAO;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.model.business.*;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;

import java.io.IOException;
import java.util.List;

public class AccountFacade implements IAccountFacade {
    private final ILegacyBusinessAbstractFactory _webServiceFactory;

    /**
     * Constructor
     *
     * @param webServiceFactory Web Service Factory used for communicating with the server
     */
    public AccountFacade(ILegacyBusinessAbstractFactory webServiceFactory) {
        _webServiceFactory = webServiceFactory;
    }

    /**
     * Gets the Upgrade options available
     */
    @Override
    public UpgradeList getUpgrades(Parameters params) throws
            MissingSystemFeatureException, InconsistentStateException, ServiceUnavailableException {

        IUpgradeListDAO daoUpgradeList = _webServiceFactory.getUpgradeListDAO();
        Parameters p = new Parameters();
        p.add(params);
        p.add("show", "product_ids");
        p.add(params);

        try {
            // Contact SpareRoom to retrieve valid product IDs
            List<String> lProductIds = daoUpgradeList.read(p);

            UpgradeList ul = new UpgradeList();
            for (String productId : lProductIds) {
                Upgrade u = new Upgrade();
                u.set_code(productId);
                ul.add(u);
            }
            return ul;

        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        } catch (ClientErrorException e) {
            throw new ServiceUnavailableException();
        } catch (ServerErrorException e) {
            throw new ServiceUnavailableException();
        }
        // return null;
    }

    @Override
    public SpareroomStatus upgrade(Parameters params) throws
            MissingSystemFeatureException, InconsistentStateException, ServiceUnavailableException {

        try {
            IUpgradeDAO daoUpgrade = _webServiceFactory.getUpgradeDAO();
            return daoUpgrade.create(params);

        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        } catch (ClientErrorException e) {
            throw new ServiceUnavailableException();
        } catch (ServerErrorException e) {
            throw new ServiceUnavailableException();
        }
    }

    @Override
    public Object getEmailPreferences() throws
            MissingSystemFeatureException, InconsistentStateException, ServiceUnavailableException {

        try {
            IProfilePreferenceListDAO dao = _webServiceFactory.getProfilePreferenceListDAO();
            return dao.read();

        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        } catch (ClientErrorException e) {
            throw new ServiceUnavailableException(e.getMessage());
        } catch (ServerErrorException e) {
            throw new ServiceUnavailableException();
        }
    }

    @Override
    public Object updateName(Parameters params) throws
            MissingSystemFeatureException, InconsistentStateException, ServiceUnavailableException {

        try {
            IProfilePreferenceListDAO dao = _webServiceFactory.getProfilePreferenceListDAO();
            return dao.update(params);

        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        } catch (ClientErrorException e) {
            throw new ServiceUnavailableException();
        } catch (ServerErrorException e) {
            throw new ServiceUnavailableException();
        }
    }

    @Override
    public Object updateEmail(Parameters params) throws
            MissingSystemFeatureException, InconsistentStateException, ServiceUnavailableException {

        try {
            IProfilePreferenceListDAO dao = _webServiceFactory.getProfilePreferenceListDAO();
            return dao.update(params);

        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        } catch (ClientErrorException e) {
            throw new ServiceUnavailableException();
        } catch (ServerErrorException e) {
            throw new ServiceUnavailableException();
        }
    }

    @Override
    public Object updatePassword(Parameters params) throws
            MissingSystemFeatureException, InconsistentStateException, ServiceUnavailableException {

        try {
            IProfilePreferenceListDAO dao = _webServiceFactory.getProfilePreferenceListDAO();
            return dao.update(params);

        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        } catch (ClientErrorException e) {
            throw new ServiceUnavailableException();
        } catch (ServerErrorException e) {
            throw new ServiceUnavailableException();
        }
    }

    @Override
    public Object updateEmailPreferences(Parameters params) throws
            MissingSystemFeatureException, InconsistentStateException, ServiceUnavailableException {

        try {
            IProfilePreferenceListDAO dao = _webServiceFactory.getProfilePreferenceListDAO();
            return dao.update(params);

        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        } catch (ClientErrorException e) {
            throw new ServiceUnavailableException();
        } catch (ServerErrorException e) {
            throw new ServiceUnavailableException();
        }
    }

    @Override
    public Stats getMyOfferedAdStats(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException,
            ClientErrorException, ServerErrorException {
        IMyOfferedAdDAO dao = _webServiceFactory.getMyOfferedAdDAO();

        return dao.read(params);
    }

    @Override
    public Stats getMyWantedAdStats(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException,
            ClientErrorException, ServerErrorException {
        IMyWantedAdDAO dao = _webServiceFactory.getMyWantedAdDAO();

        return dao.read(params);
    }

    @Override
    public PictureGallery getPictures(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException,
            ClientErrorException, ServerErrorException {
        IGalleryDAO dao = _webServiceFactory.getGalleryDAO();
        return dao.read(params);
    }

    @Override
    public void setOrderPicture(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException,
            ClientErrorException, ServerErrorException {
        IPictureDAO dao = _webServiceFactory.getPictureDAO();
        dao.update(params);
    }

    @Override
    public void removePicture(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException,
            ClientErrorException, ServerErrorException {
        IPictureDAO dao = _webServiceFactory.getPictureDAO();
        dao.delete(params);
    }

    @Override
    public void saveDraftAd(Context applicationContext, DraftAd draft) throws
            IOException {
        DraftAdDAO.save(applicationContext, draft);
    }

    @Override
    public DraftAd loadDraftAd(Context applicationContext) throws IOException {
        try {
            return DraftAdDAO.load(applicationContext);
        } catch (ClassNotFoundException e) {
            throw new IOException(e.getMessage());
        }
    }

    @Override
    public VideoGallery getVideoGallery(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException,
            ClientErrorException, ServerErrorException {
        IVideoGalleryDAO dao = _webServiceFactory.getVideoGalleryDAO();
        return dao.read(params);
    }

    @Override
    public void removeVideo(Parameters params) throws
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException,
            ClientErrorException, ServerErrorException {
        IVideoDAO dao = _webServiceFactory.getVideoDAO();
        dao.delete(params);
    }

}
