package com.spareroom.model.business

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.spareroom.integration.business.apiv2.ApiParams
import com.spareroom.lib.util.paging.Page

data class ConversationListRestResponse(
    @Expose @SerializedName("success") private val successField: String? = "",
    @Expose @SerializedName("error") private val errorMessageField: String? = "",
    @Expose @SerializedName("response_id") private val responseId: String? = "",
    @Expose @SerializedName("threads") private val listField: List<ConversationPreview>?,
    @Expose @SerializedName("thread_count_total") private val totalConversationsField: Int?
) : Page<ConversationPreview> {
    val errorMessage get() = errorMessageField ?: ""
    val errorLoggedOut get() = responseId == SpareroomStatusCode.NOT_LOGGED_IN
    val list get() = listField ?: emptyList()
    val totalConversations get() = totalConversationsField ?: 0

    override val rawItems get() = list
    override val success get() = ApiParams.YES_INT == successField
}
