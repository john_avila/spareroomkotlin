package com.spareroom.model.business.advertiser

import com.spareroom.ui.util.StringUtils
import com.spareroom.ui.util.filterOnlyLetters

interface Advertiser {
    val avatarUrl: String
    val fullName: String
    val firstName get() = firstName()
    val lastName get() = lastName()

    private fun firstName(): String {
        val firstName = StringUtils.getFirstString(fullName)
        return firstName.filterOnlyLetters()
    }

    private fun lastName(): String {
        val lastName = StringUtils.getLastName(fullName)
        return if (fullName == lastName) "" else lastName.filterOnlyLetters()
    }
}