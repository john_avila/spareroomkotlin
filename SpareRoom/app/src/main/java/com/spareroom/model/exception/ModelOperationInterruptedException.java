package com.spareroom.model.exception;

/**
 * Created by miguel.rossi on 28/09/2016.
 */
public class ModelOperationInterruptedException extends Exception {
    private static final long serialVersionUID = -8737299604756640286L;

    public ModelOperationInterruptedException() {
        super();
    }

    public ModelOperationInterruptedException(String detailMessage) {
        super(detailMessage);
    }

    public ModelOperationInterruptedException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ModelOperationInterruptedException(Throwable throwable) {
        super(throwable);
    }
}
