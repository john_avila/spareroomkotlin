package com.spareroom.model.business;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class SearchAutocompleteList implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -6069350586554630451L;
    private List<SearchAutocomplete> _lSuggestion;

    public SearchAutocompleteList() {
        _lSuggestion = new LinkedList<>();
    }

    public List<SearchAutocomplete> get_lSuggestion() {
        return _lSuggestion;
    }

    public void set_lSuggestion(List<SearchAutocomplete> _lSuggestion) {
        this._lSuggestion = _lSuggestion;
    }

    public void add(SearchAutocomplete suggestion) {
        _lSuggestion.add(suggestion);
    }

}
