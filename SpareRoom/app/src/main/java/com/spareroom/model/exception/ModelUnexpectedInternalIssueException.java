package com.spareroom.model.exception;

/**
 * Created by miguel.rossi on 23/05/2016.
 */
public class ModelUnexpectedInternalIssueException extends Exception {

    private static final long serialVersionUID = -6775073065951468235L;

    public ModelUnexpectedInternalIssueException() {
        super();
    }

    public ModelUnexpectedInternalIssueException(String detailMessage) {
        super(detailMessage);
    }

    public ModelUnexpectedInternalIssueException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ModelUnexpectedInternalIssueException(Throwable throwable) {
        super(throwable);
    }

}
