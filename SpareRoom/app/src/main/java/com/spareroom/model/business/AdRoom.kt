package com.spareroom.model.business

import com.google.gson.annotations.*

data class AdRoom(
        @Expose @SerializedName("security_deposit") private val securityDepositField: String? = null,
        @Expose @SerializedName("room_status") private val statusField: String? = null,
        @Expose @SerializedName("room_furnishings") private val furnishingsField: String? = null,
        @Expose @SerializedName("room_price") private val priceField: String? = null,
        @Expose @SerializedName("ensuite") private val ensuiteField: String? = null,
        @Expose @SerializedName("room_per") private val pricePeriodField: String? = null,
        @Expose @SerializedName("room_type") private val typeField: String? = null) {

    companion object {
        // availability
        const val STATUS_AVAILABLE = "available"
        const val STATUS_TAKEN = "taken"

        // type
        const val ROOM_TYPE_SINGLE = "single"
        const val ROOM_TYPE_DOUBLE = "double"
        const val YES = "Y"
    }

    val securityDeposit get() = securityDepositField ?: ""
    val status get() = statusField ?: ""
    val price get() = priceField ?: ""
    private val ensuite get() = ensuiteField ?: ""
    val pricePeriod get() = pricePeriodField ?: ""
    val type get() = typeField ?: ""

    fun isAvailable() = STATUS_AVAILABLE.equals(status, ignoreCase = true)

    fun isSingle() = ROOM_TYPE_SINGLE.equals(type, ignoreCase = true)

    fun isDouble() = ROOM_TYPE_DOUBLE.equals(type, ignoreCase = true)

    fun hasEnsuite() = YES.equals(ensuite, ignoreCase = true)

}