package com.spareroom.model.business;

import com.spareroom.controller.AppVersion;
import com.spareroom.lib.core.Pair;

import java.util.Calendar;

/**
 * Offered advert. Advert that has not been placed yet or is being modified.
 */
public class DraftAdOffered extends DraftAd {

    private static final long serialVersionUID = -3190888194634855734L;

    //region ENUMs

    public enum PropertyType {
        FLAT,
        HOUSE,
        OTHER
    }

    public enum AdvertiserType {
        LIVE_IN_LANDLORD,
        LIVE_OUT_LANDLORD,
        CURRENT_FLATMATE,
        AGENT,
        FORMER_FLATMATE
    }

    public enum BillsIncluded {
        NO,
        YES,
        SOME
    }

    //endregion ENUMs

    //region FIELDS

    // page 2
    private PropertyType _propertyType;

    // page 3
    private AdvertiserType _advertiserType;

    // page 3a
    private String _companyName;

    // page 4
    private Boolean _isAtProperty = null;
    private String _postCode = null;
    private String _area;

    // Street Name
    private String _streetName;

    // page 5 - picturePath

    // page 6
    private RoomList _rooms;

    // page 7
    private Calendar _availableFrom;
    private boolean _isMonFriLet;
    private BillsIncluded _areBillsIncluded;

    // Shared living room
    private Boolean _sharedLivingRoom;

    // Broadband
    private Boolean _isBroadband;

    // page 8
    private int _numExistingGuys;
    private int _numExistingGirls;

    // page 9
    private Pair<Integer, Integer> _rangeExistingAges;

    // page 10
    private int _numBedroom;

    // page 11
    private Boolean _isTherePet;

    // page 12
    private Boolean _areThereSmokers;

    // page 13
    private PreferenceList _preferenceList;

    // page 14
    private Pair<Integer, Integer> _rangeDesiredAge = null;

    // page 14
    private Boolean _feesApply = null;

    // page 15w
    private String _phoneNumber;

    // page 16 - title, description

    // Amenities
    private PreferenceList _amenitiesList =
            new PreferenceList(EditAdOfferedAmenityEnum.class.getEnumConstants(), false);

    // Not in the form
    private Boolean _displayPhone;

    //Extra
    private PlaceAdPostcodeAreaList _lPostcodeAreaList;

    private boolean wholeProperty;
    private String wholePropertyDeposit;

    //endregion FIELDS

    public DraftAdOffered() {
        if (AppVersion.isUk()) {
            _preferenceList = new PreferenceList(PlaceAdOfferedPreferenceEnum.class.getEnumConstants());
        } else {
            _preferenceList = new PreferenceList(PlaceAdOfferedPreferenceUsEnum.class.getEnumConstants());
        }

    }

    public PropertyType get_propertyType() {
        return _propertyType;
    }

    public void set_propertyType(PropertyType _propertyType) {
        this._propertyType = _propertyType;
    }

    public AdvertiserType get_advertiserType() {
        return _advertiserType;
    }

    public void set_advertiserType(AdvertiserType _advertiserType) {
        this._advertiserType = _advertiserType;
    }

    public Boolean is_isAtProperty() {
        return _isAtProperty;
    }

    public void set_isAtProperty(Boolean _isAtProperty) {
        this._isAtProperty = _isAtProperty;
    }

    public String get_postCode() {
        return _postCode;
    }

    public void set_postCode(String _postCode) {
        this._postCode = _postCode;
    }

    public String get_area() {
        return _area;
    }

    public void set_area(String _area) {
        this._area = _area;
    }

    public String getStreetName() {
        return _streetName;
    }

    public void setStreetName(String streetName) {
        _streetName = streetName;
    }

    public RoomList get_rooms() {
        return _rooms;
    }

    public void set_rooms(RoomList _rooms) {
        this._rooms = _rooms;
    }

    public Calendar get_availableFrom() {
        return _availableFrom;
    }

    public void set_availableFrom(Calendar _availableFrom) {
        this._availableFrom = _availableFrom;
    }

    public boolean is_isMonFriLet() {
        return _isMonFriLet;
    }

    public void set_isMonFriLet(boolean _isMonFriLet) {
        this._isMonFriLet = _isMonFriLet;
    }

    public BillsIncluded get_areBillsIncluded() {
        return _areBillsIncluded;
    }

    public void set_areBillsIncluded(BillsIncluded _areBillsIncluded) {
        this._areBillsIncluded = _areBillsIncluded;
    }

    public Boolean isSharedLivingRoom() {
        return _sharedLivingRoom;
    }

    public void setSharedLivingRoom(Boolean sharedLivingRoom) {
        _sharedLivingRoom = sharedLivingRoom;
    }

    public Boolean isBroadband() {
        return _isBroadband;
    }

    public void setBroadband(Boolean broadband) {
        _isBroadband = broadband;
    }

    public int get_numExistingGuys() {
        return _numExistingGuys;
    }

    public void set_numExistingGuys(int _numExistingGuys) {
        this._numExistingGuys = _numExistingGuys;
    }

    public int get_numExistingGirls() {
        return _numExistingGirls;
    }

    public void set_numExistingGirls(int _numExistingGirls) {
        this._numExistingGirls = _numExistingGirls;
    }

    public Pair<Integer, Integer> get_rangeExistingAges() {
        if (_rangeExistingAges != null)
            sanitiseAgeRange(_rangeExistingAges);

        return _rangeExistingAges;
    }

    public void set_rangeExistingAges(Pair<Integer, Integer> _rangeExistingAges) {
        this._rangeExistingAges = _rangeExistingAges;
    }

    public String wholePropertyDeposit() {
        return wholePropertyDeposit;
    }

    public void wholePropertyDeposit(String amount) {
        this.wholePropertyDeposit = amount;
    }

    public int get_numBedroom() {
        return _numBedroom;
    }

    public void set_numBedroom(int _numBedroom) {
        this._numBedroom = _numBedroom;
    }

    public Boolean is_isTherePet() {
        return _isTherePet;
    }

    public void set_isTherePet(Boolean _isTherePet) {
        this._isTherePet = _isTherePet;
    }

    public Boolean is_areThereSmokers() {
        return _areThereSmokers;
    }

    public void set_areThereSmokers(Boolean _areThereSmokers) {
        this._areThereSmokers = _areThereSmokers;
    }

    public PreferenceList get_preferenceList() {
        return _preferenceList;
    }

    public void set_preferenceList(PreferenceList _preferenceList) {
        this._preferenceList = _preferenceList;
    }

    public Pair<Integer, Integer> getRangeDesiredAge() {
        if (_rangeDesiredAge != null)
            sanitiseAgeRange(_rangeDesiredAge);

        return _rangeDesiredAge;
    }

    public void setRangeDesiredAge(Pair<Integer, Integer> _rangeDesiredAge) {
        this._rangeDesiredAge = _rangeDesiredAge;
    }

    public String getPhoneNumber() {
        return _phoneNumber;
    }

    public void setPhoneNumber(String _phoneNumber) {
        this._phoneNumber = _phoneNumber;
    }

    public PlaceAdPostcodeAreaList get_lPostcodeAreaList() {
        return _lPostcodeAreaList;
    }

    public void set_lPostcodeAreaList(PlaceAdPostcodeAreaList _lPostcodeAreaList) {
        this._lPostcodeAreaList = _lPostcodeAreaList;
    }

    public String get_companyName() {
        return _companyName;
    }

    public Boolean is_feesApply() {
        return _feesApply;
    }

    public void set_feesApply(Boolean _feesApply) {
        this._feesApply = _feesApply;
    }

    public void set_companyName(String _companyName) {
        this._companyName = _companyName;
    }

    public Boolean getDisplayPhone() {
        return _displayPhone;
    }

    public void setDisplayPhone(Boolean _displayPhone) {
        this._displayPhone = _displayPhone;
    }

    public PreferenceList getAmenitiesList() {
        return _amenitiesList;
    }

    public void setAmenitiesList(PreferenceList amenitiesList) {
        _amenitiesList = amenitiesList;
    }

    public boolean wholeProperty() {
        return wholeProperty;
    }

    public void wholeProperty(boolean wholeProperty) {
        this.wholeProperty = wholeProperty;
    }

}
