package com.spareroom.model.business;

/**
 * Created by miguel.rossi on 12/05/2016.
 * <p>
 * The amenities for editing an offered advert.
 */
public enum EditAdWantedAmenityEnum {
    FURNITURE,
    SHARED_LIVING_ROOM,
    GARAGE,
    WASHING_MACHINE,
    DISABLE_ACCESS,
    GARDEN,
    BROADBAND,
    BALCONY,
    EN_SUITE,
    PARKING;

    public String toString() {

        if (ordinal() == FURNITURE.ordinal())
            return "Furnished";
        else if (ordinal() == SHARED_LIVING_ROOM.ordinal())
            return "Shared living room";
        else if (ordinal() == GARAGE.ordinal())
            return "Garage";
        else if (ordinal() == WASHING_MACHINE.ordinal())
            return "Washing machine";
        else if (ordinal() == DISABLE_ACCESS.ordinal())
            return "Disabled access";
        else if (ordinal() == GARDEN.ordinal())
            return "Garden/roof terrace";
        else if (ordinal() == BROADBAND.ordinal())
            return "Broadband";
        else if (ordinal() == BALCONY.ordinal())
            return "Balcony/patio";
        else if (ordinal() == EN_SUITE.ordinal())
            return "En suite";
        else if (ordinal() == PARKING.ordinal())
            return "Parking";

        return null;
    } //end toString()

}
