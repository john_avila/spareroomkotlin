package com.spareroom.model.business

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.spareroom.lib.util.paging.Page
import com.spareroom.model.business.SpareroomStatusCode.NO_PLACED_ADS
import com.spareroom.model.business.SpareroomStatusCode.SUCCESS

data class MyAdverts(
    @Expose @SerializedName("response_type") private val responseType: String? = "",
    @Expose @SerializedName("response_id") private val responseId: String? = "",
    @Expose @SerializedName("response") private val messageField: String? = "",
    @Expose @SerializedName("myads") private val myAds: MyAds?
) : Page<AbstractAd> {
    val wanted get() = myAds?.wantedAdverts ?: emptyList()
    val offered get() = myAds?.offeredAdverts ?: emptyList()
    val message get() = messageField ?: ""
    val loggedIn get() = responseId != SpareroomStatusCode.NOT_LOGGED_IN
    val notEnoughListings get() = responseId == SpareroomStatusCode.NOT_ENOUGH_LISTINGS
    override val success get() = loggedIn && (responseType.equals(SUCCESS, true) || responseType.isNullOrEmpty() || responseId == NO_PLACED_ADS)

    override val rawItems get() = offered
}

data class MyAds(
    @Expose @SerializedName("wanted") val wantedAdverts: List<AdWanted>?,
    @Expose @SerializedName("offered") val offeredAdverts: List<AdOffered>?)