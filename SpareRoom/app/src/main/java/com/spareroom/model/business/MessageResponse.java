package com.spareroom.model.business;

public class MessageResponse extends Response<NewMessageResponse> {

    public boolean isMessageWithExternalLinks() {
        return SpareroomStatusCode.MESSAGE_WITH_EXTERNAL_LINKS.equalsIgnoreCase(getErrorCode());
    }
}
