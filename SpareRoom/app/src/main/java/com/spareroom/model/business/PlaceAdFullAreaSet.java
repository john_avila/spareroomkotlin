package com.spareroom.model.business;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

public class PlaceAdFullAreaSet implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1933703980317939912L;

    private List<PlaceAdFullArea> _lArea = new LinkedList<>();

    /**
     * Tree holding id of the the parent area and count of references. if references == 0, the item is removed
     */
    private TreeMap<String, Integer> _mAreaCount0 = new TreeMap<>();
    private TreeMap<String, Integer> _mAreaCount1 = new TreeMap<>();
    private TreeMap<String, Integer> _mAreaCount2 = new TreeMap<>();

    /**
     * Registers a new reference to the area with <code>id</code>
     *
     * @param mCount tree that holds the count (see private members of this class)
     * @param id     id of the area
     */
    private void addCount(TreeMap<String, Integer> mCount, String id) {
        if (!mCount.containsKey(id))
            mCount.put(id, 0);
        mCount.put(id, mCount.get(id) + 1);
    }

    /**
     * Removes a reference to the area with <code>id</code>
     *
     * @param mCount tree that holds the count (see private members of this class)
     * @param id     id of the area
     */
    private void removeCount(TreeMap<String, Integer> mCount, String id) {
        if (!mCount.containsKey(id))
            return;
        int updatedCount = mCount.get(id) - 1;
        if (updatedCount == 0)
            mCount.remove(id);
        mCount.put(id, updatedCount);
    }

    public void add(PlaceAdFullArea area) {
        _lArea.add(area);

        if (area.get(0) != null)
            addCount(_mAreaCount0, area.get(0).getId());
        if (area.get(1) != null)
            addCount(_mAreaCount1, area.get(1).getId());
        if (area.get(2) != null)
            addCount(_mAreaCount2, area.get(2).getId());
    }

    public void remove(PlaceAdFullArea area) {
        _lArea.remove(area);

        if (area.get(0) != null)
            removeCount(_mAreaCount0, area.get(0).getId());
        if (area.get(1) != null)
            removeCount(_mAreaCount1, area.get(1).getId());
        if (area.get(2) != null)
            removeCount(_mAreaCount2, area.get(2).getId());
    }

    public Collection<PlaceAdArea> getLevel(int level) {
        LinkedList<PlaceAdArea> _lAreaLevel = new LinkedList<>();
        for (PlaceAdFullArea area : _lArea) {
            _lAreaLevel.add(area.get(level));
        }
        return _lAreaLevel;
    }

    public int count(int level, PlaceAdArea area) {
        Integer count = null;
        switch (level) {
            case 0:
                count = _mAreaCount0.get(area.getId());
                break;
            case 1:
                count = _mAreaCount1.get(area.getId());
                break;
            case 2:
                count = _mAreaCount2.get(area.getId());
                break;
            default:
                count = 0;
        }
        if (count == null)
            return 0;
        return count;
    }

    public boolean contains(int level, PlaceAdArea area) {
        switch (level) {
            case 0:
                return (_mAreaCount0.get(area.getId()) != null);
            case 1:
                return (_mAreaCount1.get(area.getId()) != null);
            case 2:
                return (_mAreaCount2.get(area.getId()) != null);
            default:
                return false;
        }
    }

    public int size() {
        return _lArea.size();
    }

}
