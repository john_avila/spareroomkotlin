package com.spareroom.model.business

import com.google.gson.annotations.*

data class AdWantedCompleteResponse(@Expose @SerializedName("advert_summary") val advert: AdWanted?)