package com.spareroom.model;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.lib.core.Action;
import com.spareroom.model.business.SpareroomStatus;

import java.util.LinkedList;

/**
 * Operation performed by the model. Implements the Command and Observer patterns. Implement Model
 * logic in <code>Action</code> subclass, result will be informed to the observers.
 */
public abstract class ModelAction {
    /**
     * Current status.
     */
    private Status _status = Status.PENDING;
    /**
     * Action to be performed.
     */
    private Action _action = null;
    /**
     * ResultType after performing the action.
     */
    private ResultType _resultType;
    /**
     * Outcome of the action performed
     */
    private Object _result = null;
    /**
     * The context where this <code>ModelAction</code> is running
     */
    private final SpareroomContext _srContext;
    /**
     * List of observers currently observing <code>ModelAction</code>
     */
    private final LinkedList<ModelActionObserver> _listObservers = new LinkedList<>();

    /**
     * Status of this <code>ModelAction</code>
     */
    public enum Status {
        PENDING,
        RUNNING,
        FINISHED
    }

    /**
     * Kind of result produced by the Model after performing the action
     */
    public enum ResultType {
        RESULT,
        SPAREROOM_STATUS,
        EXCEPTION
    }

    /**
     * Sets the <code>Action</code> containing the Model logic for this <code>ModelAction</code>.
     * The action must return result of the operation, <code>null</code> if there was no result from
     * performing the operation (procedure / void method). Exceptions should be thrown.
     *
     * @param action the operation containing the business logic for this <code>ModelAction</code>
     */
    protected void setAction(Action action) {
        _action = action;
    }

    /**
     * Constructor.
     *
     * @param srContext the current <code>SpareroomContext</code>
     */
    public ModelAction(SpareroomContext srContext) {
        _status = Status.PENDING;
        _srContext = srContext;
    }

    /**
     * Constructor. Do not use unless Unit Testing. Instead, extend this class and override
     * {@link #ModelAction(SpareroomContext srContext)} specifying the <code>Action</code> within
     * the constructor to promote encapsulation.
     *
     * @param srContext the current <code>SpareroomContext</code>
     * @param action    model logic
     */
    public ModelAction(SpareroomContext srContext, Action action) {
        _status = Status.PENDING;
        _srContext = srContext;
        _action = action;
    }

    /**
     * Gets the SpareRoom context
     *
     * @return the SpareRoom context where this <code>ModelAction</code> is running
     */
    protected SpareroomContext getSpareroomContext() {
        return _srContext;
    }

    /**
     * Gets current status of this <code>ModelAction</code>
     *
     * @return <code>PENDING</code>, <code>RUNNING</code> or <code>FINISHED</code>
     */
    public Status getStatus() {
        return _status;
    }

    /**
     * Adds an observer that would be notified of the result of the operation
     *
     * @param observer the observer to be notified
     */
    public void addModelActionObserver(ModelActionObserver observer) {
        _listObservers.add(observer);
    }

    /**
     * Removes an observer so it will not be notified any longer
     *
     * @param observer the observer that will stop receiving notifications
     */
    public void removeModelActionObserver(ModelActionObserver observer) {
        _listObservers.remove(observer);
    }

    /**
     * Removes all observers so none will be notified any longer
     */
    public void removeAllObservers() {
        _listObservers.clear();
    }

    /**
     * Notifies all observers that this <code>ModelAction</code> has finished
     */
    private void notifyObservers() {
        for (ModelActionObserver observer : _listObservers) {
            observer.notifyModelActionResult(this, _resultType, _result);
        }
    }

    /**
     * Prepares this class to execute the <code>Action</code>
     */
    private void preExecuteAction() {
        _status = Status.RUNNING;
    }

    /**
     * Performs the action for this class and notifies observers about the result (calls
     * {@link #preExecuteAction()} and {@link #postExecuteAction()} internally)
     */
    private void executeAction(Object... params) {
        preExecuteAction();
        try {
            Object actionResult = _action.execute(params);

            // Process the kind of result we had
            // SpareRoom error (SpareroomStatus class)
            if (actionResult == null) {
                _resultType = ResultType.RESULT;
            } else if (actionResult.getClass().getName().equals(
                    SpareroomStatus.class.getName())) { // String check first for efficiency
                _resultType = ResultType.SPAREROOM_STATUS;
            } else if (!(actionResult instanceof Exception)) { // Expected result
                _resultType = ResultType.RESULT;
            } else { // Exception - will be captured if returned instead of thrown
                _resultType = ResultType.EXCEPTION;
            }

            _result = actionResult;
        } catch (Exception e) { // Exception - captured if thrown
            _resultType = ResultType.EXCEPTION;
            _result = e;
        }
        postExecuteAction();
    }

    /**
     * Updates this class after performing the< code>Action</code>
     */
    private void postExecuteAction() {
        _status = Status.FINISHED;
        notifyObservers();
    }

    /**
     * Performs the operation and notifies observers when finished.
     */
    public final void execute(Object... params) {
        executeAction(params);
    }

}
