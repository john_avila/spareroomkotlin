package com.spareroom.model.business;

import java.io.Serializable;

public class User implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 960769338529224942L;

    private String _userId;
    private String _firstName;
    private String _lastName;
    private String _profilePhotoUrl;

    public String get_userId() {
        return _userId;
    }

    public void set_userId(String _userId) {
        this._userId = _userId;
    }

    public String get_firstName() {
        return _firstName;
    }

    public void set_firstName(String _firstName) {
        this._firstName = _firstName;
    }

    public String get_lastName() {
        return _lastName;
    }

    public void set_lastName(String _lastName) {
        this._lastName = _lastName;
    }

    public String get_profilePhotoUrl() {
        return _profilePhotoUrl;
    }

    public void set_profilePhotoUrl(String _profilePhotoUrl) {
        this._profilePhotoUrl = _profilePhotoUrl;
    }

}
