package com.spareroom.model.business

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.spareroom.model.business.advertiser.WantedAdvertiser
import com.spareroom.model.business.date.IsoDate

const val SINGLE_OR_DOUBLE_ROOM = "a single or double room"
const val DOUBLE_ROOM = "a double room"
const val TWIN_ROOM_OR_2_ROOMS = "a twin room or 2 rooms"
const val TWO_ROOMS = "2 rooms"
const val TWO_DOUBLE_ROOMS = "2 double rooms"
const val TWO_SINGLE_ROOMS = "2 single rooms"
const val FLAT_OR_HOUSE_TO_RENT = "flat or house to rent"

data class AdWanted(

    // wanted
    @Expose @SerializedName("combined_budget") private val combinedBudgetField: String? = "",
    @Expose @SerializedName("room_size") private val roomSizeField: String? = "",
    @Expose @SerializedName("wanted_advertiser_desc") private val wantedAdvertiserDescField: String? = "",
    @Expose @SerializedName("areas_looking_in") private val areasLookingInField: List<Area>? = emptyList(),
    @Expose @SerializedName("num_areas") private val numberOfAreasField: String? = "",
    @Expose @SerializedName("number_of_seekers") private val numberOfSeekersField: String? = "",
    @Expose @SerializedName("area_list") private var areaListField: String? = "",
    @Expose @SerializedName("example_matching_area") private var searchAreasField: String? = "",

    // common
    @Expose @SerializedName("available_from") override val availableFromField: String? = "",
    @Expose @SerializedName("per") override val pricePeriodField: String? = "",
    @Expose @SerializedName("advert_id") override val idField: String? = "",
    @Expose @SerializedName("new") override val newnessStatusField: String? = "",
    @Expose @SerializedName("photos") override val photosField: List<Picture>? = emptyList(),
    @Expose @SerializedName("upgrade_applied") override val upgradeAppliedField: String? = "",
    @Expose @SerializedName("days_left") override val upgradeDaysLeftField: String? = "",
    @Expose @SerializedName("date_last_renewed") override val dateLastRenewedField: String? = "",
    @Expose @SerializedName("date_ad_placed") override val datePlacedField: String? = "",
    @Expose @SerializedName("date_live_iso8601") override val dateLastLiveField: IsoDate? = null,
    @Expose @SerializedName("ad_title") override val titleField: String? = "",
    @Expose @SerializedName("ad_status") override val statusField: String? = "",
    @Expose @SerializedName("ad_text_255") override val shortDescriptionField: String? = "",
    @Expose @SerializedName("ad_text") override val fullDescriptionField: String? = "",
    @Expose @SerializedName("featured") override val featuredField: String? = "",
    @Expose @SerializedName("bold_ad") override val boldField: String? = "",
    @Expose @SerializedName("youtube_id") override val youtubeIdField: String? = "",
    @Expose @SerializedName("advertiser_id") override val advertiserIdField: String? = "",
    @Expose @SerializedName("tel_formatted") override val advertiserPhoneNumberField: String? = "",
    @Expose @SerializedName("early_bird_required") override val earlyBirdRequiredField: String? = "",
    @Expose @SerializedName("contactable_by_tel") override val contactableByPhoneField: String? = "",
    @Expose @SerializedName("free_to_contact") override val freeToContactField: String? = "",
    @Expose @SerializedName("profession") override val professionField: String? = "",
    @Expose @SerializedName("advertiser_name") override val advertiserNameField: String? = "",
    @Expose @SerializedName("couples") override val couplesField: String? = "",
    @Expose @SerializedName("advertiser_profile_photo_url") override val advertiserProfilePhotoUrlField: String? = "",
    @Expose @SerializedName("context") override var contextField: String? = "",
    @Expose @SerializedName("main_image_large_url") override var largePictureUrlField: String? = ""

) : AbstractAd() {

    val combinedBudget get() = combinedBudgetField ?: ""
    val roomSize get() = roomSizeField ?: ""
    var searchAreas
        set(value) {
            searchAreasField = value
            areaListField = value
        }
        get() = if (searchAreasField?.isNotEmpty() == true) searchAreasField!! else areaListField ?: ""

    val wantedAdvertiserDesc get() = wantedAdvertiserDescField ?: ""

    val numberOfAreas get() = numberOfAreasField?.toIntOrNull() ?: 0

    val areasLookingIn get() = areasLookingInField ?: emptyList()

    override val price get() = getPrice(combinedBudget, pricePeriod)

    override val advertiser
        get() = WantedAdvertiser(advertiserProfilePhotoUrl, advertiserName, profession, couples, numberOfSeekersField?.toIntOrNull() ?: 0)

    fun searchAreasAsList(): List<String> {
        return if (searchAreas.isNotBlank()) {
            searchAreas.split(",").filter { it.isNotBlank() }.map { it -> it.trim() }
        } else {
            areasLookingIn.map { it.areaName }.filter { it.isNotBlank() }.map { it -> it.trim() }
        }
    }
}
