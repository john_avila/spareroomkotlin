package com.spareroom.model.business;

/**
 * Created by miguel.rossi on 12/05/2016.
 * <p>
 * The amenities for editing an offered advert.
 */
public enum EditAdOfferedAmenityEnum {
    PARKING,
    BALCONY,
    GARDEN,
    DISABLED_ACCESS,
    GARAGE;

    public String toString() {

        if (ordinal() == PARKING.ordinal())
            return "Parking";
        else if (ordinal() == BALCONY.ordinal())
            return "Balcony/patio";
        else if (ordinal() == GARDEN.ordinal())
            return "Garden/roof terrace";
        else if (ordinal() == DISABLED_ACCESS.ordinal())
            return "Disabled access";
        else if (ordinal() == GARAGE.ordinal())
            return "Garage";

        return null;
    } //end toString()

}
