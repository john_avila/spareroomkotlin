package com.spareroom.model.business

import com.spareroom.lib.util.paging.PageItem

data class AdvertItem(val advert: AbstractAd, var header: Boolean = false, var divider: Boolean = false) : PageItem {
    override val uniqueId get() = advert.id

    companion object {
        const val ADVERT_UNAVAILABLE = "AdvertUnavailable"
    }
}