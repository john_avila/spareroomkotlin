package com.spareroom.model.business;

import android.content.Context;

import com.spareroom.controller.SpareroomApplication;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class BillingHelper {

    public static String generatePayload(Context c) {
        String payload = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digest = md.digest(SpareroomApplication.getInstance().getSpareroomContext().getUserId().getBytes());
            payload = (new BigInteger(1, digest)).toString(16);

        } catch (NoSuchAlgorithmException ignore) {
        }

        return payload;
    }
}
