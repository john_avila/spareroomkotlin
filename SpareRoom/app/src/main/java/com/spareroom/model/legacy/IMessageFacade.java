package com.spareroom.model.legacy;

import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;

/**
 * Model Facade for message-related functionality
 *
 * @author manuel
 */
public interface IMessageFacade {

    SpareroomStatus newMessageThread(Parameters p)
            throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;

}
