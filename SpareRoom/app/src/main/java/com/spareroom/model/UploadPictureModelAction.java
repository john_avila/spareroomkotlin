package com.spareroom.model;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.BusinessFactorySingleton;
import com.spareroom.integration.business.SpareroomAbstractDao;
import com.spareroom.integration.exception.*;
import com.spareroom.model.business.NewPicture;

public class UploadPictureModelAction extends ModelActionImpl {

    public UploadPictureModelAction(final SpareroomContext spareroomContext) {
        super(spareroomContext);
    }

    @Override
    public Object internalExecuteAction(final SpareroomContext spareroomContext, Object param) throws UnavailableDataSourceException, InvalidArgumentException, UnexpectedResultException {
        SpareroomAbstractDao<NewPicture, Void, Void> pictureDao = BusinessFactorySingleton.getInstance(spareroomContext).getPictureDao();
        return pictureDao.create((NewPicture) param, null);
    }
}
