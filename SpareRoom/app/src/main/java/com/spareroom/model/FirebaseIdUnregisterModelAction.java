package com.spareroom.model;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.BusinessFactorySingleton;
import com.spareroom.integration.business.SpareroomAbstractDao;
import com.spareroom.integration.exception.InvalidArgumentException;
import com.spareroom.integration.exception.UnavailableDataSourceException;
import com.spareroom.integration.exception.UnexpectedResultException;
import com.spareroom.lib.core.Action;
import com.spareroom.model.exception.ModelInvalidArgumentException;
import com.spareroom.model.exception.ModelOperationCannotBePerformedException;
import com.spareroom.model.exception.ModelUnexpectedInternalIssueException;

/**
 * ModelAction for for unregister the Firebase Id in the SpareRoom database
 */
public class FirebaseIdUnregisterModelAction extends ModelAction {

    public FirebaseIdUnregisterModelAction(final SpareroomContext spareroomContext) {
        super(spareroomContext);

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws ModelOperationCannotBePerformedException, ModelInvalidArgumentException,
                    ModelUnexpectedInternalIssueException {

                SpareroomAbstractDao<String, Void, Void> spareroomAbstractDao =
                        BusinessFactorySingleton.getInstance(spareroomContext).getFirebaseIdTransactionDao();

                try {

                    spareroomAbstractDao.delete(params[0].toString(), null);
                    return null;

                } catch (UnavailableDataSourceException e) {
                    throw new ModelOperationCannotBePerformedException(e.getMessage(), e);
                } catch (InvalidArgumentException e) {
                    throw new ModelInvalidArgumentException(e.getMessage(), e);
                } catch (UnexpectedResultException e) {
                    throw new ModelUnexpectedInternalIssueException(e.getMessage(), e);
                }

            }
        });
    }

    /**
     * Do not use unless Unit Testing. Instead, extend this class and override
     * {@link #FirebaseIdUnregisterModelAction(SpareroomContext spareroomContext)} specifying
     * the <code>Action</code> within the constructor to promote encapsulation.
     *
     * @param spareroomContext SpareRoom app platform resources
     * @param action           model logic
     */
    public FirebaseIdUnregisterModelAction(SpareroomContext spareroomContext, Action action) {
        super(spareroomContext, action);
    }
}
