package com.spareroom.model.business;

import java.util.LinkedList;

public class AdBoard {
    private LinkedList<AbstractAd> _board;
    private int _total;
    private MapReference _mapReference;
    private BroadenSearches _broadenSearch;

    public AdBoard() {
        _board = new LinkedList<>();
        _mapReference = new MapReference();
        _broadenSearch = new BroadenSearches();
    }

    public AdBoard(LinkedList<AbstractAd> board) {
        _board = board;
    }

    public LinkedList<AbstractAd> get_board() {
        return _board;
    }

    public void set_board(LinkedList<AbstractAd> _board) {
        this._board = _board;
    }

    public int get_total() {
        return _total;
    }

    public void set_total(int _total) {
        this._total = _total;
    }

    public MapReference get_mapReference() {
        return _mapReference;
    }

    public void set_mapReference(MapReference _mapReference) {
        this._mapReference = _mapReference;
    }

    public BroadenSearches get_broadenSearch() {
        return _broadenSearch;
    }

    public void set_broadenSearch(BroadenSearches _broadenSearch) {
        this._broadenSearch = _broadenSearch;
    }

    public void markStatus(String adId, int status) {
        for (AbstractAd ad : _board) {
            if (ad.getId().equals(adId)) {
                switch (status) {
                    case AbstractAd.CONTACTABLE_CONTACTED:
                        ad.setContext(AbstractAd.CONTEXT_CONTACTED);
                        break;
                    case AbstractAd.SAVED:
                        ad.setContext(AbstractAd.CONTEXT_SHORTLIST);
                        break;
                    case AbstractAd.NOT_SAVED:
                        ad.setContext("");
                        break;
                }
                break;
            }
        }
    }

}
