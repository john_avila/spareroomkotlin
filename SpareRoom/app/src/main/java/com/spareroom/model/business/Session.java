package com.spareroom.model.business;

import java.io.Serializable;

public class Session implements Serializable, AccountConnectionInterface {
    private static final long serialVersionUID = -2652166711450636951L;

    private static final String IS_OFFERING = "haveashare";
    private static final String IS_SEARCHING = "lookingforashare";

    private String _sessionId;
    private String _sessionKey;
    private String _sessionType;
    private String _loggedIn;
    private String _facebookSessionKey;
    private String _facebookUserId;
    private String _messageCount;

    private String _upgradeProduct;
    private String _upgradeExpire;
    private String _upgradeTimeLeft;
    private String _upgradeNoAdsOffered;
    private String _upgradeNoAdsWanted;
    private String _profilePictureUrl;

    private String _email;

    private boolean _paid;

    private User _user;

    private UpgradeOptionList _upgradeOptions;

    private int _photoUploadLimit;
    private String _unixAccessExp;

    private String userType;

    public String get_sessionId() {
        return _sessionId;
    }

    public void set_sessionId(String _sessionId) {
        this._sessionId = _sessionId;
    }

    public String get_sessionKey() {
        return _sessionKey;
    }

    public void set_sessionKey(String _sessionKey) {
        this._sessionKey = _sessionKey;
    }

    public String get_sessionType() {
        return _sessionType;
    }

    public void set_sessionType(String _sessionType) {
        this._sessionType = _sessionType;
    }

    public String get_loggedIn() {
        return _loggedIn;
    }

    public void set_loggedIn(String _loggedIn) {
        this._loggedIn = _loggedIn;
    }

    public String get_facebookSessionKey() {
        return _facebookSessionKey;
    }

    public void set_facebookSessionKey(String _facebookSessionKey) {
        this._facebookSessionKey = _facebookSessionKey;
    }

    public String get_facebookUserId() {
        return _facebookUserId;
    }

    public void set_facebookUserId(String _facebookUserId) {
        this._facebookUserId = _facebookUserId;
    }

    public String get_messageCount() {
        return _messageCount;
    }

    public int getMessageCount() {
        try {
            return Integer.valueOf(_messageCount);
        } catch (Exception e) {
            return 0;
        }
    }

    public void set_messageCount(String _messageCount) {
        this._messageCount = _messageCount;
    }

    public User get_user() {
        return _user;
    }

    public void set_user(User _user) {
        this._user = _user;
    }

    public String get_upgradeProduct() {
        return _upgradeProduct;
    }

    public void set_upgradeProduct(String _upgradeProduct) {
        this._upgradeProduct = _upgradeProduct;
    }

    public String get_upgradeExpire() {
        return _upgradeExpire;
    }

    public void set_upgradeExpire(String _upgradeExpire) {
        this._upgradeExpire = _upgradeExpire;
    }

    public String get_upgradeTimeLeft() {
        return _upgradeTimeLeft;
    }

    public void set_upgradeTimeLeft(String _upgradeTimeLeft) {
        this._upgradeTimeLeft = _upgradeTimeLeft;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String get_upgradeNoAdsOffered() {
        return _upgradeNoAdsOffered;
    }

    public void set_upgradeNoAdsOffered(String _upgradeNoAdsOffered) {
        this._upgradeNoAdsOffered = _upgradeNoAdsOffered;
    }

    public String get_upgradeNoAdsWanted() {
        return _upgradeNoAdsWanted;
    }

    public void set_upgradeNoAdsWanted(String _upgradeNoAdsWanted) {
        this._upgradeNoAdsWanted = _upgradeNoAdsWanted;
    }

    public boolean is_paid() {
        return _paid;
    }

    public void set_paid(boolean _paid) {
        this._paid = _paid;
    }

    public String get_email() {
        return _email;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public UpgradeOptionList get_upgradeOptions() {
        return _upgradeOptions;
    }

    public void set_upgradeOptions(UpgradeOptionList _upgradeOptions) {
        this._upgradeOptions = _upgradeOptions;
    }

    public String get_profilePictureUrl() {
        return _profilePictureUrl;
    }

    public void set_profilePictureUrl(String _profilePictureUrl) {
        this._profilePictureUrl = _profilePictureUrl;
    }

    public int getPhotoUploadLimit() {
        return _photoUploadLimit;
    }

    public void setPhotoUploadLimit(int extraKeyPhotoLimit) {
        _photoUploadLimit = extraKeyPhotoLimit;
    }

    public String getUnixAccessExp() {
        return _unixAccessExp;
    }

    public void setUnixAccessExp(String unixAccessExp) {
        _unixAccessExp = unixAccessExp;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public boolean isOffering() {
        return IS_OFFERING.equalsIgnoreCase(userType);
    }

    public boolean isSearching() {
        return IS_SEARCHING.equalsIgnoreCase(userType);
    }

}
