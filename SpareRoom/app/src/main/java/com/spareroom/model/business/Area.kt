package com.spareroom.model.business

import com.google.gson.annotations.*

data class Area(@Expose @SerializedName("area_name") private val areaNameField: String?,
                @Expose @SerializedName("area_parent_name") private val areaParentNameField: String?) {

    val areaName get() = areaNameField ?: ""
}