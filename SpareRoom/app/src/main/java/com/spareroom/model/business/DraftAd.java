package com.spareroom.model.business;

import com.spareroom.lib.core.Pair;
import com.spareroom.lib.util.NumberUtil;

import java.io.Serializable;

import static com.spareroom.lib.util.NumberUtil.isLowerThanMinAge;

/**
 * TODO make it abstract
 * Advert that has not been placed yet or is being modified.
 */
public class DraftAd implements Serializable {

    private static final long serialVersionUID = -7392288813757698153L;

    private String _id = null;
    private String _firstName = null;
    private String _lastName = null;
    private String _email = null;

    private String title;
    private String description;
    private String picturePath;

    private String _interests;

    /**
     * The extra fields which are not modelled on the app but required by the server when updating
     * an advert. They are retrieved from the server when requesting an existing advert. Do not
     * modify the contents manually. <code>null</code> if there are no extras for this advert.
     */
    private String _jsonExtras = null;

    public String get_firstName() {
        return _firstName;
    }

    public void set_firstName(String _firstName) {
        this._firstName = _firstName;
    }

    public String get_lastName() {
        return _lastName;
    }

    public void set_lastName(String _lastName) {
        this._lastName = _lastName;
    }

    public String get_email() {
        return _email;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getJsonExtras() {
        return _jsonExtras;
    }

    public void setJsonExtras(String _jsonExtras) {
        this._jsonExtras = _jsonExtras;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public boolean hasPicture() {
        return picturePath != null;
    }

    public String getInterests() {
        return _interests;
    }

    public void setInterests(String interests) {
        _interests = interests;
    }

    void sanitiseAgeRange(Pair<Integer, Integer> ageRange) {
        if (isLowerThanMinAge(ageRange.getKey()))
            ageRange.setKey(NumberUtil.MIN_AGE);

        if (isLowerThanMinAge(ageRange.getValue()))
            ageRange.setValue(NumberUtil.MIN_AGE);
    }

}
