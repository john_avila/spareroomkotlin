package com.spareroom.model.legacy;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.legacy.LegacyBusinessFactorySingleton;
import com.spareroom.integration.business.legacy.rest.IVideoDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InterruptedCommunicationException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.lib.core.Action;
import com.spareroom.lib.core.ILongTaskObserver;
import com.spareroom.model.ModelAction;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.exception.ModelInvalidArgumentException;
import com.spareroom.model.exception.ModelOperationCannotBePerformedException;
import com.spareroom.model.exception.ModelOperationInterruptedException;
import com.spareroom.model.exception.ModelUnexpectedInternalIssueException;

/**
 * Created by manuel on 27/09/2016.
 */
public class VideoUploadModelAction extends ModelAction {

    public VideoUploadModelAction(SpareroomContext sc) {
        super(sc);
        setAction(new Action() {
            @Override
            public Object execute(Object... params)
                    throws ModelOperationCannotBePerformedException,
                    ModelInvalidArgumentException,
                    ModelUnexpectedInternalIssueException,
                    ModelOperationInterruptedException {
                // TODO add interceptors to UploadVideoTask and refactor parameter processing here
/*
                String flatshareId = (String) params[0];
                String flatshareType = (String) params[1];
                String videoPath = (String) params[2];
*/
                IVideoDAO dao = LegacyBusinessFactorySingleton
                        .getInstance().getVideoDAO();

                // TODO use refactored parameters and refactor VideoDAO
                try {
                    return dao.create((Parameters) params[0], (ILongTaskObserver) params[1]);
                } catch (NetworkConnectivityException e) {
                    throw new ModelOperationCannotBePerformedException(e);
                } catch (InvalidJSONFormatException e) {
                    throw new ModelUnexpectedInternalIssueException(e);
                } catch (AuthenticationException e) {
                    throw new ModelInvalidArgumentException(e);
                } catch (ClientErrorException e) {
                    throw new ModelInvalidArgumentException(e);
                } catch (ServerErrorException e) {
                    throw new ModelOperationCannotBePerformedException(e);
                } catch (InterruptedCommunicationException e) {
                    throw new ModelOperationInterruptedException(e);
                }

            } //end execute(Object... params)
        });

    } //end DraftAdOfferedReadModelAction(SpareroomContext sc)

} //end class DraftAdOfferedReadModelAction
