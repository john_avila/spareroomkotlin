package com.spareroom.model.legacy;

import android.content.Context;

import com.spareroom.integration.business.legacy.ILegacyBusinessAbstractFactory;
import com.spareroom.integration.business.legacy.rest.*;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.dependency.component.ComponentRepository;
import com.spareroom.integration.dependency.module.DaoModule;
import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.sharedpreferences.SearchSuggestionListDAO;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.model.business.*;
import com.spareroom.model.exception.*;

import javax.inject.Inject;
import javax.inject.Named;

public class SearchFacade implements ISearchFacade {
    private ILegacyBusinessAbstractFactory _webServiceFactory;

    @Inject
    IAdOfferedCompleteDao adOfferedCompleteDao;

    @Inject
    IAdWantedCompleteDAO adWantedCompleteDao;

    @Inject
    @Named(DaoModule.OFFERED_SAVED_ADS_LIST_DAO)
    ISavedAdListDAO savedAdOfferedListDao;

    @Inject
    @Named(DaoModule.WANTED_SAVED_ADS_LIST_DAO)
    ISavedAdListDAO savedAdWantedListDao;

    public SearchFacade(ILegacyBusinessAbstractFactory webServiceFactory) {
        ComponentRepository.get().getAppComponent().inject(this);
        _webServiceFactory = webServiceFactory;
    }

    @Override
    public AdBoard searchForRent(Parameters parameters, SearchType searchType) throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException {

        try {
            // The server is currently sending to different kinds of objects as a response, so needs to be handled like this
            return _webServiceFactory.getAdBoardDAO(searchType).read(parameters);
        } catch (NetworkConnectivityException | ServerErrorException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (ClientErrorException e) {
            throw new ServiceUnavailableException(e.getMessage());
        }
    }

    public AdOffered getAdOffered(String advertId) throws ServiceUnavailableException, MissingSystemFeatureException, InconsistentStateException {
        try {
            return adOfferedCompleteDao.read(advertId);
        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException | ClientErrorException | ServerErrorException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        }
    }

    public AdWanted getAdWanted(String advertId) throws ServiceUnavailableException, MissingSystemFeatureException, InconsistentStateException {
        try {
            return adWantedCompleteDao.read(advertId);
        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException | ClientErrorException | ServerErrorException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        }
    }

    public SpareroomStatus saveSearch(Parameters p) throws ServiceUnavailableException, MissingSystemFeatureException, InconsistentStateException {
        try {
            ISavedAdDAO dao = _webServiceFactory.getSavedAdDAO();
            return dao.create(p);
        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException | ClientErrorException | ServerErrorException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        }
    }

    public boolean markAsCalled(Parameters p) throws ServiceUnavailableException, MissingSystemFeatureException, InconsistentStateException {
        try {
            IMarkAdDAO dao1 = _webServiceFactory.getMarkedAdDAO();
            return dao1.create(p);
        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException | ClientErrorException | ServerErrorException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        }
    }

    public boolean clockCall(Parameters p) throws ServiceUnavailableException, MissingSystemFeatureException, InconsistentStateException {
        try {
            IClockCalledDAO dao2 = _webServiceFactory.getClockCalledDAO();
            return dao2.create(p);
        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException | ClientErrorException | ServerErrorException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        }
    }

    public boolean clockFeatured(Parameters p) throws ServiceUnavailableException, MissingSystemFeatureException, InconsistentStateException {
        try {
            IClockFeaturedDAO dao2 = _webServiceFactory.getClockFeaturedDAO();
            return dao2.create(p);
        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException | ClientErrorException | ServerErrorException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        }
    }

    public Object getSavedOfferedAds(Parameters p) throws ServiceUnavailableException, MissingSystemFeatureException, InconsistentStateException {
        try {
            Object result = savedAdOfferedListDao.read(p);

            if (result instanceof AdBoard)
                return result;
            // else if (result instanceof SpareroomStatus) // Login exception
            // 	throw new InvalidUserException();
            return null;
        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException | ServerErrorException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        } catch (ClientErrorException e) {
            throw new ServiceUnavailableException(e.getMessage());
        }
    }

    public AdBoard getSavedWantedAds(Parameters p) throws InvalidUserException, ServiceUnavailableException, MissingSystemFeatureException, InconsistentStateException {
        try {
            Object result = savedAdWantedListDao.read(p);

            if (result instanceof AdBoard)
                return (AdBoard) result;
            else if (result instanceof SpareroomStatus) // Login exception
                throw new InvalidUserException();
            return null;
        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException | ServerErrorException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        } catch (ClientErrorException e) {
            throw new ServiceUnavailableException(e.getMessage());
        }
    }

    public SpareroomStatus removeSavedAd(Parameters p) throws ServiceUnavailableException, MissingSystemFeatureException, InconsistentStateException {
        try {
            IRemovedSavedAdDAO dao = _webServiceFactory.getRemovedSavedAdDAO();
            return dao.create(p);
        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException | ServerErrorException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        } catch (ClientErrorException e) {
            throw new ServiceUnavailableException(e.getMessage());
        }
    }

    public SavedSearchList getSavedSearches(Parameters p) throws ServiceUnavailableException, MissingSystemFeatureException, InconsistentStateException {
        try {
            ISavedSearchListDAO dao = _webServiceFactory.getSavedSearchListDAO();
            return dao.read(p);
        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException | ServerErrorException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        } catch (ClientErrorException e) {
            throw new ServiceUnavailableException(e.getMessage());
        }
    }

    public Object newSavedSearch(Parameters p) {
        try {
            ISavedSearchDAO dao = _webServiceFactory.getSavedSearchDAO();
            return dao.create(p);
        } catch (AuthenticationException e) {
            return new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException e) {
            return new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            return new InconsistentStateException();
        } catch (ClientErrorException e) {
            return new ServiceUnavailableException();
        } catch (ServerErrorException e) {
            return new ServiceUnavailableException();
        }
    }

    public Object editSavedSearch(Parameters p) {
        try {
            ISavedSearchDAO dao = _webServiceFactory.getSavedSearchDAO();
            return dao.update(p);
        } catch (AuthenticationException e) {
            return new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException e) {
            return new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            return new InconsistentStateException();
        } catch (ClientErrorException e) {
            return new ServiceUnavailableException();
        } catch (ServerErrorException e) {
            return new ServiceUnavailableException();
        }
    }

    public Object deleteSavedSearch(Parameters p) {
        try {
            ISavedSearchDAO dao = _webServiceFactory.getSavedSearchDAO();
            return dao.update(p);
        } catch (AuthenticationException e) {
            return new MissingSystemFeatureException(e.getMessage());
        } catch (NetworkConnectivityException e) {
            return new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            return new InconsistentStateException();
        } catch (ClientErrorException e) {
            return new ServiceUnavailableException();
        } catch (ServerErrorException e) {
            return new ServiceUnavailableException();
        }
    }

    public SearchAutocompleteList autocomplete(Context applicationContext, String s) throws
            AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException {
        SearchSuggestionListDAO dao = new SearchSuggestionListDAO(applicationContext);
        SearchAutocompleteList lSearchSuggestion = dao.read(s);
        if (lSearchSuggestion != null)
            return lSearchSuggestion;
        ISearchSuggestionListDAO apiDao = _webServiceFactory.getSearchSuggestionListDAO();
        Parameters parameters = new Parameters();
        parameters.add("q", s);
        SearchAutocompleteList searchSuggestionList = apiDao.read(parameters);
        if (searchSuggestionList != null) {
            dao.create(s, searchSuggestionList);
        }
        return searchSuggestionList;
    }

}
