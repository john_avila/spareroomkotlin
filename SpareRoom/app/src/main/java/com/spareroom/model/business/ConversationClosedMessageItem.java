package com.spareroom.model.business;

public class ConversationClosedMessageItem extends Message {

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof ConversationClosedMessageItem;
    }

    @Override
    public int hashCode() {
        return 1;
    }
}
