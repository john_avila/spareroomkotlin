package com.spareroom.model.business

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SingleAdvertRestResponse<T : AbstractAd>(@Expose @SerializedName("advert_summary") val advert: T?)