package com.spareroom.model.legacy;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.DraftAdOfferedReadModelAction;
import com.spareroom.model.DraftAdWantedReadModelAction;
import com.spareroom.model.business.DraftAd;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.PlaceAdArea;
import com.spareroom.model.business.SpareroomStatus;

import java.util.Collection;

public interface IPlaceAdFacade {
    // public SpareroomStatus placeAdvert(DraftAd draft);

    SpareroomStatus placeOfferedAdvert(DraftAdOffered draft) throws AuthenticationException, NetworkConnectivityException, ClientErrorException,
            ServerErrorException, InvalidJSONFormatException;

    SpareroomStatus placeWantedAdvert(DraftAdWanted draft) throws AuthenticationException, NetworkConnectivityException, ClientErrorException,
            ServerErrorException, InvalidJSONFormatException;

    /**
     * @deprecated Use {@link DraftAdOfferedReadModelAction}
     * instead. This method is not safe to use any longer
     */
    @Deprecated
    Object getPlacedOfferedAdvert(String advertId) throws ServerErrorException, NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException;

    /**
     * @deprecated Use {@link DraftAdWantedReadModelAction}
     * instead. This method is not safe to use any longer
     */
    @Deprecated
    Object getPlacedWantedAdvert(String advertId) throws ServerErrorException, NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException;

    void editAdvert(DraftAd dr1aft);

    Object getNeightbourhoods(String postcode) throws AuthenticationException,
            NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException;

    void validateTitleDesc(String title, String description);

    Collection<PlaceAdArea> getAreasAjax(Parameters p) throws NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException;
}
