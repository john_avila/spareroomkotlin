package com.spareroom.model.business;

import java.io.Serializable;

public class PlaceAdPostcodeArea implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 9193619604196286218L;
    private String _name;
    private String _id;

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

}
