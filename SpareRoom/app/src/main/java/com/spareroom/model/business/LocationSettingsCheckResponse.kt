package com.spareroom.model.business

sealed class LocationSettingsCheckResponse

class SuccessfulLocationSettingsCheck : LocationSettingsCheckResponse()

class FailedLocationSettingsCheck : LocationSettingsCheckResponse()
