package com.spareroom.model;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.BusinessFactorySingleton;
import com.spareroom.integration.business.SpareroomAbstractDao;
import com.spareroom.integration.exception.InvalidArgumentException;
import com.spareroom.integration.exception.UnavailableDataSourceException;
import com.spareroom.integration.exception.UnexpectedResultException;
import com.spareroom.lib.core.Action;
import com.spareroom.model.exception.ModelInvalidArgumentException;
import com.spareroom.model.exception.ModelOperationCannotBePerformedException;
import com.spareroom.model.exception.ModelUnexpectedInternalIssueException;

/**
 * Model operation for log out.
 */
public class LogOutModelAction extends ModelAction {

    public LogOutModelAction(final SpareroomContext srContext) {
        super(srContext);

        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {
                SpareroomAbstractDao<Void, Void, Void> dao = BusinessFactorySingleton.getInstance(srContext).getLogOutTransactionDao();

                try {

                    return dao.read(null, null);

                } catch (UnavailableDataSourceException e) {
                    throw new ModelOperationCannotBePerformedException(e.getMessage(), e);
                } catch (InvalidArgumentException e) {
                    throw new ModelInvalidArgumentException(e.getMessage(), e);
                } catch (UnexpectedResultException e) {
                    throw new ModelUnexpectedInternalIssueException(e.getMessage(), e);
                }
            }
        });
    }

    /**
     * Constructor. Do not use unless Unit Testing. Instead, extend this class and override
     * {@link #LogOutModelAction(SpareroomContext srContext)} specifying the <code>Action</code>
     * within the constructor to promote encapsulation.
     *
     * @param srContext the current <code>SpareroomContext</code>
     * @param action    model logic
     */
    public LogOutModelAction(SpareroomContext srContext, Action action) {
        super(srContext, action);
    }
}
