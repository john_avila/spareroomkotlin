package com.spareroom.model.business

import androidx.recyclerview.widget.DiffUtil

class ConversationResponse : Response<Conversation>() {
    var messagesDiffResult: DiffUtil.DiffResult? = null
}
