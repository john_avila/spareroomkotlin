package com.spareroom.model.business;

import com.spareroom.controller.AppVersion;

import java.io.Serializable;

public class Price implements Serializable {

    private static final long serialVersionUID = 7595118059031619448L;

    private String _amount;
    private String _periodicity;
    private String _areBillsIncluded;

    public static final String PERIODICITY_WEEK = "pw";
    public static final String PERIODICITY_MONTH = "pcm";

    public static final String BILLS_INCLUDED_YES = "Yes";
    public static final String BILLS_INCLUDED_NO = "No";
    public static final String BILLS_INCLUDED_SOME = "Some";

    public Price() {
        _amount =
                AppVersion.flavor()
                        .getCurrency()
                        + "0";
        _periodicity = PERIODICITY_WEEK;
        _areBillsIncluded = BILLS_INCLUDED_NO;
    }

    public String get_amount() {
        return _amount;
    }

    public void set_amount(String _amount) {
        this._amount = _amount;
    }

    public String get_periodicity() {
        return _periodicity;
    }

    public void set_periodicity(String _periodicity) {
        this._periodicity = _periodicity;
    }

    public String get_areBillsIncluded() {
        return _areBillsIncluded;
    }

    public void set_areBillsIncluded(String _areBillsIncluded) {
        this._areBillsIncluded = _areBillsIncluded;
    }

}
