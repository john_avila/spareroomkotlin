package com.spareroom.model.business;

public class RegisterWithFacebookResult {
    private boolean _requireCustomPassword;
    private Session _session;

    public boolean is_requireCustomPassword() {
        return _requireCustomPassword;
    }

    public void set_requireCustomPassword(boolean _requireCustomPassword) {
        this._requireCustomPassword = _requireCustomPassword;
    }

    public Session get_session() {
        return _session;
    }

    public void set_session(Session _session) {
        this._session = _session;
    }

}
