package com.spareroom.model.exception;

/**
 * Created by miguel.rossi on 23/05/2016.
 */
public class ModelOperationCannotBePerformedException extends Exception {

    private static final long serialVersionUID = -4337835497722864396L;

    public ModelOperationCannotBePerformedException() {
        super();
    }

    public ModelOperationCannotBePerformedException(String detailMessage) {
        super(detailMessage);
    }

    public ModelOperationCannotBePerformedException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ModelOperationCannotBePerformedException(Throwable throwable) {
        super(throwable);
    }

}
