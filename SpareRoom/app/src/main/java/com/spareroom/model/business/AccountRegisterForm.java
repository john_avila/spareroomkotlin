package com.spareroom.model.business;

/**
 * Form with data needed to register a new Account
 * <p>
 * Created by manuel on 22/06/2016.
 */
public class AccountRegisterForm {
    private String _firstName;
    private String _lastName;
    private String _email;
    private String _password;
    private boolean _isInAgreement;
    private AdvertiserType _advertiserType;

    public enum AdvertiserType {OFFERER, SEEKER, OFFERER_AND_SEEKER}

    public String getFirstName() {
        return _firstName;
    }

    public void setFirstName(String firstName) {
        _firstName = firstName;
    }

    public String getLastName() {
        return _lastName;
    }

    public void setLastName(String lastName) {
        _lastName = lastName;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String email) {
        _email = email;
    }

    public String getPassword() {
        return _password;
    }

    public void setPassword(String password) {
        _password = password;
    }

    public AdvertiserType getAdvertiserType() {
        return _advertiserType;
    }

    public void setAdvertiserType(AdvertiserType advertiserType) {
        _advertiserType = advertiserType;
    }

    public boolean isInAgreement() {
        return _isInAgreement;
    }

    public void setInAgreement(boolean inAgreement) {
        _isInAgreement = inAgreement;
    }
}
