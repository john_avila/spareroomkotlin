package com.spareroom.model.legacy;

import com.spareroom.controller.SpareroomApplication;
import com.spareroom.integration.business.legacy.ILegacyBusinessAbstractFactory;
import com.spareroom.integration.business.legacy.rest.IPlaceAdAreaDAO;
import com.spareroom.integration.business.legacy.rest.IPlaceAdOfferedDAO;
import com.spareroom.integration.business.legacy.rest.IPlaceAdWantedDAO;
import com.spareroom.integration.business.legacy.rest.IPostcodeFinderDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InvalidArgumentException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.exception.UnavailableDataSourceException;
import com.spareroom.integration.exception.UnexpectedResultException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.DraftAd;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.PlaceAdArea;
import com.spareroom.model.business.SpareroomStatus;

import java.util.Collection;

public class PlaceAdFacade implements IPlaceAdFacade {
    private ILegacyBusinessAbstractFactory _webServiceFactory;
    private SpareroomApplication _application;

    /**
     * Constructor
     *
     * @param webServiceFactory Web Service Factory used for communicating with the server
     */
    public PlaceAdFacade(ILegacyBusinessAbstractFactory webServiceFactory, SpareroomApplication application) {
        _webServiceFactory = webServiceFactory;
        _application = application;
    }

    /*
    @Override
    public SpareroomStatus placeAdvert(DraftAd draft) {
        SpareroomStatus im = new SpareroomStatus();
        im.setCode(SpareroomStatusCode.CODE_SUCCESS);
        im.setCause(null);
        im.setMessage("Success");
        return im;
    }
    */
    public SpareroomStatus placeOfferedAdvert(DraftAdOffered draft)
            throws
            AuthenticationException,
            NetworkConnectivityException,
            ClientErrorException,
            ServerErrorException,
            InvalidJSONFormatException {

        IPlaceAdOfferedDAO dao = _webServiceFactory.getPlaceAdOfferedDAO();

        return dao.create(draft);
    }

    public SpareroomStatus placeWantedAdvert(DraftAdWanted draft)
            throws AuthenticationException, NetworkConnectivityException, ClientErrorException,
            ServerErrorException, InvalidJSONFormatException {
        IPlaceAdWantedDAO dao = _webServiceFactory.getPlaceAdWantedDAO();

        return dao.create(draft);
    }

    @Override
    public Object getPlacedOfferedAdvert(String advertId)
            throws ServerErrorException,
            NetworkConnectivityException,
            InvalidJSONFormatException,
            AuthenticationException,
            ClientErrorException {

        IPlaceAdOfferedDAO dao = _webServiceFactory.getPlaceAdOfferedDAO();

        try {
            DraftAd draftAd = (DraftAd) dao.read(advertId);
            draftAd.set_firstName(_application.getSpareroomContext().getSession().get_user().get_firstName());
            draftAd.set_lastName(_application.getSpareroomContext().getSession().get_user().get_lastName());

            return draftAd;
        } catch (UnavailableDataSourceException e) {
            throw new NetworkConnectivityException(e);
        } catch (InvalidArgumentException e) {
            throw new ClientErrorException(e);
        } catch (UnexpectedResultException e) {
            throw new InvalidJSONFormatException(e);
        }

    }

    @Override
    public Object getPlacedWantedAdvert(String advertId) throws ServerErrorException,
            NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException,
            ClientErrorException {
        IPlaceAdWantedDAO dao = _webServiceFactory.getPlaceAdWantedDAO();

        try {
            return dao.read(advertId);
        } catch (UnavailableDataSourceException e) {
            throw new NetworkConnectivityException(e);
        } catch (InvalidArgumentException e) {
            throw new ClientErrorException(e);
        } catch (UnexpectedResultException e) {
            throw new InvalidJSONFormatException(e);
        }
    }

    @Override
    public void editAdvert(DraftAd draft) {
        // TODO Auto-generated method stub

    }

    @Override
    public Object getNeightbourhoods(String postcode) throws AuthenticationException,
            NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException {
        Parameters p = new Parameters();
        IPostcodeFinderDAO dao = _webServiceFactory.getPostcodeFinderDAO();
        p.add("full_postcode", postcode);
        return dao.read(p);
    }

    @Override
    public void validateTitleDesc(String title, String description) {
        // TODO Auto-generated method stub

    }

    public Collection<PlaceAdArea> getAreasAjax(Parameters p) throws NetworkConnectivityException, InvalidJSONFormatException, AuthenticationException, ClientErrorException, ServerErrorException {
        IPlaceAdAreaDAO dao = _webServiceFactory.getPlaceAdAreaDAO();
        return dao.read(p);
    }
}
