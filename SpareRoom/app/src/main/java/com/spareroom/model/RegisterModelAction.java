package com.spareroom.model;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.BusinessFactorySingleton;
import com.spareroom.integration.business.SpareroomAbstractDao;
import com.spareroom.integration.exception.*;
import com.spareroom.lib.core.Action;
import com.spareroom.model.business.AccountConnectionInterface;
import com.spareroom.model.business.AccountRegisterForm;
import com.spareroom.model.exception.*;
import com.spareroom.model.extra.RegisterExtra;

/**
 * Model operation for Registering.
 */
public class RegisterModelAction extends ModelAction {

    /**
     * Constructor.
     *
     * @param srContext the current <code>SpareroomContext</code>
     */
    public RegisterModelAction(final SpareroomContext srContext) {
        super(srContext);
        setAction(new Action() {
            @Override
            public Object execute(Object... params) throws Exception {
                // no checking params here, the ControllerAction should had made sure this
                // ModelAction is invoked correctly

                AccountRegisterForm registerForm = (AccountRegisterForm) params[0];
                RegisterExtra extra = (RegisterExtra) params[1];
                SpareroomAbstractDao<AccountRegisterForm, RegisterExtra, AccountConnectionInterface> dao =
                        BusinessFactorySingleton.getInstance(srContext).getRegisterTransactionDao();

                try {
                    return dao.create(registerForm, extra);

                } catch (UnavailableDataSourceException e) {
                    throw new ModelOperationCannotBePerformedException(e.getMessage(), e);
                } catch (InvalidArgumentException e) {
                    throw new ModelInvalidArgumentException(e.getMessage(), e);
                } catch (UnexpectedResultException e) {
                    throw new ModelUnexpectedInternalIssueException(e.getMessage(), e);
                }
            }
        });
    }

    /**
     * Constructor. Do not use unless Unit Testing. Instead, extend this class and override
     * {@link #RegisterModelAction(SpareroomContext srContext)} specifying the <code>Action</code>
     * within the constructor to promote encapsulation.
     *
     * @param srContext the current <code>SpareroomContext</code>
     * @param action    model logic
     */
    public RegisterModelAction(SpareroomContext srContext, Action action) {
        super(srContext, action);
    }
}
