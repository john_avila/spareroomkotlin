package com.spareroom.model.business;

public class Suggestion {
    private String _location;

    public Suggestion() {

    }

    public String get_location() {
        return _location;
    }

    public void set_location(String _location) {
        this._location = _location;
    }

}
