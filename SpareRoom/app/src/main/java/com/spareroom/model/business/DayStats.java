package com.spareroom.model.business;

import java.util.Calendar;

public class DayStats {

    private Integer _emailCount;
    private Integer _interestCount;
    private Integer _telCount;
    private Integer _viewCount;
    private Calendar _date;

    public Integer get_emailCount() {
        return _emailCount;
    }

    public void set_emailCount(Integer _emailCount) {
        this._emailCount = _emailCount;
    }

    public Integer get_interestCount() {
        return _interestCount;
    }

    public void set_interestCount(Integer _interestCount) {
        this._interestCount = _interestCount;
    }

    public Integer get_telCount() {
        return _telCount;
    }

    public void set_telCount(Integer _telCount) {
        this._telCount = _telCount;
    }

    public Integer get_viewCount() {
        return _viewCount;
    }

    public void set_viewCount(Integer _viewCount) {
        this._viewCount = _viewCount;
    }

    public Calendar get_date() {
        return _date;
    }

    public void set_date(Calendar _date) {
        this._date = _date;
    }

}
