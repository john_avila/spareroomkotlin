package com.spareroom.model.legacy;

import android.content.Context;

import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.AdBoard;
import com.spareroom.model.business.AdOffered;
import com.spareroom.model.business.AdWanted;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.business.SavedSearchList;
import com.spareroom.model.business.SearchAutocompleteList;
import com.spareroom.model.business.SearchType;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.exception.InvalidUserException;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;

public interface ISearchFacade {

    AdBoard searchForRent(Parameters parameters, SearchType searchType) throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;

    AdOffered getAdOffered(String advertId) throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;

    AdWanted getAdWanted(String advertId) throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;

    SpareroomStatus saveSearch(Parameters p) throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;

    boolean markAsCalled(Parameters p) throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;

    boolean clockCall(Parameters p) throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;

    boolean clockFeatured(Parameters p) throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;

    Object getSavedOfferedAds(Parameters p) throws InvalidUserException, ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;

    AdBoard getSavedWantedAds(Parameters p) throws InvalidUserException, ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;

    SpareroomStatus removeSavedAd(Parameters p) throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;

    SavedSearchList getSavedSearches(Parameters p) throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;

    Object newSavedSearch(Parameters p) throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;

    Object editSavedSearch(Parameters p) throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;

    Object deleteSavedSearch(Parameters p) throws ServiceUnavailableException, InconsistentStateException, MissingSystemFeatureException;

    SearchAutocompleteList autocomplete(Context applicationContext, String s) throws AuthenticationException, NetworkConnectivityException, ClientErrorException, ServerErrorException, InvalidJSONFormatException;

}
