package com.spareroom.model.business;

import com.spareroom.ui.util.AdvertUtils;
import com.spareroom.ui.util.StringUtils;

import java.util.Arrays;

public class HeaderMessageItem extends Message {

    private final AbstractAd ad;
    private final String firstName;
    private final String lastName;
    private final String lastOnline;
    private final AdvertUtils advertUtils;

    public HeaderMessageItem(AbstractAd ad, Conversation conversation, AdvertUtils advertUtils) {
        this.ad = ad;
        this.firstName = conversation.getOtherUserFirstNames();
        this.lastName = conversation.getOtherUserLastName();
        this.lastOnline = conversation.getOtherUserLastOnline();
        this.advertUtils = advertUtils;
    }

    public AbstractAd getAd() {
        return ad;
    }

    @Override
    public String getId() {
        return ad != null ? ad.getId() : "";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof HeaderMessageItem))
            return false;

        HeaderMessageItem headerToCompare = (HeaderMessageItem) obj;

        return StringUtils.equals(getId(), headerToCompare.getId())
                && StringUtils.equals(firstName(), headerToCompare.firstName())
                && StringUtils.equals(lastName(), headerToCompare.lastName())
                && StringUtils.equals(lastOnline(), headerToCompare.lastOnline())
                && StringUtils.equals(location(), headerToCompare.location())
                && StringUtils.equals(monthlyPrice(), headerToCompare.monthlyPrice())
                && StringUtils.equals(availability(), headerToCompare.availability());
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[]{getId(), getFirstName(), getLastName(), lastOnline(), location(), monthlyPrice(), availability()});
    }

    private String monthlyPrice() {
        if (getAd() == null)
            return null;

        return getAd() instanceof AdOffered ? advertUtils.getFormattedMonthlyPrice((AdOffered) getAd(), true) : advertUtils.getFormattedMonthlyBudget((AdWanted) getAd());
    }

    private String availability() {
        return getAd() != null ? getAd().getAvailableFrom() : null;
    }

    private CharSequence location() {
        return (getAd() != null && getAd() instanceof AdOffered) ? advertUtils.getFormattedLocation((AdOffered) ad, "") : null;
    }

    private String firstName() {
        return firstName;
    }

    private String lastName() {
        return lastName;
    }

    private String lastOnline() {
        return lastOnline;
    }
}
