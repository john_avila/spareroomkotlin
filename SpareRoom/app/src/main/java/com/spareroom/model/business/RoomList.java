package com.spareroom.model.business;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class RoomList implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 6220330478460475642L;
    private List<Room> _lRoom;

    public RoomList() {
        _lRoom = new LinkedList<>();
    }

    public void add(Room r) {
        _lRoom.add(r);
    }

    public void remove(Room r) {
        _lRoom.remove(r);
    }

    public void remove(int position) {
        _lRoom.remove(position);
    }

    public void replace(Room r, int position) {
        _lRoom.remove(position);
        _lRoom.add(position, r);
    }

    public Room get(int position) {
        return _lRoom.get(position);
    }

    public int size() {
        return _lRoom.size();
    }

}
