package com.spareroom.model.business;

import java.util.LinkedList;

public class SavedSearchList {
    private LinkedList<SavedSearch> _lSavedSearch;

    public SavedSearchList() {
        _lSavedSearch = new LinkedList<>();
    }

    public void add(SavedSearch savedSearch) {
        _lSavedSearch.add(savedSearch);
    }

    public int size() {
        return _lSavedSearch.size();
    }

    public SavedSearch get(int position) {
        return _lSavedSearch.get(position);
    }

}
