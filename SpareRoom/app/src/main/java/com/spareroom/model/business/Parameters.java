package com.spareroom.model.business;

import com.spareroom.ui.util.StringUtils;

import org.jetbrains.annotations.NotNull;

import java.util.*;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A set of parameters
 *
 * @author manuel
 */
public class Parameters {
    SortedMap<String, String> _parameters;

    public Parameters() {
        _parameters = new TreeMap<>();
    }

    public Parameters(Parameters p) {
        this._parameters = new TreeMap<>(p._parameters);
    }

    /**
     * Adds a parameter to this set. The parameter is replaced if exists.
     *
     * @param name  name of the parameter
     * @param value value of the parameter
     */
    public void add(String name, @Nullable String value) {
        if (!StringUtils.isNullOrEmpty(value))
            _parameters.put(name, value);
    }

    /**
     * Adds a parameter to this set. The parameter is replaced if exists.
     *
     * @param name  name of the parameter
     * @param value value of the parameter
     */
    public void add(String name, @Nullable Integer value) {
        if (value != null)
            _parameters.put(name, String.valueOf(value));
    }

    public void add(String name, @NonNull Double value) {
        _parameters.put(name, String.valueOf(value));
    }

    /**
     * Adds all parameters to this set (merge). The parameters replaced if exist.
     */
    public void add(Parameters p) {
        _parameters.putAll(p._parameters);
    }

    /**
     * Removes all parameters stored in this object
     */
    public void clear() {
        _parameters.clear();
    }

    /**
     * Returns the value of the parameter
     *
     * @param parameterName name of the parameter
     * @return value of the parameter, or null if this map contains no mapping for the key
     */
    @Nullable
    public String get(@Nullable String parameterName) {
        return _parameters.get(parameterName);
    }

    public boolean has(@Nullable String parameterName) {
        return _parameters.get(parameterName) != null;
    }

    /**
     * Retrieves all names of the parameters
     *
     * @return the names of all the parameters
     */
    @NotNull
    public String[] getParameterNames() {
        Set<String> keys = _parameters.keySet();
        return keys.toArray(new String[keys.size()]);
    }

    @NotNull
    public String[] getParameterValues() {
        String[] names = getParameterNames();
        String[] values = new String[names.length];
        for (int i = 0; i < names.length; i++) {
            values[i] = get(names[i]);
        }
        return values;
    }

    /**
     * Removes the parameter identified by the given key
     *
     * @param parameterName name of the parameter
     */
    public void remove(String parameterName) {
        _parameters.remove(parameterName);
    }

    /**
     * Removes parameters from this set
     *
     * @param p parameters to remove
     */
    public void remove(@NotNull Parameters p) {
        for (String name : p.getParameterNames()) {
            remove(name);
        }
    }

    public int size() {
        return _parameters.size();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        return _parameters.equals(((Parameters) o)._parameters);
    }

}
