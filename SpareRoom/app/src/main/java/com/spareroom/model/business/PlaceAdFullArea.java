package com.spareroom.model.business;

import java.io.Serializable;

public class PlaceAdFullArea implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -7851811746836619977L;
    private PlaceAdArea[] _fullArea = {null, null, null};

    public PlaceAdFullArea() {
    }

    /**
     * Sets an area at a specific level
     *
     * @param level Values 0, 1, 2. 0 for first level.
     * @param area  the area in the specified level
     */
    public void set(int level, PlaceAdArea area) {
        _fullArea[level] = area;
    }

    public PlaceAdArea[] get() {
        return _fullArea;
    }

    public PlaceAdArea get(int position) {
        if ((position < 0) || position >= _fullArea.length)
            return null;
        return _fullArea[position];
    }

    @Override
    public int hashCode() {
        return (
                _fullArea[0].getName() +
                        _fullArea[1].getName() +
                        _fullArea[2].getName()
        ).hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        return _fullArea[0].getId().equals(((PlaceAdFullArea) o)._fullArea[0].getId()) &&
                _fullArea[0].getName().equals(((PlaceAdFullArea) o)._fullArea[0].getName()) &&
                _fullArea[1].getId().equals(((PlaceAdFullArea) o)._fullArea[1].getId()) &&
                _fullArea[1].getName().equals(((PlaceAdFullArea) o)._fullArea[1].getName()) &&
                _fullArea[2].getId().equals(((PlaceAdFullArea) o)._fullArea[2].getId()) &&
                _fullArea[2].getName().equals(((PlaceAdFullArea) o)._fullArea[2].getName());
    }

}
