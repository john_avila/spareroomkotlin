package com.spareroom.model.business

import com.google.gson.annotations.*
import java.io.Serializable

data class Picture(
        @Expose @SerializedName("caption") var caption: String? = null,
        @Expose @SerializedName("large_url") var largeUrl: String? = null
) : Serializable {

    companion object {
        const val LARGE_PICTURE_URL_TEMPLATE = "%s?resize&height=%s"
        const val MAX_DIMENSION = 2999
    }

    var id: String? = null

    var videoThumbnail = false
}
