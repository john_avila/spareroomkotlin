package com.spareroom.model.business

enum class SearchItemType { LOCATION, RECENT_SEARCH, SAVED_SEARCH, EXAMPLE, TITLE, DIVIDER }

data class SearchListItem(
    val title: String = "",
    val iconId: Int = 0,
    val subtitle: String = "",
    val alerts: String = "",
    val searchItemType: SearchItemType = SearchItemType.EXAMPLE,
    private val enabledField: Boolean = true,
    val loading: Boolean = false
) {

    val hasOptions
        get() = searchItemType == SearchItemType.RECENT_SEARCH || searchItemType == SearchItemType.SAVED_SEARCH

    val enabled
        get() = searchItemType != SearchItemType.EXAMPLE && enabledField

}
