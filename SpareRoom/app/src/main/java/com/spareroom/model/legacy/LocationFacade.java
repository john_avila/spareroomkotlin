package com.spareroom.model.legacy;

import com.spareroom.controller.SpareroomApplication;
import com.spareroom.integration.business.legacy.ILegacyBusinessAbstractFactory;
import com.spareroom.integration.business.legacy.rest.ILocationDAO;
import com.spareroom.integration.business.legacy.rest.apiv1.security.exception.AuthenticationException;
import com.spareroom.integration.exception.InconsistentStateException;
import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.integration.webservice.exception.ClientErrorException;
import com.spareroom.integration.webservice.exception.NetworkConnectivityException;
import com.spareroom.integration.webservice.exception.ServerErrorException;
import com.spareroom.model.business.Parameters;
import com.spareroom.model.exception.MissingSystemFeatureException;
import com.spareroom.model.exception.ServiceUnavailableException;

public class LocationFacade implements ILocationFacade {
    private ILegacyBusinessAbstractFactory _webServiceFactory;

    private SpareroomApplication _application;

    public LocationFacade(ILegacyBusinessAbstractFactory webServiceFactory, SpareroomApplication application) {
        _webServiceFactory = webServiceFactory;
        _application = application;
    }

    public Object getPostcodeUnit(Parameters p) throws InconsistentStateException, MissingSystemFeatureException, ServiceUnavailableException {
        try {
            ILocationDAO dao = _webServiceFactory.getLocationDAO();

            return dao.read(p);
        } catch (NetworkConnectivityException e) {
            throw new ServiceUnavailableException();
        } catch (InvalidJSONFormatException e) {
            throw new InconsistentStateException();
        } catch (AuthenticationException e) {
            throw new MissingSystemFeatureException(e.getMessage());
        } catch (ClientErrorException e) {
            throw new ServiceUnavailableException();
        } catch (ServerErrorException e) {
            throw new ServiceUnavailableException();
        }
    }
}
