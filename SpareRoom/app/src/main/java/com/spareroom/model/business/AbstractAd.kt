package com.spareroom.model.business

import com.spareroom.controller.AppVersion
import com.spareroom.lib.util.paging.PageItem
import com.spareroom.model.business.advertiser.Advertiser
import com.spareroom.model.business.date.IsoDate
import com.spareroom.ui.util.StringUtils

abstract class AbstractAd : PageItem {

    companion object {

        const val DSS_OK = "DSS ok"
        const val DSS_OK_QUESTION_MARK = "DSS OK?"
        const val DSS_WELCOME = "DSS welcome"

        const val ADVERTISER_TYPE_AGENT = "agent"

        // newness status
        const val NEWNESS_STATUS_NEW = "New"
        const val NEWNESS_STATUS_NEW_TODAY = "New Today"
        const val NEWNESS_STATUS_SUPER_RENEWED_TODAY = "Super Renewed Today"

        // rent options
        const val RENT_OPTION_ROOM = "room"
        const val RENT_OPTION_WHOLE = "whole"
        const val RENT_OPTION_EITHER = "either"

        // property type
        const val PROPERTY_TYPE_FLAT = "flat"
        const val PROPERTY_TYPE_HOUSE = "house"
        const val PROPERTY_TYPE_PROPERTY = "property"

        // profession
        const val PROFESSION_PROFESSIONAL = "P"
        const val PROFESSION_STUDENT = "S"

        // accommodation type
        const val ACCOMMODATION_STUDIO = "Studio flat"

        // status
        const val LIVE = "Live"
        const val PENDING = "Pending"

        // context
        const val CONTEXT_SHORTLIST = "shortlist"
        const val CONTEXT_INTEREST = "interest"
        const val CONTEXT_CONTACTED = "contacted"
        const val CONTEXT_UNSUITABLE = "unsuitable"

        const val NORMAL = 0
        const val BOLD = 1
        const val FEATURED = 2

        const val CONTACTABLE_CONTACTED = 2
        const val SAVED = 3
        const val NOT_SAVED = 4

        const val YES = "Y"
    }

    // common properties
    protected abstract val availableFromField: String?
    protected abstract val pricePeriodField: String?
    protected abstract val idField: String?
    protected abstract val newnessStatusField: String?
    protected abstract val photosField: List<Picture>?
    protected abstract val upgradeAppliedField: String?
    protected abstract val upgradeDaysLeftField: String?
    protected abstract val dateLastRenewedField: String?
    protected abstract val datePlacedField: String?
    protected abstract val dateLastLiveField: IsoDate?
    protected abstract val titleField: String?
    protected abstract val statusField: String?
    protected abstract val shortDescriptionField: String?
    protected abstract val fullDescriptionField: String?
    protected abstract val featuredField: String?
    protected abstract val boldField: String?
    protected abstract val youtubeIdField: String?
    protected abstract val advertiserIdField: String?
    protected abstract val advertiserPhoneNumberField: String?
    protected abstract val earlyBirdRequiredField: String?
    protected abstract val contactableByPhoneField: String?
    protected abstract val freeToContactField: String?
    protected abstract val professionField: String?
    protected abstract val advertiserNameField: String?
    protected abstract val couplesField: String?
    protected abstract val advertiserProfilePhotoUrlField: String?
    protected abstract var contextField: String?
    protected abstract var largePictureUrlField: String?

    val availableFrom get() = availableFromField ?: ""
    val pricePeriod get() = pricePeriodField ?: ""
    val id: String get() = idField ?: ""
    val newnessStatus get() = newnessStatusField ?: ""
    val photos get() = photosField ?: emptyList()
    val upgradeApplied get() = upgradeAppliedField ?: ""
    val upgradeDaysLeft get() = upgradeDaysLeftField ?: ""
    val dateLastRenewed get() = dateLastRenewedField ?: ""
    val datePlaced get() = datePlacedField ?: ""
    val dateLastLive get() = dateLastLiveField?.date
    val title get() = titleField ?: ""
    val status get() = statusField ?: ""
    val shortDescription get() = shortDescriptionField ?: ""
    val fullDescription get() = fullDescriptionField ?: ""
    val featured get() = featuredField ?: ""
    val bold get() = boldField ?: ""
    val youtubeId get() = youtubeIdField ?: ""
    val advertiserPhoneNumber get() = advertiserPhoneNumberField ?: ""
    val profession get() = professionField ?: ""
    val advertiserName get() = advertiserNameField ?: ""
    val couples get() = YES.equals(couplesField, ignoreCase = true)
    val advertiserProfilePhotoUrl get() = advertiserProfilePhotoUrlField ?: ""
    override val uniqueId get() = id

    open val price: String = ""

    abstract val advertiser: Advertiser

    var context
        set(value) {
            contextField = value
        }
        get() = contextField ?: ""

    val contacted get() = context == CONTEXT_CONTACTED

    val earlyBirdRequired get() = YES.equals(earlyBirdRequiredField, ignoreCase = true)

    val contactableByPhone get() = YES.equals(contactableByPhoneField, ignoreCase = true)

    val freeToContact get() = YES.equals(freeToContactField, ignoreCase = true)

    val isNew get() = NEWNESS_STATUS_NEW.equals(newnessStatus, ignoreCase = true)

    val isNewToday get() = NEWNESS_STATUS_NEW_TODAY.equals(newnessStatus, ignoreCase = true)

    val isSuperRenewedToday get() = NEWNESS_STATUS_SUPER_RENEWED_TODAY.equals(newnessStatus, ignoreCase = true)

    val isPending get() = PENDING.equals(status, ignoreCase = true)

    val isLive get() = LIVE.equals(status, ignoreCase = true)

    val isFeatured get() = YES.equals(featured, ignoreCase = true)

    val isBold get() = YES.equals(bold, ignoreCase = true)

    val isBoldOrFeatured get() = isBold || isFeatured

    var sheet: AdDetailsSheet? = null

    var largePictureUrl: String
        set(value) {
            largePictureUrlField = value
        }
        get() {
            val pictureUrl = largePictureUrlField ?: ""

            if (pictureUrl.isNotEmpty())
                return pictureUrl

            if (!photos.isEmpty()) {
                val largePictureUrl = photos[0].largeUrl
                if (largePictureUrl != null)
                    return largePictureUrl
            }

            return ""
        }

    val kind
        get() = when {
            isFeatured -> FEATURED
            isBold -> BOLD
            else -> NORMAL
        }

    fun hasNewnessStatus() = !StringUtils.isNullOrEmpty(newnessStatus, true) && isNew || isNewToday || isSuperRenewedToday

    fun hasContext() = context == CONTEXT_SHORTLIST || context == CONTEXT_INTEREST || context == CONTEXT_CONTACTED

    fun getPrice(price: String, period: String): String {
        var periodicity = period

        if (AppVersion.isUs()) {
            when (periodicity) {
                "pcm" -> periodicity = "/ month"
                "pw" -> periodicity = "/ week"
            }
        }

        periodicity = if (StringUtils.isNullOrEmpty(periodicity)) "" else " $periodicity"
        return if (StringUtils.isNullOrEmpty(price)) "" else AppVersion.flavor().currency + price + periodicity
    }
}
