package com.spareroom.model.business;

import java.io.Serializable;
import java.util.LinkedList;

public class PlaceAdPostcodeAreaList implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 7840405059629274040L;
    private LinkedList<PlaceAdPostcodeArea> _lAreas;
    private Float _minPrice = 0f;
    private Float _maxPrice = Float.MAX_VALUE;

    public PlaceAdPostcodeAreaList() {
        _lAreas = new LinkedList<>();
    }

    public LinkedList<PlaceAdPostcodeArea> get_lAreas() {
        return _lAreas;
    }

    public void set_lAreas(LinkedList<PlaceAdPostcodeArea> _lAreas) {
        this._lAreas = _lAreas;
    }

    public Float get_minPrice() {
        return _minPrice;
    }

    public void set_minPrice(Float _minPrice) {
        this._minPrice = _minPrice;
    }

    public Float get_maxPrice() {
        return _maxPrice;
    }

    public void set_maxPrice(Float _maxPrice) {
        this._maxPrice = _maxPrice;
    }

}
