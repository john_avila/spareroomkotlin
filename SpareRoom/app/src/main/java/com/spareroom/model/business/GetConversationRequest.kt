package com.spareroom.model.business

data class GetConversationRequest(val otherUserId: String, val advertId: String)