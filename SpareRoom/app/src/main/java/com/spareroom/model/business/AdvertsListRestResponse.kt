package com.spareroom.model.business

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.spareroom.lib.util.paging.Page

data class AdvertsListRestResponse<T : AbstractAd>(
    @Expose @SerializedName("results") private val advertsField: List<T>?,
    @Expose @SerializedName("success") private val successField: String?,
    @Expose @SerializedName("error") private val errorMessageField: String?
) : Page<AbstractAd> {

    val adverts get() = advertsField ?: emptyList()
    val errorMessage get() = errorMessageField ?: ""
    override val rawItems get() = adverts
    override val success get() = SpareroomStatusCode.CODE_SUCCESS == successField
}