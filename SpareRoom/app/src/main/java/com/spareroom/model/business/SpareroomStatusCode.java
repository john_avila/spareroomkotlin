package com.spareroom.model.business;

/**
 * Codes for SpareroomStatus object
 * <p>
 * Created by miguel.rossi on 31/05/2016.
 */
public class SpareroomStatusCode {

    public static final String CODE_ERROR_ID = "error_id";
    public static final String CODE_RESPONSE_ID = "response_id";
    public static final String SUCCESS = "success";

    public static final String CODE_0 = "0";
    public static final String CODE_SUCCESS = "1";
    public static final String CODE_400 = "400";
    public static final String CODE_500 = "500";
    public static final String NOT_LOGGED_IN = "600";
    public static final String CODE_649 = "649";
    public static final String CODE_650 = "650";
    public static final String CODE_800 = "800";
    public static final String NO_PERMISSION_TO_CONTACT = "801";
    public static final String MESSAGE_WITH_EXTERNAL_LINKS = "805";
    public static final String CODE_ALREADY_PROCESSED = "950";
    public static final String NOT_ENOUGH_LISTINGS = "953";
    public static final String NO_PLACED_ADS = "955";
    public static final String CODE_1101 = "1101";
    public static final String CODE_1102 = "1102";
    public static final String CODE_1103 = "1103";
    public static final String CODE_1151 = "1151";
    public static final String CODE_1152 = "1152";
    public static final String CODE_1153 = "1153";

}
