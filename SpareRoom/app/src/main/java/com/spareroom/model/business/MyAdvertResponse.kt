package com.spareroom.model.business

sealed class MyAdvertResponse

class AdvertRenewed(val advert: AbstractAd) : MyAdvertResponse()
class AdvertActivated(val advert: AbstractAd) : MyAdvertResponse()
class AdvertDeactivated(val advert: AbstractAd) : MyAdvertResponse()

class FailedToActivate(val throwable: Throwable) : MyAdvertResponse()
class FailedToRenew(val throwable: Throwable) : MyAdvertResponse()
class FailedToDeactivate(val throwable: Throwable) : MyAdvertResponse()