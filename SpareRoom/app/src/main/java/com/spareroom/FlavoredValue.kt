package com.spareroom

import java.util.*

interface FlavoredValue {

    val domain: String

    val webDomain: String

    val termsDomain: String

    val privacyDomain: String

    val twitterUsername: String

    val currency: String

    val phone: String

    val apiId: String

    val apiSecret: String

    val userAgentAppId: String

    val csEmail: String

    val locale: Locale

    val dateHourRepresentation: String

    val dateRepresentation: String

    val key: KInterface

    val upgrade1Bold: String

    val betaUrl: String

    val googlePlayAppName: String
}
