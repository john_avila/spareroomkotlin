package com.spareroom.viewmodel;

import android.app.Application;

import com.spareroom.integration.business.apiv2.IAdvertListDao;
import com.spareroom.livedata.*;
import com.spareroom.model.business.SearchAdvertListProperties;

import javax.inject.Inject;

public class FilterViewModel extends AndroidViewModel {
    private SearchAdvertListProperties searchProperties;

    private FilterAvailabilityDate filterAvailabilityDate = new FilterAvailabilityDate(getDisposables());
    private FilterAvailabilityMinTerm filterAvailabilityMinTerm = new FilterAvailabilityMinTerm(getDisposables());
    private FilterAvailabilityMaxTerm filterAvailabilityMaxTerm = new FilterAvailabilityMaxTerm(getDisposables());
    private FilterAvailabilityMinFlatmates filterAvailabilityMinFlatmates = new FilterAvailabilityMinFlatmates(getDisposables());
    private FilterAvailabilityMaxFlatmates filterAvailabilityMaxFlatmates = new FilterAvailabilityMaxFlatmates(getDisposables());
    private final SearchAdvertList userAdvertListSearch;
    private final SearchAdvertList emptyAdvertListSearch;

    private boolean shallPerformMatchesSearch = true;
    private boolean shallPerformEmptySearch = true;

    @Inject
    public FilterViewModel(Application application, IAdvertListDao advertListDao) {
        super(application);
        userAdvertListSearch = new SearchAdvertList(advertListDao, getDisposables());
        emptyAdvertListSearch = new SearchAdvertList(advertListDao, getDisposables());
    }

    public SearchAdvertList userAdvertListSearch() {
        return userAdvertListSearch;
    }

    public SearchAdvertList emptyAdvertListSearch() {
        return emptyAdvertListSearch;
    }

    public FilterAvailabilityDate filterAvailabilityDate() {
        return filterAvailabilityDate;
    }

    public FilterAvailabilityMinTerm filterAvailabilityMinTerm() {
        return filterAvailabilityMinTerm;
    }

    public FilterAvailabilityMaxTerm filterAvailabilityMaxTerm() {
        return filterAvailabilityMaxTerm;
    }

    public FilterAvailabilityMinFlatmates filterAvailabilityMinFlatmates() {
        return filterAvailabilityMinFlatmates;
    }

    public FilterAvailabilityMaxFlatmates filterAvailabilityMaxFlatmates() {
        return filterAvailabilityMaxFlatmates;
    }

    public SearchAdvertListProperties searchProperties() {
        return searchProperties;
    }

    public void searchProperties(SearchAdvertListProperties searchProperties) {
        this.searchProperties = searchProperties;
    }

    public boolean shallPerformMatchesSearch() {
        return shallPerformMatchesSearch;
    }

    public void shallPerformMatchesSearch(boolean shallPerformMatchesSearch) {
        this.shallPerformMatchesSearch = shallPerformMatchesSearch;
    }

    public void shallPerformEmptySearch(boolean shallPerformEmptySearch) {
        this.shallPerformEmptySearch = shallPerformEmptySearch;
    }

    public boolean shallPerformEmptySearch() {
        return shallPerformEmptySearch;
    }

}
