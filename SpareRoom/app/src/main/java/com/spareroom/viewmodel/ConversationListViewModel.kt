package com.spareroom.viewmodel

import android.app.Application
import com.spareroom.integration.business.apiv2.IConversationListDao
import com.spareroom.livedata.ConversationListLiveData
import javax.inject.Inject

class ConversationListViewModel @Inject constructor(
    conversationListDao: IConversationListDao,
    application: Application
) : AndroidViewModel(application) {

    val conversationListLiveData = ConversationListLiveData(conversationListDao, disposables)
    var refreshing = false
}
