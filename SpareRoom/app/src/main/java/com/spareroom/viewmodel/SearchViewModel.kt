package com.spareroom.viewmodel

import android.app.Application
import com.spareroom.integration.business.apiv2.ILocationDao
import com.spareroom.livedata.*
import javax.inject.Inject

class SearchViewModel @Inject constructor(
    application: Application,
    locationDao: ILocationDao
) : AndroidViewModel(application) {

    val postCodeLiveData = PostCodeLiveData(locationDao, disposables)

    val searchLiveData = SearchListLiveData(application, disposables)

    val locationLiveData = LocationLiveData(disposables)

    val locationSettingsLiveData = LocationSettingsLiveData(disposables)

}
