package com.spareroom.viewmodel

import android.app.Application
import com.spareroom.livedata.ConversationListRequestsLiveData

import com.spareroom.livedata.SyncDrawer

class MainActivityViewModel(application: Application) : AndroidViewModel(application) {

    val syncDrawer = SyncDrawer(disposables)

    val conversationListRequests = ConversationListRequestsLiveData(disposables)

}
