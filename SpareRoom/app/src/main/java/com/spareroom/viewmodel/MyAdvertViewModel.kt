package com.spareroom.viewmodel

import android.app.Application
import com.spareroom.integration.business.apiv2.IMyAdvertsDao
import com.spareroom.livedata.MyAdvertLiveData
import javax.inject.Inject

class MyAdvertViewModel @Inject constructor(myAdvertsDao: IMyAdvertsDao, app: Application) : AndroidViewModel(app) {

    val myAdvert = MyAdvertLiveData(myAdvertsDao, disposables)
}