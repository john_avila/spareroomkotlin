package com.spareroom.viewmodel

import android.app.Application
import com.spareroom.livedata.DiscardProfileChanges
import com.spareroom.livedata.SaveProfileChanges

class EditProfileActivityViewModel(application: Application) : AndroidViewModel(application) {

    val discardProfileChanges = DiscardProfileChanges(disposables)
    val saveProfileChanges = SaveProfileChanges(disposables)

}
