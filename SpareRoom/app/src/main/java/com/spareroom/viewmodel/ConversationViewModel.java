package com.spareroom.viewmodel;

import android.app.Application;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.business.apiv2.*;
import com.spareroom.livedata.*;
import com.spareroom.model.business.ConversationInfo;
import com.spareroom.ui.util.AdvertUtils;

import javax.inject.Inject;

public class ConversationViewModel extends AndroidViewModel {

    private final FetchConversationLiveData fetchConversation;
    private final OtherUserAdvertsLiveData otherUserAdvertsLiveData;
    private final SendMessageLiveData sendMessage;
    private final EditNewMessageConfirmation editNewMessageConfirmation;
    private final SendNewMessageConfirmation sendNewMessageConfirmation;
    private final DeclineConversationConfirmation declineConversationConfirmation;
    private final DeleteConversationConfirmation deleteConversationConfirmation;
    private final DeclineConversationLiveData declineConversation;
    private final DeleteConversationLiveData deleteConversation;
    private ConversationInfo conversationInfo;

    @Inject
    public ConversationViewModel(Application application, IWantedAdvertsDao wantedAdvertsDao, IOfferedAdvertsDao offeredAdvertsDao,
                                 IConversationDao conversationDao, SpareroomContext spareroomContext, AdvertUtils advertUtils,
                                 INewMessageDao newMessageDao) {
        super(application);

        otherUserAdvertsLiveData = new OtherUserAdvertsLiveData(wantedAdvertsDao, offeredAdvertsDao, getDisposables());
        fetchConversation = new FetchConversationLiveData(conversationDao, wantedAdvertsDao, offeredAdvertsDao, spareroomContext,
                advertUtils, otherUserAdvertsLiveData, getDisposables());
        sendMessage = new SendMessageLiveData(newMessageDao, getDisposables());
        editNewMessageConfirmation = new EditNewMessageConfirmation(getDisposables());
        sendNewMessageConfirmation = new SendNewMessageConfirmation(getDisposables());
        declineConversationConfirmation = new DeclineConversationConfirmation(getDisposables());
        deleteConversationConfirmation = new DeleteConversationConfirmation(getDisposables());
        declineConversation = new DeclineConversationLiveData(newMessageDao, getDisposables());
        deleteConversation = new DeleteConversationLiveData(conversationDao, getDisposables());
    }

    public OtherUserAdvertsLiveData getOtherUserAdvertsLiveData() {
        return otherUserAdvertsLiveData;
    }

    public FetchConversationLiveData getFetchConversation() {
        return fetchConversation;
    }

    public SendMessageLiveData getSendMessage() {
        return sendMessage;
    }

    public EditNewMessageConfirmation getEditNewMessageConfirmation() {
        return editNewMessageConfirmation;
    }

    public SendNewMessageConfirmation getSendNewMessageConfirmation() {
        return sendNewMessageConfirmation;
    }

    public DeclineConversationConfirmation getDeclineConversationConfirmation() {
        return declineConversationConfirmation;
    }

    public DeleteConversationConfirmation getDeleteConversationConfirmation() {
        return deleteConversationConfirmation;
    }

    public DeclineConversationLiveData getDeclineConversation() {
        return declineConversation;
    }

    public DeleteConversationLiveData getDeleteConversation() {
        return deleteConversation;
    }

    public ConversationInfo getConversationInfo() {
        return conversationInfo;
    }

    public void setConversationInfo(ConversationInfo conversationInfo) {
        this.conversationInfo = conversationInfo;
    }

}
