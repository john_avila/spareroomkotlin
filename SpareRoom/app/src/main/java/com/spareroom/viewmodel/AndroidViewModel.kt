package com.spareroom.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel

import io.reactivex.disposables.CompositeDisposable

open class AndroidViewModel(application: Application) : AndroidViewModel(application) {

    val disposables = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}