package com.spareroom.viewmodel

import android.app.Application
import com.spareroom.integration.business.apiv2.*
import com.spareroom.livedata.MyAdvertsListLiveData
import com.spareroom.ui.util.ScrollState
import javax.inject.Inject

class MyAdvertsListViewModel @Inject constructor(
    myAdvertsDao: IMyAdvertsDao,
    wantedAdvertsDao: IWantedAdvertsDao,
    offeredAdvertsDao: IOfferedAdvertsDao,
    app: Application) : AndroidViewModel(app) {

    var firstLoad = true
    val offeredScrollState = ScrollState()
    val wantedScrollState = ScrollState()

    val myAdverts = MyAdvertsListLiveData(myAdvertsDao, wantedAdvertsDao, offeredAdvertsDao, disposables)
}