package com.spareroom.viewmodel;

import android.app.Application;

import com.spareroom.integration.business.apiv2.IAdvertListDao;
import com.spareroom.livedata.SearchAdvertList;

import javax.inject.Inject;

public class ResultsViewModel extends AndroidViewModel {
    private final SearchAdvertList searchAdvertList;

    @Inject
    public ResultsViewModel(Application application, IAdvertListDao advertListDao) {
        super(application);
        searchAdvertList = new SearchAdvertList(advertListDao, getDisposables());
    }

    public SearchAdvertList getSearchAdvertList() {
        return searchAdvertList;
    }

}
