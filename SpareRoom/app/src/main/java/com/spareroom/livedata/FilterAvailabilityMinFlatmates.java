package com.spareroom.livedata;

import com.spareroom.livedata.event.FilterAvailabilityMinFlatmatesEvent;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.MinFlatmates;

import io.reactivex.disposables.CompositeDisposable;

public class FilterAvailabilityMinFlatmates extends LiveDataController<FilterAvailabilityMinFlatmatesEvent> {

    public FilterAvailabilityMinFlatmates(CompositeDisposable disposables) {
        super(new SingleLiveData<>(), disposables);
    }

    public void saveValues(int position, MinFlatmates maxFlatmates) {
        setValue(new FilterAvailabilityMinFlatmatesEvent(position, maxFlatmates));
    }

}
