package com.spareroom.livedata

import android.location.Location
import com.spareroom.model.business.*
import io.reactivex.disposables.CompositeDisposable

class LocationLiveData(
    disposables: CompositeDisposable
) : LiveDataController<LocationResponse>(SingleLiveData(), disposables) {

    fun location(location: Location) {
        value = SuccessfulLocation(location)
    }

    fun failed() {
        value = FailedLocation()
    }
}
