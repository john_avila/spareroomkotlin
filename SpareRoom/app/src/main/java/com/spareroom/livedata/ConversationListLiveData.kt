package com.spareroom.livedata

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.DiffUtil
import com.spareroom.integration.analytics.fabric.AnalyticsTagsFabric
import com.spareroom.integration.business.apiv2.IConversationListDao
import com.spareroom.integration.webservice.exception.NetworkConnectivityException
import com.spareroom.integration.webservice.exception.NotLoggedInException
import com.spareroom.lib.util.paging.PageLoader
import com.spareroom.model.business.*
import io.reactivex.disposables.CompositeDisposable

private const val CONVERSATION_NOT_FOUND = -1
private const val MAX_PER_PAGE = 50

class ConversationListLiveData(
    private val conversationListDao: IConversationListDao,
    disposables: CompositeDisposable
) : LiveDataController<ConversationListResponse>(MutableLiveData(), disposables) {

    var hasMoreItems = false
        private set

    private val conversationList = mutableListOf<ConversationPreview>()
    private val pagingHelper = PageLoader<ConversationPreview, ConversationPreview, ConversationListRestResponse>(
        currentItems = conversationList,
        maxPerPage = MAX_PER_PAGE,
        mergeWithPreviousPages = true,
        transformation = { items -> items })

    fun retrieveConversationList(firstPage: Boolean) {
        execute(
            task = { retrieveConversationListTask(firstPage) },
            failureResponse = { FailedConversationListResponse(throwable = it) })
    }

    fun deleteItem(conversationId: String) {
        executeTask(conversationId) { deleteItemTask(it) }
    }

    fun updateConversation(conversationId: String, snippet: String, dateLastActivity: String, direction: String) {
        executeTask(conversationId) { updateConversationTask(it, snippet, dateLastActivity, direction) }
    }

    fun setConnectionError() {
        value = FailedConversationListResponse(NetworkConnectivityException())
    }

    private inline fun executeTask(conversationId: String, crossinline task: (position: Int) -> ConversationListResponse) {
        val position = conversationList.indexOfFirst { it.threadId == conversationId }
        if (position == CONVERSATION_NOT_FOUND)
            return

        execute(
            task = { task(position) },
            failureResponse = { FailedConversationListResponse(throwable = it) })
    }

    private fun deleteItemTask(position: Int): ConversationListResponse {
        val newConversationList = conversationList.toMutableList()

        newConversationList.removeAt(position)

        val diff = diff(newConversationList)
        return if (newConversationList.isEmpty()) {
            EmptyConversationListResponse(diff, newConversationList)
        } else {
            ConversationDeletedResponse(diff, newConversationList)
        }
    }

    private fun updateConversationTask(position: Int, snippet: String, dateLastActivity: String, direction: String): ConversationListResponse {
        val newConversationList = conversationList.toMutableList()

        newConversationList[position] =
            newConversationList[position].copy(snippetField = snippet, dateLastActivityField = dateLastActivity, direction = direction)
        newConversationList[position].setRead()

        val diff = diff(newConversationList)
        return ConversationReadResponse(diff, newConversationList)
    }

    private fun retrieveConversationListTask(firstPage: Boolean): ConversationListResponse {
        return trackTaskDuration(AnalyticsTagsFabric.Event.CONVERSATION_LIST_LOADED) {
            val pageResponse =
                pagingHelper.loadPage(firstPage) { offset, maxPerPage -> conversationListDao.getConversationList(offset, maxPerPage) }

            hasMoreItems = pageResponse.hasMoreItems

            val response = pageResponse.page
            return when {
                response.success && response.totalConversations == 0 -> emptySuccess(firstPage, response)
                response.success -> success(firstPage, response, pageResponse.hadDuplicates)
                response.errorLoggedOut -> throw NotLoggedInException()
                else -> throw Exception(response.errorMessage)
            }
        }
    }

    private fun emptySuccess(firstPage: Boolean, response: ConversationListRestResponse): ConversationListResponse {
        val newList = createNewList(firstPage, response)
        val diff = diff(newList)
        return EmptyConversationListResponse(diff, newList)
    }

    private fun success(firstPage: Boolean, response: ConversationListRestResponse, hadDuplicates: Boolean): ConversationListResponse {
        val newList = createNewList(firstPage, response)
        val diff = diff(newList)
        return if (firstPage) FirstPageConversationList(diff, newList) else NextPageConversationList(diff, newList, hadDuplicates)
    }

    private fun createNewList(firstPage: Boolean, response: ConversationListRestResponse): List<ConversationPreview> {
        val newList: List<ConversationPreview>
        if (firstPage) {
            newList = response.list
        } else {
            newList = mutableListOf()
            newList.apply {
                addAll(conversationList)
                addAll(response.list)
            }
        }
        return newList
    }

    private fun diff(newList: List<ConversationPreview>): DiffUtil.DiffResult {
        val diff = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldConversation = conversationList[oldItemPosition]
                val newConversation = newList[newItemPosition]

                return oldConversation.threadId == newConversation.threadId
            }

            override fun getOldListSize() = conversationList.size

            override fun getNewListSize() = newList.size

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldConversation = conversationList[oldItemPosition]
                val newConversation = newList[newItemPosition]

                return oldConversation == newConversation
            }
        })

        conversationList.apply {
            clear()
            addAll(newList)
        }

        return diff
    }

}
