package com.spareroom.livedata

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.DiffUtil
import com.spareroom.integration.analytics.AnalyticsTrackerComposite
import com.spareroom.integration.analytics.fabric.AnalyticsTagsFabric
import com.spareroom.integration.business.apiv2.*
import com.spareroom.integration.webservice.exception.NetworkConnectivityException
import com.spareroom.integration.webservice.exception.NotLoggedInException
import com.spareroom.lib.util.paging.PageLoader
import com.spareroom.model.business.*
import com.spareroom.ui.util.AdvertsDiffUtil
import io.reactivex.disposables.CompositeDisposable

private const val MAX_PER_PAGE = 20
private const val ADVERT_NOT_FOUND = -1

class MyAdvertsListLiveData(
    private val myAdvertsDao: IMyAdvertsDao,
    private val wantedAdvertsDao: IWantedAdvertsDao,
    private val offeredAdvertsDao: IOfferedAdvertsDao,
    disposables: CompositeDisposable
) : LiveDataController<MyAdvertsListResponse>(MutableLiveData(), disposables) {

    private val wantedAdverts = mutableListOf<AdvertItem>()
    private val offeredAdverts = mutableListOf<AdvertItem>()
    private val pagingHelper = PageLoader<AbstractAd, AdvertItem, MyAdverts>(
        currentItems = offeredAdverts,
        maxPerPage = MAX_PER_PAGE,
        mergeWithPreviousPages = true,
        transformation = { items -> toAdvertItems(items) })

    var hasMoreItems = false
        private set

    fun getAdverts(firstPage: Boolean) {
        execute(task = { getAdvertsTask(firstPage) }, failureResponse = { FailedToLoadMyAdvertsList(throwable = it) })
    }

    fun updateAdvert(advertId: String, advertInfo: AdvertInfo?, offered: Boolean) {
        val adverts = if (offered) offeredAdverts else wantedAdverts
        val position = adverts.indexOfFirst { it.advert.id == advertId }
        if (position != ADVERT_NOT_FOUND)
            execute(task = { updateAdvertTask(advertId, advertInfo, offered, position) }, failureResponse = { FailedToLoadMyAdvertsList(throwable = it) })
    }

    fun emitNoNetworkError() {
        value = FailedToLoadMyAdvertsList(NetworkConnectivityException())
    }

    private fun getAdvertsTask(firstPage: Boolean): MyAdvertsListResponse {
        return trackTaskDuration(AnalyticsTagsFabric.Event.MY_ADVERTS_LOADED) {

            val pageResponse = pagingHelper.loadPage(firstPage) { offset, maxPerPage -> myAdvertsDao.getAdverts(offset, maxPerPage) }

            hasMoreItems = pageResponse.hasMoreItems

            val myAdverts = pageResponse.page
            if (!myAdverts.success)
                throw if (!myAdverts.loggedIn) NotLoggedInException() else Exception()

            val newOfferedAdverts = groupAdverts(pageResponse.items)
            val newWantedAdverts = groupAdverts(toAdvertItems(myAdverts.wanted))

            if (firstPage) {
                MyAdvertsListFirstPage(
                    wantedAdverts = newWantedAdverts,
                    wantedAdvertsDiff = advertsDiff(wantedAdverts, newWantedAdverts),
                    offeredAdverts = newOfferedAdverts,
                    offeredAdvertsDiff = advertsDiff(offeredAdverts, newOfferedAdverts))
            } else {
                MyAdvertsListNextPage(
                    wantedAdverts = newWantedAdverts,
                    wantedAdvertsDiff = advertsDiff(wantedAdverts, newWantedAdverts),
                    offeredAdverts = newOfferedAdverts,
                    offeredAdvertsDiff = advertsDiff(offeredAdverts, newOfferedAdverts))
            }
        }
    }

    private fun updateAdvertTask(advertId: String, advertInfo: AdvertInfo?, offered: Boolean, position: Int): MyAdvertsListResponse {
        return trackTaskDuration(AnalyticsTagsFabric.Event.SINGLE_ADVERT_IN_MY_ADVERTS_LOADED) {
            val newAdverts = copyAdverts(if (offered) offeredAdverts else wantedAdverts)

            if (advertInfo != null)
                updateAdvertInfo(advertInfo, offered, position, newAdverts)

            updateAdvertFromNetworkCall(advertId, offered, position, newAdverts)

            val newWantedAdverts = groupAdverts(if (offered) wantedAdverts else newAdverts)
            val newOfferedAdverts = groupAdverts(if (offered) newAdverts else offeredAdverts)

            MyAdvertUpdated(
                wantedAdverts = newWantedAdverts,
                wantedAdvertsDiff = advertsDiff(wantedAdverts, newWantedAdverts),
                offeredAdverts = newOfferedAdverts,
                offeredAdvertsDiff = advertsDiff(offeredAdverts, newOfferedAdverts))
        }
    }

    private fun copyAdverts(adverts: List<AdvertItem>): MutableList<AdvertItem> {
        val newAdverts = mutableListOf<AdvertItem>()
        for (advert in adverts) {
            newAdverts.add(advert.copy())
        }

        return newAdverts
    }

    private fun updateAdvertInfo(advertInfo: AdvertInfo, offered: Boolean, position: Int, newAdverts: MutableList<AdvertItem>) {
        newAdverts[position] = AdvertItem(advert = copyAdvert(newAdverts[position].advert, advertInfo, offered))
    }

    private fun updateAdvertFromNetworkCall(advertId: String, offered: Boolean, position: Int, newAdverts: MutableList<AdvertItem>) {
        try {
            val advert = fetchAdvert(advertId, offered) ?: throw Exception("Fetched adverts is null")
            newAdverts[position] = AdvertItem(advert = copyAdvert(advert, AdvertInfo.fromAdvert(newAdverts[position].advert), offered))
        } catch (exception: Exception) {
            AnalyticsTrackerComposite.getInstance().logHandledException("updateAdvertFromNetworkCall", exception)
        }
    }

    private fun fetchAdvert(advertId: String, offered: Boolean): AbstractAd? {
        return (if (offered) offeredAdvertsDao else wantedAdvertsDao).getAdvert(advertId).advert
    }

    private fun copyAdvert(advert: AbstractAd, advertInfo: AdvertInfo, offered: Boolean): AbstractAd {
        return if (offered) {
            copyOfferedAdvert(advert as AdOffered, advertInfo)
        } else {
            copyWantedAdvert(advert as AdWanted, advertInfo)
        }
    }

    private fun copyWantedAdvert(advert: AdWanted, advertInfo: AdvertInfo) = advert.copy(
        statusField = advertInfo.status,
        upgradeDaysLeftField = advertInfo.upgradeDaysLeft,
        dateLastRenewedField = advertInfo.dateLastRenewed,
        dateLastLiveField = advertInfo.dateLastLive,
        upgradeAppliedField = advertInfo.upgradeApplied,
        datePlacedField = advertInfo.datePlaced)

    private fun copyOfferedAdvert(advert: AdOffered, advertInfo: AdvertInfo) = advert.copy(
        statusField = advertInfo.status,
        upgradeDaysLeftField = advertInfo.upgradeDaysLeft,
        dateLastRenewedField = advertInfo.dateLastRenewed,
        dateLastLiveField = advertInfo.dateLastLive,
        upgradeAppliedField = advertInfo.upgradeApplied,
        datePlacedField = advertInfo.datePlaced)

    private fun toAdvertItems(adverts: List<AbstractAd>) = adverts.map { AdvertItem(it) }

    private fun groupAdverts(newAdvertItems: List<AdvertItem>): List<AdvertItem> {
        val liveAdverts = mutableListOf<AdvertItem>()
        val pendingAdverts = mutableListOf<AdvertItem>()
        val deactivatedAdverts = mutableListOf<AdvertItem>()

        for (advertItem in newAdvertItems) {
            advertItem.header = false
            advertItem.divider = false

            when {
                advertItem.advert.isLive -> liveAdverts.add(advertItem)
                advertItem.advert.isPending -> pendingAdverts.add(advertItem)
                else -> deactivatedAdverts.add(advertItem)
            }
        }

        val allAdverts = mutableListOf<AdvertItem>()

        if (liveAdverts.isNotEmpty()) {
            liveAdverts[0].header = true
            liveAdverts[0].divider = false
            allAdverts.addAll(liveAdverts)
        }

        if (pendingAdverts.isNotEmpty()) {
            pendingAdverts[0].header = true
            pendingAdverts[0].divider = allAdverts.isNotEmpty()
            allAdverts.addAll(pendingAdverts)
        }

        if (deactivatedAdverts.isNotEmpty()) {
            deactivatedAdverts[0].header = true
            deactivatedAdverts[0].divider = allAdverts.isNotEmpty()
            allAdverts.addAll(deactivatedAdverts)
        }

        return allAdverts
    }

    private fun advertsDiff(oldAdverts: MutableList<AdvertItem>, newAdverts: List<AdvertItem>): DiffUtil.DiffResult {
        val diff = AdvertsDiffUtil.diff(oldAdverts, newAdverts)
        oldAdverts.clear()
        oldAdverts.addAll(newAdverts)
        return diff
    }

}
