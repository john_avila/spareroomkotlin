package com.spareroom.livedata

import com.spareroom.livedata.event.SaveProfileChangesEvent
import io.reactivex.disposables.CompositeDisposable

class SaveProfileChanges(disposables: CompositeDisposable) : LiveDataController<SaveProfileChangesEvent>(SingleLiveData(), disposables) {

    fun save() {
        value = SaveProfileChangesEvent()
    }

}
