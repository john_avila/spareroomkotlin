package com.spareroom.livedata;

import com.spareroom.livedata.event.FilterAvailabilityMaxTermEvent;
import com.spareroom.model.business.SearchAdvertListProperties.MaxTerm;

import io.reactivex.disposables.CompositeDisposable;

public class FilterAvailabilityMaxTerm extends LiveDataController<FilterAvailabilityMaxTermEvent> {

    public FilterAvailabilityMaxTerm(CompositeDisposable disposables) {
        super(new SingleLiveData<>(), disposables);
    }

    public void saveValues(int position, MaxTerm minTerm) {
        setValue(new FilterAvailabilityMaxTermEvent(position, minTerm));
    }

}
