package com.spareroom.livedata;

import com.spareroom.livedata.event.FilterAvailabilityMaxFlatmatesEvent;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.MaxFlatmates;

import io.reactivex.disposables.CompositeDisposable;

public class FilterAvailabilityMaxFlatmates extends LiveDataController<FilterAvailabilityMaxFlatmatesEvent> {

    public FilterAvailabilityMaxFlatmates(CompositeDisposable disposables) {
        super(new SingleLiveData<>(), disposables);
    }

    public void saveValues(int position, MaxFlatmates maxFlatmates) {
        setValue(new FilterAvailabilityMaxFlatmatesEvent(position, maxFlatmates));
    }

}
