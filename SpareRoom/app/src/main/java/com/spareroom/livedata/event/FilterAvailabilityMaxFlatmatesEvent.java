package com.spareroom.livedata.event;

import com.spareroom.model.business.SearchAdvertListPropertiesOffered.MaxFlatmates;

public class FilterAvailabilityMaxFlatmatesEvent {
    private int position;
    private MaxFlatmates maxFlatmates;

    public FilterAvailabilityMaxFlatmatesEvent(int position, MaxFlatmates maxFlatmates) {
        this.position = position;
        this.maxFlatmates = maxFlatmates;
    }

    public MaxFlatmates maxFlatmates() {
        return maxFlatmates;
    }

    public int position() {
        return position;
    }

}
