package com.spareroom.livedata;

import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.business.apiv2.INewMessageDao;
import com.spareroom.model.business.*;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class DeclineConversationLiveData extends LiveDataController<DeclineConversationResponse> {

    private final INewMessageDao messageDao;

    public DeclineConversationLiveData(INewMessageDao messageDao, CompositeDisposable disposables) {
        super(new SingleLiveData<>(), disposables);
        this.messageDao = messageDao;
    }

    public void decline(String lastMessageId, String declineTemplate) {
        add(declineConversationTask(lastMessageId, declineTemplate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(declineConversationObserver()));
    }

    private Single<DeclineConversationResponse> declineConversationTask(final String lastMessageId, final String declineTemplate) {
        return Single.fromCallable(() -> {

            // get decline message
            NewMessageResponse messageResponse = messageDao.getDeclineMessageText(lastMessageId, declineTemplate);

            // decline the conversation
            NewMessageResponse declineResponse = messageDao.decline(lastMessageId, messageResponse.getMessageSent(), declineTemplate);

            // update response
            DeclineConversationResponse response = new DeclineConversationResponse();
            AnalyticsTrackerComposite.getInstance().trackEvent_Message_Send(messageResponse.toSpareroomStatus());
            if (SpareroomStatusCode.CODE_SUCCESS.equalsIgnoreCase(declineResponse.getStatusCode())) {
                response.setResponse(messageResponse);
                return response;
            }

            response.setError(messageResponse.getStatusCode(), messageResponse.getResponse());

            return response;
        });
    }

    private DisposableSingleObserver<DeclineConversationResponse> declineConversationObserver() {
        return new DisposableSingleObserver<DeclineConversationResponse>() {

            @Override
            public void onSuccess(DeclineConversationResponse response) {
                setValue(response);
            }

            @Override
            public void onError(Throwable throwable) {
                DeclineConversationResponse response = new DeclineConversationResponse();
                response.setThrowable(throwable);
                setValue(response);
            }
        };
    }
}
