package com.spareroom.livedata;

import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.business.apiv2.INewMessageDao;
import com.spareroom.model.business.*;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SendMessageLiveData extends LiveDataController<MessageResponse> {

    private final INewMessageDao messageDao;

    public SendMessageLiveData(INewMessageDao messageDao, CompositeDisposable disposables) {
        super(new SingleLiveData<>(), disposables);
        this.messageDao = messageDao;
    }

    public void sendNewMessage(NewMessage newMessage) {
        add(newMessageTask(newMessage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(newMessageObserver()));
    }

    private Single<MessageResponse> newMessageTask(final NewMessage newMessage) {
        return Single.fromCallable(() -> {
            NewMessageResponse newMessageResponse = messageDao.sendNewMessage(newMessage);

            MessageResponse response = new MessageResponse();
            AnalyticsTrackerComposite.getInstance().trackEvent_Message_Send(newMessageResponse.toSpareroomStatus());

            if (SpareroomStatusCode.CODE_SUCCESS.equalsIgnoreCase(newMessageResponse.getStatusCode())) {
                response.setResponse(newMessageResponse);
                return response;
            }

            response.setError(newMessageResponse.getStatusCode(), newMessageResponse.getResponse());

            return response;
        });
    }

    private DisposableSingleObserver<MessageResponse> newMessageObserver() {
        return new DisposableSingleObserver<MessageResponse>() {

            @Override
            public void onSuccess(MessageResponse response) {
                setValue(response);
            }

            @Override
            public void onError(Throwable throwable) {
                MessageResponse message = new MessageResponse();
                message.setThrowable(throwable);
                setValue(message);
            }
        };
    }
}
