package com.spareroom.livedata;

import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.business.apiv2.*;
import com.spareroom.model.business.*;
import com.spareroom.ui.util.*;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.DiffUtil;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class FetchConversationLiveData extends LiveDataController<ConversationResponse> {

    private final IConversationDao conversationDao;
    private final IWantedAdvertsDao wantedAdvertsDao;
    private final IOfferedAdvertsDao offeredAdvertsDao;
    private final SpareroomContext spareroomContext;
    private final AdvertUtils advertUtils;
    private final OtherUserAdvertsLiveData otherUserAdvertsLiveData;

    private final List<Message> messages = new ArrayList<>();

    public FetchConversationLiveData(IConversationDao conversationDao, IWantedAdvertsDao wantedAdvertsDao, IOfferedAdvertsDao offeredAdvertsDao,
                                     SpareroomContext spareroomContext, AdvertUtils advertUtils, OtherUserAdvertsLiveData otherUserAdvertsLiveData,
                                     CompositeDisposable disposables) {

        super(new MutableLiveData<>(), disposables);
        this.otherUserAdvertsLiveData = otherUserAdvertsLiveData;
        this.conversationDao = conversationDao;
        this.wantedAdvertsDao = wantedAdvertsDao;
        this.offeredAdvertsDao = offeredAdvertsDao;
        this.spareroomContext = spareroomContext;
        this.advertUtils = advertUtils;
    }

    public void loadConversation(GetConversationRequest getConversationRequest) {
        add(fetchConversationTask(getConversationRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(fetchConversationObserver()));
    }

    private Single<ConversationResponse> fetchConversationTask(final GetConversationRequest getConversationRequest) {
        return Single.fromCallable(() -> {
            long start = System.currentTimeMillis();

            Conversation conversation = conversationDao.getConversation(getConversationRequest);

            if (!conversation.isSuccessful()) {
                ConversationResponse conversationResponse = new ConversationResponse();
                conversationResponse.setError(conversation.errorCode(), conversation.errorMessage());
                return conversationResponse;
            }

            String otherUserId = getConversationRequest.getOtherUserId();
            String regardingAdvertId = getConversationRequest.getAdvertId();
            boolean showOtherUserOfferedAdverts = showOtherUserOfferedAdverts(conversation, otherUserId);

            // get advert this conversation is regarding
            AbstractAd regardingAd = getAdvert(regardingAdvertId, conversation.isAdvertWanted());

            // get other user advert
            AbstractAd otherUserAdvert = getOtherUserAdvert(conversation, otherUserId, regardingAd, showOtherUserOfferedAdverts);
            addHeaderMessages(conversation, otherUserAdvert);

            ConversationResponse conversationResponse = new ConversationResponse();
            conversationResponse.setResponse(conversation);
            conversationResponse.setMessagesDiffResult(calculateDifferenceInMessages(conversation.getMessages()));

            String otherUser = StringUtils.getFirstString(conversation.getOtherUserFirstNames());
            otherUserAdvertsLiveData.init(otherUserId, conversation.getOtherUserOfferedAdvertCount(), conversation.getOtherUserWantedAdvertCount(),
                    regardingAd, showOtherUserOfferedAdverts, otherUser, regardingAdvertId);

            AnalyticsTrackerComposite.getInstance().trackSingleConversationLoadedEvent(DateUtils.getDuration(start));

            return conversationResponse;
        });
    }

    boolean showOtherUserOfferedAdverts(@NonNull Conversation conversation, String otherUserId) {
        boolean otherUserHasOfferedAds = conversation.getOtherUserOfferedAdvertCount() > 0;
        boolean otherUserDoesNotHaveWantedAds = conversation.getOtherUserWantedAdvertCount() <= 0;
        boolean isAdvertOffered = conversation.isAdvertOffered();
        boolean isAdvertWanted = !isAdvertOffered;
        boolean isPlacedByOtherUser = isAdvertPlacedByOtherUser(otherUserId, conversation.getAdvertPlacedBy());
        boolean isPlacedByMe = !isPlacedByOtherUser;

        return (isAdvertOffered && isPlacedByOtherUser && otherUserHasOfferedAds)
                || (isAdvertOffered && isPlacedByMe && otherUserDoesNotHaveWantedAds)
                || (isAdvertWanted && isPlacedByMe && otherUserHasOfferedAds)
                || (isAdvertWanted && isPlacedByOtherUser && otherUserDoesNotHaveWantedAds);
    }

    private boolean isAdvertPlacedByOtherUser(String otherUserId, String placedBy) {
        return StringUtils.equals(otherUserId, placedBy) && !StringUtils.equals(placedBy, spareroomContext.getUserId());
    }

    private void addHeaderMessages(Conversation conversation, AbstractAd ad) {
        List<Message> messages = conversation.getMessages();
        messages.add(0, new HeaderMessageItem(ad, conversation, advertUtils));

        if (conversation.isClosed())
            messages.add(1, new ConversationClosedMessageItem());
    }

    private AbstractAd getOtherUserAdvert(Conversation conversation, String otherUserId, AbstractAd regardingAd, boolean offered) {
        // if conversation is closed - other user advert will not be used anywhere
        if (conversation.isClosed())
            return null;

        // this means that our regarding ad is also the ad that this user should see in header
        if (isAdvertPlacedByOtherUser(otherUserId, conversation.getAdvertPlacedBy()))
            return regardingAd;

        boolean wanted = !offered;

        try {
            List<? extends AbstractAd> ads = null;
            if (offered && conversation.getOtherUserOfferedAdvertCount() == 1) {
                ads = getAdverts(offeredAdvertsDao, otherUserId);
            } else if (wanted && conversation.getOtherUserWantedAdvertCount() == 1) {
                ads = getAdverts(wantedAdvertsDao, otherUserId);
            }

            return ads != null && !ads.isEmpty() ? ads.get(0) : null;

        } catch (Exception e) {
            return null;
        }
    }

    private List<? extends AbstractAd> getAdverts(IAdvertsDao<?> advertsDao, String otherUserId) throws Exception {
        return advertsDao.getUserAdverts(otherUserId, 1, 1).getAdverts();
    }

    private AbstractAd getAdvert(String advertId, boolean wanted) throws Exception {
        return (wanted ? wantedAdvertsDao : offeredAdvertsDao).getAdvert(advertId).getAdvert();
    }

    private DiffUtil.DiffResult calculateDifferenceInMessages(final List<Message> newMessages) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return messages.size();
            }

            @Override
            public int getNewListSize() {
                return newMessages.size();
            }

            @Override
            public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                Message oldMessage = messages.get(oldItemPosition);
                Message newMessage = newMessages.get(newItemPosition);

                return StringUtils.equals(oldMessage.getId(), newMessage.getId());
            }

            @Override
            public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                Message oldMessage = messages.get(oldItemPosition);
                Message newMessage = newMessages.get(newItemPosition);

                return oldMessage.equals(newMessage);
            }

        });

        messages.clear();
        messages.addAll(newMessages);

        return diffResult;
    }

    private DisposableSingleObserver<ConversationResponse> fetchConversationObserver() {
        return new DisposableSingleObserver<ConversationResponse>() {

            @Override
            public void onSuccess(ConversationResponse value) {
                setValue(value);
            }

            @Override
            public void onError(Throwable throwable) {
                AnalyticsTrackerComposite.getInstance().logHandledException(FetchConversationLiveData.class.getSimpleName(), throwable);
                final ConversationResponse conversationResponse = new ConversationResponse();
                conversationResponse.setThrowable(throwable);
                setValue(conversationResponse);
            }
        };
    }

    public List<Message> messages() {
        return messages;
    }
}
