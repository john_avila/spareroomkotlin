package com.spareroom.livedata;

import com.spareroom.livedata.event.EditNewMessageConfirmationEvent;

import io.reactivex.disposables.CompositeDisposable;

public class EditNewMessageConfirmation extends LiveDataController<EditNewMessageConfirmationEvent> {

    public EditNewMessageConfirmation(CompositeDisposable disposables) {
        super(new SingleLiveData<>(), disposables);
    }

    public void edit() {
        setValue(new EditNewMessageConfirmationEvent());
    }

}
