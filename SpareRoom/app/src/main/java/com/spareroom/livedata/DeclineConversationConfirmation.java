package com.spareroom.livedata;

import com.spareroom.livedata.event.DeclineConversationConfirmationEvent;

import io.reactivex.disposables.CompositeDisposable;

public class DeclineConversationConfirmation extends LiveDataController<DeclineConversationConfirmationEvent> {

    public DeclineConversationConfirmation(CompositeDisposable disposables) {
        super(new SingleLiveData<>(), disposables);
    }

    public void decline() {
        setValue(new DeclineConversationConfirmationEvent());
    }

}
