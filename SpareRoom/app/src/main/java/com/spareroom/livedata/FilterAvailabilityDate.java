package com.spareroom.livedata;

import com.spareroom.livedata.event.FilterAvailabilityDateEvent;

import io.reactivex.disposables.CompositeDisposable;

public class FilterAvailabilityDate extends LiveDataController<FilterAvailabilityDateEvent> {

    public FilterAvailabilityDate(CompositeDisposable disposables) {
        super(new SingleLiveData<>(), disposables);
    }

    public void saveValues(int position, String date) {
        setValue(new FilterAvailabilityDateEvent(position, date));
    }

}
