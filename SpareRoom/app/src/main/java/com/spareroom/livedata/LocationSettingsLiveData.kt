package com.spareroom.livedata

import com.spareroom.model.business.*
import io.reactivex.disposables.CompositeDisposable

class LocationSettingsLiveData(
    disposables: CompositeDisposable
) : LiveDataController<LocationSettingsCheckResponse>(SingleLiveData(), disposables) {

    fun successful() {
        value = SuccessfulLocationSettingsCheck()
    }

    fun denied() {
        value = FailedLocationSettingsCheck()
    }

}
