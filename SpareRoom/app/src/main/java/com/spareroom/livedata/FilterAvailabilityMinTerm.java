package com.spareroom.livedata;

import com.spareroom.livedata.event.FilterAvailabilityMinTermEvent;
import com.spareroom.model.business.SearchAdvertListProperties.MinTerm;

import io.reactivex.disposables.CompositeDisposable;

public class FilterAvailabilityMinTerm extends LiveDataController<FilterAvailabilityMinTermEvent> {

    public FilterAvailabilityMinTerm(CompositeDisposable disposables) {
        super(new SingleLiveData<>(), disposables);
    }

    public void saveValues(int position, MinTerm minTerm) {
        setValue(new FilterAvailabilityMinTermEvent(position, minTerm));
    }

}
