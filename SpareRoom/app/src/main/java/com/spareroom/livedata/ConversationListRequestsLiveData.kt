package com.spareroom.livedata

import com.spareroom.model.business.*
import io.reactivex.disposables.CompositeDisposable

class ConversationListRequestsLiveData(
    disposables: CompositeDisposable
) : LiveDataController<ConversationsListRequest>(SingleLiveData(), disposables) {

    fun refreshList() {
        value = RefreshConversationList
    }

    fun openConversation(conversationInfo: ConversationInfo) {
        value = OpenConversation(conversationInfo)
    }

}