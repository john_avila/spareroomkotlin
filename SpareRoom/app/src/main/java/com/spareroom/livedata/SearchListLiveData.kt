package com.spareroom.livedata

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.DiffUtil
import com.spareroom.R
import com.spareroom.model.business.*
import io.reactivex.disposables.CompositeDisposable

class SearchListLiveData(
    private val application: Application,
    disposables: CompositeDisposable
) : LiveDataController<SearchResponse>(MutableLiveData(), disposables) {

    private val searchList = mutableListOf<SearchListItem>()

    fun initList(newList: List<SearchListItem>) {
        execute(
            task = { NewListSearch(diff(newList), newList) },
            failureResponse = { FailedNewListSearch(throwable = it) })
    }

    fun setAsSearching(searching: Boolean) {
        execute(
            task = { setAsSearchingTask(searching) },
            failureResponse = { FailedNewListSearch(throwable = it) })
    }

    private fun setAsSearchingTask(searching: Boolean): SearchResponse {
        val newList = searchList.toMutableList()
        val searchingItem = SearchListItem(
            title = application.getString(R.string.nearby),
            iconId = R.drawable.ic_nearby,
            searchItemType = SearchItemType.LOCATION,
            enabledField = !searching,
            loading = searching)

        newList[0] = searchingItem
        return NewListSearch(diff(newList), newList)
    }

    private fun diff(newList: List<SearchListItem>): DiffUtil.DiffResult {
        val diff = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldItem = searchList[oldItemPosition]
                val newItem = newList[newItemPosition]

                return oldItem.title == newItem.title
            }

            override fun getOldListSize() = searchList.size

            override fun getNewListSize() = newList.size

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldItem = searchList[oldItemPosition]
                val newItem = newList[newItemPosition]

                return oldItem == newItem
            }
        })

        searchList.apply {
            clear()
            addAll(newList)
        }

        return diff
    }

}
