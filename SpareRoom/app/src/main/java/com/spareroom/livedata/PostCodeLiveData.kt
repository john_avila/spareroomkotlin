package com.spareroom.livedata

import com.spareroom.integration.analytics.fabric.AnalyticsTagsFabric
import com.spareroom.integration.business.apiv2.ILocationDao
import com.spareroom.model.business.*
import io.reactivex.disposables.CompositeDisposable

class PostCodeLiveData(
    private val locationDao: ILocationDao,
    disposables: CompositeDisposable
) : LiveDataController<SearchPostCodeResponse>(SingleLiveData(), disposables) {

    fun findPostCode(latitude: Double, longitude: Double) {
        execute(
            task = { findPostCodeTask(latitude, longitude) },
            failureResponse = { FailedPostCodeSearch(throwable = it) }
        )
    }

    private fun findPostCodeTask(latitude: Double, longitude: Double): SearchPostCodeResponse {
        return trackTaskDuration(AnalyticsTagsFabric.Event.RETRIEVE_POSTCODE_BY_COORDINATES) {
            val response = locationDao.getPostCode(latitude, longitude)

            return when {
                response.failed -> throw Exception()
                else -> SuccessfulPostCodeSearch(response.postcode)
            }
        }
    }

}