package com.spareroom.livedata

import com.spareroom.integration.analytics.fabric.AnalyticsTagsFabric
import com.spareroom.integration.business.apiv2.IMyAdvertsDao
import com.spareroom.integration.webservice.exception.NotEnoughListingsException
import com.spareroom.integration.webservice.exception.NotLoggedInException
import com.spareroom.model.business.*
import io.reactivex.disposables.CompositeDisposable

class MyAdvertLiveData(
    private val myAdvertsDao: IMyAdvertsDao,
    disposables: CompositeDisposable
) : LiveDataController<MyAdvertResponse>(SingleLiveData(), disposables) {

    var advert: AbstractAd? = null
        private set

    fun renew(advertId: String, offered: Boolean) {
        execute(task = { renewTask(advertId, offered) }, failureResponse = { FailedToRenew(throwable = it) })
    }

    fun activate(advertId: String, offered: Boolean) {
        execute(task = { activateTask(advertId, offered) }, failureResponse = { FailedToActivate(throwable = it) })
    }

    fun deactivate(advertId: String, offered: Boolean) {
        execute(task = { deactivateTask(advertId, offered) }, failureResponse = { FailedToDeactivate(throwable = it) })
    }

    private fun renewTask(advertId: String, offered: Boolean): MyAdvertResponse {
        return trackTaskDuration(AnalyticsTagsFabric.Event.ADVERT_RENEWED) {
            val myAdverts = myAdvertsDao.renewAdvert(advertId, offered)
            throwOnFailure(myAdverts)
            AdvertRenewed(advert(myAdverts, offered))
        }
    }

    private fun activateTask(advertId: String, offered: Boolean): MyAdvertResponse {
        return trackTaskDuration(AnalyticsTagsFabric.Event.ADVERT_ACTIVATED) {
            val myAdverts = myAdvertsDao.activateAdvert(advertId, offered)
            throwOnFailure(myAdverts)
            AdvertActivated(advert(myAdverts, offered))
        }
    }

    private fun deactivateTask(advertId: String, offered: Boolean): MyAdvertResponse {
        return trackTaskDuration(AnalyticsTagsFabric.Event.ADVERT_DEACTIVATED) {
            val myAdverts = myAdvertsDao.deactivateAdvert(advertId, offered)
            throwOnFailure(myAdverts)
            AdvertDeactivated(advert(myAdverts, offered))
        }
    }

    private fun throwOnFailure(myAdverts: MyAdverts) {
        if (!myAdverts.success) throw failureException(myAdverts)
    }

    private fun failureException(myAdverts: MyAdverts): Exception {
        return when {
            !myAdverts.loggedIn -> NotLoggedInException(myAdverts.message)
            myAdverts.notEnoughListings -> NotEnoughListingsException(myAdverts.message)
            else -> Exception(myAdverts.message)
        }
    }

    private fun advert(myAdverts: MyAdverts, offered: Boolean): AbstractAd {
        val advert = if (offered) myAdverts.offered.first() else myAdverts.wanted.first()
        this.advert = advert
        return advert
    }
}