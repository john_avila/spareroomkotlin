package com.spareroom.livedata

import com.spareroom.livedata.event.SyncDrawerEvent

import io.reactivex.disposables.CompositeDisposable

class SyncDrawer(disposables: CompositeDisposable) : LiveDataController<SyncDrawerEvent>(SingleLiveData(), disposables) {

    fun sync() {
        value = SyncDrawerEvent()
    }

}
