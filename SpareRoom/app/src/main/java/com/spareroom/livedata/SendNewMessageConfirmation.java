package com.spareroom.livedata;

import com.spareroom.livedata.event.SendNewMessageConfirmationEvent;

import io.reactivex.disposables.CompositeDisposable;

public class SendNewMessageConfirmation extends LiveDataController<SendNewMessageConfirmationEvent> {

    public SendNewMessageConfirmation(CompositeDisposable disposables) {
        super(new SingleLiveData<>(), disposables);
    }

    public void send() {
        setValue(new SendNewMessageConfirmationEvent());
    }

}
