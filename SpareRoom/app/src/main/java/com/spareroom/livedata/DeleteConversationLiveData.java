package com.spareroom.livedata;

import com.spareroom.integration.business.apiv2.IConversationDao;
import com.spareroom.model.business.DeleteConversationRequest;
import com.spareroom.model.business.DeleteConversationResponse;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class DeleteConversationLiveData extends LiveDataController<DeleteConversationResponse> {

    private final IConversationDao conversationDao;

    public DeleteConversationLiveData(IConversationDao conversationDao, CompositeDisposable disposables) {
        super(new SingleLiveData<>(), disposables);
        this.conversationDao = conversationDao;
    }

    public void delete(DeleteConversationRequest deleteConversationRequest) {
        add(deleteConversationTask(deleteConversationRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(deleteConversationObserver()));
    }

    private Single<DeleteConversationResponse> deleteConversationTask(final DeleteConversationRequest deleteConversationRequest) {
        return Single.fromCallable(() -> {
            conversationDao.deleteConversation(deleteConversationRequest);

            final DeleteConversationResponse deleteConversationResponse = new DeleteConversationResponse();
            deleteConversationResponse.setResponse(true);
            return deleteConversationResponse;
        });
    }

    private DisposableSingleObserver<DeleteConversationResponse> deleteConversationObserver() {
        return new DisposableSingleObserver<DeleteConversationResponse>() {

            @Override
            public void onSuccess(DeleteConversationResponse value) {
                setValue(value);
            }

            @Override
            public void onError(Throwable throwable) {
                final DeleteConversationResponse deleteConversationResponse = new DeleteConversationResponse();
                deleteConversationResponse.setThrowable(throwable);
                setValue(deleteConversationResponse);
            }
        };
    }
}
