package com.spareroom.livedata.event;

import com.spareroom.model.business.SearchAdvertListProperties.MinTerm;

public class FilterAvailabilityMinTermEvent {
    private int position;
    private MinTerm minTerm;

    public FilterAvailabilityMinTermEvent(int position, MinTerm minTerm) {
        this.position = position;
        this.minTerm = minTerm;
    }

    public MinTerm minTerm() {
        return minTerm;
    }

    public int position() {
        return position;
    }
}
