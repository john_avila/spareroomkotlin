package com.spareroom.livedata.event;

public class FilterAvailabilityDateEvent {
    private int position;
    private String date;

    public FilterAvailabilityDateEvent(int position, String date) {
        this.position = position;
        this.date = date;
    }

    public int position() {
        return position;
    }

    public String date() {
        return date;
    }

}
