package com.spareroom.livedata;

import com.spareroom.livedata.event.DeleteConversationConfirmationEvent;

import io.reactivex.disposables.CompositeDisposable;

public class DeleteConversationConfirmation extends LiveDataController<DeleteConversationConfirmationEvent> {

    public DeleteConversationConfirmation(CompositeDisposable disposables) {
        super(new SingleLiveData<>(), disposables);
    }

    public void delete() {
        setValue(new DeleteConversationConfirmationEvent());
    }

}
