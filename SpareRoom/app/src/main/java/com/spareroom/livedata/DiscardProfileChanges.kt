package com.spareroom.livedata

import com.spareroom.livedata.event.DiscardProfileChangesEvent
import io.reactivex.disposables.CompositeDisposable

class DiscardProfileChanges(disposables: CompositeDisposable) : LiveDataController<DiscardProfileChangesEvent>(SingleLiveData(), disposables) {

    fun discard() {
        value = DiscardProfileChangesEvent()
    }

}
