package com.spareroom.livedata;

import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.business.apiv2.IAdvertListDao;
import com.spareroom.model.business.*;
import com.spareroom.ui.util.DateUtils;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SearchAdvertList extends LiveDataController<SearchAdvertListResponse> {

    private final IAdvertListDao advertListDao;

    public SearchAdvertList(IAdvertListDao advertListDao, CompositeDisposable disposables) {
        super(new SingleLiveData<>(), disposables);
        this.advertListDao = advertListDao;
    }

    public void retrieveAdvertList(SearchAdvertListProperties searchAdvertListProperties, SearchType searchType, boolean useCoordinates) {
        add(
                newRetrieveAdvertListTask(searchAdvertListProperties, searchType, useCoordinates)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(newAdvertListObserver()));
    }

    private Single<SearchAdvertListResponse> newRetrieveAdvertListTask(
            final SearchAdvertListProperties searchAdvertListProperties, final SearchType searchType, boolean useCoordinates) {
        return Single.fromCallable(() -> {
            long start = System.currentTimeMillis();
            SearchAdvertListResponse searchAdvertListResponse = advertListDao.getAdvertList(searchAdvertListProperties, searchType, useCoordinates);
            AnalyticsTrackerComposite.getInstance().trackSearchAdvertListLoadedEvent(DateUtils.getDuration(start));
            return searchAdvertListResponse;
        });

    }

    private DisposableSingleObserver<SearchAdvertListResponse> newAdvertListObserver() {
        return new DisposableSingleObserver<SearchAdvertListResponse>() {

            @Override
            public void onSuccess(@NonNull SearchAdvertListResponse response) {
                setValue(response);
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                AnalyticsTrackerComposite.getInstance().logHandledException(SearchAdvertList.class.getSimpleName(), throwable);
                SearchAdvertListResponse response = new SearchAdvertListResponse();
                response.setThrowable(throwable);
                setValue(response);
            }
        };
    }

}
