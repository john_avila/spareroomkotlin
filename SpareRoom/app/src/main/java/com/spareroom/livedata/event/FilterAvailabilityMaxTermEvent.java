package com.spareroom.livedata.event;

import com.spareroom.model.business.SearchAdvertListProperties.MaxTerm;

public class FilterAvailabilityMaxTermEvent {
    private int position;
    private MaxTerm maxTerm;

    public FilterAvailabilityMaxTermEvent(int position, MaxTerm maxTerm) {
        this.position = position;
        this.maxTerm = maxTerm;
    }

    public MaxTerm maxTerm() {
        return maxTerm;
    }

    public int position() {
        return position;
    }

}
