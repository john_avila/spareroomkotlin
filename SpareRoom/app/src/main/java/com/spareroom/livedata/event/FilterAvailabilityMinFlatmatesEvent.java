package com.spareroom.livedata.event;

import com.spareroom.model.business.SearchAdvertListPropertiesOffered.MinFlatmates;

public class FilterAvailabilityMinFlatmatesEvent {
    private int position;
    private MinFlatmates minFlatmates;

    public FilterAvailabilityMinFlatmatesEvent(int position, MinFlatmates minFlatmates) {
        this.position = position;
        this.minFlatmates = minFlatmates;
    }

    public MinFlatmates minFlatmates() {
        return minFlatmates;
    }

    public int position() {
        return position;
    }

}
