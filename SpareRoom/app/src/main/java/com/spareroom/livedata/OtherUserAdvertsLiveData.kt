package com.spareroom.livedata

import androidx.lifecycle.MutableLiveData
import com.spareroom.integration.analytics.fabric.AnalyticsTagsFabric
import com.spareroom.integration.business.apiv2.*
import com.spareroom.model.business.*
import com.spareroom.ui.util.AdvertsDiffUtil
import io.reactivex.disposables.CompositeDisposable

class OtherUserAdvertsLiveData(
    private val userWantedAdvertsDao: IWantedAdvertsDao,
    private val userOfferedAdvertsDao: IOfferedAdvertsDao,
    disposables: CompositeDisposable
) : LiveDataController<OtherUserAdvertsResponse>(MutableLiveData(), disposables) {

    private val maxAdverts = 200
    private var regardingAdvert: AdvertItem? = null
    private var otherUserId = ""
    private var otherUserFirstName = ""
    private var regardingAdvertId = ""
    private var isOtherUserOffering = false
    private var otherUserWantedAdvertCount = 0
    private var otherUserOfferedAdvertCount = 0

    private val adverts = mutableListOf<AdvertItem>()

    fun init(otherUserId: String, otherUserOfferedAdvertCount: Int, otherUserWantedAdvertCount: Int, regardingAdvert: AbstractAd?,
             isOtherUserOffering: Boolean, otherUserFirstName: String, regardingAdvertId: String) {
        this.otherUserId = otherUserId
        this.regardingAdvertId = regardingAdvertId
        this.otherUserOfferedAdvertCount = otherUserOfferedAdvertCount
        this.otherUserWantedAdvertCount = otherUserWantedAdvertCount
        this.otherUserFirstName = otherUserFirstName
        this.regardingAdvert = if (regardingAdvert != null) AdvertItem(regardingAdvert) else null
        this.isOtherUserOffering = isOtherUserOffering
    }

    fun loadOtherUserAdverts() {
        execute(task = { loadOtherUserAdvertsTask() }, failureResponse = { OtherUserAdvertsResponseFailure(throwable = it) })
    }

    private fun loadOtherUserAdvertsTask(): OtherUserAdvertsResponse {
        return trackTaskDuration(AnalyticsTagsFabric.Event.OTHER_USER_ADVERTS_LOADED) {
            if (isOtherUserOffering && otherUserOfferedAdvertCount > 0) {
                responseFromNetworkCall(userOfferedAdvertsDao, otherUserOfferedAdvertCount)
            } else if (otherUserWantedAdvertCount > 0) {
                responseFromNetworkCall(userWantedAdvertsDao, otherUserWantedAdvertCount)
            } else {
                successfulResponse(emptyList())
            }
        }
    }

    private fun <T : AbstractAd> responseFromNetworkCall(advertsDao: IAdvertsDao<T>, count: Int): OtherUserAdvertsResponse {
        val advertsResponse = advertsDao.getUserAdverts(otherUserId, 1, minOf(count, maxAdverts))
        if (!advertsResponse.success)
            return OtherUserAdvertsResponseFailure(Exception(advertsResponse.errorMessage))

        return successfulResponse(advertsResponse.adverts.map { AdvertItem(it) })
    }

    private fun successfulResponse(fetchedAdverts: List<AdvertItem>): OtherUserAdvertsResponseSuccess {
        updateRegardingAdvert(fetchedAdverts)
        val newAdverts = addUnavailableAdvert(fetchedAdverts)
        val diff = AdvertsDiffUtil.diff(adverts, newAdverts)

        adverts.clear()
        adverts.addAll(newAdverts)

        return OtherUserAdvertsResponseSuccess(diff, newAdverts, otherUserFirstName)
    }

    private fun updateRegardingAdvert(otherUserAdverts: List<AdvertItem>) {
        val regardingAdvertFromSearch = otherUserAdverts.find { it.advert.id == regardingAdvertId }
        val localRegardingAdvert = regardingAdvert
        if (regardingAdvertFromSearch != null && localRegardingAdvert != null) {
            localRegardingAdvert.advert.largePictureUrl = regardingAdvertFromSearch.advert.largePictureUrl

            if (regardingAdvertFromSearch.advert is AdWanted && localRegardingAdvert.advert is AdWanted)
                localRegardingAdvert.advert.searchAreas = regardingAdvertFromSearch.advert.searchAreas
        }
    }

    private fun addUnavailableAdvert(otherUserAdverts: List<AdvertItem>): List<AdvertItem> {
        val adverts = mutableListOf<AdvertItem>()
        regardingAdvert?.let { adverts.add(it) } ?: adverts.add(AdvertItem(AdWanted(idField = AdvertItem.ADVERT_UNAVAILABLE)))
        adverts.addAll(otherUserAdverts)

        return adverts
    }
}
