package com.spareroom.livedata

import androidx.lifecycle.*
import com.spareroom.integration.analytics.AnalyticsTrackerComposite
import com.spareroom.ui.util.DateUtils
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers

abstract class LiveDataController<T>(
    private val liveData: MutableLiveData<T>,
    private val disposables: CompositeDisposable) {

    init {
        initRxJava()
    }

    var isLoading = false
        protected set

    var value: T?
        get() = liveData.value
        set(value) {
            if (value == null)
                throw IllegalStateException("Not allowed to set null as a value")

            liveData.value = value
        }

    fun observe(lifecycleOwner: LifecycleOwner, observer: Observer<T>) {
        liveData.observe(lifecycleOwner, observer)
    }

    fun add(disposable: Disposable) {
        disposables.add(disposable)
    }

    open fun onPreExecute() {

    }

    open fun onPostExecute() {

    }

    /**
     * Executes task on the given thread and delivers result to live data
     *
     * Steps:
     * 1. Adds observer to disposable container,
     * 2. Executes task on the background thread (default)
     * 3. Dispatches results to live data observers on the main thread (default)
     *
     * If live data is used in view model - view model will take care of disposing live data's observers
     * @param task job that will be performed on the given thread (background by default)
     * @param failureResponse object that contains throwable and represents failure response
     **/
    protected inline fun execute(subscribeOn: Scheduler = Schedulers.io(),
                                 observeOn: Scheduler = AndroidSchedulers.mainThread(),
                                 analyticsTracker: AnalyticsTrackerComposite = AnalyticsTrackerComposite.getInstance(),
                                 crossinline task: (disposable: Disposable) -> T,
                                 crossinline failureResponse: (throwable: Throwable) -> T) {

        if (isLoading) {
            analyticsTracker.logHandledException("Execute in LiveDataController", Exception("Trying to call 'execute' when already in progress"))
            return
        }

        onPreExecute()
        isLoading = true
        val liveDataName = this::class.java.simpleName

        val disposableSingleObserver = object : DisposableSingleObserver<T>() {

            override fun onSuccess(successResponse: T) {
                isLoading = false
                onPostExecute()
                value = successResponse
            }

            override fun onError(throwable: Throwable) {
                isLoading = false
                onPostExecute()
                analyticsTracker.logHandledException(liveDataName, throwable)
                value = failureResponse(throwable)
            }
        }

        add(Single.fromCallable { task(disposableSingleObserver) }
            .subscribeOn(subscribeOn)
            .observeOn(observeOn)
            .subscribeWith(disposableSingleObserver))
    }

    inline fun trackTaskDuration(taskName: String, analyticsTracker: AnalyticsTrackerComposite = AnalyticsTrackerComposite.getInstance(), task: () -> T): T {
        val start = System.currentTimeMillis()
        val response = task()
        analyticsTracker.trackTaskDuration(taskName, DateUtils.getDuration(start))
        return response
    }

    private fun initRxJava() {
        if (RxJavaPlugins.getErrorHandler() == null) {
            RxJavaPlugins.setErrorHandler { throwable ->
                AnalyticsTrackerComposite.getInstance().logHandledException("User left before task completed", throwable)
            }
        }
    }

}