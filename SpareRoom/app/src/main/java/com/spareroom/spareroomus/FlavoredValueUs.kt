package com.spareroom.spareroomus

import com.spareroom.FlavoredValue
import java.util.*

private const val DOMAIN = "https://androidapp.spareroom.com"
private const val WEB_DOMAIN = "http://www.spareroom.com"
private const val TERMS_DOMAIN = "https://androidapp.spareroom.com/content/padded/terms-us"
private const val PRIVACY_DOMAIN = "https://androidapp.spareroom.com/content/padded/privacy-us"
private const val TWITTER_USERNAME = "SpareRoom"
private const val PHONE = "18772574944"
private const val API_KEY = "10034"
private const val API_S_1 = "8d*!3BYFhA3BqQ"
private const val API_S_5 = 95
private const val API_S_3 = 6
private const val API_S_4 = "U2mUN5s"
private const val USER_AGENT_APP_ID = "SpareRoomAppUS"
private const val CS_EMAIL = "customerservices@spareroom.com"
private const val DATE_HOUR_REPRESENTATION = "MM/dd/yyy h:mm a"
private const val DATE_REPRESENTATION = "MMMM dd, yyyy"
private const val UPGRADE_1_BOLD = "On average Bold Adverts get double the enquiries - find your room or roommate faster"
private const val CURRENCY = "$"
private const val GOOGLE_PLAY_APP_NAME = "SpareRoom USA"

private val LOCALE = Locale.US!!

class FlavoredValueUs : FlavoredValue {

    override val domain = DOMAIN

    override val webDomain = WEB_DOMAIN

    override val termsDomain = TERMS_DOMAIN

    override val privacyDomain = PRIVACY_DOMAIN

    override val twitterUsername = TWITTER_USERNAME

    override val currency = CURRENCY

    override val phone = PHONE

    override val apiId = API_KEY

    override val apiSecret = API_S_1 + API_S_5 + API_S_3 + API_S_4

    override val userAgentAppId = USER_AGENT_APP_ID

    override val csEmail = CS_EMAIL

    override val locale = LOCALE

    override val dateHourRepresentation = DATE_HOUR_REPRESENTATION

    override val dateRepresentation = DATE_REPRESENTATION

    override val key = KeyUs()

    override val upgrade1Bold = UPGRADE_1_BOLD

    override val betaUrl = ""

    override val googlePlayAppName = GOOGLE_PLAY_APP_NAME
}
