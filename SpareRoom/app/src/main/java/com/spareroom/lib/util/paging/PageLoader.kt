package com.spareroom.lib.util.paging

class PageLoader<SrcType : PageItem, DstType : PageItem, PageType : Page<SrcType>>(
    private val currentItems: List<DstType>,
    private val maxPerPage: Int,
    private val mergeWithPreviousPages: Boolean,
    private val transformation: (items: List<SrcType>) -> List<DstType>) {

    private var offset = 0

    fun loadPage(firstPage: Boolean, task: (offset: Int, maxPerPage: Int) -> PageType): PageResponse<DstType, PageType> {
        var duplicatesFound = false
        do {
            val newOffset = if (firstPage) 0 else offset + maxPerPage
            val page = task(newOffset, maxPerPage)
            if (!page.success)
                return PageResponse(items = emptyList(), page = page, hadDuplicates = false, hasMoreItems = true, offset = offset)

            val newItems = convertItems(transformation, page)

            offset = newOffset
            val distinctItems = if (firstPage) newItems else distinct(currentItems, newItems)

            duplicatesFound = duplicatesFound || newItems.size != distinctItems.size
            val hasOnlyDuplicates = newItems.isNotEmpty() && distinctItems.isEmpty()
            val hasMoreItems = newItems.size == maxPerPage
            val shouldFetchNextPage = !firstPage && hasOnlyDuplicates && hasMoreItems

            if (!shouldFetchNextPage) {
                val newList = if (firstPage || !mergeWithPreviousPages) distinctItems else merge(currentItems, distinctItems)
                return PageResponse(newList, page, duplicatesFound, hasMoreItems, offset)
            }

        } while (true)
    }

    @Throws(Exception::class)
    private fun convertItems(transformation: (items: List<SrcType>) -> List<DstType>, page: PageType): List<DstType> {
        val newItems = transformation(page.rawItems)
        if (newItems.size != page.rawItems.size)
            throw Exception("Output list must have same size")

        if (distinct(page.rawItems, newItems).isNotEmpty() || newItems.groupBy { it.uniqueId }.size != page.rawItems.groupBy { it.uniqueId }.size)
            throw Exception("Output list must have items with same ids")

        return newItems
    }

    private fun distinct(oldItems: List<PageItem>, newItems: List<DstType>): List<DstType> {
        val distinctItems = mutableListOf<DstType>()

        for (newItem in newItems) {
            if (oldItems.find { it.uniqueId == newItem.uniqueId } == null)
                distinctItems.add(newItem)
        }

        return distinctItems
    }

    private fun merge(firstList: List<DstType>, secondList: List<DstType>): List<DstType> {
        val list = mutableListOf<DstType>()
        list.addAll(firstList)
        list.addAll(secondList)
        return list
    }
}