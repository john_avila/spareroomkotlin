package com.spareroom.lib.util;

import com.spareroom.ui.util.StringUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class EnumUtil {

    @NonNull
    public static <T extends IEnum> T searchItemByValue(@NonNull T[] enumValues, @Nullable String valueToSearch, @NonNull T defaultValue) {
        for (T item : enumValues) {
            if (StringUtils.equals(valueToSearch, item.getValue())) {
                return item;
            }
        }
        return defaultValue;
    }

}
