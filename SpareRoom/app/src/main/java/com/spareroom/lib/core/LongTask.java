package com.spareroom.lib.core;

import com.spareroom.lib.core.exception.LongTaskInterruptedException;
import com.spareroom.model.exception.ModelOperationInterruptedException;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by miguel.rossi on 23/09/2016.
 */
public abstract class LongTask implements IObservable {
    private Thread _thread;
    private List<IObserver> _listObserver = new LinkedList<>();
    private volatile boolean _isInterrupted = false; //TODO: replace with thread.interrupted

    public void run() {
        _thread.start();
    }

    @Override
    public void addObserver(IObserver o) {
        _listObserver.add(o);
    }

    @Override
    public void removeObserver(IObserver o) {
        _listObserver.remove(o);
    }

    protected void notifyObservers(Object o) {
        for (IObserver observer : _listObserver)
            observer.notifyObserver(o);
    }

    protected void setRunnable(Runnable runnable) {
        _thread = new Thread(runnable);
    }

    public void cancel() {
        _isInterrupted = true;
        notifyObservers(new ModelOperationInterruptedException());
    }

    protected Thread getThread() {
        return _thread;
    }

    public abstract int getId();

    public class InterruptionObserver implements ILongTaskObserver {
        @Override
        public void notifyObserver(Object o) throws LongTaskInterruptedException {
            if (_isInterrupted)
                throw new LongTaskInterruptedException();
        }
    }

}
