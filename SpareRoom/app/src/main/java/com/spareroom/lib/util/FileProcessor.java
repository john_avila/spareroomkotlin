package com.spareroom.lib.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileProcessor {
    private static final long CHUNK_SIZE = 256000L; // (long)(250 * 1024);
    private final String _filePath;
    private boolean isClosed = false;
    private final File _file;
    private BufferedInputStream inputStream;

    public FileProcessor(String filePath) throws IOException {
        _filePath = filePath;

        _file = new File(filePath);
        if (!_file.exists())
            throw new IOException();
        open();

    }

    private void open() throws FileNotFoundException {
        inputStream =
                new BufferedInputStream(new FileInputStream(_filePath));
    }

    public void close() throws IOException {
        inputStream.close();
        isClosed = true;
    }

    private boolean isClosed() {
        return isClosed;
    }

    public int getNumberChunks() {
        double numberChunks = (double) _file.length() / CHUNK_SIZE;
        return (int) Math.ceil(numberChunks);
    }

    /**
     * Writes a chunk to a file and returns the path to that file
     *
     * @return the path to the file with the chunk
     * @throws IOException if the <code>FileProcessor</code> was already closed
     */
    private String getChunk() throws IOException {
        String chunkFileName = _filePath + ".chunk";

        // TODO improve the following line by simply truncating the file
        deleteChunks();

        BufferedOutputStream outputStream =
                new BufferedOutputStream(new FileOutputStream(chunkFileName));

        for (int currentByte = 0; currentByte < CHUNK_SIZE; currentByte++) {
            int byteRead = inputStream.read();
            if (byteRead == -1)
                break;
            outputStream.write(byteRead);
        }
        outputStream.close();

        return chunkFileName;
    }

    public boolean deleteChunks() {
        String chunkFileName = _filePath + ".chunk";
        return (new File(chunkFileName)).delete();
    }

    public ChunkIterator getIterator() {
        return new ChunkIterator();
    }

    public class ChunkIterator {
        private int _currentChunk = 0;
        private final int _numberChunks = getNumberChunks();

        /**
         * Sets the iterator at the beginning of the original file
         *
         * @throws IOException if the FileProcessor was already closed
         */
        public void setFirstChunk() throws IOException {
            _currentChunk = 0;
            inputStream.reset();
        }

        /**
         * Writes next chunk to the file
         *
         * @return path to the file with the following chunk
         * @throws IOException if the FileProcessor was already closed
         */
        public String nextChunk() throws IOException {
            String chunkFileName = getChunk();
            _currentChunk += 1;

            return chunkFileName;
        }

        /**
         * Checks whether the previous chunk processed was the last one
         *
         * @return <code>true</code> if the previous chunk process with <code>nextChunk()</code>
         * was the last one
         */
        public boolean isLastChunk() {
            return _currentChunk == _numberChunks;
        }

    }
}
