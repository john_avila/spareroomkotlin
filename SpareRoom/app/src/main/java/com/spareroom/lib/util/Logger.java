package com.spareroom.lib.util;

import android.util.Log;

public class Logger {
    private static final String PATTERN__ = "-----";
    private static final String PATTERN_X = "xxx";

    public static void d(String message) {
        Log.d(PATTERN__, message);
    }

    public static void d(boolean message) {
        Log.d(PATTERN__, String.valueOf(message));
    }

    public static void d(long message) {
        Log.d(PATTERN__, String.valueOf(message));
    }

    public static void d(double message) {
        Log.d(PATTERN__, String.valueOf(message));
    }

    public static void d(String format, Object... values) {
        Log.d(PATTERN__, String.format(format, values));
    }

    public static void e(String message) {
        Log.e(PATTERN_X, message);
    }

    public static void e(boolean message) {
        Log.e(PATTERN_X, String.valueOf(message));
    }

    public static void e(long message) {
        Log.e(PATTERN_X, String.valueOf(message));
    }

    public static void e(double message) {
        Log.e(PATTERN_X, String.valueOf(message));
    }

    public static void e(String format, Object... values) {
        Log.e(PATTERN_X, String.format(format, values));
    }

}
