package com.spareroom.lib.util.paging

interface PageItem {
    val uniqueId: String
}