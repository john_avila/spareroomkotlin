package com.spareroom.lib.util;

import com.spareroom.integration.exception.InvalidJSONFormatException;
import com.spareroom.model.business.Parameters;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by miguel.rossi on 24/05/2016.
 */
public class Json2HttpUtil {

    public Parameters transform(String s) throws InvalidJSONFormatException {
        Parameters parameters = new Parameters();
        JSONObject jsonObject;
        Iterator iterator;

        try {
            jsonObject = new JSONObject(s);
            iterator = jsonObject.keys();

            while (iterator.hasNext()) {
                String
                        key = iterator.next().toString(),
                        value = jsonObject.getString(key);

                parameters.add(key, value);
            }
        } catch (JSONException e) {
            throw new InvalidJSONFormatException();
        }
        return parameters;

    } //end transform(String s)

}
