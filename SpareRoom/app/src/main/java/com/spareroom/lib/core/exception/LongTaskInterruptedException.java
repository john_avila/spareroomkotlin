package com.spareroom.lib.core.exception;

/**
 * Created by manuel on 27/09/2016.
 */
public class LongTaskInterruptedException extends Exception {
    private static final long serialVersionUID = -2297310296706380950L;

    public LongTaskInterruptedException() {
        super();
    }

    public LongTaskInterruptedException(String message) {
        super(message);
    }

    public LongTaskInterruptedException(Exception e) {
        super(e);
    }
}
