package com.spareroom.lib.core;

import com.spareroom.lib.core.exception.LongTaskInterruptedException;

public interface ILongTaskObserver {
    void notifyObserver(Object o) throws LongTaskInterruptedException;
}
