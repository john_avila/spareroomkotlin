package com.spareroom.lib.core;

public interface IObserver {
    void notifyObserver(Object o);
}
