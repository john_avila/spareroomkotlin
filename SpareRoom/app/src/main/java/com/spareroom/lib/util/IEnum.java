package com.spareroom.lib.util;

public interface IEnum {
    String getValue();
}
