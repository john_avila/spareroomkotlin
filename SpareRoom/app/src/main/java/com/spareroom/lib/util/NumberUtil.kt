package com.spareroom.lib.util

object NumberUtil {
    const val MIN_AGE = 18
    const val A_MILE_IN_METERS = 1609.344

    fun milesToMeters(miles: Double): Double {
        val meters = miles * A_MILE_IN_METERS
        return if (meters < 0) 0.0 else meters
    }

    @JvmStatic
    fun isLowerThanMinAge(age: Int) = age < MIN_AGE

}
