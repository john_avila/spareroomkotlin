package com.spareroom.lib.core;

public interface ObserverLifecycleInterface {

    /**
     * Notifies to the observers when the {@link android.app.Activity#onStop() onStop()} is called
     */
    void notifyOnStop();
}
