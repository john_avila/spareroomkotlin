package com.spareroom.lib.util.paging

interface Page<SrcType> {
    val rawItems: List<SrcType>
    val success: Boolean
}