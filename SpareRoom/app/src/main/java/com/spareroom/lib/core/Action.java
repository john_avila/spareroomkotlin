package com.spareroom.lib.core;

/**
 * Model logic
 */
public abstract class Action {
    /**
     * Action to be performed (Model logic)
     *
     * @return result of the operation, <code>null</code> if there was no result from performing
     * the operation (procedure / void method). Exceptions should be thrown.
     * @throws Exception if something went wrong when performing the operation. Inspect the code
     *                   for this <code>Action</code> for more details on the possible Exceptions than can be
     *                   thrown.
     */
    public abstract Object execute(Object... params) throws Exception;
}