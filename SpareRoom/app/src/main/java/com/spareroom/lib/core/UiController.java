package com.spareroom.lib.core;

import com.spareroom.model.business.SpareroomStatus;

/**
 * Controller that handles ui components in when initialising and populating them.
 * Created by manuel on 24/05/2016.
 */
public abstract class UiController {
    /**
     * initialises the components (ie: findViewById for Views)
     */
    public void reset() {
    }

    /**
     * sets the components so they display on screen the loading state
     */
    public void setLoading() {
    }

    /**
     * sets the components so they display an empty state
     */
    public void setEmpty() {
    }

    /**
     * Populates components with content. For <code>SpareroomStatus</code> and
     * <code>Exception</code> use {@link #setSpareroomStatus(SpareroomStatus)}
     * or {@link #setException(Exception)}
     *
     * @param o objects that help to update the UI
     * @see UiController#setResult(Object...)
     */
    public void setResult(Object... o) {
    }

    /**
     * Populates components with content.
     *
     * @param spareroomStatus that helps to update the UI
     */
    public void setSpareroomStatus(SpareroomStatus spareroomStatus) {
    }

    /**
     * Populates components with content.
     *
     * @param exception that helps to update the UI
     */
    public void setException(Exception exception) {
    }

}
