package com.spareroom.lib.util

import android.content.Context
import android.content.pm.PackageManager
import com.spareroom.integration.analytics.AnalyticsTrackerComposite
import com.spareroom.ui.util.UiUtils.isJellyBeanMr1OrNewer

object HardwareFeatures {

    @JvmStatic
    fun supportsLocation(context: Context) = hasSystemFeature(context, PackageManager.FEATURE_LOCATION)

    @JvmStatic
    fun supportsMicrophone(context: Context) = hasSystemFeature(context, PackageManager.FEATURE_MICROPHONE)

    @JvmStatic
    fun supportsCamera(context: Context): Boolean {
        return hasSystemFeature(context, if (isJellyBeanMr1OrNewer()) PackageManager.FEATURE_CAMERA_ANY else PackageManager.FEATURE_CAMERA)
    }

    private fun hasSystemFeature(context: Context, feature: String): Boolean {
        val supportsFeature = context.packageManager.hasSystemFeature(feature)
        if (!supportsFeature)
            AnalyticsTrackerComposite.getInstance().logHandledException("HardwareFeatures", Exception("Device has no $feature"))

        return supportsFeature
    }
}