package com.spareroom.lib.util.paging

class PageResponse<DstType, PageType>(
    val items: List<DstType>,
    val page: PageType,
    val hadDuplicates: Boolean,
    val hasMoreItems: Boolean,
    val offset: Int)