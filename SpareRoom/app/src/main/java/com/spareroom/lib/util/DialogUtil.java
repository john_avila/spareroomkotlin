package com.spareroom.lib.util;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.ui.screen.AlertDialogBuilder;

public class DialogUtil {

    public static Dialog updateDialog(Context c) {
        View v = LayoutInflater.from(c).inflate(R.layout.dialog_message_button, null);

        Button bPositive = v.findViewById(R.id.dialog_bPositive);
        TextView tvMessage = v.findViewById(R.id.dialog_message);

        tvMessage.setText(R.string.dialogUpdate_tvMessage);

        Dialog d = new AlertDialogBuilder(c, v).create();
        bPositive.setOnClickListener(new CancelOnClickListener(d));

        return d;
    }

    public static Dialog timeDateDialog(Context c) {
        View v = LayoutInflater.from(c).inflate(R.layout.dialog_message_button, null);

        Button bPositive = v.findViewById(R.id.dialog_bPositive);
        TextView tvMessage = v.findViewById(R.id.dialog_message);

        tvMessage.setText(R.string.dialog_datetime_message);

        Dialog d = new AlertDialogBuilder(c, v).create();
        bPositive.setOnClickListener(new DateTimeSettingsOnClickListener(d));

        return d;
    }

    public static class CancelOnClickListener implements OnClickListener {
        private Dialog _dialog;

        public CancelOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            _dialog.cancel();
        }
    }

    public static class DateTimeSettingsOnClickListener implements OnClickListener {
        private Dialog _dialog;

        public DateTimeSettingsOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {

            _dialog.getContext().startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));

            _dialog.cancel();
        }
    }

    public static void dismissSafely(Dialog dialog) {
        try {
            dialog.dismiss();
        } catch (Exception e) {
            // ok to ignore as activity is probably finished
        }
    }

}
