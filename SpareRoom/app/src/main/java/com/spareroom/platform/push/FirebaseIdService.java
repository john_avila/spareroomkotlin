package com.spareroom.platform.push;

import com.google.firebase.iid.FirebaseInstanceIdService;
import com.spareroom.controller.FirebaseIdController;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.model.business.Session;

/**
 * A service that extends FirebaseInstanceIdService to handle the creation, rotation, and updating
 * of registration tokens. This is required for sending to specific devices or for creating device
 * groups.
 * <p/>
 * Firebase Instance ID provides a unique identifier for each app instance and a mechanism to
 * authenticate and authorize actions (example: sending FCM messages).
 * <p/>
 * Created by miguel.rossi on 05/01/2017.
 */

public class FirebaseIdService extends FirebaseInstanceIdService {

    /**
     * Called if InstanceID token is updated. This may occur if the security of the previous token
     * had been compromised. Note that this is called when the InstanceID token is initially
     * generated so this is where you would retrieve the token.
     */
    @Override
    public void onTokenRefresh() {

        Session session =
                SpareroomApplication
                        .getInstance(getApplicationContext())
                        .getSpareroomContext()
                        .getSession();

        if (session != null) {
            FirebaseIdController.register(
                    SpareroomApplication
                            .getInstance(getApplicationContext())
                            .getSpareroomContext());
        }

    }

}
