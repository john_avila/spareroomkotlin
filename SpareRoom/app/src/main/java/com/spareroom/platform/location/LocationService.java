package com.spareroom.platform.location;

import android.content.Context;
import android.location.*;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.Toast;

import com.spareroom.lib.util.HardwareFeatures;

import java.util.Vector;

/**
 * Provides location information for this device based on the network provider and GPS signal if available.
 *
 * @author manuel
 */
public class LocationService {
    private final Context _appContext;
    private final LocationManager _locationManager;
    private final LocationListener _locationListener;
    /**
     * List of observers
     */
    private final Vector<ILocationObserver> _observers;
    private CountDownTimer _timeoutTimer;

    public LocationService(Context appContext) {
        _appContext = appContext;
        _locationManager = HardwareFeatures.supportsLocation(appContext) ? (LocationManager) appContext.getSystemService(Context.LOCATION_SERVICE) : null;
        _locationListener = new CustomLocationListener();

        _observers = new Vector<>();
    }

    /**
     * Starts requesting location updates
     */
    public void enableUpdates() throws SecurityException {
        if (isLocationFeatureGone())
            return;

        boolean isNetwork =
                _locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER);
        boolean isGPS =
                _locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER);

        if (isNetwork && isGPS) {
            _locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 1000, 10f, _locationListener);

            _locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 1000, 10f, _locationListener);

            if (!_locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
                    && !_locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                providerDisabled();

            _timeoutTimer = new CountDownTimer(5000, 5000) {

                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    Toast.makeText(
                            _appContext,
                            "Still running... Waiting for location signal",
                            Toast.LENGTH_LONG).show();
                }

            }.start();

        } else {
            providerDisabled();
        }
    }

    /**
     * Stops locations updates
     */
    public void disableUpdates() {
        if (isLocationFeatureGone())
            return;

        if (_timeoutTimer != null)
            _timeoutTimer.cancel();
        _locationManager.removeUpdates(_locationListener);
    }

    /*
    public Location getLocation() {
        return _location;
    }
    */
    private void updateLocation(Location l) {
        if (_timeoutTimer != null)
            _timeoutTimer.cancel();
        for (ILocationObserver o : _observers) {
            o.updateLocation(l);
        }
    }

    private void providerDisabled() {
        if (_timeoutTimer != null)
            _timeoutTimer.cancel();
        for (ILocationObserver o : _observers) {
            o.locationProviderDisabled();
        }
    }

    public class CustomLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            if (_timeoutTimer != null)
                _timeoutTimer.cancel();
            updateLocation(location);

        }

        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // TODO Auto-generated method stub

        }

    }

    /**
     * Registers a new observer for location updates
     *
     * @param o observer of location updates
     */
    public void addObserver(ILocationObserver o) {
        _observers.add(o);
    }

    /**
     * Removes the given observer for location updates
     *
     * @param o observer for location updates
     */
    public void removerObserver(ILocationObserver o) {
        _observers.remove(o);
    }

    private boolean isLocationFeatureGone() {
        return _locationManager == null;
    }
}
