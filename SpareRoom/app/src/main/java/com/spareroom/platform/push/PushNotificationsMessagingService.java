package com.spareroom.platform.push;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.integration.dependency.component.ComponentRepository;
import com.spareroom.integration.imageloader.ImageLoader;
import com.spareroom.integration.imageloader.TransformationUtils;
import com.spareroom.model.business.ConversationInfo;
import com.spareroom.model.business.Session;
import com.spareroom.ui.screen.ConversationFragment;
import com.spareroom.ui.screen.ConversationListFragment;
import com.spareroom.ui.screen.legacy.MainActivity;
import com.spareroom.ui.util.NotificationUtils;
import com.spareroom.ui.util.StringUtils;

import java.util.Map;

import javax.inject.Inject;

import static com.spareroom.ui.screen.legacy.MainActivity.REQUESTED_FRAGMENT_TAG;

public class PushNotificationsMessagingService extends FirebaseMessagingService {
    public final static String KEY_USER_ID_TO = "user_id_to";

    private final static String KEY_TYPE = "type";
    private final static String KEY_TITLE = "title";
    private final static String KEY_MESSAGE = "message";
    private final static String VALUE_TYPE_NEW_MESSAGE = "new_message";
    private final static String VALUE_TYPE_UNREAD_CONVERSATIONS = "unread_conversations";
    private final static String KEY_UNREAD_CONVERSATION_COUNT = "unread_conversation_count";
    private final static String VALUE_UNREAD_CONVERSATION_COUNT_ONE = "1";
    private final static String KEY_CONVERSATION_WITH = "conversation_with";
    private final static String KEY_ADVERT_ID = "advert_id";
    private final static String KEY_LARGE_ICON = "large_icon";
    private final static String KEY_SUMMARY_TITLE = "summary_title";
    private final static String KEY_SUMMARY_MESSAGE = "summary_message";

    @Inject
    ImageLoader _imageLoader;

    @Override
    public void onCreate() {
        super.onCreate();
        ComponentRepository.get().getAppComponent().inject(this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (isUserLoggedIn()) {
            Map<String, String> remoteMessageMap = remoteMessage.getData();

            if (!remoteMessageMap.isEmpty() && getUserId().equals(remoteMessageMap.get(KEY_USER_ID_TO))) {
                if (VALUE_TYPE_UNREAD_CONVERSATIONS.equals(remoteMessageMap.get(KEY_TYPE))) {
                    showSummaryMessageNotification(remoteMessageMap);
                } else if (VALUE_TYPE_NEW_MESSAGE.equals(remoteMessageMap.get(KEY_TYPE))) {
                    showMessageNotification(remoteMessageMap);
                }
            }

        } else {
            FirebaseIdController.unregister(SpareroomApplication.getInstance().getSpareroomContext());
        }

    }

    private void showMessageNotification(Map<String, String> extras) {
        reloadSession();

        PendingIntent contentIntent = getPendingIntent(extras);
        if (contentIntent != null)
            NotificationUtils.showMessagesNotification(this, getLargeIconForMessage(extras), extras.get(KEY_TYPE),
                    extras.get(KEY_TITLE), extras.get(KEY_MESSAGE), contentIntent);
    }

    private void showSummaryMessageNotification(Map<String, String> extras) {
        reloadSession();

        PendingIntent contentIntent = getPendingIntent(extras);
        if (contentIntent != null)
            NotificationUtils.showMessagesNotification(this, getLargeIconForSummaryMessage(extras), extras.get(KEY_TYPE),
                    extras.get(KEY_SUMMARY_TITLE), extras.get(KEY_SUMMARY_MESSAGE), contentIntent);
    }

    private PendingIntent getPendingIntent(Map<String, String> extras) {
        boolean singleMessageType = VALUE_TYPE_NEW_MESSAGE.equals(extras.get(KEY_TYPE));
        boolean multipleMessageType = VALUE_TYPE_UNREAD_CONVERSATIONS.equals(extras.get(KEY_TYPE));

        if (singleMessageType || multipleMessageType) {
            boolean oneMessage = VALUE_UNREAD_CONVERSATION_COUNT_ONE.equals(extras.get(KEY_UNREAD_CONVERSATION_COUNT));
            boolean singleMessage = singleMessageType || oneMessage;

            String requestedFragment = singleMessage ? ConversationFragment.TAG : ConversationListFragment.TAG;
            Intent targetIntent = new Intent(this, MainActivity.class);
            targetIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            targetIntent.putExtra(REQUESTED_FRAGMENT_TAG, requestedFragment);
            targetIntent.putExtra(KEY_USER_ID_TO, extras.get(KEY_USER_ID_TO));

            if (singleMessage) {
                targetIntent.putExtra(
                        ConversationInfo.TAG,
                        new ConversationInfo(
                                null,
                                extras.get(KEY_ADVERT_ID),
                                extras.get(KEY_CONVERSATION_WITH),
                                StringUtils.getFirstString(extras.get(KEY_TITLE)),
                                StringUtils.getLastName(extras.get(KEY_TITLE)),
                                extras.get(KEY_LARGE_ICON)));
            }

            return PendingIntent.getActivity(this, 0, targetIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
        return null;
    }

    private Bitmap getLargeIconForMessage(Map<String, String> extras) {
        if (extras.containsKey(KEY_LARGE_ICON) && extras.get(KEY_LARGE_ICON) != null && !extras.get(KEY_LARGE_ICON).isEmpty()) {
            Bitmap bitmapIcon = _imageLoader.getBitmap(extras.get(KEY_LARGE_ICON));
            return (bitmapIcon == null) ? getUserIconBitmap() : circlePicture(bitmapIcon);
        }

        return getUserIconBitmap();
    }

    private Bitmap getLargeIconForSummaryMessage(Map<String, String> extras) {
        if (VALUE_UNREAD_CONVERSATION_COUNT_ONE.equals(extras.get(KEY_UNREAD_CONVERSATION_COUNT))) {
            String largeIconUrl = extras.get(KEY_LARGE_ICON);
            if (largeIconUrl == null || largeIconUrl.isEmpty())
                return getUserIconBitmap();
            else {
                Bitmap bitmapIcon = _imageLoader.getBitmap(extras.get(KEY_LARGE_ICON));
                return (bitmapIcon == null) ? getUserIconBitmap() : circlePicture(bitmapIcon);
            }
        } else {
            return getMessageIconBitmap();
        }
    }

    private void reloadSession() {
        SessionControllerAction sessionControllerAction =
                new SessionControllerAction(SpareroomApplication.getInstance().getSpareroomContext());
        sessionControllerAction.execute();
    }

    private Bitmap circlePicture(Bitmap bitmap) {
        int circleDimens = Math.round(Math.min(bitmap.getHeight(), bitmap.getWidth()));
        return TransformationUtils.circleCrop(bitmap, circleDimens, circleDimens);
    }

    private boolean isUserLoggedIn() {
        Session session = SpareroomApplication.getInstance().getSpareroomContext().getSession();
        return session != null;
    }

    private String getUserId() {
        return SpareroomApplication.getInstance().getSpareroomContext().getUserId();
    }

    private Bitmap getMessageIconBitmap() {
        return BitmapFactory.decodeResource(getResources(), R.drawable.ic_contact_message_png);
    }

    private Bitmap getUserIconBitmap() {
        return BitmapFactory.decodeResource(getResources(), R.drawable.ic_avatar_whole_png);
    }

}
