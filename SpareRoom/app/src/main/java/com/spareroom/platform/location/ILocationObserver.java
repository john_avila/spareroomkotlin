package com.spareroom.platform.location;

import android.location.Location;

public interface ILocationObserver {
    void updateLocation(Location l);

    void locationProviderDisabled();
}
