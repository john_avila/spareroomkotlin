package com.spareroom.platform.camera;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.Surface;

import com.spareroom.R;
import com.spareroom.ui.screen.Activity;
import com.spareroom.model.business.Video;
import com.spareroom.ui.util.ToastUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Class to handle the camera following the steps specified by Google.
 *
 * @see <a href="https://developer.android.com/reference/android/hardware/Camera.html">
 * Camera documentation</a>
 * Created by manuel on 03/10/2016.
 */
public class MediaRecorderHelper {
    private static final int VIDEO_ENCODING_BIT_RATE = 1000000; // 1000 * 1000
    private static final int VIDEO_FRAME_RATE = 30;
    // The video becomes a little bit longer when the time limit is reached
    private static final int VIDEO_MAX_DURATION = Video.MAX_LENGTH_IN_MILLIS - 1000;
    private static final int AUDIO_ENCODING_BIT_RATE = 128000; // 128 * 1000
    private static final int AUDIO_CHANNELS = 1;
    private static final int AUDIO_SAMPLING_RATE = 48000; // 48 * 1000

    private Camera _camera;
    private MediaRecorder _mediaRecorder = new MediaRecorder();
    private Camera.Size _optimalSize;
    private File _outputMediaFile;

    private boolean _isCameraReady = false;
    private boolean _isMediaRecorderReady = false;
    private boolean _isRecording = false;

    public MediaRecorderHelper() {
    }

    private Camera.Size getOptimalVideoSize(
            List<Camera.Size> supportedVideoSizes,
            List<Camera.Size> previewSizes) {

        float targetWidth = 854F;
        int minWidth = 900;
        List<Camera.Size> videoSizes;
        Camera.Size optimalSize = null;

        // If supportedVideoSizes is null, we are allowed to use the preview size that interests us more
        videoSizes = (supportedVideoSizes != null) ? supportedVideoSizes : previewSizes;

        // Pick the largest size that can fit in the view and still maintain the aspect ratio.
        for (Camera.Size size : videoSizes) {

            if (size.width < minWidth) {
                float currentWidth = (float) size.width;
                Camera.Size previousSize = optimalSize;

                if (previousSize == null)
                    optimalSize = size;
                else if ((targetWidth - currentWidth) < (targetWidth - previousSize.width))
                    // replace the size if the new one is closer to the target
                    optimalSize = size;

            }

        }

        return optimalSize;
    }

    private Camera.Parameters adjustCameraParameters(Camera.Parameters parameters) {
        List<Camera.Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
        List<Camera.Size> supportedVideoSizes = parameters.getSupportedVideoSizes();
        List<String> focusModes = parameters.getSupportedFocusModes();
        Camera.Size optimalSize =
                getOptimalVideoSize(
                        supportedVideoSizes,
                        supportedPreviewSizes);
        CamcorderProfile profile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);

        // Use the same size for recording profile.
        profile.videoFrameWidth = optimalSize.width;
        profile.videoFrameHeight = optimalSize.height;

        // Auto-focus
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
        }

        // Likewise for the camera object itself.
        parameters.setPreviewSize(profile.videoFrameWidth, profile.videoFrameHeight);
        return parameters;
    }

    private int getDisplayOrientation(Activity activity,
                                      int cameraId, android.hardware.Camera camera) {
        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        return result;

    }

    private File getOutputMediaFile() {
        @SuppressWarnings("SpellCheckingInspection")
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File myDir = new File(
                Environment.getExternalStorageDirectory().getPath() + File.separator,
                "SpareRoom/Video");
        String filePath;
        File mediaFile;
        if (myDir.mkdirs() || myDir.isDirectory()) {
            filePath = myDir.getPath() + File.separator + "VID_" + timeStamp + ".mp4";
            mediaFile = new File(filePath);

        } else {
            filePath = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES)
                    .getPath()
                    + File.separator
                    + "VID_"
                    + timeStamp
                    + ".mp4";

            mediaFile = new File(filePath);
        }

        return mediaFile;
    }

    private File adjustMediaRecorderParameters(MediaRecorder.OnInfoListener listener)
            throws IOException {
        _mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        _mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        _mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        _mediaRecorder.setVideoEncodingBitRate(VIDEO_ENCODING_BIT_RATE);
        _mediaRecorder.setVideoFrameRate(VIDEO_FRAME_RATE);
        _mediaRecorder.setVideoSize(_optimalSize.width,
                _optimalSize.height);
        _mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        _mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        _mediaRecorder.setMaxDuration(VIDEO_MAX_DURATION);
        _mediaRecorder.setAudioEncodingBitRate(AUDIO_ENCODING_BIT_RATE);
        _mediaRecorder.setAudioChannels(AUDIO_CHANNELS);
        _mediaRecorder.setAudioSamplingRate(AUDIO_SAMPLING_RATE);
        if (listener != null)
            _mediaRecorder.setOnInfoListener(listener);

        File outputMediaFile = getOutputMediaFile();
        if (outputMediaFile == null) {
            throw new IOException("Output media file could not be created");
        }
        _mediaRecorder.setOutputFile(outputMediaFile.getPath());
        return outputMediaFile;
    }

    /**
     * Obtain an instance of Camera from open(int).
     */
    public void step1(CameraOpenListener listener) {
        Step1AsyncTask at = new Step1AsyncTask();

        at.execute(listener);
    }

    /**
     * Obtain an instance of Camera from open(int).
     */
    public void step1(CameraOpenListener listener, int whichCamera) {
        Step1AsyncTask at = new Step1AsyncTask(whichCamera);

        at.execute(listener);
    }

    /**
     * Get existing (default) settings with getParameters().
     */
    public Camera.Parameters step2() {
        Camera.Parameters parameters = _camera.getParameters();
        List<Camera.Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
        List<Camera.Size> supportedVideoSizes = parameters.getSupportedVideoSizes();
        _optimalSize = getOptimalVideoSize(
                supportedVideoSizes,
                supportedPreviewSizes);
        return parameters;
    }

    /**
     * If necessary, modify the returned Camera.Parameters object and call
     * setParameters(Camera.Parameters).
     */
    public void step3(Camera.Parameters parameters) {
        _camera.setParameters(adjustCameraParameters(parameters));

    }

    /**
     * Call setDisplayOrientation(int) to ensure correct orientation of preview.
     */
    public void step4(Activity cameraActivity) {
        int orientation = getDisplayOrientation(cameraActivity,
                Camera.CameraInfo.CAMERA_FACING_BACK, _camera);
        _camera.setDisplayOrientation(orientation);
    }

    /**
     * Important: Pass a fully initialized SurfaceHolder to setPreviewDisplay(SurfaceHolder).
     * Without a surface, the camera will be unable to start the preview.
     */
    public void step5(SurfaceTexture surfaceTexture) throws IOException {
        _camera.setPreviewTexture(surfaceTexture);

    }

    /**
     * Important: Call startPreview() to start updating the preview surface. Preview must be started
     * before you can take a picture.
     */
    public void step6() {
        _camera.startPreview();
        _isCameraReady = true;
        _isMediaRecorderReady = true;
    }

    /**
     * When you want, call takePicture(Camera.ShutterCallback, Camera.PictureCallback,
     * Camera.PictureCallback, Camera.PictureCallback) to capture a photo. Wait for the callbacks to
     * provide the actual image data.
     */
    public void step7() {
        // TODO
    }

    /**
     * After taking a picture, preview display will have stopped. To take more photos, call
     * startPreview() again first.
     */
    public void step8() {
        // TODO
    }

    /**
     * Call stopPreview() to stop updating the preview surface.
     */
    public void step9() {
        _camera.stopPreview();
        _isCameraReady = false;
        _isMediaRecorderReady = false;
    }

    /**
     * Important: Call release() to release the camera for use by other applications. Applications
     * should release the camera immediately in onPause() (and re-open() it in onResume()).
     */
    public void step10() {
        _camera.release();
        _isCameraReady = false;
        _isMediaRecorderReady = false;
    }

    /**
     * Obtain and initialize a Camera and start preview as described above.
     */
    public void step1VideoRecording(Activity activity, SurfaceTexture surfaceTexture) {
        step1(new VideoRecordingCameraOpenListener(activity, surfaceTexture));
    }

    /**
     * Obtain and initialize a Camera and start preview as described above.
     */
    public void step1VideoRecording(Activity activity, SurfaceTexture surfaceTexture,
                                    int whichCamera) {
        step1(new VideoRecordingCameraOpenListener(activity, surfaceTexture), whichCamera);
    }

    /**
     * Call unlock() to allow the media process to access the camera.
     */
    public void step2VideoRecording() {
        _camera.unlock();
    }

    /**
     * Pass the camera to setCamera(Camera). See MediaRecorder information about video recording.
     */
    public void step3VideoRecording(MediaRecorder.OnInfoListener listener) throws IOException {
        _mediaRecorder.setCamera(_camera);
        _outputMediaFile = adjustMediaRecorderParameters(listener);
        _mediaRecorder.prepare();
    }

    /**
     * When finished recording, call reconnect() to re-acquire and re-lock the camera.
     */
    public void step4VideoRecording() throws IOException {
        _camera.reconnect();
    }

    /**
     * If desired, restart preview and take more photos or videos.
     */
    public void step5VideoRecording() {
        _camera.startPreview();
    }

    /**
     * Call stopPreview() and release() as described above.
     */
    public void step6VideoRecording() {
        step9();
        step10();
    }

    public boolean isCameraReady() {
        return _isCameraReady;
    }

    public boolean isMediaRecorderReady() {
        return _isMediaRecorderReady;
    }

    public boolean isRecording() {
        return _isRecording;
    }

    public void takePicture() {
        // TODO
        _isCameraReady = false;
    }

    public void record() {
        _mediaRecorder.start();
        _isRecording = true;
    }

    public File stop() {
        try {
            _isCameraReady = false;
            _isMediaRecorderReady = false;
            _mediaRecorder.stop();
        } catch (RuntimeException e) {
            if (_outputMediaFile != null) {
                _outputMediaFile.delete();
            }
        }
        _isRecording = false;

        return _outputMediaFile;
    }

    private static class Step1AsyncTask extends AsyncTask<CameraOpenListener, Void, Camera> {
        private CameraOpenListener[] _vListener = null;
        private int _whichCamera;

        public Step1AsyncTask() {
            _whichCamera = Camera.CameraInfo.CAMERA_FACING_BACK;
        }

        public Step1AsyncTask(int whichCamera) {
            _whichCamera = whichCamera;
        }

        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p/>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param listeners The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected Camera doInBackground(CameraOpenListener... listeners) {
            _vListener = listeners;

            try {
                return Camera.open(_whichCamera);
            } catch (Exception e) {
                try {
                    return Camera.open(_whichCamera == Camera.CameraInfo.CAMERA_FACING_FRONT ?
                            Camera.CameraInfo.CAMERA_FACING_BACK : Camera.CameraInfo.CAMERA_FACING_FRONT);
                } catch (Exception e1) {
                    return null;
                }
            }
        }

        @Override
        protected void onPostExecute(Camera camera) {
            if (_vListener != null) {
                for (CameraOpenListener listener : _vListener)
                    listener.onCameraOpened(camera);
            }
            super.onPostExecute(camera);
        }
    }

    public interface CameraOpenListener {
        void onCameraOpened(Camera camera);
    }

    private class VideoRecordingCameraOpenListener implements CameraOpenListener {
        private Activity _activity;
        private SurfaceTexture _surfaceTexture;

        public VideoRecordingCameraOpenListener(Activity activity, SurfaceTexture surfaceTexture) {
            _activity = activity;
            _surfaceTexture = surfaceTexture;
        }

        public void onCameraOpened(Camera camera) {
            if (camera == null) {
                ToastUtils.showToast(_activity.getString(R.string.activity_camera_tFailed));
                _activity.finish();
                return;
            }
            _camera = camera;
            step3(step2());
            step4(_activity);
            try {
                step5(_surfaceTexture);
            } catch (IOException e) {
                // close the camera?
                // throw exception
            }
            step6();
        }
    }

}
