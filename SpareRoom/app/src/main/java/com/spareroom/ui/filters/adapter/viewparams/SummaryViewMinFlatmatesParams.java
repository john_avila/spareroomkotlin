package com.spareroom.ui.filters.adapter.viewparams;

import com.spareroom.model.business.SearchAdvertListPropertiesOffered.MinFlatmates;

public class SummaryViewMinFlatmatesParams extends AbstractSummaryViewParams {
    private MinFlatmates minFlatmates;

    public SummaryViewMinFlatmatesParams(String tag) {
        super(tag);
    }

    public MinFlatmates minFlatmates() {
        return minFlatmates;
    }

    public void minFlatmates(MinFlatmates minFlatmates) {
        this.minFlatmates = minFlatmates;
    }

    public void reset() {
        super.reset();
        minFlatmates(MinFlatmates.NOT_SET);
    }
}
