package com.spareroom.ui.flow;

import com.spareroom.model.business.DraftAd;
import com.spareroom.ui.screen.legacy.*;

public class PlaceAdStateLoggedOutWanted extends FlowState {

    private static final String PAGE_WANTED_1 = PlaceAdWelcomeFragment.class.getName();
    private static final String PAGE_WANTED_2 = PlaceAdAdvertiseFragment.class.getName();
    private static final String PAGE_WANTED_3 = PlaceAdWantedIAmFragment.class.getName();
    private static final String PAGE_WANTED_4 = PlaceAdWantedWantAddPhotoFragment.class.getName(); // pending
    private static final String PAGE_WANTED_4A = PlaceAdPhotoChosenFragment.class.getName();
    private static final String PAGE_WANTED_5 = PlaceAdWantedAgeFragment.class.getName();
    private static final String PAGE_WANTED_6 = PlaceAdWantedOccupationFragment.class.getName();
    private static final String PAGE_WANTED_7 = PlaceAdWantedPetsFragment.class.getName();
    private static final String PAGE_WANTED_8 = PlaceAdWantedSmokeFragment.class.getName();
    private static final String PAGE_WANTED_9 = PlaceAdWantedHappyToLiveWithFragment.class.getName(); // pending
    private static final String PAGE_WANTED_10 = PlaceAdWantedHappyAnyAgeFragment.class.getName();
    private static final String PAGE_WANTED_11 = PlaceAdWantedKindOfRoomFragment.class.getName();
    private static final String PAGE_WANTED_12 = PlaceAdWantedBuddyUpFragment.class.getName();
    private static final String PAGE_WANTED_13 = PlaceAdWantedAreasLookingInFragment.class.getName(); // pending
    private static final String PAGE_WANTED_14 = PlaceAdWantedBudgetFragment.class.getName();
    private static final String PAGE_WANTED_15 = PlaceAdWantedAvailabilityPeriodFragment.class.getName();
    private static final String PAGE_WANTED_16 = PlaceAdWantedStayAccommodationFragment.class.getName();
    private static final String PAGE_WANTED_17 = PlaceAdWantedPhoneFragment.class.getName();
    private static final String PAGE_WANTED_18 = PlaceAdTitleDescriptionFragment.class.getName();
    private static final String PAGE_WANTED_19 = PlaceAdUploadingFragment.class.getName();
    private static final String PAGE_WANTED_LAST = PlaceAdUploadCompleteFragment.class.getName();

    public PlaceAdStateLoggedOutWanted() {
        SCREENS = new String[]{
                PAGE_WANTED_1,
                PAGE_WANTED_2,
                PAGE_WANTED_3,
                PAGE_WANTED_4,
                PAGE_WANTED_4A,
                PAGE_WANTED_5,
                PAGE_WANTED_6,
                PAGE_WANTED_7,
                PAGE_WANTED_8,
                PAGE_WANTED_9,
                PAGE_WANTED_10,
                PAGE_WANTED_11,
                PAGE_WANTED_12,
                PAGE_WANTED_13,
                PAGE_WANTED_14,
                PAGE_WANTED_15,
                PAGE_WANTED_16,
                PAGE_WANTED_17,
                PAGE_WANTED_18,
                PAGE_WANTED_19,
                PAGE_WANTED_LAST
        };
        init();
    }

    @Override
    public String getPreviousFragmentName(String currentScreen, Object businessObject) {
        String previousScreen = super.getPreviousFragmentName(currentScreen, businessObject);
        DraftAd draftAd = (DraftAd) businessObject;

        if (currentScreen == null)
            return previousScreen;

        if (currentScreen.equals(PAGE_WANTED_5)) {
            if (!draftAd.hasPicture()) {
                previousScreen = super.getPreviousFragmentName(previousScreen, draftAd);
            }
        }

        return previousScreen;
    }

    @Override
    public String getNextFragmentName(String currentScreen, Object businessObject) {
        String nextScreen = super.getNextFragmentName(currentScreen, businessObject);
        DraftAd draftAd = (DraftAd) businessObject;

        if (currentScreen == null)
            return nextScreen;

        if (currentScreen.equals(PAGE_WANTED_4)) {
            if (!draftAd.hasPicture())
                nextScreen = super.getNextFragmentName(nextScreen, draftAd);
        }

        return nextScreen;
    }
}
