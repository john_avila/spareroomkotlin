package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.model.business.DraftAdOffered;

public class PlaceAdOfferedBedroomsFragment extends PlaceAdFragment {
    private TextView _tvBedrooms;
    private boolean _areThereMoreFlatmates;

    private DraftAdOffered _draft;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_offered_bedrooms, container, false);
        setTitle(getString(R.string.place_ad_offered_bedrooms));

        _tvBedrooms = rootView.findViewById(R.id.place_ad_offered_bedrooms_tvBedrooms);
        SeekBar _sbBedrooms = rootView.findViewById(R.id.place_ad_offered_bedrooms_sbBedrooms);
        Button _bConfirm = rootView.findViewById(R.id.place_ad_bConfirm);

        _draft = (DraftAdOffered) getDraftAd();

        _areThereMoreFlatmates = ((_draft.get_numExistingGuys() + _draft.get_numExistingGirls()) > 0);

        if (_draft.get_rooms() != null) {
            int max = 12 - _draft.get_rooms().size();
            if (_areThereMoreFlatmates) {
                max -= 1;
            }
            _sbBedrooms.setMax(max);
            _tvBedrooms.setText(String.format(AppVersion.flavor().getLocale(), "%d", 12 - max));
        } else {
            int max = 12 - _draft.get_rooms().size();
            if (_areThereMoreFlatmates) {
                max -= 1;
            }
            _sbBedrooms.setMax(max);
            _tvBedrooms.setText(String.format(AppVersion.flavor().getLocale(), "%d", 12 - max));
        }

        if (_draft.get_numBedroom() > 0) {
            int min = 0;
            if (_draft.get_rooms() != null)
                min = min + _draft.get_rooms().size();
            if (_areThereMoreFlatmates)
                min += 1;
            _tvBedrooms.setText(String.format(AppVersion.flavor().getLocale(), "%d", _draft.get_numBedroom()));
            _sbBedrooms.setProgress(_draft.get_numBedroom() - min);
        } else {
            int min = 0;
            if (_draft.get_rooms() != null)
                min = min + _draft.get_rooms().size();
            if (_areThereMoreFlatmates)
                min += 1;
            _tvBedrooms.setText(String.format(AppVersion.flavor().getLocale(), "%d", min));
            _sbBedrooms.setProgress(0);
        }
        _sbBedrooms.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int numberRooms = _draft.get_rooms().size() + progress;
                if (_areThereMoreFlatmates) {
                    numberRooms += 1;
                }
                _tvBedrooms.setText(String.format(AppVersion.flavor().getLocale(), "%d", numberRooms));
            }
        });

        _bConfirm.setOnClickListener(v -> {
            ((DraftAdOffered) getDraftAd()).set_numBedroom(Integer.parseInt(_tvBedrooms.getText().toString()));
            finish();
        });
        return rootView;
    }

}
