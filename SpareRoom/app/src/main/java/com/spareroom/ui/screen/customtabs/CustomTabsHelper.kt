package com.spareroom.ui.screen.customtabs

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.net.Uri
import java.util.*

private const val STABLE_PACKAGE = "com.android.chrome"
private const val BETA_PACKAGE = "com.chrome.beta"
private const val DEV_PACKAGE = "com.chrome.dev"
private const val LOCAL_PACKAGE = "com.google.android.apps.chrome"

private const val EXTRA_CUSTOM_TABS_KEEP_ALIVE = "android.support.customtabs.extra.KEEP_ALIVE"
private const val ACTION_CUSTOM_TABS_CONNECTION = "android.support.customtabs.action.CustomTabsService"
private const val HTTP_URL = "http://www.example.com"

object CustomTabsHelper {

    private var packageNameToUse = ""

    fun addKeepAliveExtra(context: Context, intent: Intent) {
        intent.putExtra(EXTRA_CUSTOM_TABS_KEEP_ALIVE, Intent().setClassName(context.packageName, KeepAliveService::class.java.canonicalName))
    }

    fun getPackageNameToUse(context: Context): String {
        if (packageNameToUse.isNotBlank())
            return packageNameToUse

        val packageManager = context.packageManager

        // Get default VIEW intent handler.
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(HTTP_URL))
        val defaultViewHandler = packageManager.resolveActivity(intent, 0)?.activityInfo?.packageName ?: ""

        // Get all apps that can handle VIEW intents.
        val resolvedActivities = packageManager.queryIntentActivities(intent, 0)
        val packagesSupportingTabs = resolvedActivities
            .filter { canHandleCustomTabsIntent(it, packageManager) }
            .mapTo(ArrayList<String>()) { it.activityInfo.packageName }

        // Now packagesSupportingTabs contains all apps that can handle both VIEW intents and service calls.
        packageNameToUse = when {
            packagesSupportingTabs.isEmpty() -> ""
            packagesSupportingTabs.size == 1 -> packagesSupportingTabs[0]
            shouldUseDefaultViewHandler(defaultViewHandler, packagesSupportingTabs, intent, context) -> defaultViewHandler
            packagesSupportingTabs.contains(STABLE_PACKAGE) -> STABLE_PACKAGE
            packagesSupportingTabs.contains(BETA_PACKAGE) -> BETA_PACKAGE
            packagesSupportingTabs.contains(DEV_PACKAGE) -> DEV_PACKAGE
            packagesSupportingTabs.contains(LOCAL_PACKAGE) -> LOCAL_PACKAGE
            else -> ""
        }

        return packageNameToUse
    }

    private fun canHandleCustomTabsIntent(info: ResolveInfo, packageManager: PackageManager): Boolean {
        val serviceIntent = Intent()
        serviceIntent.action = ACTION_CUSTOM_TABS_CONNECTION
        serviceIntent.`package` = info.activityInfo.packageName

        return packageManager.resolveService(serviceIntent, 0) != null
    }

    private fun shouldUseDefaultViewHandler(defaultViewHandler: String, packagesSupportingTabs: ArrayList<String>, activityIntent: Intent, context: Context): Boolean {
        return defaultViewHandler.isNotBlank() && !hasSpecializedHandlerIntents(context, activityIntent) && packagesSupportingTabs.contains(defaultViewHandler)
    }

    private fun hasSpecializedHandlerIntents(context: Context, intent: Intent): Boolean {
        return try {
            val handlers = context.packageManager.queryIntentActivities(intent, PackageManager.GET_RESOLVED_FILTER) ?: return false
            handlers.any { it.filter != null && it.filter.countDataAuthorities() != 0 && it.filter.countDataPaths() != 0 && it.activityInfo != null }
        } catch (e: RuntimeException) {
            false
        }
    }
}
