package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.widget.params.CompoundButtonParams;
import com.spareroom.ui.widget.params.MinMaxEditTextParams;

import java.util.*;

import androidx.annotation.NonNull;

class FilterOfferedBudgetItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.monthly_budget;

    final static String PRICE_RANGE = "price_range";
    final static String BILLS_INCLUDED = "bills_included";

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesOffered previousProperties = (SearchAdvertListPropertiesOffered) properties;
        List<FilterItem> list = new ArrayList<>();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        MinMaxEditTextParams priceRange = new MinMaxEditTextParams(PRICE_RANGE);
        priceRange.iconId(R.drawable.ic_budget);
        priceRange.title(application.getString(R.string.budgetRange));
        priceRange.textBase(application.getString(R.string.currency_symbol));
        priceRange.minHint(application.getString(R.string.minRentHint));
        priceRange.minTitle(application.getString(R.string.minimum));
        priceRange.maxHint(application.getString(R.string.maxRentHint));
        priceRange.maxTitle(application.getString(R.string.maximum));
        priceRange.minMaxCharacters(5);
        priceRange.maxMaxCharacters(5);
        priceRange.min(previousProperties.getMinRent());
        priceRange.max(previousProperties.getMaxRent());
        list.add(new FilterItem<>(priceRange));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        CompoundButtonParams billsIncluded = new CompoundButtonParams(BILLS_INCLUDED);
        billsIncluded.size(CompoundButtonParams.Size.MEDIUM);
        billsIncluded.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        billsIncluded.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        billsIncluded.enableHighlightBar(true);
        billsIncluded.text(application.getString(R.string.billsIncluded));
        billsIncluded.textColorResId(R.color.black);
        billsIncluded.iconResId(R.drawable.ic_bills);
        billsIncluded.selected(previousProperties.billsInc());
        list.add(new FilterItem<>(billsIncluded));

        this.list = list;
    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return null;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesOffered propertiesOffered = (SearchAdvertListPropertiesOffered) propertiesToUpdate;
        for (FilterItem item : list) {
            switch (item.tag()) {
                case PRICE_RANGE:
                    propertiesOffered.minMonthlyRent(((MinMaxEditTextParams) item.params()).min());
                    propertiesOffered.maxMonthlyRent(((MinMaxEditTextParams) item.params()).max());
                    break;
                case BILLS_INCLUDED:
                    propertiesOffered.billsInc(((CompoundButtonParams) item.params()).selected());
                    break;
            }
        }

        if (sortProperties)
            sortPriceRange(propertiesOffered);

    }

}
