package com.spareroom.ui.controller;

import com.spareroom.lib.core.IObserver;
import com.spareroom.model.business.ErrorCause;

/**
 * Created by manuel on 18/11/2016.
 */

public class ErrorStateController {
    private final ViewUiController _processCompositeUiController;
    private volatile boolean _isErrorDisplayed = false;

    private final IObserver inputObserver = new IObserver() {
        @Override
        public void notifyObserver(Object o) {
            if (_isErrorDisplayed) {
                _isErrorDisplayed = false;
                _processCompositeUiController.enable();
            }
        }
    };

    private final IObserver errorObserver = o -> _isErrorDisplayed = true;

    public ErrorStateController(ViewUiController processCompositeUiController) {
        _processCompositeUiController = processCompositeUiController;
    }

    public IObserver getInputObserver() {
        return inputObserver;
    }

    public IObserver getErrorObserver() {
        return errorObserver;
    }

    public static boolean isErrorCausedByUserDetails(String cause) {
        return ErrorCause.FIRST_NAME.equalsIgnoreCase(cause) || ErrorCause.LAST_NAME.equalsIgnoreCase(cause) || ErrorCause.NEW_EXISTING_USER.equalsIgnoreCase(cause);
    }

} //end class ErrorStateController