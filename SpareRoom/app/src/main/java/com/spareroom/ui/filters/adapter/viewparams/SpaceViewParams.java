package com.spareroom.ui.filters.adapter.viewparams;

import com.spareroom.ui.widget.params.ViewParamsType;

public class SpaceViewParams implements IResettableParams {
    private final SpaceType spaceType;
    private final static String TAG = "key_space";

    public enum SpaceType {SMALL, DIVIDER, BIG}

    public SpaceViewParams(SpaceType spaceType) {
        this.spaceType = spaceType;
    }

    @Override
    public void reset() {
    }

    @Override
    public String tag() {
        return TAG;
    }

    @Override
    public ViewParamsType type() {
        return ViewParamsType.SPACE;
    }

    public SpaceType spaceType() {
        return spaceType;
    }

}
