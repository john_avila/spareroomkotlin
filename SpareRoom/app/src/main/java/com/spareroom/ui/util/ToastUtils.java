package com.spareroom.ui.util;

import android.widget.Toast;

import com.spareroom.App;

public class ToastUtils {

    public static void showToast(String message) {
        Toast.makeText(App.get(), message, Toast.LENGTH_LONG).show();
    }

    public static void showToast(int messageResourceId) {
        showToast(App.get().getString(messageResourceId));
    }
}
