package com.spareroom.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.google.android.material.appbar.AppBarLayout;
import com.spareroom.R;

import androidx.annotation.Keep;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

/**
 * Keep this class. It is used in material_conversation_activity - app:layout_behavior="com.spareroom.ui.FooterScrollingBehavior"
 */
@Keep
public class FooterScrollingBehavior extends AppBarLayout.ScrollingViewBehavior {

    private View toolbar;

    public FooterScrollingBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View viewPager, View appBarLayout) {
        boolean result = super.onDependentViewChanged(parent, viewPager, appBarLayout);
        if (toolbar == null)
            toolbar = appBarLayout.findViewById(R.id.toolbar);

        if (viewPager != null && toolbar != null) {
            viewPager.setPadding(viewPager.getPaddingLeft(), viewPager.getPaddingTop(),
                    viewPager.getPaddingRight(), toolbar.getHeight() + appBarLayout.getTop());
        }
        return result;
    }
}
