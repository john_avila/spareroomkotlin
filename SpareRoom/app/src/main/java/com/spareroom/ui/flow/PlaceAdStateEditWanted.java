package com.spareroom.ui.flow;

import com.spareroom.controller.AppVersion;
import com.spareroom.ui.screen.legacy.*;

public class PlaceAdStateEditWanted extends FlowState {

    private static final String PLACE_AD_WANTED_AGE_FRAGMENT = PlaceAdWantedAgeFragment.class.getName();
    private static final String PLACE_AD_WANTED_OCCUPATION_FRAGMENT = PlaceAdWantedOccupationFragment.class.getName();
    private static final String PLACE_AD_WANTED_DSS_FRAGMENT = PlaceAdWantedDssFragment.class.getName();
    private static final String PLACE_AD_WANTED_PETS_FRAGMENT = PlaceAdWantedPetsFragment.class.getName();
    private static final String PLACE_AD_WANTED_SMOKE_FRAGMENT = PlaceAdWantedSmokeFragment.class.getName();
    private static final String PLACE_AD_WANTED_INTERESTS_FRAGMENT = PlaceAdWantedInterestsFragment.class.getName();
    private static final String PLACE_AD_WANTED_HAPPY_TO_LIVE_WITH_FRAGMENT = PlaceAdWantedHappyToLiveWithFragment.class.getName(); // pending
    private static final String PLACE_AD_WANTED_HAPPY_ANY_AGE_FRAGMENT = PlaceAdWantedHappyAnyAgeFragment.class.getName();
    private static final String PLACE_AD_WANTED_KIND_OF_ROOM_FRAGMENT = PlaceAdWantedKindOfRoomFragment.class.getName();
    private static final String PLACE_AD_WANTED_BUDDY_UP_FRAGMENT = PlaceAdWantedBuddyUpFragment.class.getName();
    private static final String PLACE_AD_WANTED_AREAS_LOOKING_IN_FRAGMENT = PlaceAdWantedEditAreasLookingInFragment.class.getName(); // pending
    private static final String PLACE_AD_WANTED_BUDGET_FRAGMENT = PlaceAdWantedBudgetFragment.class.getName();
    private static final String PLACE_AD_WANTED_AVAILABILITY_PERIOD_FRAGMENT = PlaceAdWantedAvailabilityPeriodFragment.class.getName();
    private static final String PLACE_AD_WANTED_STAY_ACCOMMODATION_FRAGMENT = PlaceAdWantedStayAccommodationFragment.class.getName();
    private static final String PLACE_AD_WANTED_AMENITY_FRAGMENT = PlaceAdWantedAmenityFragment.class.getName();
    private static final String PLACE_AD_WANTED_PHONE_FRAGMENT = PlaceAdWantedPhoneFragment.class.getName();
    private static final String PLACE_AD_WANTED_TITLE_DESCRIPTION_FRAGMENT = PlaceAdTitleDescriptionFragment.class.getName();

    private static final String PLACE_AD_UPLOADING_FRAGMENT = PlaceAdEditUploadingFragment.class.getName();
    private static final String PLACE_AD_UPLOAD_COMPLETE_FRAGMENT = PlaceAdUploadCompleteFragment.class.getName();

    public PlaceAdStateEditWanted() {
        if (AppVersion.isUk()) {
            SCREENS = new String[]{
                    PLACE_AD_WANTED_AGE_FRAGMENT,
                    PLACE_AD_WANTED_OCCUPATION_FRAGMENT,
                    PLACE_AD_WANTED_DSS_FRAGMENT,
                    PLACE_AD_WANTED_PETS_FRAGMENT,
                    PLACE_AD_WANTED_SMOKE_FRAGMENT,
                    PLACE_AD_WANTED_INTERESTS_FRAGMENT,
                    PLACE_AD_WANTED_HAPPY_TO_LIVE_WITH_FRAGMENT,
                    PLACE_AD_WANTED_HAPPY_ANY_AGE_FRAGMENT,
                    PLACE_AD_WANTED_KIND_OF_ROOM_FRAGMENT,
                    PLACE_AD_WANTED_BUDDY_UP_FRAGMENT,
                    PLACE_AD_WANTED_AREAS_LOOKING_IN_FRAGMENT,
                    PLACE_AD_WANTED_BUDGET_FRAGMENT,
                    PLACE_AD_WANTED_AVAILABILITY_PERIOD_FRAGMENT,
                    PLACE_AD_WANTED_STAY_ACCOMMODATION_FRAGMENT,
                    PLACE_AD_WANTED_AMENITY_FRAGMENT,
                    PLACE_AD_WANTED_PHONE_FRAGMENT,
                    PLACE_AD_WANTED_TITLE_DESCRIPTION_FRAGMENT,
                    PLACE_AD_UPLOADING_FRAGMENT,
                    PLACE_AD_UPLOAD_COMPLETE_FRAGMENT
            };
        } else {
            SCREENS = new String[]{
                    PLACE_AD_WANTED_AGE_FRAGMENT,
                    PLACE_AD_WANTED_OCCUPATION_FRAGMENT,
                    PLACE_AD_WANTED_PETS_FRAGMENT,
                    PLACE_AD_WANTED_SMOKE_FRAGMENT,
                    PLACE_AD_WANTED_INTERESTS_FRAGMENT,
                    PLACE_AD_WANTED_HAPPY_TO_LIVE_WITH_FRAGMENT,
                    PLACE_AD_WANTED_HAPPY_ANY_AGE_FRAGMENT,
                    PLACE_AD_WANTED_KIND_OF_ROOM_FRAGMENT,
                    PLACE_AD_WANTED_BUDDY_UP_FRAGMENT,
                    PLACE_AD_WANTED_AREAS_LOOKING_IN_FRAGMENT,
                    PLACE_AD_WANTED_BUDGET_FRAGMENT,
                    PLACE_AD_WANTED_AVAILABILITY_PERIOD_FRAGMENT,
                    PLACE_AD_WANTED_STAY_ACCOMMODATION_FRAGMENT,
                    PLACE_AD_WANTED_AMENITY_FRAGMENT,
                    PLACE_AD_WANTED_PHONE_FRAGMENT,
                    PLACE_AD_WANTED_TITLE_DESCRIPTION_FRAGMENT,
                    PLACE_AD_UPLOADING_FRAGMENT,
                    PLACE_AD_UPLOAD_COMPLETE_FRAGMENT
            };
        }
        init();
    } //end PlaceAdStateEditWanted()

}
