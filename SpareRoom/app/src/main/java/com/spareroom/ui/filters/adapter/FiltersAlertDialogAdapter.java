package com.spareroom.ui.filters.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.ui.adapter.RecyclerViewAdapter;
import com.spareroom.ui.filters.adapter.FiltersAlertDialogAdapter.ViewHolder;

import java.util.List;

import androidx.core.content.ContextCompat;
import androidx.core.widget.CompoundButtonCompat;
import androidx.recyclerview.widget.RecyclerView;

public class FiltersAlertDialogAdapter extends RecyclerViewAdapter<String, ViewHolder> {
    private final ColorStateList colorStateListOrange;
    private final ColorStateList colorStateListGray;
    private int selectedOption;

    private OnClickListener onClickListener = view -> {
        notifyItemChanged(selectedOption);
        notifyItemChanged(selectedOption = (int) view.getTag(R.id.position_tag));
    };

    public FiltersAlertDialogAdapter(Context context, List<String> options, int selectedOption) {
        super(context);
        update(options, false);
        this.selectedOption = selectedOption;
        colorStateListGray = ContextCompat.getColorStateList(context, R.color.material_compound_button_selector_gray);
        colorStateListOrange = ContextCompat.getColorStateList(context, R.color.orange);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(getInflater().inflate(R.layout.alert_dialog_scrollable_content_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
        CompoundButtonCompat
                .setButtonTintList(viewHolder.radioButton, (position == 0 || position != selectedOption) ? colorStateListGray : colorStateListOrange);

        viewHolder.radioButton.setChecked(position == selectedOption);
        viewHolder.textView.setText(getItem(position));
        viewHolder.scrollableContent.setTag(R.id.position_tag, position);
        viewHolder.scrollableContent.setOnClickListener(onClickListener);
    }

    public int valueSelectedByPosition() {
        return selectedOption;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private View scrollableContent;
        private TextView textView;
        private RadioButton radioButton;

        private ViewHolder(View itemView) {
            super(itemView);
            scrollableContent = itemView.findViewById(R.id.scrollableContent);
            textView = itemView.findViewById(R.id.scrollableContent_option);
            radioButton = itemView.findViewById(R.id.scrollableContent_radioButton);
        }

    }

}
