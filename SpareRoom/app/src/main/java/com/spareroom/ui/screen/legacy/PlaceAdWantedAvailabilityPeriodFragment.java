package com.spareroom.ui.screen.legacy;

import android.app.Dialog;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.model.business.DraftAdWanted.AdvertiserType;
import com.spareroom.ui.screen.AlertDialogBuilder;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class PlaceAdWantedAvailabilityPeriodFragment extends PlaceAdFragment {
    private final String[] vMin = {"No min", "1 month", "2 months", "3 months", "4 months", "5 months", "6 months", "7 months", "8 months",
            "9 months", "10 months", "11 months", "12 months", "13 months", "14 months", "15 months", "16 months", "17 months", "18 months",
            "19 months", "20 months", "21 months", "22 months", "23 months", "24 months",};
    private final String[] vMax = {"No max", "1 month", "2 months", "3 months", "4 months", "5 months", "6 months", "7 months", "8 months",
            "9 months", "10 months", "11 months", "12 months", "13 months", "14 months", "15 months", "16 months", "17 months", "18 months",
            "19 months", "20 months", "21 months", "22 months", "23 months", "24 months", "25 months", "26 months", "27 months", "28 months",
            "29 months", "30 months", "31 months", "32 months", "33 months", "34 months", "35 months", "36 months"};

    private TextView _tvMin;
    private TextView _tvMax;
    private Button bConfirm;
    private View vMinMax;
    private int _min;
    private int _max;

    private DraftAdWanted _draft;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_wanted_availability_period, container, false);

        TextView tvDate = rootView.findViewById(R.id.place_ad_wanted_availability_period_tvDate);
        DatePicker dpAvailableFrom = rootView.findViewById(R.id.place_ad_wanted_availability_period_dpDate);
        vMinMax = rootView.findViewById(R.id.place_ad_wanted_availability_period_llMinMax);
        _tvMin = rootView.findViewById(R.id.place_ad_wanted_availability_period_tvMin);
        _tvMax = rootView.findViewById(R.id.place_ad_wanted_availability_period_tvMax);
        bConfirm = rootView.findViewById(R.id.place_ad_bConfirm);

        _draft = (DraftAdWanted) getDraftAd();

        // In case there is an activity state change and we lost the draft
        if (_draft == null) {
            getActivity().finish();

            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }

        if ((_draft.get_advertiserType() == AdvertiserType.MALE_FEMALE)
                || (_draft.get_advertiserType() == AdvertiserType.TWO_FEMALES)
                || (_draft.get_advertiserType() == AdvertiserType.TWO_MALES)) {
            tvDate.setText(R.string.place_ad_wanted_availability_period_moreThanOne_tvDate);
        } else {
            tvDate.setText(R.string.place_ad_wanted_availability_period_one_tvDate);
        }

        _min = _draft.get_minMonths();
        _max = _draft.get_maxMonths();

        Calendar savedDate = _draft.get_availableFrom();
        Calendar now = new GregorianCalendar();
        Calendar availableFrom = savedDate;

        if (!savedDate.after(now)) {
            availableFrom = now;
            _draft.set_availableFrom(availableFrom);
        }

        dpAvailableFrom.setMinDate(System.currentTimeMillis() - 1000);
        dpAvailableFrom.updateDate(availableFrom.get(Calendar.YEAR), availableFrom.get(Calendar.MONTH), availableFrom.get(Calendar.DAY_OF_MONTH));

        _tvMin.setText(vMin[_min]);
        _tvMax.setText(vMax[_max]);

        vMinMax.setOnClickListener(new MinMaxOnClickListener());
        setTitle(getString(R.string.place_ad_wanted_availability_period_title));

        bConfirm.setOnClickListener(new ConfirmOnClickListener(dpAvailableFrom));

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bConfirm.setOnClickListener(null);
        vMinMax.setOnClickListener(null);
    }

    private class ConfirmOnClickListener implements OnClickListener {
        private final DatePicker _dpAvailableFrom;

        ConfirmOnClickListener(DatePicker dpAvailableFrom) {
            _dpAvailableFrom = dpAvailableFrom;
        }

        @Override
        public void onClick(View v) {
            int day = _dpAvailableFrom.getDayOfMonth();
            int month = _dpAvailableFrom.getMonth();
            int year = _dpAvailableFrom.getYear();

            Calendar calendar = new GregorianCalendar();
            calendar.set(Calendar.DAY_OF_MONTH, day);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.YEAR, year);

            _draft.set_availableFrom(calendar);
            finish();
        }
    }

    private class MinMaxOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            View vDialog = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_place_ad_wanted_availability_period_months, null);
            NumberPicker npMin = vDialog.findViewById(R.id.place_ad_wanted_availability_date_dpMinDate);
            NumberPicker npMax = vDialog.findViewById(R.id.place_ad_wanted_availability_date_dpMaxDate);
            Button bOk = vDialog.findViewById(R.id.dialog_bPositive);
            Button bCancel = vDialog.findViewById(R.id.dialog_bNegative);

            npMin.setMinValue(0);
            npMin.setMaxValue(24);
            npMin.setDisplayedValues(vMin);
            npMin.setValue(_min);

            npMax.setMinValue(0);
            npMax.setMaxValue(36);
            npMax.setDisplayedValues(vMax);
            npMax.setValue(_max);

            Dialog d = new AlertDialogBuilder(getActivity(), vDialog).create();

            bOk.setOnClickListener(new OkOnClickListener(d, npMin, npMax));

            bCancel.setOnClickListener(new DialogUtil.CancelOnClickListener(d));
            d.show();
        }

        private class OkOnClickListener implements OnClickListener {
            private final Dialog _dialog;
            private final NumberPicker _npMin;
            private final NumberPicker _npMax;

            OkOnClickListener(Dialog d, NumberPicker npMin, NumberPicker npMax) {
                _npMin = npMin;
                _npMax = npMax;

                _npMin.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                _npMax.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                _dialog = d;
            }

            @Override
            public void onClick(View v) {
                int min = _npMin.getValue();
                int max = _npMax.getValue();

                if ((max < min) && (max != 0)) {
                    Toast.makeText(getActivity(), "Maximum cannot be less than minimum", Toast.LENGTH_LONG).show();
                    return;
                }

                _min = min;
                _max = max;

                _draft.set_minMonths(_min);
                _draft.set_maxMonths(_max);

                _tvMin.setText(vMin[_min]);
                _tvMax.setText(vMax[_max]);
                _dialog.dismiss();
            }
        }
    }
}
