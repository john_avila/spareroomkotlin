package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;

import com.spareroom.R;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.model.business.*;
import com.spareroom.ui.widget.PlaceAdButton;

public class PlaceAdAdvertiseFragment extends PlaceAdFragment {
    private DraftAd _draft;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.place_ad_advertise, container, false);
        PlaceAdButton bOffered = rootView.findViewById(R.id.place_ad_advertise_bOffered);
        PlaceAdButton bWanted = rootView.findViewById(R.id.place_ad_advertise_bWanted);

        setTitle(getString(R.string.place_ad_advertise_title));

        _draft = getDraftAd();

        if (_draft instanceof DraftAdOffered) {
            bOffered.setPreviouslySelected();
        } else if (_draft instanceof DraftAdWanted) {
            bWanted.setPreviouslySelected();
        }

        bOffered.setOnClickListener(v -> {

            AnalyticsTrackerComposite.getInstance().trackEvent_PlaceAd_StartPlaceOfferedAd();

            if (!(_draft instanceof DraftAdOffered)) {
                PlaceAdAbstractActivity thisActivity = (PlaceAdAbstractActivity) getActivity();

                if (getActivity() != null) {
                    DraftAdOffered offered = new DraftAdOffered();

                    offered.set_firstName(thisActivity.getDraftAd().get_firstName());
                    offered.set_lastName(thisActivity.getDraftAd().get_lastName());
                    offered.set_email(thisActivity.getDraftAd().get_email());

                    thisActivity.setDraftAd(offered);
                }
            }
            finish();
        });

        bWanted.setOnClickListener(v -> {

            AnalyticsTrackerComposite.getInstance().trackEvent_PlaceAd_StartPlaceWantedAd();

            if (!(_draft instanceof DraftAdWanted)) {
                PlaceAdAbstractActivity thisActivity = (PlaceAdAbstractActivity) getActivity();

                if (getActivity() != null) {
                    DraftAdWanted wanted = new DraftAdWanted();

                    wanted.set_firstName(thisActivity.getDraftAd().get_firstName());
                    wanted.set_lastName(thisActivity.getDraftAd().get_lastName());
                    wanted.set_email(thisActivity.getDraftAd().get_email());

                    thisActivity.setDraftAd(wanted);
                }
            }
            finish();
        });
        return rootView;
    }
}
