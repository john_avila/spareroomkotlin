package com.spareroom.ui.util;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    private final Rect bounds = new Rect();
    private final Drawable divider;
    private boolean hideLastDivider;

    public DividerItemDecoration(Drawable divider, boolean hideLastDivider) {
        this.divider = divider;
        this.hideLastDivider = hideLastDivider;
    }

    @SuppressLint("NewApi")
    @Override
    public void onDraw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
        canvas.save();
        final int left;
        final int right;
        if (parent.getClipToPadding()) {
            left = parent.getPaddingLeft();
            right = parent.getWidth() - parent.getPaddingRight();
            canvas.clipRect(left, parent.getPaddingTop(), right,
                    parent.getHeight() - parent.getPaddingBottom());
        } else {
            left = 0;
            right = parent.getWidth();
        }

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            parent.getDecoratedBoundsWithMargins(child, bounds);
            final int bottom = bounds.bottom + Math.round(ViewCompat.getTranslationY(child));
            final int top = bottom - divider.getIntrinsicHeight();
            divider.setBounds(left, isLastItem(parent, child) && hideLastDivider ? bottom : top, right, bottom);
            divider.draw(canvas);
        }
        canvas.restore();
    }

    private boolean isLastItem(RecyclerView parent, View child) {
        return parent.getAdapter() != null && parent.getChildAdapterPosition(child) == parent.getAdapter().getItemCount() - 1;
    }
}
