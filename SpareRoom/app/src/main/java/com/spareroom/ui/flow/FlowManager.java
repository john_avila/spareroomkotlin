package com.spareroom.ui.flow;

/**
 * Manager for the FlowStates
 * <p>
 * Created by miguel.rossi on 18/07/2016.
 */
public class FlowManager {
    private FlowState STATE;

    public FlowManager() {

    }

    public FlowManager(FlowState state) {
        init(state);
    }

    protected void init(FlowState state) {
        setState(state);
    }

    /**
     * Returns fragment at <code>pos</code> position.
     *
     * @param pos position in the fragment list
     * @return name of the fragment, <code>null</code> if out ouf bounds
     */
    public String getFragmentAtPosition(int pos) {
        return STATE.fragmentAtPosition(pos);
    } // end getFragment(int pos)

    /**
     * Returns the position of a fragment.
     *
     * @param fragmentName the fragment to know about
     * @return the position of the fragment; <code>-1</code> if the fragment doesn't exist in the list
     */
    public int getFragmentPosition(String fragmentName) {
        return STATE.fragmentPosition(fragmentName);
    }

    /**
     * Let us know the previous fragment name.
     *
     * @param currentFragment the current fragment
     * @param businessObject  the business object completed in the multi step process;
     *                        <code>null</code> if there is no business object involved, it can
     *                        have data that influences in the flow of the fragments
     * @return the previous fragment name; <code>null</code> if there is no previous fragment for this
     * position
     */
    public String getPreviousFragmentName(String currentFragment, Object businessObject) {
        return STATE.getPreviousFragmentName(currentFragment, businessObject);
    }

    /**
     * Returns the following fragment name.
     *
     * @param currentFragment the name for the current fragment; if <code>null</code> it returns the
     *                        first fragment of the list
     * @param businessObject  the business object completed in the multi step process;
     *                        <code>null</code> if there is no business object involved, it can
     *                        have data that influences in the flow of the fragments
     * @return the following fragment name; <code>null</code> if no fragments are set for this state
     * or the current fragment is the last of the list
     */
    public String getNextFragmentName(String currentFragment, Object businessObject) {
        return STATE.getNextFragmentName(currentFragment, businessObject);
    }

    /**
     * Lets us know if it is the last fragment.
     *
     * @param fragment the fragment to check
     * @return <code>true</code> if yes; <code>false</code> if not; <code>null</code> if the
     * <code>fragment</code> is not in the list
     */
    public Boolean isLastFragment(String fragment) {
        return STATE.isLastFragment(fragment);
    }

    /**
     * Lets us know if it is the first fragment.
     *
     * @param fragment the fragment to check
     * @return <code>true</code> if yes; <code>false</code> if not; <code>null</code> if the
     * <code>fragment</code> is not in the list
     */
    public Boolean isFirstFragment(String fragment) {
        return STATE.isFirstFragment(fragment);
    }

    public void setState(FlowState state) {
        STATE = state;
    }

    public FlowState getState() {
        return STATE;
    }

    /**
     * Returns the position of the fragment in percentage with the fragment list
     *
     * @param fragment the fragment to check
     * @return the percentage
     */
    public int getProgress(String fragment) {
        int progress = 0;
        if (STATE.getProgressCalculator().progress(fragment) != -1)
            progress = STATE.getProgressCalculator().progress(fragment);

        return progress;
    }

}
