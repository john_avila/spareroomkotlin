package com.spareroom.ui.screen;

import android.os.Bundle;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.lib.core.IObservable;
import com.spareroom.lib.core.IObserver;
import com.spareroom.model.business.AccountRegisterForm;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.ui.FieldValidator;
import com.spareroom.ui.NameFieldValidator;
import com.spareroom.ui.controller.*;
import com.spareroom.ui.util.KeyboardUtils;
import com.spareroom.ui.widget.TextBox;

import java.util.LinkedList;

import androidx.annotation.Nullable;

import static com.spareroom.ui.screen.RegisterActivity.KEY_REGISTER_FORM;

/**
 * Fragment for set the name for the registration process
 */
public class RegisterNameFragment extends Fragment {

    //region FIELDS UI.CONTROL

    private RegisterProcessCompositeUiController _registerProcessCompositeUiController;

    //endregion FIELDS UI.CONTROL

    private TextBox tbName;

    //region METHODS LIFECYCLE

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        AccountRegisterForm form = ((AccountRegisterForm) ((LinearMultiStepActivity) getActivity()).getSharedObject(KEY_REGISTER_FORM));
        String firstName, lastName;
        // Views
        View rootView = inflater.inflate(R.layout.material_register_name_fragment, container, false);
        // Name input
        tbName = rootView.findViewById(R.id.registerNameFragment_nameTextBox);
        // Bottom Toolbar
        View bottomToolbar = getActivity().findViewById(R.id.activity_toolbarBottom);
        final TextView tvBottomToolbarRight = bottomToolbar.findViewById(R.id.materialToolbarBottom_rightText);
        // Controllers
        ActionButtonUiController registerButtonUiController = new ActionButtonUiController(bottomToolbar);
        ViewUiController registerNameUiController = new RegisterNameUiController(tbName);
        _registerProcessCompositeUiController =
                new RegisterProcessCompositeUiController(registerNameUiController, new ActionButtonUiController(tvBottomToolbarRight));
        ErrorStateController errorStateController = new ErrorStateController(_registerProcessCompositeUiController);

        _registerProcessCompositeUiController.addObserver(errorStateController.getErrorObserver());

        // Toolbar
        setToolbar(getActivity().findViewById(R.id.appBarLayout), getString(R.string.registerNameFragment_toolbarTitle), true);

        // Bottom Toolbar
        bottomToolbar.setVisibility(View.VISIBLE);

        tvBottomToolbarRight.setOnClickListener(new RegisterNameButtonOnClickListener(tbName));
        tvBottomToolbarRight.setText(getString(R.string.registerNameFragment_bottomToolbar));

        // Starts the views
        registerNameUiController.enable();

        // Name
        tbName.setOnContentChangedListener(
                new NameOnContentChangedListener(new ClearNameObserver(registerButtonUiController), errorStateController.getInputObserver()));

        tbName.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                tvBottomToolbarRight.performClick();
                return true;
            }
            return false;
        });

        firstName = form.getFirstName();
        lastName = form.getLastName();

        if (firstName != null && !firstName.isEmpty()) {
            tbName.setText((lastName != null && !lastName.isEmpty()) ? String.format("%s %s", firstName, lastName) : firstName);
            registerButtonUiController.enable();

        } else {
            registerButtonUiController.disable();
        }

        return rootView;
    } // end onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RegisterActivity) getActivity()).showKeyboard();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ((LinearMultiStepActivity) getActivity()).onNavigateBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        tbName.setOnContentChangedListener(null);
        tbName.setOnEditorActionListener(null);
    }

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    /**
     * It is used when the inputs are checked for controlling the the "CONTINUE" button status.
     */
    private class RegisterNameUiController implements ViewUiController {
        private final TextBox _tbName;

        RegisterNameUiController(TextBox tbUsername) {
            _tbName = tbUsername;

        }

        /**
         * For this UiController, reset means clearing the error displayed. We don't change the
         * text in the input field.
         */
        @Override
        public void enable() {
            _tbName.setEnabled(true);

        }

        @Override
        public void disable() {
            _tbName.setEnabled(false);
            KeyboardUtils.hideKeyboard(_tbName);
        }

        /**
         * For this UiController, setLoading means locking the TextBox while performing the
         * operation
         */
        @Override
        public void setLoading() {
            _tbName.setEnabled(false);
            KeyboardUtils.hideKeyboard(_tbName);
        }

        /**
         * This processes an error on this field
         */
        @Override
        public void setStatus(SpareroomStatus spareroomStatus) {
            _tbName.setEnabled(false);

            if (spareroomStatus != null) {
                _tbName.setMessageText(spareroomStatus.getMessage());
            } else {
                _tbName.setMessageText(getString(R.string.loginEmailFragment_errorGeneric));
            }

            _tbName.showMessage();

        }

        @Override
        public void clearStatus() {
        }

    } //end class RegisterNameUiController

    private class RegisterNameButtonOnClickListener implements View.OnClickListener {
        private final TextBox _tbName;
        private final FieldValidator _nameFieldValidator;

        private RegisterNameButtonOnClickListener(TextBox tbName) {
            _tbName = tbName;
            _nameFieldValidator = new NameFieldValidator();

        }

        @Override
        public void onClick(View v) {

            int nameValidationState = _nameFieldValidator.validate(_tbName.getText());

            if (nameValidationState != NameFieldValidator.VALID) {
                SpareroomStatus spareroomStatus = new SpareroomStatus();
                spareroomStatus.setCode(Integer.toString(nameValidationState));
                switch (nameValidationState) {
                    case NameFieldValidator.EMPTY:
                        spareroomStatus.setMessage(getString(R.string.registerNameFragment_errorMessage_empty));
                        break;
                    case NameFieldValidator.TOO_FEW_WORDS:
                        spareroomStatus.setMessage(getString(R.string.registerNameFragment_errorMessage_tooFewWords));
                        break;
                    case NameFieldValidator.FIRST_NAME_IS_TOO_SHORT:
                        spareroomStatus.setMessage(getString(R.string.registerNameFragment_errorMessage_firstNameIsTooShort));
                        break;
                }
                _registerProcessCompositeUiController.setSpareroomStatus(spareroomStatus);
            } else { // VALID
                ((RegisterActivity) getActivity()).nextFragment();
            }

        }

    } //end class RegisterNameButtonOnClickListener

    private class NameOnContentChangedListener implements TextBox.OnContentChangedListener, IObservable {
        private final LinkedList<IObserver> _observerList = new LinkedList<>();

        NameOnContentChangedListener(
                IObserver emptyFieldObserver,
                IObserver errorStateObserver) {

            // I decided to use the constructor header for the observers rather than the public
            // method addObserver because the order in which the actual observers we are using
            // execute matters, and using addObserver does not explicitly set the order in which
            // observers should be notified
            addObserver(errorStateObserver);
            addObserver(emptyFieldObserver);
        }

        @Override
        public void onContentChangedEvent(CharSequence s) {
            String fullName = s.toString();
            AccountRegisterForm form = ((AccountRegisterForm) ((LinearMultiStepActivity) getActivity()).getSharedObject(KEY_REGISTER_FORM));
            String firstName = "";
            String lastName = "";

            notifyObservers(fullName);

            if (!fullName.isEmpty()) {
                int posLastSpace = fullName.lastIndexOf(" ");

                if (posLastSpace != -1) // -1 is the value when the search is not int the string
                    firstName = fullName.substring(0, posLastSpace).trim();
                else
                    firstName = fullName.trim();

                if (posLastSpace != -1) // -1 is the value when the search is not int the string
                    lastName = fullName.substring(posLastSpace + 1).trim();
                else
                    lastName = "";

            }

            form.setFirstName(firstName);
            form.setLastName(lastName);

            ((LinearMultiStepActivity) getActivity()).setSharedObject(KEY_REGISTER_FORM, form);

        }

        @Override
        public void addObserver(IObserver o) {
            _observerList.add(o);
        }

        @Override
        public void removeObserver(IObserver o) {
            _observerList.remove(o);
        }

        private void notifyObservers(Object o) {
            for (IObserver observer : _observerList) {
                observer.notifyObserver(o);
            }
        }
    } //end class EmailOnContentChangedListener

    /**
     * UiController that defines how to enable or disable the SignInButton
     */
    private class ClearNameObserver implements IObserver {
        private final ViewUiController _signInButtonController;
        private boolean _isEnabled = false;
        private boolean _isNameEmpty = true;

        ClearNameObserver(ViewUiController signInButtonController) {
            _signInButtonController = signInButtonController;
        }

        @Override
        public void notifyObserver(Object o) {
            String username = (String) o;
            _isNameEmpty = (username == null) || (username.length() == 0);

            if (_isNameEmpty) {
                if (_isEnabled) {
                    _signInButtonController.disable();
                    _isEnabled = false;
                }
            } else {
                if (!_isEnabled) {
                    _signInButtonController.enable();
                    _isEnabled = true;
                }
            }
        }

    } //end class EnableSignInButtonUiController

    //endregion INNER CLASSES

}
