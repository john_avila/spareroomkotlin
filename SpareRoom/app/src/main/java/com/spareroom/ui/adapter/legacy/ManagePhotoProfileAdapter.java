package com.spareroom.ui.adapter.legacy;

import android.app.Dialog;
import android.os.AsyncTask;
import android.view.*;
import android.view.View.*;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.legacy.AsyncTaskManager;
import com.spareroom.integration.dependency.component.ComponentRepository;
import com.spareroom.integration.imageloader.ImageLoader;
import com.spareroom.integration.imageloader.ImageLoadingListener;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.model.business.*;
import com.spareroom.ui.anim.PushInAnim;
import com.spareroom.ui.screen.Activity;
import com.spareroom.ui.screen.AlertDialogBuilder;
import com.spareroom.ui.screen.legacy.ManagePhotoProfileActivity;
import com.spareroom.ui.util.UiUtils;

import java.util.LinkedList;
import java.util.TreeMap;

import javax.inject.Inject;

import androidx.appcompat.view.ActionMode;

public class ManagePhotoProfileAdapter extends BaseAdapter {
    private final PictureGallery _gallery;
    private final LinkedList<Picture> _lGallery;
    private final Activity _activity;

    @Inject
    ImageLoader _imageLoader;

    private boolean _isEditModeEnabled = false;

    private ActionMode _actionMode;

    private final TreeMap<Integer, CheckBox> _tChecked2;
    private int _checkboxWidth = 0;

    private final String _flatshareId;
    private final String _flatshareType;

    private final AsyncTaskManager _taskManager;

    public ManagePhotoProfileAdapter(Activity activity, PictureGallery gallery, String flatshareId, String flatshareType) {
        ComponentRepository.get().getAppComponent().inject(this);
        _gallery = gallery;
        _lGallery = _gallery.get_gallery();
        _activity = activity;
        _flatshareId = flatshareId;
        _flatshareType = flatshareType;

        _tChecked2 = new TreeMap<>();

        _taskManager = new AsyncTaskManager();
    }

    @Override
    public int getCount() {
        return _lGallery.size();
    }

    @Override
    public Object getItem(int position) {
        return (position < _lGallery.size()) ? _lGallery.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_manage_photos_picture, parent, false);
        }

        String tag = (String) view.getTag();

        TextView ivDefaultTag = view.findViewById(R.id.managePicture_ivDefault);
        ivDefaultTag.setBackground(UiUtils.getTintedDrawable(ivDefaultTag.getContext(), R.drawable.ribbon, R.color.cyan));

        ImageView ivPicturePreview = view.findViewById(R.id.managePicture_ivPicture);
        final ImageView ivPicturePlaceholder = view.findViewById(R.id.managePicture_ivPlaceholder);
        TextView tvCaption = view.findViewById(R.id.activity_manage_photos_picture_tvCaption);
        CheckBox cb = view.findViewById(R.id.managePicture_cb);

        if (position == 0) {
            ivDefaultTag.setVisibility(View.VISIBLE);
        } else {
            ivDefaultTag.setVisibility(View.GONE);
        }

        if ((tag == null) || !tag.equals(_lGallery.get(position).getId())) {
            // reload
            Picture p = _lGallery.get(position);

            view.setTag(p.getId());

            ivPicturePreview.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
                    MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            _imageLoader.loadImage(p.getLargeUrl() + "?square=" + (ivPicturePreview.getMeasuredWidth()), ivPicturePreview, new ImageLoadingListener() {
                @Override
                public void onLoadingComplete() {
                    ivPicturePlaceholder.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingFailed(Exception exception) {
                    ivPicturePlaceholder.setVisibility(View.VISIBLE);
                }
            });

            PhotoOnLongClickListener longClickListener = new PhotoOnLongClickListener(_lGallery.get(position).getId());

            view.setOnLongClickListener(longClickListener);
            tvCaption.setText(p.getCaption());
            tvCaption.setOnClickListener(new CaptionOnClickListener(p.getId(), p.getCaption()));
            tvCaption.setOnLongClickListener(longClickListener);
            ivPicturePreview.setOnLongClickListener(longClickListener);

            cb.setOnClickListener(new CheckBoxOnClickListener(position));
            if (_checkboxWidth == 0) {
                Display display = _activity.getWindowManager().getDefaultDisplay();
                cb.measure(display.getWidth(), display.getHeight());

                _checkboxWidth = cb.getMeasuredWidth();
            }

            if (!_isEditModeEnabled) {
                cb.getLayoutParams().width = 0;
                cb.requestLayout();
            }

            if (_tChecked2.containsKey(position))
                cb.setChecked(true);
            else
                cb.setChecked(false);
        }

        return view;
    }

    public void stop() {
        _taskManager.cancelAll();
    }

    public void editMode() {
        _actionMode = _activity.startSupportActionMode(new ModeCallback());
        _isEditModeEnabled = true;

        ListView lv = _activity.findViewById(R.id.activity_manage_photos_lv);

        if (lv != null) {
            for (int i = 0; i < lv.getCount(); i++) {
                View row = lv.getChildAt(i);
                if (row != null) {
                    CheckBox cb = row.findViewById(R.id.managePicture_cb);

                    PushInAnim anim = new PushInAnim(cb, _checkboxWidth, false);
                    anim.setDuration(500);
                    cb.startAnimation(anim);
                }
            }
            lv.requestLayout();
        }
    }

    private final class ModeCallback implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Create the menu from the xml file
            MenuInflater inflater = _activity.getMenuInflater();
            inflater.inflate(R.menu.action_mode_delete_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            // Here, you can check selected items to adapt available actions
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {

            deselectAll();

            if (mode == _actionMode) {
                _actionMode = null;
            }
            _isEditModeEnabled = false;

            ListView lv = _activity.findViewById(R.id.activity_manage_photos_lv);

            if (lv != null) {
                for (int i = 0; i < lv.getCount(); i++) {
                    View row = lv.getChildAt(i);
                    if (row != null) {
                        CheckBox cb = row.findViewById(R.id.managePicture_cb);

                        PushInAnim anim = new PushInAnim(cb, _checkboxWidth, true);
                        anim.setDuration(500);
                        cb.startAnimation(anim);
                    }
                }
            }
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

            switch (item.getItemId()) {
                case R.id.menu_item_delete:
                    for (int i = 0; i < _lGallery.size(); i++) {
                        if (_tChecked2.get(i) != null) {
                            Parameters p = new Parameters();
                            if (_flatshareId != null)
                                p.add("flatshare_id", _flatshareId);
                            if (_flatshareType != null)
                                p.add("flatshare_type", _flatshareType);
                            p.add("photo_id", _lGallery.get(i).getId());

                            Parameters[] vParameters = {p};

                            AsyncTask<Parameters, Integer, Object> at = ((ManagePhotoProfileActivity) _activity).instantiateDeletePictureAsyncTask();
                            at.execute(vParameters);
                            _taskManager.register(at);
                        }
                    }
                    break;
            }
            mode.finish();
            return true;
        }
    }

    private void deselectAll() {
        for (CheckBox cb : _tChecked2.values()) {
            cb.setChecked(false);
        }
    }

    private class CheckBoxOnClickListener implements OnClickListener {
        private final int _position;

        CheckBoxOnClickListener(int position) {
            _position = position;
        }

        @Override
        public void onClick(View v) {
            if (((CheckBox) v).isChecked()) {
                _tChecked2.put(_position, (CheckBox) v);
            } else {
                _tChecked2.remove(_position);
            }
            if (!_tChecked2.isEmpty()) {
                if (_actionMode == null)
                    _actionMode = _activity.startSupportActionMode(new ModeCallback());
            } else {
                if (_actionMode != null)
                    _actionMode.finish();
            }
        }
    }

    private class PhotoOnLongClickListener implements OnLongClickListener {

        private final String _photoId;

        PhotoOnLongClickListener(String photoId) {
            _photoId = photoId;
        }

        @Override
        public boolean onLongClick(View v) {
            View view = LayoutInflater.from(v.getContext()).inflate(R.layout.dialog_message_buttons, null, false);

            TextView tvMessage = view.findViewById(R.id.dialog_message);
            Button bPositive = view.findViewById(R.id.dialog_bPositive);
            Button bNegative = view.findViewById(R.id.dialog_bNegative);

            Dialog d = new AlertDialogBuilder(v.getContext(), view).create();

            tvMessage.setText(_activity.getString(R.string.activity_manage_photos_default_picture));
            bPositive.setOnClickListener(new DefaultOnClickListener(d));
            bNegative.setOnClickListener(new DialogUtil.CancelOnClickListener(d));
            d.show();
            return false;
        }

        private class DefaultOnClickListener implements OnClickListener {
            private final Dialog _dialog;

            DefaultOnClickListener(Dialog d) {
                _dialog = d;
            }

            @Override
            public void onClick(View v) {

                Parameters p = new Parameters();
                if (_flatshareId != null)
                    p.add("advert_id", _flatshareId);
                p.add("sort_order_" + _photoId, "1");

                for (int i = 0; i < _gallery.get_gallery().size(); i++) {
                    if (!_gallery.get(i).getId().equals(_photoId))
                        p.add("sort_order_" + _gallery.get(i).getId(), Integer.toString(i + 2));
                }
                Parameters[] vParameters = {p};

                AsyncTask<Parameters, Integer, Object> at = ((ManagePhotoProfileActivity) _activity).instantiateChangeOrderAsyncTask();
                at.execute(vParameters);
                _taskManager.register(at);

                _dialog.dismiss();
            }
        }
    }

    private class CaptionOnClickListener implements OnClickListener {
        private final String _photoId;
        private final String _defaultText;

        CaptionOnClickListener(String photoId, String defaultText) {
            _photoId = photoId;
            _defaultText = defaultText;
        }

        @Override
        public void onClick(View v) {
            View vContent = LayoutInflater.from(v.getContext()).inflate(R.layout.dialog_edit_caption, null, false);

            EditText etCaption = vContent.findViewById(R.id.dialog_edit_caption_etCaption);

            etCaption.setText(_defaultText);

            Button bPositive = vContent.findViewById(R.id.dialog_bPositive);
            Button bNegative = vContent.findViewById(R.id.dialog_bNegative);

            Dialog d = new AlertDialogBuilder(vContent.getContext(), vContent).create();

            bPositive.setOnClickListener(new UpdateOnClickListener(d));
            bNegative.setOnClickListener(new DialogUtil.CancelOnClickListener(d));

            d.show();
        }

        private class UpdateOnClickListener implements OnClickListener {
            private final Dialog _dialog;

            UpdateOnClickListener(Dialog d) {
                _dialog = d;
            }

            @Override
            public void onClick(View v) {
                Parameters p = new Parameters();
                if (_flatshareId != null)
                    p.add("advert_id", _flatshareId);
                p.add("caption_" + _photoId, ((EditText) _dialog.findViewById(R.id.dialog_edit_caption_etCaption)).getText().toString());
                Parameters[] vParameters = {p};

                AsyncTask<Parameters, Integer, Object> at = ((ManagePhotoProfileActivity) _activity).instantiateEditCaptionAsyncTask();
                at.execute(vParameters);
                _taskManager.register(at);
                _dialog.dismiss();
            }
        }
    }
}
