package com.spareroom.ui.screen;

import android.app.Dialog;
import android.os.Bundle;

import com.spareroom.R;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.viewmodel.ConversationViewModel;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

public class DeleteConversationDialog extends DialogFragment implements Injectable {

    public static final String TAG = DeleteConversationDialog.class.getName() + "Tag";

    private ConversationViewModel viewModel;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    public static DeleteConversationDialog getInstance() {
        return new DeleteConversationDialog();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(ConversationViewModel.class);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialogBuilder builder = new AlertDialogBuilder(getActivity());
        builder.setTitle(R.string.confirm);
        builder.setMessage(R.string.dialog_tvDeleteMessage);
        builder.setPositiveButton(R.string.delete, (dialog, which) -> {
            viewModel.getDeleteConversationConfirmation().delete();
            dismiss();
        });

        builder.setNegativeButton(R.string.cancel, (dialog, which) -> dismiss());

        return builder.create();
    }
}
