package com.spareroom.ui.screen.legacy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.*;
import com.spareroom.R;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.model.business.AbstractAd;
import com.spareroom.ui.util.MapsUtil;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

public class AdOfferedMapFragment extends SupportMapFragment implements OnMarkerClickListener {
    private String _newness;
    private float _latitude;
    private float _longitude;
    private String _neighbourhood;
    private String _postcode;
    private String _accomType;
    private String _price;
    private String _title;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AnalyticsTrackerComposite.getInstance().trackScreen_Fragment(this);

    }

    private class MapLoadedCallback implements OnMapLoadedCallback {
        private final GoogleMap _googleMap;

        MapLoadedCallback(GoogleMap googleMap) {
            _googleMap = googleMap;
        }

        @Override
        public void onMapLoaded() {

            if (_googleMap != null) {
                _googleMap.snapshot(arg0 -> {
                    FragmentActivity context = getActivity();
                    if (context != null) {
                        ImageView iv = context.findViewById(R.id.activity_ad_ivMap);
                        FrameLayout fl = context.findViewById(R.id.activity_ad_fMap);

                        iv.setVisibility(View.VISIBLE);
                        iv.setImageBitmap(arg0);
                        fl.setVisibility(View.GONE);
                    }
                });
            }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle b = getArguments();
        _newness = b.getString("newness", "");
        _latitude = b.getFloat("latitude", 0f);
        _longitude = b.getFloat("longitude", 0f);
        _neighbourhood = b.getString("neighbourhood");
        _postcode = b.getString("postcode");
        _accomType = b.getString("accomType");
        _price = b.getString("price");
        _title = b.getString("title");

        getMapAsync(new MapReadyCallback());

    }

    @Override
    public boolean onMarkerClick(Marker arg0) {
        arg0.showInfoWindow();

        return true;
    }

    private class MapReadyCallback implements OnMapReadyCallback {

        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapsUtil.disableIndoor(googleMap);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(_latitude, _longitude), 14));
            googleMap.getUiSettings().setRotateGesturesEnabled(false);
            googleMap.getUiSettings().setScrollGesturesEnabled(false);
            googleMap.getUiSettings().setTiltGesturesEnabled(false);
            googleMap.getUiSettings().setZoomGesturesEnabled(false);
            googleMap.getUiSettings().setZoomControlsEnabled(false);

            if (AbstractAd.NEWNESS_STATUS_NEW.equalsIgnoreCase(_newness)) {
                googleMap.addMarker(
                        new MarkerOptions()
                                .position(new LatLng(_latitude, _longitude))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin_new)));

            } else {
                googleMap.addMarker(
                        new MarkerOptions()
                                .position(new LatLng(_latitude, _longitude))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin_normal)));

            }

            ImageView iv = getActivity().findViewById(R.id.activity_ad_ivMap);

            iv.setOnClickListener(v -> {
                Intent mapAd = new Intent(getActivity(), AdOfferedMapActivity.class);

                mapAd.putExtra("newness", _newness);
                mapAd.putExtra("latitude", _latitude);
                mapAd.putExtra("longitude", _longitude);
                mapAd.putExtra("neighbourhood", _neighbourhood);
                mapAd.putExtra("postcode", _postcode);
                mapAd.putExtra("accomType", _accomType);
                mapAd.putExtra("price", _price);
                mapAd.putExtra("title", _title);

                startActivity(mapAd);

            });

            googleMap.setOnMapClickListener(arg0 -> {
                Intent mapAd = new Intent(getActivity(), AdOfferedMapActivity.class);

                mapAd.putExtra("newness", _newness);
                mapAd.putExtra("latitude", _latitude);
                mapAd.putExtra("longitude", _longitude);
                mapAd.putExtra("neighbourhood", _neighbourhood);
                mapAd.putExtra("postcode", _postcode);
                mapAd.putExtra("accomType", _accomType);
                mapAd.putExtra("price", _price);
                mapAd.putExtra("title", _title);

                startActivity(mapAd);

            });

            OnMapLoadedCallback onMapLoadedCallback = new MapLoadedCallback(googleMap);
            googleMap.setOnMapLoadedCallback(onMapLoadedCallback);

        }

    }

}
