package com.spareroom.ui.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.*;
import android.util.AttributeSet;
import android.view.View;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.ui.util.StringUtils;
import com.spareroom.ui.util.UiUtils;
import com.spareroom.ui.widget.params.MinMaxEditTextParams;

import androidx.annotation.*;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

public class MinMaxView extends ConstraintLayout {

    private ImageView icon;
    private TextView textViewTitle;

    private ControllerCoordinator controllerCoordinator;
    private TextView minTitleTextView;
    private EditText minEditText;
    private MinEditTextWatcher minEditTextWatcher;

    private TextView maxTitleTextView;
    private EditText maxEditText;
    private MaxEditTextWatcher maxEditTextWatcher;

    public interface OnValuesChangeListener {
        void onMinChangedEvent(Integer value);

        void onMaxChangedEvent(Integer value);
    }

    public MinMaxView(@NonNull Context context) {
        super(context);
        init();
    }

    public MinMaxView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MinMaxView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        minEditText.addTextChangedListener(minEditTextWatcher);
        maxEditText.addTextChangedListener(maxEditTextWatcher);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        minEditText.removeTextChangedListener(minEditTextWatcher);
        maxEditText.removeTextChangedListener(maxEditTextWatcher);
    }

    private void init() {
        inflate(getContext(), R.layout.material_widget_min_max, this);
        View highlightBar = findViewById(R.id.widgetRangeBar_highlightBar);
        icon = findViewById(R.id.widgetRangeBar_icon);
        textViewTitle = findViewById(R.id.widgetRangeBar_text_title);
        minTitleTextView = findViewById(R.id.widgetRangeBar_textBoxMin_title);
        minEditText = findViewById(R.id.widgetRangeBar_textBoxMin_content);
        maxTitleTextView = findViewById(R.id.widgetRangeBar_textBoxMax_title);
        maxEditText = findViewById(R.id.widgetRangeBar_textBoxMax_content);

        setPadding(0, 0, 0, getResources().getDimensionPixelSize(R.dimen.widgetRangeBar_marginBottom));

        controllerCoordinator = new ControllerCoordinator(minEditText, maxEditText, new HighlightStatus(highlightBar));
        minEditTextWatcher = new MinEditTextWatcher(controllerCoordinator);
        maxEditTextWatcher = new MaxEditTextWatcher(controllerCoordinator);
    }

    public void params(MinMaxEditTextParams minMaxEditTextParams) {
        textViewTitle.setText(minMaxEditTextParams.title());
        if (minMaxEditTextParams.iconId() != 0)
            icon.setImageDrawable(ContextCompat.getDrawable(getContext(), minMaxEditTextParams.iconId()));

        initEditBoxes(minMaxEditTextParams);

    }

    private void initEditBoxes(MinMaxEditTextParams minMaxEditTextParams) {
        Drawable drawable = UiUtils.stringToDrawable(getContext(), R.color.black, R.dimen.widgetRangeBar_titleTextSize, minMaxEditTextParams.textBase());
        String textBase = minMaxEditTextParams.textBase() == null ? "" : minMaxEditTextParams.textBase();

        minEditText.setHint(textBase + minMaxEditTextParams.minHint());
        minTitleTextView.setText(minMaxEditTextParams.minTitle());
        controllerCoordinator.setMinDrawable(drawable);
        if (minMaxEditTextParams.minMaxCharacters() != 0)
            minEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(minMaxEditTextParams.minMaxCharacters())});
        min(minMaxEditTextParams.min());

        maxEditText.setHint(textBase + minMaxEditTextParams.maxHint());
        maxTitleTextView.setText(minMaxEditTextParams.maxTitle());
        controllerCoordinator.setMaxDrawable(drawable);
        if (minMaxEditTextParams.maxMaxCharacters() != 0)
            maxEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(minMaxEditTextParams.maxMaxCharacters())});
        max(minMaxEditTextParams.max());
    }

    public void max(@Nullable Integer max) {
        controllerCoordinator.maxValue(max == null ? null : String.valueOf(max));
    }

    public String max() {
        return maxEditText.getText().toString();
    }

    public void min(@Nullable Integer min) {
        controllerCoordinator.minValue(min == null ? null : String.valueOf(min));
    }

    public String min() {
        return minEditText.getText().toString();
    }

    public void observer(OnValuesChangeListener observer) {
        controllerCoordinator.observer(observer);
    }

    private static class HighlightStatus {
        private final View highlightedBar;
        private boolean highlighted;

        private HighlightStatus(View highlightedBar) {
            this.highlightedBar = highlightedBar;
        }

        private void highlight(boolean highlight) {
            if (highlighted != highlight) {
                highlighted = highlight;
                highlightedBar.setVisibility(highlighted ? VISIBLE : GONE);
            }
        }

        private void changeHighlightedStatus(boolean isMinEmpty, boolean isMaxEmpty) {
            highlight(!(isMinEmpty && isMaxEmpty));
        }

    }

    private static class ControllerCoordinator {
        private final EditText minEditText;
        private Drawable minDrawable;
        private final EditText maxEditText;
        private Drawable maxDrawable;
        private final HighlightStatus highlightStatus;
        private OnValuesChangeListener observer;

        private boolean isMinEmpty = true;
        private boolean isMaxEmpty = true;

        private ControllerCoordinator(EditText minEditText, EditText maxEditText, HighlightStatus highlightStatus) {
            this.minEditText = minEditText;
            this.maxEditText = maxEditText;
            this.highlightStatus = highlightStatus;
        }

        private void minValue(String textValue) {
            isMinEmpty = TextUtils.isEmpty(textValue);
            setEditTextText(minEditText, textValue, minDrawable);

            if (observer != null)
                observer.onMinChangedEvent(!StringUtils.isNullOrEmpty(textValue) ? Integer.valueOf(textValue) : null);

            highlightStatus.changeHighlightedStatus(isMinEmpty, isMaxEmpty);
        }

        private void maxValue(String textValue) {
            isMaxEmpty = TextUtils.isEmpty(textValue);
            setEditTextText(maxEditText, textValue, maxDrawable);

            if (observer != null)
                observer.onMaxChangedEvent(!StringUtils.isNullOrEmpty(textValue) ? Integer.valueOf(textValue) : null);

            highlightStatus.changeHighlightedStatus(isMinEmpty, isMaxEmpty);

        }

        private void setEditTextText(EditText editText, String text, Drawable drawable) {
            if (!TextUtils.equals(editText.getText().toString(), text)) {
                editText.setText(text);
                editText.setSelection(editText.getText().length());
            }
            if (drawable != null) {
                if (!StringUtils.isNullOrEmpty(text)) {
                    editText.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                } else {
                    editText.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }
            }
        }

        private void observer(OnValuesChangeListener observer) {
            this.observer = observer;
        }

        private void setMinDrawable(Drawable minDrawable) {
            this.minDrawable = minDrawable;
        }

        private void setMaxDrawable(Drawable maxDrawable) {
            this.maxDrawable = maxDrawable;
        }

    }

    private static class MinEditTextWatcher implements TextWatcher {
        private final ControllerCoordinator controllerCoordinator;

        private MinEditTextWatcher(ControllerCoordinator controllerCoordinator) {
            this.controllerCoordinator = controllerCoordinator;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable e) {
            controllerCoordinator.minValue(e.toString());
        }

    }

    private static class MaxEditTextWatcher implements TextWatcher {
        private final ControllerCoordinator controllerCoordinator;

        private MaxEditTextWatcher(ControllerCoordinator controllerCoordinator) {
            this.controllerCoordinator = controllerCoordinator;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable e) {
            controllerCoordinator.maxValue(e.toString());
        }

    }

}
