package com.spareroom.ui.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.*;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;

import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.ui.screen.Activity;

import androidx.annotation.*;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;

/**
 * Tools for know screen measures.
 */

public class UiUtils {

    /**
     * Inflate orphan views
     */
    public static View inflateParentLess(Context context, @LayoutRes int layoutId) {
        return LayoutInflater.from(context).inflate(layoutId, null, false);
    }

    /**
     * Status bar height
     */
    public static int statusBarHeight(Context c) {
        int statusBarHeight = 0;
        int resourceId = c.getResources().getIdentifier("status_bar_height", "dimen", "android");

        if (resourceId > 0) {
            statusBarHeight = c.getResources().getDimensionPixelSize(resourceId);
        }

        return statusBarHeight;

    }

    /**
     * Screen height
     *
     * @param c - context of the View
     * @return - the absolute height of the display in pixels.
     */
    public static int screenHeight(Context c) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) c).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;

    }

    public static boolean isFragmentAlive(Fragment fragment) {
        final boolean isFragmentAlive = fragment.isAdded() && !fragment.isDetached();
        if (!isFragmentAlive)
            AnalyticsTrackerComposite.getInstance().trackUsingDeadFragmentEvent(fragment);

        return isFragmentAlive;
    }

    public static Drawable getTintedDrawable(Context context, @DrawableRes int drawable, @ColorRes int color) {
        Drawable tintedDrawable = DrawableCompat.wrap(ContextCompat.getDrawable(context, drawable).mutate());
        DrawableCompat.setTint(tintedDrawable, ContextCompat.getColor(context, color));
        DrawableCompat.setTintMode(tintedDrawable, Mode.SRC_IN);
        return tintedDrawable;
    }

    public static BitmapDrawable getBitmapDrawable(Resources resources, @DrawableRes int bitmapDrawableId) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        return new BitmapDrawable(resources, BitmapFactory.decodeResource(resources, bitmapDrawableId, options));
    }

    public static void setImmersiveMode(View view) {
        if (isKitKatOrNewer()) {
            view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    public static Drawable stringToDrawable(Context context, int colourId, int textSize, String text) {
        if (StringUtils.isNullOrEmpty(text))
            return null;

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Rect rect = new Rect();

        paint.setColor(ContextCompat.getColor(context, colourId));
        paint.setTextSize(context.getResources().getDimensionPixelOffset(textSize));
        paint.getTextBounds(text, 0, text.length(), rect);

        int height = rect.height();
        int width = rect.width();

        if (height <= 0 || width <= 0)
            return null;

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawText(text, -rect.left, height - rect.bottom, paint);

        return new BitmapDrawable(context.getResources(), bitmap);
    }

    public static boolean isOreoOrNewer() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }

    public static boolean isMarshmallowOrNewer() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static boolean isLollipopOrNewer() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static boolean isKitKatOrNewer() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }

    public static boolean isJellyBeanMr1OrNewer() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1;
    }

    public static boolean isJellyBeanMr2OrNewer() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;
    }

    public static void hideViews(View... views) {
        for (View view : views)
            view.setVisibility(View.GONE);
    }

    public static void showViews(View... views) {
        for (View view : views)
            view.setVisibility(View.VISIBLE);
    }
}
