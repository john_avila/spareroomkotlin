package com.spareroom.ui.adapter.conversation;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.integration.imageloader.ImageLoader;
import com.spareroom.model.business.*;
import com.spareroom.ui.screen.Fragment;
import com.spareroom.ui.screen.PictureGalleryActivity;
import com.spareroom.ui.util.*;

import androidx.recyclerview.widget.RecyclerView;

import static com.spareroom.ui.util.StringUtils.isNullOrEmpty;

class HeaderViewHolder extends RecyclerView.ViewHolder {

    private final ImageLoader imageLoader;
    private final DateUtils dateUtils;
    private final Fragment fragment;
    private final String searchingForRoom;
    private final String hasRoomToRent;
    private final String monthlyBudget;
    private final String monthlyRent;
    private final String online;
    private final String notAvailable;
    private final String loggedInUserId;
    private final String otherUserFirstNames;
    private final String otherUserLastName;
    private final String otherUserAvatarUrl;
    private final String otherUserId;

    private final ImageView imgAvatar;
    private final View imgAvatarForeground;
    private final TextView txtInitials;
    private final TextView txtName;
    private final TextView txtAdvertStatus;
    private final TextView txtOnlineDate;
    private final View imgBudget;
    private final TextView txtBudgetLabel;
    private final TextView txtBudget;
    private final View imgAvailability;
    private final TextView txtAvailabilityLabel;
    private final TextView txtAvailability;
    private final View imgLocation;
    private final TextView txtLocationLabel;
    private final TextView txtLocation;
    private final AdvertUtils advertUtils;

    HeaderViewHolder(View itemView, ImageLoader imageLoader, DateUtils dateUtils, Fragment fragment, Context context, String loggedInUserId,
                     String otherUserFirstNames, String otherUserLastName, String otherUserAvatarUrl, String otherUserId, AdvertUtils advertUtils) {
        super(itemView);
        this.imageLoader = imageLoader;
        this.dateUtils = dateUtils;
        this.fragment = fragment;
        this.loggedInUserId = loggedInUserId;
        this.otherUserFirstNames = otherUserFirstNames;
        this.otherUserLastName = otherUserLastName;
        this.otherUserAvatarUrl = otherUserAvatarUrl;
        this.otherUserId = otherUserId;
        this.advertUtils = advertUtils;

        hasRoomToRent = context.getString(R.string.has_room_to_rent);
        searchingForRoom = context.getString(R.string.is_searching_for_room);
        monthlyBudget = context.getString(R.string.monthly_budget);
        monthlyRent = context.getString(R.string.monthly_rent);
        notAvailable = context.getString(R.string.not_available);
        online = context.getString(R.string.online_date);

        // about user
        imgAvatar = itemView.findViewById(R.id.imgConversationDetailsListItemHeaderAvatar);
        imgAvatarForeground = itemView.findViewById(R.id.imgConversationDetailsListItemHeaderAvatarForeground);
        imgAvatarForeground.setOnClickListener(this::onAvatarClicked);

        txtInitials = itemView.findViewById(R.id.txtConversationDetailsListItemHeaderNameInitials);
        txtName = itemView.findViewById(R.id.txtConversationDetailsListItemHeaderName);
        txtAdvertStatus = itemView.findViewById(R.id.txtConversationDetailsListItemHeaderAdStatus);
        txtOnlineDate = itemView.findViewById(R.id.txtConversationDetailsListItemHeaderLastOnline);

        // budget
        imgBudget = itemView.findViewById(R.id.imgConversationDetailsListItemHeaderBudget);
        txtBudgetLabel = itemView.findViewById(R.id.txtConversationDetailsListItemHeaderBudgetLabel);
        txtBudget = itemView.findViewById(R.id.txtConversationDetailsListItemHeaderBudget);

        // availability
        imgAvailability = itemView.findViewById(R.id.imgConversationDetailsListItemHeaderAvailability);
        txtAvailabilityLabel = itemView.findViewById(R.id.txtConversationDetailsListItemHeaderAvailabilityLabel);
        txtAvailability = itemView.findViewById(R.id.txtConversationDetailsListItemHeaderAvailability);

        // location
        imgLocation = itemView.findViewById(R.id.imgConversationDetailsListItemHeaderLocation);
        txtLocationLabel = itemView.findViewById(R.id.txtConversationDetailsListItemHeaderLocationLabel);
        txtLocation = itemView.findViewById(R.id.txtConversationDetailsListItemHeaderLocation);
    }

    void bind(HeaderMessageItem header, Conversation conversation) {
        AbstractAd ad = header.getAd();
        boolean isMyOwnAdvert = isMyOwnAdvert(conversation);

        // avatar
        imageLoader.loadCircularImage(otherUserAvatarUrl, imgAvatar, fragment);

        // ad status
        boolean offered = !isMyOwnAdvert ? conversation.isAdvertOffered() : conversation.isAdvertWanted();
        txtAdvertStatus.setText(offered ? hasRoomToRent : searchingForRoom);

        // initials
        txtInitials.setText(getInitials());

        // name
        txtName.setText(isOtherUserAgent(conversation, ad) ? getAgentName() : getNonAgentName());

        // last online
        setLastOnline(conversation);

        // budget
        setBudget(ad);

        // availability
        setAvailability(ad);

        // set location
        setLocation(ad);
    }

    private void setLastOnline(Conversation conversation) {
        String lastOnline = dateUtils.getFormattedOnlineDate(conversation.getOtherUserLastOnline());
        txtOnlineDate.setText(String.format("%s %s", online, lastOnline));
        txtOnlineDate.setVisibility(notAvailable.equalsIgnoreCase(lastOnline) ? View.GONE : View.VISIBLE);
    }

    private boolean isOtherUserAgent(Conversation conversation, AbstractAd ad) {
        if (ad != null) {
            return (ad instanceof AdOffered) && AbstractAd.ADVERTISER_TYPE_AGENT.equalsIgnoreCase(((AdOffered) ad).getAdvertiserType());
        } else {
            return conversation.isOtherUserAgent(otherUserId);
        }
    }

    private StringBuilder getAgentName() {
        StringBuilder name = new StringBuilder();
        if (!isNullOrEmpty(otherUserFirstNames, true))
            name.append(otherUserFirstNames);

        if (!isNullOrEmpty(otherUserLastName)) {
            if (!isNullOrEmpty(name, true))
                name.append(" ");

            name.append(otherUserLastName);
        }
        return name;
    }

    private String getNonAgentName() {
        return StringUtils.getInitialsWithFullFirstName(StringUtils.getFirstString(otherUserFirstNames), otherUserLastName);
    }

    private void setLocation(AbstractAd ad) {
        if (ad != null && ad instanceof AdOffered) {
            imgLocation.setVisibility(View.VISIBLE);
            txtLocationLabel.setVisibility(View.VISIBLE);
            txtLocation.setVisibility(View.VISIBLE);

            txtLocation.setText(advertUtils.getFormattedLocation((AdOffered) ad, notAvailable));
        } else {
            imgLocation.setVisibility(View.GONE);
            txtLocationLabel.setVisibility(View.GONE);
            txtLocation.setVisibility(View.GONE);
        }
    }

    private void setAvailability(AbstractAd ad) {
        if (ad != null) {
            imgAvailability.setVisibility(View.VISIBLE);
            txtAvailabilityLabel.setVisibility(View.VISIBLE);
            txtAvailability.setVisibility(View.VISIBLE);

            txtAvailability.setText(dateUtils.getFormattedAvailabilityDate(ad.getAvailableFrom()));
        } else {
            imgAvailability.setVisibility(View.GONE);
            txtAvailabilityLabel.setVisibility(View.GONE);
            txtAvailability.setVisibility(View.GONE);
        }
    }

    private String getInitials() {
        return StringUtils.getInitials(otherUserFirstNames, otherUserLastName);
    }

    private void setBudget(AbstractAd ad) {
        if (ad != null) {
            imgBudget.setVisibility(View.VISIBLE);
            txtBudgetLabel.setVisibility(View.VISIBLE);
            txtBudget.setVisibility(View.VISIBLE);

            txtBudgetLabel.setText(ad instanceof AdWanted ? monthlyBudget : monthlyRent);
            txtBudget.setText(ad instanceof AdWanted ?
                    advertUtils.getFormattedMonthlyBudget((AdWanted) ad) : advertUtils.getFormattedMonthlyPrice((AdOffered) ad, true));
        } else {
            imgBudget.setVisibility(View.GONE);
            txtBudgetLabel.setVisibility(View.GONE);
            txtBudget.setVisibility(View.GONE);
        }
    }

    private boolean isMyOwnAdvert(Conversation conversation) {
        return loggedInUserId.equalsIgnoreCase(conversation.getAdvertPlacedBy());
    }

    private void onAvatarClicked(View view) {
        if (!StringUtils.isNullOrEmpty(otherUserAvatarUrl))
            PictureGalleryActivity.start(view.getContext(), otherUserAvatarUrl);
    }

}