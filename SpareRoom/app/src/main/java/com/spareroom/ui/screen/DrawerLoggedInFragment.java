package com.spareroom.ui.screen;

import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.widget.ImageView;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.imageloader.ImageLoader;
import com.spareroom.lib.core.IObserver;
import com.spareroom.model.business.Session;
import com.spareroom.model.business.User;
import com.spareroom.ui.screen.legacy.*;
import com.spareroom.ui.util.StringUtils;
import com.spareroom.ui.util.UiUtils;

import javax.inject.Inject;

import androidx.core.view.GravityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import static com.spareroom.ui.screen.NewExistingFragment.*;

public class DrawerLoggedInFragment extends DrawerFragment {

    private TextView _tvMessagesCounter;
    private TextView tvName;
    private TextView tvEmail;
    private ImageView ivAvatar;
    private MessagesCounterObserver _messagesCounterObserver;

    @Inject
    ImageLoader imageLoader;

    public static DrawerLoggedInFragment getInstance(String highlightedOption) {
        Bundle args = new Bundle();
        args.putString(HIGHLIGHT_OPTION, highlightedOption);
        DrawerLoggedInFragment drawer = new DrawerLoggedInFragment();
        drawer.setArguments(args);
        return drawer;
    }

    @Override
    void launchSavedSearches() {
        Intent intent = new Intent();
        intent.setClass(getActivity(), SavedSearchesActivity.class);
        getActivity().startActivity(intent);
    }

    @Override
    void launchMessages() {
        replaceFragment(ConversationListFragment.getInstance(), ConversationListFragment.TAG);
    }

    @Override
    void launchSavedAdverts() {
        replaceFragment(SavedAdvertsListHostFragment.getInstance(), SavedAdvertsListHostFragment.TAG);
    }

    @Override
    void launchMyAdverts() {
        replaceFragment(MyAdvertsListHostFragment.getInstance(), MyAdvertsListHostFragment.TAG);
    }

    private void replaceFragment(Fragment fragment, String tag) {
        if (!UiUtils.isFragmentAlive(this))
            return;

        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null && fragmentManager.findFragmentByTag(tag) == null) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_frame, fragment, tag);
            transaction.commit();

            if (getActivity() != null)
                ((MainActivity) getActivity()).notifyDrawerChanged(tag);
        }
    }

    @Override
    void launchEditMyDetails() {
        Intent intent = new Intent();
        intent.setClass(getActivity(), EditProfileActivity.class);
        getActivity().startActivity(intent);

    }

    @Override
    void launchUpgrade() {
        if (!UiUtils.isFragmentAlive(this))
            return;

        if (isOffline())
            return;

        SpareroomContext spareroomContext = SpareroomApplication.getInstance().getSpareroomContext();
        if (!spareroomContext.isUserLoggedIn()) {
            startActivity(new Intent(getActivity(), NewExistingActivity.class));
            return;
        }

        Intent intent = new Intent();
        intent.putExtra(
                UpgradeFragmentActivity.CONTEXT_ID_KEY,
                UpgradeFragmentActivity.CONTEXT_ID_VALUE);
        intent.setClass(getActivity(), UpgradeFragmentActivity.class);
        getActivity().startActivity(intent);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // Shows the beta and log out options
        setLogInViews(true);

        // Sets the header
        setHeader(R.layout.material_navigation_drawer_header);

        // Header views
        tvName = container.findViewById(R.id.navigationDrawerHeader_name);
        tvEmail = container.findViewById(R.id.navigationDrawerHeader_email);

        ivAvatar = container.findViewById(R.id.navigationDrawerHeader_avatarPhoto);

        // Menu views
        _tvMessagesCounter =
                container
                        .findViewById(R.id.navigationDrawer_messages)
                        .findViewById(R.id.drawer_item_counter);

        // Session
        Session session =
                SpareroomApplication
                        .getInstance(getActivity().getApplicationContext())
                        .getSpareroomContext()
                        .getSession();

        if (session == null || session.get_user() == null) {
            SpareroomApplication
                    .getInstance(getActivity().getApplicationContext())
                    .getSpareroomContext()
                    .deleteUserSession();

        }

        return null;
    }

    @Override
    public void onResume() {
        super.onResume();

        updateAvatar();
        updateName();
        updateEmail();
        updateMessagesCount();

        // Reload the messages counter when needed
        _messagesCounterObserver = new MessagesCounterObserver();
        SpareroomApplication
                .getInstance(getActivity().getApplicationContext())
                .getSpareroomContext()
                .addSaveSessionObserver(_messagesCounterObserver);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            String target = data.getStringExtra(EXTRA_ACTION_CODE);

            if (target != null) // When it comes from the default option
                switch (target) {
                    case EXTRA_ACTION_CODE_SAVED_SEARCHES:
                        launchSavedSearches();
                        break;
                    case EXTRA_ACTION_CODE_VIEW_MESSAGES:
                        launchMessages();
                        highlight(HIGHLIGHT_MESSAGES);
                        break;
                    case EXTRA_ACTION_CODE_SAVED_ADVERTS:
                        launchSavedAdverts();
                        highlight(HIGHLIGHT_SAVED_ADVERTS);
                        break;
                    case EXTRA_ACTION_CODE_MY_ADVERTS:
                        launchMyAdverts();
                        highlight(HIGHLIGHT_MY_ADVERTS);
                        break;
                    case EXTRA_ACTION_CODE_PROFILE:
                        launchEditMyDetails();
                        break;
                    case EXTRA_ACTION_CODE_UPGRADE:
                        launchUpgrade();
                        break;
                    default:
                        break;
                }

        }

    }

    @Override
    public void onPause() {
        super.onPause();

        // Remove the observer for reload the messages counter when needed
        SpareroomApplication
                .getInstance(getActivity().getApplicationContext())
                .getSpareroomContext()
                .removeSaveSessionObserver(_messagesCounterObserver);

    }

    private void updateAvatar() {
        Session session = SpareroomApplication.getSpareRoomContext().getSession();
        String avatarUrl = session != null ? session.get_profilePictureUrl() : null;
        if (avatarUrl != null && avatarUrl != ivAvatar.getTag(R.id.image_url_tag)) {
            ivAvatar.setTag(R.id.image_url_tag, avatarUrl);
            imageLoader.loadCircularImage(avatarUrl, ivAvatar, this);
        }
    }

    private void updateName() {
        Session session = SpareroomApplication.getSpareRoomContext().getSession();
        User user = session != null ? session.get_user() : null;
        if (user != null) {
            tvName.setText(String.format("%s %s",
                    StringUtils.capitalizeEachFirstLetter(user.get_firstName()),
                    StringUtils.capitalizeEachFirstLetter(user.get_lastName())));
        }
    }

    private void updateEmail() {
        Session session = SpareroomApplication.getSpareRoomContext().getSession();
        tvEmail.setText(session != null ? session.get_email() : "");
    }

    private void updateMessagesCount() {
        Session session = SpareroomApplication.getSpareRoomContext().getSession();
        int messagesCount = session != null ? session.getMessageCount() : 0;
        _tvMessagesCounter.setText(String.format("%s %s", messagesCount, getString(R.string.navigationDrawer_messagesUnread)));
        _tvMessagesCounter.setVisibility(messagesCount > 0 ? View.VISIBLE : View.GONE);
    }

    private class MessagesCounterObserver implements IObserver {

        @Override
        public void notifyObserver(Object o) {
            boolean open = _drawerLayout.isDrawerOpen(GravityCompat.START);

            if (open && getActivity() != null)
                getActivity().runOnUiThread(DrawerLoggedInFragment.this::updateMessagesCount);
        }
    }

}
