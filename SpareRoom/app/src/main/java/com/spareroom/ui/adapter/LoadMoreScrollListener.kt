package com.spareroom.ui.adapter

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

private const val SCROLL_DOWN = 1

abstract class LoadMoreScrollListener(private val layoutManager: LinearLayoutManager) : RecyclerView.OnScrollListener() {

    abstract fun loadMore()

    abstract fun isLoading(): Boolean

    abstract fun hasMoreItems(): Boolean

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        if (reachedBottom(recyclerView)) {
            recyclerView.stopScroll()
            loadMoreIfNeeded()
        }
    }

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        if (reachedBottom(recyclerView) && newState == RecyclerView.SCROLL_STATE_DRAGGING)
            loadMoreIfNeeded()
    }

    private fun loadMoreIfNeeded() {
        if (hasItems() && hasMoreItems() && !isLoading())
            loadMore()
    }

    private fun hasItems() = layoutManager.itemCount > 0

    private fun isShowingLastItem() = layoutManager.findLastCompletelyVisibleItemPosition() == layoutManager.itemCount - 1

    private fun reachedBottom(recyclerView: RecyclerView) = isShowingLastItem() && !canScrollDown(recyclerView)

    private fun canScrollDown(recyclerView: RecyclerView) = recyclerView.canScrollVertically(SCROLL_DOWN)
}