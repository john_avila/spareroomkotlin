package com.spareroom.ui.screen;

import android.content.Intent;
import android.graphics.drawable.*;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.animation.*;
import android.widget.*;

import com.google.android.material.tabs.TabLayout;
import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.integration.sharedpreferences.OnboardingDao;
import com.spareroom.ui.adapter.OnboardingPageAdapter;
import com.spareroom.ui.screen.legacy.MainActivity;
import com.spareroom.ui.scroller.VariableSpeedScroller;
import com.spareroom.ui.util.UiUtils;
import com.spareroom.ui.viewpager.OnboardingPageTransformer;

import java.lang.reflect.Field;

import androidx.annotation.DrawableRes;
import androidx.viewpager.widget.ViewPager;

import static com.spareroom.ui.screen.NewExistingFragment.EXTRA_ACTION_CODE;
import static com.spareroom.ui.screen.NewExistingFragment.EXTRA_ACTION_CODE_ONBOARDING;

public class OnboardingActivity extends Activity {

    //region FIELDS CONTROLLER

    public static final int ONBOARDING_EXTRA_ACTION_CODE = 65165;

    //endregion FIELDS CONTROLLER

    //region FIELDS UI.CONTROL

    private static final int SCROLL_DURATION = 1150;
    private static final long BACKGROUND_TRANSLATION_DURATION = 25000L;
    private static final int NUMBER_OF_PAGES = 6;

    //endregion FIELDS UI.CONTROL

    //region METHODS LIFECYCLE

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.material_onboarding_activity);

        // Pages and background
        ViewPager viewPager = findViewById(R.id.onboarding_pager);
        HorizontalScrollView backgroundHolder = findViewById(R.id.onBoarding_backgroundUkHolder);
        ImageView ivBackground;
        TabLayout tabLayout = findViewById(R.id.onboarding_pagerCarousel);

        // Bottom bar
        View toolBar = findViewById(R.id.onboarding_bottomToolbar);
        ImageView ivNext = toolBar.findViewById(R.id.onboardingToolbar_chevron);
        TextView tvBegin = toolBar.findViewById(R.id.onboardingToolbar_begin);
        TextView tvSkip = toolBar.findViewById(R.id.onboardingToolbar_skip);

        // Pages and background
        viewPager.setAdapter(new OnboardingPageAdapter(this, getSupportFragmentManager(), NUMBER_OF_PAGES));

        ivNext.setOnClickListener(new NextOnClickListener(viewPager));
        tvBegin.setOnClickListener(new FinishClickListener());
        tvSkip.setOnClickListener(new FinishClickListener());

        // Set the background images and effect
        if (AppVersion.isUk() && (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1)) {
            ivBackground = backgroundHolder.findViewById(R.id.onBoarding_backgroundUk);
            LayerDrawable background = getBackground();
            ivBackground.setImageDrawable(background);
            Animation translateAnimation =
                    new TranslateAnimation(
                            TranslateAnimation.ABSOLUTE,
                            0f,
                            TranslateAnimation.ABSOLUTE,
                            -((BitmapDrawable) background.getDrawable(0)).getBitmap().getHeight(),
                            TranslateAnimation.ABSOLUTE,
                            0f,
                            TranslateAnimation.ABSOLUTE,
                            0f);

            translateAnimation.setDuration(BACKGROUND_TRANSLATION_DURATION);
            translateAnimation.setRepeatCount(Animation.INFINITE);
            translateAnimation.setRepeatMode(Animation.REVERSE);
            translateAnimation.setInterpolator(new LinearInterpolator());

            ivBackground.setAnimation(translateAnimation);

        } else {
            ivBackground = findViewById(R.id.onBoarding_backgroundUs);
            ivBackground.setImageDrawable(getBackground());
            ivBackground.setVisibility(View.VISIBLE);
            backgroundHolder.setVisibility(View.GONE);

        }
        viewPager.setPageTransformer(true, new OnboardingPageTransformer(this, (LayerDrawable) ivBackground.getDrawable(), NUMBER_OF_PAGES - 1));

        tabLayout.setupWithViewPager(viewPager, true);

        // Slow down the pager scroll effect
        try {
            Field mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            mScroller.set(viewPager, new VariableSpeedScroller(viewPager.getContext(), SCROLL_DURATION));
        } catch (Exception e) {
            // In case it fails there is not need of doing anything the slow down effect is lost, that's all
        }
    }

    private LayerDrawable getBackground() {
        return new LayerDrawable(new Drawable[]{
                getLayer(R.drawable.material_onboarding_background_00),
                getLayer(R.drawable.material_onboarding_background_01),
                getLayer(R.drawable.material_onboarding_background_02),
                getLayer(R.drawable.material_onboarding_background_03),
                getLayer(R.drawable.material_onboarding_background_04),
                getLayer(R.drawable.material_onboarding_background_05)});
    }

    private BitmapDrawable getLayer(@DrawableRes int layerResourceId) {
        BitmapDrawable bitmapDrawable = UiUtils.getBitmapDrawable(getResources(), layerResourceId);
        bitmapDrawable.setGravity(Gravity.FILL);

        return bitmapDrawable;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (AppVersion.isUs()) {
                OnboardingDao.create(OnboardingActivity.this, false);
                startActivity(new Intent(this, MainActivity.class));
            }
            finish();
        }

    }

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    private class FinishClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent;
            if (AppVersion.isUk() || SpareroomApplication.getInstance().getSpareroomContext().getSession() != null) {
                OnboardingDao.create(OnboardingActivity.this, false);
                startActivity(new Intent(OnboardingActivity.this, MainActivity.class));
                finish();

            } else {
                intent = new Intent(OnboardingActivity.this, NewExistingActivity.class);
                intent.putExtra(EXTRA_ACTION_CODE, EXTRA_ACTION_CODE_ONBOARDING);
                startActivityForResult(intent, ONBOARDING_EXTRA_ACTION_CODE);
            }

        }

    }

    private class NextOnClickListener implements View.OnClickListener {
        private final ViewPager _viewPager;

        NextOnClickListener(ViewPager viewPager) {
            _viewPager = viewPager;
        }

        @Override
        public void onClick(View v) {
            _viewPager.setCurrentItem(_viewPager.getCurrentItem() + 1);
        }

    }

    //endregion INNER CLASSES

}
