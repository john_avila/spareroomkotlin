package com.spareroom.ui.screen

import android.os.Bundle
import android.view.*
import androidx.lifecycle.*
import com.google.android.material.tabs.TabLayout
import com.spareroom.R
import com.spareroom.integration.dependency.Injectable
import com.spareroom.model.business.MyAdvertsListFirstPage
import com.spareroom.model.business.MyAdvertsListResponse
import com.spareroom.ui.adapter.MyAdvertsPagerAdapter
import com.spareroom.viewmodel.MainActivityViewModel
import com.spareroom.viewmodel.MyAdvertsListViewModel
import kotlinx.android.synthetic.main.adverts_host_fragment.*
import kotlinx.android.synthetic.main.scrolling_app_bar_view_tabs.*
import javax.inject.Inject

private const val WANTED_TAB = 1

class MyAdvertsListHostFragment : Fragment(), Injectable {

    companion object {
        const val TAG = "MyAdvertsListHostFragmentTag"

        @JvmStatic
        fun getInstance() = MyAdvertsListHostFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val myAdvertsViewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(MyAdvertsListViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.adverts_host_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setUpToolbar(view)
        setUpViewPager()
        myAdvertsViewModel.myAdverts.observe(this, Observer { advertsLoaded(it!!) })
    }

    private fun setUpToolbar(view: View) {
        setToolbar(view, getString(R.string.my_adverts))
        ViewModelProviders.of(activity!!).get(MainActivityViewModel::class.java).syncDrawer.sync()
    }

    private fun setUpViewPager() {
        advertsViewPager.adapter = MyAdvertsPagerAdapter(childFragmentManager, activity!!)
        advertsViewPager.pageMargin = resources.getDimensionPixelSize(R.dimen.page_margin)
        advertsViewPager.setPageMarginDrawable(R.drawable.vertical_page_divider)
        tabLayout.tabMode = TabLayout.MODE_FIXED
        tabLayout.setupWithViewPager(advertsViewPager)
    }

    private fun advertsLoaded(response: MyAdvertsListResponse) {
        if (myAdvertsViewModel.firstLoad && response is MyAdvertsListFirstPage && hasOnlyWantedAdverts(response))
            advertsViewPager.currentItem = WANTED_TAB

        myAdvertsViewModel.firstLoad = false
    }

    private fun hasOnlyWantedAdverts(firstPage: MyAdvertsListFirstPage): Boolean {
        return firstPage.wantedAdverts.isNotEmpty() && firstPage.offeredAdverts.isEmpty()
    }

}