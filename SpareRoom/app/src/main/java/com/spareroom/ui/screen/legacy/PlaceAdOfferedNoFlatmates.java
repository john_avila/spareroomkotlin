package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.model.business.DraftAdOffered.AdvertiserType;
import com.spareroom.ui.widget.PlaceAdButton;

public class PlaceAdOfferedNoFlatmates extends PlaceAdFragment {

    private TextView _tvGuys;
    private TextView _tvGirls;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setTitle(getString(R.string.place_ad_offered_no_flatmates));
        View rootView = inflater.inflate(R.layout.place_ad_offered_no_flatmates, container, false);
        _tvGuys = rootView.findViewById(R.id.place_ad_offered_no_flatmates_tvNoGuys);
        _tvGirls = rootView.findViewById(R.id.place_ad_offered_no_flatmates_tvNoGirls);
        PlaceAdButton _pabGuysPlus = rootView.findViewById(R.id.place_ad_offered_no_flatmates_guys_pabPlus);
        PlaceAdButton _pabGuysMinus = rootView.findViewById(R.id.place_ad_offered_no_flatmates_guys_pabMinus);
        PlaceAdButton _pabGirlsPlus = rootView.findViewById(R.id.place_ad_offered_no_flatmates_girls_pabPlus);
        PlaceAdButton _pabGirlsMinus = rootView.findViewById(R.id.place_ad_offered_no_flatmates_girls_pabMinus);

        _pabGuysPlus.setOnClickListener(new PlusGuysOnClickListener());
        _pabGuysMinus.setOnClickListener(new MinusGuysOnClickListener());
        _pabGirlsPlus.setOnClickListener(new PlusGirlsOnClickListener());
        _pabGirlsMinus.setOnClickListener(new MinusGirlsOnClickListener());

        Button _bConfirm = rootView.findViewById(R.id.place_ad_bConfirm);

        final DraftAdOffered draft = ((DraftAdOffered) getDraftAd());

        // In case there is an activity state change and we lost the draft
        if (draft == null) {
            getActivity().finish();

            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }

        _tvGuys.setText(String.format(AppVersion.flavor().getLocale(), "%d", draft.get_numExistingGuys()));
        _tvGirls.setText(String.format(AppVersion.flavor().getLocale(), "%d", draft.get_numExistingGirls()));

        _bConfirm.setOnClickListener(v -> {
            int numGuys = Integer.parseInt(_tvGuys.getText().toString());
            int numGirls = Integer.parseInt(_tvGirls.getText().toString());

            if (AdvertiserType.CURRENT_FLATMATE.equals(draft.get_advertiserType()) || AdvertiserType.LIVE_IN_LANDLORD.equals(draft.get_advertiserType())) {
                if ((numGuys + numGirls) == 0) {
                    Toast.makeText(getActivity(), getString(R.string.place_ad_offered_at_least_one), Toast.LENGTH_LONG).show();
                    return;
                }
            }

            draft.set_numExistingGuys(Integer.parseInt(_tvGuys.getText().toString()));
            draft.set_numExistingGirls(Integer.parseInt(_tvGirls.getText().toString()));
            finish();
        });
        if (AdvertiserType.CURRENT_FLATMATE.equals(draft.get_advertiserType()) || AdvertiserType.LIVE_IN_LANDLORD.equals(draft.get_advertiserType())) {
            TextView tvInclude = rootView.findViewById(R.id.place_ad_offered_no_flatmates_tvInclude);
            tvInclude.setVisibility(View.VISIBLE);
        }

        return rootView;
    }

    private class MinusGuysOnClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            int currentValue = Integer.parseInt(_tvGuys.getText().toString());
            if (currentValue > 0)
                _tvGuys.setText(String.format(AppVersion.flavor().getLocale(), "%d", currentValue - 1));
        }
    }

    private class PlusGuysOnClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            int currentValueGuys = Integer.parseInt(_tvGuys.getText().toString());
            int currentValueGirls = Integer.parseInt(_tvGirls.getText().toString());
            if ((currentValueGuys + currentValueGirls) > 11) {
                Toast.makeText(getActivity(), getString(R.string.placeAdOfferedNoFlatmates_maximumReached), Toast.LENGTH_LONG).show();
                return;
            }
            if (currentValueGuys < 99)
                _tvGuys.setText(String.format(AppVersion.flavor().getLocale(), "%d", currentValueGuys + 1));
        }
    }

    private class MinusGirlsOnClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            int currentValue = Integer.parseInt(_tvGirls.getText().toString());

            if (currentValue > 0)
                _tvGirls.setText(String.format(AppVersion.flavor().getLocale(), "%d", currentValue - 1));

        }
    }

    private class PlusGirlsOnClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            int currentValueGuys = Integer.parseInt(_tvGuys.getText().toString());
            int currentValueGirls = Integer.parseInt(_tvGirls.getText().toString());
            if ((currentValueGirls + currentValueGuys) > 11) {
                Toast.makeText(getActivity(), getString(R.string.placeAdOfferedNoFlatmates_maximumReached), Toast.LENGTH_LONG).show();
                return;
            }
            if (currentValueGirls < 99)
                _tvGirls.setText(String.format(AppVersion.flavor().getLocale(), "%d", currentValueGirls + 1));
        }
    }

}
