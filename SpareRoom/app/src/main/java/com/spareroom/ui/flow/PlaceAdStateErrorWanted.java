package com.spareroom.ui.flow;

import com.spareroom.ui.screen.legacy.*;

import java.util.HashMap;

public class PlaceAdStateErrorWanted extends FlowState {
    protected static final String PAGE_WANTED_20 = PlaceAdUploadingFragment.class.getName();
    protected static final String PAGE_WANTED_18 = PlaceAdTitleDescriptionFragment.class.getName();
    protected static final String PAGE_WANTED_5 = PlaceAdWantedAgeFragment.class.getName();
    protected static final String PAGE_WANTED_14 = PlaceAdWantedBudgetFragment.class.getName();
    protected static final String PAGE_WANTED_9 = PlaceAdWantedHappyToLiveWithFragment.class.getName(); // pending
    protected static final String PAGE_WANTED_13 = PlaceAdWantedAreasLookingInFragment.class.getName(); // pending
    protected static final String PAGE_WANTED_15 = PlaceAdWantedAvailabilityPeriodFragment.class.getName();

    protected static HashMap<String, String> _mError = new HashMap<>();

    public PlaceAdStateErrorWanted() {
        SCREENS = new String[]{
                PAGE_WANTED_20
        };
        init();
    }

    @Override
    protected void init() {
        super.init();
        _mError.put("ad_text", PAGE_WANTED_18);
        _mError.put("ad_title", PAGE_WANTED_18);
        _mError.put("age", PAGE_WANTED_5);
        _mError.put("combined_budget", PAGE_WANTED_14);
        _mError.put("first_name", PAGE_WANTED_18); // shouldn't it go to the first one?
        _mError.put("gender", PAGE_WANTED_9);
        _mError.put("inagreement", PAGE_WANTED_18); // shouldn't it go to the first one?
        _mError.put("last_name", PAGE_WANTED_18); //
        _mError.put("location_select", PAGE_WANTED_13);
        // _mError.put("location", PAGE_WANTED_13);
        _mError.put("login_email_and_password", PAGE_WANTED_18); // shouldn't it go to the first one?
        _mError.put("new_or_existing_user", PAGE_WANTED_18); // shouldn't it go to the first one?
        _mError.put("password", PAGE_WANTED_18); // shouldn't it go to the first one?
        _mError.put("per", PAGE_WANTED_15);
    }

    @Override
    public String getNextFragmentName(String currentScreen, Object businessObject) {
        return PAGE_WANTED_20;
    }

    public String nextErrorScreen(String cause) {
        String screenName = _mError.get(cause);
        if (screenName == null)
            return PAGE_WANTED_18;
        else
            return _mError.get(cause);
    }

    @Override
    public Boolean isLastFragment(String screen) {
        return screen.equals(PAGE_WANTED_20);
    }

}
