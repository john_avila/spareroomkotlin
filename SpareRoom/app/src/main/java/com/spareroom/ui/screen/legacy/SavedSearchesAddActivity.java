package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.controller.legacy.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.business.apiv2.util.SearchAdvertListPropertiesAdapter;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.model.business.*;
import com.spareroom.ui.screen.InjectableActivity;
import com.spareroom.ui.util.ConnectivityChecker;
import com.spareroom.ui.util.SnackBarUtils;

import java.util.LinkedList;

import javax.inject.Inject;

import androidx.fragment.app.FragmentTransaction;

public class SavedSearchesAddActivity extends InjectableActivity {
    public static final String SEARCH_TYPE_KEY = "search_type_key";

    private ListView _lvSaved;
    private AsyncTaskManager _asyncManager;

    private SearchType searchType;

    @Inject
    ConnectivityChecker connectivityChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_savedsearches_add);

        setToolbar();

        searchType = (SearchType) getIntent().getSerializableExtra(SEARCH_TYPE_KEY);
        if (savedInstanceState == null)
            loadNewSavedSearchFragment();

        _lvSaved = findViewById(R.id.savedSearchesList);

        _asyncManager = new AsyncTaskManager();
        SpareroomApplication srApplication = SpareroomApplication.getInstance();

        SavedSearchListAsyncTask at = new SavedSearchListAsyncTask(srApplication.getSearchFacade(), srApplication.getSpareroomContext(), _savedSearchListListener);
        _asyncManager.register(at);
        Parameters p = new Parameters();
        p.add("search_type", searchType == SearchType.WANTED ? "wanted" : "offered");
        at.execute(p);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return handleHomeButton(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        _asyncManager.cancelAll();
    }

    private void loadNewSavedSearchFragment() {
        SearchAdvertListProperties properties = (SearchAdvertListProperties) getIntent().getSerializableExtra(ResultsActivity.SEARCH_PROPERTIES_INTENT_KEY);
        NewSavedSearchFragment fragment = NewSavedSearchFragment.newInstance(properties, searchType);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.activity_savedsearches_add_save_flNew, fragment);
        transaction.commit();
    }

    private class SavedSearchesAdapter extends BaseAdapter implements IAsyncResult {
        private final SavedSearchList _lSavedSearch;
        private LayoutInflater layoutInflater;

        SavedSearchesAdapter(SavedSearchList l) {
            _lSavedSearch = l;
            layoutInflater = LayoutInflater.from(SavedSearchesAddActivity.this);
        }

        @Override
        public int getCount() {
            return _lSavedSearch.size();
        }

        @Override
        public Object getItem(int position) {
            return _lSavedSearch.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            SavedSearch savedSearch = _lSavedSearch.get(position);

            if (view == null)
                view = layoutInflater.inflate(R.layout.activity_savedsearches_add_item, parent, false);

            TextView tvTitle = view.findViewById(R.id.activity_savedsearches_item_add_tvTitle);
            TextView tvDescription = view.findViewById(R.id.activity_savedsearches_item_add_tvDescription);
            Button bReplace = view.findViewById(R.id.activity_savedsearches_item_add_bReplace);

            tvTitle.setText(savedSearch.get_name());

            StringBuilder desc = new StringBuilder();
            LinkedList<String> paramDesc = savedSearch.get_paramDesc();
            for (int i = 0; i < (paramDesc.size() - 1); i++) {
                desc.append(getParam(paramDesc.get(i))).append(", ");
            }
            desc.append(getParam(paramDesc.get(savedSearch.get_paramDesc().size() - 1)));
            tvDescription.setText(desc.toString());

            bReplace.setOnClickListener(new ReplaceOnClickListener(position));

            return view;
        }

        private String getParam(String param) {
            return AbstractAd.DSS_WELCOME.equalsIgnoreCase(param) ? getString(R.string.housing_benefit_recipients) : param;
        }

        private class ReplaceOnClickListener implements OnClickListener {
            private final int _position;

            ReplaceOnClickListener(int position) {
                _position = position;
            }

            @Override
            public void onClick(View v) {

                if (!connectivityChecker.isConnected()) {
                    SnackBarUtils.show(v, getString(R.string.no_connection));
                    return;
                }

                // track save search
                AnalyticsTrackerComposite.getInstance().trackEvent_SearchRoomsMenu_SaveSearch();

                SavedSearch savedSearch = _lSavedSearch.get(_position);
                Parameters p = new Parameters();
                p.add("format", "json");
                p.add("flatshare_type", searchType == SearchType.WANTED ? "wanted" : "offered");
                p.add("search_id", savedSearch.get_searchId());
                p.add("replace_params", "1");

                SearchAdvertListProperties properties =
                        (SearchAdvertListProperties) getIntent().getSerializableExtra(ResultsActivity.SEARCH_PROPERTIES_INTENT_KEY);
                p.add(new SearchAdvertListPropertiesAdapter().adapt(properties, false));

                (new SavedSearchEditAsyncTask(
                        SpareroomApplication.getInstance().getSearchFacade(),
                        SpareroomApplication.getInstance().getSpareroomContext(),
                        SavedSearchesAdapter.this)
                ).execute(p);
            }

        }

        @Override
        public void update(Object o) {
            Toast.makeText(SavedSearchesAddActivity.this, "Search replaced", Toast.LENGTH_LONG).show();
            setResult(RESULT_OK);
            finish();
        }

        @Override
        public void handleInvalidUserException(String message) {

        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(SavedSearchesAddActivity.this).show();

        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(SavedSearchesAddActivity.this).show();

        }

        @Override
        public void handleServiceUnavailableException(String message) {

        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(SavedSearchesAddActivity.this).show();

        }
    }

    private final IAsyncResult _savedSearchListListener = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            if (o instanceof SavedSearchList) {
                SavedSearchesAdapter adapter = new SavedSearchesAdapter((SavedSearchList) o);
                _lvSaved.setAdapter(adapter);
            }
        }

    };

}
