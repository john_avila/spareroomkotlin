package com.spareroom.ui.adapter.conversation;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.integration.imageloader.ImageLoader;
import com.spareroom.model.business.Message;
import com.spareroom.model.business.SearchType;
import com.spareroom.ui.screen.Fragment;
import com.spareroom.ui.screen.PictureGalleryActivity;
import com.spareroom.ui.screen.legacy.AdActivity;
import com.spareroom.ui.util.*;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

class MessageViewHolder extends RecyclerView.ViewHolder {

    private static final String REF = "ref";
    private final Pattern advertIdPattern = Pattern.compile("(ref)?#[0-9]{3,}+", Pattern.CASE_INSENSITIVE);
    private final Locale locale = AppVersion.flavor().getLocale();
    private final ImageLoader imageLoader;
    private final ConnectivityChecker connectivityChecker;
    private final DateUtils dateUtils;
    private final Fragment fragment;
    private final String me;
    private final String read;
    private final String otherUserFirstNames;
    private final String otherUserLastName;
    private final String loggedInUserId;
    private final String loggedInUserInitials;
    private final String userAvatarUrl;
    private final String otherUserAvatarUrl;

    private final ImageView imgAvatar;
    private final View imgAvatarForeground;
    private final TextView txtInitials;
    private final TextView txtName;
    private final TextView txtDate;
    private final TextView txtMessage;
    private final TextView txtRead;

    MessageViewHolder(View itemView, ImageLoader imageLoader, DateUtils dateUtils, ConnectivityChecker connectivityChecker,
                      Fragment fragment, Context context, String loggedInUserId, String loggedInUserInitials, String userAvatarUrl,
                      String otherUserFirstNames, String otherUserLastName, String otherUserAvatarUrl) {
        super(itemView);
        this.imageLoader = imageLoader;
        this.dateUtils = dateUtils;
        this.connectivityChecker = connectivityChecker;
        this.fragment = fragment;
        this.loggedInUserId = loggedInUserId;
        this.loggedInUserInitials = loggedInUserInitials;
        this.userAvatarUrl = userAvatarUrl;
        this.otherUserFirstNames = otherUserFirstNames;
        this.otherUserLastName = otherUserLastName;
        this.otherUserAvatarUrl = otherUserAvatarUrl;

        me = context.getString(R.string.conversation_me);
        read = context.getString(R.string.conversation_read).toUpperCase(locale);

        imgAvatar = itemView.findViewById(R.id.imgConversationDetailsListItemAvatar);
        imgAvatarForeground = itemView.findViewById(R.id.imgConversationDetailsListItemAvatarForeground);
        imgAvatarForeground.setOnClickListener(this::onAvatarClicked);

        txtInitials = itemView.findViewById(R.id.txtConversationDetailsListItemNameInitials);
        txtName = itemView.findViewById(R.id.txtConversationDetailsListItemName);
        txtDate = itemView.findViewById(R.id.txtConversationDetailsListItemDate);
        txtMessage = itemView.findViewById(R.id.txtConversationDetailsListItemMessage);
        txtRead = itemView.findViewById(R.id.txtConversationDetailsListItemRead);
    }

    void bind(int position, int itemsCount, Message message) {
        boolean isMyOwnMessage = isMyOwnMessage(message);

        // avatar
        String avatarUrl = isMyOwnMessage ? userAvatarUrl : otherUserAvatarUrl;
        imgAvatarForeground.setTag(R.id.image_url_tag, avatarUrl);
        imageLoader.loadCircularImage(avatarUrl, imgAvatar, fragment);

        // initials
        String initials = isMyOwnMessage ? loggedInUserInitials : getOtherUserInitials();
        txtInitials.setText(initials.toUpperCase(locale));

        // name
        txtName.setText(isMyOwnMessage ? me : getOtherUserDisplayName());

        // date
        txtDate.setText(dateUtils.getFormattedDateOfContact(message.getDateOfContact()));

        // message
        setMessage(message);

        // read receipt
        boolean isMessageRead = position == itemsCount - 1 && isMyOwnMessage && message.isRead();
        txtRead.setText(read);
        txtRead.setVisibility(isMessageRead ? View.VISIBLE : View.GONE);
    }

    private void setMessage(Message message) {
        String messageText = StringUtils.trim(message.getMessage());

        if (StringUtils.isNullOrEmpty(messageText)) {
            txtMessage.setText(messageText);
            return;
        }

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(messageText);
        try {
            Matcher matcher = advertIdPattern.matcher(messageText);
            while (matcher.find()) {
                String fullAdvertId = spannableStringBuilder.subSequence(matcher.start(), matcher.end()).toString().toLowerCase();
                String advertId = fullAdvertId.subSequence(fullAdvertId.startsWith(REF) ? REF.length() + 1 : 1, fullAdvertId.length()).toString();
                boolean isFromThisMessage = advertId.equalsIgnoreCase(message.getAdvertId());
                spannableStringBuilder.setSpan(new AdvertClickableSpan(advertId, isFromThisMessage, connectivityChecker),
                        matcher.start(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

            txtMessage.setText(spannableStringBuilder);
        } catch (Exception e) {
            txtMessage.setText(messageText);
        }
    }

    private String getOtherUserInitials() {
        return StringUtils.getInitials(otherUserFirstNames, otherUserLastName);
    }

    private String getOtherUserDisplayName() {
        return StringUtils.getInitialsWithFullFirstName(StringUtils.getFirstString(otherUserFirstNames), otherUserLastName);
    }

    private boolean isMyOwnMessage(Message message) {
        return loggedInUserId.equalsIgnoreCase(message.getUserId());
    }

    private void onAvatarClicked(View view) {
        String avatarUrl = (String) view.getTag(R.id.image_url_tag);

        if (!StringUtils.isNullOrEmpty(avatarUrl))
            PictureGalleryActivity.start(view.getContext(), avatarUrl);
    }

    private static class AdvertClickableSpan extends ClickableSpan {

        private final String advertId;
        private final boolean isFromThisMessage;
        private final ConnectivityChecker connectivityChecker;

        private AdvertClickableSpan(String advertId, boolean isFromThisMessage, ConnectivityChecker connectivityChecker) {
            this.advertId = advertId;
            this.isFromThisMessage = isFromThisMessage;
            this.connectivityChecker = connectivityChecker;
        }

        @Override
        public void onClick(@NonNull View view) {
            Context context = view.getContext();
            if (!connectivityChecker.isConnected()) {
                SnackBarUtils.show(view, context.getString(R.string.no_connection));
                return;
            }

            Intent advertDetails = new Intent(context, AdActivity.class);
            advertDetails.putExtra(AdActivity.ADVERT_ID, advertId);
            advertDetails.putExtra(AdActivity.SEARCH_TYPE, SearchType.OFFERED);
            advertDetails.putExtra(AdActivity.TRY_WANTED, true);
            advertDetails.putExtra(AdActivity.IS_FROM_NEW_MESSAGE, isFromThisMessage);
            context.startActivity(advertDetails);
        }
    }

}