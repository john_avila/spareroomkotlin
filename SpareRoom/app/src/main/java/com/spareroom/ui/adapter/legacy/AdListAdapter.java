package com.spareroom.ui.adapter.legacy;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.controller.legacy.ClockFeaturedAsyncTask;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.dependency.component.ComponentRepository;
import com.spareroom.integration.imageloader.ImageLoader;
import com.spareroom.model.business.*;
import com.spareroom.ui.screen.legacy.AdActivity;
import com.spareroom.ui.screen.legacy.ResultsActivity;
import com.spareroom.ui.util.StringUtils;
import com.spareroom.ui.util.UiUtils;

import java.util.List;

import javax.inject.Inject;

public class AdListAdapter extends BaseAdapter {
    private AdBoard _results;
    private final ResultsActivity _activity;
    private final SearchType searchType;
    private final int imageSize;

    private final static int TITLE_ITEM = 3;
    private final static int BROADEN_ITEM = 4;

    @Inject
    ImageLoader _imageLoader;

    public AdListAdapter(ResultsActivity activity, AdBoard objects, SearchType searchType) {
        ComponentRepository.get().getAppComponent().inject(this);
        _activity = activity;
        _results = objects;
        this.searchType = searchType;
        imageSize = activity.getResources().getDimensionPixelSize(R.dimen.conversationHeaderAvatarSize);
    }

    @Override
    public int getCount() {
        int count = _results.get_board().size();
        if (_results.get_broadenSearch().size() > 0)
            count += _results.get_broadenSearch().size() + 1;
        return count;
    }

    @Override
    public Object getItem(int position) {
        int viewType = getItemViewType(position);
        if (viewType == AbstractAd.NORMAL || viewType == AbstractAd.BOLD || viewType == AbstractAd.FEATURED)
            return _results.get_board().get(position);
        else if (viewType == TITLE_ITEM)
            return getTitleString(_activity);
        else if (viewType == BROADEN_ITEM)
            return _results.get_broadenSearch().get(position - _results.get_board().size() - 1);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < _results.get_board().size())
            return _results.get_board().get(position).getKind();
        else if (position == _results.get_board().size())
            return TITLE_ITEM;
        else if (position > _results.get_board().size())
            return BROADEN_ITEM;
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 5;
    }

    private View getAd(int position, View view, ViewGroup parent) {
        AbstractAd ad = _results.get_board().get(position);
        ImageView ivPicture;
        TextView tvLocation;
        TextView tvRoom;
        TextView tvPrice;
        TextView tvDescription;
        ImageView ivTag;
        ImageView ivSaved;

        int resPic = searchType == SearchType.OFFERED ? R.drawable.placeholder_offered : R.drawable.placeholder_wanted;
        int resKind;

        if (view == null) {
            if (ad.isFeatured()) {
                view = LayoutInflater.from(_activity).inflate(R.layout.results_list_item_featured, parent, false);
            } else if (ad.isBold()) {
                view = LayoutInflater.from(_activity).inflate(R.layout.results_list_item_bold, parent, false);
            } else {
                view = LayoutInflater.from(_activity).inflate(R.layout.results_list_item_normal, parent, false);
            }
        }

        ivSaved = view.findViewById(R.id.activity_results_result_ivSaved);
        ivSaved.setVisibility(View.GONE);
        if (ad.hasContext())
            ivSaved.setVisibility(View.VISIBLE);

        if (ad.isFeatured()) {
            ivPicture = view.findViewById(R.id.activity_results_result_featured_ivPicture);
            tvLocation = view.findViewById(R.id.activity_results_result_featured_tvLocation);
            tvRoom = view.findViewById(R.id.activity_results_result_featured_tvRoom);
            tvPrice = view.findViewById(R.id.activity_results_result_featured_tvPrice);
            tvDescription = view.findViewById(R.id.activity_results_result_featured_tvDescription);
            ivTag = view.findViewById(R.id.activity_results_result_featured_ivTag);

            resKind = R.drawable.ad_status_free_to_contact_yellow;

            TextView label = view.findViewById(R.id.activity_results_result_featured_label);
            Drawable ribbon = UiUtils.getTintedDrawable(label.getContext(), R.drawable.ribbon, R.color.yellow_2);
            label.setBackground(ribbon);
        } else if (ad.isBold()) {
            ivPicture = view.findViewById(R.id.activity_results_result_bold_ivPicture);
            tvLocation = view.findViewById(R.id.activity_results_result_bold_tvLocation);
            tvRoom = view.findViewById(R.id.activity_results_result_bold_tvRoom);
            tvPrice = view.findViewById(R.id.activity_results_result_bold_tvPrice);
            tvDescription = view.findViewById(R.id.activity_results_result_bold_tvDescription);
            ivTag = view.findViewById(R.id.activity_results_result_bold_ivTag);

            resKind = R.drawable.ad_status_free_to_contact_blue;
        } else {
            ivPicture = view.findViewById(R.id.activity_results_result_normal_ivPicture);
            tvLocation = view.findViewById(R.id.activity_results_result_normal_tvLocation);
            tvRoom = view.findViewById(R.id.activity_results_result_normal_tvRoom);
            tvPrice = view.findViewById(R.id.activity_results_result_normal_tvPrice);
            tvDescription = view.findViewById(R.id.activity_results_result_normal_tvDescription);
            ivTag = view.findViewById(R.id.activity_results_result_normal_ivTag);

            resKind = R.drawable.ad_status_free_to_contact_grey;
        }

        String price, room, location;
        if (ad instanceof AdOffered) {
            AdOffered adOffered = (AdOffered) ad;
            price = getPriceText(adOffered);
            room = getRoomText(adOffered);
            location = getLocationText(adOffered);
        } else {
            AdWanted adWanted = (AdWanted) ad;
            price = getPriceText(adWanted);
            room = getRoomText(adWanted);
            location = getLocationText(adWanted);
        }
        tvLocation.setText(location);
        tvPrice.setText(price);
        tvRoom.setText(room);
        tvDescription.setText(getDescriptionText(ad));

        final String largePictureUrl = ad.getLargePictureUrl();
        if (!StringUtils.isNullOrEmpty(largePictureUrl)) {
            if (!largePictureUrl.equals(ivPicture.getTag(R.id.image_url_tag))) {
                ivPicture.setTag(R.id.image_url_tag, largePictureUrl);
                _imageLoader.loadImage(largePictureUrl + "?square=" + imageSize, ivPicture, resPic);
            }
        } else {
            ivPicture.setTag(R.id.image_url_tag, "");
            ivPicture.setImageResource(resPic);
        }

        if (ad.getContacted())
            ivTag.setImageResource(R.drawable.ad_status_contacted);
        else {
            if (ad.getEarlyBirdRequired()) {
                ivTag.setImageResource(R.drawable.ad_status_early_bird);
            } else {
                ivTag.setImageResource(resKind);
            }
        }
        view.setOnClickListener(new AdOnClickListener(ad));

        return view;
    }

    private String getRoomText(AdOffered adOffered) {
        return adOffered.getAccommodationType();
    }

    private String getRoomText(AdWanted adWanted) {
        return adWanted.getRoomSize();
    }

    private String getLocationText(AdOffered adOffered) {
        return String.format(
                AppVersion.flavor().getLocale(),
                "%s(%s)",
                adOffered.getArea(),
                adOffered.getPostcode());
    }

    private String getLocationText(AdWanted adWanted) {
        return adWanted.getWantedAdvertiserDesc();
    }

    private String getPriceText(AdOffered adOffered) {
        return
                String.format(
                        AppVersion.flavor().getLocale(),
                        "%s%s",
                        adOffered.getPrice(),
                        adOffered.getBillsIncluded().equals(Price.BILLS_INCLUDED_YES) ? " (inc bills)" : "");
    }

    private String getPriceText(AdWanted adWanted) {
        return adWanted.getPrice();
    }

    private String getDescriptionText(AbstractAd abstractAd) {
        return String.format(AppVersion.flavor().getLocale(), "%s - %s", abstractAd.getTitle(), abstractAd.getShortDescription());
    }

    private View getTitle(View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(_activity).inflate(R.layout.activity_results_broadensearch_title, parent, false);
            TextView tvTitle = view.findViewById(R.id.activity_results_broadenSearch_title_tvTitle);
            tvTitle.setText(getTitleString(view.getContext()));
        }
        return view;
    }

    private View getBroadenSearch(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        BroadenSearch bs = _results.get_broadenSearch().get(position - _results.get_board().size() - 1);

        if (view == null) {

            if (bs.get_criteriaToAdd() != null) {
                view = LayoutInflater.from(_activity).inflate(R.layout.activity_results_broadensearch_suggestion_add, parent, false);
                TextView tvSuggestion = view.findViewById(R.id.activity_results_broadenSearch_suggestion_tvSuggestion);
                TextView tvSuggestionCount = view.findViewById(R.id.activity_results_broadenSearch_suggestion_tvSuggestionCount);
                tvSuggestion.setText(getCriteria(bs.get_criteriaToAdd()));
                tvSuggestionCount.setText(String.format(AppVersion.flavor().getLocale(), "%d", bs.get_numMatches()));
            }
            if (bs.get_criteriaToLose() != null) {
                view = LayoutInflater.from(_activity).inflate(R.layout.activity_results_broadensearch_suggestion_remove, parent, false);
                TextView tvSuggestion = view.findViewById(R.id.activity_results_broadenSearch_suggestion_tvSuggestion);
                TextView tvSuggestionCount = view.findViewById(R.id.activity_results_broadenSearch_suggestion_tvSuggestionCount);
                tvSuggestion.setText(getCriteria(bs.get_criteriaToLose()));
                tvSuggestionCount.setText(String.format(AppVersion.flavor().getLocale(), "%d", bs.get_numMatches()));
            }
        }

        if (view != null)
            view.setOnClickListener(new BroadenSearchOnClickListener(bs));
        return view;
    }

    private String getCriteria(String criteria) {
        if (StringUtils.isNullOrEmpty(criteria))
            return criteria;

        return criteria.replaceAll(AbstractAd.DSS_OK, _activity.getString(R.string.housing_benefit_recipients));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int viewType = getItemViewType(position);
        if (viewType == AbstractAd.NORMAL || viewType == AbstractAd.BOLD || viewType == AbstractAd.FEATURED)
            return getAd(position, convertView, parent);
        else if (viewType == TITLE_ITEM)
            return getTitle(convertView, parent);
        else if (viewType == BROADEN_ITEM)
            return getBroadenSearch(position, convertView, parent);
        return null;
    }

    public void add(AdWanted ad) {
        _results.get_board().add(ad);
    }

    public void add(AdOffered ad) {
        List<AbstractAd> board = _results.get_board();

        for (int i = 0; i < board.size(); i += 10) {
            if (board.get(i).isFeatured() && board.get(i).getId().equals(ad.getId()))
                return;
        }
        board.add(ad);
    }

    public void setBroadenSearch(BroadenSearches broadenSearch) {
        _results.set_broadenSearch(broadenSearch);
    }

    private class AdOnClickListener implements OnClickListener {
        private final AbstractAd _ad;

        private AdOnClickListener(AbstractAd ad) {
            _ad = ad;
        }

        @Override
        public void onClick(View v) {
            if (_ad.isFeatured()) {
                ClockFeaturedAsyncTask at = new ClockFeaturedAsyncTask(SpareroomApplication.getInstance(_activity.getApplicationContext()).getSearchFacade());
                Parameters params = new Parameters();
                params.add("fad_id", _ad.getId());
                Parameters[] vParams = new Parameters[1];
                vParams[0] = params;
                at.execute(vParams);
            }

            Intent adActivity = new Intent();
            adActivity.setClass(_activity, AdActivity.class);
            adActivity.putExtra("id", _ad.getId());
            adActivity.putExtra("searchType", searchType);
            _activity.startActivityForResult(adActivity, 1);
        }
    }

    private class BroadenSearchOnClickListener implements OnClickListener {
        private final BroadenSearch _bs;

        BroadenSearchOnClickListener(BroadenSearch bs) {
            _bs = bs;
        }

        @Override
        public void onClick(View v) {
            AnalyticsTrackerComposite
                    .getInstance()
                    .trackEvent_Search_BroadenSearch(
                            ((SpareroomApplication.getInstance(_activity.getApplicationContext()).getSpareroomContext()).getSession() != null));
            _activity.broadenSearch(_bs);
        }
    }

    public AdBoard get_results() {
        return _results;
    }

    public void set_results(AdBoard adBoard) {
        _results = adBoard;
    }

    private String getTitleString(Context context) {
        int boardSize = _results.get_board().size();
        return String.format(
                AppVersion.flavor().getLocale(),
                "%s %d %s, %s",
                boardSize == 0 ? "" : context.getString(R.string.only),
                boardSize,
                context.getResources().getQuantityString(R.plurals.results, boardSize),
                context.getString(R.string.try_adjusting_your_criteria));
    }

}