package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.ui.widget.PlaceAdButton;

public class PlaceAdWantedStayAccommodationFragment extends PlaceAdFragment {
    private PlaceAdButton _previouslySelected;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_wanted_stay_accommodation, container, false);

        setTitle(getString(R.string.place_ad_wanted_stay_accommodation_title));

        final PlaceAdButton bSeven = rootView.findViewById(R.id.place_ad_wanted_stay_bSeven);
        final PlaceAdButton bMonFri = rootView.findViewById(R.id.place_ad_wanted_stay_bMonFri);
        final PlaceAdButton bWeekends = rootView.findViewById(R.id.place_ad_wanted_stay_bWeekends);

        final DraftAdWanted draft = ((DraftAdWanted) getDraftAd());

        // In case there is an activity state change and we lost the draft
        if (draft == null) {
            getActivity().finish();

            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }

        DraftAdWanted.DaysOfWeek propertyType = draft.getDaysOfWeek();

        if (propertyType != null)
            switch (propertyType) {
                case MONDAY_TO_FRIDAY:
                    _previouslySelected = bMonFri;
                    bMonFri.setPreviouslySelected();
                    break;
                case SEVEN_A_DAY:
                    _previouslySelected = bSeven;
                    bSeven.setPreviouslySelected();
                    break;
                case WEEKENDS_ONLY:
                    _previouslySelected = bWeekends;
                    bWeekends.setPreviouslySelected();
                    break;
                default:
            }

        bSeven.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bSeven.setPreviouslySelected();
            draft.setDaysOfWeek(DraftAdWanted.DaysOfWeek.SEVEN_A_DAY);
            PlaceAdWantedStayAccommodationFragment.this.finish();
        });

        bMonFri.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bMonFri.setPreviouslySelected();
            draft.setDaysOfWeek(DraftAdWanted.DaysOfWeek.MONDAY_TO_FRIDAY);
            PlaceAdWantedStayAccommodationFragment.this.finish();
        });

        bWeekends.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bWeekends.setPreviouslySelected();
            draft.setDaysOfWeek(DraftAdWanted.DaysOfWeek.WEEKENDS_ONLY);
            PlaceAdWantedStayAccommodationFragment.this.finish();
        });

        return rootView;
    }
}
