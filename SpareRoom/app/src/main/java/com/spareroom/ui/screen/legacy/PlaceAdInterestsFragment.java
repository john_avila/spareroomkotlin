package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.model.business.DraftAd;
import com.spareroom.ui.util.KeyboardUtils;

import androidx.annotation.Nullable;

public class PlaceAdInterestsFragment extends PlaceAdFragment {
    private EditText _etInterests;
    private Button bConfirm;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.place_ad_interests, container, false);
        bConfirm = rootView.findViewById(R.id.place_ad_bConfirm);
        _etInterests = rootView.findViewById(R.id.place_ad_interests_etInterests);

        final DraftAd draft = getDraftAd();

        // In case there is an activity state change and we lost the draft
        if (draft == null) {
            getActivity().finish();
            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();
            return null;
        }

        _etInterests.setText(draft.getInterests());

        // Takes care of the actions in the EditText
        _etInterests.setOnEditorActionListener((v, actionId, event) -> {
            boolean ret = false; // Determines if we take control of the event (and not the system) (true, otherwise false)

            // Manages the "Enter" button
            if (event.getAction() == EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                KeyboardUtils.hideKeyboard(v);
                ret = true; // We don't want the system take care of the event

            }

            return ret;
        });

        bConfirm.setOnClickListener(v -> {
            draft.setInterests(_etInterests.getText().toString());
            finish();
        });

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bConfirm.setOnClickListener(null);
        _etInterests.setOnEditorActionListener(null);
    }

}
