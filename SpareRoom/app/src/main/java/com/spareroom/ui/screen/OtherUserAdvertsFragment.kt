package com.spareroom.ui.screen

import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.spareroom.R
import com.spareroom.integration.dependency.Injectable
import com.spareroom.model.business.*
import com.spareroom.ui.adapter.OtherUserAdvertsAdapter
import com.spareroom.ui.util.*
import com.spareroom.ui.util.AnimationUtils.clearAnimation
import com.spareroom.ui.util.AnimationUtils.startFadeAnimation
import com.spareroom.ui.util.UiUtils.screenHeight
import com.spareroom.ui.widget.AdvertCardViewSkeleton
import com.spareroom.viewmodel.ConversationViewModel
import kotlinx.android.synthetic.main.other_user_adverts_fragment.*
import javax.inject.Inject

class OtherUserAdvertsFragment : Fragment(), Injectable {

    private val noNetwork by lazy { getString(R.string.errorOffline_body) }
    private val noNetworkTitle by lazy { getString(R.string.no_connection) }
    private val defaultErrorTitle by lazy { getString(R.string.oops) }
    private val retry by lazy { getString(R.string.retry).toUpperCase() }
    private val failedToLoadAdverts by lazy { getString(R.string.adverts_loading_error) }
    private val advertId by lazy { viewModel.conversationInfo.advertId }
    private val otherUserId by lazy { viewModel.conversationInfo.otherUserId }
    private val otherUserName by lazy { StringUtils.getFirstString(viewModel.conversationInfo.otherUserFirstNames) }
    private val viewModel by lazy { ViewModelProviders.of(activity!!, viewModelFactory).get(ConversationViewModel::class.java) }
    private val layoutManager by lazy { LinearLayoutManager(activity) }
    private var canShowErrorView = false
    private val scrollState = ScrollState()

    @Inject
    lateinit var adapter: OtherUserAdvertsAdapter

    @Inject
    lateinit var advertUtils: AdvertUtils

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var connectivityChecker: ConnectivityChecker

    companion object {
        fun getInstance(): OtherUserAdvertsFragment {
            return OtherUserAdvertsFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.other_user_adverts_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)

        adapter.init(activity!!, this, advertId)

        setUpRecyclerView()
        setUpSwipeRefreshLayout()
        showLoadingView(true)

        canShowErrorView = canShowErrorView()

        observeLoadAdverts()
        observeLoadConversation()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.refresh_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_item_refresh -> handleRefreshAdvertsOption()
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun handleRefreshAdvertsOption(): Boolean {
        if (canShowErrorView) {
            loadAdvertsWithLoadingView()
        } else {
            swipeRefreshLayout.isRefreshing = true
            loadAdverts()
        }

        return true
    }

    override fun onStart() {
        super.onStart()
        recyclerViewAdverts.adapter = adapter
        scrollState.restore(layoutManager)
    }

    override fun onStop() {
        super.onStop()
        scrollState.save(layoutManager, recyclerViewAdverts)
        recyclerViewAdverts.adapter = null
    }

    private fun setUpRecyclerView() {
        recyclerViewAdverts.setHasFixedSize(true)
        recyclerViewAdverts.layoutManager = layoutManager
    }

    private fun setUpSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(activity!!, R.color.material_deep_sea_blue))
        swipeRefreshLayout.setOnRefreshListener { loadAdverts() }
    }

    private fun showLoadingView(showLoadingView: Boolean) {
        if (showLoadingView) {
            recyclerViewAdverts.visibility = View.GONE
            addLoadingViewAdverts()
            loadingView.visibility = View.VISIBLE
            startFadeAnimation(loadingView, emptyList(), ObjectAnimator.INFINITE)
        } else {
            loadingView.visibility = View.GONE
            recyclerViewAdverts.visibility = View.VISIBLE
            clearAnimation(loadingView)
        }
    }

    private fun addLoadingViewAdverts() {
        val inflater = LayoutInflater.from(activity)
        loadingView.removeAllViews()
        val items = (screenHeight(activity) / resources.getDimensionPixelSize(R.dimen.advert_image_height)) + 1
        for (i in 0 until items) {
            inflater.inflate(R.layout.other_user_adverts_loading, loadingView, true)
            loadingView.getChildAt(i).findViewById<AdvertCardViewSkeleton>(R.id.advertSkeletonItem).hideHeader()
            val header = loadingView.getChildAt(i).findViewById<TextView>(R.id.txtHeader)
            header.visibility = if (i in 0..1) View.VISIBLE else View.GONE
            if (i == 1)
                header.text = advertUtils.getFormattedOtherUserAdvertsHeader(otherUserName)
        }
    }

    private fun loadAdverts() {
        if (hasConversationLoaded()) {
            viewModel.otherUserAdvertsLiveData.loadOtherUserAdverts()
        } else {
            viewModel.fetchConversation.loadConversation(GetConversationRequest(otherUserId, advertId))
        }
    }

    private fun canShowErrorView(): Boolean {
        return !hasConversationLoaded() || !hasAdvertsLoaded()
    }

    private fun hasConversationLoaded(): Boolean {
        val conversationResponse = viewModel.fetchConversation.value
        return conversationResponse != null && conversationResponse.isSuccessful
    }

    private fun hasAdvertsLoaded(): Boolean {
        val advertsResponse = viewModel.otherUserAdvertsLiveData.value
        return advertsResponse != null && advertsResponse.isSuccessful
    }

    private fun observeLoadAdverts() {
        viewModel.otherUserAdvertsLiveData.observe(this, Observer { response ->
            swipeRefreshLayout.isRefreshing = false
            showLoadingView(false)

            when (response) {
                is OtherUserAdvertsResponseSuccess -> handleAdvertsLoaded(response)
                is OtherUserAdvertsResponseFailure -> handleAdvertsLoadingFailure(response)
            }
        })
    }

    private fun observeLoadConversation() {
        viewModel.fetchConversation.observe(this, Observer { response ->
            response?.let {
                if (it.isSuccessful) {
                    loadAdverts()
                    return@let
                }

                swipeRefreshLayout.isRefreshing = false
                if (canShowErrorView) {
                    showLoadingView(false)
                    val isConnectionError = it.isNoConnectionException
                    val errorMessage = if (isConnectionError) noNetwork else failedToLoadAdverts
                    showErrorView(isConnectionError, errorMessage)
                }

            }
        })
    }

    private fun handleAdvertsLoaded(response: OtherUserAdvertsResponseSuccess) {
        adapter.update(response.diffResult, response.adverts, response.otherUserFirstName)
        canShowErrorView = false
        errorView.hide()
    }

    private fun handleAdvertsLoadingFailure(failureResponse: OtherUserAdvertsResponseFailure) {
        val errorMessage = failureResponse.getErrorMessage(activity, failedToLoadAdverts)
        if (canShowErrorView) {
            val isConnectionError = failureResponse.isNoConnectionException
            showErrorView(isConnectionError, if (isConnectionError) noNetwork else errorMessage)
            return
        }

        SnackBarUtils.show(recyclerViewAdverts, errorMessage)
    }

    private fun showErrorView(isConnectionError: Boolean, message: String) {
        errorView.title(if (isConnectionError) noNetworkTitle else defaultErrorTitle)
        errorView.message(message)
        errorView.icon(if (isConnectionError) R.drawable.ic_offline else R.drawable.icon_error)
        errorView.action(View.OnClickListener { loadAdvertsWithLoadingView() }, retry)
        errorView.show(isConnectionError)
    }

    private fun loadAdvertsWithLoadingView() {
        if (connectivityChecker.isConnected()) {
            showLoadingView(true)
            errorView.hide()
            loadAdverts()
        } else {
            if (errorView.isVisible() && errorView.isConnectionError) {
                startFadeAnimation(errorView, emptyList(), 0)
                return
            }

            showErrorView(true, noNetwork)
        }
    }
}
