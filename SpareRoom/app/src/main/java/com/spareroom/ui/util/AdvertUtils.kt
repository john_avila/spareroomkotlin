package com.spareroom.ui.util

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.view.View
import androidx.annotation.StringRes
import com.spareroom.R
import com.spareroom.model.business.*
import com.spareroom.model.business.advertiser.*
import com.spareroom.ui.screen.ConversationActivity
import com.spareroom.ui.screen.legacy.AdActivity
import com.spareroom.ui.util.StringUtils.EN_DASH
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject
import kotlin.math.ceil

private const val TWO = 2
private const val WEEKS_IN_A_YEAR = 52.0
private const val MONTHS_IN_A_YEAR = 12.0

class AdvertUtils @Inject constructor(
    private val locale: Locale,
    private val context: Application,
    private val connectivityChecker: ConnectivityChecker) {

    private val notAvailable = string(R.string.not_available)
    private val currencySymbol = string(R.string.currency_symbol)
    private val single = string(R.string.single)
    private val singleFirstLetterUpperCase = string(R.string.single_first_letter_upper_case)
    private val doubleString = string(R.string.double_lower_case)
    private val doubleFirstLetterUpperCase = string(R.string.double_first_letter_upper_case)
    private val bedroom = string(R.string.bedroom)
    private val bedrooms = string(R.string.bedrooms)
    private val withEnsuite = string(R.string.with_ensuite)
    private val includingBills = string(R.string.includingBills)
    private val includingSomeBills = string(R.string.includingSomeBills)
    private val excludingBills = string(R.string.excludingBills)
    private val newnessNew = string(R.string.newness_new).toUpperCase(locale)
    private val newnessNewToday = string(R.string.newness_new_today).toUpperCase(locale)
    private val newnessBoosted = string(R.string.newness_boosted).toUpperCase(locale)
    private val studio = string(R.string.studio)
    private val flat = string(R.string.flat)
    private val house = string(R.string.house)
    private val property = string(R.string.property)
    private val searchingFor = string(R.string.searching_for)
    private val inString = string(R.string.in_string)
    private val areas = string(R.string.areas)
    private val including = string(R.string.including)
    private val noConnection = string(R.string.no_connection)
    private val or = string(R.string.or)
    private val twin = string(R.string.twin)
    private val whole = string(R.string.whole)
    private val adverts = string(R.string.adverts)
    private val agent = string(R.string.agent)
    private val liveInLandlord = string(R.string.live_in_landlord)
    private val liveOutLandlord = string(R.string.live_out_landlord)
    private val currentFlatmate = string(R.string.current_flatmate)
    private val formerFlatmate = string(R.string.former_flatmate)
    private val couple = string(R.string.couple)
    private val friends = string(R.string.friends)
    private val professional = string(R.string.professional)
    private val student = string(R.string.student)

    fun openAdvertDetails(activity: Activity, advert: AbstractAd, view: View, regardingAdvertId: String = "") {
        if (!connectivityChecker.isConnected()) {
            SnackBarUtils.show(view, noConnection)
            return
        }

        val advertDetails = Intent(activity, AdActivity::class.java)
        advertDetails.putExtra(AdActivity.ADVERT_ID, advert.id)
        advertDetails.putExtra(AdActivity.SEARCH_TYPE, if (advert is AdOffered) SearchType.OFFERED else SearchType.WANTED)
        advertDetails.putExtra(AdActivity.IS_FROM_NEW_MESSAGE, regardingAdvertId == advert.id)
        activity.startActivityForResult(advertDetails, ConversationActivity.ADVERT_DETAILS_REQUEST_CODE)
    }

    fun getFormattedLocation(advert: AdOffered, notAvailable: String): String {
        val area = advert.area
        val postcode = advert.postcode

        if (area.isBlank() && postcode.isBlank())
            return notAvailable

        val builder = StringBuilder()
        if (area.isNotBlank())
            builder.append(area)

        if (postcode.isNotBlank())
            builder.append(String.format(if (builder.isNotBlank()) " (%s)" else "%s", postcode))

        return builder.toString()
    }

    fun getFormattedMonthlyPrice(advert: AdOffered, includeBills: Boolean): String {
        return try {
            if (advert.isWholePropertyForRent()) {
                getFormattedPriceForWholeProperty(advert, includeBills)
            } else {
                getFormattedPriceForRoomBasedRent(advert, includeBills)
            }
        } catch (e: Exception) {
            notAvailable
        }
    }

    fun getFormattedMonthlyBudget(advert: AdWanted): String {
        return try {
            val budget = advert.combinedBudget
            val pricePeriod = advert.pricePeriod
            String.format("%s%s", currencySymbol, getMonthlyPriceBasedOnPeriod(budget, pricePeriod))
        } catch (e: Exception) {
            notAvailable
        }
    }

    fun getFormattedDeposit(advert: AdOffered): String {
        return try {
            if (advert.isWholePropertyForRent()) {
                getFormattedDepositForWholeProperty(advert)
            } else {
                getFormattedDepositForRoomBasedRent(advert)
            }
        } catch (e: Exception) {
            notAvailable
        }
    }

    fun getFormattedNewnessStatus(advert: AbstractAd): String {
        if (advert.hasNewnessStatus()) {
            if (advert.isNew)
                return newnessNew

            if (advert.isNewToday)
                return newnessNewToday

            if (advert.isSuperRenewedToday)
                return newnessBoosted
        }

        return ""
    }

    fun getFormattedWantedAdvertTitle(advert: AdWanted): String {
        val roomSize = advert.roomSize

        return when {
            roomSize.isBlank() || roomSize == SINGLE_OR_DOUBLE_ROOM -> "$searchingFor $single $or $doubleString $bedroom"
            roomSize.startsWith(DOUBLE_ROOM) -> "$searchingFor $doubleString $bedroom"
            roomSize == TWO_ROOMS -> "$searchingFor $TWO $bedrooms"
            roomSize == TWIN_ROOM_OR_2_ROOMS -> "$searchingFor $TWO $bedrooms $or $twin"
            roomSize == TWO_DOUBLE_ROOMS -> "$searchingFor $TWO $doubleString $bedrooms"
            roomSize == TWO_SINGLE_ROOMS -> "$searchingFor $TWO $single $bedrooms"
            roomSize == FLAT_OR_HOUSE_TO_RENT -> "$searchingFor $whole $property"
            else -> "$searchingFor $single $or $doubleString $bedroom"
        }
    }

    fun getFormattedOfferedAdvertTitle(advert: AdOffered): String {
        if (advert.isStudioFlat())
            return "$studio $flat"

        if (advert.isStudioHouse())
            return "$studio $house"

        if (advert.isStudioProperty())
            return "$studio $property"

        return if (advert.isWholePropertyForRent()) {
            getFormattedTitleForWholeProperty(advert)
        } else {
            getFormattedTitleForRoomBasedProperty(advert.rooms)
        }
    }

    fun getFormattedLocationForOfferedAdvert(advert: AdOffered): String {
        val location = getFormattedLocation(advert, "")
        return if (location.isNotBlank()) String.format("%s %s", inString, location) else ""
    }

    fun getFormattedLocationForWantedAdvert(advert: AdWanted): String {
        val numberOfAreas = advert.numberOfAreas
        val areasList = advert.searchAreasAsList()

        if (numberOfAreas <= 0 || areasList.isEmpty())
            return ""

        return if (numberOfAreas == 1) {
            String.format("%s %s", inString, if (advert.searchAreas.isNotBlank()) advert.searchAreas else areasList[0])
        } else {
            String.format("%s %s %s %s %s", inString, numberOfAreas, areas, including, areasList[0])
        }
    }

    fun getFormattedOtherUserAdvertsHeader(otherUser: String): String {
        if (otherUser.isBlank())
            return adverts

        val possessive = if (otherUser.endsWith("s")) "'" else "'s"
        return "${otherUser.capitalize()}$possessive ${adverts.toLowerCase(locale)}"
    }

    fun fromPwToPcmRent(rent: Int): Int {
        // This formula is the one they use in the API
        return ceil(rent * (WEEKS_IN_A_YEAR / MONTHS_IN_A_YEAR)).toInt()
    }

    fun getFormattedAdvertiserType(advertiserType: String): String {
        return when (advertiserType) {
            AdvertiserType.AGENT -> agent
            AdvertiserType.CURRENT_FLATMATE, AdvertiserType.CURRENT_TENANTS -> currentFlatmate
            AdvertiserType.FORMER_FLATMATE -> formerFlatmate
            AdvertiserType.LIVE_IN_LANDLORD -> liveInLandlord
            AdvertiserType.LIVE_OUT_LANDLORD -> liveOutLandlord
            else -> ""
        }
    }

    fun getFormattedProfession(profession: String, numberOfSeekers: Int, couples: Boolean): String {
        return when {
            numberOfSeekers == 1 -> profession(profession)
            numberOfSeekers > 1 -> {
                val formattedProfession = profession(profession)
                val relationship = if (couples) couple else friends

                if (formattedProfession.isNotBlank()) "$formattedProfession $relationship" else relationship.capitalize()
            }
            else -> ""
        }
    }

    fun getFormattedCompanyName(companyName: String, fullName: String): String {
        val sanitisedCompanyName = companyName.sanitise()
        if (sanitisedCompanyName.isNotBlank())
            return StringUtils.capitalizeEachFirstLetter(sanitisedCompanyName)

        val sanitisedFullName = fullName.sanitise()
        return StringUtils.capitalizeEachFirstLetter(sanitisedFullName)
    }

    fun getFormattedExtraSeekersCount(advertiser: Advertiser): String {
        val seekersCount = (advertiser as? WantedAdvertiser)?.numberOfSeekers ?: 0
        return if (seekersCount > 1) "${StringUtils.PLUS_CHARACTER}${seekersCount - 1}" else ""
    }

    private fun profession(profession: String): String {
        return when (profession) {
            AbstractAd.PROFESSION_PROFESSIONAL -> professional
            AbstractAd.PROFESSION_STUDENT -> student
            else -> ""
        }.capitalize()
    }

    private fun getFormattedTitleForWholeProperty(advert: AdOffered): String {
        val rooms = advert.roomsInProperty
        return if (rooms > 0) String.format("%s %s %s", rooms, bedroom, getPropertyType(advert)) else ""
    }

    private fun getPropertyType(advert: AdOffered): String {
        if (advert.isFlat())
            return flat

        if (advert.isHouse())
            return house

        return if (advert.isProperty()) property else ""
    }

    private fun getFormattedTitleForRoomBasedProperty(rooms: List<AdRoom>): String {
        if (rooms.isEmpty())
            return ""

        var singleRooms = 0
        var doubleRooms = 0
        var hasEnsuite = false

        rooms.forEach { room ->
            if (room.hasEnsuite())
                hasEnsuite = true

            if (room.isSingle()) {
                ++singleRooms
            } else if (room.isDouble()) {
                ++doubleRooms
            }
        }

        // if only one room present
        if (singleRooms + doubleRooms == 1) {
            val propertyType = StringBuilder()
            propertyType.append(if (singleRooms == 1) singleFirstLetterUpperCase else doubleFirstLetterUpperCase)
            propertyType.append(" ")
            propertyType.append(bedroom)

            if (hasEnsuite) {
                propertyType.append(" ")
                propertyType.append(withEnsuite)
            }

            return propertyType.toString()
        }

        return if (singleRooms > 0 && doubleRooms > 0) {
            String.format("%s %s & %s %s %s", singleRooms, single, doubleRooms, doubleString, bedrooms)
        } else if (singleRooms > 0) {
            String.format("%s %s %s", singleRooms, single, bedrooms)
        } else {
            String.format("%s %s %s", doubleRooms, doubleString, bedrooms)
        }
    }

    private fun getFormattedDepositForWholeProperty(advert: AdOffered): String {
        val rooms = advert.rooms
        val hasMainDeposit = advert.securityDeposit.isNotBlank()
        if (!hasMainDeposit && !hasDeposit(rooms))
            throw Exception("deposit not specified")

        if (hasMainDeposit) {
            val deposit = BigDecimal(advert.securityDeposit)
            return formatDeposit(deposit, deposit)
        }

        // deposit here is a sum of individual deposits
        if (rooms.isEmpty() || !hasAvailableRooms(rooms))
            throw Exception("deposit not specified")

        val deposit = rooms
            .filter { it.securityDeposit.isNotBlank() && it.isAvailable() }
            .sumBy { it.securityDeposit.toInt() }

        val depositAsBigDecimal = BigDecimal(deposit)
        return formatDeposit(depositAsBigDecimal, depositAsBigDecimal)
    }

    private fun getFormattedDepositForRoomBasedRent(advert: AdOffered): String {
        val rooms = advert.rooms
        if (rooms.isEmpty() || !hasAvailableRooms(rooms) || !hasDeposit(rooms))
            throw Exception("deposit not specified")

        var minDepositPrice = BigDecimal(Int.MAX_VALUE)
        var maxDepositPrice = BigDecimal.ZERO

        rooms.filter { it.securityDeposit.isNotBlank() && it.isAvailable() }.forEach { room ->
            val price = BigDecimal(room.securityDeposit)
            minDepositPrice = minDepositPrice.min(price)
            maxDepositPrice = maxDepositPrice.max(price)
        }

        return formatDeposit(minDepositPrice, maxDepositPrice)
    }

    private fun formatDeposit(minDepositPrice: BigDecimal, maxDepositPrice: BigDecimal): String {
        return if (minDepositPrice.toInt() == maxDepositPrice.toInt()) {
            String.format("%s%s", currencySymbol, minDepositPrice.toInt())
        } else {
            String.format("%s%s%s%s%s", currencySymbol, minDepositPrice.toInt(), EN_DASH, currencySymbol, maxDepositPrice.toInt())
        }
    }

    private fun getFormattedPriceForWholeProperty(advert: AdOffered, includeBills: Boolean): String {
        if (advert.minRent.isNotBlank()) {
            val price = getMonthlyPriceBasedOnPeriod(advert.minRent, advert.pricePeriod)
            return formatPrice(price, price, advert, includeBills)
        }

        // price here is a sum of individual prices
        val rooms = advert.rooms
        if (rooms.isEmpty() || !hasAvailableRooms(rooms))
            throw Exception("no available rooms")

        val price = rooms
            .filter { it.isAvailable() }
            .sumBy { getMonthlyPriceBasedOnPeriod(it.price, it.pricePeriod) }

        return formatPrice(price, price, advert, includeBills)
    }

    private fun getFormattedPriceForRoomBasedRent(advert: AdOffered, includeBills: Boolean): String {
        val rooms = advert.rooms
        if (rooms.isEmpty() || !hasAvailableRooms(rooms))
            throw Exception("no available rooms")

        var minPrice = Int.MAX_VALUE
        var maxPrice = 0
        rooms.filter { it.isAvailable() }.forEach { room ->
            val price = getMonthlyPriceBasedOnPeriod(room.price, room.pricePeriod)
            minPrice = minOf(price, minPrice)
            maxPrice = maxOf(price, maxPrice)
        }

        return formatPrice(minPrice, maxPrice, advert, includeBills)
    }

    private fun formatPrice(minPrice: Int, maxPrice: Int, advert: AdOffered, includeBills: Boolean): String {
        if (includeBills) {
            return if (minPrice == maxPrice) {
                String.format("%s%s %s", currencySymbol, minPrice, getBillsIncluded(advert))
            } else {
                String.format("%s%s%s%s%s %s", currencySymbol, minPrice, EN_DASH, currencySymbol, maxPrice, getBillsIncluded(advert))
            }
        }

        return if (minPrice == maxPrice) {
            String.format("%s%s", currencySymbol, minPrice)
        } else {
            String.format("%s%s%s%s%s", currencySymbol, minPrice, EN_DASH, currencySymbol, maxPrice)
        }
    }

    private fun hasAvailableRooms(rooms: List<AdRoom>): Boolean {
        return rooms.any { it.isAvailable() }
    }

    private fun hasDeposit(rooms: List<AdRoom>): Boolean {
        return rooms.any { it.securityDeposit.isNotBlank() }
    }

    private fun getBillsIncluded(advert: AdOffered): String {
        if (Price.BILLS_INCLUDED_YES.equals(advert.billsIncluded, true))
            return includingBills

        if (Price.BILLS_INCLUDED_SOME.equals(advert.billsIncluded, true))
            return includingSomeBills

        return if (Price.BILLS_INCLUDED_NO.equals(advert.billsIncluded, true)) excludingBills else ""
    }

    private fun getMonthlyPriceBasedOnPeriod(priceAsString: String, period: String): Int {
        val price = priceAsString.toInt()
        return if (!Price.PERIODICITY_WEEK.equals(period, true)) price else fromPwToPcmRent(price)
    }

    private fun string(@StringRes stringRes: Int) = context.getString(stringRes)
}
