package com.spareroom.ui.flow;

import java.util.Hashtable;

/**
 * Common features for the fragment flows.
 * <p>
 * Created by miguel.rossi on 18/07/2016.
 */
public class FlowState {

    protected String[] SCREENS;

    // We should not have two fragments with the same name in the table
    private Hashtable<String, Integer> _fragmentPositionTable;

    private final ProgressCalculator _progressCalculator = new ProgressCalculator();

    public FlowState() {
    }

    protected void init() {
        _fragmentPositionTable = new Hashtable<>();
        for (int i = 0; i < SCREENS.length; i++) {
            _fragmentPositionTable.put(SCREENS[i], i);
        }
    }

    /**
     * Returns previous fragment name.
     *
     * @param currentFragment the name of the current fragment
     * @param businessObject  the business object completed in the multi step process;
     *                        <code>null</code> if there is no business object involved
     * @return the previous fragment name, <code>null</code> if there is no previous fragment for the
     * <code>currentFragment</code> or the <code>currentFragment</code> doesn't
     * exist
     */
    public String getPreviousFragmentName(String currentFragment, Object businessObject) {
        String previousFragment = null;

        if (currentFragment != null) {
            if (_fragmentPositionTable.get(currentFragment) != null) {
                int prevFragmentPosition = _fragmentPositionTable.get(currentFragment) - 1;

                if (prevFragmentPosition >= 0)
                    previousFragment = SCREENS[prevFragmentPosition];
            }
        }

        return previousFragment;
    } // end getPreviousFragmentName(String currentFragment)

    /**
     * Returns the following fragment name.
     *
     * @param currentFragment the name of the current fragment; if <code>null</code> it returns the
     *                        first fragment of the list if the list is not empty
     * @param businessObject  the business object completed in the multi step process;
     *                        <code>null</code> if there is no business object involved
     * @return the following fragment name, <code>null</code> if no fragments are set for this state, or
     * if the current fragment is the last of the list and there are fragments in
     * the list
     */
    public String getNextFragmentName(String currentFragment, Object businessObject) {
        String nextString = null;

        if (currentFragment != null) {
            if (_fragmentPositionTable.get(currentFragment) != null) {
                int nextFragmentPosition = _fragmentPositionTable.get(currentFragment) + 1;

                if (nextFragmentPosition < SCREENS.length)
                    nextString = SCREENS[nextFragmentPosition];
            }

        } else {
            if (SCREENS.length > 0)
                nextString = SCREENS[0];

        }

        return nextString;
    } // end getNextFragmentName(String currentFragment)

    /**
     * Lets us know if it is the first fragment.
     *
     * @param fragment the fragment to check
     * @return <code>true</code> if yes; <code>false</code> if no; <code>null</code> if the fragment
     * is not in the list
     */
    public Boolean isFirstFragment(String fragment) {
        return (_fragmentPositionTable.get(fragment) == 0);
    }

    /**
     * Lets us know if it is the last fragment.
     *
     * @param fragment the fragment to check
     * @return <code>true</code> if yes; <code>false</code> if not; <code>null</code> if the
     * <code>fragment</code> is not in the list
     */
    public Boolean isLastFragment(String fragment) {
        return _fragmentPositionTable.get(fragment) == (SCREENS.length - 1);
    }

    /**
     * Returns fragment at <code>pos</code> position.
     *
     * @param pos position in the fragment list
     * @return name of the fragment, <code>null</code> if out ouf bounds
     */
    public String fragmentAtPosition(int pos) {
        String fragment = null;

        if ((pos >= 0) && (pos < SCREENS.length))
            fragment = SCREENS[pos];

        return fragment;
    } // end getFragment(int pos)

    /**
     * Returns the position of a fragment.
     *
     * @param fragment the fragment to know about
     * @return the position of the fragment; <code>-1</code> if the fragment doesn't exist in the list
     */
    public int fragmentPosition(String fragment) {
        int position = -1;

        if (fragment != null)
            if (_fragmentPositionTable.get(fragment) != null)
                position = _fragmentPositionTable.get(fragment);

        return position;
    }

    /**
     * Returns the length of the fragment list.
     *
     * @return the size; <code>-1</code> if the list have a <code>null</code> value
     */
    public int fragmentListSize() {
        int length = -1;

        if (SCREENS != null)
            length = SCREENS.length;

        return length;
    } // end sizeFragmentList()

    /**
     * Return a <code>ProgressCalculator</code>.
     *
     * @return a <code>ProgressCalculator</code>
     * @see FlowState.ProgressCalculator
     */
    public ProgressCalculator getProgressCalculator() {
        return _progressCalculator;
    }

    /**
     * Util for calculate the ProgressBar features for those fragments that have a ProgressBar.
     */
    public class ProgressCalculator {

        /**
         * Returns the fragment position in percentage.
         *
         * @param fragment current fragment
         * @return the position in percentage, <code>-1</code> if the fragment is not in the list
         */
        public int progress(String fragment) {
            int position = -1;

            if ((fragment == null) || (SCREENS.length == 0) || isLastFragment(fragment) == null)
                position = 0;
            else if (isLastFragment(fragment))
                position = 100;
            else {
                if (_fragmentPositionTable.get(fragment) != null)
                    position = (int) ((((float) (_fragmentPositionTable.get(fragment)) / ((float) SCREENS.length - 1))) * 100);
            }

            return position;
        }

    } // end class ProgressCalculator

}
