package com.spareroom.ui.screen.legacy;

import android.app.Dialog;
import android.content.*;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Pair;
import android.util.TypedValue;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import android.widget.LinearLayout.LayoutParams;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.controller.legacy.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.business.legacy.rest.apiv1.MessageParamEnum;
import com.spareroom.integration.sharedpreferences.EventsDao;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.model.business.*;
import com.spareroom.model.legacy.ISearchFacade;
import com.spareroom.ui.adapter.AdvertPictureAdapter;
import com.spareroom.ui.screen.*;
import com.spareroom.ui.util.*;
import com.spareroom.ui.widget.ScrollView;

import java.util.List;
import java.util.Vector;

import javax.inject.Inject;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.*;

public class AdActivity extends InjectableActivity implements IRetriableActivity, ScrollView.Callbacks {
    public static final String ADVERT_ID = "id";
    public static final String SEARCH_TYPE = "searchType";
    public static final String IS_FROM_NEW_MESSAGE = "isFromNewMessage";
    public static final String TRY_WANTED = "tryWanted";

    private static final int LOGIN_REQUEST_CODE = 1;
    private static final int UPGRADE_REQUEST_CODE = 2;

    private static final String STATUS_NOT_AVAILABLE = "Not available";

    private ISearchFacade _sf;
    private Menu _menu;
    private int darkBlue;

    private boolean _finishedLoading = false;

    private SearchType _searchType;
    private int selectedPage;
    private String _id;

    private NoConnectionFragment _noConnectionFragment;

    private ProgressBar _progressBar;
    private View _contactBar;
    private View contactViewDivider;

    private RecyclerView advertPicturesRecyclerView;

    private AdOffered _adOffered;
    private AdWanted _adWanted;

    private boolean _isFromNewMessage = false;

    private AsyncTaskManager _asyncManager;

    private boolean _tryWanted = false;

    private View _placeHolder;

    private RelativeLayout _rlSendMessage;

    private ImageButton _ibSend;
    private EditText _etMessage;

    private TextView txtTitleOnPhone;
    private TextView txtDescriptionOnPhone;
    private TextView txtFullDescriptionOnPhone;
    private TextView _tvTitle;
    private TextView _tvDescription;

    private boolean _messageModeOn = false;

    private String _otherUserName;

    @Inject
    AdvertPictureAdapter adapter;

    @Inject
    EventsDao eventsDao;

    @Inject
    AppRating appRating;

    public static void start(Activity activity, String advertId) {
        Intent advertIntent = new Intent(activity, AdActivity.class);
        advertIntent.putExtra(ADVERT_ID, advertId);
        // Since there is no API end-point for asking for an advert without knowing the search type,
        // we try to help users requesting first an offered advert and if not successful then a wanted one regardless the search type
        advertIntent.putExtra(SEARCH_TYPE, SearchType.OFFERED);
        advertIntent.putExtra(TRY_WANTED, true);
        activity.startActivity(advertIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad);

        darkBlue = ContextCompat.getColor(this, R.color.material_deep_sea_blue);
        contactViewDivider = findViewById(R.id.contactViewDivider);

        ((RelativeLayout) findViewById(R.id.activity_ad_rlContent)).requestTransparentRegion(findViewById(R.id.activity_ad_fMap));

        _asyncManager = new AsyncTaskManager();

        Intent intent = getIntent();
        _searchType = (SearchType) intent.getSerializableExtra(SEARCH_TYPE);
        _id = intent.getStringExtra(ADVERT_ID);
        _tryWanted = intent.getBooleanExtra(TRY_WANTED, false);
        _isFromNewMessage = intent.getBooleanExtra(IS_FROM_NEW_MESSAGE, false);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("id"))
                _id = savedInstanceState.getString("id");
            if (savedInstanceState.containsKey("searchType"))
                _searchType = (SearchType) savedInstanceState.get("searchType");
        }

        setToolbar();

        ScrollView _scrollView = findViewById(R.id.activity_ad_sv);
        _scrollView.setCallbacks(this);
        _placeHolder = findViewById(R.id.placeholder);
        _progressBar = findViewById(R.id.activity_ad_pb);
        _contactBar = findViewById(R.id.activity_ad_llContactBar);

        setUpAdvertPicturesRecyclerView();

        _rlSendMessage = findViewById(R.id.activity_ad_rlSendMessage);

        _ibSend = findViewById(R.id.activity_ad_ibSend);
        _etMessage = findViewById(R.id.activity_ad_etMessage);
        txtTitleOnPhone = findViewById(R.id.txtAdActivityTile);
        txtDescriptionOnPhone = findViewById(R.id.txtAdActivityDescription);
        txtFullDescriptionOnPhone = findViewById(R.id.txtAdActivityFullDescription);
        if (isPhoneLayout())
            txtFullDescriptionOnPhone.setOnClickListener(v -> AdvertDescriptionActivity.start(this, (String) v.getTag(R.id.title_tag), (String) v.getTag(R.id.description_tag)));

        _tvTitle = findViewById(R.id.activity_ad_tvTitle);
        _tvDescription = findViewById(R.id.activity_ad_tvDesc);
        _noConnectionFragment = new NoConnectionFragment();
        _noConnectionFragment.setArguments(getIntent().getExtras());

        _sf = SpareroomApplication.getInstance(getApplicationContext()).getSearchFacade();

        adapter.setUp(this);

        loadAdvert();

    }

    private void loadAdvert() {
        if (_searchType == SearchType.OFFERED) {
            initRequestAdOfferedComplete();
        } else if (_searchType == SearchType.WANTED) {
            initRequestAdWantedComplete();
        }
    }

    private void setUpAdvertPicturesRecyclerView() {
        advertPicturesRecyclerView = findViewById(R.id.advertPictureGalleryRecyclerView);
        advertPicturesRecyclerView.setHasFixedSize(true);
        advertPicturesRecyclerView.setItemViewCacheSize(0);
        advertPicturesRecyclerView.setDrawingCacheEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false) {
            @Override
            protected int getExtraLayoutSpace(RecyclerView.State state) {
                return 0;
            }
        };
        linearLayoutManager.setInitialPrefetchItemCount(0);
        advertPicturesRecyclerView.setLayoutManager(linearLayoutManager);
        advertPicturesRecyclerView.addItemDecoration(new LinePagerIndicatorDecoration(this));
        PagerSnapHelper pagerSnapHelper = new PagerSnapHelper();
        pagerSnapHelper.attachToRecyclerView(advertPicturesRecyclerView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        advertPicturesRecyclerView.setAdapter(adapter);
        advertPicturesRecyclerView.scrollToPosition(selectedPage);
    }

    private void initRequestAdOfferedComplete() {
        _finishedLoading = false;
        _progressBar.setVisibility(View.VISIBLE);
        RequestAdOfferedCompleteTask requestAdOfferedCompleteTask =
                new RequestAdOfferedCompleteTask(
                        _sf,
                        _adOfferedCompleteListener);
        _asyncManager.register(requestAdOfferedCompleteTask);
        requestAdOfferedCompleteTask.execute(_id);
    }

    private void initRequestAdWantedComplete() {
        _finishedLoading = false;

        _progressBar.setVisibility(View.VISIBLE);
        RequestAdWantedCompleteTask requestAdWantedCompleteTask =
                new RequestAdWantedCompleteTask(
                        _sf,
                        _adWantedCompleteListener);
        _asyncManager.register(requestAdWantedCompleteTask);
        requestAdWantedCompleteTask.execute(_id);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("id", _id);
        outState.putSerializable("searchType", _searchType);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (userLoggedIn(requestCode, resultCode) || userUpgraded(requestCode))
            loadAdvert();
    }

    private boolean userLoggedIn(int requestCode, int resultCode) {
        return resultCode == RESULT_OK && requestCode == LOGIN_REQUEST_CODE;
    }

    private boolean userUpgraded(int requestCode) {
        final Session session = SpareroomApplication.getSpareRoomContext().getSession();
        return requestCode == UPGRADE_REQUEST_CODE && session != null && session.is_paid();
    }

    @Override
    protected void onPause() {
        super.onPause();
        _asyncManager.cancelAll();
    }

    @Override
    protected void onStop() {
        super.onStop();
        selectedPage = ((LinearLayoutManager) advertPicturesRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
        advertPicturesRecyclerView.setAdapter(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_ad, menu);
        menu.findItem(R.id.activity_ad_menu_save).setIcon(favouriteDefaultIcon());
        _menu = menu;
        return true;
    }

    private void markAd() {
        if (_searchType == SearchType.OFFERED)
            _adOffered.setContext(AbstractAd.CONTEXT_SHORTLIST);
        if (_searchType == SearchType.WANTED)
            _adWanted.setContext(AbstractAd.CONTEXT_SHORTLIST);

        if (_menu != null) {
            MenuItem _miFavourite = _menu.getItem(0);
            _miFavourite.setIcon(favouriteFilledIcon());
        }
    }

    private void unMarkAd() {
        if (_searchType == SearchType.OFFERED)
            _adOffered.setContext("");
        if (_searchType == SearchType.WANTED)
            _adWanted.setContext("");

        if (_menu != null) {
            MenuItem _miFavourite = _menu.getItem(0);
            _miFavourite.setIcon(favouriteDefaultIcon());
        }
    }

    private Drawable favouriteFilledIcon() {
        return UiUtils.getTintedDrawable(this, R.drawable.ic_favourites, R.color.white);
    }

    private int favouriteDefaultIcon() {
        return R.drawable.ic_favourites_outline;
    }

    private void saveAd() {
        Parameters p = new Parameters();
        if (_searchType == SearchType.OFFERED) {
            p.add("flatshare_type", "offered");
            p.add("flatshare_id", _adOffered.getId());
            if (_adOffered.isFeatured())
                p.add("featured", "1");
        } else {
            p.add("flatshare_type", "wanted");
            p.add("flatshare_id", _adWanted.getId());
        }
        p.add("function", "add");

        SaveAdAsyncTaskV2 at =
                new SaveAdAsyncTaskV2(
                        _sf,
                        _savedAdListener);
        _asyncManager.register(at);
        at.execute(p);
    }

    private void deleteAd() {
        Parameters p = new Parameters();
        if (_searchType == SearchType.OFFERED) {
            p.add("flatshare_type", "offered");
            p.add("flatshare_id", _adOffered.getId());
            if (_adOffered.isFeatured())
                p.add("featured", "1");
        } else {
            p.add("flatshare_type", "wanted");
            p.add("flatshare_id", _adWanted.getId());
        }
        p.add("function", "remove");
        RemoveSavedAdAsyncTaskV2 at =
                new RemoveSavedAdAsyncTaskV2(
                        _sf,
                        SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                        _removedAdListener);
        _asyncManager.register(at);
        at.execute(p);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        if (_finishedLoading)
            switch (item.getItemId()) {
                case R.id.activity_ad_menu_save:
                    switch (_searchType) {
                        case OFFERED:
                            if (_adOffered.hasContext()) {
                                AnalyticsTrackerComposite.getInstance()
                                        .trackEvent_Search_RemoveShortlistOffered(
                                                (SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext()).getSession() != null);
                                deleteAd();
                                _adOffered.setContext("");
                            } else {
                                AnalyticsTrackerComposite.getInstance()
                                        .trackEvent_Search_MarkFavouriteOffered(
                                                (SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext()).getSession() != null);
                                saveAd();
                                _adOffered.setContext(AbstractAd.CONTEXT_SHORTLIST);
                            }
                            break;
                        case WANTED:
                            if (_adWanted.hasContext()) {
                                AnalyticsTrackerComposite.getInstance()
                                        .trackEvent_Search_RemoveShortlistWanted(
                                                (SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext()).getSession() != null);
                                deleteAd();
                                _adWanted.setContext("");
                            } else {
                                AnalyticsTrackerComposite.getInstance()
                                        .trackEvent_Search_MarkFavouriteWanted(
                                                (SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext()).getSession() != null);
                                saveAd();
                                _adWanted.setContext(AbstractAd.CONTEXT_SHORTLIST);
                            }
                            break;

                    }
                    break;
                case R.id.activity_ad_menu_share:
                    String text = getString(R.string.activityAd_share) + AppVersion.flavor().getTwitterUsername();
                    String subject;
                    if (_adOffered != null) {
                        text += (" " + AppVersion.flavor().getWebDomain() + "/" + _adOffered.getId());
                        subject = _adOffered.getTitle();
                    } else {
                        text += (" " + AppVersion.flavor().getWebDomain() + "/" + _adWanted.getId());
                        subject = _adWanted.getTitle();
                    }

                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                    sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
                    sendIntent.setType("text/plain");
                    startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
                    break;
                case R.id.activity_ad_menu_report:
                    EmailUtils.reportAdvert(this, _id);
                default:
                    break;
            }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void retry() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.remove(_noConnectionFragment);
        transaction.commit();

        _progressBar.setVisibility(View.VISIBLE);

        if (_searchType == SearchType.OFFERED) {
            initRequestAdOfferedComplete();
        } else if (_searchType == SearchType.WANTED) {
            initRequestAdWantedComplete();
        }
    }

    @Override
    public void onBackPressed() {
        if (isMessageModeOn())
            finishMessageMode();
        else
            super.onBackPressed();
    }

    private class LoggedOutActionOnClickListener implements OnClickListener {
        private final String _action;
        private final String _otherUserName;
        private final String _pageJoinedFrom;

        LoggedOutActionOnClickListener(String action, String otherUserName, String pageJoinedFrom) {
            _action = action;
            _otherUserName = otherUserName;
            _pageJoinedFrom = pageJoinedFrom;
        }

        @Override
        public void onClick(View v) {
            Intent activityLogin = new Intent();
            activityLogin.setClass(AdActivity.this, NewExistingActivity.class);
            activityLogin.putExtra(NewExistingFragment.EXTRA_ACTION_CODE, _action);
            activityLogin.putExtra(NewExistingFragment.EXTRA_OTHER_USER_NAME, _otherUserName);
            activityLogin.putExtra(NewExistingActivity.PAGE_JOINED_FROM, _pageJoinedFrom);
            startActivityForResult(activityLogin, LOGIN_REQUEST_CODE);
        }
    }

    private class PhoneOnClickListener implements OnClickListener, IAsyncResult {
        private final String _phone;
        private int _threadsRun = 0;

        PhoneOnClickListener(String phone) {
            _phone = phone;
        }

        @Override
        public void onClick(View v) {
            _asyncManager.cancelAll(); // is also cancelling any other task running on the manager of this activity
            _threadsRun = 0;

            if (_searchType == SearchType.OFFERED)
                AnalyticsTrackerComposite.getInstance().trackEvent_AdvertOffered_PhoneAdvertiser();
            if (_searchType == SearchType.WANTED)
                AnalyticsTrackerComposite.getInstance().trackEvent_AdvertWanted_PhoneAdvertiser();

            markAsCalled();
            clockCall();
        }

        private void markAsCalled() {
            Parameters p = new Parameters();

            p.add("format", "json");

            if (_searchType == SearchType.OFFERED) {
                p.add("ad_type", "offered");
                p.add("advert_id", _adOffered.getId());
                p.add("function", "add");
                p.add("context", "called");

                if (_adOffered.isFeatured())
                    p.add("featured", "1");
            } else {
                p.add("ad_type", "wanted");
                p.add("advert_id", _adWanted.getId());
                p.add("function", "add");
                p.add("context", "called");
            }

            p.add("output", "silent");

            MarkAdAsyncTask at =
                    new MarkAdAsyncTask(_sf, SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(), this);
            _asyncManager.register(at);
            at.execute(p);
        }

        private void clockCall() {

            Parameters p = new Parameters();

            p.add("format", "json");

            if (_searchType == SearchType.OFFERED) {
                p.add("advert_id", _adOffered.getId());
                p.add("ad_type", "offered");
                p.add("mode", "contact");
                p.add("submode", "bytel");

                if (_adOffered.isFeatured())
                    p.add("featured", "1");

            } else {
                p.add("advert_id", _adWanted.getId());
                p.add("ad_type", "wanted");
                p.add("mode", "contact");
                p.add("submode", "bytel");

            }

            ClockCallAsyncTask at =
                    new ClockCallAsyncTask(_sf, SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(), this);
            _asyncManager.register(at);
            at.execute(p);
        }

        private void call() {
            View v = LayoutInflater.from(AdActivity.this).inflate(R.layout.dialog_title_message_buttons, null);
            TextView tvTitle = v.findViewById(R.id.dialog_title);
            TextView tvMessage = v.findViewById(R.id.dialog_message);
            Button bPositive = v.findViewById(R.id.dialog_bPositive);
            Button bNegative = v.findViewById(R.id.dialog_bNegative);

            tvTitle.setText(getString(R.string.dialog_telephone));
            tvMessage.setTextSize(20);
            tvMessage.setTextColor(ContextCompat.getColor(AdActivity.this, R.color.material_deep_sea_blue));
            tvMessage.setText(_phone);
            bNegative.setText(getString(R.string.dialog_close));

            Dialog d = new AlertDialogBuilder(AdActivity.this, v).create();

            bNegative.setOnClickListener(new DialogUtil.CancelOnClickListener(d));

            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            List<ResolveInfo> queryIntentActivities = AdActivity.this.getPackageManager().queryIntentActivities(callIntent, 0);

            if (queryIntentActivities.size() > 0) {
                bPositive.setText(getString(R.string.adActivity_call));
                bPositive.setOnClickListener(view -> {
                    callIntent.setData(Uri.parse("tel:" + _phone));
                    startActivity(callIntent);
                    DialogUtil.dismissSafely(d);
                });

            } else {
                bPositive.setText(getString(R.string.adActivity_copyToClipboard));
                bPositive.setOnClickListener(new CopyToClipboardOnClickListener(d));
            }

            d.show();
        }

        private class CopyToClipboardOnClickListener implements OnClickListener {

            private final Dialog dialog;

            private CopyToClipboardOnClickListener(Dialog dialog) {
                this.dialog = dialog;
            }

            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Advertiser's phone number", _phone);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(AdActivity.this, _phone + " copied to clipboard", Toast.LENGTH_LONG).show();
                DialogUtil.dismissSafely(dialog);
            }
        }

        @Override
        public void update(Object o) {
            _threadsRun++;
            if (_threadsRun == 2) {
                _threadsRun = 0;
                call();
                markAd();
            }

        }

        @Override
        public void handleInvalidUserException(String message) {

        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();

        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();

        }

        @Override
        public void handleServiceUnavailableException(String message) {
            Toast.makeText(AdActivity.this, R.string.activityAd_call_tNoConnection, Toast.LENGTH_LONG).show();

        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();

        }

    }

    private class MessageOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            if (isMessageModeOn()) {
                finishMessageMode();
            } else {
                if (_isFromNewMessage) {
                    Intent intent = new Intent();
                    intent.putExtra(ConversationActivity.NAVIGATE_TO_CONVERSATION, true);
                    setResult(RESULT_OK, intent);
                    finish();
                    return;
                }
                // Could be the first thing to be done, but it seems tricky to clock this event if the user is already messaging the advertiser and he is just checking the original ad
                if (_searchType == SearchType.OFFERED)
                    AnalyticsTrackerComposite.getInstance().trackEvent_AdvertOffered_MessageAdvertiser();
                else if (_searchType == SearchType.WANTED)
                    AnalyticsTrackerComposite.getInstance().trackEvent_AdvertWanted_MessageAdvertiser();

                startMessageMode();
            }
        }

    }

    private class UpgradeOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            Intent upgradeActivityFragment = new Intent();
            upgradeActivityFragment.putExtra("context_id", (_searchType == SearchType.OFFERED) ? "99" : "105");
            upgradeActivityFragment.setClass(AdActivity.this, UpgradeFragmentActivity.class);
            startActivityForResult(upgradeActivityFragment, UPGRADE_REQUEST_CODE);
        }

    }

    @Override
    public void onScrollChanged(int scrollY) {
        if (_placeHolder != null) {
            float y = _placeHolder.getY();// ViewHelper.getY(_placeHolder);
            if (scrollY >= y) {
                _contactBar.setTranslationY(-y + scrollY);// ViewHelper.setTranslationY(_contactBar,  - y + scrollY);
            } else {
                _contactBar.setTranslationY(0);// ViewHelper.setTranslationY(_contactBar,  0);
            }
        }
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent() {
    }

    private final IAsyncResult _adOfferedCompleteListener = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            if ((o == null) && _tryWanted) {
                _searchType = SearchType.WANTED;
                initRequestAdWantedComplete();
            } else if (o == null) {
                Toast.makeText(AdActivity.this, R.string.ad_activity_tAdNotFound, Toast.LENGTH_LONG).show();
                finish();
                return;
            } else {
                _progressBar.setVisibility(View.GONE);
                _contactBar.setVisibility(View.VISIBLE);

                AdOffered ad = (AdOffered) o;
                _adOffered = ad;

                View separator = findViewById(R.id.activity_ad_separator);
                if (separator != null)
                    separator.setVisibility(View.VISIBLE);

                if (_adOffered.hasContext()) {
                    if (_menu != null) {
                        MenuItem _miFavourite = _menu.getItem(0);
                        _miFavourite.setIcon(favouriteFilledIcon());
                    }
                    markAd();
                } else {
                    if (_menu != null) {
                        MenuItem _miFavourite = _menu.getItem(0);
                        _miFavourite.setIcon(favouriteDefaultIcon());
                    }
                    unMarkAd();
                }

                _ibSend.setOnClickListener(new SendMessageOnClickListener(ad));

                final RelativeLayout rl = findViewById(R.id.activity_ad_rl);
                final View rlContactBar = findViewById(R.id.activity_ad_llContactBar);
                final TextView tvNewToday = findViewById(R.id.activity_ad_tvNew);
                final TextView tvName = findViewById(R.id.activity_ad_tvName);
                final TextView tvRoom = findViewById(R.id.activity_ad_tvRoom);
                final TextView tvNotAvailable = findViewById(R.id.activity_ad_tvNotAvailable);
                final Button ibPhone = findViewById(R.id.activity_ad_ibPhone);
                final Button ibMessage = findViewById(R.id.activity_ad_ibMessage);
                final ImageView ivFullscreenMap = findViewById(R.id.activity_ad_ivMap_fullscreen);
                final TextView tvId = findViewById(R.id.activity_ad_tvId);

                tvId.setText(String.format(AppVersion.flavor().getLocale(), "%s%s", getString(R.string.activity_ad_tvId), ad.getId()));
                if (ad.isNewToday()) {
                    tvNewToday.setVisibility(View.VISIBLE);
                }

                boolean isBoldOrFeatured = false;
                if (ad.isBoldOrFeatured()) {
                    isBoldOrFeatured = true;
                    rl.setBackgroundResource(R.color.light_blue_3);
                    rlContactBar.setBackgroundResource(R.color.light_blue_3);
                    contactViewDivider.setBackgroundResource(R.color.light_blue_1);
                    tvId.setTextColor(ContextCompat.getColor(AdActivity.this, R.color.material_deep_sea_blue));

                    if (_tvTitle != null)
                        _tvTitle.setTextColor(ContextCompat.getColor(AdActivity.this, R.color.material_deep_sea_blue));

                    if (tvName != null)
                        tvName.setTextColor(ContextCompat.getColor(AdActivity.this, R.color.material_deep_sea_blue));
                    if (tvRoom != null)
                        tvRoom.setTextColor(ContextCompat.getColor(AdActivity.this, R.color.material_deep_sea_blue));

                    if (separator != null)
                        separator.setBackgroundResource(R.color.light_blue_1);
                }

                if (ad.getPhotos().size() == 0 && StringUtils.isNullOrEmpty(ad.getYoutubeId())) {
                    advertPicturesRecyclerView.setVisibility(View.GONE);
                    View sGallery = findViewById(R.id.activity_ad_sGallery);
                    if (sGallery != null)
                        sGallery.setVisibility(View.VISIBLE);
                }

                if (isPhoneLayout())
                    setUpPhoneLayoutViews(isBoldOrFeatured, ad.getTitle(), ad.getFullDescription());

                if (_tvTitle != null)
                    _tvTitle.setText(ad.getTitle());
                if (_tvDescription != null)
                    _tvDescription.setText(ad.getFullDescription());

                _otherUserName = ad.getAdvertiserName();
                if (tvName != null)
                    tvName.setText(_otherUserName);
                if (tvRoom != null)
                    tvRoom.setText(ad.getAccommodationType());

                setTitle(ad.getArea() + " (" + ad.getPostcode() + ")");

                setSubtitle(ad.getPrice());

                adapter.update(ad.getYoutubeId(), ad.getPhotos());

                if (STATUS_NOT_AVAILABLE.equals(ad.getStatus())) {
                    tvNotAvailable.setVisibility(View.VISIBLE);
                } else {
                    Session s = (SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext()).getSession();

                    ibMessage.setVisibility(View.VISIBLE);
                    if (ad.getContactableByPhone()) {
                        ibPhone.setVisibility(View.VISIBLE);
                    } else {
                        ibPhone.setVisibility(View.GONE);
                    }

                    if (s == null) {
                        LoggedOutActionOnClickListener phoneListener =
                                new LoggedOutActionOnClickListener(
                                        NewExistingFragment.EXTRA_ACTION_CODE_CALL_ADVERTISER,
                                        _otherUserName,
                                        "offered advert detail");
                        LoggedOutActionOnClickListener messageListener =
                                new LoggedOutActionOnClickListener(
                                        NewExistingFragment.EXTRA_ACTION_CODE_MESSAGE_ADVERTISER,
                                        _otherUserName,
                                        "offered advert detail");

                        ibPhone.setOnClickListener(phoneListener);
                        ibMessage.setOnClickListener(messageListener);

                    } else if (!ad.getFreeToContact()) {
                        UpgradeOnClickListener listener = new UpgradeOnClickListener();

                        ibPhone.setOnClickListener(listener);
                        ibMessage.setOnClickListener(listener);
                    } else {
                        ibPhone.setOnClickListener(new PhoneOnClickListener(ad.getAdvertiserPhoneNumber()));
                        ibMessage.setOnClickListener(new MessageOnClickListener());
                    }
                }
                if ((_adOffered.getLatitude() != 0) && (_adOffered.getLongitude() != 0)) {
                    int googlePlayServicesStatus = GooglePlayServicesUtil.isGooglePlayServicesAvailable(AdActivity.this);
                    if (googlePlayServicesStatus == ConnectionResult.SUCCESS) {
                        AdOfferedMapFragment _mapFragment = new AdOfferedMapFragment();
                        Bundle arguments = new Bundle();
                        arguments.putFloat("latitude", _adOffered.getLatitude());
                        arguments.putFloat("longitude", _adOffered.getLongitude());
                        arguments.putString("neighbourhood", _adOffered.getArea());
                        arguments.putString("postcode", _adOffered.getPostcode());
                        arguments.putString("accomType", _adOffered.getAccommodationType());
                        arguments.putString("price", _adOffered.getPrice());
                        arguments.putString("title", _adOffered.getTitle());
                        _mapFragment.setArguments(arguments);

                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.add(R.id.activity_ad_fMap, _mapFragment);
                        fragmentTransaction.commit();

                        ivFullscreenMap.setVisibility(View.VISIBLE);
                    } else {
                        FrameLayout fl = findViewById(R.id.activity_ad_fMap);
                        TextView tvNoMap = findViewById(R.id.activity_ad_tvNoMap);
                        fl.setVisibility(View.GONE);
                        tvNoMap.setVisibility(View.VISIBLE);
                        GooglePlayServicesUtil.getErrorDialog(googlePlayServicesStatus, AdActivity.this, 0).show();
                    }
                } else {
                    FrameLayout fl = findViewById(R.id.activity_ad_fMap);
                    fl.setVisibility(View.GONE);
                }
                _adOfferedDetailsListener.update(ad.getSheet());
            }
            _finishedLoading = true;
        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();

        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();

        }

        @Override
        public void handleServiceUnavailableException(String message) {
            _progressBar.setVisibility(View.GONE);
            findViewById(R.id.activity_ad_flNoConnection).setVisibility(View.VISIBLE);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.activity_ad_flNoConnection, _noConnectionFragment);
            transaction.commit();
        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();

        }

    };

    private final IAsyncResult _adWantedCompleteListener = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {

            if (o == null) {
                Toast.makeText(AdActivity.this, R.string.ad_activity_tAdNotFound, Toast.LENGTH_LONG).show();
                finish();
                return;
            }

            _progressBar.setVisibility(View.GONE);
            _contactBar.setVisibility(View.VISIBLE);

            View separator = findViewById(R.id.activity_ad_separator);
            if (separator != null)
                separator.setVisibility(View.VISIBLE);

            AdWanted ad = (AdWanted) o;
            _adWanted = ad;

            if (_adWanted.hasContext()) {
                if (_menu != null) {
                    MenuItem _miFavourite = _menu.getItem(0);
                    _miFavourite.setIcon(favouriteFilledIcon());
                }
            }

            _ibSend.setOnClickListener(new SendMessageOnClickListener(ad));

            final RelativeLayout rl = findViewById(R.id.activity_ad_rl);
            final View rlContactBar = findViewById(R.id.activity_ad_llContactBar);
            final TextView tvNewToday = findViewById(R.id.activity_ad_tvNew);
            final TextView tvDescription = findViewById(R.id.activity_ad_tvDesc);
            final TextView tvName = findViewById(R.id.activity_ad_tvName);
            final TextView tvRoom = findViewById(R.id.activity_ad_tvRoom);
            final TextView tvNotAvailable = findViewById(R.id.activity_ad_tvNotAvailable);
            final Button ibPhone = findViewById(R.id.activity_ad_ibPhone);
            final Button ibMessage = findViewById(R.id.activity_ad_ibMessage);
            final FrameLayout flMap = findViewById(R.id.activity_ad_fMap);
            final TextView tvId = findViewById(R.id.activity_ad_tvId);

            tvId.setText(String.format(AppVersion.flavor().getLocale(), "%s%s", getString(R.string.activity_ad_tvId), ad.getId()));

            if (ad.isNewToday()) {
                tvNewToday.setVisibility(View.VISIBLE);
            }

            if (ad.isBoldOrFeatured()) {
                rl.setBackgroundResource(R.color.light_blue_3);
                rlContactBar.setBackgroundResource(R.color.light_blue_3);
                contactViewDivider.setBackgroundResource(R.color.light_blue_1);
                if (separator != null)
                    separator.setBackgroundResource(R.color.light_blue_1);
            }

            if (ad.getPhotos().size() == 0) {
                advertPicturesRecyclerView.setVisibility(View.GONE);
                View sGallery = findViewById(R.id.activity_ad_sGallery);
                if (sGallery != null)
                    sGallery.setVisibility(View.VISIBLE);
            }

            boolean isBoldOrFeatured = false;
            if (ad.isBoldOrFeatured()) {
                isBoldOrFeatured = true;
                tvId.setTextColor(ContextCompat.getColor(AdActivity.this, R.color.material_deep_sea_blue));

                if (_tvTitle != null)
                    _tvTitle.setTextColor(ContextCompat.getColor(AdActivity.this, R.color.material_deep_sea_blue));

                if (tvName != null)
                    tvName.setTextColor(ContextCompat.getColor(AdActivity.this, R.color.material_deep_sea_blue));
                if (tvRoom != null)
                    tvRoom.setTextColor(ContextCompat.getColor(AdActivity.this, R.color.material_deep_sea_blue));
            }

            if (isPhoneLayout())
                setUpPhoneLayoutViews(isBoldOrFeatured, ad.getTitle(), ad.getFullDescription());

            if (_tvTitle != null)
                _tvTitle.setText(ad.getTitle());
            if (_tvDescription != null)
                _tvDescription.setText(ad.getFullDescription());

            flMap.setVisibility(View.GONE);

            if (ad.getPhotos().size() == 0) {
                advertPicturesRecyclerView.setVisibility(View.GONE);
            }
            if (tvDescription != null)
                tvDescription.setText(ad.getFullDescription());

            _otherUserName = ad.getAdvertiserName();
            if (tvName != null)
                tvName.setText(_otherUserName);
            if (tvRoom != null)
                tvRoom.setVisibility(View.GONE);
            View spaceBefore = findViewById(R.id.activity_ad_before_tvName);
            View spaceAfter = findViewById(R.id.activity_ad_after_tvName);

            if (spaceBefore != null)
                spaceBefore.setVisibility(View.VISIBLE);
            if (spaceBefore != null)
                spaceAfter.setVisibility(View.VISIBLE);

            setTitle(getString(R.string.ad_activity_actionbar_title) + ad.getId());

            adapter.update(ad.getYoutubeId(), ad.getPhotos());
            if (STATUS_NOT_AVAILABLE.equals(ad.getStatus())) {
                tvNotAvailable.setVisibility(View.VISIBLE);
            } else {
                Session s = (SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext()).getSession();

                ibMessage.setVisibility(View.VISIBLE);
                if (ad.getContactableByPhone()) {
                    ibPhone.setVisibility(View.VISIBLE);
                } else {
                    ibPhone.setVisibility(View.GONE);
                }

                if (s == null) {
                    LoggedOutActionOnClickListener phoneListener =
                            new LoggedOutActionOnClickListener(
                                    NewExistingFragment.EXTRA_ACTION_CODE_CALL_ADVERTISER,
                                    _otherUserName,
                                    "wanted advert detail");
                    LoggedOutActionOnClickListener messageListener =
                            new LoggedOutActionOnClickListener(
                                    NewExistingFragment.EXTRA_ACTION_CODE_MESSAGE_ADVERTISER,
                                    _otherUserName,
                                    "wanted advert detail");

                    ibPhone.setOnClickListener(phoneListener);
                    ibMessage.setOnClickListener(messageListener);

                } else if (!ad.getFreeToContact()) {
                    UpgradeOnClickListener listener = new UpgradeOnClickListener();

                    ibPhone.setOnClickListener(listener);
                    ibMessage.setOnClickListener(listener);
                } else {
                    ibPhone.setOnClickListener(new PhoneOnClickListener(ad.getAdvertiserPhoneNumber()));
                    ibMessage.setOnClickListener(new MessageOnClickListener());
                }
            }
            _adWantedDetailsListener.update(ad.getSheet());
            _finishedLoading = true;
        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();

        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();

        }

        @Override
        public void handleServiceUnavailableException(String message) {
            _progressBar.setVisibility(View.GONE);
            findViewById(R.id.activity_ad_flNoConnection).setVisibility(View.VISIBLE);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.activity_ad_flNoConnection, _noConnectionFragment);
            transaction.commit();
        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();

        }

    };

    private final IAsyncResult _adOfferedDetailsListener = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            if (o == null) {
                return;
            }

            _progressBar.setVisibility(View.GONE);
            _contactBar.setVisibility(View.VISIBLE);

            AdDetailsSheet result = (AdDetailsSheet) o;

            LinearLayout gl = AdActivity.this.findViewById(R.id.activity_ad_glSheet);
            Vector<Pair<String, Object>> entries = result.get_entries();

            gl.removeAllViews();

            for (int i = 0; i < entries.size(); i++) {
                Pair<String, Object> entry = entries.get(i);

                if (!(entry.second instanceof String)) {

                    TextView tvTitle = (TextView) AdActivity.this.getLayoutInflater().inflate(R.layout.activity_detail_sheet_title, null);
                    tvTitle.setText(entry.first);

                    if (_adOffered != null)
                        if (_adOffered.isBoldOrFeatured()) {
                            tvTitle.setBackgroundResource(R.color.light_blue_2);
                            tvTitle.setTextColor(ContextCompat.getColor(AdActivity.this, R.color.material_deep_sea_blue));
                        }

                    Space space = new Space(AdActivity.this);
                    space.setLayoutParams(
                            new LayoutParams(
                                    LayoutParams.MATCH_PARENT,
                                    Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, AdActivity.this.getResources().getDisplayMetrics()))));

                    gl.addView(tvTitle);
                    gl.addView(space);
                }

                if (entry.second instanceof Vector<?>) {
                    Vector<Pair<String, String>> vDetails = (Vector<Pair<String, String>>) entry.second;
                    for (Pair<String, String> pair : vDetails) {

                        LinearLayout llPair = new LinearLayout(AdActivity.this);
                        llPair.setOrientation(LinearLayout.HORIZONTAL);
                        LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                        llPair.setLayoutParams(lParams);

                        TextView tvKey = (TextView) AdActivity.this.getLayoutInflater().inflate(R.layout.activity_detail_sheet_pair_key_entry, null);
                        TextView tvValue = (TextView) AdActivity.this.getLayoutInflater().inflate(R.layout.activity_detail_sheet_pair_value_entry, null);

                        if (_adOffered.isBoldOrFeatured()) {
                            tvKey.setTextColor(ContextCompat.getColor(AdActivity.this, R.color.material_deep_sea_blue));
                            tvValue.setTextColor(ContextCompat.getColor(AdActivity.this, R.color.material_deep_sea_blue));
                        }

                        tvKey.setText(getKey(pair.first));
                        tvValue.setText(pair.second);

                        llPair.addView(tvKey);
                        llPair.addView(tvValue);

                        gl.addView(llPair);
                    }
                    Space space = new Space(AdActivity.this);
                    space.setLayoutParams(
                            new LayoutParams(
                                    LayoutParams.MATCH_PARENT,
                                    Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, AdActivity.this.getResources().getDisplayMetrics()))));

                    gl.addView(space);
                }

            }

        }

        private String getKey(String key) {
            return AbstractAd.DSS_OK_QUESTION_MARK.equalsIgnoreCase(key) ? getString(R.string.housing_benefit_recipients_ok) : key;
        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();

        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();

        }

        @Override
        public void handleServiceUnavailableException(String message) {
            _progressBar.setVisibility(View.GONE);
            findViewById(R.id.activity_ad_flNoConnection).setVisibility(View.VISIBLE);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.activity_ad_flNoConnection, _noConnectionFragment);
            transaction.commit();
        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();

        }

    };

    private final IAsyncResult _adWantedDetailsListener = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {

            if (o == null) {
                return;
            }

            _progressBar.setVisibility(View.GONE);
            _contactBar.setVisibility(View.VISIBLE);

            AdDetailsSheet result = (AdDetailsSheet) o;

            LinearLayout gl = AdActivity.this.findViewById(R.id.activity_ad_glSheet);
            Vector<Pair<String, Object>> entries = result.get_entries();

            gl.removeAllViews();

            for (int i = 0; i < entries.size(); i++) {
                Pair<String, Object> entry = entries.get(i);

                if (!(entry.second instanceof String)) {
                    TextView tvTitle = (TextView) AdActivity.this.getLayoutInflater().inflate(R.layout.activity_detail_sheet_title, null);
                    tvTitle.setText(entry.first);

                    if (_adWanted != null)
                        if (_adWanted.isBoldOrFeatured()) {
                            tvTitle.setBackgroundResource(R.color.light_blue_2);
                            tvTitle.setTextColor(ContextCompat.getColor(AdActivity.this, R.color.material_deep_sea_blue));
                        }

                    Space space = new Space(AdActivity.this);
                    space.setLayoutParams(
                            new LayoutParams(
                                    LayoutParams.MATCH_PARENT,
                                    Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, AdActivity.this.getResources().getDisplayMetrics()))));

                    gl.addView(tvTitle);
                    gl.addView(space);
                }

                if (entry.second instanceof Vector<?>) {
                    Vector<Pair<String, String>> vDetails = (Vector<Pair<String, String>>) entry.second;
                    for (Pair<String, String> pair : vDetails) {

                        LinearLayout llPair = new LinearLayout(AdActivity.this);
                        llPair.setOrientation(LinearLayout.HORIZONTAL);
                        LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                        llPair.setLayoutParams(lParams);

                        TextView tvKey = (TextView) AdActivity.this.getLayoutInflater().inflate(R.layout.activity_detail_sheet_pair_key_entry, null);
                        TextView tvValue = (TextView) AdActivity.this.getLayoutInflater().inflate(R.layout.activity_detail_sheet_pair_value_entry, null);

                        if (_adWanted.isBoldOrFeatured()) {
                            tvKey.setTextColor(ContextCompat.getColor(AdActivity.this, R.color.material_deep_sea_blue));
                            tvValue.setTextColor(ContextCompat.getColor(AdActivity.this, R.color.material_deep_sea_blue));
                        }

                        tvKey.setText(pair.first);
                        tvValue.setText(pair.second);

                        llPair.addView(tvKey);
                        llPair.addView(tvValue);

                        gl.addView(llPair);
                    }
                    Space space = new Space(AdActivity.this);
                    space.setLayoutParams(
                            new LayoutParams(
                                    LayoutParams.MATCH_PARENT,
                                    Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, AdActivity.this.getResources().getDisplayMetrics()))));

                    gl.addView(space);
                }
            }

        }

        @Override
        public void handleServiceUnavailableException(String message) {
            _progressBar.setVisibility(View.GONE);
            findViewById(R.id.activity_ad_flNoConnection).setVisibility(View.VISIBLE);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.activity_ad_flNoConnection, _noConnectionFragment);
            transaction.commit();
        }

    };

    private final IAsyncResult _savedAdListener = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            if (o instanceof SpareroomStatus) {
                if (_menu != null) {
                    MenuItem _miFavourite = _menu.getItem(0);
                    _miFavourite.setIcon(favouriteFilledIcon());
                }
                Toast.makeText(AdActivity.this, "Ad saved", Toast.LENGTH_LONG).show();
                setResult(AbstractAd.SAVED);
                eventsDao.logSaveAdvertEvent();
                showRatingPopupIfPossible();
            }
        }

        @Override
        public void handleInvalidUserException(String message) {
            Intent activityLogin = new Intent();
            activityLogin.setClass(AdActivity.this, NewExistingActivity.class);
            activityLogin.putExtra(NewExistingFragment.EXTRA_ACTION_CODE, NewExistingFragment.EXTRA_ACTION_CODE_SAVE_AD);
            startActivityForResult(activityLogin, LOGIN_REQUEST_CODE);
        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();

        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();

        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();

        }

    };

    private final IAsyncResult _removedAdListener = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            if (o instanceof SpareroomStatus) {
                if (_menu != null) {
                    MenuItem _miFavourite = _menu.getItem(0);
                    _miFavourite.setIcon(favouriteDefaultIcon());
                }
                Toast.makeText(AdActivity.this, "Removed from saved ads", Toast.LENGTH_LONG).show();
                setResult(AbstractAd.NOT_SAVED);
            }
        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();
        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();
        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(AdActivity.this).show();
        }

    };

    private class SendMessageOnClickListener implements OnClickListener, IAsyncResult {
        private final AbstractAd _ad;

        SendMessageOnClickListener(AbstractAd ad) {
            _ad = ad;
        }

        @Override
        public void onClick(View v) {
            _etMessage.setEnabled(false);
            _ibSend.setEnabled(false);

            Parameters p = new Parameters();
            p.add(MessageParamEnum.AD_ID, _ad.getId());
            p.add(MessageParamEnum.AD_TYPE, (_searchType == SearchType.WANTED) ? "wanted" : "offered");
            p.add(MessageParamEnum.MODE, "contact");
            p.add(MessageParamEnum.SUBMODE, "byemail");
            p.add(MessageParamEnum.MESSAGE, _etMessage.getText().toString());
            p.add(MessageParamEnum.FORMAT, "json");
            NewMessageThreadAsyncTask at = new NewMessageThreadAsyncTask(
                    SpareroomApplication.getInstance(getApplicationContext()).getMessageFacade(),
                    SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                    this);
            _asyncManager.register(at);
            at.execute(p);
        }

        @Override
        public void update(Object o) {
            _etMessage.setEnabled(true);
            _etMessage.setText("");
            _ibSend.setEnabled(true);

            finishMessageMode();
            Toast.makeText(AdActivity.this, "Message sent", Toast.LENGTH_LONG).show();

            Intent intent = new Intent();
            intent.putExtra("id", _id);

            setResult(AbstractAd.CONTACTABLE_CONTACTED, intent);
            eventsDao.logMessageAdvertEvent();
            showRatingPopupIfPossible();
        }

        @Override
        public void handleInvalidUserException(String message) {
            _etMessage.setEnabled(true);
            _ibSend.setEnabled(true);

            DialogUtil.updateDialog(AdActivity.this);
        }

        @Override
        public void handleMissingParameterException(String message) {
            _etMessage.setEnabled(true);
            _ibSend.setEnabled(true);

            DialogUtil.updateDialog(AdActivity.this);
        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            _etMessage.setEnabled(true);
            _ibSend.setEnabled(true);

            DialogUtil.updateDialog(AdActivity.this);
        }

        @Override
        public void handleServiceUnavailableException(String message) {
            _etMessage.setEnabled(true);
            _ibSend.setEnabled(true);

            Toast.makeText(AdActivity.this, "Message could not be sent", Toast.LENGTH_LONG).show();
        }

        @Override
        public void handleInconsistentStateException(String message) {
            _etMessage.setEnabled(true);
            _ibSend.setEnabled(true);

            DialogUtil.updateDialog(AdActivity.this);
        }

    }

    private void startMessageMode() {
        _messageModeOn = true;
        _rlSendMessage.setVisibility(View.VISIBLE);

        _contactBar.setVisibility(View.GONE);

        _etMessage.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(_etMessage, InputMethodManager.SHOW_IMPLICIT);
    }

    private void finishMessageMode() {
        _messageModeOn = false;
        _rlSendMessage.setVisibility(View.GONE);
        _contactBar.setVisibility(View.VISIBLE);

        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(_etMessage.getWindowToken(), 0);

    }

    private boolean isMessageModeOn() {
        return _messageModeOn;
    }

    private void showRatingPopupIfPossible() {
        appRating.showRatingPopupIfPossible(this);
    }

    private boolean isPhoneLayout() {
        return txtTitleOnPhone != null && txtDescriptionOnPhone != null && txtFullDescriptionOnPhone != null;
    }

    private void setUpPhoneLayoutViews(boolean isBoldOrFeatured, String title, String description) {
        title = title != null ? title.trim() : "";
        description = description != null ? description.trim() : "";

        txtTitleOnPhone.setText(title);
        txtTitleOnPhone.setVisibility(View.VISIBLE);

        txtDescriptionOnPhone.setText(description);
        txtDescriptionOnPhone.setVisibility(View.VISIBLE);

        txtFullDescriptionOnPhone.setTag(R.id.title_tag, title);
        txtFullDescriptionOnPhone.setTag(R.id.description_tag, description);
        txtFullDescriptionOnPhone.setVisibility(View.VISIBLE);

        if (isBoldOrFeatured) {
            txtTitleOnPhone.setTextColor(darkBlue);
            txtDescriptionOnPhone.setTextColor(darkBlue);
            txtFullDescriptionOnPhone.setTextColor(darkBlue);
        }
    }

}
