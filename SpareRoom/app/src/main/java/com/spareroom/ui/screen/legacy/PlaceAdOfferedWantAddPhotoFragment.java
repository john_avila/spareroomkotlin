package com.spareroom.ui.screen.legacy;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.controller.ControllerActionObserver;
import com.spareroom.controller.GetUriTypeControllerAction;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.ui.util.*;

import java.io.File;

import androidx.annotation.NonNull;

public class PlaceAdOfferedWantAddPhotoFragment extends PlaceAdFragment {
    private static final int GALLERY_REQUEST = 50;
    private static final int CAMERA_REQUEST = 51;
    private static final int PHOTO_CAMERA_PERMISSIONS_REQUEST = 60;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_offered_add_photo, container, false);

        setTitle(getString(R.string.place_ad_offered_want_add_photo));

        Button _bNo = rootView.findViewById(R.id.place_ad_offered_add_photo_bNo);
        Button _bChoose = rootView.findViewById(R.id.place_ad_offered_add_photo_bChoose);
        Button _bTake = rootView.findViewById(R.id.place_ad_offered_add_photo_bTake);

        _bNo.setOnClickListener(v -> {
            getDraftAd().setPicturePath(null);
            finish();
        });

        _bChoose.setOnClickListener(new GalleryOnClickListener());
        _bTake.setOnClickListener(new CameraOnClickListener());

        return rootView;
    }

    private void checkCameraPermissions() {
        if (PermissionUtils.hasCameraPermissions(getActivity())) {
            launchCamera();
        } else {
            PermissionUtils.requestCameraPermissions(this, PHOTO_CAMERA_PERMISSIONS_REQUEST);
        }

    }

    private void launchCamera() {
        File mediaFile = FileUtils.createNewFileForPhoto();

        getDraftAd().setPicturePath(Uri.fromFile(mediaFile).toString());

        Launcher.launchCamera(this, CAMERA_REQUEST, mediaFile);
    }

    private void launchGallery() {
        Launcher.launchGallery(this, GALLERY_REQUEST);
    }

    private final class CameraOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            checkCameraPermissions();
        }
    }

    private final class GalleryOnClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            launchGallery();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST || requestCode == CAMERA_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                String pictureUri = requestCode == GALLERY_REQUEST ? data.getData().toString() : getDraftAd().getPicturePath();
                getDraftAd().setPicturePath(pictureUri);

                if (requestCode == CAMERA_REQUEST) {
                    finish();
                } else {
                    checkUriType(pictureUri);
                }
            }
        }
    }

    private void checkUriType(String pictureUri) {
        GetUriTypeControllerAction getUriTypeControllerAction = GetUriTypeControllerAction.get(new UploadPictureObserver());
        addObserver(getUriTypeControllerAction);
        getUriTypeControllerAction.execute(pictureUri);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PHOTO_CAMERA_PERMISSIONS_REQUEST) {
            if ((grantResults.length > 0)
                    && (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    && (grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                launchCamera();
            } else {
                Toast.makeText(getActivity(), getString(R.string.permission_denied), Toast.LENGTH_LONG).show();
            }
        }
    }

    private final class UploadPictureObserver implements ControllerActionObserver {

        @Override
        public void onResult(Object o) {
            finish();
        }

        @Override
        public void onSpareroomStatus(SpareroomStatus status) {
            ToastUtils.showToast(status.getMessage());
        }

        @Override
        public void onException(Exception exception) {
            ToastUtils.showToast(R.string.errorGeneric_title);
        }
    }
}
