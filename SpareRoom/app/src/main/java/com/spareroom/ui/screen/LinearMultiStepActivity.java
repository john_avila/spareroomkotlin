package com.spareroom.ui.screen;

import android.os.Bundle;
import android.view.View;

import com.spareroom.R;
import com.spareroom.ui.anim.ProgressbarAnim;
import com.spareroom.ui.flow.FlowManager;
import com.spareroom.ui.screen.customtabs.CustomTabActivityHelper;
import com.spareroom.ui.util.KeyboardUtils;

import java.util.TreeMap;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

/**
 * Activity that provides extra methods in order to control fragment flow of a multi step process
 */
public class LinearMultiStepActivity extends Activity {

    private CustomTabActivityHelper customTabActivityHelper;

    private View _vProgressbar;
    private View progressBarFull;

    private static final int ANIMATION_TIME = 500;
    private int _screenWidth;
    private boolean _isProgressBarActivated = false;

    private FlowManager _flowManager;
    private String _currentFragmentName = null;
    private final TreeMap<Integer, Object> _tSharedData = new TreeMap<>();
    private static final String ERROR_MESSAGE = "Flow manager was not defined (null)";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customTabActivityHelper = new CustomTabActivityHelper();
    }

    @Override
    protected void onStart() {
        super.onStart();
        customTabActivityHelper.bindCustomTabsService(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        customTabActivityHelper.unbindCustomTabsService(this);
    }

    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null)
            KeyboardUtils.hideKeyboard(view);
    }

    public void showKeyboard() {
        View view = getCurrentFocus();
        if (view != null)
            KeyboardUtils.showKeyboard(view);
    }

    /**
     * Animates the ProgressBar placed under the ActionBar increasing/decreasing its length from the
     * current size. The new length is calculated using the position of the current fragment in
     * relation with the total of fragments in the flow.
     * <p>For use it is needed calling {@link LinearMultiStepActivity@activateProgressBarAnimation}
     * before in order of activate the progress.</p>
     */
    protected void animateProgressBar() {
        int progress = (int) (_screenWidth * ((float) getFlowManager().getProgress(getCurrentFragmentName()) / (float) 100));
        ProgressbarAnim anim = new ProgressbarAnim(_vProgressbar, progress);
        anim.setDuration(ANIMATION_TIME);
        _vProgressbar.startAnimation(anim);

        ProgressbarAnim fullProgressBarAnimation = new ProgressbarAnim(progressBarFull, progress > 0 ? _screenWidth : 0);
        fullProgressBarAnimation.setDuration(ANIMATION_TIME);
        progressBarFull.startAnimation(fullProgressBarAnimation);
    }

    /**
     * Activates the progress bar placed under the ActionBar
     */
    protected void activateProgressBarAnimation() {
        _vProgressbar = findViewById(R.id.activity_progressbar);
        progressBarFull = findViewById(R.id.progressBarFull);
        _screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        _vProgressbar.setVisibility(View.VISIBLE);
        progressBarFull.setVisibility(View.VISIBLE);
        _isProgressBarActivated = true;
    }

    /**
     * Sets the flow manager that defines the sequence of screens that will be displayed
     *
     * @param fm the flow manager that defines the fragments to be displayed on this multi step process
     */
    protected final void setFlowManager(FlowManager fm) {
        _flowManager = fm;
    }

    /**
     * Gets the current <code>FlowManager</code> for this <code>LinearMultiStepActivity</code>
     *
     * @return the current flow manager
     */
    protected final FlowManager getFlowManager() {
        return _flowManager;
    }

    /**
     * Returns the name of the fragment that is currently being displayed in this multi-step process
     *
     * @return the name of the fragment currently displayed
     */
    protected final String getCurrentFragmentName() {
        return _currentFragmentName;
    }

    /**
     * Switches the current Fragment to the next fragment defined by the flow manager.
     */
    public void nextFragment() {
        if (_flowManager == null)
            throw new RuntimeException(ERROR_MESSAGE);

        String nextFragmentName = _flowManager.getNextFragmentName(_currentFragmentName, null);

        hideKeyboard();

        _currentFragmentName = nextFragmentName;
        changeFragment(_currentFragmentName);

        if (_isProgressBarActivated)
            animateProgressBar();

    }

    /**
     * Switches the current Fragment to the previous fragment defined by the flow manager.
     */
    public void previousFragment() {
        if (_flowManager == null)
            throw new RuntimeException(ERROR_MESSAGE);

        String previousFragmentName = _flowManager.getPreviousFragmentName(_currentFragmentName, null);

        hideKeyboard();

        _currentFragmentName = previousFragmentName;
        changeFragment(_currentFragmentName);

        if (_isProgressBarActivated)
            animateProgressBar();
    }

    /**
     * Goes back to the first fragment of the flow
     */
    public void onNavigateBackPressed() {
        if (_flowManager.isFirstFragment(_currentFragmentName))
            finish();
        else
            goToSpecificFragment(0);
    }

    /**
     * Goes to the specified fragment on the flow.
     *
     * @param fragmentName name of the fragment in the flow list
     */
    public void goToSpecificFragment(String fragmentName) {
        goToSpecificFragment(_flowManager.getFragmentPosition(fragmentName));
    }

    /**
     * Goes to the specified fragment on the flow.
     *
     * @param fragmentNumber position of the fragment in the flow list
     */
    public void goToSpecificFragment(int fragmentNumber) {
        if (_flowManager == null)
            throw new RuntimeException(ERROR_MESSAGE);

        String firstFragmentName = _flowManager.getFragmentAtPosition(fragmentNumber);

        hideKeyboard();

        _currentFragmentName = firstFragmentName;
        changeFragment(_currentFragmentName);

        if (_isProgressBarActivated)
            animateProgressBar();
    }

    /**
     * Change the fragment with the next one.
     *
     * @param newFragmentName name of the new fragment
     */
    private void changeFragment(String newFragmentName) {
        if (newFragmentName != null) {

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            try {
                Fragment newFragment = (Fragment) Class.forName(newFragmentName).newInstance();
                transaction.replace(R.id.activity_fragment, newFragment).commit();

            } catch (InstantiationException ignored) {
            } catch (IllegalAccessException ignored) {
            } catch (ClassNotFoundException ignored) {
            }

        }

    }

    @Override
    public void onBackPressed() {

        if (!_flowManager.isFirstFragment(_currentFragmentName))
            previousFragment();
        else
            super.onBackPressed();

    }

    public void setSharedObject(Integer key, Object object) {
        _tSharedData.put(key, object);
    }

    public Object getSharedObject(Integer key) {
        return _tSharedData.get(key);
    }
}