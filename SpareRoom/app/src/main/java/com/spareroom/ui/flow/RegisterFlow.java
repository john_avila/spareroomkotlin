package com.spareroom.ui.flow;

import com.spareroom.ui.screen.RegisterEmailFragment;
import com.spareroom.ui.screen.RegisterMethodFragment;
import com.spareroom.ui.screen.RegisterNameFragment;
import com.spareroom.ui.screen.RegisterPasswordFragment;
import com.spareroom.ui.screen.RegisterTypeFragment;

/**
 * Register UI flow
 * <p>
 * Created by miguel.rossi on 26/07/2016.
 */
public class RegisterFlow extends FlowState {

    private static final String PAGE_REGISTER_METHOD = RegisterMethodFragment.class.getName();
    private static final String PAGE_REGISTER_NAME = RegisterNameFragment.class.getName();
    private static final String PAGE_REGISTER_EMAIL = RegisterEmailFragment.class.getName();
    private static final String PAGE_REGISTER_PASSWORD = RegisterPasswordFragment.class.getName();
    private static final String PAGE_REGISTER_TYPE = RegisterTypeFragment.class.getName();

    public RegisterFlow() {
        SCREENS = new String[]{
                PAGE_REGISTER_METHOD,
                PAGE_REGISTER_NAME,
                PAGE_REGISTER_EMAIL,
                PAGE_REGISTER_PASSWORD,
                PAGE_REGISTER_TYPE
        };
        init();
    } // end Register() {
}
