package com.spareroom.ui.adapter

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.spareroom.R
import com.spareroom.ui.screen.MyAdvertsListFragment

private const val OFFERED_TAB = 0

class MyAdvertsPagerAdapter(fragmentManager: FragmentManager, context: Context) : FragmentStatePagerAdapter(fragmentManager) {

    private val titles = arrayOf(context.getString(R.string.rooms), context.getString(R.string.flatmates))

    override fun getItem(position: Int) = MyAdvertsListFragment.getInstance(position == OFFERED_TAB)

    override fun getCount() = titles.size

    override fun getPageTitle(position: Int): CharSequence = titles[position]
}