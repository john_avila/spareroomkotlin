package com.spareroom.ui.filters.screen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.spareroom.R;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.model.business.*;
import com.spareroom.model.business.SearchAdvertListResponse.ResponseTypes;
import com.spareroom.ui.filters.adapter.FiltersRecyclerAdapter;
import com.spareroom.ui.filters.adapter.viewparams.AbstractSummaryViewParams;
import com.spareroom.ui.filters.provider.*;
import com.spareroom.ui.screen.Fragment;
import com.spareroom.ui.screen.legacy.ResultsActivity;
import com.spareroom.ui.util.ConnectivityChecker;
import com.spareroom.ui.widget.params.CompoundButtonParams;
import com.spareroom.viewmodel.FilterViewModel;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class FilterSummaryFragment extends Fragment implements Injectable {
    private static final String SEARCH_ADVERT_LIST_PROPERTIES_KEY = "search_advert_list_properties_key";
    private static final String TOTAL_RESULTS_NEW_SEARCH_KEY = "total_results_new_search_key";
    private static final String TOTAL_RESULTS_EMPTY_SEARCH_KEY = "total_results_empty_search_key";

    private BottomBarController bottomBarController;
    private FilterViewModel viewModel;
    private TextMatchesWriter textMatchesWriter;
    private FiltersRecyclerAdapter adapter;

    private AbstractItemProvider itemProvider;
    private SearchAdvertListProperties currentSearchProperties;
    private SearchType searchType;
    private boolean isOffered;

    private static final int UNKNOWN_RESULTS = -1;
    private int totalResultsNewSearch;
    private int totalResultsEmptySearch;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    ConnectivityChecker connectivityChecker;

    static FilterSummaryFragment newInstance(SearchAdvertListProperties properties) {
        Bundle fragmentArguments = new Bundle();
        fragmentArguments.putSerializable(SEARCH_ADVERT_LIST_PROPERTIES_KEY, properties);

        FilterSummaryFragment fragment = new FilterSummaryFragment();
        fragment.setArguments(fragmentArguments);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.material_filters_main_layout, container, false);
        viewModel = ViewModelProviders.of(getBaseActivity(), viewModelFactory).get(FilterViewModel.class);
        textMatchesWriter = new TextMatchesWriter();

        if (viewModel.searchProperties() != null) {
            initProperties(viewModel.searchProperties());

        } else if (savedInstanceState != null) {
            initProperties((SearchAdvertListProperties) savedInstanceState.getSerializable(SEARCH_ADVERT_LIST_PROPERTIES_KEY));

        } else {
            assert getArguments() != null;
            initProperties((SearchAdvertListProperties) getArguments().getSerializable(SEARCH_ADVERT_LIST_PROPERTIES_KEY));

        }

        if (savedInstanceState != null) {
            totalResultsNewSearch = savedInstanceState.getInt(TOTAL_RESULTS_NEW_SEARCH_KEY, UNKNOWN_RESULTS);
            totalResultsEmptySearch = savedInstanceState.getInt(TOTAL_RESULTS_EMPTY_SEARCH_KEY, UNKNOWN_RESULTS);
        }

        initToolBar();
        initBottomBar(view);
        setRecyclerView(view);
        fetchMatchesCount(viewModel.shallPerformEmptySearch(), viewModel.shallPerformMatchesSearch());

        observeEmptySearch();
        observeMatchesSearch();

        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable(SEARCH_ADVERT_LIST_PROPERTIES_KEY, currentSearchProperties);
        outState.putInt(TOTAL_RESULTS_NEW_SEARCH_KEY, totalResultsNewSearch);
        outState.putInt(TOTAL_RESULTS_EMPTY_SEARCH_KEY, totalResultsEmptySearch);
        super.onSaveInstanceState(outState);
    }

    private void initProperties(SearchAdvertListProperties properties) {
        isOffered = properties instanceof SearchAdvertListPropertiesOffered;
        searchType = isOffered ? SearchType.OFFERED : SearchType.WANTED;
        currentSearchProperties = properties;
        itemProvider = isOffered ? new FilterOfferedMainItemProvider() : new FilterWantedMainItemProvider();
        itemProvider.createNewList(currentSearchProperties, null);
    }

    private void initToolBar() {
        setTitle(getString(itemProvider.titleId()));
        setHomeAsUpIndicator(R.drawable.ic_close);
    }

    private void initBottomBar(View parent) {
        TextView matches = parent.findViewById(R.id.filtersMain_matches);
        FloatingActionButton fab = parent.findViewById(R.id.filtersMain_floatingButton);
        ProgressBar progressBar = parent.findViewById(R.id.filtersMain_loading);
        bottomBarController = new BottomBarController(matches, progressBar);

        fab.setOnClickListener(v -> {
            Intent newSearch = new Intent();
            newSearch.putExtra(ResultsActivity.SEARCH_PROPERTIES_INTENT_KEY, currentSearchProperties);
            getBaseActivity().setResult(Activity.RESULT_OK, newSearch);
            getBaseActivity().finish();
        });
    }

    private void setRecyclerView(View parent) {
        RecyclerView recyclerView = parent.findViewById(R.id.filtersMain_recycler);
        adapter =
                new FiltersRecyclerAdapter(
                        getBaseActivity(),
                        itemProvider.get(),
                        this::summaryParamsOnClickListener,
                        this::compoundButtonOnClickListener,
                        null);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
    }

    private void observeEmptySearch() {
        viewModel.emptyAdvertListSearch().observe(this, searchAdvertListResponse -> {
            assert searchAdvertListResponse != null;
            if (ResponseTypes.SUCCESS.equals(searchAdvertListResponse.getResponseType())) {
                AdBoard board = (AdBoard) searchAdvertListResponse.getResponse();
                totalResultsEmptySearch = board.get_total();
            } else if (searchAdvertListResponse.isSuccessful()) {
                totalResultsEmptySearch = 0;
            }

            textMatchesWriter.showMatches();
        });
    }

    private void observeMatchesSearch() {
        viewModel.userAdvertListSearch().observe(this, searchAdvertListResponse -> {
            assert searchAdvertListResponse != null;
            if (ResponseTypes.SUCCESS.equals(searchAdvertListResponse.getResponseType())) {
                AdBoard board = (AdBoard) searchAdvertListResponse.getResponse();
                totalResultsNewSearch = board.get_total();
            } else if (searchAdvertListResponse.isSuccessful()) {
                totalResultsNewSearch = 0;
            }

            textMatchesWriter.showMatches();
        });
    }

    private void fetchMatchesCount(boolean shallPerformEmptySearch, boolean shallPerformMatchesSearch) {
        if (!shallPerformEmptySearch && !shallPerformMatchesSearch) {
            textMatchesWriter.showMatches();

        } else if (connectivityChecker.isConnected()) {
            if (shallPerformEmptySearch)
                performEmptySearch();

            if (shallPerformMatchesSearch) {
                performMatchesSearch();
            } else {
                textMatchesWriter.showMatches();
            }

        } else {
            textMatchesWriter.showError();

        }
    }

    private void performEmptySearch() {
        SearchAdvertListProperties emptySearchParameters;

        if (isOffered) {
            emptySearchParameters = new SearchAdvertListPropertiesOffered();
            ((SearchAdvertListPropertiesOffered) emptySearchParameters).setMilesFromMax(((SearchAdvertListPropertiesOffered) currentSearchProperties).getMilesFromMax());
        } else {
            emptySearchParameters = new SearchAdvertListPropertiesWanted();
        }
        emptySearchParameters.setWhere(currentSearchProperties.getWhere());
        emptySearchParameters.setIncludeHidden(currentSearchProperties.getIncludeHidden());

        viewModel.shallPerformEmptySearch(false);
        viewModel.emptyAdvertListSearch().retrieveAdvertList(emptySearchParameters, searchType, false);

        bottomBarController.showAsLoading(true);
    }

    private void performMatchesSearch() {
        viewModel.shallPerformMatchesSearch(false);
        viewModel.userAdvertListSearch().retrieveAdvertList(currentSearchProperties, searchType, false);

        bottomBarController.showAsLoading(true);
    }

    private void compoundButtonOnClickListener(View view) {
        int position = (int) view.getTag(R.id.position_tag);
        CompoundButtonParams params = (CompoundButtonParams) itemProvider.get().get(position).params();
        params.selected(!params.selected());
        adapter.updateAdapter(position);

        itemProvider.updateProperties(currentSearchProperties, true);
        fetchMatchesCount(true, true);
    }

    private void summaryParamsOnClickListener(View summaryParamsView) {
        String summaryName = (String) summaryParamsView.getTag(R.id.id_tag);
        AbstractSummaryViewParams summaryParams = (AbstractSummaryViewParams) summaryParamsView.getTag(R.id.param_tag);
        Fragment fragment;

        switch (summaryName) {
            case AbstractItemProvider.SEARCH_RADIUS:
                fragment = FilterMapsFragment.newInstance(((SearchAdvertListPropertiesOffered) currentSearchProperties).duplicate());
                break;
            default:
                fragment = FilterDetailFragment.newInstance(
                        isOffered
                                ? ((SearchAdvertListPropertiesOffered) currentSearchProperties).duplicate()
                                : ((SearchAdvertListPropertiesWanted) currentSearchProperties).duplicate(),
                        summaryParams.tag());
                break;
        }
        loadFragment(fragment);
    }

    private void loadFragment(Fragment nextFragment) {
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
        fragmentTransaction.replace(R.id.main_frame, nextFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private class TextMatchesWriter {

        private void showMatches() {
            String matches = totalResultsNewSearch + " " + getResources().getQuantityString(R.plurals.matches, totalResultsNewSearch);
            bottomBarController.updateMatches(
                    totalResultsNewSearch == UNKNOWN_RESULTS
                            ? matches
                            : matches + " " + getString(R.string.outOf, totalResultsEmptySearch)
                            + " " + getResources().getQuantityString(R.plurals.results, totalResultsEmptySearch));
        }

        private void showError() {
            bottomBarController.updateMatches(getString(R.string.no_connection));
        }

    }

    private class BottomBarController {
        private final TextView textViewMatches;
        private final ProgressBar progressBar;

        private BottomBarController(TextView textViewMatches, ProgressBar progressBar) {
            this.textViewMatches = textViewMatches;
            this.progressBar = progressBar;
        }

        private void hideMatches(boolean hide) {
            showAsLoading(false);
            textViewMatches.setVisibility(hide ? View.INVISIBLE : View.VISIBLE);
        }

        private void updateMatches(String matches) {
            textViewMatches.setText(matches);
            showAsLoading(false);
            hideMatches(false);
        }

        private void showAsLoading(boolean show) {
            if (show)
                hideMatches(true);

            progressBar.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
            textViewMatches.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
        }

    }

}
