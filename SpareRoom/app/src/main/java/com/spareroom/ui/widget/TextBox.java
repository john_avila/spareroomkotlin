package com.spareroom.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.*;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;

import com.spareroom.R;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

/**
 * Custom edit text with title, underline, hide feature, message and message icon.
 * <p>
 * See {@link R.styleable#TextBox}
 */
public class TextBox extends ConstraintLayout {
    private static String PASSWORD_HIDE;
    private static String FONT_FAMILY_MEDIUM;
    private static String PASSWORD_SHOW;

    private OnContentChangedListener _eventListener;
    private final Context _context;

    private String _titleText;
    private String _messageText;
    private Drawable _messageIconSrc;
    private int _underlineFocusedColor; // @color/white by default
    private int _underlineNonFocusedColor; // @color/white_55 by default
    private boolean _isPassword; // false by default
    private int _underlineBiggerThickness; // @dimen/textBoxUnderlineBiggerThickness by default
    private int _underlineSmallerThickness; // @dimen/textBoxUnderlineSmallerThickness by default

    private TextView _tvTitle;
    private EditText _etEditableText;
    private TextView _tvHide;
    private View _vUnderlineLeft;
    private View _vUnderlineBottom;
    private View _vUnderlineRight;
    private TextView _tvMessageText;
    private ImageView _ivMessageIcon;

    public interface OnContentChangedListener {
        void onContentChangedEvent(CharSequence s);
    }

    public TextBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        _context = context;
        init(context, attrs);
    }

    public TextBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        _context = context;
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attributeSet) {
        getStrings();

        TypedArray a = context.obtainStyledAttributes(attributeSet, R.styleable.TextBox);

        // Get custom view attributes
        int _titleTextColor;
        int _editableTextInputType;
        boolean _isMessageInUse;
        try {
            _titleText = a.getString(R.styleable.TextBox_titleText);
            _messageText = a.getString(R.styleable.TextBox_messageText);
            _messageIconSrc = ContextCompat.getDrawable(context, a.getResourceId(R.styleable.TextBox_messageIconSrc, R.drawable.icon_error));
            _titleTextColor = a.getInt(R.styleable.TextBox_titleColor, ContextCompat.getColor(context, R.color.light_blue));
            _editableTextInputType = a.getInt(R.styleable.TextBox_android_inputType, InputType.TYPE_CLASS_TEXT);
            _underlineFocusedColor = a.getInt(R.styleable.TextBox_underlineFocusedColor, ContextCompat.getColor(context, R.color.white));
            _underlineNonFocusedColor = a.getInt(R.styleable.TextBox_underlineFocusedColor, ContextCompat.getColor(context, R.color.light_blue));
            _underlineSmallerThickness = a.getDimensionPixelSize(R.styleable.TextBox_underlineSmallerThickness, getResources().getDimensionPixelSize(
                    R.dimen.textBoxUnderlineSmallerThickness));
            _underlineBiggerThickness = a.getDimensionPixelSize(R.styleable.TextBox_underlineBiggerThickness, getResources().getDimensionPixelSize(
                    R.dimen.textBoxUnderlineBiggerThickness));
            _isMessageInUse = a.getBoolean(R.styleable.TextBox_messageVisibility, false);
            _isPassword = a.getBoolean(R.styleable.TextBox_isPassword, false);

        } finally {
            a.recycle();
        }

        // Initializing the views
        inflate(getContext(), R.layout.material_widget_text_box, this);
        View rEditableText = findViewById(R.id.textBox_rEditableText);
        _tvTitle = findViewById(R.id.textBox_title);
        _etEditableText = findViewById(R.id.textBox_editableText);
        _tvHide = findViewById(R.id.textBox_hide);
        _vUnderlineLeft = findViewById(R.id.textBox_underlineLeft);
        _vUnderlineBottom = findViewById(R.id.textBox_underlineBottom);
        _vUnderlineRight = findViewById(R.id.textBox_underlineRight);
        _tvMessageText = findViewById(R.id.textBox_messageText);
        _ivMessageIcon = findViewById(R.id.textBox_messageIcon);

        rEditableText.setOnClickListener(v -> _etEditableText.requestFocus());

        _etEditableText.setInputType(_editableTextInputType);
        _etEditableText.setTypeface(Typeface.create(getResources().getString(R.string.font_family_medium), Typeface.NORMAL));
        _etEditableText.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                changeUnderlineColor(_underlineFocusedColor);
                changeUnderlineSize(_underlineBiggerThickness);
            } else {
                changeUnderlineColor(_underlineNonFocusedColor);
                changeUnderlineSize(_underlineSmallerThickness);
            }
        });
        _etEditableText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // the event listener may not have been set (see setOnContentChangedListener)
                if (_eventListener != null)
                    _eventListener.onContentChangedEvent(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

        });

        if (_etEditableText.hasFocus()) {
            changeUnderlineColor(_underlineFocusedColor);
            changeUnderlineSize(_underlineBiggerThickness);
        }

        _tvTitle.setText(_titleText);
        _tvTitle.setTextColor(_titleTextColor);
        _tvTitle.setTypeface(Typeface.create(getResources().getString(R.string.font_family_regular), Typeface.NORMAL));
        _tvMessageText.setTypeface(Typeface.create(getResources().getString(R.string.font_family_regular), Typeface.NORMAL));

        activatePasswordFeatures(_isPassword);
        changeMessageViewsVisibility(_isMessageInUse);

    }

    private void getStrings() {
        PASSWORD_HIDE = getResources().getString(R.string.textBox_passwordHide);
        FONT_FAMILY_MEDIUM = getResources().getString(R.string.font_family_medium);
        PASSWORD_SHOW = getResources().getString(R.string.textBox_passwordShow);
    }

    /**
     * Activates the password feature.
     */
    private void activatePasswordFeatures(boolean active) {
        if (active) {
            _tvHide.setVisibility(View.VISIBLE);
            _tvHide.setOnClickListener(new HideOnClickListener(_etEditableText));
        } else {
            _tvHide.setVisibility(View.GONE);
            _tvHide.setOnClickListener(null);
        }
        _etEditableText.requestFocus();
    }

    /**
     * If the TextBox is using the message it is set as <code>INVISIBLE</code> to make use of the
     * space the message and its icon need. Otherwise, the visibility is set to
     * <code>GONE</code>, in this way the space is not used allowing other components (or
     * the TextBor) sets their margin in relation with the underline instead of the message.
     */
    private void changeMessageViewsVisibility(boolean areVisible) {

        if (areVisible) {
            _tvMessageText.setVisibility(INVISIBLE);
            if (_messageText != null)
                _tvMessageText.setText(_messageText);
            _ivMessageIcon.setVisibility(INVISIBLE);
            _ivMessageIcon.setImageDrawable(_messageIconSrc);

        } else {
            _tvMessageText.setVisibility(GONE);
            _ivMessageIcon.setVisibility(GONE);
        }

    }

    private void changeUnderlineColor(int color) {
        _vUnderlineLeft.setBackgroundColor(color);
        _vUnderlineBottom.setBackgroundColor(color);
        _vUnderlineRight.setBackgroundColor(color);
    }

    /**
     * Swap the thickness between two predefined values
     */
    private void changeUnderlineSize(int newThickness) {
        _vUnderlineLeft.getLayoutParams().width = newThickness;
        _vUnderlineBottom.getLayoutParams().height = newThickness;
        _vUnderlineRight.getLayoutParams().width = newThickness;

    }

    /**
     * Shows the message and the icon. Take care of setting before the <code>isMessageVisible</code> as true if we don't want the
     * components moving.
     */
    public void showMessage() {
        if (!isMessageDisplayed()) {
            Animation animation = AnimationUtils.loadAnimation(_context, R.anim.slide_up);

            _ivMessageIcon.setVisibility(VISIBLE);
            _ivMessageIcon.startAnimation(animation);

            _tvMessageText.setVisibility(VISIBLE);
            _tvMessageText.startAnimation(animation);
        }
    }

    /**
     * Hides the message and the icon. Take care of setting before the <code>isMessageVisible</code> as true if we don't want the
     * components moving.
     */
    public void hideMessage() {
        if (isMessageDisplayed()) {
            Animation animation = AnimationUtils.loadAnimation(_context, R.anim.slide_down);

            _ivMessageIcon.setVisibility(INVISIBLE);
            _ivMessageIcon.startAnimation(animation);

            _tvMessageText.setVisibility(INVISIBLE);
            _tvMessageText.startAnimation(animation);
        }
    }

    public void setOnContentChangedListener(OnContentChangedListener eventListener) {
        _eventListener = eventListener;
    }

    public void setOnEditorActionListener(EditText.OnEditorActionListener listener) {
        _etEditableText.setOnEditorActionListener(listener);
    }

    public String getTitleText() {
        return _titleText;
    }

    public void setTitleText(String titleText) {
        _titleText = titleText;
        _tvTitle.setText(titleText);
    }

    public String getMessageText() {
        return _messageText;
    }

    public void setMessageText(String messageText) {
        _messageText = messageText;
        _tvMessageText.setText(messageText);
    }

    public Drawable getMessageIconSrc() {
        return _messageIconSrc;
    }

    public void setMessageIconSrc(Drawable messageIconSrc) {
        _messageIconSrc = messageIconSrc;
        _ivMessageIcon.setImageDrawable(messageIconSrc);
    }

    public String getText() {
        return _etEditableText.getText().toString();
    }

    public boolean isMessageDisplayed() {
        return (_tvMessageText.getVisibility() == View.VISIBLE);
    }

    public boolean isPassword() {
        return _isPassword;
    }

    public void setPassword(boolean isPassword) {
        _isPassword = isPassword;
        activatePasswordFeatures(isPassword);
    }

    public int getMessageTextHeight() {
        return _tvMessageText.getHeight();
    }

    public void setText(String text) {
        if (text == null)
            _etEditableText.setText("");
        else {
            _etEditableText.setText(text);
            _etEditableText.setSelection(text.length());
        }
    }

    private static class HideOnClickListener implements OnClickListener {
        private final EditText _etPasswordInput;
        private boolean _isHidden = false;

        HideOnClickListener(EditText etPasswordInput) {
            _etPasswordInput = etPasswordInput;
        }

        @Override
        public void onClick(View v) {
            _etPasswordInput.requestFocus();
            if (_isHidden) {
                ((TextView) v).setText(PASSWORD_HIDE);
                _etPasswordInput.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                _etPasswordInput.setTypeface(Typeface.create(FONT_FAMILY_MEDIUM, Typeface.NORMAL));

            } else {
                ((TextView) v).setText(PASSWORD_SHOW);
                _etPasswordInput.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                _etPasswordInput.setTypeface(Typeface.create(FONT_FAMILY_MEDIUM, Typeface.NORMAL));

            }

            _etPasswordInput.setSelection(_etPasswordInput.getText().length());
            _isHidden = !_isHidden;

        }
    }
}
