package com.spareroom.ui.filters.provider;

import com.spareroom.ui.filters.adapter.viewparams.IResettableParams;
import com.spareroom.ui.widget.params.ViewParamsType;

public class FilterItem<T extends IResettableParams> {
    private final T params;

    public FilterItem(T params) {
        this.params = params;
    }

    public ViewParamsType type() {
        return params.type();
    }

    public String tag() {
        return params.tag();
    }

    public T params() {
        return params;
    }

}
