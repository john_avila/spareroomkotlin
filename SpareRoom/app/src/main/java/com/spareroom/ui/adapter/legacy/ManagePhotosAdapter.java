package com.spareroom.ui.adapter.legacy;

import android.app.Dialog;
import android.os.AsyncTask;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.animation.Animation;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.LongTaskManager;
import com.spareroom.controller.UploadVideoTask;
import com.spareroom.controller.legacy.AsyncTaskManager;
import com.spareroom.integration.dependency.component.ComponentRepository;
import com.spareroom.integration.imageloader.ImageLoader;
import com.spareroom.integration.imageloader.ImageLoadingListener;
import com.spareroom.integration.sharedpreferences.UploadVideoDAO;
import com.spareroom.lib.core.LongTask;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.model.business.*;
import com.spareroom.ui.anim.PushInAnim;
import com.spareroom.ui.screen.Activity;
import com.spareroom.ui.screen.AlertDialogBuilder;
import com.spareroom.ui.screen.legacy.ManagePhotosActivity;
import com.spareroom.ui.util.UiUtils;

import java.util.LinkedList;
import java.util.TreeMap;

import javax.inject.Inject;

import androidx.appcompat.view.ActionMode;
import androidx.core.content.ContextCompat;

public class ManagePhotosAdapter extends BaseAdapter {

    private static final int VIEW_TYPE_PHOTO = 0;
    private static final int VIEW_TYPE_VIDEO = 1;

    private LinkedList<Picture> _lPictureGallery;
    private LinkedList<Video> _lVideoGallery;
    private final Activity _activity;
    private final LayoutInflater layoutInflater;

    private boolean _isEditModeEnabled = false;

    private ActionMode _actionMode;

    private final TreeMap<Integer, CheckBox> _tChecked2;
    private int _checkboxWidth = 0;

    private final String _flatshareId;
    private final String _flatshareType;

    private final AsyncTaskManager _taskManager;

    @Inject
    ImageLoader _imageLoader;

    public ManagePhotosAdapter(Activity activity, PictureGallery pictureGallery, VideoGallery videoGallery, String flatshareId, String flatshareType) {
        ComponentRepository.get().getAppComponent().inject(this);

        layoutInflater = LayoutInflater.from(activity);

        _lPictureGallery = (pictureGallery != null) ? pictureGallery.get_gallery() : null;
        _lVideoGallery = (videoGallery != null) ? videoGallery.getGallery() : null;
        _activity = activity;
        _flatshareId = flatshareId;
        _flatshareType = flatshareType;
        _tChecked2 = new TreeMap<>();
        _taskManager = new AsyncTaskManager();
    }

    @Override
    public int getCount() {
        return ((_lPictureGallery != null) ? _lPictureGallery.size() : 0) + ((_lVideoGallery != null) ? _lVideoGallery.size() : 0);
    }

    @Override
    public Object getItem(int position) {
        Object o = null;

        if (position < _lPictureGallery.size())
            o = _lPictureGallery.get(position);
        else if (position >= _lPictureGallery.size() && position < (_lPictureGallery.size() + _lVideoGallery.size()))
            o = _lVideoGallery.get(position);

        return o;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position < _lPictureGallery.size() ? VIEW_TYPE_PHOTO : VIEW_TYPE_VIDEO;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

        int itemViewType = getItemViewType(position);

        if (view == null)
            view = layoutInflater.inflate(R.layout.activity_manage_photos_picture, parent, false);

        CheckBox cb = view.findViewById(R.id.managePicture_cb);
        final ImageView ivPicturePlaceholder = view.findViewById(R.id.managePicture_ivPlaceholder);
        View placeholder = view.findViewById(R.id.managePicture_rlPlaceholder);
        final ImageView ivPicturePreview = placeholder.findViewById(R.id.managePicture_ivPicture);
        Button bCancel = view.findViewById(R.id.activity_manage_photos_bCancel);
        bCancel.setVisibility(View.GONE);

        ivPicturePreview.setTag(R.id.image_view_type_tag, itemViewType);
        ivPicturePlaceholder.setImageResource(itemViewType == VIEW_TYPE_PHOTO ? R.drawable.my_ads_photo_placeholder : R.drawable.ic_play_circle_filled);
        ivPicturePlaceholder.setVisibility(View.VISIBLE);

        TextView ivDefaultTag = view.findViewById(R.id.managePicture_ivDefault);
        ivDefaultTag.setBackground(UiUtils.getTintedDrawable(ivDefaultTag.getContext(), R.drawable.ribbon, R.color.cyan));
        TextView tvCaption = view.findViewById(R.id.activity_manage_photos_picture_tvCaption);

        if (itemViewType == VIEW_TYPE_PHOTO) {
            if (position == 0) {
                ivDefaultTag.setVisibility(View.VISIBLE);
            } else {
                ivDefaultTag.setVisibility(View.GONE);
            }

            placeholder.setBackgroundResource(R.color.grey_5);

            Picture p = _lPictureGallery.get(position);

            _imageLoader.loadImage(p.getLargeUrl() + "?square=" + (ivPicturePreview.getMeasuredWidth()), ivPicturePreview, new ImageLoadingListener() {
                @Override
                public void onLoadingComplete() {
                    ivPicturePlaceholder.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingFailed(Exception exception) {
                    ivPicturePlaceholder.setVisibility(View.VISIBLE);
                }
            });

            PhotoOnLongClickListener longClickListener = new PhotoOnLongClickListener(_lPictureGallery.get(position).getId());

            view.setOnLongClickListener(longClickListener);
            tvCaption.setText(p.getCaption());
            tvCaption.setOnClickListener(new CaptionOnClickListener(p.getId(), p.getCaption()));
            tvCaption.setOnLongClickListener(longClickListener);
            ivPicturePreview.setOnLongClickListener(longClickListener);

        } else {
            Video v = _lVideoGallery.get(position - _lPictureGallery.size());
            String status;

            switch (v.getStatus()) {
                case LIVE:
                    status = _activity.getString(R.string.activity_manage_photos_video_live);
                    break;

                case REJECTED:
                    status = _activity.getString(R.string.activity_manage_photos_video_rejected);
                    break;

                case PENDING:
                    status = _activity.getString(R.string.activity_manage_photos_video_pending);
                    break;

                case UPLOADING:
                    bCancel.setVisibility(View.VISIBLE);
                    bCancel.setText(_activity.getString(R.string.activity_manage_photos_video_upload_cancel));
                    bCancel.setOnClickListener(btn -> {
                        LongTask longTask = LongTaskManager.getInstance().getLongTask(UploadVideoTask.ID);
                        if (longTask != null)
                            longTask.cancel();
                        UploadVideoDAO.delete(_activity, _flatshareId);

                        ((ManagePhotosActivity) _activity).deleteVideoUpload();
                    });
                    status = _activity.getString(R.string.activity_manage_photos_video_inProcess);
                    break;

                case UPLOAD_FAILED:
                    bCancel.setVisibility(View.VISIBLE);
                    bCancel.setText(_activity.getString(R.string.retry));
                    bCancel.setOnClickListener(btn -> {
                        Parameters params = new Parameters();
                        params.add(ManagePhotosActivity.EXTRA_KEY_FLATSHARE_ID, _flatshareId);

                        UploadVideoDAO.delete(_activity, _flatshareId);
                        ((ManagePhotosActivity) _activity).uploadVideo(params);
                        _lVideoGallery.getFirst().setStatus(Video.Status.UPLOADING);
                    });
                    status = _activity.getString(R.string.activity_manage_photos_video_upload_failed);
                    break;

                default:
                    status = _activity.getString(R.string.activity_manage_photos_video_inProcess);

            }

            ivDefaultTag.setVisibility(View.GONE);

            placeholder.setBackgroundResource(R.drawable.blurred_preview);

            ivPicturePreview.setImageBitmap(null);
            ivPicturePreview.setOnLongClickListener(null);

            tvCaption.setText(status);
            tvCaption.setOnLongClickListener(null);
            tvCaption.setOnClickListener(null);
        }

        cb.setOnClickListener(new CheckBoxOnClickListener(position));
        if (_checkboxWidth == 0) {
            Display display = _activity.getWindowManager().getDefaultDisplay();
            cb.measure(display.getWidth(), display.getHeight());

            _checkboxWidth = cb.getMeasuredWidth();
        }

        if (_tChecked2.containsKey(position))
            cb.setChecked(true);
        else
            cb.setChecked(false);

        if (!_isEditModeEnabled) {
            cb.getLayoutParams().width = 0;
            cb.requestLayout();
        } else {
            cb.getLayoutParams().width = _checkboxWidth;
            cb.requestLayout();
        }

        return view;
    }

    public void stop() {
        _taskManager.cancelAll();
    }

    public void editMode() {
        _actionMode = _activity.startSupportActionMode(new ModeCallback());
        _isEditModeEnabled = true;

        ListView lv = _activity.findViewById(R.id.activity_manage_photos_lv);
        if (lv != null) {
            for (int i = 0; i < lv.getCount(); i++) {
                View row = lv.getChildAt(i);
                if (row != null) {
                    CheckBox cb = row.findViewById(R.id.managePicture_cb);

                    PushInAnim anim = new PushInAnim(cb, _checkboxWidth, false);
                    anim.setDuration(500);
                    cb.startAnimation(anim);
                }
            }
            lv.requestLayout();
        }
    }

    private void deselectAll() {
        for (CheckBox cb : _tChecked2.values()) {
            cb.setChecked(false);
        }
    }

    public void clearVideo() {
        _lVideoGallery.clear();
    }

    public void addVideo() {
        Video video = new Video();
        video.setStatus(Video.Status.UPLOADING);
        _lVideoGallery.clear();
        _lVideoGallery.add(video);
    }

    public void setPictureGallery(PictureGallery pictureGallery) {
        _lPictureGallery = pictureGallery.get_gallery();
    }

    public void setVideoGallery(VideoGallery videoGallery) {
        _lVideoGallery = videoGallery.getGallery();
    }

    //region INNER CLASSES

    private final class ModeCallback implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = _activity.getMenuInflater();
            inflater.inflate(R.menu.action_mode_delete_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            // Here, you can check selected items to adapt available actions
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            deselectAll();

            ListView lv = _activity.findViewById(R.id.activity_manage_photos_lv);
            final View bAddPicture = _activity.findViewById(R.id.activity_manage_bAddPicture);

            if (mode == _actionMode) {
                _actionMode = null;
            }
            _isEditModeEnabled = false;

            if (lv != null)
                for (int i = 0; i < lv.getCount(); i++) {
                    View row = lv.getChildAt(i);
                    if (row != null) {
                        CheckBox cb = row.findViewById(R.id.managePicture_cb);

                        PushInAnim anim = new PushInAnim(cb, _checkboxWidth, true);
                        anim.setDuration(500);
                        cb.startAnimation(anim);

                        // only for the last animation enables the button
                        if (i == (lv.getChildCount()) - 1)
                            anim.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {
                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    if (bAddPicture != null) {
                                        bAddPicture.setClickable(true);
                                        bAddPicture.setBackgroundColor(ContextCompat.getColor(_activity, R.color.orange));
                                    }
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {
                                }
                            });
                    }
                }
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

            switch (item.getItemId()) {
                case R.id.menu_item_delete:

                    // Pictures
                    for (int i = 0; i < _lPictureGallery.size(); i++) {
                        if (_tChecked2.get(i) != null) {
                            Parameters p = new Parameters();
                            if (_flatshareId != null)
                                p.add("flatshare_id", _flatshareId);
                            if (_flatshareType != null)
                                p.add("flatshare_type", _flatshareType);
                            p.add("photo_id", _lPictureGallery.get(i).getId());

                            Parameters[] vParameters = {p};

                            AsyncTask<Parameters, Integer, Object> at = ((ManagePhotosActivity) _activity).instantiateDeletePictureAsyncTask();
                            _taskManager.register(at);
                            at.execute(vParameters);
                        }
                    }

                    // Videos
                    for (int i = 0; i < _lVideoGallery.size(); i++) {
                        if (_tChecked2.get(i + _lPictureGallery.size()) != null) {
                            // The video is not in the server but it is in the shared preferences
                            Video video = _lVideoGallery.get(i);

                            switch (video.getStatus()) {
                                case UPLOAD_FAILED:
                                    UploadVideoDAO.delete(_activity, _flatshareId);
                                    break;

                                case UPLOADING:
                                    LongTask longTask = LongTaskManager.getInstance().getLongTask(UploadVideoTask.ID);
                                    if (longTask != null)
                                        longTask.cancel();

                                    LongTaskManager.getInstance().removeLongTask(UploadVideoTask.ID);
                                    break;

                                default:
                                    Parameters p = new Parameters();

                                    if (_flatshareId != null)
                                        p.add("flatshare_id", _flatshareId);
                                    p.add("video_id", _lVideoGallery.get(i).getId());

                                    AsyncTask<Parameters, Integer, Object> at = ((ManagePhotosActivity) _activity).instantiateDeleteVideoAsyncTask();
                                    _taskManager.register(at);
                                    at.execute(p);

                            }

                            ((ManagePhotosActivity) _activity).deleteVideoUpload();

                        }
                    }

                    break;
            }

            mode.finish();

            return true;
        }

    } //end class ModeCallback

    private class CheckBoxOnClickListener implements OnClickListener {
        private final int _position;

        CheckBoxOnClickListener(int position) {
            _position = position;
        }

        @Override
        public void onClick(View v) {
            if (((CheckBox) v).isChecked()) {
                _tChecked2.put(_position, (CheckBox) v);
            } else {
                _tChecked2.remove(_position);
            }
            if (!_tChecked2.isEmpty()) {
                if (_actionMode == null)
                    _actionMode = _activity.startSupportActionMode(new ModeCallback());
            } else {
                if (_actionMode != null)
                    _actionMode.finish();
            }
        }

    } //end class CheckBoxOnClickListener

    private class PhotoOnLongClickListener implements OnLongClickListener {

        private final String _photoId;

        PhotoOnLongClickListener(String photoId) {
            _photoId = photoId;
        }

        @Override
        public boolean onLongClick(View v) {
            View view = LayoutInflater.from(v.getContext()).inflate(R.layout.dialog_message_buttons, null);

            TextView tvMessage = view.findViewById(R.id.dialog_message);
            Button bPositive = view.findViewById(R.id.dialog_bPositive);
            Button bNegative = view.findViewById(R.id.dialog_bNegative);

            Dialog d = new AlertDialogBuilder(v.getContext(), view).create();

            tvMessage.setText(_activity.getString(R.string.activity_manage_photos_default_picture));
            bPositive.setOnClickListener(new DefaultOnClickListener(d));
            bNegative.setOnClickListener(new DialogUtil.CancelOnClickListener(d));
            d.show();
            return false;
        }

        private class DefaultOnClickListener implements OnClickListener {
            private final Dialog _dialog;

            DefaultOnClickListener(Dialog d) {
                _dialog = d;
            }

            @Override
            public void onClick(View v) {

                Parameters p = new Parameters();
                if (_flatshareId != null)
                    p.add("advert_id", _flatshareId);
                p.add("sort_order_" + _photoId, "1");

                for (int i = 0; i < _lPictureGallery.size(); i++) {
                    if (!_lPictureGallery.get(i).getId().equals(_photoId))
                        p.add("sort_order_" + _lPictureGallery.get(i).getId(), Integer.toString(i + 2));
                }
                Parameters[] vParameters = {p};

                AsyncTask<Parameters, Integer, Object> at = ((ManagePhotosActivity) _activity).instantiateChangeOrderAsyncTask();
                _taskManager.register(at);
                at.execute(vParameters);

                _dialog.dismiss();
            }
        }

    } //end class PhotoOnLongClickListener

    private class CaptionOnClickListener implements OnClickListener {
        private final String _photoId;
        private final String _defaultText;

        CaptionOnClickListener(String photoId, String defaultText) {
            _photoId = photoId;
            _defaultText = defaultText;
        }

        @Override
        public void onClick(View v) {
            View vContent = LayoutInflater.from(v.getContext()).inflate(R.layout.dialog_edit_caption, null);
            EditText etCaption = vContent.findViewById(R.id.dialog_edit_caption_etCaption);

            etCaption.setText(_defaultText);

            Button bPositive = vContent.findViewById(R.id.dialog_bPositive);
            Button bNegative = vContent.findViewById(R.id.dialog_bNegative);

            Dialog d = new AlertDialogBuilder(vContent.getContext(), vContent).create();

            bPositive.setOnClickListener(new UpdateOnClickListener(d));
            bNegative.setOnClickListener(new DialogUtil.CancelOnClickListener(d));

            d.show();
        }

        private class UpdateOnClickListener implements OnClickListener {
            private final Dialog _dialog;

            UpdateOnClickListener(Dialog d) {
                _dialog = d;
            }

            @Override
            public void onClick(View v) {
                Parameters p = new Parameters();
                if (_flatshareId != null)
                    p.add("advert_id", _flatshareId);
                p.add("caption_" + _photoId, ((EditText) _dialog.findViewById(R.id.dialog_edit_caption_etCaption)).getText().toString());
                Parameters[] vParameters = {p};

                AsyncTask<Parameters, Integer, Object> at = ((ManagePhotosActivity) _activity).instantiateEditCaptionAsyncTask();
                _taskManager.register(at);
                at.execute(vParameters);

                _dialog.dismiss();
            }
        }
    } //end class CaptionOnClickListener

    //endregion INNER CLASSES

}
