package com.spareroom.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.spareroom.R;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

public class SummaryView extends ConstraintLayout {
    private View viewHighlight;
    private ImageView imageViewIcon;
    private TextView textViewTitle;
    private TextView textViewSubtitle;

    public SummaryView(Context context) {
        super(context);
        init();
    }

    public SummaryView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SummaryView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.material_widget_summary, this);
        viewHighlight = findViewById(R.id.widget_summary_highlight);
        imageViewIcon = findViewById(R.id.widget_summary_icon);
        textViewTitle = findViewById(R.id.widget_summary_title);
        textViewSubtitle = findViewById(R.id.widget_summary_subtitle);

        setMinimumHeight(getResources().getDimensionPixelSize(R.dimen.material_widget_summary_height));
        setBackgroundResource(R.drawable.material_borderless_ripple_black);
    }

    public void highlight(String subtitleText, boolean highlight) {
        viewHighlight.setVisibility(highlight ? VISIBLE : GONE);
        textViewSubtitle.setTextColor(ContextCompat.getColor(getContext(), highlight ? R.color.orange : R.color.grey_2));
        textViewSubtitle.setText(subtitleText);
    }

    public void icon(int drawableResource) {
        imageViewIcon.setImageDrawable((drawableResource != 0) ? ContextCompat.getDrawable(getContext(), drawableResource) : null);
    }

    public void title(String title) {
        textViewTitle.setText(title);
    }

}
