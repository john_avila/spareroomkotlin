package com.spareroom.ui.flow;

import com.spareroom.ui.screen.legacy.*;

import java.util.HashMap;

public class PlaceAdStateErrorOffered extends FlowState {
    protected static final String PAGE_OFFERED_20 = PlaceAdUploadingFragment.class.getName();
    protected static HashMap<String, String> _mError = new HashMap<>();

    protected static final String PAGE_OFFERED_19 = PlaceAdTitleDescriptionFragment.class.getName();
    protected static final String PAGE_OFFERED_12 = PlaceAdOfferedAgeFlatmates.class.getName();
    protected static final String PAGE_OFFERED_17 = PlaceAdOfferedHappyAnyAgeFragment.class.getName();
    protected static final String PAGE_OFFERED_16 = PlaceAdOfferedHappyToLiveWithFragment.class.getName(); // binding pending
    protected static final String PAGE_OFFERED_10 = PlaceAdOfferedAvailabilityPeriodBills.class.getName();
    protected static final String PAGE_OFFERED_5 = PlaceAdOfferedAreYouAtPropertyFragment.class.getName();
    protected static final String PAGE_OFFERED_4A = PlaceAdOfferedCompanyNameFragment.class.getName();
    protected static final String PAGE_OFFERED_9 = PlaceAdOfferedAddRoomFragment.class.getName(); // binding pending

    public PlaceAdStateErrorOffered() {
        SCREENS = new String[]{
                PAGE_OFFERED_20
        };
        init();
    }

    @Override
    protected void init() {
        super.init();
        _mError.put("ad_text", PAGE_OFFERED_19);
        _mError.put("ad_title", PAGE_OFFERED_19);
        _mError.put("age", PAGE_OFFERED_12);
        _mError.put("age_req", PAGE_OFFERED_17);
        _mError.put("first_name", PAGE_OFFERED_19); // shouldn't it go to the first one?
        _mError.put("gender", PAGE_OFFERED_16);
        _mError.put("inagreement", PAGE_OFFERED_19);
        _mError.put("last_name", PAGE_OFFERED_19); // shouldn't it go to the first one?
        _mError.put("length_stay", PAGE_OFFERED_10);
        _mError.put("location", PAGE_OFFERED_5);
        _mError.put("login_email_and_password", PAGE_OFFERED_19);
        _mError.put("new_or_existing_user", PAGE_OFFERED_19);
        _mError.put("password", PAGE_OFFERED_19);
        _mError.put("company_name", PAGE_OFFERED_4A);
        _mError.put("roomprice", PAGE_OFFERED_9);
    }

    @Override
    public String getNextFragmentName(String currentScreen, Object businessObject) {
        return PAGE_OFFERED_20;
    }

    public String nextErrorScreen(String cause) {
        String screenName = _mError.get(cause);
        if (screenName == null)
            return PAGE_OFFERED_19;
        else
            return _mError.get(cause);
    }

    @Override
    public Boolean isLastFragment(String screen) {
        return screen.equals(PAGE_OFFERED_20);
    }

}
