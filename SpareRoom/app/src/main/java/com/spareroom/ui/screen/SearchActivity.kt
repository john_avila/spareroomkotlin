package com.spareroom.ui.screen

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.spareroom.R
import com.spareroom.model.business.SearchType
import com.spareroom.viewmodel.SearchViewModel
import javax.inject.Inject

private const val SEARCH_TYPE = "search_type"

class SearchActivity : InjectableActivity() {
    private val viewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel::class.java) }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    companion object {
        @JvmStatic
        fun start(activity: Activity, searchType: SearchType) {
            val searchIntent = Intent(activity, SearchActivity::class.java)
            searchIntent.putExtra(SEARCH_TYPE, searchType)
            activity.startActivity(searchIntent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_activity)

        if (savedInstanceState == null)
            loadFragment()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == SearchFragment.SETTINGS_CHECK_REQUEST_CODE) {
            when (resultCode) {
                RESULT_OK -> viewModel.locationSettingsLiveData.successful()
                RESULT_CANCELED -> viewModel.locationSettingsLiveData.denied()
            }
        }
    }

    private fun loadFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        val searchFragment = SearchFragment.getInstance(intent.getSerializableExtra(SEARCH_TYPE) as SearchType)
        transaction.replace(R.id.searchCoordinatorLayout, searchFragment, SearchFragment.TAG)
        transaction.commit()
    }

}
