/**
 * Activities and fragments. This package will be moved to com.spareroom.ui after
 * changing Analytics tracking.
 */
package com.spareroom.ui.screen.legacy;