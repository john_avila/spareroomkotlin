package com.spareroom.ui.screen.legacy;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;
import com.spareroom.R;
import com.spareroom.ui.screen.Activity;

import androidx.fragment.app.FragmentTransaction;

public class AdOfferedMapActivity extends Activity implements OnMarkerClickListener {
    SupportMapFragment _mapFragment;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_ad_map);

        Intent intent = getIntent();

        _mapFragment = new AdOfferedBoardMapFragment();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        Bundle b = new Bundle();
        b.getString("newness", intent.getStringExtra("newness"));
        b.putFloat("latitude", intent.getFloatExtra("latitude", 0f));
        b.putFloat("longitude", intent.getFloatExtra("longitude", 0f));
        b.putString("neighbourhood", intent.getStringExtra("neighbourhood"));
        b.putString("postcode", intent.getStringExtra("postcode"));
        b.putString("accomType", intent.getStringExtra("accomType"));
        b.putString("price", intent.getStringExtra("price"));
        //b.putString("title", intent.getStringExtra("title"));

        _mapFragment.setArguments(b);

        setToolbar(intent.getStringExtra("title"));
        fragmentTransaction.add(R.id.activity_ad_map_fMap, _mapFragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return handleHomeButton(item);
    }

    @Override
    public boolean onMarkerClick(Marker arg0) {
        arg0.showInfoWindow();
        return true;
    }

}
