package com.spareroom.ui.screen.customtabs.feedback

import android.app.Activity
import android.content.Intent
import android.net.Uri

import com.spareroom.ui.screen.SendFeedbackActivity
import com.spareroom.ui.screen.customtabs.CustomTabFallback

class SendFeedbackWebViewFallback : CustomTabFallback {

    override fun openUri(activity: Activity, uri: Uri) {
        val intent = Intent(activity, SendFeedbackActivity::class.java)
        intent.putExtra(SendFeedbackActivity.INTENT_EXTRA_URL, uri.toString())
        activity.startActivity(intent)
    }
}