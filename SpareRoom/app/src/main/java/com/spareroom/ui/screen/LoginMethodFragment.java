package com.spareroom.ui.screen;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.*;
import android.widget.*;

import com.facebook.*;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.model.business.*;
import com.spareroom.model.extra.RegisterExtra;
import com.spareroom.ui.controller.ViewUiController;
import com.spareroom.ui.util.*;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import static com.spareroom.ui.screen.LoginActivity.REQUEST_CODE_REGISTER;
import static com.spareroom.ui.screen.RegisterActivity.*;

/**
 * login fragment for choose between sign in with facebook or email
 */
public class LoginMethodFragment extends Fragment {

    private ProcessCompositeUiController _processCompositeUiController;
    private CallbackManager _callbackManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        View _rootView = inflater.inflate(R.layout.material_login_method_fragment, container, false);
        // Facebook
        _callbackManager = CallbackManager.Factory.create();
        RelativeLayout rlFacebook = _rootView.findViewById(R.id.loginMethodFragment_facebookButton);
        TextView tvFbButton = rlFacebook.findViewById(R.id.facebookButton_text);
        // Email button
        TextView tvEmail = _rootView.findViewById(R.id.loginMethodFragment_emailButton);
        // Terms and Conditions
        TextView tvTerms = _rootView.findViewById(R.id.loginMethodFragment_terms);
        SpannableString terms, policy;
        ClickableInsideTextView clickableInsideTextView = new ClickableInsideTextView();
        // Bottom Toolbar
        View bottomToolbar = getActivity().findViewById(R.id.activity_toolbarBottom);
        // Controllers
        _processCompositeUiController =
                new ProcessCompositeUiController(
                        new EmailButtonUiController(tvEmail),
                        new FacebookButtonUiController(rlFacebook),
                        new TermsConditionsUiController(tvTerms));

        // Toolbar
        setToolbar(getActivity().findViewById(R.id.appBarLayout), getString(R.string.loginMethodFragment_toolbarTitle), true, R.drawable.ic_close);

        // Facebook
        SpareroomApplication.getInstance(getActivity().getApplicationContext()).initializeFacebook(getActivity().getApplication());
        LoginManager.getInstance().registerCallback(_callbackManager, new CustomFacebookCallBack());

        tvFbButton.setText(R.string.loginMethodFragment_loginWithFacebook);

        rlFacebook.setOnClickListener(view -> {
            LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("email", "user_birthday"));
            _processCompositeUiController.setLoading();
        });

        // Email button
        tvEmail.setOnClickListener(v -> ((LoginActivity) getActivity()).nextFragment());

        // Terms and Conditions
        terms = clickableInsideTextView.makeLinkSpan(
                String.format(" %s ", getString(R.string.loginMethodFragment_termsPrivacy_terms)),
                v -> WebLauncher.launchTerms(getActivity()));
        policy = clickableInsideTextView.makeLinkSpan(
                String.format(" %s", getString(R.string.loginMethodFragment_termsPrivacy_policy)),
                v -> WebLauncher.launchPrivacy(getActivity()));
        tvTerms.setText(getString(R.string.loginMethodFragment_termsPrivacy));
        tvTerms.append(terms);
        tvTerms.append(getString(R.string.loginMethodFragment_termsPrivacy_and));
        tvTerms.append(policy);
        tvTerms.append(getString(R.string.loginMethodFragment_termsPrivacy_dot));
        tvTerms.setHighlightColor(Color.TRANSPARENT); // Keeps the background color when click

        clickableInsideTextView.makeLinksFocusable(tvTerms);

        // Bottom Toolbar
        bottomToolbar.setVisibility(View.GONE);

        _processCompositeUiController.enable();

        return _rootView;
    } // end onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            _callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class CustomFacebookCallBack implements FacebookCallback<LoginResult> {

        @Override
        public void onSuccess(LoginResult loginResult) {
            GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new CustomGraphRequest());
            Bundle parameters = new Bundle();

            parameters.putString("fields", "first_name,last_name,email,birthday");

            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            _processCompositeUiController.enable();
        }

        @Override
        public void onError(FacebookException error) {
            _processCompositeUiController.enable();
            SnackBarUtils.showException(LoginMethodFragment.this.getView(), error);
        }

    }

    private class CustomGraphRequest implements GraphRequest.GraphJSONObjectCallback {
        private final ControllerActionObserver _controllerActionObserver = new ControllerActionObserver() {

            @Override
            public void onResult(Object o) {
                if (!UiUtils.isFragmentAlive(LoginMethodFragment.this))
                    return;

                RegisterWithFacebookResult registerWithFacebookResult = (RegisterWithFacebookResult) o;

                if (registerWithFacebookResult.is_requireCustomPassword()) {
                    Intent registerStep2 = new Intent(getActivity(), RegisterActivity.class);
                    registerStep2.putExtra(RegisterActivity.INTENT_KEY_REGISTER_METHOD, INTENT_VALUE_REGISTER_METHOD_FACEBOOK);
                    registerStep2.putExtra(INTENT_KEY_REGISTER_STEP, INTENT_VALUE_REGISTER_STEP_2);

                    // Send analytics
                    AnalyticsTrackerComposite.getInstance().trackEvent_Account_FacebookLoginToRegisterRedirection();

                    AccountRegisterFormFacebook accountCredentialsFacebook =
                            (AccountRegisterFormFacebook) ((LinearMultiStepActivity) getActivity()).getSharedObject(RegisterActivity.KEY_REGISTER_FORM);
                    registerStep2.putExtra(RegisterActivity.INTENT_KEY_REGISTER_FACEBOOK_DATA_EMAIL, accountCredentialsFacebook.getEmail());
                    registerStep2.putExtra(RegisterActivity.INTENT_KEY_REGISTER_FACEBOOK_DATA_FIRSTNAME, accountCredentialsFacebook.getFirstName());
                    registerStep2.putExtra(RegisterActivity.INTENT_KEY_REGISTER_FACEBOOK_DATA_LASTNAME, accountCredentialsFacebook.getLastName());
                    getActivity().startActivityForResult(registerStep2, REQUEST_CODE_REGISTER);

                } else {
                    ToastUtils.showToast(R.string.signed_in);
                    ((LoginActivity) getActivity()).setActivityResultOK();

                    // Updates Push Notifications
                    FirebaseIdController.register(SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext());

                    // Send analytics
                    AnalyticsTrackerComposite.getInstance().trackEvent_Account_Login_Facebook();

                    getActivity().finish();
                }

                _processCompositeUiController.enable();

            }

            @Override
            public void onSpareroomStatus(SpareroomStatus status) {
                _processCompositeUiController.enable();

            }

            @Override
            public void onException(Exception exception) {
                _processCompositeUiController.enable();
                SnackBarUtils.showException(LoginMethodFragment.this.getView(), exception);

            }

        };

        @Override
        public void onCompleted(JSONObject object, GraphResponse response) {
            if (!UiUtils.isFragmentAlive(LoginMethodFragment.this))
                return;

            FacebookRequestError facebookRequestError = response.getError();

            if (facebookRequestError == null) {
                AccountRegisterFormFacebook accountCredentialsFacebook = new AccountRegisterFormFacebook();
                accountCredentialsFacebook.setInAgreement(true);
                accountCredentialsFacebook.setFacebookAccessToken(response.getRequest().getAccessToken().getToken());
                accountCredentialsFacebook.setFacebookTokenExpires(String.valueOf(response.getRequest().getAccessToken().getExpires().getTime()));

                try {
                    accountCredentialsFacebook.setEmail(object.getString("email"));
                } catch (JSONException je) {
                    // If retrieving the email is not possible we can leave it blank.
                    // The email field may be empty if someone signed up for Facebook with a phone
                    // number instead of an email address.
                }
                try {
                    accountCredentialsFacebook.setFirstName(object.getString("first_name"));
                } catch (JSONException je) {
                    // If retrieving it is not possible we can leave it blank, it is only
                    // needed, probably, for statistics purposes.
                }
                try {
                    accountCredentialsFacebook.setLastName(object.getString("last_name"));
                } catch (JSONException je) {
                    // If retrieving it is not possible we can leave it blank, it is only
                    // needed, probably, for statistics purposes.
                }
                try {
                    accountCredentialsFacebook.setFacebookUserId(object.getString("id"));
                } catch (JSONException je) {
                    // If retrieving it is not possible we can leave it blank, it is not needed
                }

                RegisterExtra registerExtra = new RegisterExtra();
                registerExtra.setPageJoinedFrom(((LoginActivity) getActivity()).getPageJoinedFrom());
                registerExtra.setVersion(AppVersion.appId());

                RegisterFacebookStep1ControllerAction registerFacebookStep1ControllerAction =
                        new RegisterFacebookStep1ControllerAction(
                                SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext());
                registerFacebookStep1ControllerAction.addControllerActionObserver(_controllerActionObserver);
                addObserver(registerFacebookStep1ControllerAction);
                registerFacebookStep1ControllerAction.execute(accountCredentialsFacebook, registerExtra);

                ((LinearMultiStepActivity) getActivity()).setSharedObject(RegisterActivity.KEY_REGISTER_FORM, accountCredentialsFacebook);

                // Log out the user from Facebook
                LoginManager.getInstance().logOut();

            }

        }

    }

    private class ProcessCompositeUiController implements ViewUiController {
        private final ViewUiController _emailViewUiController;
        private final ViewUiController _facebookViewUiController;
        private final ViewUiController _termsConditionsUiController;

        ProcessCompositeUiController(
                ViewUiController emailViewUiController,
                ViewUiController facebookViewUiController,
                ViewUiController termsConditionsUiController) {

            _emailViewUiController = emailViewUiController;
            _facebookViewUiController = facebookViewUiController;
            _termsConditionsUiController = termsConditionsUiController;

        }

        @Override
        public void enable() {
            _emailViewUiController.enable();
            _facebookViewUiController.enable();
            _termsConditionsUiController.enable();
        }

        @Override
        public void disable() {
        }

        @Override
        public void setLoading() {
            _emailViewUiController.disable();
            _facebookViewUiController.setLoading();
            _termsConditionsUiController.setLoading();
        }

        @Override
        public void setStatus(SpareroomStatus status) {
        }

        @Override
        public void clearStatus() {
        }

    }

    private class EmailButtonUiController implements ViewUiController {
        private final TextView _tvEmailButton;

        EmailButtonUiController(TextView tvEmailButton) {
            _tvEmailButton = tvEmailButton;

        }

        @Override
        public void enable() {
            _tvEmailButton.setEnabled(true);
            _tvEmailButton.setTextColor(ContextCompat.getColor(_tvEmailButton.getContext(), R.color.white));

        }

        @Override
        public void disable() {
            _tvEmailButton.setEnabled(false);
            _tvEmailButton.setTextColor(ContextCompat.getColor(_tvEmailButton.getContext(), R.color.light_blue));
        }

        @Override
        public void setLoading() {
            disable();
        }

        @Override
        public void setStatus(SpareroomStatus status) {
        }

        @Override
        public void clearStatus() {
        }

    }

    private class FacebookButtonUiController implements ViewUiController {
        private final RelativeLayout _rlFacebookButton;
        private final TextView _tvText;
        private final ImageView _ivIcon;
        private final ProgressBar _pbLoading;

        FacebookButtonUiController(RelativeLayout rlFacebookButton) {
            _rlFacebookButton = rlFacebookButton;
            _pbLoading = rlFacebookButton.findViewById(R.id.facebookButton_progressBar);
            _ivIcon = rlFacebookButton.findViewById(R.id.facebookButton_icon);
            _tvText = rlFacebookButton.findViewById(R.id.facebookButton_text);

        }

        @Override
        public void enable() {
            _rlFacebookButton.setEnabled(true);
            _pbLoading.setVisibility(View.GONE);
            _ivIcon.setVisibility(View.VISIBLE);
            _tvText.setVisibility(View.VISIBLE);
        }

        @Override
        public void disable() {
        }

        @Override
        public void setLoading() {
            _rlFacebookButton.setEnabled(false);
            _pbLoading.setVisibility(View.VISIBLE);
            _ivIcon.setVisibility(View.INVISIBLE);
            _tvText.setVisibility(View.INVISIBLE);
        }

        @Override
        public void setStatus(SpareroomStatus status) {
        }

        @Override
        public void clearStatus() {
        }

    }

    private class TermsConditionsUiController implements ViewUiController {
        private final TextView _tvTerms;

        private TermsConditionsUiController(TextView tvTerms) {
            _tvTerms = tvTerms;
        }

        @Override
        public void enable() {
            _tvTerms.setEnabled(true);
        }

        @Override
        public void disable() {
            _tvTerms.setEnabled(false);
        }

        @Override
        public void setLoading() {
            disable();
        }

        @Override
        public void setStatus(SpareroomStatus status) {
            enable();
        }

        @Override
        public void clearStatus() {
            enable();
        }

    }

}
