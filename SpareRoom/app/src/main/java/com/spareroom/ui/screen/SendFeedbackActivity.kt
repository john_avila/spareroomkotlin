package com.spareroom.ui.screen

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.spareroom.R
import com.spareroom.ui.screen.customtabs.ProgressWebChromeClient
import com.spareroom.ui.util.ToastUtils
import kotlinx.android.synthetic.main.web_view_activity.*

private const val EXIT_URL = "https://www.surveymonkey.co.uk/r/spareroom/exit-from-feedback-survey"

class SendFeedbackActivity : Activity() {

    companion object {
        const val INTENT_EXTRA_URL = "extra.url"
    }

    private val webView by lazy { WebView(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.web_view_activity)
        setToolbar(getString(R.string.send_feedback), true, R.drawable.ic_close)
        setUpWebView()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return handleHomeButton(item)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setUpWebView() {
        webViewContainer.addView(webView)

        webView.webChromeClient = ProgressWebChromeClient(webViewProgressBar)
        webView.webViewClient = OverrideUrlWebViewClient(this)
        webView.settings.javaScriptEnabled = true

        webView.loadUrl(intent.getStringExtra(INTENT_EXTRA_URL))
    }

    private class OverrideUrlWebViewClient(private val activity: Activity) : WebViewClient() {

        @Suppress("OverridingDeprecatedMember")
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            return handleUrl(url)
        }

        @TargetApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            return handleUrl(request?.url?.toString())
        }

        private fun handleUrl(url: String?): Boolean {
            return if (url?.startsWith(EXIT_URL) == true && !activity.isActivityDestroyed) {
                ToastUtils.showToast(R.string.feedback_received)
                activity.finish()
                true
            } else {
                false
            }
        }
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
            return
        }

        super.onBackPressed()
    }
}