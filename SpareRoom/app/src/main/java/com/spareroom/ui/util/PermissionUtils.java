package com.spareroom.ui.util;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;

import com.spareroom.ui.screen.Activity;
import com.spareroom.ui.screen.Fragment;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class PermissionUtils {

    private static final String[] CAMERA_PERMISSIONS = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private static final String[] LOCATION_PERMISSIONS = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    private static final String[] VIDEO_PERMISSIONS = new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static final String[] STORAGE_PERMISSION = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static boolean hasCameraPermissions(Context context) {
        int storagePermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraPermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);

        return storagePermissionCheck == PackageManager.PERMISSION_GRANTED && cameraPermissionCheck == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasStoragePermissions(Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasRecordVideoPermissions(Context context) {
        int cameraPermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        int audioPermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO);
        int storagePermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        return storagePermissionCheck == PackageManager.PERMISSION_GRANTED && cameraPermissionCheck == PackageManager.PERMISSION_GRANTED
                && audioPermissionCheck == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasLocationPermissions(Context context) {
        int fineLocationPermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        int coarseLocationPermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION);

        return fineLocationPermissionCheck == PackageManager.PERMISSION_GRANTED && coarseLocationPermissionCheck == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestVideoPermissions(Activity activity, int requestCode) {
        ActivityCompat.requestPermissions(activity, VIDEO_PERMISSIONS, requestCode);
    }

    public static void requestCameraPermissions(Activity activity, int requestCode) {
        ActivityCompat.requestPermissions(activity, CAMERA_PERMISSIONS, requestCode);
    }

    public static void requestCameraPermissions(Fragment fragment, int requestCode) {
        fragment.requestPermissions(CAMERA_PERMISSIONS, requestCode);
    }

    public static void requestStoragePermissions(Activity activity, int requestCode) {
        ActivityCompat.requestPermissions(activity, STORAGE_PERMISSION, requestCode);
    }

    public static void requestLocationPermissions(Activity activity, int requestCode) {
        ActivityCompat.requestPermissions(activity, LOCATION_PERMISSIONS, requestCode);
    }

    public static void requestLocationPermissions(Fragment fragment, int requestCode) {
        fragment.requestPermissions(LOCATION_PERMISSIONS, requestCode);
    }

    public static boolean havePermissionsBeenGranted(@NonNull String[] permissions, int[] grantResults) {
        if (grantResults.length == 0 || permissions.length == 0)
            return false;

        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED)
                return false;
        }
        return true;
    }

    public static void openAppSettings(@NonNull Fragment fragment, @NonNull String packageName) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.fromParts("package", packageName, null));
        if (Launcher.canHandleIntent(fragment.getBaseActivity(), intent))
            fragment.startActivity(intent);
    }

}
