package com.spareroom.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import androidx.recyclerview.widget.RecyclerView
import com.spareroom.R
import com.spareroom.integration.imageloader.ImageLoader
import com.spareroom.model.business.AbstractAd
import com.spareroom.model.business.AdvertItem
import com.spareroom.ui.screen.Fragment
import com.spareroom.ui.util.AdvertUtils
import com.spareroom.ui.util.DateUtils
import com.spareroom.ui.widget.AdvertCardView
import javax.inject.Inject

class MyAdvertsAdapter @Inject constructor(
    private val imageLoader: ImageLoader,
    private val dateUtils: DateUtils,
    private val advertUtils: AdvertUtils,
    private val animation: Animation
) : RecyclerViewAdapter<AdvertItem, RecyclerView.ViewHolder>() {

    private lateinit var fragment: Fragment
    private lateinit var advertClickListener: View.OnClickListener

    private val active by lazy { context.getString(R.string.active) }
    private val pending by lazy { context.getString(R.string.pending) }
    private val deactivated by lazy { context.getString(R.string.deactivated) }

    fun init(context: Context, fragment: Fragment, advertClickListener: View.OnClickListener) {
        super.context = context
        this.fragment = fragment
        this.advertClickListener = advertClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AdvertViewHolder(inflater.inflate(R.layout.my_adverts_advert_card_view_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is AdvertViewHolder -> bindAdvertViewHolder(position, holder)
        }
    }

    private fun bindAdvertViewHolder(position: Int, holder: AdvertViewHolder) {
        val advertItem = getItem(position)
        val showHeader = advertItem.header
        val showDivider = advertItem.divider
        holder.advertCardView.bind(advertItem.advert, advertUtils, dateUtils, advertClickListener, imageLoader, fragment, animation,
            showHeader, status(advertItem.advert), showDivider, false)
    }

    private fun status(advert: AbstractAd): String {
        return when {
            advert.isLive -> active
            advert.isPending -> pending
            else -> deactivated
        }
    }

    private class AdvertViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val advertCardView: AdvertCardView = itemView as AdvertCardView
    }

}