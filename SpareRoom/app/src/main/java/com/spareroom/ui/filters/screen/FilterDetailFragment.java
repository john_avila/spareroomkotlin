package com.spareroom.ui.filters.screen;

import android.os.Bundle;
import android.view.*;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListProperties.MaxTerm;
import com.spareroom.model.business.SearchAdvertListProperties.MinTerm;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.MaxFlatmates;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.MinFlatmates;
import com.spareroom.ui.filters.adapter.FiltersRecyclerAdapter;
import com.spareroom.ui.filters.adapter.viewparams.*;
import com.spareroom.ui.filters.provider.*;
import com.spareroom.ui.screen.DateDialog;
import com.spareroom.ui.screen.Fragment;
import com.spareroom.ui.util.KeyboardUtils;
import com.spareroom.ui.widget.params.CompoundButtonParams;
import com.spareroom.ui.widget.params.ViewParamsType;
import com.spareroom.viewmodel.FilterViewModel;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.spareroom.ui.filters.adapter.viewparams.AbstractSummaryViewParams.SummaryAction.SHOW_DATE_DIALOG;
import static com.spareroom.ui.filters.adapter.viewparams.AbstractSummaryViewParams.SummaryAction.SHOW_SCROLL_DIALOG;
import static com.spareroom.ui.widget.params.ViewParamsType.SUMMARY;

public class FilterDetailFragment extends Fragment implements Injectable {
    private static final String SPECIFIC_FILTER_NAME = "specific_filter_name";
    private static final String SEARCH_ADVERT_LIST_PROPERTIES_KEY = "search_advert_list_properties_key";

    private RecyclerView recyclerView;
    private FilterViewModel viewModel;
    private FiltersRecyclerAdapter adapter;
    private SearchAdvertListProperties currentSearchProperties;
    private AbstractItemProvider itemProvider;

    @Inject
    SubtitleUtil subtitleUtil;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    static FilterDetailFragment newInstance(SearchAdvertListProperties properties, String filterName) {
        Bundle fragmentArguments = new Bundle();
        fragmentArguments.putString(SPECIFIC_FILTER_NAME, filterName);
        fragmentArguments.putSerializable(SEARCH_ADVERT_LIST_PROPERTIES_KEY, properties);

        FilterDetailFragment fragment = new FilterDetailFragment();
        fragment.setArguments(fragmentArguments);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.material_filters_specific_layout, container, false);
        viewModel = ViewModelProviders.of(getBaseActivity(), viewModelFactory).get(FilterViewModel.class);

        initItemProvider(savedInstanceState != null ? savedInstanceState : getArguments());
        initToolbar();
        initButtons(view);
        setRecyclerView(view);
        setUpLiveDataObservers();

        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        itemProvider.updateProperties(currentSearchProperties, false);
        outState.putSerializable(SEARCH_ADVERT_LIST_PROPERTIES_KEY, currentSearchProperties);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        KeyboardUtils.hideKeyboard(recyclerView);
    }

    private void initItemProvider(Bundle args) {
        String filterName = getFilterName();
        currentSearchProperties = (SearchAdvertListProperties) args.getSerializable(SEARCH_ADVERT_LIST_PROPERTIES_KEY);
        itemProvider = FilterItemProviderFactory.getItemProvider(filterName);
        itemProvider.createNewList(currentSearchProperties, filterName);
    }

    @NonNull
    private String getFilterName() {
        assert getArguments() != null;
        String filterName = getArguments().getString(SPECIFIC_FILTER_NAME);
        assert filterName != null;
        return filterName;
    }

    private void initToolbar() {
        setTitle(getString(itemProvider.titleId()));
        setHomeAsUpIndicator(R.drawable.ic_back);
    }

    private void initButtons(View parent) {
        TextView textViewReset = parent.findViewById(R.id.filtersSpecific_reset);
        textViewReset.setOnClickListener(v -> {
            itemProvider.reset(itemProvider.get());
            adapter.resetAdapter(itemProvider.get());
        });

        TextView textViewApply = parent.findViewById(R.id.filtersSpecific_apply);
        textViewApply.setOnClickListener(v -> {
            itemProvider.updateProperties(currentSearchProperties, true);
            viewModel.searchProperties(currentSearchProperties);
            viewModel.shallPerformMatchesSearch(true);
            getBaseActivity().onBackPressed();
        });

    }

    private void setRecyclerView(View parent) {
        recyclerView = parent.findViewById(R.id.filtersSpecific_recycler);
        adapter =
                new FiltersRecyclerAdapter(
                        getBaseActivity(),
                        itemProvider.get(),
                        this::summaryParamsListener,
                        this::compoundButtonOnClickListener,
                        this::compoundButtonGroupOnClickListener);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
    }

    private void setUpLiveDataObservers() {
        viewModel.filterAvailabilityDate().observe(this, filterAvailabilityDateEvent -> {
            for (FilterItem item : itemProvider.get()) {
                if (SUMMARY.equals(item.type())) {
                    AbstractSummaryViewParams summaryParams = (AbstractSummaryViewParams) item.params();

                    if (SHOW_DATE_DIALOG.equals(summaryParams.summaryAction())) {
                        assert filterAvailabilityDateEvent != null;
                        summaryParams.subtitleString(subtitleUtil.getMoveInText(filterAvailabilityDateEvent.date()));
                        ((SummaryViewDateParams) summaryParams).date(filterAvailabilityDateEvent.date());
                        ((FiltersRecyclerAdapter) recyclerView.getAdapter()).updateAdapter(filterAvailabilityDateEvent.position());
                        break;
                    }
                }
            }
        });

        viewModel.filterAvailabilityMinTerm().observe(this, filterAvailabilityMinTerm -> {
            for (FilterItem item : itemProvider.get()) {
                if (SUMMARY.equals(item.type())) {
                    AbstractSummaryViewParams summaryParams = (AbstractSummaryViewParams) item.params();

                    if (SHOW_SCROLL_DIALOG.equals(summaryParams.summaryAction())) {
                        if (summaryParams instanceof SummaryViewMinTermParams) {
                            assert filterAvailabilityMinTerm != null;
                            summaryParams.subtitleString(subtitleUtil.getTermText(getBaseActivity(), filterAvailabilityMinTerm.minTerm().getIntValue()));
                            ((SummaryViewMinTermParams) summaryParams).minTerm(filterAvailabilityMinTerm.minTerm());
                            ((FiltersRecyclerAdapter) recyclerView.getAdapter()).updateAdapter(filterAvailabilityMinTerm.position());
                            break;
                        }
                    }
                }
            }
        });

        viewModel.filterAvailabilityMaxTerm().observe(this, filterAvailabilityMaxTerm -> {
            for (FilterItem item : itemProvider.get()) {
                if (SUMMARY.equals(item.type())) {
                    AbstractSummaryViewParams summaryParams = (AbstractSummaryViewParams) item.params();

                    if (SHOW_SCROLL_DIALOG.equals(summaryParams.summaryAction())) {
                        if (summaryParams instanceof SummaryViewMaxTermParams) {
                            assert filterAvailabilityMaxTerm != null;
                            summaryParams.subtitleString(subtitleUtil.getTermText(getBaseActivity(), filterAvailabilityMaxTerm.maxTerm().getIntValue()));
                            ((SummaryViewMaxTermParams) summaryParams).maxTerm(filterAvailabilityMaxTerm.maxTerm());
                            ((FiltersRecyclerAdapter) recyclerView.getAdapter()).updateAdapter(filterAvailabilityMaxTerm.position());
                            break;
                        }
                    }
                }
            }
        });

        viewModel.filterAvailabilityMinFlatmates().observe(this, filterAvailabilityMinFlatmates -> {
            for (FilterItem item : itemProvider.get()) {
                if (SUMMARY.equals(item.type())) {
                    AbstractSummaryViewParams summaryParams = (AbstractSummaryViewParams) item.params();

                    if (SHOW_SCROLL_DIALOG.equals(summaryParams.summaryAction())) {
                        if (summaryParams instanceof SummaryViewMinFlatmatesParams) {
                            assert filterAvailabilityMinFlatmates != null;
                            summaryParams.subtitleString(subtitleUtil.getFlatmatesText(getContext(), filterAvailabilityMinFlatmates.minFlatmates().getValue()));
                            ((SummaryViewMinFlatmatesParams) summaryParams).minFlatmates(filterAvailabilityMinFlatmates.minFlatmates());
                            ((FiltersRecyclerAdapter) recyclerView.getAdapter()).updateAdapter(filterAvailabilityMinFlatmates.position());
                            break;
                        }
                    }
                }
            }
        });

        viewModel.filterAvailabilityMaxFlatmates().observe(this, filterAvailabilityMaxFlatmates -> {
            for (FilterItem item : itemProvider.get()) {
                if (SUMMARY.equals(item.type())) {
                    AbstractSummaryViewParams summaryParams = (AbstractSummaryViewParams) item.params();

                    if (SHOW_SCROLL_DIALOG.equals(summaryParams.summaryAction())) {
                        if (summaryParams instanceof SummaryViewMaxFlatmatesParams) {
                            assert filterAvailabilityMaxFlatmates != null;
                            summaryParams.subtitleString(subtitleUtil.getFlatmatesText(getContext(), filterAvailabilityMaxFlatmates.maxFlatmates().getValue()));
                            ((SummaryViewMaxFlatmatesParams) summaryParams).maxFlatmates(filterAvailabilityMaxFlatmates.maxFlatmates());
                            ((FiltersRecyclerAdapter) recyclerView.getAdapter()).updateAdapter(filterAvailabilityMaxFlatmates.position());
                            break;
                        }
                    }
                }
            }
        });

    }

    private void summaryParamsListener(View summaryParamsView) {
        String summaryName = (String) summaryParamsView.getTag(R.id.id_tag);
        AbstractSummaryViewParams summaryParams = (AbstractSummaryViewParams) summaryParamsView.getTag(R.id.param_tag);
        int itemPosition = (int) summaryParamsView.getTag(R.id.position_tag);

        switch (summaryName) {
            case AbstractItemProvider.LENGTH_OF_STAY_MIN:
                minTermOnClickListener(summaryParams, itemPosition);
                break;
            case AbstractItemProvider.LENGTH_OF_STAY_MAX:
                maxTermOnClickListener(summaryParams, itemPosition);
                break;
            case AbstractItemProvider.NUMBER_OF_FLATMATES_MIN:
                minFlatmatesOnClickListener(summaryParams, itemPosition);
                break;
            case AbstractItemProvider.NUMBER_OF_FLATMATES_MAX:
                maxFlatmatesOnClickListener(summaryParams, itemPosition);
                break;
            case AbstractItemProvider.MOVE_IN:
                datePickerOnClickListener(summaryParams, itemPosition);
                break;

        }
    }

    private void compoundButtonOnClickListener(View view) {
        int position = (int) view.getTag(R.id.position_tag);
        CompoundButtonParams params = (CompoundButtonParams) itemProvider.get().get(position).params();
        params.selected(!params.selected());
        adapter.updateAdapter(position);
    }

    private void compoundButtonGroupOnClickListener(View view) {
        String groupName = view.getTag(R.id.group_tag).toString();
        String filterTag = view.getTag(R.id.id_tag).toString();
        List<GroupedCompoundButtonParams> groupItems = itemProvider.groupList().get(groupName);

        for (GroupedCompoundButtonParams params : groupItems) {
            params.selected(filterTag.equals(params.tag()));
        }

        List<FilterItem> itemList = itemProvider.get();
        for (int i = 0; i < itemList.size(); i++) {
            FilterItem item = itemList.get(i);
            if (item.type() == ViewParamsType.GROUPED_COMPOUND) {
                if (groupName.equals(((GroupedCompoundButtonParams) item.params()).groupName())) {
                    adapter.updateListItems(i, groupItems.size());
                    break;
                }
            }
        }

    }

    private void minTermOnClickListener(AbstractSummaryViewParams summaryParams, int itemPosition) {
        MinTerm minTerm = ((SummaryViewMinTermParams) summaryParams).minTerm();
        int previousSelection = minTerm != null ? minTerm.ordinal() : MinTerm.NOT_SET.ordinal();
        MinTermDialog minTermDialog = MinTermDialog.newInstance(itemPosition, previousSelection);
        minTermDialog.show(this, MinTermDialog.TAG);
    }

    private void maxTermOnClickListener(AbstractSummaryViewParams summaryParams, int itemPosition) {
        MaxTerm maxTerm = ((SummaryViewMaxTermParams) summaryParams).maxTerm();
        int previousSelection = maxTerm != null ? maxTerm.ordinal() : MaxTerm.NOT_SET.ordinal();
        MaxTermDialog maxTermDialog = MaxTermDialog.newInstance(itemPosition, previousSelection);
        maxTermDialog.show(this, MaxTermDialog.TAG);
    }

    private void minFlatmatesOnClickListener(AbstractSummaryViewParams summaryParams, int itemPosition) {
        MinFlatmates minFlatmates = ((SummaryViewMinFlatmatesParams) summaryParams).minFlatmates();
        int previousSelection = minFlatmates != null ? minFlatmates.ordinal() : MinFlatmates.NOT_SET.ordinal();
        MinFlatmatesDialog minFlatmatesDialog = MinFlatmatesDialog.newInstance(itemPosition, previousSelection);
        minFlatmatesDialog.show(this, MinFlatmatesDialog.TAG);
    }

    private void maxFlatmatesOnClickListener(AbstractSummaryViewParams summaryParams, int itemPosition) {
        MaxFlatmates maxFlatmates = ((SummaryViewMaxFlatmatesParams) summaryParams).maxFlatmates();
        int previousSelection = maxFlatmates != null ? maxFlatmates.ordinal() : MaxTerm.NOT_SET.ordinal();
        MaxFlatmatesDialog maxFlatmatesDialog = MaxFlatmatesDialog.newInstance(itemPosition, previousSelection);
        maxFlatmatesDialog.show(this, MaxFlatmatesDialog.TAG);
    }

    private void datePickerOnClickListener(AbstractSummaryViewParams summaryParams, int itemPosition) {
        DateDialog datePicker = DateDialog.getInstance(itemPosition, ((SummaryViewDateParams) summaryParams).date());
        datePicker.show(this, DateDialog.TAG);
    }

}
