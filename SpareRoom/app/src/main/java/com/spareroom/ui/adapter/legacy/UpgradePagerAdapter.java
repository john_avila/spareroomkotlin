package com.spareroom.ui.adapter.legacy;

import android.os.Bundle;

import com.spareroom.controller.SpareroomApplication;
import com.spareroom.model.business.*;
import com.spareroom.ui.screen.Fragment;
import com.spareroom.ui.screen.legacy.UpgradeItemFragment;
import com.spareroom.ui.screen.legacy.UpgradeSummaryFragment;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class UpgradePagerAdapter extends FragmentPagerAdapter {
    private UpgradeSummaryFragment _summaryFragment;
    private UpgradeList _upgradeList;
    private final List<String> titles;
    private SpareroomApplication _application;
    private String _contextId;

    public UpgradePagerAdapter(FragmentManager fm,
                               UpgradeList ul,
                               SpareroomApplication appContext,
                               String contextId,
                               List<String> titles) {
        super(fm);
        _upgradeList = ul;
        this.titles = titles;
        _summaryFragment = new UpgradeSummaryFragment();
        _application = appContext;
        _contextId = contextId;
    }

    public void updateSummaryFragment() {
        _application.getSpareroomContext().loadUserSession();
        Session session = _application.getSpareroomContext().getSession();
        _summaryFragment.update(session);
    }

    private Fragment getSummaryFragment() {
        _summaryFragment.update(_application.getSpareroomContext().loadUserSession());
        return _summaryFragment;
    }

    @Override
    public Fragment getItem(int arg0) {
        if (arg0 == 0)
            return getSummaryFragment();

        Upgrade u = _upgradeList.get(arg0 - 1);

        Bundle b = new Bundle();
        b.putString("header", u.get_title());
        b.putString("price", u.get_price());
        b.putString("code", u.get_code());
        b.putSerializable("features", u.get_features());
        b.putString("contextId", _contextId);
        b.putInt("position", arg0);
        b.putSerializable("upgrade", u);

        Fragment sf = new UpgradeItemFragment();
        sf.setArguments(b);

        return sf;
    }

    @Override
    public int getCount() {
        return _upgradeList.getSize() + 1; // upgrade list size + summary
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }
}