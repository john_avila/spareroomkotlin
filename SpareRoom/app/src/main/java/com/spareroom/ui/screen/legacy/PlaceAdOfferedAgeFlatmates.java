package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.widget.*;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.spareroom.R;
import com.spareroom.lib.core.Pair;
import com.spareroom.lib.util.NumberUtil;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.ui.widget.RangeSeekBar;

import java.util.Locale;

/**
 * Age of the current flatmates
 */
public class PlaceAdOfferedAgeFlatmates extends PlaceAdFragment {

    //region FIELDS UI

    private TextView _tvMin;
    private TextView _tvMax;
    private RangeSeekBar rsbAge;
    private SeekBar sbAge;
    private Button bConfirm;

    //endregion FIELDS UI

    //region FIELDS CONTROLLER

    private DraftAdOffered _draft;

    //endregion FIELDS CONTROLLER

    //region METHODS LIFECYCLE

    @SuppressWarnings("unchecked")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.place_ad_offered_age_flatmates, container, false);
        rsbAge = rootView.findViewById(R.id.place_ad_offered_age_flatmates_rsbAge);
        sbAge = rootView.findViewById(R.id.place_ad_offered_age_flatmates_sbAge);
        bConfirm = rootView.findViewById(R.id.place_ad_bConfirm);
        int numGuys;
        int numGirls;

        _draft = (DraftAdOffered) getDraftAd();

        // In case there is an activity state change and we lost the draft
        if (_draft == null) {
            getActivity().finish();
            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();
            return null;
        }

        numGuys = _draft.get_numExistingGuys();
        numGirls = _draft.get_numExistingGirls();

        final int minAge = NumberUtil.MIN_AGE, maxAge = 65;

        _tvMin = rootView.findViewById(R.id.place_ad_offered_age_flatmates_tvMin);
        _tvMax = rootView.findViewById(R.id.place_ad_offered_age_flatmates_tvMax);

        if (_draft.get_advertiserType() == DraftAdOffered.AdvertiserType.LIVE_IN_LANDLORD &&
                ((_draft.get_numExistingGirls() + _draft.get_numExistingGuys()) == 1)) {
            setTitle(getString(R.string.place_ad_offered_age_landlord));
        } else {
            setTitle(getString(R.string.place_ad_offered_age_flatmates));
        }

        if (_draft.get_rangeExistingAges() != null) {
            _tvMin.setText(String.format(Locale.getDefault(), "%d", _draft.get_rangeExistingAges().getKey()));
            _tvMax.setText(String.format(Locale.getDefault(), "%d", _draft.get_rangeExistingAges().getValue()));
            rsbAge.setSelectedMinValue(_draft.get_rangeExistingAges().getKey());
            rsbAge.setSelectedMaxValue(_draft.get_rangeExistingAges().getValue());
        } else {
            rsbAge.setSelectedMinValue(minAge);
            rsbAge.setSelectedMaxValue(maxAge);
        }

        rsbAge.setNotifyWhileDragging(true);
        rsbAge.setOnRangeSeekBarChangeListener((bar, minValue, maxValue) -> {
            _tvMin.setText(String.format(Locale.getDefault(), "%d", minValue));
            _tvMax.setText(String.format(Locale.getDefault(), "%d", maxValue));
        });

        if (_draft.get_rangeExistingAges() != null)
            sbAge.setProgress(_draft.get_rangeExistingAges().getKey() - minAge);
        else
            sbAge.setProgress(0);

        sbAge.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _tvMin.setText(String.format(Locale.getDefault(), "%d", minAge + progress));
                _tvMax.setText(String.format(Locale.getDefault(), "%d", minAge + progress));
            }

        });

        if ((numGuys + numGirls) == 1) {
            rsbAge.setVisibility(View.GONE);
            _tvMax.setVisibility(View.GONE);
            rootView.findViewById(R.id.place_ad_offered_age_flatmates_tvTo).setVisibility(View.GONE);
        } else {
            sbAge.setVisibility(View.GONE);
        }

        bConfirm.setOnClickListener(v -> {
            _draft.set_rangeExistingAges(
                    new Pair<>(Integer.valueOf(_tvMin.getText().toString()), Integer.valueOf(_tvMax.getText().toString())));
            finish();
        });

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rsbAge.setOnRangeSeekBarChangeListener(null);
        sbAge.setOnSeekBarChangeListener(null);
        bConfirm.setOnClickListener(null);
    }

    //region METHODS LIFECYCLE

}
