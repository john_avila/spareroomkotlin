package com.spareroom.ui.util

import androidx.annotation.WorkerThread
import androidx.recyclerview.widget.DiffUtil
import com.spareroom.model.business.AdvertItem

object AdvertsDiffUtil {

    @WorkerThread
    fun diff(oldAdverts: List<AdvertItem>, newAdverts: List<AdvertItem>): DiffUtil.DiffResult {

        return DiffUtil.calculateDiff(object : DiffUtil.Callback() {

            override fun getOldListSize() = oldAdverts.size

            override fun getNewListSize() = newAdverts.size

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldAdvert = oldAdverts[oldItemPosition]
                val newAdvert = newAdverts[newItemPosition]

                return oldAdvert.advert.id == newAdvert.advert.id
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldAdvert = oldAdverts[oldItemPosition]
                val newAdvert = newAdverts[newItemPosition]

                return oldAdvert == newAdvert
            }
        })
    }

}
