package com.spareroom.ui;

/**
 * Created by manuel on 04/11/2016.
 */

public class NameFieldValidator implements FieldValidator {
    public static final int VALID = 0;
    public static final int EMPTY = 1;
    public static final int TOO_FEW_WORDS = 2;
    public static final int FIRST_NAME_IS_TOO_SHORT = 3;

    @Override
    public int validate(String name) {
        if (name == null)
            return EMPTY;

        // [\p{L}\p{Z}] means not a letter in any language and not a separator.
        name = name.replaceAll("[^\\p{L}\\p{Z}]", "");
        name = name.trim();

        if (name.isEmpty())
            return EMPTY;

        if (name.contains(" ")) {
            String firstName = name.substring(0, name.lastIndexOf(" ")).trim();
            String lastName = name.substring(name.lastIndexOf(" ") + 1).trim();

            if ((firstName.length() >= 2) && !firstName.equalsIgnoreCase(" "))
                if ((lastName.length() >= 1) && !lastName.equalsIgnoreCase(" "))
                    return VALID;
                else
                    return FIRST_NAME_IS_TOO_SHORT;
            else
                return FIRST_NAME_IS_TOO_SHORT;
        } else {
            return TOO_FEW_WORDS;
        }
    }
}
