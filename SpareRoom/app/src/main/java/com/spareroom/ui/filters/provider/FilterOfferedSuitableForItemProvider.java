package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListProperties.Smoking;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.RoomsFor;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import java.util.*;

import androidx.annotation.NonNull;

class FilterOfferedSuitableForItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.suitableFor;
    private final static String KEY_ROOMS_FOR_GROUP = "key_rooms_for_group";
    private final static String KEY_SMOKING_GROUP = "key_smoking_group";

    final static String ALL_MALE = "all_male";
    final static String ALL_FEMALE = "all_female";
    final static String COUPLES = "couples";
    final static String ROOMS_FOR_NO_PREFERENCE = "rooms_for_no_preference";
    final static String BENEFITS = "benefits";
    final static String PETS = "pets";
    final static String SMOKERS = "smokers";
    final static String NON_SMOKERS = "non_smokers";
    final static String SMOKERS_NO_PREFERENCE = "smokers_no_preference";

    private final List<GroupedCompoundButtonParams> roomsForGroup = new ArrayList<>();
    private final List<GroupedCompoundButtonParams> smokingGroup = new ArrayList<>();
    private final Map<String, List<GroupedCompoundButtonParams>> groupList = new HashMap<>();

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesOffered previousProperties = (SearchAdvertListPropertiesOffered) properties;
        List<FilterItem> list = new ArrayList<>();

        smokingGroup.clear();
        roomsForGroup.clear();
        groupList.clear();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        GroupedCompoundButtonParams allFemale = new GroupedCompoundButtonParams(ALL_FEMALE);
        allFemale.size(CompoundButtonParams.Size.MEDIUM);
        allFemale.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        allFemale.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        allFemale.enableHighlightBar(true);
        allFemale.text(application.getString(R.string.females));
        allFemale.textColorResId(R.color.black);
        allFemale.iconResId(R.drawable.ic_female_user);
        allFemale.value(RoomsFor.FEMALES.toString());
        allFemale.groupName(KEY_ROOMS_FOR_GROUP);
        roomsForGroup.add(allFemale);
        list.add(new FilterItem<>(allFemale));

        GroupedCompoundButtonParams allMale = new GroupedCompoundButtonParams(ALL_MALE);
        allMale.size(CompoundButtonParams.Size.MEDIUM);
        allMale.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        allMale.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        allMale.enableHighlightBar(true);
        allMale.text(application.getString(R.string.males));
        allMale.textColorResId(R.color.black);
        allMale.iconResId(R.drawable.ic_male_user);
        allMale.value(RoomsFor.MALES.toString());
        allMale.groupName(KEY_ROOMS_FOR_GROUP);
        roomsForGroup.add(allMale);
        list.add(new FilterItem<>(allMale));

        GroupedCompoundButtonParams couples = new GroupedCompoundButtonParams(COUPLES);
        couples.size(CompoundButtonParams.Size.MEDIUM);
        couples.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        couples.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        couples.enableHighlightBar(true);
        couples.text(application.getString(R.string.couples));
        couples.textColorResId(R.color.black);
        couples.iconResId(R.drawable.ic_couple);
        couples.value(RoomsFor.COUPLES.toString());
        couples.groupName(KEY_ROOMS_FOR_GROUP);
        roomsForGroup.add(couples);
        list.add(new FilterItem<>(couples));

        GroupedCompoundButtonParams genderNoPreference = new GroupedCompoundButtonParams(ROOMS_FOR_NO_PREFERENCE);
        genderNoPreference.size(CompoundButtonParams.Size.MEDIUM);
        genderNoPreference.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.GRAY);
        genderNoPreference.enableHighlightBar(false);
        genderNoPreference.text(application.getString(R.string.noPreference));
        genderNoPreference.textColorResId(R.color.black);
        genderNoPreference.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        genderNoPreference.value(RoomsFor.NOT_SET.toString());
        genderNoPreference.defaultItem(true);
        genderNoPreference.groupName(KEY_ROOMS_FOR_GROUP);
        roomsForGroup.add(genderNoPreference);
        list.add(new FilterItem<>(genderNoPreference));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        CompoundButtonParams benefits = new CompoundButtonParams(BENEFITS);
        benefits.size(CompoundButtonParams.Size.MEDIUM);
        benefits.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        benefits.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        benefits.enableHighlightBar(true);
        benefits.text(application.getString(R.string.benefitsRecipients));
        benefits.textColorResId(R.color.black);
        benefits.iconResId(R.drawable.ic_whole_property);
        benefits.selected(previousProperties.getBenefits());
        list.add(new FilterItem<>(benefits));

        CompoundButtonParams pets = new CompoundButtonParams(PETS);
        pets.size(CompoundButtonParams.Size.MEDIUM);
        pets.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        pets.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        pets.enableHighlightBar(true);
        pets.text(application.getString(R.string.petsOwners));
        pets.textColorResId(R.color.black);
        pets.iconResId(R.drawable.ic_pets);
        pets.selected(previousProperties.getPets());
        list.add(new FilterItem<>(pets));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        GroupedCompoundButtonParams smokers = new GroupedCompoundButtonParams(SMOKERS);
        smokers.size(CompoundButtonParams.Size.MEDIUM);
        smokers.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        smokers.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        smokers.enableHighlightBar(true);
        smokers.text(application.getString(R.string.smokers));
        smokers.textColorResId(R.color.black);
        smokers.iconResId(R.drawable.ic_smoking);
        smokers.value(Smoking.YES.toString());
        smokers.groupName(KEY_SMOKING_GROUP);
        smokingGroup.add(smokers);
        list.add(new FilterItem<>(smokers));

        GroupedCompoundButtonParams nonSmokers = new GroupedCompoundButtonParams(NON_SMOKERS);
        nonSmokers.size(CompoundButtonParams.Size.MEDIUM);
        nonSmokers.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        nonSmokers.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        nonSmokers.enableHighlightBar(true);
        nonSmokers.text(application.getString(R.string.nonSmokers));
        nonSmokers.textColorResId(R.color.black);
        nonSmokers.iconResId(R.drawable.ic_non_smoking);
        nonSmokers.value(Smoking.NO.toString());
        nonSmokers.groupName(KEY_SMOKING_GROUP);
        smokingGroup.add(nonSmokers);
        list.add(new FilterItem<>(nonSmokers));

        GroupedCompoundButtonParams smokingNoPreference = new GroupedCompoundButtonParams(SMOKERS_NO_PREFERENCE);
        smokingNoPreference.size(CompoundButtonParams.Size.MEDIUM);
        smokingNoPreference.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        smokingNoPreference.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.GRAY);
        smokingNoPreference.enableHighlightBar(false);
        smokingNoPreference.text(application.getString(R.string.noPreference));
        smokingNoPreference.textColorResId(R.color.black);
        smokingNoPreference.value(Smoking.NOT_SET.toString());
        smokingNoPreference.groupName(KEY_SMOKING_GROUP);
        smokingNoPreference.defaultItem(true);
        smokingGroup.add(smokingNoPreference);
        list.add(new FilterItem<>(smokingNoPreference));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        groupList.put(KEY_ROOMS_FOR_GROUP, roomsForGroup);
        groupList.put(KEY_SMOKING_GROUP, smokingGroup);
        setGroupPreviousSearch(previousProperties);

        this.list = list;
    }

    private void setGroupPreviousSearch(SearchAdvertListPropertiesOffered properties) {
        Set<String> groupListKeySet = groupList.keySet();

        for (String key : groupListKeySet) {
            List<GroupedCompoundButtonParams> group = groupList.get(key);

            for (GroupedCompoundButtonParams params : group) {
                switch (key) {
                    case KEY_ROOMS_FOR_GROUP:
                        params.selected((properties.getRoomsFor() != RoomsFor.NOT_SET) && params.value().equals(properties.getRoomsFor().toString()));
                        break;
                    case KEY_SMOKING_GROUP:
                        params.selected((properties.getSmoking() != Smoking.NOT_SET) && params.value().equals(properties.getSmoking().toString()));
                        break;
                }
            }
            selectedItem(group);
        }

    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return groupList;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesOffered propertiesOffered = (SearchAdvertListPropertiesOffered) propertiesToUpdate;
        for (FilterItem item : list) {
            switch (item.tag()) {
                case BENEFITS:
                    propertiesOffered.setBenefits(((CompoundButtonParams) item.params()).selected());
                    break;
                case PETS:
                    propertiesOffered.setPets(((CompoundButtonParams) item.params()).selected());
                    break;
            }
        }

        propertiesOffered.setRoomsFor(RoomsFor.valueOf(selectedValue(groupList.get(KEY_ROOMS_FOR_GROUP))));
        propertiesOffered.setSmoking(Smoking.valueOf(selectedValue(groupList.get(KEY_SMOKING_GROUP))));

    }

}
