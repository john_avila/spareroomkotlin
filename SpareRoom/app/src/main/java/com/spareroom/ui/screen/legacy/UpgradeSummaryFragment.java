package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.model.business.Session;
import com.spareroom.ui.screen.Fragment;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class UpgradeSummaryFragment extends Fragment {
    private TextView _tvType;
    private TextView _tvExpire;
    private TextView _tvTimeLeft;
    private TextView _tvNoAds;
    private Button _bRenew;
    private Session _session;

    private String _upgradeProduct = "";
    private String _upgradeExpire = "";
    private String _upgradeTimeLeft = "";
    private String _upgradenoAdsOffered = "";
    private String _upgradenoAdsWanted = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.upgrade_summary, container, false);

        _tvType = rootView.findViewById(R.id.upgradeSummary_tvType_value);
        _tvExpire = rootView.findViewById(R.id.upgradeSummary_tvExpire_value);
        _tvTimeLeft = rootView.findViewById(R.id.upgradeSummary_tvTimeLeft_value);
        _tvNoAds = rootView.findViewById(R.id.upgradeSummary_tvNoAds_value);

        _bRenew = rootView.findViewById(R.id.upgrade_summary_bRenew);

        _bRenew.setOnClickListener(v -> ((UpgradeFragmentActivity) getActivity()).firstUpgradePage());

        update(SpareroomApplication.getInstance(
                getActivity().getApplicationContext()
        ).getSpareroomContext().getSession());
        /*
        _session = ((SpareroomApplication) getActivity().getApplication()).getSpareroomContext().getSession();
		
		_tvType.setText(_session.get_upgradeProduct());
		_tvExpire.setText(_session.get_upgradeExpire());
		_tvTimeLeft.setText(_session.get_upgradeTimeLeft());
		_tvNoAds.setText(_session.get_upgradeNoAdsOffered());
		update();
		*/
        return rootView;
    }

    public void update(Session s) {

        if (s.get_upgradeProduct() != null)
            _upgradeProduct = s.get_upgradeProduct();
        if (s.getUnixAccessExp() != null)
            _upgradeExpire = formatDate(s.getUnixAccessExp());
        if (s.get_upgradeTimeLeft() != null)
            _upgradeTimeLeft = s.get_upgradeTimeLeft();
        if (s.get_upgradeNoAdsOffered() != null)
            _upgradenoAdsOffered = s.get_upgradeNoAdsOffered();
        if (s.get_upgradeNoAdsWanted() != null)
            _upgradenoAdsWanted = s.get_upgradeNoAdsWanted();

        updateView();

    }

    private void updateView() {
        if (_tvType != null) {
            if ((_upgradeProduct != null) && !_upgradeProduct.equals("null") && (!_upgradeProduct.equals(""))) {
                _tvType.setText(_upgradeProduct);
            } else {
                _tvType.setText("-");
            }
        }
        if (_tvExpire != null) {
            if ((_upgradeExpire != null) && !_upgradeExpire.equals("null") && (!_upgradeExpire.equals(""))) {
                _tvExpire.setText(_upgradeExpire);
            } else {
                _tvExpire.setText("-");
            }
        }
        if (_tvTimeLeft != null) {
            if ((_upgradeTimeLeft != null) && !_upgradeTimeLeft.equals("null") && (!_upgradeTimeLeft.equals(""))) {
                _tvTimeLeft.setText(_upgradeTimeLeft);
            } else {
                _tvTimeLeft.setText("-");
            }
        }
        if (_tvNoAds != null) {
            if ((_upgradenoAdsOffered != null) && (!_upgradenoAdsOffered.equals("null")) && (!_upgradenoAdsOffered.equals(""))) {
                _tvNoAds.setText(_upgradenoAdsOffered);
            } else {
                _tvNoAds.setText("-");
            }
        }
    }

    private String formatDate(String date) {
        if (!date.isEmpty()) {
            Date time = new java.util.Date(Long.parseLong(date) * 1000);
            Locale locale = AppVersion.flavor().getLocale();
            SimpleDateFormat simpleDateFormat =
                    new SimpleDateFormat(
                            AppVersion.flavor().getDateHourRepresentation(),
                            locale);
            date = simpleDateFormat.format(time);
        }

        return date;
    } //end formatDate(String date)

}
