package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListProperties.RoomTypes;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import java.util.*;

import androidx.annotation.NonNull;

class FilterOfferedBedroomTypeItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.bedroomType;
    private final static String KEY_ROOM_TYPE_GROUP = "key_room_type_group";

    final static String SINGLE_BEDROOM = "single_bedroom";
    final static String DOUBLE_BEDROOM = "double_bedroom";
    final static String NO_PREFERENCE = "no_preference";
    final static String EN_SUITE = "en_suite";

    private final List<GroupedCompoundButtonParams> roomTypeGroup = new ArrayList<>();
    private final Map<String, List<GroupedCompoundButtonParams>> groupList = new HashMap<>();

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesOffered previousProperties = (SearchAdvertListPropertiesOffered) properties;
        List<FilterItem> list = new ArrayList<>();
        roomTypeGroup.clear();
        groupList.clear();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        GroupedCompoundButtonParams doubleBedroom = new GroupedCompoundButtonParams(DOUBLE_BEDROOM);
        doubleBedroom.size(CompoundButtonParams.Size.MEDIUM);
        doubleBedroom.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        doubleBedroom.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        doubleBedroom.enableHighlightBar(true);
        doubleBedroom.text(application.getString(R.string.doubleBedroom));
        doubleBedroom.textColorResId(R.color.black);
        doubleBedroom.iconResId(R.drawable.ic_double_bed);
        doubleBedroom.value(RoomTypes.DOUBLE.toString());
        doubleBedroom.groupName(KEY_ROOM_TYPE_GROUP);
        roomTypeGroup.add(doubleBedroom);
        list.add(new FilterItem<>(doubleBedroom));

        GroupedCompoundButtonParams singleBedroom = new GroupedCompoundButtonParams(SINGLE_BEDROOM);
        singleBedroom.size(CompoundButtonParams.Size.MEDIUM);
        singleBedroom.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        singleBedroom.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        singleBedroom.enableHighlightBar(true);
        singleBedroom.text(application.getString(R.string.singleBedroom));
        singleBedroom.textColorResId(R.color.black);
        singleBedroom.iconResId(R.drawable.ic_single_bed);
        singleBedroom.value(RoomTypes.SINGLE.toString());
        singleBedroom.groupName(KEY_ROOM_TYPE_GROUP);
        roomTypeGroup.add(singleBedroom);
        list.add(new FilterItem<>(singleBedroom));

        GroupedCompoundButtonParams noPreference = new GroupedCompoundButtonParams(NO_PREFERENCE);
        noPreference.size(CompoundButtonParams.Size.MEDIUM);
        noPreference.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        noPreference.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.GRAY);
        noPreference.enableHighlightBar(false);
        noPreference.text(application.getString(R.string.noPreference));
        noPreference.textColorResId(R.color.black);
        noPreference.value(RoomTypes.NOT_SET.toString());
        noPreference.groupName(KEY_ROOM_TYPE_GROUP);
        noPreference.defaultItem(true);
        roomTypeGroup.add(noPreference);
        list.add(new FilterItem<>(noPreference));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        CompoundButtonParams enSuite = new CompoundButtonParams(EN_SUITE);
        enSuite.size(CompoundButtonParams.Size.MEDIUM);
        enSuite.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        enSuite.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        enSuite.enableHighlightBar(true);
        enSuite.text(application.getString(R.string.enSuite));
        enSuite.textColorResId(R.color.black);
        enSuite.iconResId(R.drawable.ic_ensuite);
        enSuite.selected(previousProperties.getEnSuite());
        list.add(new FilterItem<>(enSuite));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        groupList.put(KEY_ROOM_TYPE_GROUP, roomTypeGroup);
        setGroupPreviousSearch(previousProperties);

        this.list = list;
    }

    private void setGroupPreviousSearch(SearchAdvertListPropertiesOffered properties) {
        Set<String> groupListKeySet = groupList.keySet();

        for (String key : groupListKeySet) {
            List<GroupedCompoundButtonParams> group = groupList.get(key);

            for (GroupedCompoundButtonParams params : group) {
                switch (key) {
                    case KEY_ROOM_TYPE_GROUP:
                        params.selected((properties.getRoomType() != RoomTypes.NOT_SET)
                                && params.value().equals(properties.getRoomType().toString()));
                        break;
                }
            }
            selectedItem(group);
        }

    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return groupList;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesOffered propertiesOffered = (SearchAdvertListPropertiesOffered) propertiesToUpdate;
        for (FilterItem item : list) {
            switch (item.tag()) {
                case EN_SUITE:
                    propertiesOffered.setEnSuite(((CompoundButtonParams) item.params()).selected());
                    break;
            }
        }

        propertiesOffered.setRoomType(RoomTypes.valueOf(selectedValue(groupList.get(KEY_ROOM_TYPE_GROUP))));

    }

}
