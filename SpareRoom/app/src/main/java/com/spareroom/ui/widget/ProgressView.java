package com.spareroom.ui.widget;

import android.animation.*;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import com.spareroom.R;

import androidx.annotation.*;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.ViewCompat;

public class ProgressView extends ConstraintLayout {

    private final AnimatorSet animatorSet = new AnimatorSet();

    private TextView txtTitle;

    public ProgressView(@NonNull Context context) {
        super(context);
        init(context);
    }

    public ProgressView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ProgressView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopAnimation();
        animatorSet.removeAllListeners();
    }

    public void show(String title) {
        setTitleText(title);
        setVisibility(View.VISIBLE);

        startAnimation();
    }

    public void show() {
        show("");
    }

    public void hide() {
        setVisibility(View.GONE);
        stopAnimation();
    }

    private void init(Context context) {
        inflate(context, R.layout.progress_view, this);

        int paddingInPx = getResources().getDimensionPixelSize(R.dimen.material_margin_large);
        setBackgroundColor(Color.WHITE);
        setPadding(paddingInPx, paddingInPx, paddingInPx, paddingInPx);

        txtTitle = findViewById(R.id.txtTitle);
        View progressViewDot1 = findViewById(R.id.progressViewDot1);
        View progressViewDot2 = findViewById(R.id.progressViewDot2);
        View progressViewDot3 = findViewById(R.id.progressViewDot3);

        animatorSet.playSequentially(getFadeAnimation(progressViewDot1), getFadeAnimation(progressViewDot2), getFadeAnimation(progressViewDot3));
        animatorSet.addListener(new AnimatorListener(animatorSet, this));
    }

    private void setTitleText(String title) {
        txtTitle.setText(title);
    }

    private void stopAnimation() {
        animatorSet.cancel();
    }

    private void startAnimation() {
        animatorSet.start();
    }

    private ObjectAnimator getFadeAnimation(View view) {
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "alpha", 1f, 0.5f);
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.setDuration(200);

        return animation;
    }

    private static class AnimatorListener implements Animator.AnimatorListener {
        private final View view;
        private final AnimatorSet animatorSet;
        private boolean isCancelled;

        private AnimatorListener(AnimatorSet animatorSet, View view) {
            this.animatorSet = animatorSet;
            this.view = view;
        }

        @Override
        public void onAnimationStart(Animator animator) {
            isCancelled = false;
        }

        @Override
        public void onAnimationEnd(Animator animator) {
            if (!isCancelled && ViewCompat.isAttachedToWindow(view)) {
                animatorSet.start();
            }
        }

        @Override
        public void onAnimationCancel(Animator animator) {
            isCancelled = true;
        }

        @Override
        public void onAnimationRepeat(Animator animator) {

        }
    }
}
