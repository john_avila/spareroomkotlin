package com.spareroom.ui.filters.adapter.viewparams;

import com.spareroom.ui.widget.params.CompoundButtonParams;
import com.spareroom.ui.widget.params.ViewParamsType;

public class GroupedCompoundButtonParams extends CompoundButtonParams {
    private boolean defaultItem;
    private String groupName;

    public GroupedCompoundButtonParams(String tag) {
        super(tag);
    }

    @Override
    public ViewParamsType type() {
        return ViewParamsType.GROUPED_COMPOUND;
    }

    public boolean defaultItem() {
        return defaultItem;
    }

    public void defaultItem(boolean defaultItem) {
        this.defaultItem = defaultItem;
        selected(defaultItem);
    }

    public String groupName() {
        return groupName;
    }

    public void groupName(String groupName) {
        this.groupName = groupName;
    }

    public void reset() {
        selected(defaultItem());
    }

}
