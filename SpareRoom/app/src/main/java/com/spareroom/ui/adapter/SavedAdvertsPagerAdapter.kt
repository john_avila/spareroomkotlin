package com.spareroom.ui.adapter

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.spareroom.R
import com.spareroom.ui.screen.Fragment
import com.spareroom.ui.screen.legacy.SavedAdOfferedFragment
import com.spareroom.ui.screen.legacy.SavedAdWantedFragment

class SavedAdvertsPagerAdapter(fragmentManager: FragmentManager, context: Context) : FragmentStatePagerAdapter(fragmentManager) {

    private val titles = arrayOf(context.getString(R.string.rooms), context.getString(R.string.flatmates))

    override fun getItem(position: Int): Fragment {
        return if (position == 0) SavedAdOfferedFragment() else SavedAdWantedFragment()
    }

    override fun getCount(): Int {
        return titles.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return titles[position]
    }
}