package com.spareroom.ui.filters.adapter;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.spareroom.R;
import com.spareroom.ui.adapter.RecyclerViewAdapter;
import com.spareroom.ui.filters.adapter.viewholder.*;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;
import com.spareroom.ui.filters.provider.FilterItem;
import com.spareroom.ui.widget.params.ViewParamsType;

import java.util.List;

import androidx.annotation.NonNull;

import static com.spareroom.ui.widget.params.ViewParamsType.GROUPED_COMPOUND;

public class FiltersRecyclerAdapter extends RecyclerViewAdapter<FilterItem, BaseFilterViewHolder> {
    private final OnClickListener summaryOnClickListener;
    private final OnClickListener compoundButtonOnClickListener;
    private final OnClickListener compoundButtonGroupOnClickListener;

    public FiltersRecyclerAdapter(Context context, List<FilterItem> filterItems, OnClickListener summaryOnClickListener,
                                  OnClickListener compoundButtonOnClickListener, OnClickListener compoundButtonGroupOnClickListener) {
        super(context);
        init(filterItems);
        this.summaryOnClickListener = summaryOnClickListener;
        this.compoundButtonOnClickListener = compoundButtonOnClickListener;
        this.compoundButtonGroupOnClickListener = compoundButtonGroupOnClickListener;
    }

    private void init(List<FilterItem> filterItems) {
        update(filterItems, false);
    }

    @NonNull
    @Override
    public BaseFilterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ViewParamsType.SUMMARY.ordinal()) {
            View summary = getInflater().inflate(R.layout.material_widget_summary_view, parent, false);
            summary.setOnClickListener(summaryOnClickListener);
            return new SummaryViewHolder(summary);
        } else if (viewType == ViewParamsType.COMPOUND.ordinal() || viewType == ViewParamsType.GROUPED_COMPOUND.ordinal()) {
            return new CompoundButtonViewHolder(getInflater().inflate(R.layout.material_widget_compound_button_view, parent, false));
        } else if (viewType == ViewParamsType.MIN_MAX.ordinal()) {
            return new MinMaxViewHolder(getInflater().inflate(R.layout.material_widget_min_max_view, parent, false));
        } else if (viewType == ViewParamsType.TITLE.ordinal()) {
            return new TitleViewHolder(getInflater().inflate(R.layout.material_list_title_item, parent, false));
        } else {
            return new SpaceViewHolder(getInflater().inflate(R.layout.material_list_space_item, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).type().ordinal();
    }

    @Override
    public void onBindViewHolder(@NonNull BaseFilterViewHolder holder, int position) {
        FilterItem filterItem = getItem(position);
        if (holder instanceof SummaryViewHolder) {
            holder.itemView.setTag(R.id.position_tag, position);
            holder.itemView.setTag(R.id.id_tag, filterItem.tag());
            holder.itemView.setTag(R.id.param_tag, filterItem.params());

        } else if (holder instanceof CompoundButtonViewHolder) {
            holder.itemView.setTag(R.id.position_tag, position);
            holder.itemView.setTag(R.id.id_tag, filterItem.tag());

            boolean isGrouped = GROUPED_COMPOUND == filterItem.type();
            holder.itemView.setTag(R.id.group_tag, isGrouped ? ((GroupedCompoundButtonParams) filterItem.params()).groupName() : null);
            holder.itemView.setOnClickListener(isGrouped ? compoundButtonGroupOnClickListener : compoundButtonOnClickListener);
        }
        holder.bind(filterItem);
    }

    public void updateAdapter(int position) {
        updateListItems(position, 1);
    }

    public void resetAdapter(List<FilterItem> filterItems) {
        init(filterItems);
        notifyDataSetChanged();
    }

    public void updateListItems(int position, int count) {
        notifyItemRangeChanged(position, count);
    }

}
