package com.spareroom.ui.screen.legacy;

import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.controller.legacy.*;
import com.spareroom.model.business.*;
import com.spareroom.ui.screen.Activity;
import com.spareroom.ui.screen.InjectableActivity;
import com.spareroom.ui.util.*;

import java.util.Collection;
import java.util.TreeSet;

import javax.inject.Inject;

public class PlaceAdAreasActivity extends InjectableActivity {
    private AsyncTaskManager _asyncManager;

    private LinearLayout _llBreadcrumbs;
    private ListView _lvLevel0;
    private ListView _lvLevel1;
    private ListView _lvLevel2;
    private TextView _tvLevel0;
    private TextView _tvLevel0Separator;
    private TextView _tvLevel1;
    private Level1AsyncResult _asyncResultLevel1;
    private Level2AsyncResult _asyncResultLevel2;

    private Collection<PlaceAdArea> _cLevel0;
    private Collection<PlaceAdArea> _cLevel1;

    private final PlaceAdFullArea _currentArea = new PlaceAdFullArea();
    private final View[] _currentlySelectedViews = {null, null, null};

    private PlaceAdFullAreaSet _areaSet = null;

    @Inject
    ConnectivityChecker connectivityChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.place_ad_wanted_areas_looking_in_list);

        _llBreadcrumbs = findViewById(R.id.place_ad_wanted_areas_llLevel);
        _lvLevel0 = findViewById(R.id.place_ad_wanted_areas_looking_in_list_lvLevel0);
        _lvLevel1 = findViewById(R.id.place_ad_wanted_areas_looking_in_list_lvLevel1);
        _lvLevel2 = findViewById(R.id.place_ad_wanted_areas_looking_in_list_lvLevel2);
        _tvLevel0 = findViewById(R.id.place_ad_wanted_areas_tvLevel0);
        _tvLevel0Separator = findViewById(R.id.place_ad_wanted_areas_tvLevel0Separator);
        _tvLevel1 = findViewById(R.id.place_ad_wanted_areas_tvLevel1);

        _tvLevel0.setOnClickListener(v -> backToLevel0());

        _tvLevel1.setOnClickListener(v -> backToLevel1());

        Level0AsyncResult _asyncResultLevel0 = new Level0AsyncResult(_lvLevel0);
        _asyncResultLevel1 = new Level1AsyncResult(_lvLevel1);
        _asyncResultLevel2 = new Level2AsyncResult(_lvLevel2);

        PlaceAdFullAreaSet areaSet = (PlaceAdFullAreaSet) getIntent().getSerializableExtra("areas");
        if (areaSet == null)
            _areaSet = new PlaceAdFullAreaSet();
        else
            _areaSet = areaSet;

        _asyncManager = new AsyncTaskManager();

        setToolbar();

        Parameters p = new Parameters();
        p.add("level", "0");

        SpareroomApplication application = SpareroomApplication
                .getInstance(getApplicationContext());
        PlaceAdWantedAreasAsyncTask at = new PlaceAdWantedAreasAsyncTask(application.getPlaceAdFacade(), _asyncResultLevel0);
        Parameters[] vParameters = {p};
        at.execute(vParameters);
        _asyncManager.register(at);

        setActivityResultCanceled();

    }

    private class Level0AsyncResult extends IAsyncResultImpl {
        private final ListView _lv;

        Level0AsyncResult(ListView lv) {
            _lv = lv;
        }

        @Override
        public void update(Object o) {
            if (noAreas((Collection<PlaceAdArea>) o))
                return;

            _cLevel0 = (Collection<PlaceAdArea>) o;

            Animation animSlideInRight = AnimationUtils.loadAnimation(PlaceAdAreasActivity.this, R.anim.slide_in_right);
            animSlideInRight.setAnimationListener(new StartPanelAnimation(_lvLevel1));
            _lvLevel1.setVisibility(View.VISIBLE);

            _lv.setAdapter(new AreasLevel0Adapter(_cLevel0));
        }

        @Override
        public void handleServiceUnavailableException(String message) {
            handleError(message);
        }
    }

    private class Level1AsyncResult extends IAsyncResultImpl {
        private final ListView _lv;

        Level1AsyncResult(ListView lv) {
            _lv = lv;
        }

        @Override
        public void update(Object o) {
            if (noAreas((Collection<PlaceAdArea>) o))
                return;

            _cLevel1 = (Collection<PlaceAdArea>) o;

            Animation animSlideInRight = AnimationUtils.loadAnimation(PlaceAdAreasActivity.this, R.anim.slide_in_right);
            animSlideInRight.setAnimationListener(new StartPanelAnimation(_lvLevel1));
            _lvLevel1.setVisibility(View.VISIBLE);

            _lv.setAdapter(new AreasLevel1Adapter(_cLevel1));
        }

        @Override
        public void handleServiceUnavailableException(String message) {
            handleError(message);
        }
    }

    private class Level2AsyncResult extends IAsyncResultImpl {
        private final ListView _lv;

        Level2AsyncResult(ListView lv) {
            _lv = lv;
        }

        @Override
        public void update(Object o) {
            if (noAreas((Collection<PlaceAdArea>) o))
                return;

            Collection<PlaceAdArea> _cLevel2 = (Collection<PlaceAdArea>) o;

            Animation animSlideInRight = AnimationUtils.loadAnimation(PlaceAdAreasActivity.this, R.anim.slide_in_right);
            animSlideInRight.setAnimationListener(new StartPanelAnimation(_lvLevel2));
            _lvLevel2.setVisibility(View.VISIBLE);

            _lv.setAdapter(new AreasLevel2Adapter(_cLevel2));
        }

        @Override
        public void handleServiceUnavailableException(String message) {
            handleError(message);
        }
    }

    private boolean noAreas(Collection<PlaceAdArea> areas) {
        if (areas == null || areas.isEmpty()) {
            showErrorMessage(getString(R.string.no_areas));
            return true;
        }

        return false;
    }

    private boolean noConnection() {
        if (!connectivityChecker.isConnected()) {
            showErrorMessage(getString(R.string.no_connection));
            return true;
        }

        return false;
    }

    private void handleError(String message) {
        if (!connectivityChecker.isConnected()) {
            showErrorMessage(getString(R.string.no_connection));
        } else if (!StringUtils.isNullOrEmpty(message)) {
            showErrorMessage(message);
        } else {
            showErrorMessage(getString(R.string.error_message));
        }
    }

    private void showErrorMessage(String message) {
        if (!isActivityDestroyed())
            SnackBarUtils.show(_llBreadcrumbs, message);
    }

    private void setActivityResultCanceled() {
        setResult(Activity.RESULT_CANCELED);
    }

    private void confirmAreas() {
        int areas = _areaSet != null ? _areaSet.size() : 0;
        String areasSelected = String.format("%s %s %s", areas, getResources().getQuantityString(R.plurals.areas, areas), getString(R.string.selected));
        ToastUtils.showToast(areas == 0 ? getString(R.string.no_areas_selected) : areasSelected);

        Intent resultIntent = new Intent();
        resultIntent.putExtra("areas", _areaSet);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        setActivityResultCanceled();
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.apply_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_item_apply:
                confirmAreas();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        _asyncManager.cancelAll();
    }

    private class StartPanelAnimation implements AnimationListener {
        private final View _panel;

        StartPanelAnimation(View panel) {
            _panel = panel;
        }

        @Override
        public void onAnimationStart(Animation animation) {
            _panel.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
        }
    }

    private class ExitPanelAnimation implements AnimationListener {
        private final View _panel;

        ExitPanelAnimation(View panel) {
            _panel = panel;
        }

        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            _panel.setVisibility(View.GONE);
        }
    }

    private void selectAreaLevel0(PlaceAdArea a) {
        _tvLevel0.setVisibility(View.VISIBLE);
        _tvLevel0.setText(a.getName());
        Animation animSlideOutLeft = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);
        animSlideOutLeft.setAnimationListener(new ExitPanelAnimation(_lvLevel0));
        _lvLevel0.startAnimation(animSlideOutLeft);

        Parameters p = new Parameters();
        p.add("level", "1");
        p.add("id", a.getId());

        SpareroomApplication application = SpareroomApplication
                .getInstance(getApplicationContext());
        PlaceAdWantedAreasAsyncTask at = new PlaceAdWantedAreasAsyncTask(application.getPlaceAdFacade(), _asyncResultLevel1);
        Parameters[] vParameters = {p};
        at.execute(vParameters);
        _asyncManager.register(at);

        _currentArea.set(0, a);
    }

    private void selectAreaLevel1(PlaceAdArea a) {
        _tvLevel1.setVisibility(View.VISIBLE);
        _tvLevel0Separator.setVisibility(View.VISIBLE);
        _tvLevel1.setText(a.getName());

        Animation animSlideOutLeft = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);
        animSlideOutLeft.setAnimationListener(new ExitPanelAnimation(_lvLevel1));
        _lvLevel1.startAnimation(animSlideOutLeft);

        Parameters p = new Parameters();
        p.add("level", "2");
        p.add("id", a.getId());

        SpareroomApplication application = SpareroomApplication
                .getInstance(getApplicationContext());
        PlaceAdWantedAreasAsyncTask at = new PlaceAdWantedAreasAsyncTask(application.getPlaceAdFacade(), _asyncResultLevel2);
        Parameters[] vParameters = {p};
        at.execute(vParameters);
        _asyncManager.register(at);

        _currentArea.set(1, a);
    }

    private void selectAreaLevel2(PlaceAdArea a) {
        _currentArea.set(2, a);

        PlaceAdFullArea fullArea = new PlaceAdFullArea();
        fullArea.set(0, _currentArea.get(0));
        fullArea.set(1, _currentArea.get(1));
        fullArea.set(2, _currentArea.get(2));

        _areaSet.add(fullArea);
    }

    private void unselectAreaLevel2(PlaceAdArea a) {
        _currentArea.set(2, a);

        PlaceAdFullArea fullArea = new PlaceAdFullArea();
        fullArea.set(0, _currentArea.get(0));
        fullArea.set(1, _currentArea.get(1));
        fullArea.set(2, _currentArea.get(2));

        _areaSet.remove(fullArea);
    }

    /**
     * launches animations and show the right UI when navigating from Level 2 to Level 1
     */
    private void backToLevel1() {
        Animation animSlideOutRight = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
        animSlideOutRight.setAnimationListener(new ExitPanelAnimation(_lvLevel2));
        _lvLevel2.startAnimation(animSlideOutRight);

        Animation animSlideInLeft = AnimationUtils.loadAnimation(PlaceAdAreasActivity.this, R.anim.slide_in_left);
        animSlideInLeft.setAnimationListener(new StartPanelAnimation(_lvLevel1));
        _lvLevel1.startAnimation(animSlideInLeft);

        _tvLevel1.setVisibility(View.GONE);
        _tvLevel0Separator.setVisibility(View.GONE);

        _currentlySelectedViews[1] = null;
        // TODO Improve the following line:
        _lvLevel1.setAdapter(new AreasLevel1Adapter(_cLevel1));
    }

    /**
     * launches animations and show the right UI when navigating from Level 1 to Level 0
     */
    private void backToLevel0() {

        Animation animSlideOutRight = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
        animSlideOutRight.setAnimationListener(new ExitPanelAnimation(_lvLevel1));
        _lvLevel1.startAnimation(animSlideOutRight);

        Animation animSlideInLeft = AnimationUtils.loadAnimation(PlaceAdAreasActivity.this, R.anim.slide_in_left);
        animSlideInLeft.setAnimationListener(new StartPanelAnimation(_lvLevel0));
        _lvLevel0.startAnimation(animSlideInLeft);

        _tvLevel0.setVisibility(View.GONE);
        _llBreadcrumbs.setVisibility(View.GONE);

        _currentlySelectedViews[0] = null;
        // TODO Improve the following line:
        _lvLevel0.setAdapter(new AreasLevel0Adapter(_cLevel0));

    }

    private void selectViewLevel2(View row) {
        row.setBackgroundResource(R.color.light_blue_2);
        ImageView ivChosen = row.findViewById(R.id.place_ad_wanted_happy_ivChosen);
        if (ivChosen != null)
            ivChosen.setVisibility(View.VISIBLE);
    }

    private void unselectViewLevel2(View row) {
        row.setBackgroundResource(R.color.white);
        ImageView ivChosen = row.findViewById(R.id.place_ad_wanted_happy_ivChosen);
        ivChosen.setVisibility(View.GONE);
    }

    private void selectView(View row) {
        row.setBackgroundResource(R.color.light_blue_2);
        View tvChosen = row.findViewById(R.id.place_ad_offered_areas_option_chosen_tvNumChosen);
        tvChosen.setVisibility(View.VISIBLE);
    }

    private void unselectView(View row) {
        row.setBackgroundResource(R.color.white);
        View tvChosen = row.findViewById(R.id.place_ad_offered_areas_option_chosen_tvNumChosen);
        tvChosen.setVisibility(View.GONE);
    }

    private class AreasLevel0Adapter extends BaseAdapter {
        private final PlaceAdArea[] vArea;
        private final TreeSet<PlaceAdArea> _sChosen = new TreeSet<>();

        AreasLevel0Adapter(Collection<PlaceAdArea> cArea) {
            vArea = cArea.toArray(new PlaceAdArea[cArea.size()]);
            Collection<PlaceAdArea> cAreaLevel = _areaSet.getLevel(0);
            for (PlaceAdArea area : cAreaLevel) {
                _sChosen.add(area);
            }

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(PlaceAdAreasActivity.this).inflate(R.layout.place_ad_wanted_areas_item, parent, false);
            }
            unselectView(convertView);

            TextView tvOption = convertView.findViewById(R.id.place_ad_wanted_happy_tvOption);
            TextView tvCount = convertView.findViewById(R.id.place_ad_offered_areas_option_chosen_tvNumChosen);
            tvOption.setText(vArea[position].getName());
            tvCount.setText(String.format(AppVersion.flavor().getLocale(), "%d", _areaSet.count(0, vArea[position])));

            convertView.setOnClickListener(new SelectOnClickListener(position));

            if (_sChosen.contains(vArea[position])) {
                tvCount.setVisibility(View.VISIBLE);
                selectView(convertView);
            } else
                tvCount.setVisibility(View.GONE);

            return convertView;
        }

        private class SelectOnClickListener implements OnClickListener {
            private final int _position;

            SelectOnClickListener(int position) {
                _position = position;
            }

            @Override
            public void onClick(View v) {
                if (noConnection())
                    return;

                selectAreaLevel0(vArea[_position]);
                _currentlySelectedViews[0] = v;
                _llBreadcrumbs.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {

            return vArea[position];
        }

        @Override
        public int getCount() {
            // return _content.length;
            return vArea.length;
        }
    }

    private class AreasLevel1Adapter extends BaseAdapter {
        private final PlaceAdArea[] vArea;
        private final TreeSet<PlaceAdArea> _sChosen = new TreeSet<>();

        AreasLevel1Adapter(Collection<PlaceAdArea> cArea) {
            vArea = cArea.toArray(new PlaceAdArea[cArea.size()]);
            Collection<PlaceAdArea> cAreaLevel = _areaSet.getLevel(1);
            for (PlaceAdArea area : cAreaLevel) {
                _sChosen.add(area);
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(PlaceAdAreasActivity.this).inflate(R.layout.place_ad_wanted_areas_item, parent, false);
            }
            unselectView(convertView);

            TextView tvOption = convertView.findViewById(R.id.place_ad_wanted_happy_tvOption);
            TextView tvCount = convertView.findViewById(R.id.place_ad_offered_areas_option_chosen_tvNumChosen);

            if (position == 0) {
                tvOption.setText(R.string.placeAdAreasActivity_backAllAreas);
                convertView.setOnClickListener(v -> backToLevel0());

                return convertView;
            }

            tvOption.setText(vArea[position - 1].getName());
            tvCount.setText(String.format(AppVersion.flavor().getLocale(), "%d", _areaSet.count(1, vArea[position - 1])));
            convertView.setOnClickListener(new SelectOnClickListener(position - 1));

            if (_sChosen.contains(vArea[position - 1])) {
                tvCount.setVisibility(View.VISIBLE);
                selectView(convertView);
            } else
                tvCount.setVisibility(View.GONE);

            return convertView;
        }

        private class SelectOnClickListener implements OnClickListener {
            private final int _position;

            SelectOnClickListener(int position) {
                _position = position;
            }

            @Override
            public void onClick(View v) {
                if (noConnection())
                    return;

                selectAreaLevel1(vArea[_position]);
                _currentlySelectedViews[1] = v;
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {

            return vArea[position];
        }

        @Override
        public int getCount() {
            // return _content.length;
            return vArea.length + 1;
        }
    }

    private class AreasLevel2Adapter extends BaseAdapter {
        private final PlaceAdArea[] vArea;
        private final TreeSet<PlaceAdArea> _sChosen = new TreeSet<>();

        AreasLevel2Adapter(Collection<PlaceAdArea> cArea) {
            vArea = cArea.toArray(new PlaceAdArea[cArea.size()]);
            Collection<PlaceAdArea> cAreaLevel = _areaSet.getLevel(2);
            for (PlaceAdArea area : cAreaLevel) {
                _sChosen.add(area);
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView =
                        LayoutInflater.from(PlaceAdAreasActivity.this).inflate(R.layout.place_ad_wanted_happy_to_live_with_item, parent, false);
            }
            unselectViewLevel2(convertView);

            TextView tvOption = convertView.findViewById(R.id.place_ad_wanted_happy_tvOption);

            if (position == 0) {
                tvOption.setText(String.format(AppVersion.flavor().getLocale(), "%s %s", getString(R.string.placeAdAreasActivity_back), _tvLevel0.getText()));
                convertView.setOnClickListener(v -> backToLevel1());

                return convertView;
            }

            tvOption.setText(vArea[position - 1].getName());

            if (_sChosen.contains(vArea[position - 1])) {
                convertView.setOnClickListener(new UnselectOnClickListener(position - 1));
                selectViewLevel2(convertView);
            } else {
                convertView.setOnClickListener(new SelectOnClickListener(position - 1));
            }

            return convertView;
        }

        private class SelectOnClickListener implements OnClickListener {
            private final int _position;

            SelectOnClickListener(int position) {
                _position = position;
            }

            @Override
            public void onClick(View v) {
                _sChosen.add(vArea[_position]);
                selectAreaLevel2(vArea[_position]);
                v.setOnClickListener(new UnselectOnClickListener(_position));
                _currentlySelectedViews[2] = v;
                for (View row : _currentlySelectedViews)
                    selectViewLevel2(row);
                // The following should be improved:
                _lvLevel1.setAdapter(new AreasLevel1Adapter(_cLevel1));
                _lvLevel0.setAdapter(new AreasLevel0Adapter(_cLevel0));
            }
        }

        private class UnselectOnClickListener implements OnClickListener {
            private final int _position;

            UnselectOnClickListener(int position) {
                _position = position;
            }

            @Override
            public void onClick(View v) {
                unselectViewLevel2(v);
                _sChosen.remove(vArea[_position]);
                unselectAreaLevel2(vArea[_position]);
                v.setOnClickListener(new SelectOnClickListener(_position));
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {

            return vArea[position];
        }

        @Override
        public int getCount() {
            return vArea.length + 1; // We add an additional row for going 1 level back
        }
    }

}
