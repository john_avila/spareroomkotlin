package com.spareroom.ui.controller;

import com.spareroom.lib.core.IObservable;
import com.spareroom.lib.core.IObserver;
import com.spareroom.model.business.SpareroomStatus;

import java.util.LinkedList;

/**
 * Created by manuel on 18/11/2016.
 */

public class RegisterProcessCompositeUiController implements ViewUiController, ActionUiController,
        IObservable {
    private ViewUiController _uiControllerTextBox;
    private ActionButtonUiController _uiControllerRegisterButton;
    private final LinkedList<IObserver> _errorObserverList = new LinkedList<>();

    public RegisterProcessCompositeUiController(ViewUiController uiControllerTextBox,
                                                ActionButtonUiController
                                                        uiControllerRegisterButton) {
        _uiControllerTextBox = uiControllerTextBox;
        _uiControllerRegisterButton = uiControllerRegisterButton;
    }

    /**
     * Enable the view for its use
     */
    @Override
    public void enable() {
        _uiControllerTextBox.enable();
        _uiControllerRegisterButton.enable();
    }

    /**
     * Disable the view for make it non usable
     */
    @Override
    public void disable() {
        _uiControllerTextBox.setLoading();
        _uiControllerRegisterButton.disable();
    }

    /**
     * Sets the components so they display on screen the loading state
     */
    @Override
    public void setLoading() {
        _uiControllerTextBox.setLoading();
        _uiControllerRegisterButton.setLoading();
    }

    /**
     * Sets a status for this view (normally an error for fields that require validation)
     *
     * @param error a <code>SparerooomStatus</code> status
     */
    @Override
    public void setStatus(SpareroomStatus error) {

    }

    /**
     * Clears the status for this view
     */
    @Override
    public void clearStatus() {

    }

    /**
     * Populates components with content. For <code>SpareroomStatus</code> and
     * <code>Exception</code> use {@link #setSpareroomStatus(SpareroomStatus)}
     * or {@link #setException(Exception)}
     *
     * @param o objects that help to update the UI
     */
    @Override
    public void setResult(Object... o) {
        _uiControllerTextBox.enable();
        _uiControllerTextBox.clearStatus();
        _uiControllerRegisterButton.enable();
    }

    /**
     * Populates components with content.
     *
     * @param spareroomStatus that helps to update the UI
     */
    @Override
    public void setSpareroomStatus(SpareroomStatus spareroomStatus) {
        _uiControllerTextBox.setStatus(spareroomStatus);
        _uiControllerRegisterButton.setSpareroomStatus(spareroomStatus);
        notifyObservers(spareroomStatus);
    }

    /**
     * Populates components with content.
     *
     * @param exception that helps to update the UI
     */
    @Override
    public void setException(Exception exception) {
        // We would never have an exception from client side validation

    }

    @Override
    public void addObserver(IObserver errorObsever) {
        _errorObserverList.add(errorObsever);
    }

    @Override
    public void removeObserver(IObserver errorObsever) {
        _errorObserverList.remove(errorObsever);
    }

    private void notifyObservers(SpareroomStatus s) {
        for (IObserver observer : _errorObserverList) {
            observer.notifyObserver(s);
        }
    }
}