package com.spareroom.ui.adapter;

import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.integration.imageloader.ImageLoader;
import com.spareroom.integration.imageloader.ImageLoadingListener;
import com.spareroom.model.business.Picture;
import com.spareroom.ui.util.StringUtils;
import com.spareroom.ui.widget.ProgressView;

import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.RecyclerView;

public class FullScreenPictureAdapter extends RecyclerViewAdapter<Picture, FullScreenPictureAdapter.ViewHolder> {

    private final ImageLoader imageLoader;
    private final int maxImageHeight;

    @Inject
    public FullScreenPictureAdapter(ImageLoader imageLoader, Application application) {
        this.imageLoader = imageLoader;
        DisplayMetrics displayMetrics = application.getResources().getDisplayMetrics();
        maxImageHeight = Math.min(Math.max(displayMetrics.widthPixels, displayMetrics.heightPixels), Picture.MAX_DIMENSION);
    }

    public void setUp(Context context, List<Picture> pictures) {
        setContext(context);
        update(pictures, false);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new ViewHolder(getInflater().inflate(R.layout.full_screen_picture_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Picture picture = getItem(position);

        // set caption
        if (StringUtils.isNullOrEmpty(picture.getCaption())) {
            viewHolder.txtCaption.setVisibility(View.GONE);
        } else {
            viewHolder.txtCaption.setText(picture.getCaption());
            viewHolder.txtCaption.setVisibility(View.VISIBLE);
        }

        // set image
        viewHolder.imgPicture.setImageDrawable(null);
        imageLoader.clear(viewHolder.imgPicture);
        String pictureUrl = generatePictureUrl(picture);
        if (StringUtils.isNullOrEmpty(pictureUrl)) {
            viewHolder.progressBar.hide();
        } else {
            viewHolder.progressBar.show();
            imageLoader.loadImage(pictureUrl, viewHolder.imgPicture, new PictureLoadingListener(viewHolder.progressBar), true);
        }
    }

    private String generatePictureUrl(Picture picture) {
        String largeUrl = picture.getLargeUrl();
        if (StringUtils.isNullOrEmpty(largeUrl, true))
            return "";

        return String.format(Picture.LARGE_PICTURE_URL_TEMPLATE, largeUrl, maxImageHeight);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView imgPicture;
        private final TextView txtCaption;
        private final ProgressView progressBar;

        private ViewHolder(View itemView) {
            super(itemView);
            imgPicture = itemView.findViewById(R.id.imgFullScreenPicture);
            txtCaption = itemView.findViewById(R.id.txtFullScreenPictureCaption);
            progressBar = itemView.findViewById(R.id.fullScreenPictureProgressBar);
            progressBar.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private static class PictureLoadingListener extends ImageLoadingListener {
        private final ProgressView progressBar;

        private PictureLoadingListener(ProgressView progressBar) {
            this.progressBar = progressBar;
        }

        @Override
        public void onLoadingFailed(Exception exception) {
            progressBar.hide();
        }

        @Override
        public void onLoadingComplete() {
            progressBar.hide();
        }
    }
}
