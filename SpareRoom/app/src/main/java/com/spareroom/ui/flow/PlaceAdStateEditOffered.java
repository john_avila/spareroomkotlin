package com.spareroom.ui.flow;

import com.spareroom.controller.AppVersion;
import com.spareroom.model.business.DraftAd;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.ui.screen.legacy.*;

public class PlaceAdStateEditOffered extends FlowState {

    //region FIELDS

    private static final String PLACE_AD_OFFERED_ADD_ROOM_FRAGMENT = PlaceAdOfferedAddRoomFragment.class.getName();
    private static final String PLACE_AD_OFFERED_DEPOSIT_FRAGMENT = PlaceAdOfferedDepositFragment.class.getName();
    private static final String PLACE_AD_OFFERED_AVAILABILITY_PERIOD_BILLS = PlaceAdOfferedAvailabilityPeriodBills.class.getName();
    private static final String PLACE_AD_OFFERED_SHARED_LIVING_ROOM_FRAGMENT = PlaceAdOfferedSharedLivingRoomFragment.class.getName();
    private static final String PLACE_AD_OFFERED_BROADBAND_FRAGMENT = PlaceAdOfferedBroadBandFragment.class.getName();
    private static final String PLACE_AD_OFFERED_AMENITY_FRAGMENT = PlaceAdOfferedAmenityFragment.class.getName();
    private static final String PLACE_AD_OFFERED_NO_FLATMATES = PlaceAdOfferedNoFlatmates.class.getName();
    private static final String PLACE_AD_OFFERED_AGE_FLATMATES = PlaceAdOfferedAgeFlatmates.class.getName();
    private static final String PLACE_AD_OFFERED_INTERESTS_FRAGMENT = PlaceAdOfferedInterestsFragment.class.getName();
    private static final String PLACE_AD_OFFERED_PETS_FRAGMENT = PlaceAdOfferedPetsFragment.class.getName();
    private static final String PLACE_AD_OFFERED_SMOKE_FRAGMENT = PlaceAdOfferedSmokeFragment.class.getName();
    private static final String PLACE_AD_OFFERED_HAPPY_TO_LIVE_WITH_FRAGMENT = PlaceAdOfferedHappyToLiveWithFragment.class.getName();
    private static final String PLACE_AD_OFFERED_HAPPY_ANY_AGE_FRAGMENT = PlaceAdOfferedHappyAnyAgeFragment.class.getName();
    private static final String PLACE_AD_OFFERED_FEES_APPLY_FRAGMENT = PlaceAdOfferedFeesApplyFragment.class.getName();
    private static final String PLACE_AD_OFFERED_PHONE_FRAGMENT = PlaceAdOfferedPhoneFragment.class.getName();
    private static final String PLACE_AD_OFFERED_TITLE_DESCRIPTION_FRAGMENT = PlaceAdTitleDescriptionFragment.class.getName();

    private static final String PLACE_AD_UPLOADING_FRAGMENT = PlaceAdEditUploadingFragment.class.getName();
    private static final String PLACE_AD_UPLOAD_COMPLETE_FRAGMENT = PlaceAdUploadCompleteFragment.class.getName();

    //endregion FIELDS

    //region CONSTRUCTORS

    public PlaceAdStateEditOffered() {
        if (AppVersion.isUk()) {
            SCREENS = new String[]{
                    PLACE_AD_OFFERED_ADD_ROOM_FRAGMENT,
                    PLACE_AD_OFFERED_DEPOSIT_FRAGMENT,
                    PLACE_AD_OFFERED_AVAILABILITY_PERIOD_BILLS,
                    PLACE_AD_OFFERED_SHARED_LIVING_ROOM_FRAGMENT,
                    PLACE_AD_OFFERED_BROADBAND_FRAGMENT,
                    PLACE_AD_OFFERED_AMENITY_FRAGMENT,
                    PLACE_AD_OFFERED_NO_FLATMATES,
                    PLACE_AD_OFFERED_AGE_FLATMATES,
                    PLACE_AD_OFFERED_INTERESTS_FRAGMENT,
                    PLACE_AD_OFFERED_PETS_FRAGMENT,
                    PLACE_AD_OFFERED_SMOKE_FRAGMENT,
                    PLACE_AD_OFFERED_HAPPY_TO_LIVE_WITH_FRAGMENT,
                    PLACE_AD_OFFERED_HAPPY_ANY_AGE_FRAGMENT,
                    PLACE_AD_OFFERED_FEES_APPLY_FRAGMENT,
                    PLACE_AD_OFFERED_PHONE_FRAGMENT,
                    PLACE_AD_OFFERED_TITLE_DESCRIPTION_FRAGMENT,
                    PLACE_AD_UPLOADING_FRAGMENT,
                    PLACE_AD_UPLOAD_COMPLETE_FRAGMENT
            };
        } else {
            SCREENS = new String[]{
                    PLACE_AD_OFFERED_ADD_ROOM_FRAGMENT,
                    PLACE_AD_OFFERED_DEPOSIT_FRAGMENT,
                    PLACE_AD_OFFERED_AVAILABILITY_PERIOD_BILLS,
                    PLACE_AD_OFFERED_SHARED_LIVING_ROOM_FRAGMENT,
                    PLACE_AD_OFFERED_BROADBAND_FRAGMENT,
                    PLACE_AD_OFFERED_AMENITY_FRAGMENT,
                    PLACE_AD_OFFERED_NO_FLATMATES,
                    PLACE_AD_OFFERED_AGE_FLATMATES,
                    PLACE_AD_OFFERED_INTERESTS_FRAGMENT,
                    PLACE_AD_OFFERED_PETS_FRAGMENT,
                    PLACE_AD_OFFERED_SMOKE_FRAGMENT,
                    PLACE_AD_OFFERED_HAPPY_TO_LIVE_WITH_FRAGMENT,
                    PLACE_AD_OFFERED_HAPPY_ANY_AGE_FRAGMENT,
                    PLACE_AD_OFFERED_PHONE_FRAGMENT,
                    PLACE_AD_OFFERED_TITLE_DESCRIPTION_FRAGMENT,
                    PLACE_AD_UPLOADING_FRAGMENT,
                    PLACE_AD_UPLOAD_COMPLETE_FRAGMENT
            };
        }
        init();
    } //end PlaceAdStateEditLoggedInOffered()

    //endregion CONSTRUCTORS

    //region METHODS OVERRIDDEN

    @Override
    public String getPreviousFragmentName(String currentScreen, Object businessObject) {
        String previousScreen = super.getPreviousFragmentName(currentScreen, businessObject);
        DraftAd draftAd = (DraftAd) businessObject;

        if (currentScreen == null)
            return previousScreen;

        if (currentScreen.equals(PLACE_AD_OFFERED_AVAILABILITY_PERIOD_BILLS)) {
            if (!((DraftAdOffered) draftAd).wholeProperty())
                previousScreen = super.getPreviousFragmentName(previousScreen, draftAd);

        } else if (currentScreen.equals(PLACE_AD_OFFERED_ADD_ROOM_FRAGMENT)) {
            if (!draftAd.hasPicture()) {
                previousScreen = super.getPreviousFragmentName(previousScreen, draftAd);
            }
        } else if (currentScreen.equals(PLACE_AD_OFFERED_PHONE_FRAGMENT)) {
            if (((DraftAdOffered) draftAd).get_advertiserType() != DraftAdOffered.AdvertiserType.AGENT &&
                    ((DraftAdOffered) draftAd).get_advertiserType() != DraftAdOffered.AdvertiserType.LIVE_OUT_LANDLORD) {
                previousScreen = super.getPreviousFragmentName(previousScreen, draftAd);
            }
        }

        return previousScreen;
    } // end previousScreen(String currentScreen, DraftAd draft)

    @Override
    public String getNextFragmentName(String currentScreen, Object businessObject) {
        String nextScreen = super.getNextFragmentName(currentScreen, businessObject);
        DraftAd draftAd = (DraftAd) businessObject;

        if (currentScreen == null)
            return nextScreen;

        if (currentScreen.equals(PLACE_AD_OFFERED_ADD_ROOM_FRAGMENT)) {
            if (!((DraftAdOffered) draftAd).wholeProperty())
                nextScreen = super.getNextFragmentName(nextScreen, draftAd);

        } else if (currentScreen.equals(PLACE_AD_OFFERED_HAPPY_ANY_AGE_FRAGMENT)) {
            if (((DraftAdOffered) draftAd).get_advertiserType() != DraftAdOffered.AdvertiserType.AGENT &&
                    ((DraftAdOffered) draftAd).get_advertiserType() != DraftAdOffered.AdvertiserType.LIVE_OUT_LANDLORD) {
                nextScreen = super.getNextFragmentName(nextScreen, draftAd);
            }
        }

        return nextScreen;
    } // end nextScreen(String currentScreen, DraftAd draft)

    //endregion METHODS OVERRIDDEN

}