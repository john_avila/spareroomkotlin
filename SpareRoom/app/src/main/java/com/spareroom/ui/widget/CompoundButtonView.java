package com.spareroom.ui.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.*;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.core.widget.CompoundButtonCompat;

import static androidx.constraintlayout.widget.ConstraintSet.*;

public class CompoundButtonView extends ConstraintLayout {
    private final int defaultColor = R.color.white;

    private OnCheckedChangeListener _eventListener;

    private PorterDuffColorFilter iconPorterDuff;
    private PorterDuffColorFilter iconNotSelectedPorterDuff;
    private CompoundButtonParams.Size currentSize = CompoundButtonParams.Size.SMALL; // Everything is set up by default in the xml for this configuration
    private int textColor;
    private int subtitleColor;
    private int textColorNotSelected;
    private Drawable icon;
    private String text;
    private String subtitle;
    private boolean highlightBarVisible;

    private boolean isChecked;
    private boolean stopUserStateChange;

    private View viewHighlightBar;
    private ImageView iIcon;
    private TextView tvText;
    private TextView tvSubtitle;
    private android.widget.CompoundButton compoundButton;
    private FrameLayout checkView;

    public interface OnCheckedChangeListener {
        void onCheckedChangeEvent(boolean isChecked);
    }

    public CompoundButtonView(Context context) {
        super(context);
        init(context);
    }

    public CompoundButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CompoundButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context) {
        inflate(context, R.layout.material_widget_compound_button, this);
        viewHighlightBar = findViewById(R.id.widget_highlightBar);
        iIcon = findViewById(R.id.widgetCompoundButton_icon);
        tvText = findViewById(R.id.widgetCompoundButton_title);
        tvSubtitle = findViewById(R.id.widgetCompoundButton_subtitle);
        checkView = findViewById(R.id.widgetCompoundButton_check);

        setMinimumHeight(getResources().getDimensionPixelSize(R.dimen.material_widgetCompoundButton_minHeight_small));
        setOnClickListener(v -> {
            if (!stopUserStateChange) {
                isChecked = !isChecked;
                changeState();
                if (_eventListener != null)
                    _eventListener.onCheckedChangeEvent(isChecked);
            }

        });

    }

    private void init(Context context, AttributeSet attrs) {
        int defaultNotSelectedColor = R.color.light_blue;
        init(context);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CompoundButtonView);
        try {
            textColor = a.getInteger(R.styleable.CompoundButtonView_compoundTextColor, ContextCompat.getColor(context, defaultColor));
            subtitleColor = a.getInteger(R.styleable.CompoundButtonView_compoundSubtitleTextColor, ContextCompat.getColor(context, defaultColor));
            textColorNotSelected = a.getInteger(R.styleable.CompoundButtonView_compoundTextColorNotSelected, ContextCompat.getColor(context, defaultNotSelectedColor));
            int iconResId = a.getResourceId(R.styleable.CompoundButtonView_compoundSrcIcon, 0);
            if (iconResId != 0)
                icon = ContextCompat.getDrawable(context, iconResId);
            iconPorterDuff = setPorterDuff(a.getResourceId(R.styleable.CompoundButtonView_compoundIconColor, defaultColor));
            iconNotSelectedPorterDuff = setPorterDuff(a.getResourceId(R.styleable.CompoundButtonView_compoundIconColorNotSelected, defaultNotSelectedColor));
            text = a.getString(R.styleable.CompoundButtonView_compoundTextDefault);
            subtitle = a.getString(R.styleable.CompoundButtonView_compoundSubtitle);

            setBackgroundResource(a.getResourceId(R.styleable.CompoundButtonView_compoundBackground, 0));
        } finally {
            a.recycle();
        }

        initItems();

        setCompoundButton(CompoundButtonParams.CompoundButtonType.CHECK_BOX, CompoundButtonParams.CompoundButtonColorStateList.WHITE);
        changeState();
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener eventListener) {
        _eventListener = eventListener;
    }

    private void changeState() {
        compoundButton.setChecked(isChecked);

        if (isChecked) {
            iIcon.setColorFilter(iconPorterDuff);
            tvText.setTextColor(textColor);
            viewHighlightBar.setVisibility(highlightBarVisible ? VISIBLE : INVISIBLE);

        } else {
            iIcon.setColorFilter(iconNotSelectedPorterDuff);
            tvText.setTextColor((textColorNotSelected != 0) ? textColorNotSelected : textColor);
            viewHighlightBar.setVisibility(INVISIBLE);

        }

    }

    public void setParameters(CompoundButtonParams compoundButtonParams) {
        setSize(compoundButtonParams.size());
        setCompoundButton(compoundButtonParams.compoundButtonType(), compoundButtonParams.compoundButtonColorStateList());
        textColor = compoundButtonParams.textColor(getContext());
        subtitleColor = compoundButtonParams.subtitleColor(getContext());
        textColorNotSelected = compoundButtonParams.textColorNotSelected(getContext());
        icon = compoundButtonParams.icon(getContext());
        iconPorterDuff = setPorterDuff(compoundButtonParams.iconColor());
        iconNotSelectedPorterDuff = setPorterDuff(compoundButtonParams.iconNotSelectedColor());
        text = compoundButtonParams.text();
        subtitle = compoundButtonParams.subtitle();
        setBackgroundResource(compoundButtonParams.backgroundResId());
        highlightBarVisible = compoundButtonParams.highlightBarVisible();

        boolean subtitleIsEmpty = TextUtils.isEmpty(subtitle);
        tvSubtitle.setVisibility(subtitleIsEmpty ? GONE : VISIBLE);
        tvSubtitle.setText(subtitleIsEmpty ? "" : subtitle);
        tvSubtitle.setTextColor(subtitleIsEmpty ? ContextCompat.getColor(tvSubtitle.getContext(), defaultColor) : subtitleColor);

        initItems();

        alignItems(subtitleIsEmpty);
    }

    private void initItems() {
        iIcon.setImageDrawable(icon);
        tvText.setText(text);

    }

    private void alignItems(boolean subtitleIsEmpty) {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this);

        if (subtitleIsEmpty) {
            constraintSet.connect(viewHighlightBar.getId(), BOTTOM, PARENT_ID, BOTTOM);
            constraintSet.connect(viewHighlightBar.getId(), TOP, PARENT_ID, TOP);
        } else {
            constraintSet.clear(viewHighlightBar.getId(), BOTTOM);
            constraintSet.connect(viewHighlightBar.getId(), TOP, tvText.getId(), TOP);
        }
        constraintSet.applyTo(this);
    }

    private PorterDuffColorFilter setPorterDuff(int colorResId) {
        int iconColor = colorResId == 0 ? defaultColor : colorResId;
        return new PorterDuffColorFilter(ContextCompat.getColor(getContext(), iconColor), PorterDuff.Mode.SRC_IN);
    }

    private void setSize(CompoundButtonParams.Size size) {
        if (!currentSize.equals(size)) {
            currentSize = size;

            ViewGroup.LayoutParams iconLayoutParams =
                    iIcon.getLayoutParams() == null
                            ? new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                            : iIcon.getLayoutParams();

            switch (size) {
                case MEDIUM:
                    setMinimumHeight(getResources().getDimensionPixelSize(CompoundButtonParams.Size.MEDIUM.getMinHeightResId()));
                    iconLayoutParams.height = getResources().getDimensionPixelSize(CompoundButtonParams.Size.MEDIUM.getIconSizeResId());
                    iconLayoutParams.width = getResources().getDimensionPixelSize(CompoundButtonParams.Size.MEDIUM.getIconSizeResId());
                    tvText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(CompoundButtonParams.Size.MEDIUM.getTextSizeResId()));
                    break;

                case SMALL:
                default:
                    setMinimumHeight(getResources().getDimensionPixelSize(CompoundButtonParams.Size.SMALL.getMinHeightResId()));
                    iconLayoutParams.height = getResources().getDimensionPixelSize(CompoundButtonParams.Size.SMALL.getIconSizeResId());
                    iconLayoutParams.width = getResources().getDimensionPixelSize(CompoundButtonParams.Size.SMALL.getIconSizeResId());
                    tvText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(CompoundButtonParams.Size.SMALL.getTextSizeResId()));

            }
        }

    }

    private void setCompoundButton(CompoundButtonParams.CompoundButtonType compoundButtonType, CompoundButtonParams.CompoundButtonColorStateList compoundButtonColorStateList) {
        checkView.removeAllViews();
        LayoutInflater.from(getContext()).inflate(compoundButtonType.getLayoutResId(), checkView, true);

        compoundButton = (android.widget.CompoundButton) checkView.getChildAt(0);
        ColorStateList drawable = ContextCompat.getColorStateList(getContext(), compoundButtonColorStateList.getColorStateList());
        CompoundButtonCompat.setButtonTintList(compoundButton, drawable);
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
        changeState();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        changeState();
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        this.isChecked = checked;
        changeState();
    }

    public void stopUserToChangeState(boolean stopUserToChangeState) {
        this.stopUserStateChange = stopUserToChangeState;
    }

}
