package com.spareroom.ui.util

import android.app.Activity
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import com.spareroom.R
import com.spareroom.controller.AppVersion
import com.spareroom.ui.screen.customtabs.*

object WebLauncher {

    @JvmStatic
    fun launchBetaProgram(activity: Activity) {
        openCustomTab(activity, Uri.parse(AppVersion.flavor().betaUrl), WebBrowserFallback())
    }

    @JvmStatic
    fun launchTerms(activity: Activity) {
        val title = activity.getString(R.string.loginMethodFragment_termsPrivacy_terms)
        openCustomTab(activity, Uri.parse(AppVersion.flavor().termsDomain), WebViewActivityFallback(title))
    }

    @JvmStatic
    fun launchPrivacy(activity: Activity) {
        val title = activity.getString(R.string.loginMethodFragment_termsPrivacy_policy)
        openCustomTab(activity, Uri.parse(AppVersion.flavor().privacyDomain), WebViewActivityFallback(title))
    }

    private fun openCustomTab(activity: Activity, uri: Uri, customTabFallback: CustomTabFallback) {
        val customTabsIntent = customTabIntentBuilder(activity).build()
        CustomTabActivityHelper.openCustomTab(activity, customTabsIntent, uri, customTabFallback)
    }

    private fun customTabIntentBuilder(activity: Activity): CustomTabsIntent.Builder {
        return CustomTabsIntent.Builder()
            .setToolbarColor(ContextCompat.getColor(activity, R.color.material_deep_sea_blue))
            .setShowTitle(true)
    }
}