package com.spareroom.ui.screen.customtabs

import android.app.Activity
import android.content.Intent
import android.net.Uri
import com.spareroom.ui.screen.WebViewActivity

class WebViewActivityFallback(private val title: String) : CustomTabFallback {

    override fun openUri(activity: Activity, uri: Uri) {
        val intent = Intent(activity, WebViewActivity::class.java)
        intent.putExtra(WebViewActivity.INTENT_EXTRA_URL, uri.toString())
        intent.putExtra(WebViewActivity.INTENT_EXTRA_TITLE, title)
        activity.startActivity(intent)
    }
}