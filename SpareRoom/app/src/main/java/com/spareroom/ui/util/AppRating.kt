package com.spareroom.ui.util

import android.content.Intent
import android.net.Uri
import com.spareroom.R
import com.spareroom.controller.AppVersion
import com.spareroom.integration.sharedpreferences.EventsDao
import com.spareroom.integration.sharedpreferences.UserEvents
import com.spareroom.ui.screen.*
import java.util.*
import javax.inject.Inject

private const val GOOGLE_PLAY_APP_URL_TEMPLATE = "market://details?id="
private const val GOOGLE_PLAY_WEBSITE_URL_TEMPLATE = "http://play.google.com/store/apps/details?id="
private const val INITIAL_POPUP_MIN_EVENTS_COUNT = 15
private const val INITIAL_POPUP_MIN_DAYS_OF_USAGE = 2
private const val INITIAL_POPUP_MIN_ADVERT_LIVE_LENGTH = 2
private const val REMINDER_POPUP_DAYS_OF_USAGE = 10

class AppRating @Inject constructor(
    private val eventsDao: EventsDao,
    private val connectivityChecker: ConnectivityChecker) {

    @JvmOverloads
    fun showRatingPopupIfPossible(activity: Activity, advertDeactivated: Boolean = false, dateAdvertLastLive: Calendar? = null) {
        if (activity.isActivityDestroyed || !connectivityChecker.isConnected())
            return

        val events = eventsDao.getEvents()
        if (shouldShowInitialRatingPopup(events)
            || shouldShowInitialRatingPopupAfterAdvertDeactivation(advertDeactivated, dateAdvertLastLive, events)) {
            RatingInitialDialog.getInstance().show(activity.supportFragmentManager, RatingInitialDialog.TAG)
        } else if (shouldShowReminderRatingPopup(events)) {
            RatingReminderDialog.getInstance().show(activity.supportFragmentManager, RatingReminderDialog.TAG)
        }
    }

    fun rateApp(context: Activity) {
        val rateAppInGooglePlayAppIntent = Intent(Intent.ACTION_VIEW, Uri.parse("$GOOGLE_PLAY_APP_URL_TEMPLATE${AppVersion.appId()}"))
        if (Launcher.canHandleIntent(context, rateAppInGooglePlayAppIntent)) {
            context.startActivity(rateAppInGooglePlayAppIntent)
            return
        }

        val rateAppOnTheWebSiteIntent = Intent(Intent.ACTION_VIEW, Uri.parse("$GOOGLE_PLAY_WEBSITE_URL_TEMPLATE${AppVersion.appId()}"))
        if (Launcher.canHandleIntent(context, rateAppOnTheWebSiteIntent)) {
            context.startActivity(rateAppOnTheWebSiteIntent)
            return
        }

        ToastUtils.showToast(R.string.no_browser)
    }

    fun shouldShowInitialRatingPopup(events: UserEvents): Boolean {
        val totalEventsCount = events.searchCount + events.saveAdvertCount + events.messageAdvertCount

        return events.initialRatingPopupShownDate == null
            && events.searchCount >= 1
            && (events.saveAdvertCount >= 1 || events.messageAdvertCount >= 1)
            && totalEventsCount >= INITIAL_POPUP_MIN_EVENTS_COUNT
            && events.daysOfUsage >= INITIAL_POPUP_MIN_DAYS_OF_USAGE
    }

    fun shouldShowReminderRatingPopup(events: UserEvents): Boolean {
        return !events.appRated
            && events.reminderRatingPopupShownDate == null
            && events.initialRatingPopupShownDate != null
            && DateUtils.getTimeDifferenceInDays(Calendar.getInstance(), events.initialRatingPopupShownDate) >= REMINDER_POPUP_DAYS_OF_USAGE
    }

    fun shouldShowInitialRatingPopupAfterAdvertDeactivation(advertDeactivated: Boolean, dateAdvertLastLive: Calendar?,
                                                            events: UserEvents): Boolean {
        if (advertDeactivated
            && dateAdvertLastLive != null
            && events.initialRatingPopupShownDate == null
            && events.daysOfUsage >= INITIAL_POPUP_MIN_DAYS_OF_USAGE) {

            return DateUtils.getTimeDifferenceInDays(Calendar.getInstance(), dateAdvertLastLive) >= INITIAL_POPUP_MIN_ADVERT_LIVE_LENGTH
        }

        return false
    }
}