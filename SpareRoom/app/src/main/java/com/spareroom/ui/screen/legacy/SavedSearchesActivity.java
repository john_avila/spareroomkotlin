package com.spareroom.ui.screen.legacy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.controller.legacy.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.model.business.*;
import com.spareroom.ui.anim.PushInAnim;
import com.spareroom.ui.screen.Activity;

import java.util.*;

import androidx.appcompat.view.ActionMode;

import static com.spareroom.ui.screen.legacy.ResultsActivity.BREADCRUMBS;

public class SavedSearchesActivity extends Activity {

    private ListView _lvSaved;
    private AsyncTaskManager _asyncManager;

    private ActionMode _actionMode;

    private ProgressBar _pb;

    private int _firstMaxResults;

    private SavedSearchesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_savedsearches);

        float screenHeight = SpareroomApplication.getInstance(
                getApplicationContext()
        ).getSpareroomContext().get_screenHeight();
        float maxResults = screenHeight / 80;
        int firstPages = (int) Math.floor((maxResults / 10) + 1);
        _firstMaxResults = firstPages * 10;

        setToolbar();

        adapter = new SavedSearchesAdapter();
        _lvSaved = findViewById(R.id.activity_savedSearches_lvSaved);
        _lvSaved.setAdapter(adapter);

        _asyncManager = new AsyncTaskManager();

        _pb = findViewById(R.id.activity_savedsearches_pb);

        launchSavedSearchList();
    }

    private void launchSavedSearchList() {
        _pb.setVisibility(View.VISIBLE);

        SpareroomApplication srApplication = SpareroomApplication
                .getInstance(getApplicationContext());

        SavedSearchListAsyncTask at = new SavedSearchListAsyncTask(srApplication.getSearchFacade(), srApplication.getSpareroomContext(), _savedSearchListListener);
        at.execute(new Parameters());
        _asyncManager.register(at);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.delete_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_item_delete:
                adapter.editMode();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        _asyncManager.cancelAll();
    }

    private final IAsyncResult _savedSearchListListener = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            _pb.setVisibility(View.GONE);

            if (o instanceof SavedSearchList) {
                int savedSearchesCount = ((SavedSearchList) o).size();
                setSubtitle(String.format(AppVersion.flavor().getLocale(), "%s %s",
                        savedSearchesCount,
                        getResources().getQuantityString(R.plurals.searches, savedSearchesCount).toLowerCase()));
                adapter.update((SavedSearchList) o);
            }

        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(SavedSearchesActivity.this).show();
        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(SavedSearchesActivity.this).show();
        }

        @Override
        public void handleServiceUnavailableException(String message) {

            if ((message != null) && (message.contains("Certificate not valid until"))) {
                Calendar calendar = Calendar.getInstance();
                Calendar certificateDate = new GregorianCalendar(2009, Calendar.APRIL, 2, 13, 0);

                if (calendar.before(certificateDate)) {
                    DialogUtil.timeDateDialog(SavedSearchesActivity.this).show();
                }
            }
        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(SavedSearchesActivity.this).show();
        }
    };

    private final IAsyncResult _savedSearchDeleteListener = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            _pb.setVisibility(View.GONE);

            AnalyticsTrackerComposite.getInstance().trackEvent_SavedSearch_DeleteSavedSearch();

            Toast.makeText(SavedSearchesActivity.this, "Deleted", Toast.LENGTH_LONG).show();
            launchSavedSearchList();
        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(SavedSearchesActivity.this).show();

        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(SavedSearchesActivity.this).show();

        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(SavedSearchesActivity.this).show();

        }
    };

    private class SavedSearchesAdapter extends BaseAdapter implements OnItemClickListener, IAsyncResult {
        private SavedSearchList _lSavedSearch = new SavedSearchList();
        private final TreeMap<Integer, CheckBox> _tChecked2 = new TreeMap<>();

        private int _checkboxWidth = 0;
        private boolean _isEditModeEnabled = false;

        private void update(SavedSearchList l) {
            if (l != null)
                _lSavedSearch = l;

            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return _lSavedSearch.size();
        }

        @Override
        public Object getItem(int position) {
            return _lSavedSearch.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private void deselectAll() {
            for (CheckBox cb : _tChecked2.values()) {
                cb.setChecked(false);
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            SavedSearch savedSearch = _lSavedSearch.get(position);

            if (view == null) {
                final LayoutInflater vi = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = vi.inflate(R.layout.activity_savedsearches_item, parent, false);
            }

            CheckBox cb = view.findViewById(R.id.activity_savedsearches_item_cb);
            TextView tvTitle = view.findViewById(R.id.activity_savedsearches_item_tvTitle);
            TextView tvDescription = view.findViewById(R.id.activity_savedsearches_item_tvDescription);

            if (_checkboxWidth == 0) {
                Display display = SavedSearchesActivity.this.getWindowManager().getDefaultDisplay();
                cb.measure(display.getWidth(), display.getHeight());

                _checkboxWidth = cb.getMeasuredWidth();
            }

            if (!_isEditModeEnabled) {
                cb.getLayoutParams().width = 0;
                cb.requestLayout();
            }

            if (_tChecked2.containsKey(position))
                cb.setChecked(true);
            else
                cb.setChecked(false);

            tvTitle.setText(savedSearch.get_name());
            CheckBoxOnClickListener checkBoxOnClickListener = new CheckBoxOnClickListener(position);
            cb.setOnClickListener(checkBoxOnClickListener);

            StringBuilder desc = new StringBuilder();
            LinkedList<String> paramDesc = savedSearch.get_paramDesc();
            for (int i = 0; i < (paramDesc.size() - 1); i++) {
                desc.append(getParam(paramDesc.get(i))).append(", ");
            }
            desc.append(getParam(paramDesc.get(savedSearch.get_paramDesc().size() - 1)));
            tvDescription.setText(desc.toString());

            OnClickListener listener = new OnItemClick(savedSearch, checkBoxOnClickListener, cb);
            view.setOnClickListener(listener);

            return view;
        }

        private String getParam(String param) {
            return AbstractAd.DSS_WELCOME.equalsIgnoreCase(param) ? getString(R.string.housing_benefit_recipients) : param;
        }

        private void editMode() {
            _actionMode = startSupportActionMode(new ModeCallback());
            _isEditModeEnabled = true;

            for (int i = 0; i < _lvSaved.getCount(); i++) {
                View row = _lvSaved.getChildAt(i);
                if (row != null) {
                    CheckBox cb = row.findViewById(R.id.activity_savedsearches_item_cb);

                    PushInAnim anim = new PushInAnim(cb, _checkboxWidth, false);
                    anim.setDuration(0);
                    cb.startAnimation(anim);
                }
            }
            _lvSaved.requestLayout();
        }

        private class OnItemClick implements OnClickListener {
            private final SavedSearch _savedSearch;
            private final CheckBoxOnClickListener checkBoxOnClickListener;
            private final CheckBox checkBox;

            OnItemClick(SavedSearch savedSearch, CheckBoxOnClickListener checkBoxOnClickListener, CheckBox checkBox) {
                _savedSearch = savedSearch;
                this.checkBoxOnClickListener = checkBoxOnClickListener;
                this.checkBox = checkBox;
            }

            @Override
            public void onClick(View v) {
                if (_isEditModeEnabled) {
                    checkBox.setChecked(!checkBox.isChecked());
                    checkBoxOnClickListener.onClick(checkBox);
                    return;
                }

                setSearch();

                AnalyticsTrackerComposite.getInstance().trackEvent_SavedSearch_LaunchSavedSearch();

                Intent activityResults = new Intent();
                activityResults.setClass(SavedSearchesActivity.this, ResultsActivity.class);
                activityResults.putExtra("search", _savedSearch.get_location());
                activityResults.putExtra("miles_from_max", _savedSearch.getMilesFromMax());
                activityResults.putExtra("type", _savedSearch.get_searchType());
                activityResults.putExtra(BREADCRUMBS, "from: SavedSearchesActivity; savedSearchId: " + _savedSearch.get_searchId());
                activityResults.putExtra("saved", true);
                startActivity(activityResults);

            }

            private void setSearch() {
                Parameters currentSearch = new Parameters();
                currentSearch.add(SearchParamEnum.WHERE, _savedSearch.get_location());
                currentSearch.add(SearchParamEnum.PAGE, "1");
                currentSearch.add(SearchParamEnum.MAX_PER_PAGE, Integer.toString(_firstMaxResults));
                currentSearch.add(SearchParamEnum.FEATURED, "1");
                currentSearch.add(_savedSearch.get_parameters());
                SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext().updateCurrentSearch(currentSearch);
            }
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        }

        @Override
        public void update(Object o) {
            Toast.makeText(SavedSearchesActivity.this, "Search replaced", Toast.LENGTH_LONG).show();
            finish();
        }

        private final class ModeCallback implements ActionMode.Callback {

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                // Create the menu from the xml file
                MenuInflater inflater = SavedSearchesActivity.this.getMenuInflater();
                inflater.inflate(R.menu.action_mode_delete_menu, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                // Here, you can check selected items to adapt available actions
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                deselectAll();

                if (mode == _actionMode) {
                    _actionMode = null;
                }

                _isEditModeEnabled = false;

                for (int i = 0; i < _lvSaved.getCount(); i++) {
                    View row = _lvSaved.getChildAt(i);
                    if (row != null) {
                        CheckBox cb = row.findViewById(R.id.activity_savedsearches_item_cb);

                        PushInAnim anim = new PushInAnim(cb, _checkboxWidth, true);
                        anim.setDuration(0);
                        cb.startAnimation(anim);
                    }
                }
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menu_item_delete:
                        for (int i = 0; i < _lSavedSearch.size(); i++) {
                            if (_tChecked2.get(i) != null) {
                                Parameters p = new Parameters();
                                p.add("search_id", _lSavedSearch.get(i).get_searchId());
                                p.add("format", "json");
                                p.add("delete", "1");

                                SavedSearchDeleteAsyncTask at =
                                        new SavedSearchDeleteAsyncTask(
                                                SpareroomApplication.getInstance(getApplicationContext()).getSearchFacade(),
                                                SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                                                SavedSearchesActivity.this._savedSearchDeleteListener);
                                _asyncManager.register(at);
                                at.execute(p);
                            }
                        }
                        break;
                }

                mode.finish();
                return true;
            }
        }

        private class CheckBoxOnClickListener implements OnClickListener {
            private final int _position;

            CheckBoxOnClickListener(int position) {
                _position = position;
            }

            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    _tChecked2.put(_position, (CheckBox) v);
                } else {
                    _tChecked2.remove(_position);
                }
                if (!_tChecked2.isEmpty()) {
                    if (_actionMode == null)
                        _actionMode = startSupportActionMode(new ModeCallback());
                } else {
                    if (_actionMode != null)
                        _actionMode.finish();
                }
            }
        }

        @Override
        public void handleInvalidUserException(String message) {

        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(SavedSearchesActivity.this).show();

        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(SavedSearchesActivity.this).show();

        }

        @Override
        public void handleServiceUnavailableException(String message) {
            DialogUtil.updateDialog(SavedSearchesActivity.this).show();

        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(SavedSearchesActivity.this).show();

        }
    }
}
