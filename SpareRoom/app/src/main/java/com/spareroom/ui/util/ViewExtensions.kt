package com.spareroom.ui.util

import android.view.View

/**
 * Set visibility to View.GONE
 */
fun View.hide() {
    visibility = View.GONE
}

/**
 * Set visibility to View.VISIBLE
 */
fun View.show() {
    visibility = View.VISIBLE
}

fun View.isVisible() = View.VISIBLE == visibility