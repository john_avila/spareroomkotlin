package com.spareroom.ui.screen.legacy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.model.business.PlaceAdFullAreaSet;
import com.spareroom.ui.util.ConnectivityChecker;

import javax.inject.Inject;

public class PlaceAdWantedAreasLookingInFragment extends PlaceAdFragment implements Injectable {

    @Inject
    ConnectivityChecker connectivityChecker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_wanted_areas_looking_in, container, false);

        setTitle(getString(R.string.place_ad_wanted_areas_looking_in));

        View bSelect = rootView.findViewById(R.id.place_ad_wanted_areas_looking_in_bSelect);

        bSelect.setOnClickListener(new SelectOnClickListener());

        return rootView;
    }

    private class SelectOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            if (connectivityChecker.isConnected()) {
                DraftAdWanted draft = (DraftAdWanted) getDraftAd();
                PlaceAdFullAreaSet areaSet = draft.get_areas();
                Intent activityAreas = new Intent();
                activityAreas.setClass(getActivity(), PlaceAdAreasActivity.class);
                if ((areaSet != null) && (areaSet.size() > 0)) {
                    activityAreas.putExtra("areas", areaSet);
                }
                startActivityForResult(activityAreas, 1478);
            } else {
                Toast.makeText(getActivity(), getString(R.string.no_connection), Toast.LENGTH_LONG).show();
            }
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1478) {
            if (resultCode == Activity.RESULT_OK) {
                PlaceAdFullAreaSet areas = (PlaceAdFullAreaSet) data.getSerializableExtra("areas");
                DraftAdWanted draft = (DraftAdWanted) getDraftAd();
                draft.set_areas(areas);
                if ((areas != null) && (areas.size() > 0)) {
                    finish();
                }
            }
        }
    }
}
