package com.spareroom.ui.screen

import android.os.Bundle
import android.view.MenuItem
import android.webkit.WebView
import android.webkit.WebViewClient
import com.spareroom.R
import com.spareroom.ui.screen.customtabs.ProgressWebChromeClient
import kotlinx.android.synthetic.main.web_view_activity.*

class WebViewActivity : Activity() {

    companion object {
        const val INTENT_EXTRA_URL = "IntentExtraUrl"
        const val INTENT_EXTRA_TITLE = "IntentExtraTitle"
    }

    private val webView by lazy { WebView(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.web_view_activity)
        setToolbar(intent.getStringExtra(INTENT_EXTRA_TITLE), true, R.drawable.ic_close)
        setUpWebView()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return handleHomeButton(item)
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
            return
        }

        super.onBackPressed()
    }

    private fun setUpWebView() {
        webViewContainer.addView(webView)

        webView.webChromeClient = ProgressWebChromeClient(webViewProgressBar)
        webView.webViewClient = WebViewClient()
        webView.loadUrl(intent.getStringExtra(INTENT_EXTRA_URL))
    }

}