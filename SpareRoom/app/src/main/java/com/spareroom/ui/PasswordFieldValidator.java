package com.spareroom.ui;

/**
 * Created by manuel on 04/11/2016.
 */

public class PasswordFieldValidator implements FieldValidator {
    public static final int VALID = 0;
    public static final int EMPTY = 1;
    public static final int TOO_SHORT = 2;
    public static final int TOO_LONG = 3;
    public static final int INVALID_CHARACTERS = 4;

    @Override
    public int validate(String field) {
        if ((field == null) || field.isEmpty())
            return EMPTY;

        if (field.length() < 4)
            return TOO_SHORT;

        return VALID;
    }
}
