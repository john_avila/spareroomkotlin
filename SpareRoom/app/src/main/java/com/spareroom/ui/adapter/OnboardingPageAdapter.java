package com.spareroom.ui.adapter;

import android.content.Context;
import android.view.View;

import com.spareroom.R;
import com.spareroom.ui.screen.Fragment;
import com.spareroom.ui.screen.OnboardingFragment;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class OnboardingPageAdapter extends FragmentStatePagerAdapter {
    private final String[] titlesIds, bodyTextsId;
    private final int numberOfPages;

    public OnboardingPageAdapter(Context context, FragmentManager fm, int numberOfPages) {
        super(fm);
        this.numberOfPages = numberOfPages;
        titlesIds = context.getResources().getStringArray(R.array.onboarding_titles);
        bodyTextsId = context.getResources().getStringArray(R.array.onboarding_bodies);
    }

    @Override
    public Fragment getItem(int position) {
        return OnboardingFragment.newInstance(position, titlesIds[position], bodyTextsId[position]);
    }

    @Override
    public int getCount() {
        return numberOfPages;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        view.setTag(((OnboardingFragment) object).getPosition());
        return super.isViewFromObject(view, object);
    }

}
