package com.spareroom.ui.filters.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.spareroom.ui.filters.adapter.viewparams.TitleViewParams;
import com.spareroom.ui.filters.provider.FilterItem;

public class TitleViewHolder extends BaseFilterViewHolder {
    private final TextView title;

    public TitleViewHolder(View title) {
        super(title);
        this.title = (TextView) title;
    }

    @Override
    public void bind(FilterItem filterItem) {
        title.setText(((TitleViewParams) filterItem.params()).title());
    }

}
