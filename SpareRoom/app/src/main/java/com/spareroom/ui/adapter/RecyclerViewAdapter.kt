package com.spareroom.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class RecyclerViewAdapter<T, VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH> {

    protected lateinit var context: Context
    protected val inflater: LayoutInflater by lazy { LayoutInflater.from(context) }
    private val items = mutableListOf<T>()

    protected constructor()

    protected constructor(context: Context) {
        this.context = context
    }

    protected fun update(newItems: List<T>, notifyDataSetChanged: Boolean) {
        items.clear()
        items.addAll(newItems)

        if (notifyDataSetChanged)
            notifyDataSetChanged()
    }

    fun update(diffResult: DiffUtil.DiffResult, items: List<T>) {
        update(items, false)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemCount() = items.size

    protected fun getItem(position: Int) = items[position]
}
