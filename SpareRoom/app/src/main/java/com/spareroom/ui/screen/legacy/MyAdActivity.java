package com.spareroom.ui.screen.legacy;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.controller.legacy.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.googleplay.billing.*;
import com.spareroom.integration.webservice.exception.*;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.model.business.*;
import com.spareroom.ui.screen.*;
import com.spareroom.ui.util.*;
import com.spareroom.viewmodel.MyAdvertViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

public class MyAdActivity extends InjectableActivity implements IAsyncResult {

    public static final String ADVERT_INFO = "advert_info";
    public static final String ID = "id";

    private static final String OFFERED = "offered";
    private static final String WANTED = "wanted";
    private static final String TYPE = "type";

    private IabHelper mHelper;
    private static final int RC_REQUEST = 10002;

    private static final String EXTRA_LISTING_SKU = AppVersion.appId() + ".1extralisting1year";

    private View _upgradeView;
    private View _photosView;
    private View _renewView;
    private View _deactivateView;
    private View _infoView;
    private View progressBar;

    private TextView _tvViewsToday;
    private TextView _tvViewsLastWk;
    private TextView _tvContactsToday;
    private TextView _tvContactsLastWk;

    private TextView _tvAdStatus;

    private AsyncTaskManager _asyncManager;

    private String _advertId;
    private String _advertType;

    private String adStatus;

    private Calendar dateLastLive;
    private String _dateAdPlaced;
    private String _dateLastRenewed;
    private String _daysLeft;
    private String _upgradeApplied;
    private MyAdvertViewModel viewModel;

    /**
     * whether the retrieval of upgrades from Google Play was finished. If <code>true</code>,
     * <code>_upgradeGooglePlay</code> would be set.
     */
    private boolean _isGooglePlayDataReady = false;
    /**
     * upgrade retrieved from Google Play. Without it, a purchase flow cannot be started.
     */
    private Upgrade _upgradeGooglePlay = null;

    @Inject
    AppRating appRating;

    @Inject
    SpareroomContext spareroomContext;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    public static void startForResult(@NonNull Fragment fragment, @NonNull AdvertInfo advertInfo, boolean offered, int requestCode) {
        Intent intent = new Intent(fragment.getActivity(), MyAdActivity.class)
                .putExtra(ADVERT_INFO, advertInfo)
                .putExtra(TYPE, offered ? OFFERED : WANTED);

        fragment.startActivityForResult(intent, requestCode);
    }

    @Override
    public void finish() {
        Intent intent = new Intent();
        intent.putExtra(ID, _advertId != null ? _advertId : "");

        AbstractAd advert = viewModel.getMyAdvert().getAdvert();
        if (advert != null)
            intent.putExtra(ADVERT_INFO, AdvertInfo.fromAdvert(advert));

        setResult(Activity.RESULT_OK, intent);
        super.finish();
    }

    private void launchMyAdTask() {
        Parameters params = new Parameters();
        params.add("advert_id", _advertId);
        params.add("advert_type", _advertType);

        SpareroomApplication app = SpareroomApplication.getInstance(
                getApplicationContext()
        );

        if (OFFERED.equals(_advertType)) {
            MyOfferedAdAsyncTask at = new MyOfferedAdAsyncTask(app.getAccountFacade(), this);
            at.execute(params);
            _asyncManager.cancelAll();
        }
        if (WANTED.equals(_advertType)) {
            MyWantedAdAsyncTask at = new MyWantedAdAsyncTask(app.getAccountFacade(), this);
            at.execute(params);
            _asyncManager.cancelAll();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_my_ad);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MyAdvertViewModel.class);

        AdvertInfo advertInfo = (AdvertInfo) getIntent().getSerializableExtra(ADVERT_INFO);
        _advertId = advertInfo.getId();
        _advertType = getIntent().getStringExtra(TYPE);
        _dateAdPlaced = advertInfo.getDatePlaced();
        _daysLeft = advertInfo.getUpgradeDaysLeft();
        _dateLastRenewed = advertInfo.getDateLastRenewed();
        _upgradeApplied = advertInfo.getUpgradeApplied();
        dateLastLive = advertInfo.getDateLastLive().getDate();

        setToolbar(advertInfo.getTitle());

        _asyncManager = new AsyncTaskManager();

        initHelper();

        // Init default values that cannot be customized on the layout because we are reusing layouts with the include tag

        ((TextView) findViewById(R.id.fragment_my_ad_views_pastWk).findViewById(R.id.fragment_my_ad_stats_cell_tvLabel)).setText(R.string.fragment_my_ad_past_week);
        ((TextView) findViewById(R.id.fragment_my_ad_contacts_pastWk).findViewById(R.id.fragment_my_ad_stats_cell_tvLabel)).setText(R.string.fragment_my_ad_past_week);

        _tvViewsToday = findViewById(R.id.fragment_my_ad_views_today).findViewById(R.id.fragment_my_ad_stats_cell_tvCount);
        _tvViewsLastWk = findViewById(R.id.fragment_my_ad_views_pastWk).findViewById(R.id.fragment_my_ad_stats_cell_tvCount);
        _tvContactsToday = findViewById(R.id.fragment_my_ad_contacts_today).findViewById(R.id.fragment_my_ad_stats_cell_tvCount);
        _tvContactsLastWk = findViewById(R.id.fragment_my_ad_contacts_pastWk).findViewById(R.id.fragment_my_ad_stats_cell_tvCount);

        _upgradeView = findViewById(R.id.fragment_my_ad_upgrade);
        _photosView = findViewById(R.id.fragment_my_ad_photos);
        _renewView = findViewById(R.id.fragment_my_ad_renew);
        _deactivateView = findViewById(R.id.fragment_my_ad_deactivate);
        _infoView = findViewById(R.id.fragment_my_ad_info);
        progressBar = findViewById(R.id.progressBar);

        TextView tvUpgrade = _upgradeView.findViewById(R.id.fragment_my_ad_button_cell_tvText);

        _tvAdStatus = findViewById(R.id.fragment_myAd_adStatus);
        TextView tvUpgradeStatus = findViewById(R.id.fragment_myAd_adUpgradeStatus);

        tvUpgrade.setText(R.string.fragment_my_ad_edit);
        ((TextView) _photosView.findViewById(R.id.fragment_my_ad_button_cell_tvText)).setText(R.string.fragment_my_ad_photos);
        ((TextView) _renewView.findViewById(R.id.fragment_my_ad_button_cell_tvText)).setText(R.string.fragment_my_ad_renew);
        ((TextView) _deactivateView.findViewById(R.id.fragment_my_ad_button_cell_tvText)).setText(R.string.fragment_my_ad_deactivate);
        ((TextView) _infoView.findViewById(R.id.fragment_my_ad_button_cell_tvText)).setText(R.string.info);

        ((ImageView) _upgradeView.findViewById(R.id.fragment_my_ad_button_cell_ivIcon)).setImageResource(R.drawable.ic_edit);
        ((ImageView) _photosView.findViewById(R.id.fragment_my_ad_button_cell_ivIcon)).setImageResource(R.drawable.ic_photos);
        ((ImageView) _renewView.findViewById(R.id.fragment_my_ad_button_cell_ivIcon)).setImageResource(R.drawable.ic_renew);
        ((ImageView) _deactivateView.findViewById(R.id.fragment_my_ad_button_cell_ivIcon)).setImageResource(R.drawable.ic_activate);
        ((ImageView) _infoView.findViewById(R.id.fragment_my_ad_button_cell_ivIcon)).setImageResource(R.drawable.ic_info);

        adStatus = advertInfo.getStatus();
        setAdStatus();
        updateDeactivateButton();

        Session session = spareroomContext.getSession();
        String upgrade = session.get_upgradeProduct();

        if (upgrade == null || upgrade.equals("")) {
            tvUpgradeStatus.setText(R.string.myAdActivity_upgradeNow);
            tvUpgradeStatus.setBackgroundResource(R.color.orange);
        } else {
            if (session.get_upgradeTimeLeft().contains("2 days") || session.get_upgradeTimeLeft().contains("1 day") || !session.get_upgradeTimeLeft().contains("day"))
                tvUpgradeStatus.setText(
                        String.format(AppVersion.flavor().getLocale(), "%s: %s %s", getString(R.string.myAdActivity_upgraded), session.get_upgradeTimeLeft(), getString(R.string.myAdActivity_left)));
            else
                tvUpgradeStatus.setText(R.string.myAdActivity_upgraded);
            tvUpgradeStatus.setBackgroundResource(R.color.green);
        }

        tvUpgradeStatus.setOnClickListener(v -> {
            Intent upgradeActivityFragment = new Intent();
            upgradeActivityFragment.putExtra("context_id", "46");
            upgradeActivityFragment.setClass(MyAdActivity.this, UpgradeFragmentActivity.class);
            startActivity(upgradeActivityFragment);
        });

        launchMyAdTask();
        viewModel.getMyAdvert().observe(this, this::advertStatusUpdated);
    }

    private void advertStatusUpdated(MyAdvertResponse response) {
        UiUtils.hideViews(progressBar);
        if (response instanceof AdvertRenewed) {
            advertRenewed(((AdvertRenewed) response).getAdvert());
        } else if (response instanceof AdvertActivated) {
            advertActivated(((AdvertActivated) response).getAdvert());
        } else if (response instanceof AdvertDeactivated) {
            advertDeactivated(((AdvertDeactivated) response).getAdvert());
        } else if (response instanceof FailedToDeactivate) {
            failedToDeactivate(((FailedToDeactivate) response).getThrowable());
        } else if (response instanceof FailedToRenew) {
            handleStatusUpdateFailure(((FailedToRenew) response).getThrowable());
        } else if (response instanceof FailedToActivate) {
            handleStatusUpdateFailure(((FailedToActivate) response).getThrowable());
        }
    }

    private void advertRenewed(AbstractAd advert) {
        ToastUtils.showToast(R.string.advert_renewed);
        updateStatusUi(advert);
    }

    private void advertActivated(AbstractAd advert) {
        updateStatusUi(advert);
    }

    private void advertDeactivated(AbstractAd advert) {
        ToastUtils.showToast(R.string.advert_deactivated);
        updateStatusUi(advert);
        appRating.showRatingPopupIfPossible(this, true, dateLastLive);
    }

    private void updateStatusUi(AbstractAd advert) {
        adStatus = advert.getStatus();
        setAdStatus();
        updateDeactivateButton();
    }

    private void failedToDeactivate(Throwable throwable) {
        String message = errorMessage(throwable);
        if (isSessionOrConnectivityError(throwable)) {
            ToastUtils.showToast(message);
        } else {
            showErrorPopup(message);
        }
    }

    private void handleStatusUpdateFailure(Throwable throwable) {
        String message = errorMessage(throwable);
        if (isSessionOrConnectivityError(throwable)) {
            ToastUtils.showToast(message);
        } else if (throwable instanceof NotEnoughListingsException) {
            showNotEnoughListingsPopup(message);
        } else {
            showErrorPopup(message);
        }
    }

    private void showNotEnoughListingsPopup(String message) {
        Session session = spareroomContext.getSession();

        if (!session.is_paid()) {
            getUpgradeDialog().show();
        } else if (_isGooglePlayDataReady) {
            getExtraListingDialog(message).show();
        } else {
            getGooglePlayDialog(message).show();
        }
    }

    private boolean isSessionOrConnectivityError(Throwable throwable) {
        return throwable instanceof NetworkConnectivityException || throwable instanceof NotLoggedInException;
    }

    private String errorMessage(Throwable throwable) {
        if (throwable instanceof NetworkConnectivityException) {
            return getString(R.string.no_connection);
        } else if (throwable instanceof NotLoggedInException) {
            return getString(R.string.please_log_in);
        } else if (!StringUtils.isNullOrEmpty(throwable.getMessage())) {
            return throwable.getMessage();
        } else {
            return getString(R.string.errorGeneric_title);
        }
    }

    private void showErrorPopup(String message) {
        InfoFragment.getInstance(message).show(this, InfoFragment.TAG);
    }

    private void initHelper() {
        UpgradeList ul = new UpgradeList();
        Upgrade u = new Upgrade();
        u.set_code(EXTRA_LISTING_SKU);
        ul.add(u);

        try {
            mHelper = new IabHelper(MyAdActivity.this, AppVersion.flavor().getKey());
        } catch (NullPointerException | IllegalStateException e) {
            AnalyticsTrackerComposite.getInstance().trackEvent_Upgrade_BillingUnavailableExtraListing_Create();
        }
        try {
            mHelper.enableDebugLogging(false);
        } catch (NullPointerException | IllegalStateException e) {
            AnalyticsTrackerComposite.getInstance().trackEvent_Upgrade_BillingUnavailableExtraListing_DisableDebug();
        }
        try {
            mHelper.startSetup(new UpgradeListOnIabSetupFinishedListener(ul));
        } catch (NullPointerException | IllegalStateException e) {
            AnalyticsTrackerComposite.getInstance().trackEvent_Upgrade_BillingUnavailableExtraListing_Setup();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            if (mHelper != null) {
                mHelper.disposeWhenFinished();
                mHelper = null;
            }
        } catch (IllegalArgumentException e) {
            // another empty catch...
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            // Pass on the activity result to the helper for handling
            if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
                // not handled, so handle it ourselves (here's where you'd
                // perform any handling of activity results not related to in-app
                // billing...
                super.onActivityResult(requestCode, resultCode, data);
            }
        } catch (NullPointerException | IllegalStateException e) {
            AnalyticsTrackerComposite.getInstance().trackEvent_Upgrade_BillingUnavailableExtraListing_HandlePaymentResult();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return handleHomeButton(item);
    }

    private void setAdStatus() {
        switch (adStatus) {
            case AbstractAd.PENDING:
                _tvAdStatus.setBackgroundResource(R.color.yellow_2);
                _tvAdStatus.setText(R.string.pending);
                break;
            case AbstractAd.LIVE:
                _tvAdStatus.setBackgroundResource(R.color.green);
                _tvAdStatus.setText(R.string.active);
                break;
            default:
                _tvAdStatus.setBackgroundResource(R.color.red_1);
                _tvAdStatus.setText(R.string.deactivated);
                break;
        }
    }

    private void updateDeactivateButton() {
        if (AbstractAd.LIVE.equalsIgnoreCase(adStatus) || AbstractAd.PENDING.equalsIgnoreCase(adStatus)) {
            ((ImageView) _deactivateView.findViewById(R.id.fragment_my_ad_button_cell_ivIcon)).setImageResource(R.drawable.ic_deactivate);
            ((TextView) _deactivateView.findViewById(R.id.fragment_my_ad_button_cell_tvText)).setText(R.string.fragment_my_ad_deactivate);
            _deactivateView.setOnClickListener(new DeactivateButtonListener());
        } else {
            ((ImageView) _deactivateView.findViewById(R.id.fragment_my_ad_button_cell_ivIcon)).setImageResource(R.drawable.ic_activate);
            ((TextView) _deactivateView.findViewById(R.id.fragment_my_ad_button_cell_tvText)).setText(R.string.fragment_my_ad_activate);
            _deactivateView.setOnClickListener(new ActivateButtonListener());
        }
    }

    private class ActivateButtonListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            if (!spareroomContext.isUserLoggedIn()) {
                ToastUtils.showToast(R.string.please_log_in);
                return;
            }
            View vContent = LayoutInflater.from(MyAdActivity.this).inflate(R.layout.dialog_message_buttons, null, false);

            TextView tvContent = vContent.findViewById(R.id.dialog_message);
            Button bPositive = vContent.findViewById(R.id.dialog_bPositive);
            Button bNegative = vContent.findViewById(R.id.dialog_bNegative);

            tvContent.setText(R.string.myAdActivity_questionMakeLive);
            bPositive.setText(R.string.myAdActivity_makeLive);

            Dialog d = new AlertDialogBuilder(MyAdActivity.this, vContent).create();
            bPositive.setOnClickListener(new ActivateOnClickListener(d));
            bNegative.setOnClickListener(new DialogUtil.CancelOnClickListener(d));
            d.show();
        }
    }

    private class DeactivateButtonListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            if (!spareroomContext.isUserLoggedIn()) {
                ToastUtils.showToast(R.string.please_log_in);
                return;
            }

            View vContent = LayoutInflater.from(MyAdActivity.this).inflate(R.layout.dialog_message_buttons, null, false);

            TextView tvContent = vContent.findViewById(R.id.dialog_message);
            Button bPositive = vContent.findViewById(R.id.dialog_bPositive);
            Button bNegative = vContent.findViewById(R.id.dialog_bNegative);

            tvContent.setText(R.string.myAdActivity_questionDeactivate);
            bPositive.setText(R.string.myAdActivity_deactivate);

            Dialog d = new AlertDialogBuilder(MyAdActivity.this, vContent).create();
            bPositive.setOnClickListener(new DeactivateOnClickListener(d));
            bNegative.setOnClickListener(new DialogUtil.CancelOnClickListener(d));
            d.show();
        }
    }

    @Override
    public void update(Object o) {
        if (!spareroomContext.isUserLoggedIn()) {
            ToastUtils.showToast(R.string.please_log_in);
            finish();
            return;
        }

        if (_advertType != null && o != null && o instanceof Stats) {
            Stats stats = (Stats) o;
            _tvViewsToday.setText(String.format(AppVersion.flavor().getLocale(), "%d", stats.get_viewsToday()));
            _tvViewsLastWk.setText(String.format(AppVersion.flavor().getLocale(), "%d", stats.get_viewsPastWeek()));
            _tvContactsToday.setText(String.format(AppVersion.flavor().getLocale(), "%d", stats.get_contactsToday()));
            _tvContactsLastWk.setText(String.format(AppVersion.flavor().getLocale(), "%d", stats.get_contactsPastWeek()));
        }

        _upgradeView.setOnClickListener(v -> {
            if (!spareroomContext.isUserLoggedIn()) {
                ToastUtils.showToast(R.string.please_log_in);
                return;
            }

            Intent editAdActivity = new Intent();
            editAdActivity.setClass(MyAdActivity.this, EditAdActivity.class);
            editAdActivity.putExtra("advert_id", _advertId);
            editAdActivity.putExtra("advert_type", _advertType);
            startActivity(editAdActivity);
        });

        _photosView.setOnClickListener(v -> {
            if (!spareroomContext.isUserLoggedIn()) {
                ToastUtils.showToast(R.string.please_log_in);
                return;
            }

            Intent managePhotosActivity = new Intent();
            managePhotosActivity.setClass(MyAdActivity.this, ManagePhotosActivity.class);
            managePhotosActivity.putExtra(ManagePhotosActivity.EXTRA_KEY_FLATSHARE_ID, _advertId);
            managePhotosActivity.putExtra(ManagePhotosActivity.EXTRA_KEY_FLATSHARE_TYPE, _advertType);
            managePhotosActivity.putExtra(
                    ManagePhotosActivity.EXTRA_KEY_PHOTO_UPLOAD_LIMIT,
                    spareroomContext.getSession().getPhotoUploadLimit());
            managePhotosActivity.putExtra(ManagePhotosActivity.EXTRA_KEY_AT_RETRIEVE, RetrievePhotosAsyncTask.class.getName());
            managePhotosActivity.putExtra(ManagePhotosActivity.EXTRA_KEY_AT_EDIT_CAPTION, ChangeOrderPictureAsyncTask.class.getName());
            managePhotosActivity.putExtra(ManagePhotosActivity.EXTRA_KEY_AT_DELETE, DeletePictureAsyncTask.class.getName());
            managePhotosActivity.putExtra(ManagePhotosActivity.EXTRA_KEY_AT_CHANGE_ORDER, ChangeOrderPictureAsyncTask.class.getName());
            startActivity(managePhotosActivity);
        });

        if (AbstractAd.LIVE.equalsIgnoreCase(adStatus) || AbstractAd.PENDING.equalsIgnoreCase(adStatus)) {
            _deactivateView.setOnClickListener(new DeactivateButtonListener());
        } else {
            ((ImageView) _deactivateView.findViewById(R.id.fragment_my_ad_button_cell_ivIcon)).setImageResource(R.drawable.ic_activate);
            ((TextView) _deactivateView.findViewById(R.id.fragment_my_ad_button_cell_tvText)).setText(R.string.fragment_my_ad_activate);

            _deactivateView.setOnClickListener(new ActivateButtonListener());
        }

        _renewView.setOnClickListener(v -> {
            if (!spareroomContext.isUserLoggedIn()) {
                ToastUtils.showToast(R.string.please_log_in);
                return;
            }

            View vContent = LayoutInflater.from(MyAdActivity.this).inflate(R.layout.dialog_message_buttons, null, false);

            TextView tvContent = vContent.findViewById(R.id.dialog_message);
            Button bPositive = vContent.findViewById(R.id.dialog_bPositive);
            Button bNegative = vContent.findViewById(R.id.dialog_bNegative);

            tvContent.setText(R.string.myAdActivity_questionRenew);
            bPositive.setText(R.string.myAdActivity_renew);

            Dialog d = new AlertDialogBuilder(MyAdActivity.this, vContent).create();
            bPositive.setOnClickListener(new RenewOnClickListener(d));
            bNegative.setOnClickListener(new DialogUtil.CancelOnClickListener(d));
            d.show();
        });

        _infoView.setOnClickListener(new InfoOnClickListener());

    }

    @Override
    public void handleInvalidUserException(String message) {

    }

    @Override
    public void handleMissingParameterException(String message) {

    }

    @Override
    public void handleMissingSystemFeatureException(String message) {

    }

    @Override
    public void handleServiceUnavailableException(String message) {

    }

    @Override
    public void handleInconsistentStateException(String message) {

    }

    private Dialog getUpgradeDialog() {
        View vContent = LayoutInflater.from(this).inflate(R.layout.dialog_message_buttons, null);

        TextView tvContent = vContent.findViewById(R.id.dialog_message);
        Button bPositive = vContent.findViewById(R.id.dialog_bPositive);
        Button bNegative = vContent.findViewById(R.id.dialog_bNegative);

        tvContent.setText(R.string.dialog_myAd_maxReached_upgrade_tvMessage);

        bPositive.setText(R.string.dialog_myAd_maxReached_upgrade_bPositive);
        bNegative.setText(R.string.dialog_myAd_maxReached_bNegative);
        bNegative.setVisibility(View.GONE);
        Dialog d = new AlertDialogBuilder(this, vContent).create();

        bPositive.setOnClickListener(new BuyUpgradeOnClickListener(d));
        bNegative.setOnClickListener(new DialogUtil.CancelOnClickListener(d));
        return d;

    }

    private Dialog getExtraListingDialog(String message) {
        View vContent = LayoutInflater.from(this).inflate(R.layout.dialog_message_buttons, null);

        TextView tvContent = vContent.findViewById(R.id.dialog_message);
        Button bPositive = vContent.findViewById(R.id.dialog_bPositive);
        Button bNegative = vContent.findViewById(R.id.dialog_bNegative);

        tvContent.setText(message);

        bPositive.setText(R.string.dialog_myAd_maxReached_bPositive);
        bNegative.setText(R.string.dialog_myAd_maxReached_bNegative);
        Dialog d = new AlertDialogBuilder(this, vContent).create();

        bPositive.setOnClickListener(new BuyExtraListingOnClickListener(d, _upgradeGooglePlay));
        bNegative.setOnClickListener(new DialogUtil.CancelOnClickListener(d));
        return d;
    }

    private Dialog getGooglePlayDialog(String message) {
        View vContent = LayoutInflater.from(this).inflate(R.layout.dialog_message_buttons, null);

        TextView tvContent = vContent.findViewById(R.id.dialog_message);
        Button bPositive = vContent.findViewById(R.id.dialog_bPositive);
        Button bNegative = vContent.findViewById(R.id.dialog_bNegative);

        tvContent.setText(message);

        bPositive.setText(R.string.dialog_myAd_maxReached_bPositive);
        bNegative.setText(R.string.dialog_myAd_maxReached_bNegative);
        Dialog d = new AlertDialogBuilder(this, vContent).create();

        bPositive.setOnClickListener(new GooglePlayUpgradesNotReadyOnClickListener(d));
        bNegative.setOnClickListener(new DialogUtil.CancelOnClickListener(d));
        return d;
    }

    /**
     * Listener that handles an attempt of purchase when the Upgrade from Google Play has not yet
     * been retrieved. This listener will simply display a Toast and dismiss the dialog passed onto
     * it
     */
    private class GooglePlayUpgradesNotReadyOnClickListener implements View.OnClickListener {
        private final Dialog _dialog;

        /**
         * Constructor.
         *
         * @param d the dialog that contains the option for initiating the purchase. <code>d</code>
         *          will be dismissed.
         */
        GooglePlayUpgradesNotReadyOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(MyAdActivity.this, getString(R.string.fragment_myAds_tGooglePlayNotReady), Toast.LENGTH_LONG).show();
            _dialog.dismiss();
        }
    }

    private class ActivateOnClickListener implements OnClickListener {
        private final Dialog _dialog;

        ActivateOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            _dialog.dismiss();
            if (!spareroomContext.isUserLoggedIn()) {
                ToastUtils.showToast(R.string.please_log_in);
                return;
            }

            UiUtils.showViews(progressBar);
            viewModel.getMyAdvert().activate(_advertId, StringUtils.equals(OFFERED, _advertType));
        }
    }

    private class BuyExtraListingOnClickListener implements OnClickListener {
        private final Dialog _dialog;
        private final Upgrade _upgrade;

        BuyExtraListingOnClickListener(Dialog d, Upgrade u) {
            _dialog = d;
            _upgrade = u;
        }

        @Override
        public void onClick(View v) {
            _dialog.dismiss();
            launchPurchaseFlow(EXTRA_LISTING_SKU, _upgrade);
        }
    }

    private class BuyUpgradeOnClickListener implements OnClickListener {
        private final Dialog _dialog;

        BuyUpgradeOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            _dialog.dismiss();
            Intent upgradeActivityFragment = new Intent();
            upgradeActivityFragment.putExtra("context_id", "46");
            upgradeActivityFragment.setClass(MyAdActivity.this, UpgradeFragmentActivity.class);
            startActivity(upgradeActivityFragment);
        }
    }

    private class DeactivateOnClickListener implements OnClickListener {
        private final Dialog _dialog;

        DeactivateOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            _dialog.dismiss();

            if (!spareroomContext.isUserLoggedIn()) {
                ToastUtils.showToast(R.string.please_log_in);
                return;
            }

            UiUtils.showViews(progressBar);
            viewModel.getMyAdvert().deactivate(_advertId, StringUtils.equals(OFFERED, _advertType));
        }
    }

    private class RenewOnClickListener implements OnClickListener {
        private final Dialog _dialog;

        RenewOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            _dialog.dismiss();
            if (!spareroomContext.isUserLoggedIn()) {
                ToastUtils.showToast(R.string.please_log_in);
                return;
            }

            UiUtils.showViews(progressBar);
            viewModel.getMyAdvert().renew(_advertId, StringUtils.equals(OFFERED, _advertType));
        }
    }

    private class InfoOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            if (!spareroomContext.isUserLoggedIn()) {
                ToastUtils.showToast(R.string.please_log_in);
                return;
            }

            View vContent = LayoutInflater.from(MyAdActivity.this).inflate(R.layout.dialog_myad_info, null, false);

            TextView tvTitle = vContent.findViewById(R.id.dialog_title);
            TextView tvAdPlaced = vContent.findViewById(R.id.dialog_myAd_adPlaced_value);
            TextView tvLastRenewed = vContent.findViewById(R.id.dialog_myAd_lastRenewed_value);
            TextView tvDaysLeft = vContent.findViewById(R.id.dialog_myAd_daysLeft_value);
            TextView tvUpgrade = vContent.findViewById(R.id.dialog_myAd_upgrade_value);
            TextView tvUpgradeExpires = vContent.findViewById(R.id.dialog_myAd_upgradeExpires_value);
            Button bPreview = vContent.findViewById(R.id.dialog_myAd_bPreview);

            tvTitle.setText(String.format(AppVersion.flavor().getLocale(), "%s (#%s)", getString(R.string.myAdActivity_advertInformation), _advertId));

            if (_dateAdPlaced != null)
                tvAdPlaced.setText(formatDate(_dateAdPlaced));
            if (_dateLastRenewed != null)
                tvLastRenewed.setText(formatDate(_dateLastRenewed));
            if (_daysLeft != null)
                tvDaysLeft.setText(
                        String.format(
                                AppVersion.flavor().getLocale(),
                                "%s %s",
                                _daysLeft,
                                getString(R.string.myAdActivity_daysRemaining)));

            if (_upgradeApplied != null && !_upgradeApplied.equals("null") && !_upgradeApplied.equals(""))
                tvUpgrade.setText(_upgradeApplied);

            String sUpgradeExpire = spareroomContext.getSession().getUnixAccessExp();
            if (sUpgradeExpire != null && !sUpgradeExpire.equals("null") && !sUpgradeExpire.equals(""))
                tvUpgradeExpires.setText(formatExpirationDate(sUpgradeExpire));

            Dialog d = new AlertDialogBuilder(MyAdActivity.this, vContent).create();
            bPreview.setOnClickListener(new AdOnClickListener(d, _advertId));
            d.show();

        }
    }

    private String formatExpirationDate(String date) {
        Date time = new java.util.Date(Long.parseLong(date) * 1000);
        Locale locale = AppVersion.flavor().getLocale();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(AppVersion.flavor().getDateHourRepresentation(), locale);
        return simpleDateFormat.format(time);

    } //end formatExpirationDate(String date)

    private String formatDate(String date) {
        Locale locale = AppVersion.flavor().getLocale();
        SimpleDateFormat inputFormat = new SimpleDateFormat("dd MMMM yyyy", locale);
        SimpleDateFormat outputFormat = new SimpleDateFormat(AppVersion.flavor().getDateRepresentation(), locale);
        try {
            Date d1 = inputFormat.parse(date);
            date = outputFormat.format(d1);
        } catch (ParseException ignored) {
        }
        return date;
    } //end formatDate(String date)

    private class AdOnClickListener implements OnClickListener {
        private final Dialog dialog;
        private final String _id;

        public AdOnClickListener(Dialog dialog, String id) {
            this.dialog = dialog;
            _id = id;
        }

        @Override
        public void onClick(View v) {
            dialog.dismiss();
            Intent adActivity = new Intent();
            adActivity.setClass(MyAdActivity.this, AdActivity.class);
            adActivity.putExtra(ID, _id);
            adActivity.putExtra("searchType", ((_advertType.equals(OFFERED)) ? SearchType.OFFERED : SearchType.WANTED));
            MyAdActivity.this.startActivityForResult(adActivity, 1);
        }
    }

    private void launchPurchaseFlow(String sku, Upgrade u) {
        String payload = BillingHelper.generatePayload(getApplicationContext());
        try {
            mHelper.launchPurchaseFlow(MyAdActivity.this, sku, RC_REQUEST, new PurchaseFinishedListener(u), payload);
        } catch (NullPointerException | IllegalStateException e) {
            AnalyticsTrackerComposite.getInstance().trackEvent_Upgrade_BillingUnavailableExtraListing_LaunchPurchaseFlow();
        } catch (IabHelper.IabAsyncInProgressException e) {
            // another empty catch...
        }
    }

    private class PurchaseFinishedListener implements IabHelper.OnIabPurchaseFinishedListener {
        private final Upgrade _upgrade;

        PurchaseFinishedListener(Upgrade u) {
            _upgrade = u;
        }

        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (result.isFailure() || !verifyDeveloperPayload(purchase))
                return;

            LinkedList<Purchase> lPurchase = new LinkedList<>();
            lPurchase.add(purchase);
            LinkedList<Upgrade> lUpgrade = new LinkedList<>();
            lUpgrade.add(_upgrade);
            (new PurchaseConsumer(lPurchase, lUpgrade)).execute();
        }
    }

    private class PurchaseConsumer implements IAsyncResult {
        private final List<Purchase> _lPurchase;
        private final List<Upgrade> _lUpgrade;
        private final IabHelper.OnConsumeFinishedListener consumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {

            @Override
            public void onConsumeFinished(Purchase purchase, IabResult result) {

                // Sanity check
                if ((_lUpgrade != null) && (_lUpgrade.get(0) != null) && (_lPurchase != null) && (_lPurchase.get(0) != null)) {
                    AnalyticsTrackerComposite.getInstance().trackEvent_ExtraListing_Purchase(_lUpgrade.get(0), _lPurchase.get(0).getOrderId());
                    AnalyticsTrackerComposite.getInstance().trackEcommerce_ExtraListing_Purchase(_lUpgrade.get(0), _lPurchase.get(0).getOrderId());
                }

                _lPurchase.remove(0);
                Toast.makeText(MyAdActivity.this, getString(R.string.inAppBilling_extra_listing_purchased), Toast.LENGTH_LONG).show();
                consumeNext();
            }
        };

        PurchaseConsumer(List<Purchase> lPurchase, List<Upgrade> lUpgrade) {
            _lPurchase = lPurchase;
            _lUpgrade = lUpgrade;
        }

        public void execute() {
            consumeNext();
        }

        private void consumeNext() {
            if (_lPurchase.size() > 0) {
                Purchase p = _lPurchase.get(0);
                registerOnSpareRoom(p);
            }
        }

        private void registerOnSpareRoom(Purchase p) {
            Parameters params = new Parameters();
            params.add("product_id", p.getSku());
            params.add("transaction_id", p.getOrderId());
            params.add("purchase_token", p.getPurchaseToken());

            UpgradeProductAsyncTask at =
                    new UpgradeProductAsyncTask(
                            SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(),
                            SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                            this);
            _asyncManager.register(at);
            at.execute(params);
        }

        @Override
        public void update(Object o) {
            Purchase p = _lPurchase.get(0);
            if (o instanceof SpareroomStatus) {
                if (((SpareroomStatus) o).getCode().equals(SpareroomStatusCode.CODE_SUCCESS)
                        || ((SpareroomStatus) o).getCode().equals(SpareroomStatusCode.CODE_ALREADY_PROCESSED)) {
                    try {
                        mHelper.consumeAsync(p, consumeFinishedListener);
                    } catch (NullPointerException | IllegalStateException e) {
                        AnalyticsTrackerComposite.getInstance().trackEvent_Upgrade_BillingUnavailableExtraListing_Consume();
                    } catch (IabHelper.IabAsyncInProgressException e) {
                        // another empty catch...
                    }
                }
            }
        }

        @Override
        public void handleInvalidUserException(String message) {

        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(MyAdActivity.this).show();
        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(MyAdActivity.this).show();
        }

        @Override
        public void handleServiceUnavailableException(String message) {

        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(MyAdActivity.this).show();
        }
    }

    private boolean verifyDeveloperPayload(Purchase p) {
        String purchasePayload = p.getDeveloperPayload();

        return (purchasePayload.equals(BillingHelper.generatePayload(getApplicationContext())));

        /*
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

    }

    private class UpgradeListOnIabSetupFinishedListener implements IabHelper.OnIabSetupFinishedListener {
        private final UpgradeList _ul;

        UpgradeListOnIabSetupFinishedListener(UpgradeList ul) {
            _ul = ul;
        }

        public void onIabSetupFinished(IabResult result) {

            if (!result.isSuccess()) {
                alert(getString(R.string.inAppBilling_problem_settingUp));
                return;
            }

            LinkedList<String> lProductIds = new LinkedList<>();
            for (Upgrade u : _ul.get_upgradeList()) {
                lProductIds.add(u.get_code());
            }
            try {
                mHelper.queryInventoryAsync(true, lProductIds, null, new UpgradeListQueryInventoryFinishedListener(_ul));
            } catch (NullPointerException | IllegalStateException e) {
                AnalyticsTrackerComposite.getInstance().trackEvent_Upgrade_BillingUnavailableExtraListing_QueryInventory();
            } catch (IabHelper.IabAsyncInProgressException e) {
                // another empty catch...
            }

        }
    }

    private class UpgradeListQueryInventoryFinishedListener implements IabHelper.QueryInventoryFinishedListener {
        private final UpgradeList _ul;

        UpgradeListQueryInventoryFinishedListener(UpgradeList ul) {
            _ul = ul;
        }

        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

            if (result.isFailure()) {
                return;
            }

            LinkedList<Purchase> lPurchase = new LinkedList<>();
            LinkedList<Upgrade> lUpgrade = new LinkedList<>();
            for (Upgrade u : _ul.get_upgradeList()) {
                SkuDetails sku = inventory.getSkuDetails(u.get_code());
                if (sku != null) {
                    u.set_title(sku.getTitle());
                    u.set_price(sku.getPrice());
                    u.set_priceInMicros(sku.getPriceAmountMicros());
                    u.set_currency(sku.getPriceCurrencyCode());
                }
                Purchase p = inventory.getPurchase(u.get_code());
                if (p != null) {
                    lPurchase.add(p);
                    lUpgrade.add(u);
                }
            }
            if (!lPurchase.isEmpty()) {
                updateUi(lUpgrade);
                (new PurchaseConsumer(lPurchase, lUpgrade)).execute();
                Toast.makeText(MyAdActivity.this, getString(R.string.myAdActivity_updated), Toast.LENGTH_LONG).show();
            }
            _isGooglePlayDataReady = true;
        }
    }

    // TODO refactor this method if more than one kind of extra listing can be purchased

    /**
     * Updates UI so new upgrades can be purchased
     *
     * @param ul the list of upgrades available
     */
    private void updateUi(List<Upgrade> ul) {
        // We are previously only dealing with 1 upgrade (EXTRA_LISTING_SKU), so we can safely
        // deal only with the first element. Needs changing if we handled more than one upgrade
        if ((ul != null) && (ul.size() > 0)) {// Sanity check
            _upgradeGooglePlay = ul.get(0);
        }
    }

    private void alert(String message) {
        View v = LayoutInflater.from(this).inflate(R.layout.dialog_message_button, null);

        android.widget.TextView tvMessage = v.findViewById(R.id.dialog_message);
        Button bPositive = v.findViewById(R.id.dialog_bPositive);

        Dialog d = new AlertDialogBuilder(this, v).create();
        d.show();
        tvMessage.setText(message);
        bPositive.setOnClickListener(new DialogUtil.CancelOnClickListener(d));

    }

}
