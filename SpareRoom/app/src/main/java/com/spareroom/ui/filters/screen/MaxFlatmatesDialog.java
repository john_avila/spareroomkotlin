package com.spareroom.ui.filters.screen;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;

import com.spareroom.R;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.MaxFlatmates;
import com.spareroom.ui.filters.adapter.FiltersAlertDialogAdapter;
import com.spareroom.ui.filters.provider.SubtitleUtil;
import com.spareroom.ui.screen.AlertDialogBuilder;
import com.spareroom.ui.screen.DialogFragment;
import com.spareroom.ui.util.UiUtils;
import com.spareroom.viewmodel.FilterViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MaxFlatmatesDialog extends DialogFragment implements Injectable {
    public final static String TAG = MaxFlatmatesDialog.class.getName() + "Tag";

    private final static String KEY_POSITION = "key_position";
    private final static String KEY_PREVIOUS_SELECTION = "key_previous_selection";

    private RecyclerView recyclerView;
    private FilterViewModel viewModel;
    private int position;
    private int previousSelection;

    @Inject
    SubtitleUtil subtitleUtil;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    public static MaxFlatmatesDialog newInstance(int position, int previousSelection) {
        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        args.putInt(KEY_PREVIOUS_SELECTION, previousSelection);

        MaxFlatmatesDialog fragment = new MaxFlatmatesDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(getBaseActivity(), viewModelFactory).get(FilterViewModel.class);
        assert getArguments() != null;
        position = getArguments().getInt(KEY_POSITION);
        previousSelection =
                savedInstanceState == null
                        ? getArguments().getInt(KEY_PREVIOUS_SELECTION)
                        : savedInstanceState.getInt(KEY_PREVIOUS_SELECTION, MaxFlatmates.NOT_SET.ordinal());
        return createDialog(createRecyclerView()).create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(KEY_PREVIOUS_SELECTION, ((FiltersAlertDialogAdapter) recyclerView.getAdapter()).valueSelectedByPosition());
        super.onSaveInstanceState(outState);
    }

    private RecyclerView createRecyclerView() {
        recyclerView = (RecyclerView) UiUtils.inflateParentLess(getBaseActivity(), R.layout.dialog_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        recyclerView.setAdapter(new FiltersAlertDialogAdapter(getBaseActivity(), maxFlatmatesOptions(), previousSelection));
        return recyclerView;
    }

    private AlertDialogBuilder createDialog(View customView) {
        AlertDialogBuilder builder = new AlertDialogBuilder(getBaseActivity(), customView);
        builder.setTitle(getString(R.string.maximumFlatmates));
        builder.setPositiveButton(R.string.confirm, (dialog, which) -> {
            int selectedPosition = ((FiltersAlertDialogAdapter) ((RecyclerView) customView).getAdapter()).valueSelectedByPosition();
            viewModel.filterAvailabilityMaxFlatmates().saveValues(position, MaxFlatmates.values()[selectedPosition]);
            dismiss();
        });
        builder.setNegativeButton(R.string.cancel, (dialog, which) -> dismiss());
        return builder;
    }

    private ArrayList<String> maxFlatmatesOptions() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < MaxFlatmates.values().length - 1; i++) {
            list.add(
                    (i == 0)
                            ? getString(R.string.noPreference)
                            : subtitleUtil.getFlatmatesText(getBaseActivity(), MaxFlatmates.values()[i].getValue()));
        }
        return list;
    }

}
