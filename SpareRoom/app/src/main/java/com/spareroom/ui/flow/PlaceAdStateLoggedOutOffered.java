package com.spareroom.ui.flow;

import com.spareroom.controller.AppVersion;
import com.spareroom.model.business.DraftAd;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.ui.screen.legacy.*;

public class PlaceAdStateLoggedOutOffered extends FlowState {

    //region FIELDS

    private static final String PAGE_OFFERED_1 = PlaceAdWelcomeFragment.class.getName();
    private static final String PAGE_OFFERED_2 = PlaceAdAdvertiseFragment.class.getName();
    private static final String PAGE_OFFERED_3 = PlaceAdOfferedPropertyFragment.class.getName();
    private static final String PAGE_OFFERED_4 = PlaceAdOfferedTypeAdvertiserFragment.class.getName();
    private static final String PAGE_OFFERED_4A = PlaceAdOfferedCompanyNameFragment.class.getName();
    private static final String PAGE_OFFERED_5 = PlaceAdOfferedAreYouAtPropertyFragment.class.getName();
    private static final String PAGE_OFFERED_6 = PlaceAdOfferedPostcodeAreaFragment.class.getName(); // binding pending
    private static final String PAGE_OFFERED_7 = PlaceAdOfferedAreaFragment.class.getName();
    private static final String PAGE_OFFERED_8 = PlaceAdOfferedWantAddPhotoFragment.class.getName(); // binding pending
    private static final String PAGE_OFFERED_8A = PlaceAdPhotoChosenFragment.class.getName();
    private static final String PAGE_OFFERED_9 = PlaceAdOfferedAddRoomFragment.class.getName(); // binding pending
    private static final String PAGE_OFFERED_10 = PlaceAdOfferedAvailabilityPeriodBills.class.getName();
    private static final String PAGE_OFFERED_11 = PlaceAdOfferedNoFlatmates.class.getName();
    private static final String PAGE_OFFERED_12 = PlaceAdOfferedAgeFlatmates.class.getName();
    private static final String PAGE_OFFERED_13 = PlaceAdOfferedBedroomsFragment.class.getName();
    private static final String PAGE_OFFERED_14 = PlaceAdOfferedPetsFragment.class.getName();
    private static final String PAGE_OFFERED_15 = PlaceAdOfferedSmokeFragment.class.getName();
    private static final String PAGE_OFFERED_16 = PlaceAdOfferedHappyToLiveWithFragment.class.getName(); // binding pending
    private static final String PAGE_OFFERED_17 = PlaceAdOfferedHappyAnyAgeFragment.class.getName();
    private static final String PAGE_OFFERED_17A = PlaceAdOfferedFeesApplyFragment.class.getName();
    private static final String PAGE_OFFERED_18 = PlaceAdOfferedPhoneFragment.class.getName();
    private static final String PAGE_OFFERED_19 = PlaceAdTitleDescriptionFragment.class.getName();
    private static final String PAGE_OFFERED_20 = PlaceAdUploadingFragment.class.getName();
    private static final String PAGE_OFFERED_LAST = PlaceAdUploadCompleteFragment.class.getName();

    //endregion FIELDS

    //region CONSTRUCTORS

    public PlaceAdStateLoggedOutOffered() {
        if (AppVersion.isUk()) {
            SCREENS = new String[]{
                    PAGE_OFFERED_1,
                    PAGE_OFFERED_2,
                    PAGE_OFFERED_3,
                    PAGE_OFFERED_4,
                    PAGE_OFFERED_4A,
                    PAGE_OFFERED_5,
                    PAGE_OFFERED_6,
                    PAGE_OFFERED_7,
                    PAGE_OFFERED_8,
                    PAGE_OFFERED_8A,
                    PAGE_OFFERED_9,
                    PAGE_OFFERED_10,
                    PAGE_OFFERED_11,
                    PAGE_OFFERED_12,
                    PAGE_OFFERED_13,
                    PAGE_OFFERED_14,
                    PAGE_OFFERED_15,
                    PAGE_OFFERED_16,
                    PAGE_OFFERED_17,
                    PAGE_OFFERED_17A,
                    PAGE_OFFERED_18,
                    PAGE_OFFERED_19,
                    PAGE_OFFERED_20,
                    PAGE_OFFERED_LAST
            };
        } else {
            SCREENS = new String[]{
                    PAGE_OFFERED_1,
                    PAGE_OFFERED_2,
                    PAGE_OFFERED_3,
                    PAGE_OFFERED_4,
                    PAGE_OFFERED_4A,
                    PAGE_OFFERED_5,
                    PAGE_OFFERED_6,
                    PAGE_OFFERED_7,
                    PAGE_OFFERED_8,
                    PAGE_OFFERED_8A,
                    PAGE_OFFERED_9,
                    PAGE_OFFERED_10,
                    PAGE_OFFERED_11,
                    PAGE_OFFERED_12,
                    PAGE_OFFERED_13,
                    PAGE_OFFERED_14,
                    PAGE_OFFERED_15,
                    PAGE_OFFERED_16,
                    PAGE_OFFERED_17,
                    PAGE_OFFERED_18,
                    PAGE_OFFERED_19,
                    PAGE_OFFERED_20,
                    PAGE_OFFERED_LAST
            };
        }

        init();
    } // PlaceAdStateLoggedOutOffered() end

    //endregion CONSTRUCTORS

    //region OVERRIDDEN METHODS

    @Override
    public String getPreviousFragmentName(String currentScreen, Object businessObject) {
        String previousScreen = super.getPreviousFragmentName(currentScreen, businessObject);
        DraftAd draftAd = (DraftAd) businessObject;

        if (currentScreen == null)
            return previousScreen;

        // [Place ad form] Alternative flows: Offered - If agent show company name page
        if (currentScreen.equals(PAGE_OFFERED_5)) {
            if (((DraftAdOffered) draftAd).get_advertiserType() != DraftAdOffered.AdvertiserType.AGENT) {
                previousScreen = super.getPreviousFragmentName(previousScreen, draftAd);
            }
        } else if (currentScreen.equals(PAGE_OFFERED_9)) {
            if (!draftAd.hasPicture()) {
                previousScreen = super.getPreviousFragmentName(previousScreen, draftAd);
            }
        } else if (currentScreen.equals(PAGE_OFFERED_13)) {
            if ((((DraftAdOffered) draftAd).get_numExistingGuys() +
                    ((DraftAdOffered) draftAd).get_numExistingGirls()) == 0) {
                previousScreen = super.getPreviousFragmentName(previousScreen, draftAd);
            }
        } else if (currentScreen.equals(PAGE_OFFERED_18)) {
            if (((DraftAdOffered) draftAd).get_advertiserType() != DraftAdOffered.AdvertiserType.AGENT &&
                    ((DraftAdOffered) draftAd).get_advertiserType() != DraftAdOffered.AdvertiserType.LIVE_OUT_LANDLORD) {
                previousScreen = super.getPreviousFragmentName(previousScreen, draftAd);
            }
        }

        return previousScreen;
    } //previousScreen(String, DraftAd)

    @Override
    public String getNextFragmentName(String currentScreen, Object businessObject) {
        String nextScreen = super.getNextFragmentName(currentScreen, businessObject);
        DraftAd draftAd = (DraftAd) businessObject;

        if (currentScreen == null)
            return nextScreen;

        // [Place ad form] Alternative flows: Offered - If agent show company name page
        if (currentScreen.equals(PAGE_OFFERED_4)) {
            if (((DraftAdOffered) draftAd).get_advertiserType() != DraftAdOffered.AdvertiserType.AGENT)
                nextScreen = super.getNextFragmentName(nextScreen, draftAd);
        } else if (currentScreen.equals(PAGE_OFFERED_8)) {
            if (!draftAd.hasPicture())
                nextScreen = super.getNextFragmentName(nextScreen, draftAd);
        } else if (currentScreen.equals(PAGE_OFFERED_11)) {
            if ((((DraftAdOffered) draftAd).get_numExistingGuys() +
                    ((DraftAdOffered) draftAd).get_numExistingGirls()) == 0) {
                nextScreen = super.getNextFragmentName(nextScreen, draftAd);
            }
        } else if (currentScreen.equals(PAGE_OFFERED_17)) {
            if (((DraftAdOffered) draftAd).get_advertiserType() != DraftAdOffered.AdvertiserType.AGENT &&
                    ((DraftAdOffered) draftAd).get_advertiserType() != DraftAdOffered.AdvertiserType.LIVE_OUT_LANDLORD) {
                nextScreen = super.getNextFragmentName(nextScreen, draftAd);
            }
        }

        return nextScreen;
    } // nextScreen(String, DraftAd) end

    //endregion OVERRIDDEN METHODS

}
