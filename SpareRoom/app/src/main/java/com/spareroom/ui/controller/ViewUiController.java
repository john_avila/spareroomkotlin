package com.spareroom.ui.controller;

import com.spareroom.model.business.SpareroomStatus;

/**
 * Controller to handle instantiated UI components.
 * <p>
 * Created by miguel.rossi on 21/11/2016.
 */

public interface ViewUiController {
    /**
     * Enables the view(s)
     */
    void enable();

    /**
     * Disables the view(s)
     */
    void disable();

    /**
     * Sets the components so they display on screen the loading state
     */
    void setLoading();

    /**
     * Sets a status for this view (normally an error for fields that require validation)
     *
     * @param status a <code>SpareroomStatus</code> status
     */
    void setStatus(SpareroomStatus status);

    /**
     * Clears the status for this view
     */
    void clearStatus();
}
