package com.spareroom.ui.screen

import android.animation.ObjectAnimator
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.view.View.VISIBLE
import android.view.animation.AnimationUtils.loadAnimation
import androidx.core.content.ContextCompat
import androidx.lifecycle.*
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.spareroom.App
import com.spareroom.R
import com.spareroom.controller.SpareroomApplication
import com.spareroom.integration.dependency.Injectable
import com.spareroom.integration.webservice.exception.NetworkConnectivityException
import com.spareroom.integration.webservice.exception.NotLoggedInException
import com.spareroom.model.business.*
import com.spareroom.ui.adapter.ConversationListAdapter
import com.spareroom.ui.adapter.LoadMoreScrollListener
import com.spareroom.ui.screen.legacy.PlaceAdActivity
import com.spareroom.ui.util.*
import com.spareroom.ui.util.AnimationUtils.clearAnimation
import com.spareroom.ui.util.AnimationUtils.startFadeAnimation
import com.spareroom.viewmodel.ConversationListViewModel
import com.spareroom.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.conversation_list.*
import kotlinx.android.synthetic.main.conversation_list_empty.*
import javax.inject.Inject

private const val REQUEST_CODE_CONVERSATION = 300

class ConversationListFragment : Fragment(), Injectable {

    private val inflater by lazy { LayoutInflater.from(activity) }
    private val screenHeight by lazy { UiUtils.screenHeight(activity) }

    private val layoutManager by lazy { LinearLayoutManager(activity) }
    private val viewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(ConversationListViewModel::class.java) }
    private val activityViewModel by lazy { ViewModelProviders.of(activity!!, viewModelFactory).get(MainActivityViewModel::class.java) }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var adapter: ConversationListAdapter

    @Inject
    lateinit var connectivityChecker: ConnectivityChecker

    companion object {
        const val UPDATE_CONVERSATION = "update_conversation"
        const val CONVERSATION_DELETED = "conversation_deleted"
        const val CONVERSATION_SNIPPET = "conversation_snippet"
        const val MESSAGE_DIRECTION = "message_direction"
        const val DATE_LAST_ACTIVITY = "date_last_activity"
        const val CONVERSATION_ID = "conversation_id"
        const val TAG = "ConversationListFragmentTag"

        @JvmStatic
        fun getInstance(): ConversationListFragment = ConversationListFragment()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.conversation_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        setUpToolbar(view)

        adapter.init(activity!!, this, View.OnClickListener { v -> handleListItemClick(v) })
        setUpRecyclerView()
        setUpSwipeRefreshLayout()
        setUpErrorView()

        observeLoadConversationList()
        observerMainActivityRequests()

        if (savedInstanceState == null || viewModel.conversationListLiveData.value == null) {
            showSkeleton()
            NotificationUtils.closeNotifications(App.get())
            loadConversationList(true)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_CONVERSATION && resultCode == RESULT_OK && data != null) {
            when {
                data.hasExtra(CONVERSATION_DELETED) -> viewModel.conversationListLiveData.deleteItem(data.getStringExtra(CONVERSATION_ID))
                data.hasExtra(UPDATE_CONVERSATION) -> updateConversation(data)
            }
        }
    }

    private fun updateConversation(data: Intent) {
        val conversationId = data.getStringExtra(CONVERSATION_ID)
        val snippet = data.getStringExtra(CONVERSATION_SNIPPET)
        val dateLastConversation = data.getStringExtra(DATE_LAST_ACTIVITY)
        val direction = data.getStringExtra(MESSAGE_DIRECTION)
        viewModel.conversationListLiveData.updateConversation(conversationId, snippet, dateLastConversation, direction)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.refresh_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when {
            item.itemId == R.id.menu_item_refresh -> menuRefresh()
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setUpToolbar(view: View) {
        setToolbar(view, getString(R.string.messages))
        activityViewModel.syncDrawer.sync()
    }

    private fun setUpRecyclerView() {
        conversationListRecyclerView.addOnScrollListener(ConversationListScrollListener())
        conversationListRecyclerView.layoutManager = layoutManager
        conversationListRecyclerView.adapter = adapter
    }

    private fun setUpSwipeRefreshLayout() {
        conversationListSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(activity!!, R.color.material_deep_sea_blue))
        conversationListSwipeRefreshLayout.setOnRefreshListener { refreshConversationList() }
    }

    private fun setUpErrorView() {
        conversationListError.backgroundResource(R.color.transparent)
        conversationListError.action(errorAction(), getString(R.string.retry))
    }

    private fun errorAction(): View.OnClickListener {
        return View.OnClickListener {
            when {
                connectivityChecker.isConnected() -> {
                    showSkeleton()
                    loadConversationList(true)
                    hideErrorView()
                }
                conversationListError.isConnectionError -> startFadeAnimation(conversationListError, repeatCount = 0)
                else -> viewModel.conversationListLiveData.setConnectionError()
            }
        }
    }

    private fun showSkeleton() {
        skeletonView.show()
        fillSkeleton()
        startFadeAnimation(skeletonView, repeatCount = ObjectAnimator.INFINITE)
    }

    private fun observeLoadConversationList() {
        viewModel.conversationListLiveData.observe(this, Observer { conversationListLoaded(it!!) })
    }

    private fun observerMainActivityRequests() {
        activityViewModel.conversationListRequests.observe(this, Observer { handleMainActivityRequests(it!!) })
    }

    private fun handleMainActivityRequests(request: ConversationsListRequest) {
        when (request) {
            is RefreshConversationList -> refreshConversationList()
            is OpenConversation -> openConversation(request.conversationInfo)
        }
    }

    private fun loadConversationList(firstPage: Boolean = false) {
        viewModel.conversationListLiveData.retrieveConversationList(firstPage)
    }

    private fun refreshConversationList() {
        viewModel.refreshing = true
        conversationListSwipeRefreshLayout.isRefreshing = true
        loadConversationList(true)
    }

    private fun menuRefresh(): Boolean {
        when {
            conversationListEmpty.isVisible() -> refreshEmptyList()
            conversationListError.isVisible() -> refreshError()
            adapter.itemCount > 0 -> refreshList()
        }
        return true
    }

    private fun refreshEmptyList() {
        hideEmptyList()
        if (connectivityChecker.isConnected()) {
            showSkeleton()
            loadConversationList(true)

        } else {
            viewModel.conversationListLiveData.setConnectionError()
        }
    }

    private fun refreshError() {
        if (!connectivityChecker.isConnected() && conversationListError.isConnectionError) {
            startFadeAnimation(conversationListError, repeatCount = 0)

        } else if (!connectivityChecker.isConnected()) {
            viewModel.conversationListLiveData.setConnectionError()

        } else {
            hideErrorView()
            showSkeleton()
            loadConversationList(true)
        }
    }

    private fun refreshList() {
        if (connectivityChecker.isConnected()) {
            refreshConversationList()
        } else {
            showSnackBar(getString(R.string.no_connection))
        }
    }

    private fun conversationListLoaded(response: ConversationListResponse) {
        when (response) {
            is EmptyConversationListResponse -> handleEmptyState(response)
            is ConversationReadResponse -> updateAdapter(response.diff, response.conversations)
            is ConversationDeletedResponse -> handleDeleteItem(response)
            is NextPageConversationList -> handleNextPage(response)
            is FirstPageConversationList -> handleFirstPage(response)
            is FailedConversationListResponse -> handleFailure(response)
        }
        conversationListSwipeRefreshLayout.isRefreshing = false
    }

    private fun handleEmptyState(response: EmptyConversationListResponse) {
        showEmptyList()
        updateAdapter(response.diff, response.conversations)
        hideSkeletonView()
        hideList()
    }

    private fun handleNextPage(response: NextPageConversationList) {
        updateAdapter(response.diff, response.conversations)

        if (response.hadDuplicates)
            SnackBarUtils.show(
                conversationListRecyclerView,
                getString(R.string.you_ve_got_new_messages),
                getString(R.string.refresh),
                View.OnClickListener { refreshConversationList() })
    }

    private fun handleFirstPage(response: FirstPageConversationList) {
        NotificationUtils.closeNotifications(App.get())
        updateAdapter(response.diff, response.conversations)

        if (viewModel.refreshing) {
            viewModel.refreshing = false
            layoutManager.scrollToPosition(0)
        }

        hideSkeletonView()
        hideEmptyList()
    }

    private fun handleDeleteItem(response: ConversationDeletedResponse) {
        updateAdapter(response.diff, response.conversations)
        SnackBarUtils.show(conversationListRecyclerView, getString(R.string.activityConversation_deleted))
    }

    private fun handleFailure(failureResponse: FailedConversationListResponse) {
        if (adapter.itemCount > 0) {
            showSnackBar(snackBarMessage(failureResponse.throwable))
        } else {
            showErrorScreen(failureResponse)
        }
    }

    private fun showErrorScreen(failureResponse: FailedConversationListResponse) {
        val networkIssue = failureResponse.throwable is NetworkConnectivityException

        setErrorScreen(errorTitle(failureResponse.throwable), errorMessage(failureResponse.throwable), networkIssue)
        hideSkeletonView()
    }

    private fun setErrorScreen(title: String, message: String, networkIssue: Boolean) {
        conversationListError.title(title)
        conversationListError.message(message)
        conversationListError.icon(if (networkIssue) R.drawable.ic_offline else R.drawable.ic_error)
        conversationListError.show(networkIssue)
    }

    private fun errorTitle(throwable: Throwable): String {
        return getString(when (throwable) {
            is NotLoggedInException -> R.string.oops
            is NetworkConnectivityException -> R.string.no_connection
            else -> R.string.errorGeneric_title
        })
    }

    private fun errorMessage(throwable: Throwable): String {
        return getString(
            when (throwable) {
                is NotLoggedInException -> R.string.conversationList_error_log_in
                is NetworkConnectivityException -> R.string.errorOffline_body
                else -> R.string.dialogUpdate_tvMessage
            })
    }

    private fun showSnackBar(message: String) {
        hideProgressbar()
        SnackBarUtils.show(conversationListRecyclerView, message)
    }

    private fun snackBarMessage(throwable: Throwable): String {
        return getString(
            when (throwable) {
                is NotLoggedInException -> R.string.please_log_in
                is NetworkConnectivityException -> R.string.no_connection
                else -> R.string.dialogUpdate_tvMessage
            }
        )
    }

    private fun hideEmptyList() {
        conversationListEmpty.hide()
    }

    private fun hideErrorView() {
        conversationListError.hide()
    }

    private fun hideList() {
        conversationListSwipeRefreshLayout.hide()
    }

    private fun showEmptyList() {
        val isOffered = SpareroomApplication.getInstance().spareroomContext.session?.isOffering == true

        conversationListEmpty.show()
        hideSkeletonView()

        emptyListBodyTextView.text =
            getString(if (isOffered) R.string.conversationList_emptyList_body_offered else R.string.conversationList_emptyList_body_searcher)
        emptyListCreateAdvertButtonTextView.setOnClickListener { startActivity(Intent(activity, PlaceAdActivity::class.java)) }
    }

    private fun updateAdapter(diffResult: DiffUtil.DiffResult, conversationList: List<ConversationPreview>) {
        conversationListSwipeRefreshLayout.show()
        adapter.update(diffResult, conversationList)
        hideProgressbar()
    }

    private fun hideProgressbar() {
        if (conversationListProgressBar.visibility != VISIBLE)
            return

        conversationListProgressBar.animation = loadAnimation(activity, R.anim.slide_out_to_bottom)
        conversationListProgressBar.hide()
    }

    private fun showProgressbar() {
        conversationListProgressBar.show()
        conversationListProgressBar.animation = loadAnimation(activity, R.anim.slide_in_from_bottom)
    }

    private fun hideSkeletonView() {
        skeletonView.hide()
        clearAnimation(skeletonView)
    }

    private fun fillSkeleton() {
        skeletonView.removeAllViews()
        val viewHeight = resources.getDimensionPixelSize(R.dimen.material_conversationListItem_height)
        var currentHeight = 0
        do {
            inflater.inflate(R.layout.conversation_list_item_skeleton, skeletonView)
            currentHeight += viewHeight
        } while (screenHeight > currentHeight)
    }

    private fun handleListItemClick(view: View) {
        val loggedIn = SpareroomApplication.getInstance().spareroomContext.isUserLoggedIn
        if (!loggedIn || !connectivityChecker.isConnected()) {
            showSnackBar(getString(if (loggedIn) R.string.no_connection else R.string.please_log_in))
            return
        }

        val preview = view.getTag(R.id.item_tag) as ConversationPreview
        openConversation(
            ConversationInfo(
                threadIdField = preview.threadId,
                advertIdField = preview.advertId,
                otherUserIdField = preview.otherUserId,
                otherUserFirstNamesField = preview.otherUserName,
                otherUserLastNameField = preview.otherUserLastName,
                otherUserAvatarUrlField = preview.otherUserAvatarUrl))
    }

    private fun openConversation(conversationInfo: ConversationInfo) {
        ConversationActivity.startForResult(this, REQUEST_CODE_CONVERSATION, conversationInfo)
    }

    private inner class ConversationListScrollListener : LoadMoreScrollListener(layoutManager) {

        override fun isLoading() = viewModel.conversationListLiveData.isLoading

        override fun hasMoreItems() = viewModel.conversationListLiveData.hasMoreItems

        override fun loadMore() {
            showProgressbar()
            loadConversationList()
        }

    }
}
