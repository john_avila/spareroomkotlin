package com.spareroom.ui.screen.legacy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.model.business.*;
import com.spareroom.ui.util.UiUtils;

import java.util.Locale;

public class PlaceAdOfferedAddRoomFragment extends PlaceAdFragment {

    //region FIELDS UI

    private ListView _lvRooms;

    //endregion FIELDS UI

    //region FIELDS CONTROLLER

    public final static String KEY_EXTRA_WHOLE_PROPERTY = "key_extra_whole_property";
    public final static String KEY_EXTRA_DEPOSIT = "deposit";
    public final static String KEY_EXTRA_ROOM_RENT_AMOUNT = "room_rent_amount";
    public final static String KEY_EXTRA_ROOM_RENT_ARE_BILLS_INCLUDED = "room_rent_areBillsIncluded";
    public final static String KEY_EXTRA_ROOM_RENT_PERIODICITY = "room_rent_periodicity";
    public final static String KEY_EXTRA_ROOM_STATUS = "room_status";
    public final static String KEY_EXTRA_ROOM_SIZE = "room_roomSize";
    public final static String KEY_EXTRA_POSITION = "position";

    public final static String KEY_EXTRA_ACTION = "action";
    public final static String VALUE_REMOVE = "remove";

    public final static String KEY_REQUEST_CODE = "requestCode";
    public final static int VALUE_REQUEST_CODE_ADD_ROOM = 1919;
    public final static int VALUE_REQUEST_CODE_EDIT_ROOM = 1918;

    private DraftAdOffered _draft;
    private RoomList _roomList;

    private Button bConfirm;
    private Button bAdd;

    //endregion FIELDS CONTROLLER

    //region METHODS LIFECYCLE

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setTitle(getString(R.string.place_ad_offered_property_add_rooms));

        View rootView = inflater.inflate(R.layout.place_ad_offered_add_rooms, container, false);
        bAdd = rootView.findViewById(R.id.place_ad_offered_add_rooms_bAdd);
        bConfirm = rootView.findViewById(R.id.place_ad_bConfirm);
        _lvRooms = rootView.findViewById(R.id.place_ad_offered_add_rooms_lvRooms);

        RoomList roomList;

        _draft = (DraftAdOffered) getDraftAd();

        // In case there is an activity state change and we lost the draft
        if (_draft == null) {
            getActivity().finish();
            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();
            return null;
        }

        roomList = _draft.get_rooms();

        if (roomList != null && roomList.size() > 0) {
            _roomList = roomList;
        } else {
            _roomList = new RoomList();

            Room room = new Room();
            Price rent = new Price();

            rent.set_amount(getString(R.string.place_ad_offered_add_room_zero));
            rent.set_areBillsIncluded(Price.BILLS_INCLUDED_NO);
            rent.set_periodicity(Price.PERIODICITY_MONTH);
            room.set_rent(rent);
            room.set_roomSize(Room.ROOM_SIZE_DOUBLE);

            _roomList.add(room);
        }
        _lvRooms.setAdapter(new RoomsAdapter(_roomList));

        /* Listeners */

        bAdd.setOnClickListener(new OnbAddClick());

        bConfirm.setOnClickListener(v -> {
            if (_roomList.size() == 0) {
                Toast.makeText(getActivity(), getString(R.string.place_ad_offered_add_room_at_least_one), Toast.LENGTH_LONG).show();

            } else {
                for (int i = 0; i < _roomList.size(); i++) {
                    Integer intAmount = 0;
                    try {
                        intAmount = Integer.parseInt(_roomList.get(i).get_rent().get_amount());
                    } catch (NumberFormatException ignored) {
                    }

                    if (intAmount == 0) {
                        Toast.makeText(getActivity(), R.string.place_ad_offered_add_room_rent_for_all, Toast.LENGTH_LONG).show();
                        return; // If any of them doesn't have price
                    }
                }
                _draft.set_rooms(_roomList);
                finish();
            }
        });

        bAdd.setCompoundDrawablesWithIntrinsicBounds(
                UiUtils.getTintedDrawable(getActivity(), R.drawable.ic_place_advert, R.color.white), null, null, null);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        _lvRooms.setAdapter(null);
        bConfirm.setOnClickListener(null);
        bAdd.setOnClickListener(null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Room room = new Room();
        Price rent = new Price();

        if ((requestCode == VALUE_REQUEST_CODE_ADD_ROOM) && (resultCode == Activity.RESULT_OK)) {

            rent.set_amount(data.getStringExtra(KEY_EXTRA_ROOM_RENT_AMOUNT));
            rent.set_areBillsIncluded(data.getStringExtra(KEY_EXTRA_ROOM_RENT_ARE_BILLS_INCLUDED));
            rent.set_periodicity(data.getStringExtra(KEY_EXTRA_ROOM_RENT_PERIODICITY));
            room.set_rent(rent);

            room.deposit(data.getStringExtra(KEY_EXTRA_DEPOSIT));
            room.set_roomStatus(data.getIntExtra(KEY_EXTRA_ROOM_STATUS, Room.ROOM_STATUS_AVAILABLE));
            room.set_roomSize(data.getIntExtra(KEY_EXTRA_ROOM_SIZE, Room.ROOM_SIZE_DOUBLE));

            _roomList.add(room);

        } else if ((requestCode == VALUE_REQUEST_CODE_EDIT_ROOM) && (resultCode == Activity.RESULT_OK)) {

            String action = data.getStringExtra(KEY_EXTRA_ACTION);

            if ((action != null) && (action.equals(VALUE_REMOVE))) {
                int position = data.getIntExtra(KEY_EXTRA_POSITION, -1);

                if (position != -1) {
                    _roomList.remove(position);

                } else {
                    Toast.makeText(getActivity(), R.string.place_ad_offered_add_room_no_remove, Toast.LENGTH_LONG).show();
                }

            } else {
                rent.set_amount(data.getStringExtra(KEY_EXTRA_ROOM_RENT_AMOUNT));
                rent.set_areBillsIncluded(data.getStringExtra(KEY_EXTRA_ROOM_RENT_ARE_BILLS_INCLUDED));
                rent.set_periodicity(data.getStringExtra(KEY_EXTRA_ROOM_RENT_PERIODICITY));
                room.set_rent(rent);

                room.deposit(data.getStringExtra(KEY_EXTRA_DEPOSIT));
                room.set_roomStatus(data.getIntExtra(KEY_EXTRA_ROOM_STATUS, Room.ROOM_STATUS_AVAILABLE));
                room.set_roomSize(data.getIntExtra(KEY_EXTRA_ROOM_SIZE, 0));

                int position = data.getIntExtra(KEY_EXTRA_POSITION, -1);
                if (position != -1) {
                    _roomList.replace(room, position);
                } else {
                    Toast.makeText(getActivity(), R.string.place_ad_offered_add_room_no_update, Toast.LENGTH_LONG).show();
                }
            }
        }

        _draft.set_rooms(_roomList);
        _lvRooms.setAdapter(new RoomsAdapter(_roomList));

    } //end onActivityResult(int requestCode, int resultCode, Intent data)

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    private class OnbAddClick implements OnClickListener {
        @Override
        public void onClick(View v) {

            if (_roomList.size() > 11) {
                Toast.makeText(getActivity(), R.string.place_ad_offered_add_room_maximum, Toast.LENGTH_LONG).show();

            } else {
                Intent activityAddRoom = new Intent();

                activityAddRoom.putExtra(KEY_EXTRA_WHOLE_PROPERTY, _draft.wholeProperty());
                activityAddRoom.putExtra(PlaceAdAbstractActivity.BUNDLE_KEY_FORM_ID, ((PlaceAdAbstractActivity) getActivity()).getId());
                activityAddRoom.putExtra(KEY_REQUEST_CODE, VALUE_REQUEST_CODE_ADD_ROOM);
                activityAddRoom.setClass(getActivity(), PlaceAdAddRoomActivity.class);

                startActivityForResult(activityAddRoom, VALUE_REQUEST_CODE_ADD_ROOM);
            }

        } //end onClick(View v)

    } //end class OnbAddClick

    private class RoomsAdapter extends BaseAdapter {
        private final RoomList _list;

        RoomsAdapter(RoomList list) {
            _list = list;
        }

        @Override
        public int getCount() {
            return _list.size();
        }

        @Override
        public Object getItem(int position) {
            return _list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null)
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.place_ad_offered_room_item, parent, false);

            TextView tvTitle = convertView.findViewById(R.id.place_ad_offered_room_item_tvTitle);
            TextView tvSubtitle = convertView.findViewById(R.id.place_ad_offered_room_item_tvSubtitle);
            ImageView bEdit = convertView.findViewById(R.id.place_ad_offered_room_item_bEdit);

            Room room = _list.get(position);
            EditOnClickListener listener;

            String periodicity = room.get_rent().get_periodicity();

            if (AppVersion.isUs())
                switch (periodicity) {
                    case "pcm":
                        periodicity = "/ month";
                        break;
                    case "pw":
                        periodicity = "/ week";
                        break;
                    default:
                        break;
                }

            tvTitle.setText(String.format(Locale.getDefault(), "%s %d", getString(R.string.place_ad_offered_add_room_room), (position + 1)));

            tvSubtitle.setText(
                    String.format(
                            Locale.getDefault(),
                            "%s%s %s, %s",
                            AppVersion.flavor().getCurrency(),
                            room.get_rent().get_amount(),
                            periodicity,
                            ((room.get_roomSize() == Room.ROOM_SIZE_SINGLE) ? getString(R.string.place_ad_offered_room_bSingle) : getString(R.string.place_ad_offered_room_bDouble))));

            listener = new EditOnClickListener(room, position);
            bEdit.setOnClickListener(listener);
            convertView.setOnClickListener(listener);

            return convertView;
        }

    } //end class RoomsAdapter

    private class EditOnClickListener implements android.view.View.OnClickListener {
        private final Room _room;
        private final int _position;

        EditOnClickListener(Room room, int positionInList) {
            _room = room;
            _position = positionInList;
        }

        @Override
        public void onClick(View v) {

            Intent activityEditRoom = new Intent();

            activityEditRoom.putExtra(KEY_EXTRA_WHOLE_PROPERTY, _draft.wholeProperty());
            activityEditRoom.putExtra(KEY_EXTRA_DEPOSIT, _room.deposit());
            activityEditRoom.putExtra(KEY_EXTRA_ROOM_RENT_AMOUNT, _room.get_rent().get_amount());
            activityEditRoom.putExtra(KEY_EXTRA_ROOM_RENT_ARE_BILLS_INCLUDED, _room.get_rent().get_areBillsIncluded());
            activityEditRoom.putExtra(KEY_EXTRA_ROOM_RENT_PERIODICITY, _room.get_rent().get_periodicity());
            activityEditRoom.putExtra(KEY_EXTRA_ROOM_SIZE, _room.get_roomSize());
            activityEditRoom.putExtra(KEY_EXTRA_ROOM_STATUS, _room.get_roomStatus());
            activityEditRoom.putExtra(KEY_EXTRA_POSITION, _position);
            activityEditRoom.putExtra(KEY_REQUEST_CODE, VALUE_REQUEST_CODE_EDIT_ROOM);
            activityEditRoom.putExtra(PlaceAdAbstractActivity.BUNDLE_KEY_FORM_ID, ((PlaceAdAbstractActivity) getActivity()).getId());
            activityEditRoom.setClass(getActivity(), PlaceAdAddRoomActivity.class);

            startActivityForResult(activityEditRoom, VALUE_REQUEST_CODE_EDIT_ROOM);

        }

    } //end class EditOnClickListener

    //endregion INNER CLASSES

}
