package com.spareroom.ui.util

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.SphericalUtil
import com.spareroom.lib.util.NumberUtil
import com.spareroom.model.business.Coordinates

object MapsUtil {

    @JvmStatic
    fun createLatLngBounds(coordinates: Coordinates): LatLngBounds {
        val centerLongitude = coordinates.longitude
        val centerLatitude = coordinates.latitude
        val latitudeDelta = coordinates.latitudeDelta
        val longitudeDelta = coordinates.longitudeDelta

        val swLat = centerLatitude - latitudeDelta / 2
        val swLong = centerLongitude - longitudeDelta / 2
        val neLat = centerLatitude + latitudeDelta / 2
        val neLong = centerLongitude + longitudeDelta / 2

        val swLatLng = LatLng(swLat, swLong)
        val neLatLng = LatLng(neLat, neLong)

        return LatLngBounds.builder().include(swLatLng).include(neLatLng).build()
    }

    @JvmStatic
    fun changeLatLngBoundsSize(latLngBounds: LatLngBounds, miles: Double): LatLngBounds {
        val milesFromMax = NumberUtil.milesToMeters(miles)

        // Heading in degrees clockwise from north
        val toSw = 225.0
        val toNe = 45.0

        val newSwLatLng = SphericalUtil.computeOffset(latLngBounds.southwest, milesFromMax / 2, toSw)
        val newNeLatLng = SphericalUtil.computeOffset(latLngBounds.northeast, milesFromMax / 2, toNe)

        return LatLngBounds.builder().include(newSwLatLng).include(newNeLatLng).build()
    }

    @JvmStatic
    fun getDistance(from: Coordinates, to: LatLng): Double {
        val centerLongitude = from.longitude
        val centerLatitude = from.latitude

        val center = LatLng(centerLatitude, centerLongitude)
        val side = LatLng(to.latitude, centerLongitude)

        return SphericalUtil.computeDistanceBetween(center, side)
    }

    // fixes crash in maps, please see https://issuetracker.google.com/issues/35829548
    @JvmStatic
    fun disableIndoor(map: GoogleMap) {
        map.isIndoorEnabled = false
    }

}
