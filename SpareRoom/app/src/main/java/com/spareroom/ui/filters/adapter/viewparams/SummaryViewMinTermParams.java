package com.spareroom.ui.filters.adapter.viewparams;

import com.spareroom.model.business.SearchAdvertListProperties.MinTerm;

public class SummaryViewMinTermParams extends AbstractSummaryViewParams {
    private MinTerm minTerm;

    public SummaryViewMinTermParams(String tag) {
        super(tag);
    }

    public MinTerm minTerm() {
        return minTerm;
    }

    public void minTerm(MinTerm minTerm) {
        this.minTerm = minTerm;
    }

    public void reset() {
        super.reset();
        minTerm(MinTerm.NOT_SET);
    }
}
