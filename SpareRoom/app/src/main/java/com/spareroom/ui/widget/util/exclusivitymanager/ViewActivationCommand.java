package com.spareroom.ui.widget.util.exclusivitymanager;
/*
 * Third party library developed by Manuel Luis Padilla Mochon under Apache License, Version 2.0
 * This code was developed before his employment with Flatshare Ltd
 *
 * @author Manuel Luis Padilla Mochon
 */

import android.view.View;

public class ViewActivationCommand {
    protected View _view;

    public ViewActivationCommand(View v) {
        _view = v;
    }

    public void activate() {
    }

    public void deactivate() {
    }
}
