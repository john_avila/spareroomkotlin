package com.spareroom.ui.screen.legacy;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.LinearLayout.LayoutParams;

import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.controller.legacy.*;
import com.spareroom.integration.imageloader.ImageLoader;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.model.business.*;
import com.spareroom.ui.screen.*;
import com.spareroom.ui.util.ToastUtils;
import com.spareroom.viewmodel.EditProfileActivityViewModel;

import java.util.*;

import javax.inject.Inject;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

public class EditProfileActivity extends InjectableActivity implements IEmptyAsyncTaskManagerListener {
    private AsyncTaskManager _asyncManager;

    private ImageView _ivProfilePhoto;
    private TextView _tvName;
    private TextView _tvEmail;
    private TextView _tvPassword;
    private LinearLayout _llEmailPreferences;
    private ProgressBar _pb;

    private String _newFirstName;
    private String _newLastName;
    private String _newEmail;
    private String _newPassword;

    private ProfilePreferenceList _newEmailPreferences;
    private boolean _preferencesChanged;

    private Dialog _updateDialog;

    private boolean isLaunchUpdatePassword = true;
    private boolean isLaunchUpdateName = true;
    private boolean isLaunchUpdateEmail = true;
    private boolean isLaunchUpdateEmailPreferences = true;

    @Inject
    ImageLoader _imageLoader;

    @Inject
    SpareroomContext spareroomContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_editprofile);

        setToolbar();

        _asyncManager = new AsyncTaskManager();
        _asyncManager.registerSizeListener(this);

        LinearLayout _llProfilePhoto = findViewById(R.id.activity_editprofile_llPhoto);
        _ivProfilePhoto = findViewById(R.id.activity_editprofile_avatar);
        _tvName = findViewById(R.id.activity_editprofile_tvName);
        _tvEmail = findViewById(R.id.activity_editprofile_tvEmail);
        _tvPassword = findViewById(R.id.activity_editprofile_tvPassword);

        _llEmailPreferences = findViewById(R.id.activity_editprofile_ll);

        _pb = findViewById(R.id.activity_editprofile_pb);
        Session _session = spareroomContext.getSession();

        if (_session != null && _session.get_user() != null) {

            _llProfilePhoto.setOnClickListener(v -> {
                Intent iManagePhotos = new Intent();

                iManagePhotos.setClass(EditProfileActivity.this, ManagePhotoProfileActivity.class);
                iManagePhotos.putExtra(ManagePhotoProfileActivity.EXTRA_KEY_AT_RETRIEVE, RetrievePhotosAsyncTask.class.getName());
                iManagePhotos.putExtra(ManagePhotoProfileActivity.EXTRA_KEY_AT_EDIT_CAPTION, ChangeOrderPictureAsyncTask.class.getName());
                iManagePhotos.putExtra(ManagePhotoProfileActivity.EXTRA_KEY_AT_DELETE, DeletePictureAsyncTask.class.getName());
                iManagePhotos.putExtra(ManagePhotoProfileActivity.EXTRA_KEY_AT_CHANGE_ORDER, ChangeOrderPictureAsyncTask.class.getName());

                startActivityForResult(iManagePhotos, 456);
            });

            loadImage();
            loadFromSession();
            launchEmailPreferences();

            final EditProfileActivityViewModel viewModel = ViewModelProviders.of(this).get(EditProfileActivityViewModel.class);
            viewModel.getDiscardProfileChanges().observe(this, discardProfileChangesEvent -> finish());
            viewModel.getSaveProfileChanges().observe(this, saveProfileChangesEvent -> saveChanges());

        } else {
            spareroomContext.deleteUserSession();
            finish();
        }

    }

    private void loadImage() {
        if (!spareroomContext.isUserLoggedIn()) {
            ToastUtils.showToast(R.string.please_log_in);
            return;
        }

        // The following four cases do happen
        Session _session = spareroomContext.getSession();
        if ((_session.get_user() != null)
                && (_session.get_user().get_profilePhotoUrl() != null)
                && !_session.get_user().get_profilePhotoUrl().equals("null")
                && !_session.get_user().get_profilePhotoUrl().equals(""))

            _imageLoader.loadCircularImage(_session.get_user().get_profilePhotoUrl(), _ivProfilePhoto);
    }

    private void loadFromSession() {
        Session s = spareroomContext.getSession();
        _tvName.setText(String.format(AppVersion.flavor().getLocale(), "%s %s", s.get_user().get_firstName(), s.get_user().get_lastName()));
        _tvName.setOnClickListener(new NameOnClickListener());
        _tvEmail.setText(s.get_email());
        _tvEmail.setOnClickListener(new EmailOnClickListener());
        _tvPassword.setText("*************");
        _tvPassword.setOnClickListener(new PasswordOnClickListener());
    }

    private void launchEmailPreferences() {
        Parameters p = new Parameters();

        EditProfileSettingsAsyncTask at = new EditProfileSettingsAsyncTask(
                SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(),
                SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                _profilePreferencesListener);
        _asyncManager.register(at);
        at.execute(p);
    }

    @Override
    protected void onPause() {
        super.onPause();
        _asyncManager.cancelAll();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == 456) && (resultCode == Activity.RESULT_OK)) {
            loadImage();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.apply_menu, menu);
        menu.findItem(R.id.menu_item_apply).setVisible(hasUnsavedChanges());
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                checkForUnsavedChanges();
                return true;
            case R.id.menu_item_apply:
                saveChanges();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkForUnsavedChanges() {
        if (hasUnsavedChanges()) {
            SaveProfileChangesConfirmationDialog.getInstance().show(this, SaveProfileChangesConfirmationDialog.TAG);
        } else {
            finish();
        }
    }

    private boolean hasUnsavedChanges() {
        return _newFirstName != null || _newLastName != null || _newEmail != null || _newPassword != null || _preferencesChanged;
    }

    private void finishEditProfile() {
        if (isLaunchUpdatePassword && isLaunchUpdateName && isLaunchUpdateEmail && isLaunchUpdateEmailPreferences) {
            _pb.setVisibility(View.GONE);
            ToastUtils.showToast(R.string.preferencesUpdated);
            finish();
        }
    }

    private class CheckBoxListener implements OnClickListener {
        private final ProfilePreference _pp;
        private String falseValue = "N";
        private String trueValue = "Y";

        CheckBoxListener(ProfilePreference pp) {
            _pp = pp;
            if (_pp.hasAvailableOption()) {
                for (ProfilePreference.AvailableOption ao : _pp.get_listAvailableOption()) {
                    if (ao._dataValue.equals("false")) {
                        falseValue = ao._value;
                    } else if (ao._dataValue.equals("true")) {
                        trueValue = ao._value;
                    }
                }
            }
        }

        @Override
        public void onClick(View v) {
            _pp.set_permission(_pp.get_permission().equals(falseValue) ? trueValue : falseValue);
            _preferencesChanged = true;
            supportInvalidateOptionsMenu();
        }

    }

    private void saveChanges() {
        View contentView = LayoutInflater.from(this).inflate(R.layout.dialog_editprofile_confirm, null, false);
        Dialog d = new AlertDialogBuilder(this, contentView).create();
        Button _bPositive = contentView.findViewById(R.id.dialog_bPositive);
        Button _bNegative = contentView.findViewById(R.id.dialog_bNegative);
        _bPositive.setOnClickListener(new OKOnClickListener(d, contentView.findViewById(R.id.dialog_editprofile_confirm_etConfirmPassword)));
        _bPositive.setText(R.string.activity_editProfile_bPositive);
        _bNegative.setOnClickListener(new CancelOnClickListener(d));
        d.show();
    }

    private class OKOnClickListener implements OnClickListener {
        private final Dialog _dialog;
        private final EditText _etPassword;

        OKOnClickListener(Dialog d, EditText etPassword) {
            _dialog = d;
            _etPassword = etPassword;
        }

        @Override
        public void onClick(View v) {
            _dialog.dismiss();
            if (_newPassword != null) {
                launchUpdatePassword(_etPassword.getText().toString());
            } else {
                if (_newFirstName != null || _newLastName != null) {
                    launchUpdateName(_etPassword.getText().toString());
                }
                if (_newEmail != null) {
                    launchUpdateEmail(_etPassword.getText().toString());
                }

                if (_preferencesChanged) {
                    launchUpdateEmailPreferences();
                }

                _newPassword = null;
            }
            ProgressBar progressBar = new ProgressBar(EditProfileActivity.this);
            progressBar.setIndeterminate(true);
            progressBar.setIndeterminateDrawable(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.progressbar_navy_white));
            _updateDialog = new AlertDialogBuilder(EditProfileActivity.this, progressBar).create();
            _updateDialog.show();
        }
    }

    private class CancelOnClickListener implements OnClickListener {
        private final Dialog _dialog;

        public CancelOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            _dialog.dismiss();
        }
    }

    private void launchUpdateName(String password) {
        if (_newFirstName == null || _newLastName == null)
            return;

        isLaunchUpdateName = false;

        Parameters parameters = new Parameters();
        parameters.add("edit", "name");
        if (_newFirstName != null)
            parameters.add("first_name", _newFirstName);
        if (_newLastName != null)
            parameters.add("last_name", _newLastName);
        parameters.add("password_confirm", password);

        EditProfileSettingsUpdateNameAsyncTask at = new EditProfileSettingsUpdateNameAsyncTask(
                SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(),
                SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                _updateNameListener);
        at.execute(parameters);
        _asyncManager.register(at);
    }

    private void launchUpdateEmail(String password) {
        if (_newEmail == null)
            return;
        isLaunchUpdateEmail = false;

        Parameters parameters = new Parameters();
        parameters.add("edit", "email");
        if (_newEmail != null) {
            parameters.add("email", _newEmail);
            parameters.add("email_again", _newEmail);
        }
        parameters.add("password_confirm", password);
        parameters.add("format", "json");

        EditProfileSettingsUpdateEmailAsyncTask at = new EditProfileSettingsUpdateEmailAsyncTask(
                SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(),
                SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                _updateEmailListener);
        _asyncManager.register(at);
        at.execute(parameters);
    }

    private void launchUpdatePassword(String password) {
        if (_newPassword == null)
            return;
        isLaunchUpdatePassword = false;

        Parameters parameters = new Parameters();
        parameters.add("edit", "password");
        if (_newPassword != null) {
            parameters.add("password", _newPassword);
            parameters.add("password_again", _newPassword);
        }
        parameters.add("password_confirm", password);

        EditProfileSettingsUpdatePasswordAsyncTask at = new EditProfileSettingsUpdatePasswordAsyncTask(
                SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(),
                SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                _updatePasswordListener);
        _asyncManager.register(at);
        at.execute(parameters);
    }

    private void launchUpdateEmailPreferences() {
        if (!_preferencesChanged)
            return;
        isLaunchUpdateEmailPreferences = false;

        Parameters parameters = new Parameters();
        parameters.add("edit", "email_preferences");
        if (_preferencesChanged) {
            for (ProfilePreference pp : _newEmailPreferences.get_lProfilePreference()) {
                parameters.add(pp.get_type(), pp.get_permission());
            }
        }
        parameters.add("password_confirm", "password");

        EditProfileSettingsUpdateEmailPreferencesAsyncTask at = new EditProfileSettingsUpdateEmailPreferencesAsyncTask(
                SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(),
                SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                _updateEmailPreferencesListener);
        _asyncManager.register(at);
        at.execute(parameters);
    }

    private class NameOnClickListener implements OnClickListener {
        private TextView _tvFirstName;
        private TextView _tvLastName;

        @Override
        public void onClick(View v) {
            if (!spareroomContext.isUserLoggedIn()) {
                ToastUtils.showToast(R.string.please_log_in);
                return;
            }

            View contentView = LayoutInflater.from(EditProfileActivity.this).inflate(R.layout.dialog_editprofile_name, null, false);
            Session s = spareroomContext.getSession();

            _tvFirstName = contentView.findViewById(R.id.dialog_editprofile_name_tvFirstName);
            _tvLastName = contentView.findViewById(R.id.dialog_editprofile_name_tvLastName);

            _tvFirstName.setText(s.get_user().get_firstName());
            _tvLastName.setText(s.get_user().get_lastName());

            Dialog d = new AlertDialogBuilder(EditProfileActivity.this, contentView).create();
            Button _bPositive = contentView.findViewById(R.id.dialog_bPositive);
            Button _bNegative = contentView.findViewById(R.id.dialog_bNegative);
            _bPositive.setOnClickListener(new OKOnClickListener(d));
            _bNegative.setOnClickListener(new CancelOnClickListener(d));
            d.show();
        }

        private class OKOnClickListener implements View.OnClickListener {
            private final Dialog _dialog;

            OKOnClickListener(Dialog d) {
                _dialog = d;
            }

            @Override
            public void onClick(View v) {
                String newName = "";
                if (!"".equals(_tvFirstName.getText().toString())) {
                    _newFirstName = _tvFirstName.getText().toString();
                    newName += _newFirstName;
                } else {
                    newName += _tvFirstName.getText().toString();
                }
                newName += " ";
                if (!"".equals(_tvLastName.getText().toString())) {
                    _newLastName = _tvLastName.getText().toString();
                    newName += _newLastName;
                } else {
                    newName += _tvLastName.getText().toString();
                }
                _tvName.setText(newName);
                _dialog.dismiss();
                supportInvalidateOptionsMenu();
            }
        }

        private class CancelOnClickListener implements View.OnClickListener {
            private final Dialog _dialog;

            public CancelOnClickListener(Dialog d) {
                _dialog = d;
            }

            @Override
            public void onClick(View v) {
                _dialog.dismiss();
            }
        }
    }

    private class EmailOnClickListener implements OnClickListener {
        private TextView _tvEmail;
        private TextView _tvEmail2;

        @Override
        public void onClick(View v) {
            if (!spareroomContext.isUserLoggedIn()) {
                ToastUtils.showToast(R.string.please_log_in);
                return;
            }

            View contentView = LayoutInflater.from(EditProfileActivity.this).inflate(R.layout.dialog_editprofile_email, null, false);
            Session s = spareroomContext.getSession();

            _tvEmail = contentView.findViewById(R.id.dialog_editprofile_email_tvEmail);
            _tvEmail2 = contentView.findViewById(R.id.dialog_editprofile_email_tvEmail2);

            _tvEmail.setText(s.get_email());
            _tvEmail2.setText(s.get_email());

            Dialog d = new AlertDialogBuilder(EditProfileActivity.this, contentView).create();
            Button _bPositive = contentView.findViewById(R.id.dialog_bPositive);
            Button _bNegative = contentView.findViewById(R.id.dialog_bNegative);
            _bPositive.setOnClickListener(new OKOnClickListener(d));
            _bNegative.setOnClickListener(new CancelOnClickListener(d));
            d.show();
        }

        private class OKOnClickListener implements View.OnClickListener {
            private final Dialog _dialog;

            OKOnClickListener(Dialog d) {
                _dialog = d;
            }

            @Override
            public void onClick(View v) {
                if (_tvEmail.getText() != null && _tvEmail2.getText() != null) {
                    if (!"".equals(_tvEmail.getText().toString()) && !"".equals(_tvEmail2.getText().toString())) {
                        if (_tvEmail.getText().toString().equals(_tvEmail2.getText().toString())) {
                            _newEmail = _tvEmail.getText().toString();
                            EditProfileActivity.this._tvEmail.setText(_newEmail);
                            _dialog.dismiss();
                            supportInvalidateOptionsMenu();
                        } else {
                            Toast.makeText(EditProfileActivity.this, getString(R.string.dialog_editProfile_confirm_tEmailsMismatch), Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        }

        private class CancelOnClickListener implements View.OnClickListener {
            private final Dialog _dialog;

            public CancelOnClickListener(Dialog d) {
                _dialog = d;
            }

            @Override
            public void onClick(View v) {
                _dialog.dismiss();
            }
        }
    }

    private class PasswordOnClickListener implements OnClickListener {
        private TextView _tvPassword;
        private TextView _tvPassword2;

        @Override
        public void onClick(View v) {
            View contentView = LayoutInflater.from(EditProfileActivity.this).inflate(R.layout.dialog_editprofile_password, null, false);

            _tvPassword = contentView.findViewById(R.id.dialog_editprofile_password_tvNewPassword);
            _tvPassword2 = contentView.findViewById(R.id.dialog_editprofile_password_tvNewPassword2);

            Dialog d = new AlertDialogBuilder(EditProfileActivity.this, contentView).create();
            Button _bPositive = contentView.findViewById(R.id.dialog_bPositive);
            Button _bNegative = contentView.findViewById(R.id.dialog_bNegative);
            _bPositive.setOnClickListener(new OKOnClickListener(d));
            _bNegative.setOnClickListener(new CancelOnClickListener(d));
            d.show();
        }

        private class OKOnClickListener implements View.OnClickListener {
            private final Dialog _dialog;

            OKOnClickListener(Dialog d) {
                _dialog = d;
            }

            @Override
            public void onClick(View v) {
                if (_tvPassword.getText() != null && _tvPassword2.getText() != null) {
                    if (!"".equals(_tvPassword.getText().toString()) && !"".equals(_tvPassword2.getText().toString())) {
                        if (_tvPassword.getText().toString().equals(_tvPassword2.getText().toString())) {
                            _newPassword = _tvPassword.getText().toString();
                            _dialog.dismiss();
                            supportInvalidateOptionsMenu();
                        } else {
                            Toast.makeText(EditProfileActivity.this, getString(R.string.dialog_editProfile_confirm_tPasswordsMismatch), Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        }

        private class CancelOnClickListener implements View.OnClickListener {
            private final Dialog _dialog;

            public CancelOnClickListener(Dialog d) {
                _dialog = d;
            }

            @Override
            public void onClick(View v) {
                _dialog.dismiss();
            }
        }
    }

    @Override
    public void notifyEmptyManager() {
        if (_updateDialog != null)
            _updateDialog.dismiss();

    }

    private final IAsyncResult _profilePreferencesListener = new IAsyncResultImpl() {

        private View buildBooleanWidget(ProfilePreference pp) {
            CheckBox cbPreference = new AppCompatCheckBox(EditProfileActivity.this);
            LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(getResources().getDimensionPixelSize(R.dimen.padding_screen_sides), 0, getResources().getDimensionPixelSize(R.dimen.padding_screen_sides), 0);
            cbPreference.setLayoutParams(lp);
            cbPreference.setTextColor(ContextCompat.getColor(EditProfileActivity.this, R.color.material_deep_sea_blue));
            cbPreference.setText(pp.get_description());

            String trueValue = "Y";
            if (pp.hasAvailableOption()) {
                for (ProfilePreference.AvailableOption ao : pp.get_listAvailableOption()) {
                    if (ao._dataValue.equals("true")) {
                        trueValue = ao._value;
                        break;
                    }
                }
            }
            cbPreference.setChecked(pp.get_permission().equals(trueValue));
            cbPreference.setOnClickListener(new CheckBoxListener(pp));
            return cbPreference;
        }

        private View buildEnumWidget(ProfilePreference pp) {
            LinearLayout llOption = new LinearLayout(EditProfileActivity.this);
            llOption.setOrientation(LinearLayout.HORIZONTAL);
            LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(getResources().getDimensionPixelSize(R.dimen.padding_screen_sides), 0, getResources().getDimensionPixelSize(R.dimen.padding_screen_sides), 0);
            llOption.setGravity(Gravity.CENTER_VERTICAL);
            llOption.setLayoutParams(lp);
            TextView tvLabel = new TextView(EditProfileActivity.this);
            tvLabel.setText(pp.get_description());
            llOption.addView(tvLabel);
            Spinner spPreference = new Spinner(EditProfileActivity.this);
            spPreference.setOnItemSelectedListener(new OptionItemSelected(pp));
            spPreference.setAdapter(new PreferenceSpinnerAdapter(pp));
            ProfilePreference.AvailableOption ao = new ProfilePreference.AvailableOption(pp.get_permission(), null, null);
            int positionSelected = pp.get_listAvailableOption().indexOf(ao);
            spPreference.setSelection(positionSelected);
            llOption.addView(spPreference);
            return llOption;
        }

        class OptionItemSelected implements android.widget.AdapterView.OnItemSelectedListener {
            private ProfilePreference _pp;
            private boolean firstSelection = true;

            OptionItemSelected(ProfilePreference pp) {
                _pp = pp;
            }

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                _pp.set_permission(_pp.get_listAvailableOption().get(position)._value);
                if (firstSelection) {
                    firstSelection = false;
                    return;
                }
                _preferencesChanged = true;
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        }

        @Override
        public void update(Object o) {
            _pb.setVisibility(View.GONE);
            if (o instanceof ProfilePreferenceList) {
                ProfilePreferenceList ppl = (ProfilePreferenceList) o;
                _newEmailPreferences = ppl;
                for (ProfilePreference pp : ppl.get_lProfilePreference()) {
                    if (pp.get_dataType() == null) {
                        _llEmailPreferences.addView(buildBooleanWidget(pp));
                    } else {
                        if (pp.get_dataType().equals(ProfilePreference.DATA_TYPE_BOOLEAN))
                            _llEmailPreferences.addView(buildBooleanWidget(pp));
                        else if (pp.get_dataType().equals(ProfilePreference.DATA_TYPE_ENUM))
                            _llEmailPreferences.addView(buildEnumWidget(pp));
                    }
                }
            }
        }

        class PreferenceSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {
            private List<ProfilePreference.AvailableOption> _lOption;

            PreferenceSpinnerAdapter(ProfilePreference pp) {
                _lOption = pp.get_listAvailableOption();
            }

            @Override
            public int getCount() {
                return _lOption.size();
            }

            @Override
            public Object getItem(int position) {
                return _lOption.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = convertView;
                if (view == null) {
                    view = LayoutInflater.from(EditProfileActivity.this).inflate(R.layout.actionbar_spinner_dropdown_item, parent, false);
                }

                TextView tv = view.findViewById(R.id.actionbar_spinner_dropdown_item_tvTitle);
                tv.setText(_lOption.get(position)._description);
                return view;
            }

        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(EditProfileActivity.this).show();
        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(EditProfileActivity.this).show();
        }

        @Override
        public void handleServiceUnavailableException(String message) {
            _pb.setVisibility(View.GONE);

            if ((message != null) && (message.contains("Certificate not valid until"))) {
                Calendar calendar = Calendar.getInstance();
                Calendar certificateDate = new GregorianCalendar(2009, Calendar.APRIL, 2, 13, 0);

                if (calendar.before(certificateDate)) {
                    DialogUtil.timeDateDialog(EditProfileActivity.this).show();
                }
            }

        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(EditProfileActivity.this).show();
        }
    };

    private final IAsyncResult _updateNameListener = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            if (_updateDialog != null)
                _updateDialog.dismiss();
            if (o instanceof ProfilePreferenceList) {
                _newFirstName = null;
                _newLastName = null;
                isLaunchUpdateName = true;
                finishEditProfile();
            } else if (o instanceof SpareroomStatus) {
                Toast.makeText(EditProfileActivity.this, ((SpareroomStatus) o).getMessage(), Toast.LENGTH_LONG).show();
            } else
                Toast.makeText(EditProfileActivity.this, "Error", Toast.LENGTH_LONG).show();
        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(EditProfileActivity.this).show();
        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(EditProfileActivity.this).show();
        }

        @Override
        public void handleServiceUnavailableException(String message) {
            _pb.setVisibility(View.GONE);

        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(EditProfileActivity.this).show();
        }

    };

    private final IAsyncResult _updateEmailListener = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            _updateDialog.dismiss();
            if (o instanceof ProfilePreferenceList) {
                _newEmail = null;
                isLaunchUpdateEmail = true;
                finishEditProfile();
            } else if (o instanceof SpareroomStatus) {
                Toast.makeText(EditProfileActivity.this, ((SpareroomStatus) o).getMessage(), Toast.LENGTH_LONG).show();
            } else
                Toast.makeText(EditProfileActivity.this, "Error", Toast.LENGTH_LONG).show();
        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(EditProfileActivity.this).show();
        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(EditProfileActivity.this).show();
        }

        @Override
        public void handleServiceUnavailableException(String message) {
            _pb.setVisibility(View.GONE);

        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(EditProfileActivity.this).show();
        }
    };
    private final IAsyncResult _updateEmailPreferencesListener = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            _updateDialog.dismiss();
            if (o instanceof ProfilePreferenceList) {
                _preferencesChanged = false;
                isLaunchUpdateEmailPreferences = true;
                finishEditProfile();
            } else
                Toast.makeText(EditProfileActivity.this, "Error", Toast.LENGTH_LONG).show();
        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(EditProfileActivity.this).show();
        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(EditProfileActivity.this).show();
        }

        @Override
        public void handleServiceUnavailableException(String message) {
            _pb.setVisibility(View.GONE);

        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(EditProfileActivity.this).show();
        }
    };

    private final IAsyncResult _updatePasswordListener = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            _updateDialog.dismiss();
            if (o instanceof ProfilePreferenceList) {

                if (_newFirstName != null || _newLastName != null) {
                    launchUpdateName(_newPassword);
                }
                if (_newEmail != null) {
                    launchUpdateEmail(_newPassword);
                }
                if (_preferencesChanged) {
                    launchUpdateEmailPreferences();
                }
                _newPassword = null;
                isLaunchUpdatePassword = true;
                finishEditProfile();
            } else if (o instanceof SpareroomStatus) {
                Toast.makeText(EditProfileActivity.this, ((SpareroomStatus) o).getMessage(), Toast.LENGTH_LONG).show();
            } else
                Toast.makeText(EditProfileActivity.this, "Error", Toast.LENGTH_LONG).show();
        }

        @Override
        public void handleMissingParameterException(String message) {
            DialogUtil.updateDialog(EditProfileActivity.this).show();
        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            DialogUtil.updateDialog(EditProfileActivity.this).show();
        }

        @Override
        public void handleServiceUnavailableException(String message) {
            _pb.setVisibility(View.GONE);

        }

        @Override
        public void handleInconsistentStateException(String message) {
            DialogUtil.updateDialog(EditProfileActivity.this).show();
        }
    };

}
