package com.spareroom.ui.filters.provider

import com.spareroom.ui.filters.provider.FilterOfferedMainItemProvider.OFFERED_AVAILABILITY
import com.spareroom.ui.filters.provider.FilterOfferedMainItemProvider.OFFERED_BEDROOM_TYPE
import com.spareroom.ui.filters.provider.FilterOfferedMainItemProvider.OFFERED_MONTHLY_BUDGET
import com.spareroom.ui.filters.provider.FilterOfferedMainItemProvider.OFFERED_POSTED_BY
import com.spareroom.ui.filters.provider.FilterOfferedMainItemProvider.OFFERED_PROPERTY_TYPE
import com.spareroom.ui.filters.provider.FilterOfferedMainItemProvider.OFFERED_SHARING_WITH
import com.spareroom.ui.filters.provider.FilterOfferedMainItemProvider.OFFERED_SUITABLE_FOR
import com.spareroom.ui.filters.provider.FilterWantedMainItemProvider.WANTED_AGE
import com.spareroom.ui.filters.provider.FilterWantedMainItemProvider.WANTED_AVAILABILITY
import com.spareroom.ui.filters.provider.FilterWantedMainItemProvider.WANTED_BEDROOM_PREFERENCE
import com.spareroom.ui.filters.provider.FilterWantedMainItemProvider.WANTED_EMPLOYMENT_STATUS
import com.spareroom.ui.filters.provider.FilterWantedMainItemProvider.WANTED_GENDER
import com.spareroom.ui.filters.provider.FilterWantedMainItemProvider.WANTED_MONTHLY_BUDGET
import com.spareroom.ui.filters.provider.FilterWantedMainItemProvider.WANTED_RELATIONSHIP
import com.spareroom.ui.filters.provider.FilterWantedMainItemProvider.WANTED_SMOKING

object FilterItemProviderFactory {

    @JvmStatic
    fun getItemProvider(itemProviderName: String): AbstractItemProvider {
        return when (itemProviderName) {
            OFFERED_AVAILABILITY -> FilterOfferedAvailabilityItemProvider()
            OFFERED_BEDROOM_TYPE -> FilterOfferedBedroomTypeItemProvider()
            OFFERED_MONTHLY_BUDGET -> FilterOfferedBudgetItemProvider()
            OFFERED_PROPERTY_TYPE -> FilterOfferedPropertyTypeItemProvider()
            OFFERED_SHARING_WITH -> FilterOfferedSharingWithItemProvider()
            OFFERED_SUITABLE_FOR -> FilterOfferedSuitableForItemProvider()
            OFFERED_POSTED_BY -> FilterOfferedPostedByItemProvider()
            WANTED_AGE -> FilterWantedAgeItemProvider()
            WANTED_AVAILABILITY -> FilterWantedAvailabilityItemProvider()
            WANTED_BEDROOM_PREFERENCE -> FilterWantedBedroomTypeItemProvider()
            WANTED_MONTHLY_BUDGET -> FilterWantedBudgetItemProvider()
            WANTED_EMPLOYMENT_STATUS -> FilterWantedEmploymentItemProvider()
            WANTED_GENDER -> FilterWantedGenderTypeItemProvider()
            WANTED_RELATIONSHIP -> FilterWantedRelationshipItemProvider()
            WANTED_SMOKING -> FilterWantedSmokingItemProvider()
            else -> throw IllegalStateException()
        }

    }

}
