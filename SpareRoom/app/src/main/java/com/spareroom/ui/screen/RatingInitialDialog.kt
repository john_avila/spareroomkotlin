package com.spareroom.ui.screen

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.KeyEvent
import com.spareroom.R
import com.spareroom.controller.SpareroomApplication
import com.spareroom.integration.analytics.AnalyticsTrackerComposite
import com.spareroom.integration.dependency.Injectable
import com.spareroom.integration.sharedpreferences.EventsDao
import com.spareroom.ui.util.AppRating
import java.util.*
import javax.inject.Inject

class RatingInitialDialog : DialogFragment(), Injectable {

    private var tappedOutside = true

    @Inject
    lateinit var eventsDao: EventsDao

    @Inject
    lateinit var appRating: AppRating

    companion object {
        const val TAG = "RatingInitialDialogTag"
        fun getInstance() = RatingInitialDialog()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if (savedInstanceState == null)
            eventsDao.logShowInitialRatingPopupEvent(Calendar.getInstance())

        return AlertDialogBuilder(activity!!)
            .setTitle(R.string.rating_initial_title)
            .setMessage(message())
            .setPositiveButton(R.string.rate_app) { _, _ -> rateApp() }
            .setNegativeButton(R.string.maybe_later) { _, _ -> delayRating() }
            .setOnKeyListener { _, keyCode, keyEvent -> handleBackButton(keyCode, keyEvent) }
            .create()
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        if (isDismissedByTapOutside())
            AnalyticsTrackerComposite.getInstance().trackInitialRatingPopupTapOutside()
    }

    private fun message(): String {
        val session = SpareroomApplication.getSpareRoomContext().session
        val isOffering = session?.isOffering ?: false
        val isSearching = session?.isSearching ?: false
        val searchingFor = when {
            isOffering -> getString(R.string.flatmate)
            isSearching -> getString(R.string.room)
            else -> "${getString(R.string.room)} ${getString(R.string.or)} ${getString(R.string.flatmate)}"
        }
        return "${getString(R.string.rating_initial_message)} $searchingFor."
    }

    private fun delayRating() {
        resetTapOutside()
        AnalyticsTrackerComposite.getInstance().trackInitialRatingPopupMaybeLater()
        dismiss()
    }

    private fun rateApp() {
        resetTapOutside()
        AnalyticsTrackerComposite.getInstance().trackInitialRatingPopupRateApp()
        eventsDao.logRateAppEvent()

        appRating.rateApp(activity as Activity)
        dismiss()
    }

    private fun handleBackButton(keyCode: Int, keyEvent: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_UP == keyEvent?.action) {
            resetTapOutside()
            AnalyticsTrackerComposite.getInstance().trackInitialRatingPopupBackButton()
        }

        return false
    }

    private fun resetTapOutside() {
        tappedOutside = false
    }

    private fun isDismissedByTapOutside() = tappedOutside && isResumed
}