package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.ui.widget.PlaceAdButton;

public class PlaceAdWantedIAmFragment extends PlaceAdFragment {
    private PlaceAdButton _bMale;
    private PlaceAdButton _bFemale;
    private PlaceAdButton _bMaleFemale;
    private PlaceAdButton _b2Male;
    private PlaceAdButton _b2Female;

    private PlaceAdButton _previouslySelected;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_wanted_iam, container, false);

        setTitle(getString(R.string.place_ad_wanted_iam_title));

        _bMale = rootView.findViewById(R.id.place_ad_wanted_iam_bMale);
        _bFemale = rootView.findViewById(R.id.place_ad_wanted_iam_bFemale);
        _bMaleFemale = rootView.findViewById(R.id.place_ad_wanted_iam_bMaleFemale);
        _b2Male = rootView.findViewById(R.id.place_ad_wanted_iam_b2Male);
        _b2Female = rootView.findViewById(R.id.place_ad_wanted_iam_b2Female);

        DraftAdWanted.AdvertiserType advertiserType = ((DraftAdWanted) getDraftAd()).get_advertiserType();
        if (((DraftAdWanted) getDraftAd()).get_advertiserType() != null) {
            switch (advertiserType) {
                case FEMALE:
                    _previouslySelected = _bFemale;
                    _bFemale.setPreviouslySelected();
                    break;
                case MALE:
                    _previouslySelected = _bMale;
                    _bMale.setPreviouslySelected();
                    break;
                case MALE_FEMALE:
                    _previouslySelected = _bMaleFemale;
                    _bMaleFemale.setPreviouslySelected();
                    break;
                case TWO_FEMALES:
                    _previouslySelected = _b2Female;
                    _b2Female.setPreviouslySelected();
                    break;
                case TWO_MALES:
                    _previouslySelected = _b2Male;
                    _b2Male.setPreviouslySelected();
                    break;
                default:
                    break;
            }
        }

        _bMale.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            _bMale.setPreviouslySelected();
            ((DraftAdWanted) getDraftAd()).set_advertiserType(DraftAdWanted.AdvertiserType.MALE);
            finish();
        });

        _bFemale.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            _bFemale.setPreviouslySelected();
            ((DraftAdWanted) getDraftAd()).set_advertiserType(DraftAdWanted.AdvertiserType.FEMALE);
            finish();
        });

        _bMaleFemale.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            _bMaleFemale.setPreviouslySelected();
            ((DraftAdWanted) getDraftAd()).set_advertiserType(DraftAdWanted.AdvertiserType.MALE_FEMALE);
            finish();
        });

        _b2Male.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            _b2Male.setPreviouslySelected();
            ((DraftAdWanted) getDraftAd()).set_advertiserType(DraftAdWanted.AdvertiserType.TWO_MALES);
            finish();
        });

        _b2Female.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            _b2Female.setPreviouslySelected();
            ((DraftAdWanted) getDraftAd()).set_advertiserType(DraftAdWanted.AdvertiserType.TWO_FEMALES);
            finish();
        });

        return rootView;
    }
}
