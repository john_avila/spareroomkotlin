package com.spareroom.ui.screen;

import android.os.Bundle;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.lib.core.IObservable;
import com.spareroom.lib.core.IObserver;
import com.spareroom.model.business.AccountRegisterForm;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.ui.EmailFieldValidator;
import com.spareroom.ui.FieldValidator;
import com.spareroom.ui.controller.*;
import com.spareroom.ui.util.KeyboardUtils;
import com.spareroom.ui.widget.TextBox;

import java.util.LinkedList;

import androidx.annotation.Nullable;

import static com.spareroom.ui.screen.RegisterActivity.KEY_REGISTER_FORM;

/**
 * Fragment for set the email for the registration process
 */
public class RegisterEmailFragment extends Fragment {

    //region FIELDS UI.CONTROL
    private static final String FIELD_USERNAME = "username";

    private RegisterProcessCompositeUiController _registerProcessCompositeUiController;

    //endregion FIELDS UI.CONTROL

    private TextBox tbEmail;

    //region METHODS LIFECYCLE

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        View rootView = inflater.inflate(R.layout.material_register_email_fragment, container, false);

        // Email input
        tbEmail = rootView.findViewById(R.id.registerEmailFragment_emailTextBox);
        // Bottom Toolbar
        View bottomToolbar = getActivity().findViewById(R.id.activity_toolbarBottom);
        final TextView tvBottomToolbarRight = bottomToolbar.findViewById(R.id.materialToolbarBottom_rightText);
        // Controllers
        ActionButtonUiController _registerButtonUiController = new ActionButtonUiController(bottomToolbar);
        ViewUiController _emailTextBoxUiController = new EmailTextBoxUiController(tbEmail);
        _emailTextBoxUiController.enable();

        _registerProcessCompositeUiController = new RegisterProcessCompositeUiController(_emailTextBoxUiController, _registerButtonUiController);

        ErrorStateController errorStateController = new ErrorStateController(_registerProcessCompositeUiController);

        _registerProcessCompositeUiController.addObserver(errorStateController.getErrorObserver());

        // Toolbar
        setToolbar(getActivity().findViewById(R.id.appBarLayout), getString(R.string.registerEmailFragment_toolbarTitle), true);

        // Bottom Toolbar
        bottomToolbar.setVisibility(View.VISIBLE);

        tvBottomToolbarRight.setOnClickListener(new RegisterEmailButtonOnClickListener(tbEmail));
        tvBottomToolbarRight.setText(getString(R.string.registerEmailFragment_bottomToolbar));

        // Email
        tbEmail.setOnContentChangedListener(
                new EmailOnContentChangedListener(new ClearEmailObserver(_registerButtonUiController), errorStateController.getInputObserver()));

        tbEmail.setOnEditorActionListener(
                (v, actionId, event) -> {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        tvBottomToolbarRight.performClick();
                        return true;
                    }
                    return false;
                });

        AccountRegisterForm form = ((AccountRegisterForm) ((LinearMultiStepActivity) getActivity()).getSharedObject(KEY_REGISTER_FORM));
        if (form.getEmail() != null) {
            tbEmail.setText(form.getEmail());
            if (form.getEmail().length() > 0)
                _registerButtonUiController.enable();
            else
                _registerButtonUiController.disable();
        } else {
            _registerButtonUiController.disable();
        }

        return rootView;
    } // end onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RegisterActivity) getActivity()).showKeyboard();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ((LinearMultiStepActivity) getActivity()).onNavigateBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        tbEmail.setOnContentChangedListener(null);
        tbEmail.setOnEditorActionListener(null);
    }

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    /**
     * UiController that defines the status of the Username TextBox
     */
    private class EmailTextBoxUiController implements ViewUiController {
        private final TextBox _tbUsername;

        EmailTextBoxUiController(TextBox tbUsername) {
            _tbUsername = tbUsername;

        }

        /**
         * For this UiController, reset means clearing the error displayed. We don't change the
         * text in the input field.
         */
        @Override
        public void enable() {
            _tbUsername.setEnabled(true);

        }

        @Override
        public void disable() {
            _tbUsername.setEnabled(false);
            KeyboardUtils.hideKeyboard(_tbUsername);
        }

        /**
         * For this UiController, setLoading means locking the TextBox while performing the
         * operation
         */
        @Override
        public void setLoading() {
            _tbUsername.setEnabled(false);
            KeyboardUtils.hideKeyboard(_tbUsername);
        }

        /**
         * This processes an error on this field
         */
        @Override
        public void setStatus(SpareroomStatus spareroomStatus) {
            _tbUsername.setEnabled(false);

            if (spareroomStatus != null) {
                _tbUsername.setMessageText(spareroomStatus.getMessage());
            } else {
                _tbUsername.setMessageText(getString(R.string.loginEmailFragment_errorGeneric));
            }

            _tbUsername.showMessage();

        }

        @Override
        public void clearStatus() {
        }

    }

    private class RegisterEmailButtonOnClickListener implements View.OnClickListener {
        private final TextBox _tbEmail;
        private final FieldValidator _emailFieldValidator;

        private RegisterEmailButtonOnClickListener(TextBox tbEmail) {
            _tbEmail = tbEmail;
            _emailFieldValidator = new EmailFieldValidator();
        }

        @Override
        public void onClick(View v) {
            String email = _tbEmail.getText();

            int usernameValidationState = _emailFieldValidator.validate(email);

            if (usernameValidationState != EmailFieldValidator.VALID) {
                SpareroomStatus spareroomStatus = new SpareroomStatus();
                spareroomStatus.setCode(Integer.toString(usernameValidationState));
                switch (usernameValidationState) {
                    case EmailFieldValidator.EMPTY:
                        spareroomStatus.setMessage(getString(R.string.loginEmailFragment_errorUsernameEmpty));
                        break;
                    case EmailFieldValidator.MALFORMED_ADDRESS:
                        spareroomStatus.setMessage(getString(R.string.loginEmailFragment_errorUsernameMalformed));
                        break;
                }
                spareroomStatus.setCause(FIELD_USERNAME);
                _registerProcessCompositeUiController.setSpareroomStatus(spareroomStatus);
            } else { // VALID
                ((AccountRegisterForm) ((LinearMultiStepActivity) getActivity()).getSharedObject(KEY_REGISTER_FORM)).setEmail(email);
                ((LinearMultiStepActivity) getActivity()).nextFragment();
            }

        }

    } //end class RegisterEmailButtonOnClickListener

    /**
     * UiController that defines how to enable or disable the SignInButton
     */
    private class ClearEmailObserver implements IObserver {
        private final ViewUiController _signInButtonController;
        private boolean _isEnabled = false;
        private boolean _isEmailEmpty = true;

        ClearEmailObserver(ViewUiController signInButtonController) {
            _signInButtonController = signInButtonController;
        }

        @Override
        public void notifyObserver(Object o) {
            String username = (String) o;
            _isEmailEmpty = (username == null) || (username.length() == 0);

            if (_isEmailEmpty) {
                if (_isEnabled) {
                    _signInButtonController.disable();
                    _isEnabled = false;
                }
            } else {
                if (!_isEnabled) {
                    _signInButtonController.enable();
                    _isEnabled = true;
                }
            }
        }

    } //end class EnableSignInButtonUiController

    private class EmailOnContentChangedListener implements TextBox.OnContentChangedListener, IObservable {
        private final LinkedList<IObserver> _observerList = new LinkedList<>();

        EmailOnContentChangedListener(IObserver emptyFieldObserver, IObserver errorStateObserver) {

            // I decided to use the constructor header for the observers rather than the public
            // method addObserver because the order in which the actual observers we are using
            // execute matters, and using addObserver does not explicitly set the order in which
            // observers should be notified
            addObserver(errorStateObserver);
            addObserver(emptyFieldObserver);
        }

        @Override
        public void onContentChangedEvent(CharSequence s) {

            notifyObservers(s.toString());

            AccountRegisterForm form = ((AccountRegisterForm) ((LinearMultiStepActivity) getActivity()).getSharedObject(KEY_REGISTER_FORM));

            form.setEmail(s.toString());

            ((LinearMultiStepActivity) getActivity()).setSharedObject(KEY_REGISTER_FORM, form);

        }

        @Override
        public void addObserver(IObserver o) {
            _observerList.add(o);
        }

        @Override
        public void removeObserver(IObserver o) {
            _observerList.remove(o);
        }

        private void notifyObservers(Object o) {
            for (IObserver observer : _observerList) {
                observer.notifyObserver(o);
            }
        }
    } //end class EmailOnContentChangedListener

    //endregion INNER CLASSES
}
