package com.spareroom.ui.util;

import android.app.Application;
import android.content.Context;

import com.spareroom.R;

import java.text.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static com.spareroom.ui.util.StringUtils.isNullOrEmpty;

public class DateUtils {
    public static final String YEAR_MONTH_DAY_FORMAT = "yyyy-MM-dd";
    private static final String ISO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    static final String DOT_GLYPH = "\u2022";

    private final int MAX_DAY_WITH_WEEKDAY_NAME = 6;

    private final Locale locale;
    private final Context context;
    private final SimpleDateFormat inputDateFormat;
    private final SimpleDateFormat outputDateFormatDays;
    private final SimpleDateFormat inputDateFormatAvailability;
    private final SimpleDateFormat outputDateFormatDayAndMonth;
    private final SimpleDateFormat outputTimeFormat;
    private final Calendar calendar = Calendar.getInstance();
    private final Calendar calendarNow = Calendar.getInstance();

    private final String today;
    private final String yesterday;

    @Inject
    public DateUtils(Locale locale, Application context) {
        this.locale = locale;
        this.context = context;
        this.inputDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", locale);
        this.outputDateFormatDays = new SimpleDateFormat("EEEE", locale);
        this.inputDateFormatAvailability = new SimpleDateFormat(YEAR_MONTH_DAY_FORMAT, locale);
        this.outputDateFormatDayAndMonth = new SimpleDateFormat("d MMMM", locale);
        this.outputTimeFormat = new SimpleDateFormat("h:mm aaa", locale);

        today = context.getString(R.string.today);
        yesterday = context.getString(R.string.yesterday);
    }

    public static void clearTime(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    public static long getTimeDifferenceInDays(Calendar calendarNow, Calendar calendarToCompare) {
        clearTime(calendarToCompare);
        clearTime(calendarNow);
        return TimeUnit.MILLISECONDS.toDays(calendarNow.getTimeInMillis() - calendarToCompare.getTimeInMillis());
    }

    public String getFormattedOnlineDate(String lastOnlineDate) {

        if (isNullOrEmpty(lastOnlineDate, true))
            return context.getString(R.string.not_available);

        try {
            Date inputDate = inputDateFormat.parse(lastOnlineDate);
            calendar.setTime(inputDate);

            long timeDifferenceInDays = getTimeDifferenceInDays(calendarNow, calendar);
            if (timeDifferenceInDays == 0)
                return today.toLowerCase();

            if (timeDifferenceInDays == 1)
                return yesterday.toLowerCase();

            if (timeDifferenceInDays >= 0 && timeDifferenceInDays <= MAX_DAY_WITH_WEEKDAY_NAME)
                return outputDateFormatDays.format(inputDate);

            if (timeDifferenceInDays < 0)
                return context.getString(R.string.not_available);

            return context.getString(R.string.over_week_ago);
        } catch (Exception e) {
            return context.getString(R.string.not_available);
        }
    }

    public String getFormattedAvailabilityDate(String availabilityDate) {
        if (isNullOrEmpty(availabilityDate, true))
            return context.getString(R.string.not_available);

        try {
            Date inputDate = inputDateFormatAvailability.parse(availabilityDate);
            calendar.setTime(inputDate);

            long timeDifferenceInDays = getTimeDifferenceInDays(calendarNow, calendar);
            if (timeDifferenceInDays >= 0)
                return context.getString(R.string.now);

            if (timeDifferenceInDays == -1)
                return context.getString(R.string.tomorrow);

            return outputDateFormatDayAndMonth.format(inputDate);
        } catch (Exception e) {
            return availabilityDate;
        }
    }

    public String getFormattedDateOfContact(String dateOfContact) {
        return getFormattedDateOfContact(dateOfContact, false);
    }

    public String getFormattedDateOfContact(String dateOfContact, boolean trimTime) {

        if (isNullOrEmpty(dateOfContact, true))
            return context.getString(R.string.not_available);

        try {
            Date inputDate = inputDateFormat.parse(dateOfContact);
            calendar.setTime(inputDate);

            long timeDifferenceInDays = getTimeDifferenceInDays(calendarNow, calendar);
            if (timeDifferenceInDays == 0)
                return formatDate(dateOfContact, today, trimTime);

            if (timeDifferenceInDays == 1)
                return formatDate(dateOfContact, yesterday, trimTime);

            if (timeDifferenceInDays >= 0 && timeDifferenceInDays <= MAX_DAY_WITH_WEEKDAY_NAME)
                return formatDate(dateOfContact, outputDateFormatDays.format(inputDate), trimTime);

            return formatDate(dateOfContact, outputDateFormatDayAndMonth.format(inputDate), trimTime);
        } catch (Exception e) {
            return dateOfContact;
        }
    }

    public static float getDuration(long start) {
        return (System.currentTimeMillis() - start) / 1000f;
    }

    private String formatDate(String dateString, String formattedDate, boolean trimTime) throws ParseException {
        if (trimTime)
            return formattedDate;
        return String.format("%s %s %s", outputTimeFormat.format(inputDateFormat.parse(dateString)).toUpperCase(locale), DOT_GLYPH, formattedDate);
    }

    String prefixWithZero(int date) {
        String value = String.valueOf(date);
        return value.length() != 1 ? value : "0" + value;
    }

    /**
     * Save the date as String with format "yyyy-MM-dd" (<code>DateUtils.YEAR_MONTH_DAY_FORMAT</code>)
     *
     * @param year needs to have four digits
     */
    public String createStringDate(int year, int month, int day) {
        return year + "-" + prefixWithZero(month) + "-" + prefixWithZero(day);
    }

    public Calendar getCalendar(@NonNull String currentFormat, @NonNull String date) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(currentFormat, locale);
        try {
            c.setTime(sdf.parse(date));
        } catch (ParseException e) {
            return c;
        }
        return c;
    }

    @Nullable
    public Calendar parseIsoDate(@NonNull String date) {
        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat(ISO_DATE_FORMAT, locale);
            calendar.setTime(sdf.parse(date));
            return calendar;
        } catch (Exception e) {
            return null;
        }
    }

    public String getMonthName(int month) {
        return new DateFormatSymbols().getMonths()[month];
    }

}
