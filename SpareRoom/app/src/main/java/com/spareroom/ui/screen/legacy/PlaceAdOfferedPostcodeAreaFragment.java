package com.spareroom.ui.screen.legacy;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.controller.legacy.*;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.lib.util.HardwareFeatures;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.model.business.Parameters;
import com.spareroom.platform.location.ILocationObserver;
import com.spareroom.platform.location.LocationService;
import com.spareroom.ui.screen.AlertDialogBuilder;
import com.spareroom.ui.util.*;

import java.text.DecimalFormat;

import javax.inject.Inject;

import androidx.annotation.NonNull;

public class PlaceAdOfferedPostcodeAreaFragment extends PlaceAdFragment implements ILocationObserver, Injectable {

    private static final int MY_PERMISSIONS_REQUEST_READ_LOCATION = 1;

    private EditText _etPostcode;
    private LocationService _ls;
    private View bConfirm;

    private final AsyncTaskManager _asyncManager = new AsyncTaskManager();

    @Inject
    ConnectivityChecker connectivityChecker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View rootView = inflater.inflate(R.layout.place_ad_offered_postcode_area, container, false);

        setTitle(getString(R.string.place_ad_offered_postcode_title));

        _etPostcode = rootView.findViewById(R.id.fragment_place_ad_offered_postcode_area_etPostcode);
        bConfirm = rootView.findViewById(R.id.fragment_place_ad_offered_postcode_area_bConfirm);

        DraftAdOffered draft = (DraftAdOffered) getDraftAd();

        _etPostcode.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_NEXT && (!StringUtils.isNullOrEmpty(_etPostcode.getText(), true)))
                confirm();

            return false;
        });

        bConfirm.setOnClickListener(v -> {
            if (StringUtils.isNullOrEmpty(_etPostcode.getText(), true)) {
                Toast.makeText(getActivity(), getString(R.string.enter_postcode), Toast.LENGTH_SHORT).show();
                return;
            }

            if (connectivityChecker.isConnected()) {
                confirm();
            } else {
                Toast.makeText(getActivity(), getString(R.string.no_connection), Toast.LENGTH_LONG).show();
            }
        });

        _ls = new LocationService(getActivity().getApplicationContext());
        _ls.addObserver(PlaceAdOfferedPostcodeAreaFragment.this);

        if (draft.get_postCode() != null && !draft.get_postCode().isEmpty()) {
            if (!draft.get_postCode().equals(getString(R.string.placeAdOfferedPostcodeAreaFragment_retrieving)))
                _etPostcode.setText(draft.get_postCode());
            else
                _etPostcode.setText("");
        } else {
            if (((DraftAdOffered) getDraftAd()).is_isAtProperty() && HardwareFeatures.supportsLocation(getActivity()))
                if (PermissionUtils.hasLocationPermissions(getActivity())) {
                    lockLocation();
                    _ls.enableUpdates();
                } else {
                    PermissionUtils.requestLocationPermissions(this, MY_PERMISSIONS_REQUEST_READ_LOCATION);
                }
        }

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        _etPostcode.setOnEditorActionListener(null);
        bConfirm.setOnClickListener(null);
        _ls.removerObserver(this);
    }

    private void confirm() {
        DraftAdOffered draft = (DraftAdOffered) getDraftAd();
        draft.set_postCode(_etPostcode.getText().toString());

        finish();
    }

    @Override
    public void updateLocation(Location l) {
        SpareroomApplication srApplication = SpareroomApplication.getInstance(getActivity().getApplicationContext());
        DecimalFormat df = new DecimalFormat("####.########");
        Parameters p = new Parameters();

        p.add("latitude", df.format(l.getLatitude()));
        p.add("longitude", df.format(l.getLongitude()));
        Point2PostcodeAsyncTask at =
                new Point2PostcodeAsyncTask(srApplication.getLocationFacade(), srApplication.getSpareroomContext(), arLocation);
        _asyncManager.register(at);
        at.execute(p);
    }

    @Override
    public void onPause() {
        super.onPause();
        _ls.disableUpdates();
        if (_etPostcode.getText().toString().equals(getString(R.string.placeAdOfferedPostcodeAreaFragment_retrieving)))
            _etPostcode.setText("");
    }

    @Override
    protected void finish() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(_etPostcode.getWindowToken(), 0);
        super.finish();
    }

    private final IAsyncResult arLocation = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            if (o instanceof String) {
                _etPostcode.setText((String) o);
                releaseLocation();
            }
        }

    };

    @Override
    public void locationProviderDisabled() {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_message_buttons, null);
        TextView tvMessage = v.findViewById(R.id.dialog_message);
        Button bPositive = v.findViewById(R.id.dialog_bPositive);
        Button bNegative = v.findViewById(R.id.dialog_bNegative);

        tvMessage.setText(getString(R.string.dialog_location_message));

        Dialog d = new AlertDialogBuilder(getActivity(), v).create();

        bPositive.setOnClickListener(new SettingsOnClickListener(d));
        bPositive.setText(R.string.settings);
        bNegative.setOnClickListener(new DialogUtil.CancelOnClickListener(d));
        bNegative.setText(R.string.dialog_location_bCancel);

        d.show();
    }

    private class SettingsOnClickListener implements android.view.View.OnClickListener {
        private final Dialog _dialog;

        SettingsOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            _dialog.dismiss();
        }

    }

    private void releaseLocation() {
        _etPostcode.setEnabled(true);
    }

    private void lockLocation() {
        _etPostcode.setText(getString(R.string.placeAdOfferedPostcodeAreaFragment_retrieving));
        _etPostcode.setEnabled(false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    lockLocation();
                    _ls.enableUpdates();
                } else {
                    ToastUtils.showToast(getString(R.string.permission_denied));
                    releaseLocation();
                }
                break;
            }
        }
    }

}
