package com.spareroom.ui.util

import android.graphics.Typeface
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.facebook.FacebookException
import com.google.android.material.snackbar.Snackbar
import com.spareroom.R
import com.spareroom.integration.exception.*
import com.spareroom.model.exception.*

object SnackBarUtils {

    @Deprecated("", ReplaceWith("show()"))
    @JvmStatic
    fun showException(rootView: View, exception: Exception?) {
        when (exception) {
            is UnavailableDataSourceException, is ModelOperationCannotBePerformedException
            -> show(rootView, rootView.context.getString(R.string.snackBar_UnavailableDataSourceException))

            is InvalidArgumentException, is ModelInvalidArgumentException, is UnexpectedResultException, is ModelUnexpectedInternalIssueException
            -> show(rootView, rootView.context.getString(R.string.error_message))

            is FacebookException -> show(rootView, rootView.context.getString(R.string.snackBar_FacebookException))
        }
    }

    @JvmStatic
    @JvmOverloads
    fun show(rootView: View, message: CharSequence, actionTitle: CharSequence? = null, action: View.OnClickListener? = null,
             indefinite: Boolean = false): Snackbar {

        val snackbar = Snackbar.make(rootView, message, if (indefinite) Snackbar.LENGTH_INDEFINITE else Snackbar.LENGTH_LONG)

        if (actionTitle != null && action != null) {
            val actionButton = snackbar.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_action)
            actionButton.typeface = Typeface.create(rootView.context.getString(R.string.font_family_medium), Typeface.NORMAL)
            actionButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, rootView.context.resources.getDimensionPixelSize(R.dimen.textSize).toFloat())
            snackbar.setAction(actionTitle, action)
            snackbar.setActionTextColor(ContextCompat.getColor(rootView.context, R.color.orange))
        }

        snackbar.show()
        return snackbar
    }

}