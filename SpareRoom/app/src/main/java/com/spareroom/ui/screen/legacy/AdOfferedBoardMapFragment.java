package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.widget.TextView;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.*;
import com.spareroom.R;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.model.business.AbstractAd;
import com.spareroom.ui.util.MapsUtil;

import androidx.annotation.Nullable;

public class AdOfferedBoardMapFragment extends SupportMapFragment implements OnMarkerClickListener {
    private String _newness;
    private float _latitude;
    private float _longitude;
    private String _neighbourhood;
    private String _postcode;
    private String _accomType;
    private String _price;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AnalyticsTrackerComposite.getInstance().trackScreen_Fragment(this);
    }

    @Override
    public View onCreateView(LayoutInflater arg0, ViewGroup arg1, Bundle arg2) {
        View v = super.onCreateView(arg0, arg1, arg2);

        Bundle b = getArguments();
        _newness = b.getString("newness", "");
        _latitude = b.getFloat("latitude", 0f);
        _longitude = b.getFloat("longitude", 0f);
        _neighbourhood = b.getString("neighbourhood");
        _postcode = b.getString("postcode");
        _accomType = b.getString("accomType");
        _price = b.getString("price");

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getMapAsync(new MapReadyCallback());

    }

    @Override
    public boolean onMarkerClick(Marker arg0) {
        arg0.showInfoWindow();
        return true;
    }

    private class MyInfoWindowAdapter implements InfoWindowAdapter {

        @Override
        public View getInfoWindow(Marker arg0) {
            final View infoWindow = getActivity().getLayoutInflater().inflate(R.layout.map_marker_info_window, null);
            final TextView tvTitle = infoWindow.findViewById(R.id.map_marker_tvTitle);
            final TextView tvSnippet = infoWindow.findViewById(R.id.map_marker_tvSnippet);

            tvTitle.setText(arg0.getTitle());
            tvSnippet.setText(arg0.getSnippet());

            return infoWindow;
        }

        @Override
        public View getInfoContents(Marker arg0) {
            return null;
        }
    }

    private class MapReadyCallback implements OnMapReadyCallback {

        @Override
        public void onMapReady(GoogleMap googleMap) {

            if (googleMap != null) {
                MapsUtil.disableIndoor(googleMap);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(_latitude, _longitude), 14));
                googleMap.setOnMarkerClickListener(AdOfferedBoardMapFragment.this);

                googleMap.setInfoWindowAdapter(new MyInfoWindowAdapter());

                if (AbstractAd.NEWNESS_STATUS_NEW.equalsIgnoreCase(_newness)) {
                    googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(_latitude, _longitude))
                            .title(_neighbourhood + " (" + _postcode + ")")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin_new))
                            .snippet((_accomType != null ? _accomType + " - " : "") + _price)
                    );
                } else {
                    googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(_latitude, _longitude))
                            .title(_neighbourhood + " (" + _postcode + ")")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin_normal))
                            .snippet((_accomType != null ? _accomType + " - " : "") + _price)
                    );
                }
            }
        }
    }

}
