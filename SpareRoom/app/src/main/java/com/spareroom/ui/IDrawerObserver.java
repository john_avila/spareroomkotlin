package com.spareroom.ui;

public interface IDrawerObserver {

    void notifyDrawerChanged(String option);

}
