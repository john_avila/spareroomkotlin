package com.spareroom.ui.screen.legacy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.widget.*;

import com.google.android.material.tabs.TabLayout;
import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.controller.legacy.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.googleplay.billing.*;
import com.spareroom.integration.sharedpreferences.UpgradeFirstRunDao;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.model.business.*;
import com.spareroom.ui.adapter.legacy.UpgradePagerAdapter;
import com.spareroom.ui.screen.Activity;
import com.spareroom.ui.screen.AlertDialogBuilder;
import com.spareroom.ui.util.ToastUtils;

import java.util.*;

import androidx.viewpager.widget.ViewPager;

public class UpgradeFragmentActivity extends Activity {

    //region FIELDS UI

    private ViewPager viewPager;
    private ProgressBar _pb;
    private UpgradeList ul;

    //endregion FIELDS UI

    //region FIELDS CONTROLLER

    public static final String CONTEXT_ID_KEY = "context_id";
    public static final String CONTEXT_ID_VALUE = "46";

    private static final int RC_REQUEST = 10001;
    private static final Integer[] mem = {67, 81, 112, 119, 121, 122, 127, 138, 143};
    private IabHelper mHelper;

    private final AsyncTaskManager _asyncManager = new AsyncTaskManager();
    private String _contextId = CONTEXT_ID_VALUE;
    private final IAsyncResult _upgradeListListener = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {

            if (o instanceof UpgradeList) {
                ul = reorder((UpgradeList) o);

                for (Upgrade u : ul.get_upgradeList()) {
                    u.set_context(_contextId);
                }

                try {
                    mHelper = new IabHelper(UpgradeFragmentActivity.this, AppVersion.flavor().getKey());
                } catch (NullPointerException | IllegalStateException e) {
                    AnalyticsTrackerComposite.getInstance().trackEvent_Upgrade_BillingUnavailable_Create();
                    showDialogUnexpectedBillingProblem();
                }
                try {
                    mHelper.enableDebugLogging(false);
                } catch (NullPointerException | IllegalStateException e) {
                    AnalyticsTrackerComposite.getInstance().trackEvent_Upgrade_BillingUnavailable_DisableDebug();
                    showDialogUnexpectedBillingProblem();
                }
                try {
                    mHelper.startSetup(new UpgradeListOnIabSetupFinishedListener());
                } catch (NullPointerException | IllegalStateException e) {
                    AnalyticsTrackerComposite.getInstance().trackEvent_Upgrade_BillingUnavailable_Setup();
                    showDialogUnexpectedBillingProblem();
                }

            }
        }

        @Override
        public void handleMissingParameterException(String message) {
            _pb.setVisibility(View.GONE);
            showUpdateDialog();
        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            _pb.setVisibility(View.GONE);
            showUpdateDialog();
        }

        @Override
        public void handleServiceUnavailableException(String message) {
            _pb.setVisibility(View.GONE);
            ToastUtils.showToast(R.string.no_connection);
            finish();
        }

        @Override
        public void handleInconsistentStateException(String message) {
            _pb.setVisibility(View.GONE);
            showUpdateDialog();
        }

    };

    //endregion FIELDS CONTROLLER

    //region METHODS UI

    private void alert(String message) {
        if (!isActivityDestroyed()) {
            View v = LayoutInflater.from(this).inflate(R.layout.dialog_message_button, null);

            android.widget.TextView tvMessage = v.findViewById(R.id.dialog_message);
            Button bPositive = v.findViewById(R.id.dialog_bPositive);

            new AlertDialogBuilder(this, v).show();
            tvMessage.setText(message);
            bPositive.setOnClickListener(view -> finish());
        }

    }

    private void showUpdateDialog() {
        if (!isActivityDestroyed())
            DialogUtil.updateDialog(this).show();
    }

    private void updateUi() {
        viewPager.setAdapter(new UpgradePagerAdapter(getSupportFragmentManager(), ul, SpareroomApplication.getInstance(), _contextId, titles()));
        _pb.setVisibility(View.GONE);
    }

    private List<String> titles() {
        String productNameRemainder = getString(R.string.upgrade) + " (" + AppVersion.flavor().getGooglePlayAppName() + ")";

        List<String> titles = new ArrayList<>();
        titles.add(getString(R.string.activityUpgradeFragment_yourExistingUpgrade));

        for (Upgrade u : ul.get_upgradeList()) {
            String title = u.get_title();
            if (title != null && title.contains(productNameRemainder))
                title = title.substring(0, title.indexOf(productNameRemainder));

            titles.add(title);
        }

        return titles;

    }

    private void showDialogUnexpectedBillingProblem() {
        if (!isActivityDestroyed()) {
            AlertDialogBuilder builder = new AlertDialogBuilder(this);
            builder.setMessage(R.string.upgrade_dialog_tvBillingNotAvailable);
            builder.setPositiveButton(R.string.upgrade_dialog_bBillingNotAvailable, (dialog, which) -> finish());
            builder.show();
        }
    }

    //endregion METHODS UI

    //region METHODS CONTROLLER

    private UpgradeList reorder(UpgradeList ul) {
        LinkedList<Upgrade> lUpgrades = ul.get_upgradeList();

        for (int i = 0; i < lUpgrades.size(); i++) {
            Upgrade u = lUpgrades.get(i);

            if (u.get_code().equals(AppVersion.appId() + ".eba30days") || u.get_code().equals(AppVersion.appId() + ".b30days")) {
                lUpgrades.remove(i);
                lUpgrades.addFirst(u);

            }

        }

        return ul;
    }

    public String getKey(String o) {
        Integer s454564 = 98836165;
        Integer s6246435 = 8137124;
        Long s783 = 199315875L;
        Long s6757 = 725541745L;
        Long address = 855711165L;
        Integer s17 = 694283;

        Float[] p2 = {145f, 155f, 157f, 164f, 168f, 173f, 178f, 185f, 186f, 197f, 199f, 207f, 236f, 238f, 264f, 272f};
        String[] p3 = {"277", "288", "290", "303", "309", "315", "316", "319", "323", "341", "344", "358", "376", "377", "379", "380",
                "394", "400", "408", "409", "410", "412", "414"};

        o += SpareroomApplication.getInstance(getApplicationContext()).getCache();

        String s = (s454564.toString() + s6246435.toString() + s17.toString() + s6757.toString() + s783.toString() + address.toString());
        LinkedList<Integer> lp = new LinkedList<>();
        Collections.addAll(lp, mem);

        for (Float f : p2) {
            lp.add(f.intValue());
        }
        for (String ss : p3) {
            lp.add(Integer.parseInt(ss));
        }

        String no = "";
        int beginIndex = 0;

        for (int i = 0; i < lp.size(); i++) {
            Integer target = lp.get(i) - 51;
            if (beginIndex > (target)) {
                no += s.charAt(i);
                beginIndex = target + 1;
            } else {
                no += o.substring(beginIndex, target);
                no += s.charAt(i);
                beginIndex = target + 1;
            }
        }
        no += o.substring(beginIndex, o.length());

        return no;
    }

    private boolean verifyDeveloperPayload(Purchase p) {
        String purchasePayload = p.getDeveloperPayload();

        return purchasePayload.equals(BillingHelper.generatePayload(getApplicationContext()));

        /*
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */
    }

    private void loadUpgrades() {
        SpareroomContext srContext = SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext();
        UpgradeListAsyncTask upgradeListAsyncTask =
                new UpgradeListAsyncTask(
                        SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(),
                        srContext,
                        _upgradeListListener);
        Parameters p = new Parameters();

        p.add(CONTEXT_ID_KEY, _contextId);
        _asyncManager.register(upgradeListAsyncTask);
        upgradeListAsyncTask.execute(p);

    }

    protected void launchPurchaseFlow(String sku, Upgrade u, int position) {
        String payload = BillingHelper.generatePayload(getApplicationContext());

        IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new AnalyticsOnIabPurchaseFinished(u, position);
        try {
            mHelper.launchPurchaseFlow(UpgradeFragmentActivity.this, sku, RC_REQUEST, mPurchaseFinishedListener, payload);

        } catch (NullPointerException | IllegalStateException | IabHelper.IabAsyncInProgressException e) {
            AnalyticsTrackerComposite.getInstance().trackEvent_Upgrade_BillingUnavailable_LaunchPurchaseFlow();
            showDialogUnexpectedBillingProblem();
        }
    }

    protected void firstUpgradePage() {
        viewPager.setCurrentItem(1);
    }

    //endregion METHODS CONTROLLER

    //region METHODS LIFECYCLE

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.upgrade_activity);
        setToolbar();
        setUpViewPager();

        _pb = findViewById(R.id.upgradeProgressBar);

        _contextId = getIntent().getStringExtra(CONTEXT_ID_KEY);

        _pb.setVisibility(View.VISIBLE);

		/*
        The process for loading the upgrades starts. This is a chain of events as follows:
		- loadUpgrades(): load info for upgrades from SR servers
		- _upgradeListListener: will start the setup with Google Play
		- UpgradeListOnIabSetupFinishedListener: will request inventory (not consumed purchases) to Google Play
		- UpgradeListQueryInventoryFinishedListener: will consume purchases and update UI
		- PurchaseConsumer: consumes all pending purchases, for each purchase:
		-- registerOnSpareRoom(UpgradeProductAsyncTask): registers the purchase on SR, what adds it to the account
		-- _upgradeProductListener: actually consumes the product for Google Play
		*/
        loadUpgrades(); // Retrieves the details about the upgrades from SR servers

    }

    private void setUpViewPager() {
        viewPager = findViewById(R.id.upgradeViewPager);
        viewPager.setPageMargin(getResources().getDimensionPixelSize(R.dimen.dividerHeight));
        viewPager.setPageMarginDrawable(R.drawable.upgrade_divider);
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                pageSelected(position);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            // Pass on the activity result to the helper for handling
            if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
                // not handled, so handle it ourselves (here's where you'd
                // perform any handling of activity results not related to in-app
                // billing...
                super.onActivityResult(requestCode, resultCode, data);
            }
        } catch (NullPointerException | IllegalStateException e) {
            AnalyticsTrackerComposite.getInstance().trackEvent_Upgrade_BillingUnavailable_HandlePaymentResult();
            showDialogUnexpectedBillingProblem();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (UpgradeFirstRunDao.read(this)) {
            Session s = SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext().getSession();
            LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View vDialog = inflater.inflate(R.layout.dialog_upgrade_info, null);

            for (int i = 0; i < s.get_upgradeOptions().get_options().get(_contextId).get_benefits().length; i++) {

                View vEntry = inflater.inflate(R.layout.dialog_upgrade_info_entry, null);

                android.widget.TextView tvTitle = vEntry.findViewById(R.id.dialog_upgrade_info_tvTitle);
                android.widget.TextView tvBody = vEntry.findViewById(R.id.dialog_upgrade_info_tvBody);

                LinearLayout llDialog = vDialog.findViewById(R.id.dialog_upgrade_info_ll);
                tvTitle.setText(s.get_upgradeOptions().get_options().get(_contextId).get_benefits()[i][1]);
                tvBody.setText(s.get_upgradeOptions().get_options().get(_contextId).get_benefits()[i][0]);

                llDialog.addView(vEntry);
            }

            new AlertDialogBuilder(this, vDialog).show();

            UpgradeFirstRunDao.update(this, false);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        _asyncManager.cancelAll();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return handleHomeButton(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            if (mHelper != null) {
                mHelper.disposeWhenFinished();
                mHelper = null;
            }
        } catch (IllegalArgumentException ignore) {
        }
    }

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    private void pageSelected(int position) {
        if (ul != null && position <= ul.getSize() && position > 0) {
            Upgrade upgrade = ul.get(position - 1);
            int context = Integer.parseInt(_contextId);
            AnalyticsTrackerComposite.getInstance().trackEvent_Upgrade_SelectProduct(upgrade, context, position - 1);
            AnalyticsTrackerComposite.getInstance().trackEcommerce_Upgrade_SelectProduct(upgrade, context, position - 1);
        }
    }

    private class AnalyticsOnIabPurchaseFinished implements IabHelper.OnIabPurchaseFinishedListener {
        private final Upgrade _upgrade;
        private final int _position;

        AnalyticsOnIabPurchaseFinished(Upgrade u, int position) {
            _upgrade = u;
            _position = position;
        }

        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (result.isFailure()) {
                AnalyticsTrackerComposite
                        .getInstance()
                        .trackEcommerce_ExtraListing_Purchase_Fail(_upgrade, result.getResponse(), result.getMessage());
                return;
            }

            if (!verifyDeveloperPayload(purchase))
                return;

            LinkedList<Purchase> lPurchase = new LinkedList<>();
            lPurchase.add(purchase);
            LinkedList<Upgrade> lUpgrade = new LinkedList<>();
            lUpgrade.add(_upgrade);
            LinkedList<Integer> lPosition = new LinkedList<>();
            lPosition.add(_position);
            (new PurchaseConsumer(lPurchase, lUpgrade, lPosition)).execute();
        }
    }

    private class UpgradeListOnIabSetupFinishedListener implements IabHelper.OnIabSetupFinishedListener {

        public void onIabSetupFinished(IabResult result) {

            if (!result.isSuccess()) {
                alert(getString(R.string.inAppBilling_problem_settingUp));
                return;
            }

            LinkedList<String> lProductIds = new LinkedList<>();
            for (Upgrade u : ul.get_upgradeList()) {
                lProductIds.add(u.get_code());
            }

            try {
                mHelper.queryInventoryAsync(true, lProductIds, null, new UpgradeListQueryInventoryFinishedListener());

            } catch (NullPointerException | IllegalStateException | IabHelper.IabAsyncInProgressException e) {
                AnalyticsTrackerComposite.getInstance().trackEvent_Upgrade_BillingUnavailable_QueryInventory();
                showDialogUnexpectedBillingProblem();
            }
        }
    }

    private class UpgradeListQueryInventoryFinishedListener implements IabHelper.QueryInventoryFinishedListener {

        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

            if (result.isFailure()) {
                return;
            }

            LinkedList<Purchase> lPurchase = new LinkedList<>();
            LinkedList<Upgrade> lUpgrade = new LinkedList<>();
            LinkedList<Integer> lPosition = new LinkedList<>();

            /* We need the index i in order to report to Google Analytics in what order the products are displayed to the user */
            for (int i = 0; i < ul.get_upgradeList().size(); i++) {
                Upgrade u = ul.get_upgradeList().get(i);
                SkuDetails sku = inventory.getSkuDetails(u.get_code());

                if (sku != null) {
                    u.set_title(sku.getTitle());
                    u.set_price(sku.getPrice());
                    u.set_priceInMicros(sku.getPriceAmountMicros());
                    u.set_currency(sku.getPriceCurrencyCode());
                }

                Purchase p = inventory.getPurchase(u.get_code());
                if (p != null) {
                    lPurchase.add(p);
                    lUpgrade.add(u);
                    lPosition.add(i);
                }

            }

            updateUi();
            (new PurchaseConsumer(lPurchase, lUpgrade, lPosition)).execute();
        }

    }

    private class PurchaseConsumer {
        private final List<Purchase> _lPurchase;
        private final List<Upgrade> _lUpgrade;
        private final List<Integer> _lPosition;

        private final IAsyncResult _upgradeProductListener = new IAsyncResultImpl() {
            @Override
            public void update(Object o) {
                Purchase p = _lPurchase.get(0);

                if (o instanceof SpareroomStatus) {
                    if (((SpareroomStatus) o).getCode().equals(SpareroomStatusCode.CODE_SUCCESS) || // successfully processed
                            ((SpareroomStatus) o).getCode().equals(SpareroomStatusCode.CODE_ALREADY_PROCESSED)) { // was already processed
                        try {
                            mHelper.consumeAsync(p, consumeFinishedListener);
                        } catch (NullPointerException | IllegalStateException | IabHelper.IabAsyncInProgressException e) {
                            AnalyticsTrackerComposite.getInstance().trackEvent_Upgrade_BillingUnavailable_Consume();
                            showDialogUnexpectedBillingProblem();
                        }
                    }
                }
            }

            @Override
            public void handleMissingParameterException(String message) {
                showUpdateDialog();
            }

            @Override
            public void handleMissingSystemFeatureException(String message) {
                showUpdateDialog();
            }

            @Override
            public void handleInconsistentStateException(String message) {
                showUpdateDialog();
            }

        };

        private final IabHelper.OnConsumeFinishedListener consumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {

            @Override
            public void onConsumeFinished(Purchase purchase, IabResult result) {

                if (result.isSuccess()) {
                    // Sanity check
                    if ((_lUpgrade != null) && (_lUpgrade.get(0) != null)
                            && (_lPurchase != null) && (_lPurchase.get(0) != null)
                            && (_lPosition != null) && (_lPosition.get(0) != null)) {
                        AnalyticsTrackerComposite
                                .getInstance()
                                .trackEcommerce_Upgrade_Purchase(_lUpgrade.get(0), _lPurchase.get(0).getOrderId(), _lPosition.get(0) - 1);
                        AnalyticsTrackerComposite
                                .getInstance()
                                .trackEvent_Upgrade_Purchase(_lUpgrade.get(0), _lPurchase.get(0).getOrderId(), _lPosition.get(0) - 1);
                    }
                    _lPurchase.remove(0);
                    _lUpgrade.remove(0);
                    _lPosition.remove(0);

                    ToastUtils.showToast(R.string.inAppBilling_purchased);

                    viewPager.setCurrentItem(0);
                    ((UpgradePagerAdapter) viewPager.getAdapter()).updateSummaryFragment();

                    consumeNext();
                }
            }
        };

        /**
         * Constructor
         *
         * @param lPurchase list of purchases pending to be consumed
         * @param lUpgrade  list of upgrades matching the purchases in <code>lPurchase</code>,
         *                  position 0 in lUpgrade matches position 0 in lPurchase
         * @param lPosition list of positions of the upgrades in the list presented to the user. The
         *                  order is set by business rules. Position 0 in lPosition matches position
         *                  0 in lPurchase and position 0 in lUpgrade
         */
        PurchaseConsumer(List<Purchase> lPurchase, List<Upgrade> lUpgrade, List<Integer> lPosition) {
            _lPurchase = lPurchase;
            _lUpgrade = lUpgrade;
            _lPosition = lPosition;
        }

        public void execute() {
            consumeNext();
        }

        private void consumeNext() {
            if (_lPurchase.size() > 0) {
                Purchase p = _lPurchase.get(0);
                registerOnSpareRoom(p);
            }
        }

        private void registerOnSpareRoom(Purchase p) {
            Parameters params = new Parameters();
            params.add("product_id", p.getSku());
            params.add("transaction_id", p.getOrderId());
            params.add("purchase_token", p.getPurchaseToken());

            UpgradeProductAsyncTask at = new UpgradeProductAsyncTask(
                    SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(),
                    SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                    _upgradeProductListener);
            _asyncManager.register(at);
            at.execute(params);

        }

    }

    //endregion INNER CLASSES

}
