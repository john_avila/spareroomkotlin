package com.spareroom.ui.screen.legacy;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.model.business.SearchType;

public class PlaceAdUploadCompleteFragment extends PlaceAdFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_complete, container, false);
        setTitle(getString(R.string.place_ad_complete_title));
        Button _bViewAdvert = rootView.findViewById(R.id.place_ad_complete_bViewAdvert);
        Button _bPlaceAnother = rootView.findViewById(R.id.place_ad_complete_bPlaceAnother);

        if (!(getActivity() instanceof EditAdActivity)) {
            _bPlaceAnother.setOnClickListener(v -> finish());
        } else {
            _bPlaceAnother.setVisibility(View.GONE);
        }

        if (getDraftAd() == null || getDraftAd().getId() == null)
            _bViewAdvert.setVisibility(View.GONE);
        else {
            _bViewAdvert.setOnClickListener(v -> {
                Intent activityAd = new Intent();
                activityAd.setClass(getActivity(), AdActivity.class);
                activityAd.putExtra("id", getDraftAd().getId());
                activityAd.putExtra("searchType", (getDraftAd() instanceof DraftAdOffered) ? SearchType.OFFERED : SearchType.WANTED);
                activityAd.putExtra("tryWanted", true);
                startActivity(activityAd);
            });
        }

        return rootView;
    }
}
