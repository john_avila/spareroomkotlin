package com.spareroom.ui.screen.legacy;

import android.app.Dialog;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.ui.screen.AlertDialogBuilder;
import com.spareroom.ui.widget.util.exclusivitymanager.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import androidx.core.content.ContextCompat;

public class PlaceAdOfferedAvailabilityPeriodBills extends PlaceAdFragment {

    //region FIELDS CONTROLLER

    private DraftAdOffered.BillsIncluded _billsIncluded;
    private boolean _isMonFriLet = false;
    private Calendar _date;
    private DraftAdOffered _draftAd;

    //endregion FIELDS CONTROLLER

    //region METHODS LIFECYCLE

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setTitle(getString(R.string.place_ad_offered_availability));

        View rootView = inflater.inflate(R.layout.place_ad_offered_availability_period_bills, container, false);
        View vDate = rootView.findViewById(R.id.place_ad_offered_availability_rlDate);
        Button bConfirm = rootView.findViewById(R.id.place_ad_bConfirm);
        Button bBillsNo = rootView.findViewById(R.id.place_ad_availability_period_bills_bNo);
        Button bBillsSome = rootView.findViewById(R.id.place_ad_availability_period_bills_bSome);
        Button bBillsYes = rootView.findViewById(R.id.place_ad_availability_period_bills_bYes);
        Button bMonFriYes = rootView.findViewById(R.id.place_ad_offered_room_bMonFriYes);
        Button bMonFriNo = rootView.findViewById(R.id.place_ad_offered_room_bMonFriNo);
        final TextView _tvDate = rootView.findViewById(R.id.place_ad_offered_availability_etDate);

        _draftAd = (DraftAdOffered) getDraftAd();

        // In case there is an activity state change and we lost the draft
        if (_draftAd == null) {
            getActivity().finish();
            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();
            return null;
        }

        ExclusivityManager emBillsIncluded = new ExclusivityManager();
        ExclusivityManager emMonFri = new ExclusivityManager();
        _date = _draftAd.get_availableFrom();
        SimpleDateFormat sdf = new SimpleDateFormat(AppVersion.flavor().getDateRepresentation(), AppVersion.flavor().getLocale());

        // Showing the data

        if (_date == null) {
            _date = new GregorianCalendar();
        }
        _tvDate.setText(sdf.format(_date.getTime()));

        if (_draftAd.is_isMonFriLet())
            _isMonFriLet = _draftAd.is_isMonFriLet();

        if (_draftAd.get_areBillsIncluded() != null)
            _billsIncluded = _draftAd.get_areBillsIncluded();

        /* Listeners and highlighting */

        // Date listener

        vDate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                View vDialog = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_place_ad_offered_availability_date, null);
                DatePicker dpDate = vDialog.findViewById(R.id.place_ad_offered_availability_date_dpDate);
                Button bOk = vDialog.findViewById(R.id.dialog_bPositive);
                Button bCancel = vDialog.findViewById(R.id.dialog_bNegative);

                Calendar minDate = new GregorianCalendar();
                Dialog d;

                // Sets the DatePicker to the first second of the day to avoid an IllegalArgumentException
                minDate.set(Calendar.HOUR_OF_DAY, minDate.getMinimum(Calendar.HOUR_OF_DAY));
                minDate.set(Calendar.MINUTE, minDate.getMinimum(Calendar.MINUTE));
                minDate.set(Calendar.SECOND, minDate.getMinimum(Calendar.SECOND));
                minDate.set(Calendar.MILLISECOND, minDate.getMinimum(Calendar.MILLISECOND));
                dpDate.setMinDate(minDate.getTimeInMillis());
                d = new AlertDialogBuilder(getActivity(), vDialog).create();

                bOk.setOnClickListener(new OkOnClickListener(d, dpDate));

                bCancel.setOnClickListener(new DialogUtil.CancelOnClickListener(d));
                d.show();
            }

            class OkOnClickListener implements OnClickListener {
                private Dialog _dialog;
                private DatePicker _dpDate;

                OkOnClickListener(Dialog d, DatePicker dpDate) {
                    _dialog = d;
                    _dpDate = dpDate;
                }

                @Override
                public void onClick(View v) {
                    int day = _dpDate.getDayOfMonth();
                    int month = _dpDate.getMonth();
                    int year = _dpDate.getYear();

                    Calendar calendar = new GregorianCalendar();
                    SimpleDateFormat sdf =
                            new SimpleDateFormat(
                                    AppVersion.flavor().getDateRepresentation(),
                                    AppVersion.flavor().getLocale());

                    calendar.set(Calendar.DAY_OF_MONTH, day);
                    calendar.set(Calendar.MONTH, month);
                    calendar.set(Calendar.YEAR, year);

                    _date = calendar;

                    _tvDate.setText(sdf.format(_date.getTime()));

                    _draftAd.set_availableFrom(calendar);
                    _dialog.dismiss();
                }
            }

        });

        // Mon-fri listeners

        bMonFriYes.setOnClickListener(new ExclusiveButtonOnClickListener(emMonFri) {
            @Override
            public void onClick(View v) {
                super.onClick(v);
                _isMonFriLet = true;
            }
        });
        emMonFri.add(new ButtonViewActivationCommand(bMonFriYes));

        bMonFriNo.setOnClickListener(new ExclusiveButtonOnClickListener(emMonFri) {
            @Override
            public void onClick(View v) {
                super.onClick(v);
                _isMonFriLet = false;
            }
        });
        emMonFri.add(new ButtonViewActivationCommand(bMonFriNo));

        try {
            if (_isMonFriLet)
                emMonFri.activate(bMonFriYes);
            else
                emMonFri.activate(bMonFriNo);
        } catch (ExclusiveViewNotFoundException ignored) {
        }

        // Bills listeners

        bBillsNo.setOnClickListener(new ExclusiveButtonOnClickListener(emBillsIncluded) {
            @Override
            public void onClick(View v) {
                super.onClick(v);
                _billsIncluded = DraftAdOffered.BillsIncluded.NO;
            }
        });
        emBillsIncluded.add(new ButtonViewActivationCommand(bBillsNo));

        bBillsSome.setOnClickListener(new ExclusiveButtonOnClickListener(emBillsIncluded) {
            @Override
            public void onClick(View v) {
                super.onClick(v);
                _billsIncluded = DraftAdOffered.BillsIncluded.SOME;
            }
        });
        emBillsIncluded.add(new ButtonViewActivationCommand(bBillsSome));

        bBillsYes.setOnClickListener(new ExclusiveButtonOnClickListener(emBillsIncluded) {
            @Override
            public void onClick(View v) {
                super.onClick(v);
                _billsIncluded = DraftAdOffered.BillsIncluded.YES;
            }
        });
        emBillsIncluded.add(new ButtonViewActivationCommand(bBillsYes));

        if (_billsIncluded != null)
            try {
                if (_billsIncluded.equals(DraftAdOffered.BillsIncluded.NO))
                    emBillsIncluded.activate(bBillsNo);
                else if (_billsIncluded.equals(DraftAdOffered.BillsIncluded.YES))
                    emBillsIncluded.activate(bBillsYes);
                else if (_billsIncluded.equals(DraftAdOffered.BillsIncluded.SOME))
                    emBillsIncluded.activate(bBillsSome);

            } catch (ExclusiveViewNotFoundException ignored) {
            }

        // Confirm listener

        bConfirm.setOnClickListener(v -> {
            _draftAd.set_areBillsIncluded(_billsIncluded);
            _draftAd.set_isMonFriLet(_isMonFriLet);
            _draftAd.set_availableFrom(_date);

            finish();
        });

        return rootView;
    } //end onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    private class ButtonViewActivationCommand extends ViewActivationCommand {

        ButtonViewActivationCommand(View v) {
            super(v);
        }

        @Override
        public void activate() {
            super.activate();
            _view.setBackgroundResource(R.drawable.button_place_ad_bkg_selected);
            if (_view instanceof TextView)
                ((TextView) _view).setTextColor(ContextCompat.getColor(_view.getContext(), R.color.cyan));
        }

        @Override
        public void deactivate() {
            super.deactivate();
            _view.setBackgroundResource(R.drawable.button_place_ad_bkg);
            if (_view instanceof TextView)
                ((TextView) _view).setTextColor(ContextCompat.getColor(_view.getContext(), R.color.grey_2));
        }

    } //end class ButtonViewActivationCommand

    private class ExclusiveButtonOnClickListener implements OnClickListener {

        private final ExclusivityManager _em;

        ExclusiveButtonOnClickListener(ExclusivityManager em) {
            _em = em;
        }

        @Override
        public void onClick(View v) {
            try {
                _em.activate(v);
            } catch (ExclusiveViewNotFoundException ignored) {
            }
        }

    } //end class ExclusiveButtonOnClickListener

    //endregion INNER CLASSES

}
