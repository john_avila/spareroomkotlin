package com.spareroom.ui.adapter

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.spareroom.R
import com.spareroom.ui.screen.*

class ConversationPagerAdapter(fragmentManager: FragmentManager, private val context: Context) : FragmentStatePagerAdapter(fragmentManager) {

    private val titles = arrayOf(R.string.conversation_message_tab, R.string.conversation_adverts_tab)

    override fun getItem(position: Int): Fragment {
        return if (position == 0) ConversationFragment.getInstance() else OtherUserAdvertsFragment.getInstance()
    }

    override fun getCount(): Int {
        return titles.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return context.getString(titles[position])
    }
}
