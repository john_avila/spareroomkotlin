package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.lib.core.Pair;
import com.spareroom.lib.util.NumberUtil;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.ui.widget.RangeSeekBar;

import java.util.Locale;

public class PlaceAdOfferedHappyAnyAgeFragment extends PlaceAdFragment {

    //region FIELD UI

    private TextView _tvMin;
    private TextView _tvMax;
    private RangeSeekBar rsbAge;
    private Button bConfirm;

    //endregion FIELD UI

    //region METHODS LIFECYCLE

    @Override
    @SuppressWarnings("unchecked")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        final int minimumAge = NumberUtil.MIN_AGE, maximumAge = 65;
        View rootView = inflater.inflate(R.layout.place_ad_offered_happy_any_age, container, false);
        rsbAge = rootView.findViewById(R.id.place_ad_offered_happy_any_age_rsbAge);
        bConfirm = rootView.findViewById(R.id.place_ad_bConfirm);
        Pair wantedAge;

        setTitle(getString(R.string.place_ad_offered_happy_any_age_title));

        _tvMin = rootView.findViewById(R.id.place_ad_offered_happy_any_age_tvMin);
        _tvMax = rootView.findViewById(R.id.place_ad_offered_happy_any_age_tvMax);

        final DraftAdOffered draft = ((DraftAdOffered) getDraftAd());

        // In case there is an activity state change and we lost the draft
        if (draft == null) {
            getActivity().finish();
            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();
            return null;
        }

        wantedAge = draft.getRangeDesiredAge();

        // Sets the values
        if (draft.getRangeDesiredAge() != null) {
            _tvMin.setText(wantedAge.getKey().toString());
            _tvMax.setText(wantedAge.getValue().toString());
            rsbAge.setSelectedMinValue((Integer) wantedAge.getKey());
            rsbAge.setSelectedMaxValue((Integer) wantedAge.getValue());
        } else {
            rsbAge.setSelectedMinValue(minimumAge);
            rsbAge.setSelectedMaxValue(maximumAge);
        }

        /* Listeners */

        rsbAge.setNotifyWhileDragging(true); // Allow to notify the listener in the meantime the thumb is being moved
        rsbAge.setOnRangeSeekBarChangeListener((bar, minValue, maxValue) -> {
            _tvMin.setText(String.format(Locale.getDefault(), "%d", minValue));
            _tvMax.setText(String.format(Locale.getDefault(), "%d", maxValue));
        });

        bConfirm.setOnClickListener(v -> {
            draft.setRangeDesiredAge(
                    new Pair<>(Integer.valueOf(_tvMin.getText().toString()), Integer.valueOf(_tvMax.getText().toString())));
            finish();
        });

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rsbAge.setOnRangeSeekBarChangeListener(null);
        bConfirm.setOnClickListener(null);
    }

    //endregion METHODS LIFECYCLE
}
