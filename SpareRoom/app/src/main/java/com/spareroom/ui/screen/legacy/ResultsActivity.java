package com.spareroom.ui.screen.legacy;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.widget.ProgressBar;

import com.google.android.material.snackbar.Snackbar;
import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.business.apiv2.ApiParams;
import com.spareroom.integration.business.apiv2.util.SearchAdvertListParametersAdapter;
import com.spareroom.integration.business.apiv2.util.SearchAdvertListPropertiesAdapter;
import com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum;
import com.spareroom.integration.sharedpreferences.EventsDao;
import com.spareroom.model.business.*;
import com.spareroom.model.business.SearchAdvertListResponse.ResponseTypes;
import com.spareroom.ui.IScrollableContainer;
import com.spareroom.ui.filters.screen.FilterActivity;
import com.spareroom.ui.screen.*;
import com.spareroom.ui.util.*;
import com.spareroom.viewmodel.ResultsViewModel;

import javax.inject.Inject;

import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import static com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum.*;
import static com.spareroom.model.business.SearchAdvertListProperties.SortedBy.*;
import static com.spareroom.ui.screen.legacy.SavedSearchesAddActivity.SEARCH_TYPE_KEY;

public class ResultsActivity extends InjectableActivity implements IScrollableContainer {
    private static final String SEARCH = "search";
    private static final String TYPE = "type";
    public static final String SEARCH_PROPERTIES_INTENT_KEY = "search_properties_intent_key";
    public static final String BREADCRUMBS = "breadcrumbs";

    private static final String SEARCH_PROPERTIES_INSTANCE_STATE_KEY = "search_properties_instance_state_key";
    private final static String DEFAULT_FIRST_PAGE = "1";
    private final static String DEFAULT_MAX_PER_PAGE = "10";

    private final static int REFINE_REQUEST_CODE = 0;
    private final static int SAVED_SEARCHES_ADD_CODE = 2468;

    private SuggestionList _suggestions = new SuggestionList();
    private ProgressBar _progressBar;

    private Parameters _currentSearch;
    private SearchType _searchType;
    private ResultsFragment resultsFragment;

    private SpareroomContext _spareroomContext;

    private int _firstMaxResults;

    private boolean _isLoading = false;

    private Snackbar snackbar;
    private ResultsViewModel viewModel;
    private boolean nextPageSearchAllowed;
    private boolean searchingFromOnCreate = true;
    private boolean clearAdapter;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    EventsDao eventsDao;

    @Inject
    SearchAdvertListParametersAdapter parametersAdapter;

    public static void start(Activity activity, String search, SearchType searchType) {
        Intent resultsActivityIntent = new Intent(activity, ResultsActivity.class);
        resultsActivityIntent.putExtra(SEARCH, search);
        resultsActivityIntent.putExtra(TYPE, searchType);
        activity.startActivity(resultsActivityIntent);
    }

    public void update(Object o) {
        SearchAdvertListResponse response = (SearchAdvertListResponse) o;
        nextPageSearchAllowed = true;
        _isLoading = false;
        _progressBar.setVisibility(View.GONE);

        if (!response.isSuccessful()) {
            snackbar = SnackBarUtils.show(
                    (View) _progressBar.getParent(),
                    response.getErrorMessage(this, getString(R.string.errorGeneric_title)),
                    getString(R.string.retry),
                    v -> retry(), true);

        } else {

            if (response.getResponseType() == ResponseTypes.SUGGESTION_LIST) {
                showEmptyText(true);
                _suggestions = (SuggestionList) response.getResponse();

                if (_suggestions.get_suggestion().size() > 0) {
                    Bundle arguments = new Bundle();
                    String[] suggestedLocations = new String[_suggestions.get_suggestion().size()];
                    for (int i = 0; i < _suggestions.get_suggestion().size(); i++) {
                        suggestedLocations[i] = _suggestions.get_suggestion().get(i).get_location();
                    }
                    arguments.putStringArray("suggestions", suggestedLocations);

                    SuggestionDialogFragment sdf = new SuggestionDialogFragment();
                    sdf.setArguments(arguments);
                    sdf.show(getSupportFragmentManager(), "Suggestions");
                }

                saveCurrentSearch();

            } else {
                AdBoard board = (AdBoard) response.getResponse();

                if (board.get_mapReference() != null) // It happens when it is a wanted board
                    saveMapReferenceParams(board);

                setSubtitle(
                        String.format(
                                AppVersion.flavor().getLocale(),
                                "%s %s",
                                resultsCount(board.get_total()),
                                getResources().getQuantityString(R.plurals.results, board.get_total())));

                searchNextPage();

                if (searchingFromOnCreate) {
                    resultsFragment.empty();
                    searchingFromOnCreate = false;
                }

                resultsFragment.update(board, clearAdapter);
                clearAdapter = false;

                if (board.get_total() == 0)
                    launchSuggestionSearch();

            }

        }

    }

    private String resultsCount(int results) {
        int thousands = results / 1000;
        if (thousands >= 1)
            return thousands + "k";

        return results + "";
    }

    private void saveMapReferenceParams(AdBoard board) {
        _currentSearch.add(LATITUDE, String.valueOf(board.get_mapReference().get_latitude()));
        _currentSearch.add(LATITUDE_DELTA, String.valueOf(board.get_mapReference().get_latitudeDelta()));
        _currentSearch.add(LONGITUDE, String.valueOf(board.get_mapReference().get_longitude()));
        _currentSearch.add(LONGITUDE_DELTA, String.valueOf(board.get_mapReference().get_longitudeDelta()));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ResultsViewModel.class);
        _spareroomContext = SpareroomApplication.getInstance().getSpareroomContext();
        _searchType = (SearchType) getIntent().getSerializableExtra(TYPE);

        if (savedInstanceState == null) {
            eventsDao.logSearchEvent();

            _currentSearch = _spareroomContext.getCurrentSearch();
            if (!getIntent().getBooleanExtra("saved", false))
                searchNew(getIntent().getStringExtra(SEARCH));

            replaceFragment();

        } else {
            resultsFragment = (ResultsFragment) getSupportFragmentManager().findFragmentByTag(ResultsFragment.TAG);
            SearchAdvertListProperties properties =
                    (SearchAdvertListProperties) savedInstanceState.getSerializable(SEARCH_PROPERTIES_INSTANCE_STATE_KEY);
            assert properties != null;
            Parameters search = new SearchAdvertListPropertiesAdapter().adapt(properties, true);
            _spareroomContext.updateCurrentSearch(search);
            _currentSearch = _spareroomContext.getCurrentSearch();

        }

        float screenHeight = _spareroomContext.get_screenHeight();
        float maxResults = screenHeight / 80;
        int firstPages = (int) Math.floor((maxResults / 10) + 1);
        _firstMaxResults = firstPages * 10;
        String milesFromMax = getIntent().getStringExtra("miles_from_max");

        setToolbar(_currentSearch.get(SearchParamEnum.WHERE));

        _progressBar = findViewById(R.id.activityResults_pb);

        _currentSearch.add(HIDE_UNSUITABLE, ApiParams.YES_INT);

        initSearch();

        if (milesFromMax == null || SearchType.WANTED == _searchType) {
            saveParameters();
            launchAdSearch();
        } else {
            BroadenSearch broadenSearch = new BroadenSearch();
            Parameters parameters = new Parameters();

            parameters.add("miles_from_max", milesFromMax);
            broadenSearch.set_paramsToAdd(parameters);
            launchBroadenSearch(saveParameters(), broadenSearch);
        }

        observeResults();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(SEARCH_PROPERTIES_INSTANCE_STATE_KEY, parametersAdapter.adapt(_currentSearch, _searchType));
        super.onSaveInstanceState(outState);
    }

    private void initSearch() {
        saveCurrentSearch();
        clearAdapter = true;
    }

    private void replaceFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        resultsFragment = ResultsFragment.newInstance(_searchType);
        transaction.replace(R.id.activityResults_flResults, resultsFragment, ResultsFragment.TAG);
        transaction.commit();
    }

    private void observeResults() {
        viewModel.getSearchAdvertList().observe(this, this::update);
    }

    private void launchAdSearch() {
        _isLoading = true;
        _progressBar.setVisibility(View.VISIBLE);
        nextPageSearchAllowed = false;
        viewModel.getSearchAdvertList()
                .retrieveAdvertList(parametersAdapter.adapt(_currentSearch, _searchType), _searchType, false);

    }

    private void launchBroadenSearch(Parameters parameters, BroadenSearch broadenSearch) {
        _progressBar.setVisibility(View.VISIBLE);

        if (broadenSearch.get_paramsToAdd() != null)
            parameters.add(broadenSearch.get_paramsToAdd());
        if (broadenSearch.get_paramsToLose() != null)
            parameters.remove(broadenSearch.get_paramsToLose());

        saveParameters();

        nextPageSearchAllowed = false;
        viewModel.getSearchAdvertList()
                .retrieveAdvertList(parametersAdapter.adapt(parameters, _searchType), _searchType, false);
    }

    private void launchSuggestionSearch() {
        if (_currentSearch == null || StringUtils.isNullOrEmpty(_currentSearch.get(SearchParamEnum.WHERE))) {
            ToastUtils.showToast(R.string.amend_search);
            finish();
        }

        _progressBar.setVisibility(View.VISIBLE);
        SearchAdvertListResponse<SuggestionList> response = new SearchAdvertListResponse<>();
        response.setResponseType(ResponseTypes.SUGGESTION_LIST);
        response.setResponse(StringUtils.equals(_currentSearch.get(SearchParamEnum.WHERE), _suggestions.get_location()) ? _suggestions : new SuggestionList());
        update(response);
    }

    public void launchSuggestedSearch(Parameters p) {
        searchNew(p.get(SearchParamEnum.WHERE));
        initSearch();
        showEmptyText(false);
        launchAdSearch();
    }

    private void launchRefineActivity() {
        saveCurrentSearch();
        Intent filterIntent = new Intent(this, FilterActivity.class);
        filterIntent.putExtra(SEARCH_PROPERTIES_INTENT_KEY, parametersAdapter.adapt(_currentSearch, _searchType));
        startActivityForResult(filterIntent, REFINE_REQUEST_CODE);
    }

    private Parameters saveParameters() {
        _spareroomContext.updateCurrentSearch(_currentSearch);
        return _currentSearch;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REFINE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                SearchAdvertListProperties properties = (SearchAdvertListProperties) data.getSerializableExtra(SEARCH_PROPERTIES_INTENT_KEY);
                _currentSearch = new SearchAdvertListPropertiesAdapter().adapt(properties, false);
                saveParameters();
                clearAdapter = true;
                showEmptyText(false);
                launchAdSearch();

                if (snackbar != null)
                    snackbar.dismiss();
            }

        } else if (requestCode == 1) {
            if (resultCode == AbstractAd.CONTACTABLE_CONTACTED) {
                if (data != null) {
                    String adId = data.getStringExtra("id");
                    resultsFragment.markStatus(adId, AbstractAd.CONTACTABLE_CONTACTED);
                }
            } else if (resultCode == AbstractAd.SAVED) {
                if (data != null) {
                    String adId = data.getStringExtra("id");
                    resultsFragment.markStatus(adId, AbstractAd.SAVED);
                }
            } else if (resultCode == AbstractAd.NOT_SAVED) {
                if (data != null) {
                    String adId = data.getStringExtra("id");
                    resultsFragment.markStatus(adId, AbstractAd.NOT_SAVED);
                }
            }
        } else if (requestCode == SAVED_SEARCHES_ADD_CODE && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        _progressBar.setVisibility(View.GONE);

        if (isFinishing())
            _spareroomContext.cleanCurrentSearch();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_results, menu);
        menu.findItem(R.id.activityResults_menu_save).setVisible(_searchType == SearchType.OFFERED);
        menu.findItem(R.id.activityResults_menu_map).setVisible(_searchType == SearchType.OFFERED);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.activityResults_menu_map:
                if (!_isLoading) {
                    AnalyticsTrackerComposite.getInstance().trackEvent_SearchRoomsMenu_Map();
                    Intent intent = new Intent();
                    intent.putExtra("location", _currentSearch.get(SearchParamEnum.WHERE));
                    intent.setClass(ResultsActivity.this, ResultsMapActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.activityResults_menu_refine:
                if (!_isLoading) {
                    if (_searchType == SearchType.OFFERED) {
                        AnalyticsTrackerComposite.getInstance().trackEvent_SearchRoomsMenu_Refine();
                    } else {
                        AnalyticsTrackerComposite.getInstance().trackEvent_SearchFlatmatesMenu_Refine();
                    }

                    launchRefineActivity();
                }
                break;
            case R.id.activityResults_menu_save:
                if (!_isLoading) {
                    AnalyticsTrackerComposite.getInstance().trackEvent_SearchRoomsMenu_Map();
                    if (_spareroomContext.getSession() != null) {
                        Intent savedSearchesActivity = new Intent();
                        savedSearchesActivity.putExtra(SEARCH_TYPE_KEY, _searchType);
                        savedSearchesActivity.putExtra(SEARCH_PROPERTIES_INTENT_KEY, parametersAdapter.adapt(_currentSearch, _searchType));
                        savedSearchesActivity.setClass(ResultsActivity.this, SavedSearchesAddActivity.class);
                        startActivityForResult(savedSearchesActivity, SAVED_SEARCHES_ADD_CODE);
                    } else {
                        Intent activityLogin = new Intent();
                        activityLogin.setClass(this, NewExistingActivity.class);
                        activityLogin.putExtra(NewExistingFragment.EXTRA_ACTION_CODE, NewExistingFragment.EXTRA_ACTION_CODE_SAVED_SEARCHES);
                        startActivity(activityLogin);
                    }
                }
                break;
            case R.id.activityResults_menu_sort:
                if (!_isLoading) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Sort by");
                    if (_searchType == SearchType.OFFERED) {
                        AnalyticsTrackerComposite.getInstance().trackEvent_SearchRoomsMenu_SortOption();
                        CharSequence[] items = {
                                "Default",
                                "Last updated",// SORT_BY_LAST_UPDATED,
                                "Newest",// SORT_BY_DAYS_SINCE_PLACED,
                                "Most expensive",// SORT_BY_PRICE_HIGH_TO_LOW,
                                "Least expensive",// SORT_BY_PRICE_LOW_TO_HIGH
                        };

                        builder.setItems(items, (dialog, which) -> {
                            switch (which) {
                                case 0:
                                    AnalyticsTrackerComposite.getInstance().trackEvent_SearchRooms_SortByDefault();
                                    _currentSearch.remove(SearchParamEnum.SORTING);
                                    break;
                                case 1:
                                    AnalyticsTrackerComposite.getInstance().trackEvent_SearchRooms_SortByLastUpdated();
                                    _currentSearch.add(SearchParamEnum.SORTING, LAST_UPDATED.getValue());
                                    break;
                                case 2:
                                    AnalyticsTrackerComposite.getInstance().trackEvent_SearchRooms_SortByNewest();
                                    _currentSearch.add(SearchParamEnum.SORTING, DAYS_SINCE_PLACED.getValue());
                                    break;
                                case 3:
                                    AnalyticsTrackerComposite.getInstance().trackEvent_SearchRooms_SortByMostExpensive();
                                    _currentSearch.add(SearchParamEnum.SORTING, PRICE_HIGH_TO_LOW.getValue());
                                    break;
                                case 4:
                                    AnalyticsTrackerComposite.getInstance().trackEvent_SearchRooms_SortByLeastExpensive();
                                    _currentSearch.add(SearchParamEnum.SORTING, PRICE_LOW_TO_HIGH.getValue());
                                    break;
                            }
                            initSearch();
                            launchAdSearch();

                        });

                    } else {
                        AnalyticsTrackerComposite.getInstance().trackEvent_SearchFlatmatesMenu_SortOption();
                        builder.setTitle("Sort by");
                        CharSequence[] items = {
                                "Default",
                                "Last updated",// SORT_BY_LAST_UPDATED,
                                "Newest"// SORT_BY_DAYS_SINCE_PLACED,
                        };

                        builder.setItems(items, (dialog, which) -> {
                            switch (which) {
                                case 0:
                                    AnalyticsTrackerComposite.getInstance().trackEvent_SearchFlatmates_SortByDefault();
                                    _currentSearch.remove(SearchParamEnum.SORTING);
                                    break;
                                case 1:
                                    AnalyticsTrackerComposite.getInstance().trackEvent_SearchFlatmates_SortByLastUpdated();
                                    _currentSearch.add(SearchParamEnum.SORTING, LAST_UPDATED.getValue());
                                    break;
                                case 2:
                                    AnalyticsTrackerComposite.getInstance().trackEvent_SearchFlatmates_SortByNewest();
                                    _currentSearch.add(SearchParamEnum.SORTING, DAYS_SINCE_PLACED.getValue());
                                    break;
                            }
                            initSearch();
                            launchAdSearch();

                        });
                    }
                    builder.create().show();
                }
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void newAdSearch() {
        resultsFragment.empty();
    }

    public void onMoreResultsRequest() {
        if (nextPageSearchAllowed) {
            saveParameters();
            launchAdSearch();
        }
    }

    public void retry() {
        _progressBar.setVisibility(View.GONE);
        showEmptyText(false);
        launchAdSearch();
    }

    public void broadenSearch(BroadenSearch bs) {
        saveCurrentSearch();
        newAdSearch();
        showEmptyText(false);
        launchBroadenSearch(_currentSearch, bs);
    }

    private void saveCurrentSearch() {
        _currentSearch.add(SearchParamEnum.PAGE, DEFAULT_FIRST_PAGE);
        _currentSearch.add(SearchParamEnum.MAX_PER_PAGE, Integer.toString(_firstMaxResults));
        _currentSearch.add(SearchParamEnum.FEATURED, ApiParams.YES_INT);
        saveParameters();
    }

    private void searchNew(String location) {
        _currentSearch.add(SearchParamEnum.WHERE, location);
        _currentSearch.add(SearchParamEnum.SORTING, DEFAULT.getValue());
        saveParameters();
    }

    private void searchNextPage() {
        int currentPage = Integer.parseInt(_currentSearch.get(SearchParamEnum.PAGE));
        int nextPage = currentPage + 1;
        if (currentPage == 1) {
            nextPage = _firstMaxResults / 10 + 1;
        }
        _currentSearch.add(SearchParamEnum.MAX_PER_PAGE, DEFAULT_MAX_PER_PAGE);
        _currentSearch.add(SearchParamEnum.PAGE, String.valueOf(nextPage));
        saveParameters();
    }

    String breadcrumbs() {
        return String.format("searchType: %s; breadcrumbs: %s", _searchType, getIntent().getStringExtra(BREADCRUMBS));
    }

    public void showEmptyText(boolean show) {
        resultsFragment.setEmptyText(show ? getString(R.string.activityResults_tvEmptyResults) : null);
    }

}
