package com.spareroom.ui;

import android.app.PendingIntent;
import android.content.Context;

import com.spareroom.R;
import com.spareroom.controller.LongTaskManager;
import com.spareroom.controller.UploadVideoTask;
import com.spareroom.integration.sharedpreferences.UploadVideoDAO;
import com.spareroom.lib.core.IObserver;
import com.spareroom.model.exception.ModelOperationInterruptedException;
import com.spareroom.ui.screen.legacy.ManagePhotosActivity;
import com.spareroom.ui.util.NotificationUtils;

public class VideoUploadApplicationObserver implements IObserver {

    private Context _applicationContext;

    private int _maxPhotos;
    private String _flatshareId;
    private String _flatshareType;

    public VideoUploadApplicationObserver(Context applicationContext, int maxPhotos,
                                          String flatshareId, String flatshareType) {
        _applicationContext = applicationContext;
        _maxPhotos = maxPhotos;
        _flatshareId = flatshareId;
        _flatshareType = flatshareType;
    }

    @Override
    public void notifyObserver(Object o) {

        LongTaskManager.getInstance().removeLongTask(UploadVideoTask.ID);

        if (o == null) { // Video successful upload notifies this observer with o == null

            UploadVideoDAO.delete(_applicationContext, _flatshareId);
            UploadVideoDAO.removeVideoPath(_applicationContext, _flatshareId);

            NotificationUtils.showVideoUploadNotification(_applicationContext,
                    _applicationContext.getString(R.string.notification_video_uploaded_message), notificationIntent());
        } else {
            notifyObserverContent(o);
        }
    }

    private void notifyObserverContent(Object o) {
        // In the case of LongTaskInterruptedException was the user who cancelled the process, we
        //          don't any action but the UI ones.
        if (o instanceof Exception && !(o instanceof ModelOperationInterruptedException)) {
            // Failed video upload
            UploadVideoDAO.create(_applicationContext, _flatshareId);

            NotificationUtils.showVideoUploadNotification(_applicationContext,
                    _applicationContext.getString(R.string.notification_video_failed_message), notificationIntent());
        } else if (o instanceof ModelOperationInterruptedException) {
            UploadVideoDAO.delete(_applicationContext, _flatshareId);
        }

    }

    private PendingIntent notificationIntent() {
        return ManagePhotosActivity.startFromNotificationIntent(_applicationContext, _maxPhotos, _flatshareId, _flatshareType);
    }
}
