package com.spareroom.ui.util

import android.animation.*
import android.view.View
import android.view.ViewGroup
import com.spareroom.R

private const val HIDE_ANIMATION_DURATION = 300L
private const val FADE_ANIMATION_DURATION = 2000L
private const val SOLID_COLOR = 1f
private const val TRANSPARENT = 0f
private const val HALF_TRANSPARENT = 0.5f
private const val ALPHA = "alpha"

object AnimationUtils {

    fun shakeLeft(v: View) {
        val shake = android.view.animation.AnimationUtils.loadAnimation(v.context, R.anim.shake_to_left)
        v.startAnimation(shake)
    }

    fun hideView(view: View) {
        val animation = ObjectAnimator.ofFloat(view, ALPHA, SOLID_COLOR, TRANSPARENT)
        animation.duration = HIDE_ANIMATION_DURATION
        animation.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                view.visibility = View.INVISIBLE
                view.alpha = SOLID_COLOR
            }
        })
        animation.start()
    }

    fun clearAnimation(viewGroup: ViewGroup) {
        (0 until viewGroup.childCount)
            .map { viewGroup.getChildAt(it) }
            .forEach {
                if (it is ViewGroup) {
                    clearAnimation(it)
                } else {
                    val animatorSet = it.getTag(R.id.animation_tag)
                    if (animatorSet != null && animatorSet is Animator) {
                        animatorSet.cancel()
                        it.setTag(R.id.animation_tag, null)
                    }
                }
            }
    }

    fun startFadeAnimation(viewGroup: ViewGroup, viewsToExcludeFormAnimation: List<Int> = emptyList(), repeatCount: Int) {
        (0 until viewGroup.childCount)
            .map { viewGroup.getChildAt(it) }
            .forEach {
                if (it is ViewGroup) {
                    startFadeAnimation(it, viewsToExcludeFormAnimation, repeatCount)
                } else {
                    if (viewsToExcludeFormAnimation.isEmpty() || !viewsToExcludeFormAnimation.contains(it.id))
                        startSingleFadeAnimation(it, repeatCount)
                }
            }
    }

    private fun startSingleFadeAnimation(view: View, repeatCount: Int) {
        val animation = ObjectAnimator.ofFloat(view, ALPHA, SOLID_COLOR, HALF_TRANSPARENT, SOLID_COLOR)
        animation.duration = FADE_ANIMATION_DURATION
        animation.repeatCount = repeatCount

        view.setTag(R.id.animation_tag, animation)
        animation.start()
    }
}
