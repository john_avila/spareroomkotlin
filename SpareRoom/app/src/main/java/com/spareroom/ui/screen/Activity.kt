package com.spareroom.ui.screen

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import com.spareroom.R
import com.spareroom.integration.analytics.AnalyticsTrackerComposite
import com.spareroom.integration.sharedpreferences.EventsDao
import com.spareroom.lib.core.ObserverLifecycleInterface
import java.util.*

abstract class Activity : AppCompatActivity() {

    protected var isActivityResumed = false
    var isActivityDestroyed = false

    private val lifecycleObservers = LinkedList<ObserverLifecycleInterface>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AnalyticsTrackerComposite.getInstance().trackScreen_Activity(this)
        if (savedInstanceState == null)
            EventsDao(this.application).logDaysOfUseEvent(Calendar.getInstance())
    }

    override fun onResume() {
        super.onResume()
        isActivityResumed = true
    }

    override fun onPause() {
        super.onPause()
        isActivityResumed = false
    }

    override fun onStop() {
        for (lifecycleInterface in lifecycleObservers)
            lifecycleInterface.notifyOnStop()

        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        isActivityDestroyed = true
    }

    fun addObserver(o: ObserverLifecycleInterface) {
        lifecycleObservers.add(o)
    }

    fun handleHomeButton(menuItem: MenuItem): Boolean {
        return when (menuItem.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(menuItem)
        }
    }

    @JvmOverloads
    fun setToolbar(title: String? = null, homeAsUp: Boolean = true, @DrawableRes icon: Int = R.drawable.ic_back, view: View? = null) {
        setSupportActionBar(if (view != null) view.findViewById(R.id.toolbar) else findViewById(R.id.toolbar))
        val actionBar = supportActionBar
        if (actionBar != null) {
            if (!title.isNullOrBlank())
                actionBar.title = title

            if (homeAsUp) {
                actionBar.setDisplayHomeAsUpEnabled(true)
                actionBar.setHomeAsUpIndicator(icon)
            }
        }
    }

    fun setSubtitle(subtitle: String?) {
        supportActionBar?.subtitle = subtitle
    }

}