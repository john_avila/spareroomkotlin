package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.model.business.DraftAdOffered.PropertyType;
import com.spareroom.ui.widget.PlaceAdButton;

public class PlaceAdOfferedPropertyFragment extends PlaceAdFragment {
    private PlaceAdButton _previouslySelected;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_offered_property, container, false);

        setTitle(getString(R.string.place_ad_offered_property_title));

        final PlaceAdButton bFlat = rootView.findViewById(R.id.place_ad_offered_property_bFlat);
        final PlaceAdButton bHouse = rootView.findViewById(R.id.place_ad_offered_property_bHouse);
        final PlaceAdButton bOther = rootView.findViewById(R.id.place_ad_offered_property_bOther);

        PropertyType propertyType = ((DraftAdOffered) getDraftAd()).get_propertyType();

        if (propertyType != null)
            switch (propertyType) {
                case FLAT:
                    _previouslySelected = bFlat;
                    bFlat.setPreviouslySelected();
                    break;
                case HOUSE:
                    _previouslySelected = bHouse;
                    bHouse.setPreviouslySelected();
                    break;
                case OTHER:
                    _previouslySelected = bOther;
                    bOther.setPreviouslySelected();
                    break;
                default:
            }

        bFlat.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bFlat.setPreviouslySelected();
            PlaceAdOfferedPropertyFragment.this.finish();
            DraftAdOffered draft = (DraftAdOffered) getDraftAd();
            draft.set_propertyType(PropertyType.FLAT);
        });

        bHouse.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bHouse.setPreviouslySelected();
            PlaceAdOfferedPropertyFragment.this.finish();
            DraftAdOffered draft = (DraftAdOffered) getDraftAd();
            draft.set_propertyType(PropertyType.HOUSE);
        });

        bOther.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bOther.setPreviouslySelected();
            PlaceAdOfferedPropertyFragment.this.finish();
            DraftAdOffered draft = (DraftAdOffered) getDraftAd();
            draft.set_propertyType(PropertyType.OTHER);
        });

        return rootView;
    }
}
