package com.spareroom.ui.screen.customtabs

import android.app.Activity
import android.content.Intent
import android.net.Uri
import com.spareroom.R
import com.spareroom.ui.util.Launcher
import com.spareroom.ui.util.ToastUtils

class WebBrowserFallback : CustomTabFallback {

    override fun openUri(activity: Activity, uri: Uri) {
        val intent = Intent(Intent.ACTION_VIEW, uri)

        if (Launcher.canHandleIntent(activity, intent)) {
            activity.startActivity(intent)
        } else {
            ToastUtils.showToast(activity.getString(R.string.no_browser))
        }
    }
}