package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.lib.util.NumberUtil;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.ui.widget.RangeSeekBar;
import com.spareroom.ui.widget.util.exclusivitymanager.*;

public class PlaceAdWantedAgeFragment extends PlaceAdFragment {
    private TextView _tvMin;
    private TextView _tvMax;
    private Button _bYes;
    private Button _bNo;
    private Boolean _isCouple = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_wanted_age, container, false);

        setTitle(getString(R.string.place_ad_wanted_age_title));

        int minAge = NumberUtil.MIN_AGE;

        SeekBar _sbAge = rootView.findViewById(R.id.place_ad_wanted_age_sbAge);
        RangeSeekBar<Integer> _rsbAge = rootView.findViewById(R.id.place_ad_wanted_age_rsbAge);
        _tvMin = rootView.findViewById(R.id.place_ad_wanted_age_tvMin);
        TextView _tvTo = rootView.findViewById(R.id.place_ad_wanted_age_tvTo);
        _tvMax = rootView.findViewById(R.id.place_ad_wanted_age_tvMax);
        Button _bConfirm = rootView.findViewById(R.id.place_ad_bConfirm);
        TextView _tvCouple = rootView.findViewById(R.id.place_ad_wanted_age_tvCouple);
        LinearLayout _llCouple = rootView.findViewById(R.id.place_ad_wanted_age_llCouple);
        _bYes = rootView.findViewById(R.id.place_ad_wanted_age_bCoupleYes);
        _bNo = rootView.findViewById(R.id.place_ad_wanted_age_bCoupleNo);

        final DraftAdWanted draft = (DraftAdWanted) getDraftAd();

        // In case there is an activity state change and we lost the draft
        if (draft == null) {
            getActivity().finish();

            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }

        if ((draft.get_advertiserType() == DraftAdWanted.AdvertiserType.MALE_FEMALE)
                || (draft.get_advertiserType() == DraftAdWanted.AdvertiserType.TWO_FEMALES)
                || (draft.get_advertiserType() == DraftAdWanted.AdvertiserType.TWO_MALES)) {
            _tvTo.setVisibility(View.VISIBLE);
            _tvMax.setVisibility(View.VISIBLE);
            _rsbAge.setVisibility(View.VISIBLE);
            _sbAge.setVisibility(View.GONE);
            _tvCouple.setVisibility(View.VISIBLE);
            _llCouple.setVisibility(View.VISIBLE);
        }

        if (draft.get_age() != 0) {
            _tvMin.setText(String.format(AppVersion.flavor().getLocale(), "%d", draft.get_age()));
            _sbAge.setProgress(draft.get_age() - minAge);
            _rsbAge.setSelectedMinValue(draft.get_age() - minAge);
        } else {
            if (draft.get_ageMin() != 0) {
                _tvMin.setText(String.format(AppVersion.flavor().getLocale(), "%d", draft.get_ageMin()));
                _sbAge.setProgress(draft.get_ageMin() - minAge);
                _rsbAge.setSelectedMinValue(draft.get_ageMin() - minAge);
            }

            if (draft.get_ageMax() != 0) {
                _tvMax.setText(String.format(AppVersion.flavor().getLocale(), "%d", draft.get_ageMax()));
                _rsbAge.setSelectedMaxValue(draft.get_ageMax() - minAge);
            }

        }

        if (draft.isCouple() != null) {
            if (draft.isCouple())
                _bYes.setBackgroundResource(R.drawable.button_place_ad_bkg_selected);
            else
                _bNo.setBackgroundResource(R.drawable.button_place_ad_bkg_selected);
        }

        _rsbAge.setNotifyWhileDragging(true);
        _sbAge.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _tvMin.setText(String.format(AppVersion.flavor().getLocale(), "%d", progress + minAge));
            }
        });

        _rsbAge.setOnRangeSeekBarChangeListener((bar, minValue, maxValue) -> {
            _tvMin.setText(String.format(AppVersion.flavor().getLocale(), "%d", minValue));
            _tvMax.setText(String.format(AppVersion.flavor().getLocale(), "%d", maxValue));
        });

        ExclusivityManager billingManager = new ExclusivityManager();

        ViewActivationCommand vacPerMonth = new ButtonViewActivationCommand(_bYes);
        billingManager.add(vacPerMonth);
        _bYes.setOnClickListener(new ButtonYesOnClickListener(billingManager));

        ViewActivationCommand vacPerWeek = new ButtonViewActivationCommand(_bNo);
        billingManager.add(vacPerWeek);
        _bNo.setOnClickListener(new ButtonNoOnClickListener(billingManager));

        _bConfirm.setOnClickListener(v -> {
            (draft).setIsCouple(_isCouple);

            if ((draft.get_advertiserType() == DraftAdWanted.AdvertiserType.MALE_FEMALE)
                    || (draft.get_advertiserType() == DraftAdWanted.AdvertiserType.TWO_FEMALES)
                    || (draft.get_advertiserType() == DraftAdWanted.AdvertiserType.TWO_MALES)) {
                (draft).set_ageMin(Integer.parseInt(_tvMin.getText().toString()));
                (draft).set_ageMax(Integer.parseInt(_tvMax.getText().toString()));
                (draft).set_age(0);
            } else {
                (draft).set_age(Integer.parseInt(_tvMin.getText().toString()));
                (draft).set_ageMin(0);
                (draft).set_ageMax(0);
            }
            finish();

        });
        return rootView;
    }

    private class ButtonViewActivationCommand extends ViewActivationCommand {
        ButtonViewActivationCommand(View v) {
            super(v);
        }

        @Override
        public void activate() {
            super.activate();
            _view.setBackgroundResource(R.drawable.button_place_ad_bkg_pressed);
        }

        @Override
        public void deactivate() {
            super.deactivate();
            _view.setBackgroundResource(R.drawable.button_place_ad_bkg);
        }
    }

    private class ButtonYesOnClickListener implements OnClickListener {
        private final ExclusivityManager _em;

        ButtonYesOnClickListener(ExclusivityManager em) {
            _em = em;
        }

        @Override
        public void onClick(View v) {
            try {
                _bNo.setBackgroundResource(R.drawable.button_place_ad_bkg);
                _em.activate(v);
                _isCouple = true;
            } catch (ExclusiveViewNotFoundException e) {
                // another empty catch
            }
        }
    }

    private class ButtonNoOnClickListener implements OnClickListener {
        private final ExclusivityManager _em;

        ButtonNoOnClickListener(ExclusivityManager em) {
            _em = em;
        }

        @Override
        public void onClick(View v) {
            try {
                _bYes.setBackgroundResource(R.drawable.button_place_ad_bkg);
                _em.activate(v);
                _isCouple = false;
            } catch (ExclusiveViewNotFoundException e) {
                // another empty catch
            }
        }
    }

}
