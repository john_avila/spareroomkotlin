package com.spareroom.ui.filters.screen;

import android.os.Bundle;
import android.view.MenuItem;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.ui.screen.InjectableActivity;
import com.spareroom.ui.screen.legacy.ResultsActivity;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

public class FilterActivity extends InjectableActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_activity);

        setToolbar();

        if (savedInstanceState == null)
            loadMainFragment();

    }

    private void loadMainFragment() {
        SearchAdvertListProperties properties =
                (SearchAdvertListProperties) getIntent().getSerializableExtra(ResultsActivity.SEARCH_PROPERTIES_INTENT_KEY);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        FilterSummaryFragment fragment = FilterSummaryFragment.newInstance(properties);
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
