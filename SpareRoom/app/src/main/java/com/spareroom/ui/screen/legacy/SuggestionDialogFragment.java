package com.spareroom.ui.screen.legacy;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum;
import com.spareroom.model.business.Parameters;
import com.spareroom.ui.screen.DialogFragment;

import androidx.annotation.NonNull;

public class SuggestionDialogFragment extends DialogFragment {
    private String[] suggestions;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle arguments = getArguments();
        suggestions = arguments.getStringArray("suggestions");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setAdapter(new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_list_item_1, suggestions), new SuggestionOnClickListener());
        return builder.create();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        AnalyticsTrackerComposite.getInstance().trackEvent_Results_CancelSuggestionAdvertSearch();
    }

    private class SuggestionOnClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            Parameters p = new Parameters();
            p.add(SearchParamEnum.WHERE, suggestions[which]);
            ((ResultsActivity) getActivity()).launchSuggestedSearch(p);
            AnalyticsTrackerComposite.getInstance().trackEvent_Results_SuggestionAdvertSearch();
            ((ResultsActivity) getActivity()).showEmptyText(false);
        }

    }
}