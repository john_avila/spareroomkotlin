package com.spareroom.ui.filters.adapter.viewholder;

import android.view.View;

import com.spareroom.ui.filters.adapter.viewparams.AbstractSummaryViewParams;
import com.spareroom.ui.filters.provider.FilterItem;
import com.spareroom.ui.util.StringUtils;
import com.spareroom.ui.widget.SummaryView;

public class SummaryViewHolder extends BaseFilterViewHolder {
    private final SummaryView summaryView;

    public SummaryViewHolder(View itemView) {
        super(itemView);
        summaryView = (SummaryView) itemView;
    }

    @Override
    public void bind(FilterItem filterItem) {
        AbstractSummaryViewParams summaryParams = (AbstractSummaryViewParams) filterItem.params();
        summaryView.icon(summaryParams.iconDrawableId());
        summaryView.title(summaryView.getContext().getString(summaryParams.titleStringId()));

        summaryView.highlight(
                !StringUtils.isNullOrEmpty(summaryParams.subtitleString())
                        ? summaryParams.subtitleString()
                        : summaryParams.defaultSubtitleString(),
                !StringUtils.isNullOrEmpty(summaryParams.subtitleString()));
    }

}
