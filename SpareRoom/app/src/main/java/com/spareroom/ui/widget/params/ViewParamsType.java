package com.spareroom.ui.widget.params;

public enum ViewParamsType {GROUPED_COMPOUND, COMPOUND, SUMMARY, MIN_MAX, TITLE, SPACE}