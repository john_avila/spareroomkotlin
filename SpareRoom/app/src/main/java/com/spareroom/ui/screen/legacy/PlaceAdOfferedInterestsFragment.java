package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;

import com.spareroom.R;

import androidx.annotation.Nullable;

public class PlaceAdOfferedInterestsFragment extends PlaceAdInterestsFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setTitle(getString(R.string.place_ad_offered_room_interests_title));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

}