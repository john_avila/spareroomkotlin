package com.spareroom.ui.controller;

import com.spareroom.model.business.SpareroomStatus;

/**
 * Controller that handles ui components when populating them.
 * <p>
 * Created by miguel.rossi on 21/11/2016.
 */

public interface ActionUiController {
    /**
     * Populates components with content. For <code>SpareroomStatus</code> and
     * <code>Exception</code> use {@link #setSpareroomStatus(SpareroomStatus)}
     * or {@link #setException(Exception)}
     *
     * @param o objects that help to update the UI
     */
    void setResult(Object... o);

    /**
     * Populates components with content.
     *
     * @param spareroomStatus that helps to update the UI
     */
    void setSpareroomStatus(SpareroomStatus spareroomStatus);

    /**
     * Populates components with content.
     *
     * @param exception that helps to update the UI
     */
    void setException(Exception exception);

}
