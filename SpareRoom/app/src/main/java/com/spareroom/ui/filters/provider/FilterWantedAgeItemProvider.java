package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.widget.params.MinMaxEditTextParams;

import java.util.*;

import androidx.annotation.NonNull;

class FilterWantedAgeItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.age;

    final static String AGE_RANGE = "age_range";

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesWanted previousProperties = (SearchAdvertListPropertiesWanted) properties;
        List<FilterItem> list = new ArrayList<>();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        MinMaxEditTextParams ageRange = new MinMaxEditTextParams(AGE_RANGE);
        ageRange.iconId(R.drawable.ic_age);
        ageRange.title(application.getString(R.string.ageRange));
        ageRange.minTitle(application.getString(R.string.minimum));
        ageRange.minHint(application.getString(R.string.min_age));
        ageRange.maxTitle(application.getString(R.string.maximum));
        ageRange.maxHint(application.getString(R.string.sixty_five_plus));
        ageRange.minMaxCharacters(2);
        ageRange.maxMaxCharacters(2);
        ageRange.min(previousProperties.getMinAge());
        ageRange.max(previousProperties.getMaxAge());
        list.add(new FilterItem<>(ageRange));

        this.list = list;
    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return null;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesWanted propertiesWanted = (SearchAdvertListPropertiesWanted) propertiesToUpdate;
        for (FilterItem item : list) {
            switch (item.tag()) {
                case AGE_RANGE:
                    propertiesWanted.setMinAge(((MinMaxEditTextParams) item.params()).min());
                    propertiesWanted.setMaxAge(((MinMaxEditTextParams) item.params()).max());
                    break;
            }
        }

        if (sortProperties)
            sortAgeRange(propertiesWanted);
    }

}
