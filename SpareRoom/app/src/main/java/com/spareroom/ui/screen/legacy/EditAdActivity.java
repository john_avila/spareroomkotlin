package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.model.business.*;
import com.spareroom.ui.flow.*;

public class EditAdActivity extends PlaceAdAbstractActivity {

    //region FIELDS CONTROLLER

    private String _advertId;
    private String _advertType;

    private FlowState _state;

    protected void screenFinished() {
        _nextFragmentName = _manager.getNextFragmentName(_currentFragmentName, _draft);
        _ghostFragmentName = _nextFragmentName;

        changeForwardProgress(_manager.getProgress(_nextFragmentName));

        startScreen(true);

    } //end screenFinished()

    //endregion FIELDS CONTROLLER

    //region METHODS UI

    @Override
    public String getId() {
        return BUNDLE_VALUE_FORM_ID_EDIT;
    }

    @Override
    protected void changeNavigationProgress() {

        if (_nextFragmentName.equals(PlaceAdTitleDescriptionFragment.class.getName())) {
            disableForwardNavigation();
        } else if (_nextFragmentName.equals(PlaceAdUploadCompleteFragment.class.getName()) ||
                _nextFragmentName.equals(PlaceAdEditUploadingFragment.class.getName())) {
            disableForwardNavigation();
        } else {
            enableForwardNavigation();
        }

    }

    //endregion METHODS UI

    //region METHODS CONTROLLER

    private void init() {
        disableForwardNavigation();
        if (_advertType.equals("offered")) {
            _state = new PlaceAdStateEditOffered();
            _manager.setState(_state);
            launchReadDraftOffered();
        } else { // _advertType is "wanted"
            _state = new PlaceAdStateEditWanted();
            _manager.setState(_state);
            launchReadDraftWanted();
        }
    }

    private final ControllerActionObserver _readDraftAdObserver = new ControllerActionObserver() {
        // region UTIL
        private void handleInconsistentAdvertType() {
            // TODO send report
            Toast.makeText(EditAdActivity.this,
                    getString(R.string.editAdActivity_handleInconsistentAdvertType_toast),
                    Toast.LENGTH_LONG).show();
            finish();
        }

        private void handleError() {
            // TODO send report
            Toast.makeText(EditAdActivity.this,
                    getString(R.string.editAdActivity_handleInconsistentAdvertType_toast),
                    Toast.LENGTH_LONG).show();
            finish();
        }

        private void handleInternalMessage(SpareroomStatus im) {
            if (im.getMessage() != null)
                Toast.makeText(EditAdActivity.this,
                        im.getMessage(),
                        Toast.LENGTH_LONG).show();
            else
                handleError();
        }

        //endregion UTIL

        @Override
        // Processes the successful response - it can only be null
        public void onResult(Object o) {

            if (o == null) {
                Toast.makeText(
                        EditAdActivity.this,
                        getString(R.string.editAdActivity_notAbleToEdit_toast),
                        Toast.LENGTH_LONG).show();
                finish();
            }

            if (o instanceof SpareroomStatus) {
                handleInternalMessage((SpareroomStatus) o);
                return;
            }

            if (_advertType.equals("offered")) {
                if (!(o instanceof DraftAdOffered)) {
                    handleInconsistentAdvertType();
                    return;
                }

                _draft = (DraftAdOffered) o;
                AnalyticsTrackerComposite.getInstance().trackEvent_EditAd_ReadOfferedAd();

            } else if (_advertType.equals("wanted")) {
                if (!(o instanceof DraftAdWanted)) {
                    handleInconsistentAdvertType();
                    return;
                }

                _draft = (DraftAdWanted) o;
                AnalyticsTrackerComposite.getInstance().trackEvent_EditAd_ReadWantedAd();
            }

            _currentFragmentName = _manager.getNextFragmentName(null, _draft);
            _nextFragmentName = _manager.getNextFragmentName(null, _draft);
            _ghostFragmentName = _state.fragmentAtPosition(_state.fragmentListSize());

            changeForwardProgress(_manager.getProgress(_nextFragmentName));

            startScreen(true);
            enableForwardNavigation();
        }

        @Override
        public void onSpareroomStatus(SpareroomStatus status) {
            if (_advertType.equals("offered"))
                AnalyticsTrackerComposite.getInstance().trackEvent_EditAd_ReadOfferedAdWithErrors(status);
            else if (_advertType.equals("wanted"))
                AnalyticsTrackerComposite.getInstance().trackEvent_EditAd_ReadWantedAdWithErrors(status);

            if (status.getMessage() != null)
                Toast.makeText(EditAdActivity.this,
                        status.getMessage(),
                        Toast.LENGTH_LONG).show();
            else
                handleError();
        }

        @Override
        public void onException(Exception exception) {
            handleError();
        }

    };

    private void launchReadDraftOffered() {

        DraftAdOfferedReadControllerAction draftAdOfferedReadControllerAction =
                new DraftAdOfferedReadControllerAction(
                        SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext());
        draftAdOfferedReadControllerAction.addControllerActionObserver(_readDraftAdObserver);
        _asyncManager.register(draftAdOfferedReadControllerAction);
        draftAdOfferedReadControllerAction.execute(_advertId);

    }

    private void launchReadDraftWanted() {

        DraftAdWantedReadControllerAction draftAdWantedReadControllerAction =
                new DraftAdWantedReadControllerAction(
                        SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext());
        draftAdWantedReadControllerAction.addControllerActionObserver(_readDraftAdObserver);
        _asyncManager.register(draftAdWantedReadControllerAction);
        draftAdWantedReadControllerAction.execute(_advertId);

    }

    @Override
    protected void nextScreen() {
        super.nextScreen();
        _ghostFragmentName = _currentFragmentName;
    }

    //endregion METHODS CONTROLLER

    //region METHODS LIFECYCLE

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        _advertId = getIntent().getStringExtra("advert_id");
        _advertType = getIntent().getStringExtra("advert_type");

        if (_advertId == null || _advertType == null) {
            Toast.makeText(
                    EditAdActivity.this,
                    getString(R.string.editAdActivity_notAbleToEdit_toast),
                    Toast.LENGTH_LONG).show();
            finish();
        }

        init();
    }

    @Override
    public void onBackPressed() {
        int currentProgress = _manager.getProgress(_currentFragmentName);

        // TODO this may not happen as we should possibly close the EditAdActivity after finishing last step
        if (currentProgress == 100) {
            Toast.makeText(this, "Your advert has already been updated", Toast.LENGTH_LONG).show();
            finish();
        }

        super.onBackPressed();

    } //end onBackPressed()

    @Override
    protected void onStop() {
        super.onStop();
        _asyncManager.cancelAll();
    }

    //endregion METHODS LIFECYCLE

}
