package com.spareroom.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.*;
import android.widget.*;

import com.spareroom.R;

import androidx.core.content.ContextCompat;

public class PlaceAdButton extends LinearLayout {
    private Context _context;
    private TextView _tvText;
    private ImageView _ivIcon;
    private ImageView _ivBulletIcon;

    private int _resBulletId;
    private int _resBulletHighlightId = -1;
    private int _resIconTopId;
    private int _resIconTopHighlightId = -1;

    private String _text = "";

    private OnClickListener _onClickListener;

    private OnTouchListener _onTouchListener;

    public PlaceAdButton(Context context) {
        super(context);
        _context = context;
        init(context);
    }

    public PlaceAdButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        _context = context;

        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.PlaceAdButton);

        for (int i = 0; i < styledAttrs.getIndexCount(); ++i) {
            int attrIndex = styledAttrs.getIndex(i);
            if (attrIndex == R.styleable.PlaceAdButton_drawableIconTop) {
                String bulletIconResId = styledAttrs.getString(attrIndex);
                String[] vBulletRes = bulletIconResId.split("/");
                String resBullet = vBulletRes[vBulletRes.length - 1].split("\\.")[0];
                _resIconTopId = _context.getResources().getIdentifier(resBullet, "drawable", _context.getPackageName());
                _ivIcon = new ImageView(_context);
                _ivIcon.setImageResource(_resIconTopId);
            } else if (attrIndex == R.styleable.PlaceAdButton_drawableIconTopHighlight) {
                String bulletIconHighlightResId = styledAttrs.getString(attrIndex);
                String[] vBulletHighlightRes = bulletIconHighlightResId.split("/");
                String resBulletHighlight = vBulletHighlightRes[vBulletHighlightRes.length - 1].split("\\.")[0];
                _resIconTopHighlightId = _context.getResources().getIdentifier(resBulletHighlight, "drawable", _context.getPackageName());
            } else if (attrIndex == R.styleable.PlaceAdButton_drawableBulletLeft) {
                String topIconResId = styledAttrs.getString(attrIndex);
                String[] vIconRes = topIconResId.split("/");
                String res = vIconRes[vIconRes.length - 1].split("\\.")[0];
                _resBulletId = _context.getResources().getIdentifier(res, "drawable", _context.getPackageName());
                _ivBulletIcon = new ImageView(_context);
                _ivBulletIcon.setImageResource(_resBulletId);
            } else if (attrIndex == R.styleable.PlaceAdButton_drawableBulletLeftHighlight) {
                String topIconHighlightResId = styledAttrs.getString(attrIndex);
                String[] vIconHighlightRes = topIconHighlightResId.split("/");
                String resHighlight = vIconHighlightRes[vIconHighlightRes.length - 1].split("\\.")[0];
                _resBulletHighlightId = _context.getResources().getIdentifier(resHighlight, "drawable", _context.getPackageName());
            } else if (attrIndex == R.styleable.PlaceAdButton_text) {
                _text = styledAttrs.getString(attrIndex);
            }
        }
        styledAttrs.recycle();
        init(context);
    }

    public void setPreviouslySelected() {
        setBackgroundResource(R.drawable.button_place_ad_bkg_selected);
        _tvText.setTextColor(ContextCompat.getColor(getContext(), R.color.cyan));
        if (_ivBulletIcon != null)
            _ivBulletIcon.setBackgroundResource(R.drawable.button_place_ad_bullet_blue_bkg);
        if (_ivIcon != null)
            _ivIcon.setBackgroundResource(R.drawable.bkg_blue_icon_button_place_ad);
    }

    public void setPreviouslyNotSelected() {
        setBackgroundResource(R.drawable.button_place_ad_bkg);
        _tvText.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_2));
        if (_ivBulletIcon != null)
            _ivBulletIcon.setBackgroundResource(R.drawable.button_place_ad_bullet_grey_bkg);
        if (_ivIcon != null)
            _ivIcon.setBackgroundResource(R.drawable.bkg_grey_icon_button_place_ad);
    }

    private void init(Context context) {
        _context = context;

        setClickable(true);
        // setBackgroundResource(R.drawable.button_place_ad_bkg);

        LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        setGravity(Gravity.CENTER);

        if (_ivBulletIcon != null) {
            this.setOrientation(LinearLayout.HORIZONTAL);
            LayoutParams lpBullet = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            _ivBulletIcon.setBackgroundResource(R.drawable.button_place_ad_bullet_blue_bkg);
            _ivBulletIcon.setId(R.id.placeAdButtonBulletIconId);
            _ivBulletIcon.setLayoutParams(lpBullet);
            addView(_ivBulletIcon);
        }

        if ((_ivIcon != null) && (_ivBulletIcon == null)) {
            this.setOrientation(LinearLayout.VERTICAL);
            int iconSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, getResources().getDisplayMetrics());
            setGravity(Gravity.CENTER);
            LayoutParams lpIcon = new LayoutParams(iconSize, iconSize);
            _ivIcon.setBackgroundResource(R.drawable.bkg_grey_icon_button_place_ad);
            _ivIcon.setId(R.id.placeAdButtonIconId);
            _ivIcon.setLayoutParams(lpIcon);
            addView(_ivIcon);
        }

        _tvText = new TextView(context);
        _tvText.setId(R.id.placeAdButtonTextId);
        _tvText.setGravity(Gravity.CENTER);
        _tvText.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_2));
        _tvText.setPadding(15, 15, 15, 15);
        _tvText.setText(_text);
        if (_ivBulletIcon != null) {
            LayoutParams lpText = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            _tvText.setLayoutParams(lpText);
        } else {
            LinearLayout.LayoutParams lpText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lpText.gravity = Gravity.CENTER;
            _tvText.setLayoutParams(lpText);
        }

        addView(_tvText);

        setLayoutParams(lp);

        unselect();

        _onTouchListener = new WidgetOnTouchListener();

        setOnTouchListener(_onTouchListener);

    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        _onClickListener = l;

        if (_tvText != null)
            _tvText.setOnTouchListener(_onTouchListener);
        if (_ivBulletIcon != null)
            _ivBulletIcon.setOnTouchListener(_onTouchListener);
        if (_ivIcon != null)
            _ivIcon.setOnTouchListener(_onTouchListener);

    }

    @Override
    public boolean performClick() {
        _onClickListener.onClick(this);
        return super.performClick();
    }

    private void press() {
        setBackgroundResource(R.drawable.button_place_ad_bkg_pressed);
        if (_ivIcon != null) {
            _ivIcon.setBackgroundResource(R.drawable.bkg_blue_icon_button_place_ad);
            if (_resIconTopHighlightId != -1) {
                _ivIcon.setImageResource(_resIconTopHighlightId);
            }
        } else if (_ivBulletIcon != null) {
            _ivBulletIcon.setBackgroundResource(R.drawable.button_place_ad_bullet_blue_bkg);
            if (_resBulletHighlightId != -1) {
                _ivBulletIcon.setImageResource(_resBulletHighlightId);
            }
        }
        _tvText.setTextColor(ContextCompat.getColor(getContext(), R.color.cyan));
    }

    private void select() {
        setBackgroundResource(R.drawable.button_place_ad_bkg_selected);
        if (_ivIcon != null) {
            _ivIcon.setBackgroundResource(R.drawable.bkg_blue_icon_button_place_ad);
            if (_resIconTopHighlightId != -1) {
                _ivIcon.setImageResource(_resIconTopHighlightId);
            }
        } else if (_ivBulletIcon != null) {
            _ivBulletIcon.setBackgroundResource(R.drawable.button_place_ad_bullet_blue_bkg);
            if (_resBulletHighlightId != -1) {
                _ivBulletIcon.setImageResource(_resBulletHighlightId);
            }
        }
        _tvText.setTextColor(ContextCompat.getColor(getContext(), R.color.cyan));
    }

    private void unselect() {
        setBackgroundResource(R.drawable.button_place_ad_bkg);
        if (_ivIcon != null) {
            _ivIcon.setBackgroundResource(R.drawable.bkg_grey_icon_button_place_ad);
            if (_resIconTopHighlightId != -1) {
                _ivIcon.setImageResource(_resIconTopId);
            }
        } else if (_ivBulletIcon != null) {
            _ivBulletIcon.setBackgroundResource(R.drawable.button_place_ad_bullet_grey_bkg);
            if (_resBulletHighlightId != -1) {
                _ivBulletIcon.setImageResource(_resBulletId);
            }
        }
        _tvText.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_2));
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        if (selected) {
            select();
            return;
        }
        unselect();
    }

    private class WidgetOnTouchListener implements OnTouchListener {
        private boolean _isBeingPressed = false;

        public WidgetOnTouchListener() {
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                press();
                _isBeingPressed = true;
                return true;
            } else if (_isBeingPressed && (((event.getAction() == MotionEvent.ACTION_UP) || (event.getAction() == MotionEvent.ACTION_MOVE)))) {
                unselect();
                performClick();
                _isBeingPressed = false;
                return false;
            }
            return false;
        }
    }

}
