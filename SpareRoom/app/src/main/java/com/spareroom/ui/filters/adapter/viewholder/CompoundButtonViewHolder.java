package com.spareroom.ui.filters.adapter.viewholder;

import android.view.View;

import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;
import com.spareroom.ui.filters.provider.FilterItem;
import com.spareroom.ui.util.StringUtils;
import com.spareroom.ui.widget.CompoundButtonView;
import com.spareroom.ui.widget.params.CompoundButtonParams;

public class CompoundButtonViewHolder extends BaseFilterViewHolder {
    private final CompoundButtonView compoundButton;

    public CompoundButtonViewHolder(View itemView) {
        super(itemView);
        compoundButton = (CompoundButtonView) itemView;
    }

    @Override
    public void bind(FilterItem filterItem) {
        final CompoundButtonParams params = (CompoundButtonParams) filterItem.params();
        compoundButton.setParameters(params);
        compoundButton.setChecked(params.selected());

        if (params instanceof GroupedCompoundButtonParams)
            stopUserToChangeState(((GroupedCompoundButtonParams) params).groupName(), params.selected());
    }

    private void stopUserToChangeState(String groupName, boolean isChecked) {
        if (!StringUtils.isNullOrEmpty(groupName))
            compoundButton.stopUserToChangeState(isChecked);
    }

}
