package com.spareroom.ui.screen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.spareroom.R;

public class OnboardingFragment extends Fragment {

    //region FIELDS CONTROLLER

    private static final String ARGUMENT_BODY_TEXT_ID = "body_text_id";
    private static final String ARGUMENT_TITLE_TEXT_ID = "title_text_id";
    private static final String ARGUMENT_FRAGMENT_POSITION = "fragment_position";
    private int _position;

    //endregion FIELDS CONTROLLER

    //region METHODS CONTROLLER

    public int getPosition() {
        return _position;
    }

    public static OnboardingFragment newInstance(int position, String titleTextId, String bodyTextId) {
        OnboardingFragment onboardingFragment = new OnboardingFragment();
        Bundle args = new Bundle();

        args.putString(ARGUMENT_TITLE_TEXT_ID, titleTextId);
        args.putString(ARGUMENT_BODY_TEXT_ID, bodyTextId);
        args.putInt(ARGUMENT_FRAGMENT_POSITION, position);

        onboardingFragment.setArguments(args);

        return onboardingFragment;
    }

    //endregion METHODS CONTROLLER

    //region METHODS LIFECYCLE

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.material_onboarding_fragment, container, false);
        TextView tvTitle = v.findViewById(R.id.onboarding_title);
        TextView tvBodyText = v.findViewById(R.id.onboarding_bodyText);

        setRetainInstance(true);

        tvTitle.setText(getArguments().getString(ARGUMENT_TITLE_TEXT_ID));
        tvBodyText.setText(getArguments().getString(ARGUMENT_BODY_TEXT_ID));
        _position = getArguments().getInt(ARGUMENT_FRAGMENT_POSITION);

        return v;
    }

    //endregion METHODS LIFECYCLE

}