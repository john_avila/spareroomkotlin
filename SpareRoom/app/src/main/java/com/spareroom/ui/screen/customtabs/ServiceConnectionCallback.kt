package com.spareroom.ui.screen.customtabs

import androidx.browser.customtabs.CustomTabsClient

interface ServiceConnectionCallback {
    fun onServiceConnected(client: CustomTabsClient)
    fun onServiceDisconnected()
}