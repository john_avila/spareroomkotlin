package com.spareroom.ui.flow;

import com.spareroom.ui.screen.LoginEmailFragment;
import com.spareroom.ui.screen.LoginMethodFragment;
import com.spareroom.ui.screen.LoginResetPasswordFragment;

/**
 * Login UI flow
 * <p>
 * Created by miguel.rossi on 15/07/2016.
 */
public class LoginFlow extends FlowState {

    private static final String PAGE_LOGIN_METHOD = LoginMethodFragment.class.getName();
    private static final String PAGE_LOGIN_EMAIL = LoginEmailFragment.class.getName();
    private static final String PAGE_RESET_EMAIL = LoginResetPasswordFragment.class.getName();

    public LoginFlow() {
        SCREENS = new String[]{
                PAGE_LOGIN_METHOD,
                PAGE_LOGIN_EMAIL,
                PAGE_RESET_EMAIL
        };
        init();
    } // end Register()

}
