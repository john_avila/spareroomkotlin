package com.spareroom.ui.adapter.conversation;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.spareroom.R;
import com.spareroom.integration.imageloader.ImageLoader;
import com.spareroom.model.business.*;
import com.spareroom.ui.adapter.RecyclerViewAdapter;
import com.spareroom.ui.screen.Fragment;
import com.spareroom.ui.util.*;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

public class ConversationAdapter extends RecyclerViewAdapter<Message, RecyclerView.ViewHolder> {

    private String loggedInUserId;
    private String loggedInUserInitials;
    private String userAvatarUrl;
    private String otherUserAvatarUrl;
    private String otherUserId;
    private Fragment fragment;
    private Conversation conversation;
    private String otherUserFirstNames;
    private String otherUserLastName;

    private final ImageLoader imageLoader;
    private final DateUtils dateUtils;
    private final AdvertUtils advertUtils;
    private final ConnectivityChecker connectivityChecker;

    @Inject
    public ConversationAdapter(ImageLoader imageLoader, DateUtils dateUtils, AdvertUtils advertUtils, ConnectivityChecker connectivityChecker) {
        this.imageLoader = imageLoader;
        this.dateUtils = dateUtils;
        this.advertUtils = advertUtils;
        this.connectivityChecker = connectivityChecker;
    }

    public void init(Context context, Fragment fragment, String loggedInUserId, String loggedInUserInitials,
                     String userAvatarUrl, String otherUserFirstNames, String otherUserLastName, String otherUserAvatarUrl, String otherUserId) {
        setContext(context);
        this.fragment = fragment;
        this.loggedInUserId = loggedInUserId;
        this.otherUserFirstNames = otherUserFirstNames;
        this.otherUserLastName = otherUserLastName;
        this.loggedInUserInitials = loggedInUserInitials;
        this.userAvatarUrl = userAvatarUrl;
        this.otherUserAvatarUrl = otherUserAvatarUrl;
        this.otherUserId = otherUserId;
    }

    public void update(ConversationResponse conversationResponse) {
        conversation = conversationResponse.getResponse();
        otherUserFirstNames = conversation.getOtherUserFirstNames();
        otherUserLastName = conversation.getOtherUserLastName();

        update(conversation.getMessages(), false);
        DiffUtil.DiffResult diffResult = conversationResponse.getMessagesDiffResult();
        diffResult.dispatchUpdatesTo(this);
    }

    public boolean hasMoreThanOneMessage() {
        return getConversationMessagesCount() > 1;
    }

    public boolean hasMessages() {
        return getConversationMessagesCount() > 0;
    }

    public boolean isLastMessageMyOwn() {
        return hasMessages() && isMyOwnMessage(getItem(getItemCount() - 1));
    }

    public String getLastMessageId() {
        return getItem(getItemCount() - 1).getId();
    }

    public String getDeclineTemplate() {
        return getItem(getItemCount() - 1).getDeclineTemplate();
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof HeaderMessageItem)
            return R.layout.material_conversation_detail_list_item_header;

        if (getItem(position) instanceof ConversationClosedMessageItem)
            return R.layout.material_conversation_detail_list_item_closed;

        return R.layout.material_conversation_detail_list_item_message;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = getInflater().inflate(viewType, parent, false);
        if (viewType == R.layout.material_conversation_detail_list_item_header)
            return new HeaderViewHolder(view, imageLoader, dateUtils, fragment, getContext(), loggedInUserId, otherUserFirstNames,
                    otherUserLastName, otherUserAvatarUrl, otherUserId, advertUtils);

        if (viewType == R.layout.material_conversation_detail_list_item_closed)
            return new ConversationClosedViewHolder(view);

        return new MessageViewHolder(view, imageLoader, dateUtils, connectivityChecker, fragment, getContext(), loggedInUserId,
                loggedInUserInitials, userAvatarUrl, otherUserFirstNames, otherUserLastName, otherUserAvatarUrl);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderViewHolder) {
            ((HeaderViewHolder) holder).bind((HeaderMessageItem) getItem(position), conversation);
            return;
        }

        if (holder instanceof MessageViewHolder)
            ((MessageViewHolder) holder).bind(position, getItemCount(), getItem(position));
    }

    private int getConversationMessagesCount() {
        if (conversation == null)
            return 0;

        int itemCount = getItemCount();

        // item count includes 'user details' item
        --itemCount;

        // item count may include 'conversation closed' item
        if (conversation.isClosed())
            --itemCount;

        return itemCount;
    }

    private boolean isMyOwnMessage(Message message) {
        return loggedInUserId.equalsIgnoreCase(message.getUserId());
    }

}
