package com.spareroom.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.Animation
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.spareroom.R
import com.spareroom.integration.imageloader.ImageLoader
import com.spareroom.integration.imageloader.ImageLoadingListener
import com.spareroom.model.business.*
import com.spareroom.model.business.advertiser.*
import com.spareroom.ui.screen.Fragment
import com.spareroom.ui.util.*
import com.spareroom.ui.util.UiUtils.hideViews
import com.spareroom.ui.util.UiUtils.showViews
import kotlinx.android.synthetic.main.advert_card_view.view.*
import java.util.*

class AdvertCardView : ConstraintLayout {

    private val monthlyBudget by lazy { getString(R.string.monthly_budget) }
    private val monthlyRent by lazy { getString(R.string.monthly_rent) }

    private val white by lazy { getColor(R.color.white) }
    private val whiteFour by lazy { getColor(R.color.white_four) }
    private val grey by lazy { getColor(R.color.grey_2) }

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private fun init(context: Context) {
        inflate(context, R.layout.advert_card_view, this)
    }

    fun bind(advert: AbstractAd, advertUtils: AdvertUtils, dateUtils: DateUtils, advertClickListener: OnClickListener, imageLoader: ImageLoader,
             fragment: Fragment, animation: Animation, showHeader: Boolean, headerTitle: String, showDivider: Boolean, showFooter: Boolean = true) {

        hideUserDetails()
        setHeader(headerTitle, showHeader)
        setDivider(showDivider)
        setUpDefaultViews(advert, advertUtils, advertClickListener, imageLoader, fragment, animation)
        setFooter(advert, advertUtils, dateUtils, showFooter)
    }

    fun bindWithUserDetails(advert: AbstractAd, advertUtils: AdvertUtils, dateUtils: DateUtils, advertClickListener: OnClickListener,
                            imageLoader: ImageLoader, fragment: Fragment, animation: Animation, avatarClickListener: OnClickListener,
                            showFooter: Boolean = true) {

        setUserDetails(imageLoader, fragment, advert.advertiser, avatarClickListener, advertUtils)
        setHeader(show = false)
        setDivider(show = false)
        setUpDefaultViews(advert, advertUtils, advertClickListener, imageLoader, fragment, animation)
        setFooter(advert, advertUtils, dateUtils, showFooter)
    }

    private fun setHeader(title: String = "", show: Boolean) {
        if (show) {
            txtHeader.text = title
            txtHeader.visibility = View.VISIBLE
        } else {
            txtHeader.visibility = View.GONE
        }
    }

    private fun setDivider(show: Boolean) {
        divider.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun setUpDefaultViews(advert: AbstractAd, advertUtils: AdvertUtils, advertClickListener: OnClickListener,
                                  imageLoader: ImageLoader, fragment: Fragment, animation: Animation) {
        setTag(R.id.item_tag, advert)
        setOnClickListener(advertClickListener)
        setStatus(advert, advertUtils)
        setTitle(advert, advertUtils)
        setLocation(advert, advertUtils)
        loadImage(advert, imageLoader, fragment, animation)
    }

    private fun setFooter(advert: AbstractAd, advertUtils: AdvertUtils, dateUtils: DateUtils, show: Boolean) {
        setBudget(advert, advertUtils, show)
        setDeposit(advert, advertUtils, show)
        setAvailability(advert, dateUtils, show)
    }

    private fun setUserDetails(imageLoader: ImageLoader, fragment: Fragment, advertiser: Advertiser, avatarClickListener: OnClickListener,
                               advertUtils: AdvertUtils) {
        val firstName = advertiser.firstName
        val lastName = advertiser.lastName
        val extraSeekerInfo = advertUtils.getFormattedExtraSeekersCount(advertiser)
        val fullName = if (advertiser is OfferedAdvertiser && advertiser.agent()) {
            advertUtils.getFormattedCompanyName(advertiser.companyName, advertiser.fullName)
        } else {
            StringUtils.getInitialsWithFullFirstName(firstName, lastName)
        }

        val description = if (advertiser is OfferedAdvertiser) {
            advertUtils.getFormattedAdvertiserType(advertiser.advertiserType)
        } else {
            advertUtils.getFormattedProfession((advertiser as WantedAdvertiser).profession, advertiser.numberOfSeekers, advertiser.couples)
        }

        userDetails.bind(avatarClickListener, imageLoader, fragment, advertiser.avatarUrl, fullName, firstName, lastName, description, extraSeekerInfo)
        userDetails.show()
    }

    private fun hideUserDetails() {
        userDetails.hide()
    }

    private fun setBudget(advert: AbstractAd, advertUtils: AdvertUtils, show: Boolean) {
        if (!show) {
            hideViews(txtAdvertBudgetLabel, txtAdvertBudget)
            return
        }

        showViews(txtAdvertBudgetLabel, txtAdvertBudget)

        if (advert is AdWanted) {
            txtAdvertBudgetLabel.text = monthlyBudget
            txtAdvertBudget.text = advertUtils.getFormattedMonthlyBudget(advert)
        } else {
            txtAdvertBudgetLabel.text = monthlyRent
            txtAdvertBudget.text = advertUtils.getFormattedMonthlyPrice(advert as AdOffered, false)
        }
    }

    private fun setAvailability(advert: AbstractAd, dateUtils: DateUtils, show: Boolean) {
        if (show) {
            showViews(txtAdvertAvailabilityLabel, txtAdvertAvailability)
            txtAdvertAvailability.text = dateUtils.getFormattedAvailabilityDate(advert.availableFrom)
        } else {
            hideViews(txtAdvertAvailabilityLabel, txtAdvertAvailability)
        }
    }

    private fun setDeposit(advert: AbstractAd, advertUtils: AdvertUtils, show: Boolean) {
        if (advert is AdWanted || !show) {
            hideViews(txtAdvertDepositLabel, txtAdvertDeposit)
        } else if (advert is AdOffered) {
            showViews(txtAdvertDepositLabel, txtAdvertDeposit)
            txtAdvertDeposit.text = advertUtils.getFormattedDeposit(advert)
        }
    }

    private fun setStatus(advert: AbstractAd, advertUtils: AdvertUtils) {
        if (advert.hasNewnessStatus()) {
            txtAdvertStatus.setBackgroundResource(R.drawable.rounded_corner_rect_grey)
            txtAdvertStatus.setTextColor(whiteFour)
            txtAdvertStatus.text = advertUtils.getFormattedNewnessStatus(advert)
            txtAdvertStatus.visibility = View.VISIBLE
        } else {
            txtAdvertStatus.visibility = View.GONE
        }
    }

    private fun setTitle(advert: AbstractAd, advertUtils: AdvertUtils) {
        txtAdvertTitle.text =
            if (advert is AdWanted) advertUtils.getFormattedWantedAdvertTitle(advert)
            else advertUtils.getFormattedOfferedAdvertTitle(advert as AdOffered)

        setUpTextView(txtAdvertTitle, false)
    }

    private fun setLocation(advert: AbstractAd, advertUtils: AdvertUtils) {
        val location =
            if (advert is AdOffered) advertUtils.getFormattedLocationForOfferedAdvert(advert)
            else advertUtils.getFormattedLocationForWantedAdvert(advert as AdWanted)

        setUpTextView(txtAdvertLocation, false)
        txtAdvertLocation.visibility = if (location.isNotBlank()) View.VISIBLE else View.GONE
        txtAdvertLocation.text = location
    }

    private fun loadImage(advert: AbstractAd, imageLoader: ImageLoader, fragment: Fragment, animation: Animation) {
        imageLoader.clear(imgAdvertPhoto)
        hideLoadingImage()

        val hasImage = advert.largePictureUrl.isNotBlank()
        imgAdvertNoPhoto.visibility = if (hasImage) View.GONE else View.VISIBLE
        imgAdvertPhoto.setBackgroundResource(R.drawable.advert_background)
        advertForeground.setBackgroundResource(R.drawable.advert_selector_transparent)

        if (hasImage) {
            showLoadingImage(animation)
            imageLoader.loadCenterCropImageFromFragment(advert.largePictureUrl, imgAdvertPhoto, object : ImageLoadingListener() {
                override fun onLoadingFailed(exception: Exception?) {
                    imageLoaded(false, requestId)
                }

                override fun onLoadingComplete() {
                    imageLoaded(true, requestId)
                }
            }, fragment, true)
        }
    }

    private fun imageLoaded(success: Boolean, requestId: UUID) {
        if (requestId == imgAdvertPhoto.getTag(R.id.request_id_tag)) {
            hideLoadingImage()
            imgAdvertPhoto.setBackgroundResource(if (success) 0 else R.drawable.advert_background)
            advertForeground.setBackgroundResource(if (success) R.drawable.advert_selector_gradient else R.drawable.advert_selector_transparent)

            txtAdvertStatus.setBackgroundResource(if (success) R.drawable.rounded_corner_rect_black else R.drawable.rounded_corner_rect_grey)
            txtAdvertStatus.setTextColor(if (success) white else whiteFour)

            setUpTextView(txtAdvertTitle, success)
            setUpTextView(txtAdvertLocation, success)
        }
    }

    private fun showLoadingImage(animation: Animation) {
        imgAdvertLoadingPhoto.visibility = View.VISIBLE
        imgAdvertLoadingPhoto.animation = animation
        animation.start()
    }

    private fun hideLoadingImage() {
        imgAdvertLoadingPhoto.clearAnimation()
        imgAdvertLoadingPhoto.visibility = View.GONE
    }

    private fun setUpTextView(txtTitle: TextView, activeState: Boolean) {
        txtTitle.setTextColor(if (activeState) white else grey)
    }

    private fun getColor(@ColorRes colorResourceId: Int): Int {
        return ContextCompat.getColor(context, colorResourceId)
    }

    private fun getString(@StringRes textResourceId: Int): String {
        return context.getString(textResourceId)
    }

}
