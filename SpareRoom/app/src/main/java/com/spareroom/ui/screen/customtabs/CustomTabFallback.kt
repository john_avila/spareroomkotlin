package com.spareroom.ui.screen.customtabs

import android.app.Activity
import android.net.Uri

interface CustomTabFallback {
    fun openUri(activity: Activity, uri: Uri)
}