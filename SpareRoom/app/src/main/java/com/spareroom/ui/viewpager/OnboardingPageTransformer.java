package com.spareroom.ui.viewpager;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.ui.controller.ViewUiController;
import com.spareroom.ui.screen.Activity;

import androidx.viewpager.widget.ViewPager;

/**
 * A PageTransformer is invoked whenever a visible/attached page is scrolled.
 * This offers an opportunity for the application to apply a custom transformation to the page views using animation properties.
 */
public class OnboardingPageTransformer implements ViewPager.PageTransformer {
    private boolean firstFragment = true;
    private final Activity activity;
    private final int lastPagePosition;
    private final LayerDrawable background;
    private final SkipButtonUiController skipButtonUiController;
    private final BeginButtonUiController beginButtonUiController;
    private final ChevronButtonUiController chevronButtonUiController;
    private final BottomToolBarController bottomToolBarController;

    public OnboardingPageTransformer(Activity activity, LayerDrawable background, int lastPagePosition) {
        this.background = background;
        this.activity = activity;
        this.lastPagePosition = lastPagePosition;

        // Set the alpha for the first time to all the backgrounds
        this.background.getDrawable(0).setAlpha(255);
        for (int i = 1; i <= lastPagePosition; i++)
            this.background.getDrawable(i).setAlpha(0);

        View bottomBar = activity.findViewById(R.id.onboarding_bottomToolbar);
        skipButtonUiController = new SkipButtonUiController(bottomBar);
        beginButtonUiController = new BeginButtonUiController(bottomBar);
        chevronButtonUiController = new ChevronButtonUiController(bottomBar);
        bottomToolBarController = new BottomToolBarController(skipButtonUiController, beginButtonUiController, chevronButtonUiController);

    }

    /**
     * Apply a property transformation to the given page.
     *
     * @param page     Apply the transformation to this page
     * @param position Position of page relative to the current front-and-center position of the pager. 0 is front and center. 1 is one
     *                 full page position to the right, and -1 is one page position to the left.
     */
    @Override
    public void transformPage(View page, float position) {
        int index = (Integer) page.getTag();

        setBackgroundFading(index, page, position);
        setBottomBarFading(index, position);
        setFirstAnimation(page);
    }

    /**
     * Set the background fading
     *
     * @param index    the current page position
     * @param page     the current page
     * @param position Position of page relative to the current front-and-center position of the pager. 0 is front and center. 1 is one
     *                 full page position to the right, and -1 is one page position to the left.
     */
    private void setBackgroundFading(int index, View page, float position) {
        Drawable currentDrawableInLayerDrawable = background.getDrawable(index);
        TextView tvTitle = page.findViewById(R.id.onboarding_title);
        TextView tvBody = page.findViewById(R.id.onboarding_bodyText);

        if (position <= -1 || position >= 1) {
            currentDrawableInLayerDrawable.setAlpha(0);
            tvTitle.setAlpha(0);
            tvBody.setAlpha(0);

        } else if (position == 0) {
            currentDrawableInLayerDrawable.setAlpha(255);
            tvTitle.setAlpha(1);
            tvBody.setAlpha(1);

            tvTitle.setX(tvBody.getX());
            bottomToolBarController.setPosition(index);

            if (index == lastPagePosition)
                beginButtonUiController.clearStatus();
            else
                beginButtonUiController.setLoading();

        } else {
            int gap = 5; // Gap between the two animations (title and body)

            currentDrawableInLayerDrawable.setAlpha((int) (255 - Math.abs(position * 255)));
            tvTitle.setAlpha(1 - Math.abs(position * 2));
            tvBody.setAlpha(1 - Math.abs(position * 2));

            tvTitle.setX(tvBody.getX() + (position * tvBody.getX() * gap));
            bottomToolBarController.setLoading();

        }
    }

    /**
     * Set the bottom bar buttons fading
     *
     * @param index    the current page position
     * @param position Position of page relative to the current front-and-center position of the pager. 0 is front and center. 1 is one
     *                 full page position to the right, and -1 is one page position to the left.
     */
    private void setBottomBarFading(int index, float position) {
        if (index == lastPagePosition) {
            beginButtonUiController.setAlpha(Math.max(0F, (0.5F - position) * 2F));
            chevronButtonUiController.setAlpha(Math.max(0F, (position - 0.5F) * 2F));
            skipButtonUiController.setAlpha(Math.max(0F, (position - 0.5F) * 2F));

        } else if (index == lastPagePosition - 1) {
            beginButtonUiController.enable();
            chevronButtonUiController.enable();
            skipButtonUiController.enable();

        }
    }

    /**
     * Set the first animation of the activity
     */
    private void setFirstAnimation(View page) {
        if (firstFragment) {
            firstFragment = false;

            Animation iconAnimation = AnimationUtils.loadAnimation(activity, R.anim.slide_in_from_bottom_delayed);
            Animation textAnimation = AnimationUtils.loadAnimation(activity, R.anim.slide_in_from_bottom_delayed);
            Animation barAnimation = AnimationUtils.loadAnimation(activity, R.anim.slide_in_from_bottom_delayed);
            View ivIcon = activity.findViewById(R.id.onBoarding_icon);
            View vBottomBar = activity.findViewById(R.id.onboarding_bottomToolbar);
            View carousel = activity.findViewById(R.id.onboarding_pagerCarousel);
            TextView tvTitle = page.findViewById(R.id.onboarding_title);
            TextView tvBody = page.findViewById(R.id.onboarding_bodyText);

            iconAnimation.setStartOffset(200);
            ivIcon.setAnimation(iconAnimation);
            barAnimation.setStartOffset(1050);
            vBottomBar.setAnimation(barAnimation);
            carousel.setAnimation(barAnimation);
            textAnimation.setStartOffset(800);
            tvTitle.setAnimation(textAnimation);
            tvBody.setAnimation(textAnimation);

        }

    }

    /**
     * Manages the behaviour of the bottom bar
     */
    private class BottomToolBarController implements ViewUiController {
        private final ViewUiController _skipUiController;
        private final ViewUiController _beginUiController;
        private final ViewUiController _chevronUiController;

        BottomToolBarController(
                SkipButtonUiController skipButtonUiController,
                BeginButtonUiController beginButtonUiController,
                ChevronButtonUiController chevronButtonUiController) {

            _skipUiController = skipButtonUiController;
            _beginUiController = beginButtonUiController;
            _chevronUiController = chevronButtonUiController;

        }

        /**
         * Set the status of all the views for the fragment position and enable the views
         *
         * @param position - fragment position
         */
        public void setPosition(int position) {

            if (position == lastPagePosition)
                setLastPosition();
            else
                clearStatus();

            enable();
        }

        @Override
        public void disable() {

        }

        /**
         * Disable all the views that have listeners
         */
        public void setLoading() {
            _skipUiController.setLoading();
            _beginUiController.setLoading();
            _chevronUiController.setLoading();
        }

        @Override
        public void setStatus(SpareroomStatus status) {

        }

        /**
         * Enable all the views that have listeners
         */
        @Override
        public void enable() {
            _skipUiController.clearStatus();
            _beginUiController.clearStatus();
            _chevronUiController.clearStatus();
        }

        /**
         * Set the visibilities when the last fragment is achieved
         */
        private void setLastPosition() {
            _skipUiController.disable();
            _beginUiController.enable();
            _chevronUiController.disable();
        }

        /**
         * Set the visibilities of all fragment positions but the last
         */
        @Override
        public void clearStatus() {
            _skipUiController.enable();
            _beginUiController.disable();
            _chevronUiController.enable();
        }

    }

    private class SkipButtonUiController implements ViewUiController {
        private final TextView _tvSkip;

        SkipButtonUiController(View toolBar) {
            _tvSkip = toolBar.findViewById(R.id.onboardingToolbar_skip);

        }

        @Override
        public void enable() {
            if (_tvSkip.getVisibility() == View.GONE)
                _tvSkip.setVisibility(View.VISIBLE);

        }

        @Override
        public void disable() {
            if (_tvSkip.getVisibility() == View.VISIBLE)
                _tvSkip.setVisibility(View.GONE);

        }

        @Override
        public void setLoading() {
            if (_tvSkip.isEnabled())
                _tvSkip.setEnabled(false);

        }

        @Override
        public void setStatus(SpareroomStatus status) {

        }

        @Override
        public void clearStatus() {
            if (!_tvSkip.isEnabled())
                _tvSkip.setEnabled(true);

        }

        void setAlpha(float alpha) {
            _tvSkip.setAlpha(alpha);
        }

    }

    private class BeginButtonUiController implements ViewUiController {
        private final TextView _tvBegin;

        BeginButtonUiController(View toolBar) {
            _tvBegin = toolBar.findViewById(R.id.onboardingToolbar_begin);
        }

        @Override
        public void enable() {
            if (_tvBegin.getVisibility() == View.GONE)
                _tvBegin.setVisibility(View.VISIBLE);

        }

        @Override
        public void disable() {
            if (_tvBegin.getVisibility() == View.VISIBLE)
                _tvBegin.setVisibility(View.GONE);

        }

        @Override
        public void setLoading() {
            if (_tvBegin.isEnabled())
                _tvBegin.setEnabled(false);

        }

        @Override
        public void setStatus(SpareroomStatus status) {

        }

        @Override
        public void clearStatus() {
            if (!_tvBegin.isEnabled())
                _tvBegin.setEnabled(true);

        }

        void setAlpha(float alpha) {
            _tvBegin.setAlpha(alpha);
        }

    }

    private class ChevronButtonUiController implements ViewUiController {
        private final ImageView _ivChevron;

        ChevronButtonUiController(View toolBar) {
            _ivChevron = toolBar.findViewById(R.id.onboardingToolbar_chevron);

        }

        @Override
        public void enable() {
            if (_ivChevron.getVisibility() == View.GONE)
                _ivChevron.setVisibility(View.VISIBLE);

        }

        @Override
        public void disable() {
            if (_ivChevron.getVisibility() == View.VISIBLE)
                _ivChevron.setVisibility(View.GONE);

        }

        @Override
        public void setLoading() {
            if (_ivChevron.isEnabled())
                _ivChevron.setEnabled(false);

        }

        @Override
        public void setStatus(SpareroomStatus status) {

        }

        @Override
        public void clearStatus() {
            if (!_ivChevron.isEnabled())
                _ivChevron.setEnabled(true);

        }

        void setAlpha(float alpha) {
            _ivChevron.setAlpha(alpha);
        }

    }

}