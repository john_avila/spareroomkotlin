package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.spareroom.R;
import com.spareroom.controller.legacy.IRetriableActivity;
import com.spareroom.ui.screen.Fragment;

public class NoConnectionFragment extends Fragment {

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_results_no_connection, container, false);
        Button bRetry = v.findViewById(R.id.activityResults_bNoConnection);
        bRetry.setOnClickListener(new RetryOnClickListener());
        return v;
    }

    private class RetryOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            ((IRetriableActivity) getActivity()).retry();
        }

    }

}
