package com.spareroom.ui.screen;

import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.lib.core.UiController;
import com.spareroom.ui.widget.CompoundButtonView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

/**
 * Fragment for set the type of account for the registration process.
 * Final fragment for the registration process.
 */
public class NewExistingFragment extends Fragment {

    //region FIELDS CONTROLLER

    public static final String EXTRA_ACTION_CODE = "extra_previous_activity";
    public static final String EXTRA_ACTION_CODE_CALL_ADVERTISER = "extra_action_call_advertiser";
    public static final String EXTRA_ACTION_CODE_MESSAGE_ADVERTISER = "extra_action_message_advert";
    public static final String EXTRA_ACTION_CODE_SAVE_AD = "extra_action_code_save_advert";
    public static final String EXTRA_ACTION_CODE_VIEW_MESSAGES = "extra_action_code_view_messages";
    public static final String EXTRA_ACTION_CODE_MY_ADVERTS = "extra_action_code_my_ads";
    public static final String EXTRA_ACTION_CODE_SAVED_ADVERTS = "extra_action_code_saved_ads";
    public static final String EXTRA_ACTION_CODE_UPGRADE = "extra_action_code_upgrade";
    public static final String EXTRA_ACTION_CODE_SAVED_SEARCHES = "extra_action_code_saved_searches";
    public static final String EXTRA_ACTION_CODE_PROFILE = "extra_action_code_profile";
    public static final String EXTRA_ACTION_CODE_ONBOARDING = "extra_action_code_onboarding";
    public static final String EXTRA_OTHER_USER_NAME = "extra_other_user_name";

    //endregion FIELDS CONTROLLER

    //region METHODS LIFECYCLE

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        View rootView = inflater.inflate(R.layout.material_new_existing_fragment, container, false);
        // Description
        TextView tvDescription = rootView.findViewById(R.id.newExistingFragment_explanation);
        // Renting box
        CompoundButtonView cbRegister = rootView.findViewById(R.id.newExistingFragment_registerCheckbox);
        // Searching box
        CompoundButtonView cbLogin = rootView.findViewById(R.id.newExistingFragment_loginCheckbox);
        // Bottom Toolbar
        View bottomToolbar = getActivity().findViewById(R.id.activity_toolbarBottom);
        TextView tvContinueButton = bottomToolbar.findViewById(R.id.materialToolbarBottom_rightText);
        // Controller
        ButtonUiController buttonUiController = new ButtonUiController(tvContinueButton);

        // Toolbar
        setToolbar(getActivity().findViewById(R.id.appBarLayout), getString(R.string.newExistingFragment_toolbarTitle), true, R.drawable.ic_close);

        // Bottom Toolbar
        bottomToolbar.setVisibility(View.VISIBLE);

        tvContinueButton.setOnClickListener(new ContinueButtonOnClickListener(cbLogin, cbRegister));
        tvContinueButton.setText(getString(R.string.newExistingFragment_bottomToolbar));
        tvContinueButton.setEnabled(false);

        // Renting and Searching boxes
        cbRegister.setOnCheckedChangeListener(new RegisterOnChangeListener(cbLogin, buttonUiController));
        cbLogin.setOnCheckedChangeListener(new LoginOnChangeListener(cbRegister, buttonUiController));

        Intent intent = getActivity().getIntent();
        String activity = intent.getStringExtra(EXTRA_ACTION_CODE);
        String otherUserName;

        if (activity != null)
            switch (activity) {
                case EXTRA_ACTION_CODE_CALL_ADVERTISER:
                    otherUserName = intent.getStringExtra(EXTRA_OTHER_USER_NAME);
                    if (otherUserName != null)
                        tvDescription.setText(
                                String.format(
                                        "%s %s. %s",
                                        getString(R.string.newExistingFragment_explanation_fromCallAdvertiser),
                                        otherUserName,
                                        getString(R.string.newExistingFragment_explanation_end)));
                    break;
                case EXTRA_ACTION_CODE_MESSAGE_ADVERTISER:
                    otherUserName = intent.getStringExtra(EXTRA_OTHER_USER_NAME);
                    if (otherUserName != null)
                        tvDescription.setText(
                                String.format(
                                        "%s %s. %s",
                                        getString(R.string.newExistingFragment_explanation_fromMessageAdvertiser),
                                        otherUserName,
                                        getString(R.string.newExistingFragment_explanation_end)));
                    break;
                case EXTRA_ACTION_CODE_VIEW_MESSAGES:
                    tvDescription.setText(R.string.newExistingFragment_explanation_fromViewMessages);
                    break;
                case EXTRA_ACTION_CODE_UPGRADE:
                    tvDescription.setText(R.string.newExistingFragment_explanation_fromUpgrade);
                    break;
                case EXTRA_ACTION_CODE_MY_ADVERTS:
                    tvDescription.setText(R.string.newExistingFragment_explanation_fromMyAdverts);
                    break;
                case EXTRA_ACTION_CODE_PROFILE:
                    tvDescription.setText(R.string.newExistingFragment_explanation_fromProfile);
                    break;
                case EXTRA_ACTION_CODE_SAVE_AD:
                    tvDescription.setText(R.string.newExistingFragment_explanation_fromSaveAdvert);
                    break;
                case EXTRA_ACTION_CODE_SAVED_ADVERTS:
                    tvDescription.setText(R.string.newExistingFragment_explanation_fromSavedAdverts);
                    break;
                case EXTRA_ACTION_CODE_SAVED_SEARCHES:
                    tvDescription.setText(R.string.newExistingFragment_explanation_fromSavedSearches);
                    break;
                case EXTRA_ACTION_CODE_ONBOARDING:
                    tvDescription.setText(R.string.newExistingFragment_explanation_fromOnboarding);
                    break;
                default:
                    break;
            }

        buttonUiController.reset();

        return rootView;
    } // end onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    private class ButtonUiController extends UiController {
        private final TextView _tvContinueButton;

        ButtonUiController(TextView tvContinueButton) {
            _tvContinueButton = tvContinueButton;
        }

        @Override
        public void reset() {
            super.reset();
            _tvContinueButton.setEnabled(false);
            _tvContinueButton.setTextColor(ContextCompat.getColor(_tvContinueButton.getContext(), R.color.light_blue));
        }

        @Override
        public void setLoading() {
            super.setLoading();
            _tvContinueButton.setEnabled(true);
            _tvContinueButton.setTextColor(ContextCompat.getColor(_tvContinueButton.getContext(), R.color.white));

        }

    } //end class ButtonUIController

    private class ContinueButtonOnClickListener implements View.OnClickListener {
        private final CompoundButtonView _tbLogin;
        private final CompoundButtonView _tbRegister;

        private ContinueButtonOnClickListener(CompoundButtonView tbLogin, CompoundButtonView tbRegister) {
            _tbLogin = tbLogin;
            _tbRegister = tbRegister;

        }

        @Override
        public void onClick(View v) {
            Intent intent = ((NewExistingActivity) getActivity()).getIntentWithExtras();
            intent.putExtra(EXTRA_ACTION_CODE, getActivity().getIntent().getStringExtra(EXTRA_ACTION_CODE));

            if (_tbLogin.isChecked()) {
                intent.setClass(getActivity(), LoginActivity.class);
                getActivity().startActivityForResult(intent, NewExistingActivity.REQUEST_CODE_LOGIN);

            } else if (_tbRegister.isChecked()) {
                intent.setClass(getActivity(), RegisterActivity.class);
                getActivity().startActivityForResult(intent, NewExistingActivity.REQUEST_CODE_REGISTER);

            } else {
                Animation shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake_to_left);
                v.startAnimation(shake);

            }
        }

    } //end class RegisterNameButtonOnClickListener

    private class RegisterOnChangeListener implements CompoundButtonView.OnCheckedChangeListener {
        private final CompoundButtonView _loginCheckBox;
        private final UiController _buttonUIController;

        private RegisterOnChangeListener(CompoundButtonView loginCheckBox, UiController buttonUIController) {
            _loginCheckBox = loginCheckBox;
            _buttonUIController = buttonUIController;
        }

        @Override
        public void onCheckedChangeEvent(boolean isChecked) {

            if (isChecked) {
                _loginCheckBox.setChecked(false);
                _buttonUIController.setLoading();

            } else {
                _buttonUIController.reset();

            }

        }

    } //end class RentingChangeListener

    private class LoginOnChangeListener implements CompoundButtonView.OnCheckedChangeListener {
        private final CompoundButtonView _registerCheckBox;
        private final UiController _buttonUIController;

        private LoginOnChangeListener(CompoundButtonView registerCheckBox, UiController buttonUIController) {
            _registerCheckBox = registerCheckBox;
            _buttonUIController = buttonUIController;
        }

        @Override
        public void onCheckedChangeEvent(boolean isChecked) {

            if (isChecked) {
                _registerCheckBox.setChecked(false);
                _buttonUIController.setLoading();

            } else {
                _buttonUIController.reset();

            }

        }

    } //end class SearchingChangeListener

    //endregion INNER CLASSES

}
