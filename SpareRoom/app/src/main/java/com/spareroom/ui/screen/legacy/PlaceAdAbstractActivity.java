package com.spareroom.ui.screen.legacy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.controller.legacy.AsyncTaskManager;
import com.spareroom.model.business.*;
import com.spareroom.model.legacy.IAccountFacade;
import com.spareroom.ui.anim.ProgressbarAnim;
import com.spareroom.ui.controller.ErrorStateController;
import com.spareroom.ui.flow.*;
import com.spareroom.ui.screen.Fragment;
import com.spareroom.ui.screen.InjectableActivity;
import com.spareroom.ui.util.SnackBarUtils;

import java.io.IOException;

import androidx.fragment.app.FragmentTransaction;

public abstract class PlaceAdAbstractActivity extends InjectableActivity {

    //region FIELDS UI

    private View _vForward;
    private View _vForwardEmpty;

    private TextView _tvTitle;

    protected TextView _tvProgress;
    private int _screenWidth;
    private View _vProgress;
    private View _vProgressTag;
    private View _vTipProgress;
    private View _vProgressTagSpace;

    //endregion FIELDS  UI

    //region FIELDS UI CONTROL

    public static final String BUNDLE_KEY_FORM_ID = "form_id";
    public static final String BUNDLE_VALUE_FORM_ID_PLACE = "place";
    public static final String BUNDLE_VALUE_FORM_ID_EDIT = "edit";

    private static final int ANIMATION_TIME = 500;

    /**
     * current progress for the screen that is currently displayed
     */
    protected int _currentProgress = 0;
    /**
     * name of the fragment currently displayed, null when no fragment displayed (init)
     */
    protected String _currentFragmentName;
    /**
     * name of the next fragment to be displayed
     */
    protected String _nextFragmentName;
    /**
     * name fo the last fragment to be previously filled out. This is used to calculate progress.
     */
    protected String _ghostFragmentName;

    //endregion FIELDS UI CONTROL

    // region FIELDS CONTROLLER

    private IAccountFacade _af;
    protected AsyncTaskManager _asyncManager;
    protected PlaceAdFlowManager _manager;
    protected DraftAd _draft;

    // endregion FIELDS CONTROLLER

    //region METHODS LIFECYCLE

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_place_ad);

        _screenWidth = getWindowManager().getDefaultDisplay().getWidth();

        findViewById(R.id.imgClosePlaceAd).setOnClickListener(v -> finish());

        _vForward = findViewById(R.id.activity_place_ad_ivForward);
        _vForwardEmpty = findViewById(R.id.activity_place_ad_ivForwardEmpty);
        _tvProgress = findViewById(R.id.activity_place_ad_tvProgress);
        _tvTitle = findViewById(R.id.activity_place_ad_tvTitle);
        _vProgress = findViewById(R.id.activity_place_ad_vProgress);
        _vProgressTag = findViewById(R.id.activity_place_ad_llProgressTag);
        _vTipProgress = _vProgressTag.findViewById(R.id.activity_place_ad_tip_progress);
        _vProgressTagSpace = findViewById(R.id.activity_place_ad_vProgressTagSpace);

        _vForward.setOnClickListener(new ForwardOnClickListener());

        _asyncManager = new AsyncTaskManager();
        _af = SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade();
        _manager = new PlaceAdFlowManager();

    } //end onCreate(Bundle arg0)

    @Override
    public void onBackPressed() {
        int currentProgress = _manager.getProgress(_currentFragmentName);

        if (currentProgress == 0) {
            super.onBackPressed();
            return;
        }

        _nextFragmentName = _manager.getPreviousFragmentName(_currentFragmentName, _draft);

        if (_nextFragmentName != null) {
            changeBackwardProgress(_manager.getProgress(_nextFragmentName));
            startScreen(false);
        } else {
            super.onBackPressed();
        }

    } //end onBackPressed()

    //endregion METHODS LIFECYCLE

    //region METHODS CONTROLLER

    protected DraftAd loadDraft() throws IOException {
        return _af.loadDraftAd(getApplicationContext());
    }

    protected void saveDraft(DraftAd draft) throws IOException {
        _af.saveDraftAd(getApplicationContext(), draft);
    }

    public DraftAd getDraftAd() {
        return _draft;
    }

    public void setDraftAd(DraftAd draft) {
        _draft = draft;

        if (SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext().getSession() == null) {
            if (draft instanceof DraftAdOffered)
                _manager.setState(new PlaceAdStateLoggedOutOffered());
            if (draft instanceof DraftAdWanted)
                _manager.setState(new PlaceAdStateLoggedOutWanted());
        } else {
            if (draft instanceof DraftAdOffered)
                _manager.setState(new PlaceAdStateLoggedInOffered());
            if (draft instanceof DraftAdWanted)
                _manager.setState(new PlaceAdStateLoggedInWanted());
        }

    } //end setDraftAd(DraftAd draft)

    protected abstract void screenFinished();

    protected void previousScreen() {

        hideKeyboard();

        _nextFragmentName = _manager.getPreviousFragmentName(_currentFragmentName, _draft);
        if (_nextFragmentName != null) {
            changeBackwardProgress(_manager.getProgress(_nextFragmentName));
            startScreen(false);
        }

    } //end previousScreen()

    protected void nextScreen() {

        hideKeyboard();

        _nextFragmentName = _manager.getNextFragmentName(_currentFragmentName, _draft);
        if (_nextFragmentName != null) {
            changeForwardProgress(_manager.getProgress(_nextFragmentName));
            startScreen(true);
        }

    } //end nextScreen()

    /**
     * Handles an error raised by the server when posting the advert
     *
     * @param im <code>SpareroomStatus</code> containing the name of the screen that created the
     *           issue as cause (see <code>im.getCause()</code>)
     */
    protected void handleError(SpareroomStatus im) {
        Toast.makeText(this, im.getMessage(), Toast.LENGTH_LONG).show();
        if (ErrorStateController.isErrorCausedByUserDetails(im.getCause())) {
            SnackBarUtils.show(_tvTitle, getString(R.string.update_details), getString(R.string.update),
                    v -> startActivity(new Intent(PlaceAdAbstractActivity.this, EditProfileActivity.class)), true);
        }

        String errorScreen = _manager.nextErrorScreen(im.getCause());
        if (errorScreen == null) {
            previousScreen();
            return;
        }

        _nextFragmentName = errorScreen;
        changeBackwardProgress(_manager.getProgress(_nextFragmentName));
        startScreen(false);

    }

    //endregion METHODS CONTROLLER

    //region METHODS UI

    public abstract String getId();

    protected void startScreen(boolean isForward) {
        _currentFragmentName = _nextFragmentName;
        _nextFragmentName = null;

        replaceFragment(isForward);
    }

    private void replaceFragment(boolean isForward) {
        try {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (isForward)
                transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            else
                transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);

            Bundle metaData = new Bundle();
            metaData.putString(BUNDLE_KEY_FORM_ID, getId());
            Fragment newFragment = (Fragment) Class.forName(_currentFragmentName).newInstance();
            newFragment.setArguments(metaData);

            transaction.replace(R.id.activity_place_ad_flContent, newFragment);
            transaction.commit();
        } catch (InstantiationException e) {
            // another empty catch
        } catch (IllegalAccessException e) {
            // another empty catch
        } catch (ClassNotFoundException e) {
            // another empty catch
        }
    }

    protected void hideKeyboard() {

        if (getCurrentFocus() != null) {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void changeBackwardProgress(int newProgress) {
        int finalPixel = (int) (_screenWidth * ((float) newProgress / (float) 100));

        _currentProgress = _manager.getProgress(_currentFragmentName);

        animateBackwardProgress(finalPixel);
        animateBackwardProgressCount(_currentProgress, newProgress);

        changeNavigationProgress();
    }

    protected void changeForwardProgress(int newProgress) {
        int finalPixel = (int) (_screenWidth * ((float) newProgress / (float) 100));

        _currentProgress = _manager.getProgress(_currentFragmentName);

        animateForwardProgress(finalPixel);
        animateForwardProgressCount(_currentProgress, newProgress);
        _currentProgress = newProgress;

        changeNavigationProgress();
    }

    protected void enableForwardNavigation() {
        _vForward.setVisibility(View.VISIBLE);
        _vForwardEmpty.setVisibility(View.GONE);
    }

    protected void disableForwardNavigation() {
        _vForward.setVisibility(View.INVISIBLE);
        _vForwardEmpty.setVisibility(View.VISIBLE);
    }

    protected void changeNavigationProgress() {
        if (_nextFragmentName != null && _ghostFragmentName != null)
            if (_nextFragmentName.equals(_ghostFragmentName)) {
                disableForwardNavigation();
            } else {
                enableForwardNavigation();
            }
    }

    protected void animateForwardProgress(int finalPixel) {
        ProgressbarAnim anim = new ProgressbarAnim(_vProgress, finalPixel);
        anim.setDuration(ANIMATION_TIME);
        _vProgress.startAnimation(anim);

        _vProgressTag.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        int _vProgressTagWidth = _vProgressTag.getMeasuredWidth();

        if ((_vProgressTagWidth + finalPixel) > _screenWidth) {// Progress tag (%) is pushed outside the screen
            ProgressbarAnim anim2 = new ProgressbarAnim(_vProgressTagSpace, (finalPixel - _vProgressTagWidth - 20));
            anim2.setDuration(ANIMATION_TIME);
            _vProgressTagSpace.startAnimation(anim2);
            _vTipProgress.setVisibility(View.INVISIBLE);
        } else {
            ProgressbarAnim anim2 = new ProgressbarAnim(_vProgressTagSpace, finalPixel);
            anim2.setDuration(ANIMATION_TIME);
            _vProgressTagSpace.startAnimation(anim2);
            _vTipProgress.setVisibility(View.VISIBLE);
        }
    }

    private void animateBackwardProgress(int finalPixel) {
        ProgressbarAnim anim = new ProgressbarAnim(_vProgress, finalPixel);
        anim.setDuration(ANIMATION_TIME);
        _vProgress.startAnimation(anim);

        _vProgressTag.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        int _vProgressTagWidth = _vProgressTag.getMeasuredWidth();

        if ((_vProgressTagWidth + finalPixel) <= 0) {
            ProgressbarAnim anim2 = new ProgressbarAnim(_vProgressTagSpace, (finalPixel - _vProgressTagWidth - 20));
            anim2.setDuration(ANIMATION_TIME);
            _vProgressTagSpace.startAnimation(anim2);
            _vTipProgress.setVisibility(View.INVISIBLE);
        } else {
            ProgressbarAnim anim2 = new ProgressbarAnim(_vProgressTagSpace, finalPixel);
            anim2.setDuration(ANIMATION_TIME);
            _vProgressTagSpace.startAnimation(anim2);
            _vTipProgress.setVisibility(View.VISIBLE);
        }
    }

    protected void animateForwardProgressCount(int fromProgress, int toProgress) {
        final int originalProgress = fromProgress;
        final int finalProgress = toProgress;
        final float updateWait = (float) ANIMATION_TIME / (float) (toProgress - originalProgress);

        new Thread(new Runnable() {
            private int currentProgress = originalProgress;

            public void run() {
                while (currentProgress < finalProgress) {
                    currentProgress++;

                    try {
                        Thread.sleep((long) updateWait);
                    } catch (InterruptedException e) {
                        // another empty catch
                    }

                    _tvProgress.post(new ProgressCountUpdater(currentProgress));
                }
            }
        }).start();
    }

    private void animateBackwardProgressCount(int fromProgress, int toProgress) {
        final int originalProgress = fromProgress;
        final int finalProgress = toProgress;
        final float updateWait = (float) ANIMATION_TIME / (float) (originalProgress - toProgress);

        new Thread(new Runnable() {
            private int currentProgress = originalProgress;

            public void run() {
                while (currentProgress > finalProgress) {
                    currentProgress--;

                    try {
                        Thread.sleep((long) updateWait);
                    } catch (InterruptedException e) {
                        // another empty catch
                    }

                    _tvProgress.post(new ProgressCountUpdater(currentProgress));
                }
            }
        }).start();
    }

    protected void setTitle(String title) {
        if (title != null)
            _tvTitle.setText(title);
    }

    //endregion METHODS UI

    //region INNER CLASSES

    private class ForwardOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            nextScreen();
        }

    }

    private class ProgressCountUpdater implements Runnable {
        private final int _progress;

        ProgressCountUpdater(int progress) {
            this._progress = progress;
        }

        public void run() {
            _tvProgress.setText(String.format(AppVersion.flavor().getLocale(), "%d%%", _progress));
        }
    }

    //endregion INNER CLASSES

}
