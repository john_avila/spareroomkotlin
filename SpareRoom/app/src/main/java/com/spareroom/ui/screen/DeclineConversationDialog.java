package com.spareroom.ui.screen;

import android.app.Dialog;
import android.os.Bundle;

import com.spareroom.R;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.viewmodel.ConversationViewModel;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

public class DeclineConversationDialog extends DialogFragment implements Injectable {

    public static final String TAG = DeclineConversationDialog.class.getName() + "Tag";

    private static final String USER_NAME_KEY = "USERNAME_KEY";

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private ConversationViewModel viewModel;

    public static DeclineConversationDialog getInstance(String userName) {
        Bundle args = new Bundle();
        args.putString(USER_NAME_KEY, userName);

        DeclineConversationDialog dialogFragment = new DeclineConversationDialog();
        dialogFragment.setArguments(args);

        return dialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(ConversationViewModel.class);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        final String userName = args.getString(USER_NAME_KEY);

        AlertDialogBuilder builder = new AlertDialogBuilder(getActivity());
        builder.setTitle(R.string.are_you_sure);
        builder.setMessage(getString(R.string.fragmentConversation_declineMessage, userName));
        builder.setPositiveButton(R.string.decline, (dialog, which) -> {
            viewModel.getDeclineConversationConfirmation().decline();
            dismiss();
        });

        builder.setNegativeButton(R.string.cancel, (dialog, which) -> dismiss());

        return builder.create();
    }
}
