package com.spareroom.ui.widget.params;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.spareroom.R;
import com.spareroom.ui.filters.adapter.viewparams.IResettableParams;

import androidx.core.content.ContextCompat;

public class CompoundButtonParams implements IResettableParams {
    private final static int DEFAULT_BACKGROUND_RES_ID = R.drawable.material_borderless_ripple_black;
    private final static int DEFAULT_BLACK = R.color.material_black;
    private final static int DEFAULT_GRAY = R.color.grey_2;

    private String tag;

    private Size size = CompoundButtonParams.Size.SMALL;
    private CompoundButtonType compoundButtonType = CompoundButtonParams.CompoundButtonType.CHECK_BOX;
    private CompoundButtonColorStateList compoundButtonColorStateList = CompoundButtonParams.CompoundButtonColorStateList.WHITE;
    private String text;
    private String subtitle;
    private String value;
    private int textColorNotSelected;
    private int textColor;
    private int subtitleColor = DEFAULT_GRAY;
    private int srcIcon;
    private int iconColor = DEFAULT_GRAY;
    private int iconNotSelectedColor = DEFAULT_GRAY;
    private int backgroundResId = DEFAULT_BACKGROUND_RES_ID;
    private boolean highlightBarVisible;
    private boolean selected;

    public enum Size {
        SMALL(R.dimen.material_widgetCompoundButton_minHeight_small, R.dimen.material_widgetCompoundButton_iconSize_small, R.dimen.material_widgetCompoundButton_text_size_small),
        MEDIUM(R.dimen.material_widgetCompoundButton_minHeight_medium, R.dimen.icon_size, R.dimen.material_widgetCompoundButton_text_size_medium);

        private final int minHeightResId;
        private final int iconSizeResId;
        private final int textSizeResId;

        Size(int minHeightResId, int iconSizeResId, int textSizeResId) {
            this.minHeightResId = minHeightResId;
            this.iconSizeResId = iconSizeResId;
            this.textSizeResId = textSizeResId;

        }

        public int getMinHeightResId() {
            return this.minHeightResId;
        }

        public int getIconSizeResId() {
            return this.iconSizeResId;
        }

        public int getTextSizeResId() {
            return this.textSizeResId;
        }

    }

    public enum CompoundButtonType {
        CHECK_BOX(R.layout.material_widget_compound_button_checkbox),
        RADIO_BUTTON(R.layout.material_widget_compound_button_radiobutton);

        private int layoutResId;

        CompoundButtonType(int layoutResId) {
            this.layoutResId = layoutResId;
        }

        public int getLayoutResId() {
            return this.layoutResId;
        }

    }

    public enum CompoundButtonColorStateList {
        WHITE(R.color.material_compound_button_selector_white),
        ORANGE(R.color.material_compound_button_selector_orange),
        GRAY(R.color.material_compound_button_selector_gray);

        private int colorStateList;

        CompoundButtonColorStateList(int colorStateList) {
            this.colorStateList = colorStateList;
        }

        public int getColorStateList() {
            return this.colorStateList;
        }

    }

    public CompoundButtonParams() {
    }

    public CompoundButtonParams(String tag) {
        this.tag = tag;
    }

    @Override
    public ViewParamsType type() {
        return ViewParamsType.COMPOUND;
    }

    @Override
    public String tag() {
        return tag;
    }

    @Override
    public void reset() {
        selected = false;
    }

    public Size size() {
        return size;
    }

    public void size(Size size) {
        this.size = size;
    }

    public CompoundButtonType compoundButtonType() {
        return compoundButtonType;
    }

    public void compoundButtonType(CompoundButtonType compoundButtonType) {
        this.compoundButtonType = compoundButtonType;
    }

    public CompoundButtonColorStateList compoundButtonColorStateList() {
        return compoundButtonColorStateList;
    }

    public void compoundButtonColorStateList(CompoundButtonColorStateList compoundButtonColorStateList) {
        this.compoundButtonColorStateList = compoundButtonColorStateList;
    }

    public String text() {
        return text;
    }

    public void text(String text) {
        this.text = text;
    }

    public String subtitle() {
        return subtitle;
    }

    public void subtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String value() {
        return value;
    }

    public void value(String value) {
        this.value = value;
    }

    public int textColorNotSelected(Context context) {
        return ContextCompat.getColor(context, (textColorNotSelected == 0) ? DEFAULT_BLACK : textColorNotSelected);
    }

    public void textColorNotSelectedResId(int textColorNotSelected) {
        this.textColorNotSelected = textColorNotSelected;
    }

    public int textColor(Context context) {
        return ContextCompat.getColor(context, (textColor == 0) ? DEFAULT_BLACK : textColor);
    }

    public void textColorResId(int textColor) {
        this.textColor = textColor;
    }

    public int subtitleColor(Context context) {
        return ContextCompat.getColor(context, (subtitleColor == 0) ? DEFAULT_GRAY : subtitleColor);
    }

    public void subtitleColorResId(int subtitleColor) {
        this.subtitleColor = subtitleColor;
    }

    public Drawable icon(Context context) {
        return (srcIcon == 0) ? null : ContextCompat.getDrawable(context, srcIcon);
    }

    public void iconResId(int srcIcon) {
        this.srcIcon = srcIcon;
    }

    public int iconColor() {
        return iconColor;
    }

    public void iconColor(int iconColor) {
        this.iconColor = iconColor;
    }

    public int iconNotSelectedColor() {
        return iconNotSelectedColor;
    }

    public void iconNotSelectedColor(int iconNotSelectedColor) {
        this.iconNotSelectedColor = iconNotSelectedColor;
    }

    public int backgroundResId() {
        return backgroundResId;
    }

    public void backgroundResId(int backgroundResId) {
        this.backgroundResId = backgroundResId;
    }

    public boolean highlightBarVisible() {
        return highlightBarVisible;
    }

    public void enableHighlightBar(boolean highlightBarVisible) {
        this.highlightBarVisible = highlightBarVisible;
    }

    public boolean selected() {
        return selected;
    }

    public void selected(boolean selected) {
        this.selected = selected;
    }

}
