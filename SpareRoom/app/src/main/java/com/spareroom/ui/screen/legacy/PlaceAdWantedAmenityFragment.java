package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.model.business.PreferenceList;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

/**
 * Wanted amenities Fragment
 */
public class PlaceAdWantedAmenityFragment extends PlaceAdFragment {

    //region FIELDS CONTROLLER

    private DraftAdWanted _draftAd;
    private PreferenceList _pl;

    //endregion FIELDS CONTROLLER

    //region METHODS LIFECYCLE

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.place_ad_wanted_amenity, container, false);

        setTitle(getString(R.string.place_ad_wanted_amenity_title));

        ListView _lvAreas = rootView.findViewById(R.id.place_ad_wanted_amenity_lv);
        Button _bConfirm = rootView.findViewById(R.id.place_ad_bConfirm);

        _draftAd = (DraftAdWanted) getDraftAd();

        // In case there is an activity state change and we lost the draft
        if (_draftAd == null) {
            getActivity().finish();

            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }

        _pl = _draftAd.getAmenitiesList();

        _lvAreas.setAdapter(new ContentAdapter());
        _bConfirm.setOnClickListener(v -> {
            _draftAd.setAmenitiesList(_pl);
            finish();
        });

        return rootView;
    }

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    private class ContentAdapter extends BaseAdapter {

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.place_ad_amenity_option, parent, false);
            }

            ((TextView) convertView.findViewById(R.id.place_ad_amenity_option_tvOption)).setText(_pl.get(position).getName());

            if (_pl.get(position).isChecked()) {
                select(convertView);
            } else {
                unselect(convertView);
            }

            convertView.setOnClickListener(new OnRowClickListener(position));

            return convertView;
        } //end getView(int position, View convertView, ViewGroup parent)

        private void unselect(View row) {
            View vRow = row.findViewById(R.id.place_ad_amenity_option_rl);
            View vIcon = row.findViewById(R.id.place_ad_amenity_option_ivChosen);
            vRow.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
            vIcon.setVisibility(View.INVISIBLE);
        } //end unselect(View row)

        private void select(View row) {
            View vRow = row.findViewById(R.id.place_ad_amenity_option_rl);
            View vIcon = row.findViewById(R.id.place_ad_amenity_option_ivChosen);
            vRow.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.light_blue_2));
            vIcon.setVisibility(View.VISIBLE);
        } //end select(View row)

        @Override
        public long getItemId(int position) {
            return _pl.get(position).getId();
        }

        @Override
        public Object getItem(int position) {
            return _pl.get(position);
        }

        @Override
        public int getCount() {
            return _pl.size();
        }

        private class OnRowClickListener implements View.OnClickListener {
            private final int _position;

            OnRowClickListener(int position) {
                _position = position;
            }

            @Override
            public void onClick(View v) {
                if (_pl.get(_position).isChecked()) {
                    unselect(v);
                    _pl.get(_position).setChecked(false);
                } else {
                    select(v);
                    _pl.get(_position).setChecked(true);
                }
            } //end onClick(View v)

        } //end class OnRowClickListener

    } //end class ContentAdapter

    //endregion INNER CLASSES

}
