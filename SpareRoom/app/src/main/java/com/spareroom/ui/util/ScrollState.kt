package com.spareroom.ui.util

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ScrollState {

    private var selectedPosition = 0
    private var scrollOffset = 0
    var canScrollToTheTop = true

    fun restore(layoutManager: LinearLayoutManager) {
        layoutManager.scrollToPositionWithOffset(selectedPosition, scrollOffset)
    }

    fun save(layoutManager: LinearLayoutManager, recyclerView: RecyclerView) {
        selectedPosition = layoutManager.findFirstVisibleItemPosition()
        scrollOffset = getFirstVisibleItemVerticalOffset(recyclerView)
    }

    private fun getFirstVisibleItemVerticalOffset(recyclerView: RecyclerView): Int {
        val firstView = recyclerView.getChildAt(0)
        return if (firstView == null) 0 else firstView.top - recyclerView.paddingTop
    }
}