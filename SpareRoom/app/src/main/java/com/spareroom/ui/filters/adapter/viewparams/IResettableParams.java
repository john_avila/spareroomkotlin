package com.spareroom.ui.filters.adapter.viewparams;

import com.spareroom.ui.widget.params.ViewParamsType;

public interface IResettableParams {
    void reset();

    String tag();

    ViewParamsType type();
}
