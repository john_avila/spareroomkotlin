package com.spareroom.ui.filters.provider;

import android.content.Context;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListProperties.*;
import com.spareroom.ui.util.DateUtils;
import com.spareroom.ui.util.StringUtils;

import java.util.Calendar;

import javax.inject.Inject;

import androidx.annotation.Nullable;

public class SubtitleUtil {
    private final String SPACE = " ";
    private final String EMPTY_STRING = "";

    private final DateUtils dateUtils;

    @Inject
    public SubtitleUtil(DateUtils dateUtils) {
        this.dateUtils = dateUtils;
    }

    String getRentAmountText(Context context, Integer minRent, Integer maxRent) {
        if ((minRent != null) && (maxRent != null)) {
            return // "Between £140 - £230"
                    context.getString(R.string.between)
                            + SPACE
                            + context.getString(R.string.currency_symbol)
                            + minRent
                            + context.getString(R.string.en_dash)
                            + context.getString(R.string.currency_symbol)
                            + maxRent;

        } else if ((minRent != null) || (maxRent != null)) {
            return // "From £390" || "Up to £150"
                    context.getString((minRent == null) ? R.string.upTo : R.string.from)
                            + SPACE
                            + context.getString(R.string.currency_symbol)
                            + ((minRent == null) ? maxRent : minRent);

        } else {
            return EMPTY_STRING;
        }

    }

    public String getMoveInText(@Nullable String availableFrom) {
        if (availableFrom == null)
            return EMPTY_STRING;

        final Calendar calendar = dateUtils.getCalendar(DateUtils.YEAR_MONTH_DAY_FORMAT, availableFrom);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        // "Move in on 9 February 2018"
        return day + SPACE + dateUtils.getMonthName(month) + SPACE + year;

    }

    String getLengthOfStayText(Context context, Integer min, Integer max) {
        if ((min != null) && (max != null)) {
            final int monthsInAYear = 12;

            if (min < monthsInAYear && max < monthsInAYear) {
                return context.getString(R.string.between) + SPACE + min + context.getString(R.string.en_dash) + getTermText(context, max);
            } else if (min % monthsInAYear == 0 && max % monthsInAYear == 0) {
                return context.getString(R.string.between) + SPACE + min / monthsInAYear + context.getString(R.string.en_dash) + getTermText(context, max);
            } else {
                return context.getString(R.string.between) + SPACE + getTermText(context, min) + context.getString(R.string.en_dash) + getTermText(context, max);
            }

        } else if ((min != null) || (max != null)) {
            return // "From 3 months" || "Up to 9 months"
                    context.getString((min != null) ? R.string.from : R.string.upTo)
                            + SPACE + ((min != null) ? getTermText(context, min) : getTermText(context, max));
        } else {
            return EMPTY_STRING;
        }

    }

    String getRoomTypeText(Context context, SearchAdvertListProperties.RoomTypes roomType) {
        if (roomType == RoomTypes.SINGLE)
            return context.getString(R.string.singleBedroom);
        else if (roomType == RoomTypes.DOUBLE)
            return context.getString(R.string.doubleBedroom);
        else
            return EMPTY_STRING;
    }

    String getGenderFilterText(Context context, GenderFilter genderFilter) {
        if (genderFilter == GenderFilter.FEMALES)
            return context.getString(R.string.females);
        else if (genderFilter == GenderFilter.MALES)
            return context.getString(R.string.males);
        else if (genderFilter == GenderFilter.MIXED)
            return context.getString(R.string.mixed);
        else
            return EMPTY_STRING;
    }

    String getAgeText(Context context, Integer minAge, Integer maxAge) {
        if ((minAge != null) && (maxAge != null)) {
            // "Between 18 - 24 years"
            return context.getString(R.string.between) + SPACE + minAge + context.getString(R.string.en_dash) + maxAge + SPACE + context.getResources().getQuantityString(R.plurals.years, maxAge);
        } else if ((minAge != null) || (maxAge != null)) {
            // "From 18 years" || "Up to 24 years"
            Integer ageToShow = (maxAge == null) ? minAge : maxAge;
            return context.getString((maxAge == null) ? R.string.from : R.string.upTo) + SPACE + ageToShow + SPACE + context.getResources().getQuantityString(R.plurals.years, ageToShow);
        } else {
            return EMPTY_STRING;
        }

    }

    String getEmploymentStatusText(Context context, ShareType shareType) {
        if (shareType == ShareType.PROFESSIONALS)
            return context.getString(R.string.professionals);
        else if (shareType == ShareType.STUDENTS)
            return context.getString(R.string.students);
        else
            return EMPTY_STRING;
    }

    public String getTermText(Context context, int totalInMonths) {
        String term = EMPTY_STRING;
        int years = totalInMonths / 12;
        int months = totalInMonths % 12;

        // 1 year 1 month
        if (years != 0)
            term = years + SPACE + context.getResources().getQuantityString(R.plurals.years, years);
        if ((years != 0) && (months != 0))
            term = term + SPACE;
        if (months != 0)
            term = term + months + SPACE + context.getResources().getQuantityString(R.plurals.months, months);

        return term;
    }

    public String getFlatmatesText(Context context, String flatmates) {
        if (StringUtils.isNullOrEmpty(flatmates))
            return EMPTY_STRING;
        return flatmates + SPACE + context.getString(R.string.flatmates).toLowerCase();
    }

}
