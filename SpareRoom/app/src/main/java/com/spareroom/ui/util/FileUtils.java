package com.spareroom.ui.util;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import com.spareroom.App;
import com.spareroom.controller.AppVersion;
import com.spareroom.integration.webservice.exception.ClientErrorException;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.core.content.FileProvider;

/**
 * Utility class for managing files
 */
public class FileUtils {

    private static final String FILE_NAME_DATE_FORMAT = "yyyyMMdd_HHmmss";
    private static final String SPARE_ROOM_IMAGES_FOLDER = "/SpareRoom/Image";
    private static final String FILENAME_PATTERN = "IMG_%s.jpg";
    private static final String FILE_PROVIDER_AUTHORITY = "%s.fileprovider";
    private static final String TEMP_IMAGES_DIR = "tempImages";
    private static final String TEMP_IMAGE_FILE = "tempImageFile.jpg";
    private static final String IMAGE_MIME_TYPE = "image/";

    static Uri getFileUri(Context context, File file) {
        return FileProvider.getUriForFile(context, String.format(FILE_PROVIDER_AUTHORITY, AppVersion.appId()), file);
    }

    public static File createNewFileForPhoto() {
        String timeStamp = new SimpleDateFormat(FILE_NAME_DATE_FORMAT, Locale.getDefault()).format(new Date());
        String fileName = String.format(FILENAME_PATTERN, timeStamp);

        File externalStorageDir = new File(Environment.getExternalStorageDirectory(), SPARE_ROOM_IMAGES_FOLDER);
        if (externalStorageDir.mkdirs() || externalStorageDir.isDirectory())
            return new File(externalStorageDir, fileName);

        externalStorageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        externalStorageDir.mkdirs();
        return new File(externalStorageDir, fileName);
    }

    public static File copyImageToInternalStorage(Context context, Uri imageUri) throws ClientErrorException {
        File tempImageDir = new File(context.getFilesDir(), TEMP_IMAGES_DIR);
        tempImageDir.mkdirs();
        File tempFile = new File(tempImageDir, TEMP_IMAGE_FILE);
        if (tempFile.exists())
            tempFile.delete();

        InputStream inputStream = null;
        FileOutputStream outputStream = null;
        try {
            inputStream = context.getContentResolver().openInputStream(imageUri);
            outputStream = new FileOutputStream(tempFile);
            byte[] imageChunks = new byte[100 * 1000];
            int read;
            while ((read = inputStream.read(imageChunks)) > 0) {
                outputStream.write(imageChunks, 0, read);
            }

            return tempFile;
        } catch (IOException e) {
            throw new ClientErrorException(e);
        } finally {
            closeResource(inputStream);
            closeResource(outputStream);
        }
    }

    public static void closeResource(Closeable closeableResource) throws ClientErrorException {
        if (closeableResource == null)
            return;

        try {
            closeableResource.close();
        } catch (IOException e) {
            throw new ClientErrorException(e);
        }

    }

    public static boolean isUriImageType(Uri uri) {
        String mimeType = App.get().getContentResolver().getType(uri);
        return mimeType == null || mimeType.startsWith(IMAGE_MIME_TYPE);
    }

}
