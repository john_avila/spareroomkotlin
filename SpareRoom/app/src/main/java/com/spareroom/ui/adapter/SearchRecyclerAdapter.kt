package com.spareroom.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.spareroom.R
import com.spareroom.model.business.SearchItemType
import com.spareroom.model.business.SearchListItem
import javax.inject.Inject

class SearchRecyclerAdapter @Inject constructor() : RecyclerViewAdapter<SearchListItem, RecyclerView.ViewHolder>() {

    private lateinit var onClickListener: View.OnClickListener

    fun init(context: Context, onClickListener: View.OnClickListener) {
        super.context = context
        this.onClickListener = onClickListener
    }

    private val examplesText by lazy { context.getString(R.string.examples) }
    private val previousSearchesText by lazy { context.getString(R.string.previous_searches) }

    private val black by lazy { ContextCompat.getColor(context, R.color.black) }
    private val grey by lazy { ContextCompat.getColor(context, R.color.grey_2) }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position).searchItemType) {
            SearchItemType.DIVIDER -> R.layout.search_divider
            SearchItemType.TITLE -> R.layout.search_title
            else -> R.layout.search_item
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflatedView = inflater.inflate(viewType, parent, false)
        return when (viewType) {
            R.layout.search_divider -> DividerViewHolder(inflatedView)
            R.layout.search_title -> TitleViewHolder(inflatedView)
            else -> SearchViewHolder(inflatedView)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is TitleViewHolder -> bindTitleViewHolder(holder)
            is SearchViewHolder -> bindSearchViewHolder(holder, position)
        }
    }

    private fun bindTitleViewHolder(holder: TitleViewHolder) {
        holder.title.text = examplesText
    }

    private fun bindSearchViewHolder(holder: SearchViewHolder, position: Int) {
        val item = getItem(position)
        holder.searchItemTitle.text = item.title
        holder.searchItemSubtitle.visibility = if (item.subtitle.isNotEmpty()) View.VISIBLE else View.GONE
        holder.searchItemSubtitle.text = item.subtitle
        holder.searchItemAlerts.visibility = if (item.alerts.isNotEmpty()) View.VISIBLE else View.GONE
        holder.searchItemAlerts.text = item.alerts
        holder.searchItemOptions.visibility = if (item.hasOptions) View.VISIBLE else View.GONE
        holder.searchItemIcon.setImageResource(item.iconId)
        holder.searchItemTitle.setTextColor(if (item.enabled) black else grey)
        holder.searchItemProgressBar.visibility = if (item.loading) View.VISIBLE else View.GONE
        holder.itemView.isEnabled = item.enabled
        holder.itemView.isClickable = item.enabled
        holder.itemView.setTag(R.id.item_tag, item)
        holder.itemView.setOnClickListener(onClickListener)
    }

    private class DividerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private class TitleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView as TextView
    }

    private class SearchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val searchItemIcon: ImageView = itemView.findViewById(R.id.searchItemIcon)
        val searchItemTitle: TextView = itemView.findViewById(R.id.searchItemTitle)
        val searchItemSubtitle: TextView = itemView.findViewById(R.id.searchItemSubtitle)
        val searchItemAlerts: TextView = itemView.findViewById(R.id.searchItemAlerts)
        val searchItemOptions: ImageView = itemView.findViewById(R.id.searchItemOptions)
        val searchItemProgressBar: ProgressBar = itemView.findViewById(R.id.searchItemProgressBar)
    }

}
