package com.spareroom.ui.controller;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.lib.core.IObservable;
import com.spareroom.lib.core.IObserver;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.ui.util.AnimationUtils;

import java.util.LinkedList;

import androidx.core.content.ContextCompat;

/**
 * UiController for the "Continue" button in the multi-step process for registering.
 * Created by manuel on 18/11/2016.
 */

public class ActionButtonUiController implements ViewUiController, ActionUiController, IObservable {
    private final TextView _tvButton;
    private final ProgressBar _pbButton;
    private boolean _isLoading = false;
    private LinkedList<IObserver> _lObserser = new LinkedList<>();

    public ActionButtonUiController(View bottomToolbar) {
        // TODO refactor to pass View/Widget that controls TextView+ProgressBar
        _tvButton = bottomToolbar.findViewById(R.id.materialToolbarBottom_rightText);
        _pbButton = bottomToolbar.findViewById(R.id.materialToolbarBottom_loading);
    }

    private void setNotLoading() {
        if (_isLoading) {
            _isLoading = false;
            _tvButton.setVisibility(View.VISIBLE);
            _pbButton.setVisibility(View.GONE);
        }
    }

    /**
     * Enable the view for its use
     */
    @Override
    public void enable() {
        setNotLoading();
        _tvButton.setEnabled(true);
        _tvButton.setTextColor(ContextCompat.getColor(_tvButton.getContext(), R.color.white));
    }

    /**
     * Disable the view for make it non usable
     */
    @Override
    public void disable() {
        setNotLoading();
        _tvButton.setEnabled(false);
        _tvButton.setTextColor(ContextCompat.getColor(_tvButton.getContext(), R.color.light_blue));
    }

    /**
     * Sets the components so they display on screen the loading state
     */
    @Override
    public void setLoading() {
        disable();
        _tvButton.setEnabled(false);
        _isLoading = true;
        _tvButton.setVisibility(View.GONE);
        _pbButton.setVisibility(View.VISIBLE);
    }

    /**
     * Sets a status for this view (normally an error for fields that require validation)
     *
     * @param error a <code>SparerooomStatus</code> status
     */
    @Override
    public void setStatus(SpareroomStatus error) {

    }

    /**
     * Clears the status for this view
     */
    @Override
    public void clearStatus() {

    }

    /**
     * Populates components with content. For <code>SpareroomStatus</code> and
     * <code>Exception</code> use {@link #setSpareroomStatus(SpareroomStatus)}
     * or {@link #setException(Exception)}
     *
     * @param o objects that help to update the UI
     */
    @Override
    public void setResult(Object... o) {
        enable();
    }

    /**
     * Populates components with content.
     *
     * @param spareroomStatus that helps to update the UI
     */
    @Override
    public void setSpareroomStatus(SpareroomStatus spareroomStatus) {
        setNotLoading();
        AnimationUtils.INSTANCE.shakeLeft(_tvButton);
    }

    /**
     * Populates components with content.
     *
     * @param exception that helps to update the UI
     */
    @Override
    public void setException(Exception exception) {
        setNotLoading();
        AnimationUtils.INSTANCE.shakeLeft(_tvButton);
    }

    @Override
    public void addObserver(IObserver o) {
        _lObserser.add(o);
    }

    @Override
    public void removeObserver(IObserver o) {
        _lObserser.remove(o);
    }

} //end class RegisterEmailUiController
