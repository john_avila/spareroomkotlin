package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListProperties.ShareType;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import java.util.*;

import androidx.annotation.NonNull;

class FilterWantedEmploymentItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.employmentStatus;
    private final static String KEY_EMPLOYMENT_GROUP = "key_employment_group";

    static final String PROFESSIONALS = "professionals";
    static final String STUDENTS = "students";
    static final String NO_PREFERENCE = "noPreference";

    private final List<GroupedCompoundButtonParams> employmentGroup = new ArrayList<>();
    private final Map<String, List<GroupedCompoundButtonParams>> groupList = new HashMap<>();

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesWanted previousProperties = (SearchAdvertListPropertiesWanted) properties;
        List<FilterItem> list = new ArrayList<>();
        employmentGroup.clear();
        groupList.clear();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        GroupedCompoundButtonParams professionals = new GroupedCompoundButtonParams(PROFESSIONALS);
        professionals.size(CompoundButtonParams.Size.MEDIUM);
        professionals.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        professionals.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        professionals.enableHighlightBar(true);
        professionals.text(application.getString(R.string.professionals));
        professionals.textColorResId(R.color.black);
        professionals.iconResId(R.drawable.ic_professional);
        professionals.value(ShareType.PROFESSIONALS.toString());
        professionals.groupName(KEY_EMPLOYMENT_GROUP);
        employmentGroup.add(professionals);
        list.add(new FilterItem<>(professionals));

        GroupedCompoundButtonParams students = new GroupedCompoundButtonParams(STUDENTS);
        students.size(CompoundButtonParams.Size.MEDIUM);
        students.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        students.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        students.enableHighlightBar(true);
        students.text(application.getString(R.string.students));
        students.textColorResId(R.color.black);
        students.iconResId(R.drawable.ic_student);
        students.value(ShareType.STUDENTS.toString());
        students.groupName(KEY_EMPLOYMENT_GROUP);
        employmentGroup.add(students);
        list.add(new FilterItem<>(students));

        GroupedCompoundButtonParams noPreference = new GroupedCompoundButtonParams(NO_PREFERENCE);
        noPreference.size(CompoundButtonParams.Size.MEDIUM);
        noPreference.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        noPreference.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.GRAY);
        noPreference.enableHighlightBar(false);
        noPreference.text(application.getString(R.string.noPreference));
        noPreference.textColorResId(R.color.black);
        noPreference.value(ShareType.NOT_SET.toString());
        noPreference.groupName(KEY_EMPLOYMENT_GROUP);
        noPreference.defaultItem(true);
        employmentGroup.add(noPreference);
        list.add(new FilterItem<>(noPreference));

        groupList.put(KEY_EMPLOYMENT_GROUP, employmentGroup);
        setGroupPreviousSearch(previousProperties);

        this.list = list;
    }

    private void setGroupPreviousSearch(SearchAdvertListPropertiesWanted properties) {
        Set<String> groupListKeySet = groupList.keySet();

        for (String key : groupListKeySet) {
            List<GroupedCompoundButtonParams> group = groupList.get(key);

            for (GroupedCompoundButtonParams params : group) {
                switch (key) {
                    case KEY_EMPLOYMENT_GROUP:
                        params.selected((properties.getShareType() != ShareType.NOT_SET)
                                && params.value().equals(properties.getShareType().toString()));
                        break;
                }
            }
            selectedItem(group);
        }

    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return groupList;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesWanted propertiesWanted = (SearchAdvertListPropertiesWanted) propertiesToUpdate;
        propertiesWanted.setShareType(ShareType.valueOf(selectedValue(groupList.get(KEY_EMPLOYMENT_GROUP))));

    }

}
