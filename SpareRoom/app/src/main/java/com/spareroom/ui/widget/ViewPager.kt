package com.spareroom.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

class ViewPager : ViewPager {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    override fun onInterceptTouchEvent(event: MotionEvent?): Boolean {
        return try {
            super.onInterceptTouchEvent(event)
        } catch (exception: Exception) {
            false
        }
    }
}
