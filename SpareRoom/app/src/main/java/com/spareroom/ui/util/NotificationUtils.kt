package com.spareroom.ui.util

import android.app.*
import android.app.NotificationManager.IMPORTANCE_DEFAULT
import android.content.Context
import android.graphics.Bitmap
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.*
import androidx.core.content.ContextCompat
import com.spareroom.R
import com.spareroom.controller.AppVersion

private const val MESSAGE_NOTIFICATION_ID = 1
private const val VIDEO_UPLOAD_NOTIFICATION_ID = 2

object NotificationUtils {

    private val MESSAGES_CHANNEL_ID = AppVersion.appId() + "_messages"
    private val VIDEO_UPLOAD_CHANNEL_ID = AppVersion.appId() + "_videoUpload"

    @JvmStatic
    fun closeNotifications(context: Context) {
        notificationManager(context)?.let {
            it.cancel(MESSAGE_NOTIFICATION_ID)
            it.cancel(VIDEO_UPLOAD_NOTIFICATION_ID)
        }
    }

    @JvmStatic
    fun createNotificationChannels(context: Context) {
        if (UiUtils.isOreoOrNewer()) {

            // messages channel
            val messagesChannelName = context.getString(R.string.navigationDrawer_messages)
            val messagesChannel = NotificationChannel(MESSAGES_CHANNEL_ID, messagesChannelName, IMPORTANCE_DEFAULT)
            messagesChannel.setShowBadge(true)
            messagesChannel.enableLights(true)
            messagesChannel.enableVibration(true)

            // video upload channel
            val videoUploadChannelName = context.getString(R.string.video_upload)
            val videoUploadChannel = NotificationChannel(VIDEO_UPLOAD_CHANNEL_ID, videoUploadChannelName, IMPORTANCE_DEFAULT)
            videoUploadChannel.setShowBadge(false)
            videoUploadChannel.enableLights(true)
            videoUploadChannel.enableVibration(true)

            notificationManager(context)?.createNotificationChannels(listOf(messagesChannel, videoUploadChannel))
        }
    }

    @JvmStatic
    fun showMessagesNotification(context: Context, largeIcon: Bitmap?, group: String?, title: String?, message: String?, contentIntent: PendingIntent) {
        val builder = NotificationCompat.Builder(context, MESSAGES_CHANNEL_ID)
            .setDefaults(DEFAULT_SOUND or DEFAULT_VIBRATE or DEFAULT_LIGHTS)
            .setGroup(group)
            .setSmallIcon(R.drawable.ic_notification)
            .setLargeIcon(largeIcon)
            .setContentTitle(title)
            .setStyle(NotificationCompat.BigTextStyle().bigText(message))
            .setAutoCancel(true)
            .setContentText(message)
            .setColor(ContextCompat.getColor(context, R.color.material_deep_sea_blue_dark))
            .setContentIntent(contentIntent)
            .setOnlyAlertOnce(true)
            .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)

        notificationManager(context)?.notify(MESSAGE_NOTIFICATION_ID, builder.build())
    }

    @JvmStatic
    @JvmOverloads
    fun showVideoUploadNotification(context: Context, message: String, intent: PendingIntent, silent: Boolean = false) {
        val builder = NotificationCompat.Builder(context, VIDEO_UPLOAD_CHANNEL_ID)
            .setGroup(context.getString(R.string.app_name))
            .setSmallIcon(R.drawable.ic_notification)
            .setContentTitle(context.getString(R.string.app_name))
            .setStyle(NotificationCompat.BigTextStyle().bigText(message))
            .setAutoCancel(true)
            .setContentText(message)
            .setColor(ContextCompat.getColor(context, R.color.material_deep_sea_blue_dark))
            .setContentIntent(intent)
            .setOnlyAlertOnce(true)
            .setBadgeIconType(NotificationCompat.BADGE_ICON_NONE)
            .also { if (!silent) it.setDefaults(DEFAULT_SOUND or DEFAULT_VIBRATE or DEFAULT_LIGHTS) }

        notificationManager(context)?.notify(VIDEO_UPLOAD_NOTIFICATION_ID, builder.build())
    }

    private fun notificationManager(context: Context) = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?

}
