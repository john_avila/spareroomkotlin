package com.spareroom.ui.flow;

public class PlaceAdFlowManager extends FlowManager {

    public PlaceAdFlowManager() {
        FlowState defaultState = new PlaceAdStateLoggedInOffered();
        setState(defaultState);
        init(defaultState);
    }

    public PlaceAdFlowManager(FlowState state) {
        init(state);
    }

    public String nextErrorScreen(String cause) {
        FlowState currentState = getState();
        if (getState() instanceof PlaceAdStateLoggedInOffered || currentState instanceof PlaceAdStateLoggedOutOffered) {
            PlaceAdStateErrorOffered errorState = new PlaceAdStateErrorOffered();
            return errorState.nextErrorScreen(cause);
        } else if (currentState instanceof PlaceAdStateLoggedInWanted || currentState instanceof PlaceAdStateLoggedOutWanted) {
            PlaceAdStateErrorWanted errorState = new PlaceAdStateErrorWanted();
            return errorState.nextErrorScreen(cause);
        }
        return null;
    }

}
