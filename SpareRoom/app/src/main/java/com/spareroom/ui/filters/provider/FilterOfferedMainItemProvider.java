package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListProperties.*;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.*;
import com.spareroom.ui.filters.adapter.viewparams.*;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.util.StringUtils;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import java.util.*;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class FilterOfferedMainItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.filter;

    private static final String SUBTITLE_SPACE = " ";
    private static final String SUBTITLE_PLUS = " + ";
    private static final String SUBTITLE_EMPTY_STRING = "";

    final static String OFFERED_MONTHLY_BUDGET = "offered_monthly_budget";
    final static String OFFERED_AVAILABILITY = "offered_availability";
    final static String OFFERED_PROPERTY_TYPE = "offered_property_type";
    final static String OFFERED_BEDROOM_TYPE = "offered_bedroom_type";
    final static String OFFERED_SHARING_WITH = "offered_sharing_with";
    final static String OFFERED_SUITABLE_FOR = "offered_suitable_for";
    final static String OFFERED_POSTED_BY = "offered_posted_by";
    final static String OFFERED_WITH_PHOTOS = "offered_with_photos";
    final static String OFFERED_UNSUITABLE = "offered_unsuitable";

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesOffered previousProperties = (SearchAdvertListPropertiesOffered) properties;
        List<FilterItem> list = new ArrayList<>();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        SummaryViewSearchRadiusParams searchRadius = new SummaryViewSearchRadiusParams(SEARCH_RADIUS);
        searchRadius.iconDrawableId(R.drawable.ic_radius);
        searchRadius.titleStringId(R.string.searchRadius);
        searchRadius.defaultSubtitleString(application.getString(R.string.noPreference));
        searchRadius.subtitleString(getSearchRadiusSubtitle(previousProperties));
        list.add(new FilterItem<>(searchRadius));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        SummaryViewParams monthlyBudget = new SummaryViewParams(OFFERED_MONTHLY_BUDGET);
        monthlyBudget.iconDrawableId(R.drawable.ic_budget);
        monthlyBudget.titleStringId(R.string.monthly_budget);
        monthlyBudget.defaultSubtitleString(application.getString(R.string.noPreference));
        monthlyBudget.subtitleString(getMonthlyBudgetSubtitle(previousProperties));
        list.add(new FilterItem<>(monthlyBudget));

        SummaryViewParams availability = new SummaryViewParams(OFFERED_AVAILABILITY);
        availability.iconDrawableId(R.drawable.ic_availability);
        availability.titleStringId(R.string.availability);
        availability.defaultSubtitleString(application.getString(R.string.noPreference));
        availability.subtitleString(getAvailabilitySubtitle(previousProperties));
        list.add(new FilterItem<>(availability));

        SummaryViewParams propertyType = new SummaryViewParams(OFFERED_PROPERTY_TYPE);
        propertyType.iconDrawableId(R.drawable.ic_studio_apartment);
        propertyType.titleStringId(R.string.propertyType);
        propertyType.defaultSubtitleString(application.getString(R.string.noPreference));
        propertyType.subtitleString(getPropertyTypeSubtitle(previousProperties));
        list.add(new FilterItem<>(propertyType));

        SummaryViewParams bedroomType = new SummaryViewParams(OFFERED_BEDROOM_TYPE);
        bedroomType.iconDrawableId(R.drawable.ic_double_bed);
        bedroomType.titleStringId(R.string.bedroomType);
        bedroomType.defaultSubtitleString(application.getString(R.string.noPreference));
        bedroomType.subtitleString(getBedroomTypeSubtitle(previousProperties));
        list.add(new FilterItem<>(bedroomType));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        SummaryViewParams sharingWith = new SummaryViewParams(OFFERED_SHARING_WITH);
        sharingWith.iconDrawableId(R.drawable.ic_generic_user);
        sharingWith.titleStringId(R.string.sharingWith);
        sharingWith.defaultSubtitleString(application.getString(R.string.noPreference));
        sharingWith.subtitleString(getSharingWithSubtitle(previousProperties));
        list.add(new FilterItem<>(sharingWith));

        SummaryViewParams suitableFor = new SummaryViewParams(OFFERED_SUITABLE_FOR);
        suitableFor.iconDrawableId(R.drawable.ic_generic_user);
        suitableFor.titleStringId(R.string.suitableFor);
        suitableFor.defaultSubtitleString(application.getString(R.string.noPreference));
        suitableFor.subtitleString(getSuitableForSubtitle(previousProperties));
        list.add(new FilterItem<>(suitableFor));

        SummaryViewParams postedBy = new SummaryViewParams(OFFERED_POSTED_BY);
        postedBy.iconDrawableId(R.drawable.ic_generic_user);
        postedBy.titleStringId(R.string.postedBy);
        postedBy.defaultSubtitleString(application.getString(R.string.noPreference));
        postedBy.subtitleString(getPostedBySubtitle(previousProperties));
        list.add(new FilterItem<>(postedBy));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        CompoundButtonParams withPhotos = new CompoundButtonParams(OFFERED_WITH_PHOTOS);
        withPhotos.size(CompoundButtonParams.Size.MEDIUM);
        withPhotos.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        withPhotos.enableHighlightBar(true);
        withPhotos.text(application.getString(R.string.advertsWithPhotos));
        withPhotos.textColorResId(R.color.black);
        withPhotos.iconResId(R.drawable.ic_photos);
        withPhotos.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        withPhotos.selected(previousProperties.getPhotoAdvertsOnly());
        list.add(new FilterItem<>(withPhotos));

        CompoundButtonParams showHidden = new CompoundButtonParams(OFFERED_UNSUITABLE);
        showHidden.size(CompoundButtonParams.Size.MEDIUM);
        showHidden.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        showHidden.enableHighlightBar(true);
        showHidden.text(application.getString(R.string.includeHidden));
        showHidden.textColorResId(R.color.black);
        showHidden.iconResId(R.drawable.ic_unsuitables);
        showHidden.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        showHidden.selected(previousProperties.getIncludeHidden());
        list.add(new FilterItem<>(showHidden));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.BIG)));

        this.list = list;
    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return null;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesOffered propertiesOffered = (SearchAdvertListPropertiesOffered) propertiesToUpdate;
        for (FilterItem item : list) {
            switch (item.tag()) {
                case OFFERED_WITH_PHOTOS:
                    propertiesOffered.setPhotoAdvertsOnly(((CompoundButtonParams) item.params()).selected());
                    break;
                case OFFERED_UNSUITABLE:
                    propertiesOffered.setIncludeHidden(((CompoundButtonParams) item.params()).selected());
                    break;
            }
        }

    }

    @NonNull
    String getSearchRadiusSubtitle(SearchAdvertListPropertiesOffered previousProperties) {
        if (previousProperties.getMilesFromMax() != null) {
            return
                    previousProperties.getWhere()
                            + SUBTITLE_PLUS
                            + previousProperties.getMilesFromMax()
                            + SUBTITLE_SPACE
                            + application.getResources().getQuantityString(R.plurals.miles, previousProperties.getMilesFromMax()).toLowerCase();
        } else if (!StringUtils.isNullOrEmpty(previousProperties.getWhere())) {
            return previousProperties.getWhere();
        } else {
            return SUBTITLE_EMPTY_STRING;
        }
    }

    @NonNull
    String getMonthlyBudgetSubtitle(SearchAdvertListPropertiesOffered previousProperties) {
        StringBuilder stringBuilder = new StringBuilder();

        Integer minRent = previousProperties.getMinRent();
        Integer maxRent = previousProperties.getMaxRent();
        if ((minRent != null) || (maxRent != null))
            stringBuilder.append(subtitleUtil.getRentAmountText(application, minRent, maxRent));

        if (previousProperties.billsInc()) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));

            stringBuilder.append(application.getString(R.string.billsIncluded));
        }

        return stringBuilder.toString();
    }

    @NonNull
    String getAvailabilitySubtitle(SearchAdvertListPropertiesOffered previousProperties) {
        StringBuilder stringBuilder = new StringBuilder();

        String availableFrom = previousProperties.getAvailableFrom();
        if (availableFrom != null)
            stringBuilder.append(subtitleUtil.getMoveInText(availableFrom));

        boolean isMinTerm = previousProperties.getMinTerm() != MinTerm.NOT_SET;
        boolean isMaxTerm = previousProperties.getMaxTerm() != MaxTerm.NOT_SET;
        if (isMinTerm || isMaxTerm) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));

            stringBuilder.append(
                    subtitleUtil.getLengthOfStayText(
                            application,
                            isMinTerm ? previousProperties.getMinTerm().getIntValue() : null,
                            isMaxTerm ? previousProperties.getMaxTerm().getIntValue() : null));
        }

        if (previousProperties.getShortLets()) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));
            stringBuilder.append(application.getString(R.string.shortTermLets));
        }

        if (previousProperties.getDaysOfWeekAvailable() != DaysOfWeek.NOT_SET) {
            String daysOfWeekAvailable = null;
            switch (previousProperties.getDaysOfWeekAvailable()) {
                case SEVEN_DAYS:
                    daysOfWeekAvailable = application.getString(R.string.allWeekAdverts);
                    break;
                case WEEKDAYS:
                    daysOfWeekAvailable = application.getString(R.string.weekdayOnlyAdverts);
                    break;
                case WEEKENDS:
                    daysOfWeekAvailable = application.getString(R.string.weekendOnlyAdverts);
                    break;
            }

            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));
            stringBuilder.append(daysOfWeekAvailable);
        }

        return stringBuilder.toString();
    }

    @NonNull
    String getPropertyTypeSubtitle(SearchAdvertListPropertiesOffered previousProperties) {
        StringBuilder stringBuilder = new StringBuilder();

        if (previousProperties.getShowRooms())
            stringBuilder.append(application.getString(R.string.rooms));

        if (previousProperties.getShowStudios()) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));
            stringBuilder.append(application.getString(R.string.studios1bedFlats));
        }

        if (previousProperties.getShowWholeProperty()) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));
            stringBuilder.append(application.getString(R.string.wholeProperties));
        }

        NoOfRooms noOfRooms = previousProperties.getNoOfRooms();
        if (noOfRooms != NoOfRooms.NOT_SET) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));

            switch (noOfRooms) {
                case MIN_TWO:
                    stringBuilder.append(application.getString(R.string.twoOrMoreBedrooms));
                    break;
                case MIN_THREE:
                    stringBuilder.append(application.getString(R.string.threeOrMoreBedrooms));
                    break;
            }

        }

        if (previousProperties.getParking()) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));
            stringBuilder.append(application.getString(R.string.parking));
        }

        if (previousProperties.getLivingRoom()) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));
            stringBuilder.append(application.getString(R.string.sharedLivingRoom));
        }

        if (previousProperties.getDisabledAccess()) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));
            stringBuilder.append(application.getString(R.string.disabledAccess));
        }

        return stringBuilder.toString();
    }

    @NonNull
    String getBedroomTypeSubtitle(SearchAdvertListPropertiesOffered previousProperties) {
        StringBuilder stringBuilder = new StringBuilder();

        RoomTypes roomType = previousProperties.getRoomType();
        if (roomType != RoomTypes.NOT_SET)
            stringBuilder.append(subtitleUtil.getRoomTypeText(application, roomType));

        if (previousProperties.getEnSuite()) {
            if (stringBuilder.length() != 0) {
                stringBuilder.append(" ");
                stringBuilder.append(application.getString(R.string.with_ensuite));
            } else {
                stringBuilder.append(application.getString(R.string.enSuite));
            }
        }

        return stringBuilder.toString();
    }

    @NonNull
    String getSharingWithSubtitle(SearchAdvertListPropertiesOffered previousProperties) {
        StringBuilder stringBuilder = new StringBuilder();

        Integer minAge = previousProperties.getMinAge();
        Integer maxAge = previousProperties.getMaxAge();
        if ((minAge != null) || (maxAge != null))
            stringBuilder.append(subtitleUtil.getAgeText(application, minAge, maxAge));

        GenderFilter genderFilter = previousProperties.getGenderFilter();
        if (genderFilter != GenderFilter.NOT_SET) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));
            stringBuilder.append(subtitleUtil.getGenderFilterText(application, genderFilter));
        }

        ShareType shareType = previousProperties.getShareType();
        if (shareType != ShareType.NOT_SET) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));
            stringBuilder.append(subtitleUtil.getEmploymentStatusText(application, shareType));
        }

        boolean isMin = previousProperties.getMinFlatmates() != MinFlatmates.NOT_SET;
        boolean isMax = previousProperties.maxFlatmates() != MaxFlatmates.NOT_SET && previousProperties.maxFlatmates() != MaxFlatmates.MORE_THAN_SIX;
        if (isMin || isMax) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));
            stringBuilder.append(
                    getMinMaxFlatmatesText(
                            isMin ? previousProperties.getMinFlatmates().getValue() : null,
                            isMax ? previousProperties.maxFlatmates().getValue() : null));
        }

        Landlord landlord = previousProperties.getLandlord();
        if (landlord != Landlord.NOT_SET) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));

            switch (landlord) {
                case LIVE_IN:
                    stringBuilder.append(application.getString(R.string.withLandlord));
                    break;
                case LIVE_OUT:
                    stringBuilder.append(application.getString(R.string.notWithLandlord));
                    break;
            }
        }

        return stringBuilder.toString();
    }

    String getMinMaxFlatmatesText(@Nullable String min, @Nullable String max) {
        if ((min != null) && (max != null)) {
            return application.getString(R.string.between)
                    + SUBTITLE_SPACE + min
                    + application.getString(R.string.en_dash)
                    + subtitleUtil.getFlatmatesText(application, max);
        } else if ((min != null) || (max != null)) {
            return
                    application.getString((min != null) ? R.string.from : R.string.upTo)
                            + SUBTITLE_SPACE
                            + ((min != null) ? subtitleUtil.getFlatmatesText(application, min) : subtitleUtil.getFlatmatesText(application, max));
        } else {
            return SUBTITLE_EMPTY_STRING;
        }

    }

    @NonNull
    String getSuitableForSubtitle(SearchAdvertListPropertiesOffered previousProperties) {
        StringBuilder stringBuilder = new StringBuilder();

        RoomsFor roomsFor = previousProperties.getRoomsFor();
        if (roomsFor != RoomsFor.NOT_SET) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));

            switch (roomsFor) {
                case FEMALES:
                    stringBuilder.append(application.getString(R.string.females));
                    break;
                case MALES:
                    stringBuilder.append(application.getString(R.string.males));
                    break;
                case COUPLES:
                    stringBuilder.append(application.getString(R.string.couples));
                    break;
            }
        }

        if (previousProperties.getBenefits()) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));
            stringBuilder.append(application.getString(R.string.benefitsRecipients));
        }

        if (previousProperties.getPets()) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));
            stringBuilder.append(application.getString(R.string.petsOwners));
        }

        Smoking smoking = previousProperties.getSmoking();
        if (smoking != Smoking.NOT_SET) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));

            switch (smoking) {
                case YES:
                    stringBuilder.append(application.getString(R.string.smokers));
                    break;
                case NO:
                    stringBuilder.append(application.getString(R.string.nonSmokers));
                    break;
            }
        }

        return stringBuilder.toString();
    }

    @NonNull
    String getPostedBySubtitle(SearchAdvertListPropertiesOffered previousProperties) {
        PostedBy postedBy = previousProperties.getPostedBy();

        if (postedBy == PostedBy.PRIVATE_LANDLORDS)
            return application.getString(R.string.individualUsers);
        if (postedBy == PostedBy.AGENTS)
            return application.getString(R.string.agents);
        else
            return SUBTITLE_EMPTY_STRING;
    }

}
