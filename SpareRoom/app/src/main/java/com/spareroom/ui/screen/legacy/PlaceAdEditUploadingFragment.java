package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.controller.ControllerActionObserver;
import com.spareroom.controller.DraftAdOfferedUpdateControllerAction;
import com.spareroom.controller.DraftAdWantedUpdateControllerAction;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.model.business.DraftAd;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.model.business.Session;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.ui.anim.ProgressbarAnim;

import java.util.Locale;

public class PlaceAdEditUploadingFragment extends PlaceAdFragment {

    //region FIELDS UI

    private ImageView _ivGear;
    private View _vProgress;
    private TextView _tvProgress;

    //endregion FIELDS UI

    //region FIELDS UI CONTROLLER

    private static final int PROGRESS_BAR_ANIMATION_TIME = 5000;
    private static final int GEAR_ANIMATION_TIME = 4000;
    private int _finalPixel;

    //endregion FIELDS UI CONTROLLER

    //region FIELDS CONTROLLER

    private Thread _countThread;
    private boolean _apiFinished = false;
    private boolean _animFinished = false;

    private boolean firstVisit = false;

    private final ControllerActionObserver _uploadOfferedDraftAdUpdateObserver = new ControllerActionObserver() {

        @Override
        // Processes the successful response - it can only be null
        public void onResult(Object o) {
            AnalyticsTrackerComposite.getInstance().trackEvent_EditAd_EditOfferedAd();
            uploadFinished();
        }

        @Override
        // Processes any error when updating the advert
        public void onSpareroomStatus(SpareroomStatus status) {
            AnalyticsTrackerComposite.getInstance().trackEvent_EditAd_EditOfferedAdWithErrors(status);
            ((PlaceAdAbstractActivity) getActivity()).handleError(status);
        }

        @Override
        public void onException(Exception exception) {
            SpareroomStatus spareroomStatus = new SpareroomStatus();
            spareroomStatus.setMessage(getString(R.string.tNoConnection));

            ((PlaceAdAbstractActivity) getActivity()).handleError(spareroomStatus);
        }

    };

    private final ControllerActionObserver _uploadWantedDraftAdUpdateObserver = new ControllerActionObserver() {

        @Override
        // Processes the successful response - it can only be null
        public void onResult(Object o) {
            AnalyticsTrackerComposite.getInstance().trackEvent_EditAd_EditWantedAd();
            uploadFinished();
        }

        @Override
        // Processes any error when updating the advert
        public void onSpareroomStatus(SpareroomStatus status) {
            AnalyticsTrackerComposite.getInstance().trackEvent_EditAd_EditWantedAdWithErrors(status);
            ((PlaceAdAbstractActivity) getActivity()).handleError(status);
        }

        @Override
        public void onException(Exception exception) {
            SpareroomStatus spareroomStatus = new SpareroomStatus();
            spareroomStatus.setMessage(getString(R.string.tNoConnection));

            ((PlaceAdAbstractActivity) getActivity()).handleError(spareroomStatus);
        }

    };

    //endregion FIELDS CONTROLLER

    //region METHODS UI

    private void uploadFinished() {
        _apiFinished = true;
        if (_animFinished)
            finishUploading();
    }

    /**
     * Actualizes the progress bar and percentage.
     */
    private void animateForwardProgressCount() {
        final int originalProgress = 0;
        final int finalProgress = 100;
        final float updateWait = (float) PROGRESS_BAR_ANIMATION_TIME / (float) (finalProgress - originalProgress);

        _countThread = new Thread(new Runnable() {
            private int currentProgress = originalProgress;

            public void run() {
                while (currentProgress < finalProgress) {
                    if (Thread.interrupted())
                        return;
                    currentProgress++;
                    try {
                        Thread.sleep((long) updateWait);
                    } catch (InterruptedException e) {
                        return;
                    }
                    if (currentProgress == 100) {
                        _animFinished = true;
                        if (_apiFinished)
                            finishUploading();
                    } else
                        _tvProgress.post(new ProgressCountUpdater(currentProgress));

                }
            }

        });
        _countThread.start();

    } //end animateForwardProgressCount()

    /**
     * Sets as finalized the dynamics views and finalize the fragment
     */
    private void finishUploading() {
        getActivity().runOnUiThread(() -> {
            _ivGear.clearAnimation();
            _tvProgress.setText(getString(R.string.place_ad_complete_upload));
            _vProgress.getLayoutParams().width = _finalPixel;
            _vProgress.requestLayout();
            finish();
        });
    } //end finishUploading()

    //endregion METHODS UI

    //region METHODS CONTROLLER
    private void launchDraftAdOfferedUpdate(DraftAdOffered draft) {
        DraftAdOfferedUpdateControllerAction draftAdOfferedUpdateControllerAction =
                new DraftAdOfferedUpdateControllerAction(
                        SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext());
        draftAdOfferedUpdateControllerAction.addControllerActionObserver(_uploadOfferedDraftAdUpdateObserver);
        addObserver(draftAdOfferedUpdateControllerAction);
        draftAdOfferedUpdateControllerAction.execute(draft);

        addObserver(draftAdOfferedUpdateControllerAction);
    }

    private void launchDraftAdWantedUpdate(DraftAdWanted draft) {
        DraftAdWantedUpdateControllerAction draftAdWantedUpdateControllerAction =
                new DraftAdWantedUpdateControllerAction(
                        SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext());
        draftAdWantedUpdateControllerAction.addControllerActionObserver(_uploadWantedDraftAdUpdateObserver);
        addObserver(draftAdWantedUpdateControllerAction);
        draftAdWantedUpdateControllerAction.execute(draft);

        addObserver(draftAdWantedUpdateControllerAction);
    }
    //endregion METHODS CONTROLLER

    //region METHODS LIFECYCLE

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setTitle(getString(R.string.place_ad_uploading_title));

        View rootView = inflater.inflate(R.layout.place_ad_uploading, container, false);
        View vProgressBackground = rootView.findViewById(R.id.place_ad_uploading_rlProgress);

        RotateAnimation anim = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        ProgressbarAnim barAnim;

        DraftAd draft = getDraftAd();

        // In case there is an activity state change and we lost the draft
        if (draft == null) {
            getActivity().finish();

            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }

        Session session = SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext().getSession();
        if (session != null) {
            draft.set_firstName(session.get_user().get_firstName());
            draft.set_lastName(session.get_user().get_lastName());
            draft.set_email(session.get_email());
        }

        _ivGear = rootView.findViewById(R.id.place_ad_uploading_ivGear);
        _tvProgress = rootView.findViewById(R.id.place_ad_uploading_tvProgress);
        _vProgress = rootView.findViewById(R.id.place_ad_uploading_vProgress);

        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(GEAR_ANIMATION_TIME);
        _ivGear.startAnimation(anim);

        _finalPixel = vProgressBackground.getLayoutParams().width;

        barAnim = new ProgressbarAnim(_vProgress, _finalPixel);
        barAnim.setInterpolator(new AccelerateDecelerateInterpolator());
        barAnim.setDuration(PROGRESS_BAR_ANIMATION_TIME);
        _vProgress.startAnimation(barAnim);

        animateForwardProgressCount();

        if (draft instanceof DraftAdOffered) {
            launchDraftAdOfferedUpdate((DraftAdOffered) draft);
        } else if (draft instanceof DraftAdWanted) {
            launchDraftAdWantedUpdate((DraftAdWanted) draft);
        }

        firstVisit = true;

        return rootView;
    } //end onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)

    @Override
    public void onResume() {
        super.onResume();

        if (firstVisit) {
            firstVisit = false;
        } else {
            if (_apiFinished) {
                finish();
            } else {
                ((PlaceAdAbstractActivity) getActivity()).previousScreen();
            }
        }

    } //end onResume()

    @Override
    public void onStop() {
        super.onStop();

        if (_countThread != null)
            _countThread.interrupt();
    } //end onStop()

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    /**
     * Updates the progress views.
     */
    private class ProgressCountUpdater implements Runnable {
        @SuppressWarnings("CanBeFinal")
        private int _progress;

        ProgressCountUpdater(int progress) {
            _progress = progress;
        }

        public void run() {
            _tvProgress.setText(String.format(Locale.getDefault(), "%d %s", _progress, "%"));
        }

    } //end class ProgressCountUpdater

    //endregion INNER CLASSES

}
