package com.spareroom.ui.screen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.spareroom.integration.sharedpreferences.OnboardingDao;
import com.spareroom.ui.screen.legacy.MainActivity;

import androidx.annotation.Nullable;

public class SplashActivity extends Activity {

    private final static int DELAY_TIME = 0;
    private long timeLeft;
    private static final String TIME_LEFT = "time_left";
    private long timeWhenStarting;
    private final Handler handler = new Handler();
    private final Runnable runnable = () -> {
        Intent intent = OnboardingDao.read(this) ? new Intent(this, OnboardingActivity.class) : new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isTaskRoot()) {
            finish();
            return;
        }

        timeLeft = (savedInstanceState == null) ? DELAY_TIME : savedInstanceState.getLong(TIME_LEFT);
    }

    @Override
    protected void onStart() {
        super.onStart();
        handler.postDelayed(runnable, timeLeft);
        timeWhenStarting = System.currentTimeMillis();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        timeLeft = DELAY_TIME - (System.currentTimeMillis() - timeWhenStarting);
        outState.putLong(TIME_LEFT, timeLeft);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
    }

}
