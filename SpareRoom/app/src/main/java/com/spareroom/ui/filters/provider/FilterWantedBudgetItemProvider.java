package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.widget.params.MinMaxEditTextParams;

import java.util.*;

import androidx.annotation.NonNull;

class FilterWantedBudgetItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.monthly_budget;

    static final String PRICE_RANGE = "price_Range";

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesWanted previousProperties = (SearchAdvertListPropertiesWanted) properties;
        List<FilterItem> list = new ArrayList<>();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        MinMaxEditTextParams priceRange = new MinMaxEditTextParams(PRICE_RANGE);
        priceRange.iconId(R.drawable.ic_budget);
        priceRange.title(application.getString(R.string.budgetRange));
        priceRange.textBase(application.getString(R.string.currency_symbol));
        priceRange.minHint(application.getString(R.string.minRentHint));
        priceRange.minTitle(application.getString(R.string.minimum));
        priceRange.maxHint(application.getString(R.string.maxRentHint));
        priceRange.maxTitle(application.getString(R.string.maximum));
        priceRange.minMaxCharacters(5);
        priceRange.maxMaxCharacters(5);
        priceRange.min(previousProperties.getMinRent());
        priceRange.max(previousProperties.getMaxRent());
        list.add(new FilterItem<>(priceRange));

        this.list = list;
    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return null;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesWanted propertiesWanted = (SearchAdvertListPropertiesWanted) propertiesToUpdate;
        for (FilterItem item : list) {
            switch (item.tag()) {
                case PRICE_RANGE:
                    propertiesWanted.minMonthlyRent(((MinMaxEditTextParams) item.params()).min());
                    propertiesWanted.maxMonthlyRent(((MinMaxEditTextParams) item.params()).max());
                    break;
            }
        }

        if (sortProperties)
            sortPriceRange(propertiesWanted);

    }

}
