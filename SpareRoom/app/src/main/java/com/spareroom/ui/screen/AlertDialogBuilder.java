package com.spareroom.ui.screen;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.spareroom.R;

import androidx.annotation.*;
import androidx.appcompat.app.AlertDialog;

public class AlertDialogBuilder extends AlertDialog.Builder {

    private final View view;

    @SuppressLint("InflateParams")
    public AlertDialogBuilder(@NonNull Context context) {
        this(context, LayoutInflater.from(context).inflate(R.layout.alert_dialog_txt_message, null, false));
    }

    @SuppressLint("InflateParams")
    public AlertDialogBuilder(@NonNull Context context, @NonNull View contentVew) {
        super(context);
        view = LayoutInflater.from(context).inflate(R.layout.alert_dialog, null, false);
        FrameLayout frameLayout = view.findViewById(R.id.alertDialogFrame);
        frameLayout.addView(contentVew);
        setView(view);
    }

    @Override
    public AlertDialog.Builder setTitle(@Nullable CharSequence title) {
        TextView txtTitle = view.findViewById(R.id.txtTitle);
        txtTitle.setVisibility(View.VISIBLE);
        txtTitle.setText(title);
        return this;
    }

    @Override
    public AlertDialog.Builder setTitle(@StringRes int textId) {
        setTitle(getString(textId));
        return this;
    }

    @Override
    public AlertDialog.Builder setMessage(@Nullable CharSequence message) {
        TextView txtMessage = view.findViewById(R.id.txtMessage);
        txtMessage.setText(message);
        return this;
    }

    @Override
    public AlertDialog.Builder setMessage(@StringRes int textId) {
        setMessage(getString(textId));
        return this;
    }

    @Override
    public AlertDialog.Builder setPositiveButton(CharSequence text, final DialogInterface.OnClickListener listener) {
        TextView btnPositive = view.findViewById(R.id.btnPositive);
        btnPositive.setText(text);
        btnPositive.setOnClickListener(v -> listener.onClick(null, AlertDialog.BUTTON_POSITIVE));

        showButtonBar();

        return this;
    }

    @Override
    public AlertDialog.Builder setPositiveButton(@StringRes int textId, final DialogInterface.OnClickListener listener) {
        setPositiveButton(getString(textId), listener);
        return this;
    }

    @Override
    public AlertDialog.Builder setNegativeButton(CharSequence text, final DialogInterface.OnClickListener listener) {
        TextView btnNegative = view.findViewById(R.id.btnNegative);
        btnNegative.setText(text);
        btnNegative.setOnClickListener(v -> listener.onClick(null, AlertDialog.BUTTON_NEGATIVE));

        showButtonBar();

        return this;
    }

    @Override
    public AlertDialog.Builder setNegativeButton(@StringRes int textId, final DialogInterface.OnClickListener listener) {
        setNegativeButton(getString(textId), listener);
        return this;
    }

    private CharSequence getString(@StringRes int textId) {
        return getContext().getString(textId);
    }

    private void showButtonBar() {
        view.findViewById(R.id.buttonBar).setVisibility(View.VISIBLE);
    }
}
