package com.spareroom.ui.widget.params;

import com.spareroom.ui.filters.adapter.viewparams.IResettableParams;

import androidx.annotation.Nullable;

public class MinMaxEditTextParams implements IResettableParams {
    private int iconId;
    private String title;
    private String minHint;
    private String minTitle;
    private String maxHint;
    private String maxTitle;
    private String textBase;
    private int minMaxCharacters;
    private int maxMaxCharacters;
    private Integer min;
    private Integer max;

    private String tag;

    public MinMaxEditTextParams(String tag) {
        this.tag = tag;
    }

    @Override
    public void reset() {
        min(null);
        max(null);
    }

    @Override
    public ViewParamsType type() {
        return ViewParamsType.MIN_MAX;
    }

    @Override
    public String tag() {
        return tag;
    }

    public int iconId() {
        return iconId;
    }

    public void iconId(int iconId) {
        this.iconId = iconId;
    }

    public String title() {
        return title;
    }

    public void title(String title) {
        this.title = title;
    }

    public String minHint() {
        return minHint;
    }

    public void minHint(String minHint) {
        this.minHint = minHint;
    }

    public String minTitle() {
        return minTitle;
    }

    public void minTitle(String minTitle) {
        this.minTitle = minTitle;
    }

    public String maxHint() {
        return maxHint;
    }

    public void maxHint(String maxHint) {
        this.maxHint = maxHint;
    }

    public String maxTitle() {
        return maxTitle;
    }

    public void maxTitle(String maxTitle) {
        this.maxTitle = maxTitle;
    }

    public String textBase() {
        return textBase;
    }

    public void textBase(String textBase) {
        this.textBase = textBase;
    }

    public int minMaxCharacters() {
        return minMaxCharacters;
    }

    public void minMaxCharacters(int minMaxCharacters) {
        this.minMaxCharacters = minMaxCharacters;
    }

    public int maxMaxCharacters() {
        return maxMaxCharacters;
    }

    public void maxMaxCharacters(int maxMaxCharacters) {
        this.maxMaxCharacters = maxMaxCharacters;
    }

    @Nullable
    public Integer min() {
        return min;
    }

    public void min(@Nullable Integer currentMinValue) {
        this.min = currentMinValue;
    }

    @Nullable
    public Integer max() {
        return max;
    }

    public void max(@Nullable Integer currentMaxValue) {
        this.max = currentMaxValue;
    }

}
