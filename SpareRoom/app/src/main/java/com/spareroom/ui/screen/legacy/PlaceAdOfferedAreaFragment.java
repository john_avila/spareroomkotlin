package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.controller.legacy.*;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.model.business.*;
import com.spareroom.ui.util.*;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

public class PlaceAdOfferedAreaFragment extends PlaceAdFragment implements Injectable {
    private AsyncTaskManager _asyncManager = new AsyncTaskManager();
    private ListView _lvAreas;
    private ProgressBar _pb;
    private DraftAdOffered _draftAd;

    @Inject
    ConnectivityChecker connectivityChecker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        _asyncManager = new AsyncTaskManager();
        setTitle(getString(R.string.place_ad_offered_area_title));
        View rootView = inflater.inflate(R.layout.place_ad_offered_area, container, false);
        _lvAreas = rootView.findViewById(R.id.place_ad_offered_area_lv);
        _pb = rootView.findViewById(R.id.place_ad_offered_area_pb);

        _pb.setVisibility(View.VISIBLE);

        _draftAd = (DraftAdOffered) getDraftAd();

        String postcode = ((DraftAdOffered) getDraftAd()).get_postCode();

        SpareroomApplication app = (SpareroomApplication.getInstance(getActivity().getApplicationContext()));

        FindAreaByPostcodeAsyncTask findAreaAsyncTask = new FindAreaByPostcodeAsyncTask(app.getPlaceAdFacade(), areasAsyncResult);
        _asyncManager.register(findAreaAsyncTask);
        findAreaAsyncTask.execute(postcode);

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        _asyncManager.cancelAll();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        _lvAreas.setAdapter(null);
    }

    private final IAsyncResult areasAsyncResult = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            if (o instanceof PlaceAdPostcodeAreaList) {
                _pb.setVisibility(View.GONE);

                if (noAreas(((PlaceAdPostcodeAreaList) o).get_lAreas()))
                    return;

                _lvAreas.setAdapter(new AreasAdapter((PlaceAdPostcodeAreaList) o));
                _draftAd.set_lPostcodeAreaList((PlaceAdPostcodeAreaList) o);
            }
            if (o instanceof SpareroomStatus) {
                Toast.makeText(getActivity(), ((SpareroomStatus) o).getMessage(), Toast.LENGTH_LONG).show();
                ((PlaceAdAbstractActivity) getActivity()).previousScreen();
            }
        }

        @Override
        public void handleServiceUnavailableException(String message) {
            handleError(message);
        }
    };

    private boolean noAreas(List<PlaceAdPostcodeArea> areas) {
        if (areas == null || areas.isEmpty()) {
            showErrorMessage(getString(R.string.no_areas));
            return true;
        }

        return false;
    }

    private void handleError(String message) {
        _pb.setVisibility(View.GONE);
        if (!connectivityChecker.isConnected()) {
            showErrorMessage(getString(R.string.no_connection));
        } else if (!StringUtils.isNullOrEmpty(message)) {
            showErrorMessage(message);
        } else {
            showErrorMessage(getString(R.string.error_message));
        }
    }

    private void showErrorMessage(String message) {
        if (UiUtils.isFragmentAlive(this))
            SnackBarUtils.show(_pb, message);
    }

    private class AreasAdapter extends BaseAdapter {
        private final PlaceAdPostcodeAreaList _postcodeAreaList;
        private final LinkedList<PlaceAdPostcodeArea> _lAreas;

        private int positionSelected = -1;

        AreasAdapter(PlaceAdPostcodeAreaList postcodeAreaList) {
            _postcodeAreaList = postcodeAreaList;
            _lAreas = _postcodeAreaList.get_lAreas();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.place_ad_offered_areas_option_chosen, parent, false);
            }

            ((TextView) convertView.findViewById(R.id.place_ad_offered_areas_option_tvOption)).setText(_lAreas.get(position).get_name());

            if ((positionSelected == position) || ((_draftAd.get_area() != null) && _draftAd.get_area().equals(_lAreas.get(position).get_id()))) {
                positionSelected = position;
                convertView.setBackgroundResource(R.color.light_blue_2);
                convertView.findViewById(R.id.place_ad_offered_areas_option_chosen_ivChosen).setVisibility(View.VISIBLE);
                convertView.setOnClickListener(new UnselectAreaOnClickListener(position));
            } else {
                convertView.setBackgroundResource(R.color.white);
                convertView.findViewById(R.id.place_ad_offered_areas_option_chosen_ivChosen).setVisibility(View.GONE);
                convertView.setOnClickListener(new SelectAreaOnClickListener(position));
            }
            return convertView;
        }

        private class UnselectAreaOnClickListener implements View.OnClickListener {
            final int _position;

            UnselectAreaOnClickListener(int position) {
                _position = position;

            }

            @Override
            public void onClick(View v) {
                positionSelected = -1;
                _draftAd.set_area(null);
                v.setBackgroundResource(R.color.white);
                v.findViewById(R.id.place_ad_offered_areas_option_chosen_ivChosen).setVisibility(View.GONE);
                v.setOnClickListener(new SelectAreaOnClickListener(_position));
            }
        }

        private class SelectAreaOnClickListener implements View.OnClickListener {
            final int _position;

            SelectAreaOnClickListener(int position) {
                _position = position;
            }

            @Override
            public void onClick(View v) {
                positionSelected = _position;
                _draftAd.set_area(_lAreas.get(_position).get_id());

                notifyDataSetChanged();

                finish();
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return _lAreas.get(position);
        }

        @Override
        public int getCount() {
            return (_lAreas != null) ? _lAreas.size() : 0;
        }
    }

}
