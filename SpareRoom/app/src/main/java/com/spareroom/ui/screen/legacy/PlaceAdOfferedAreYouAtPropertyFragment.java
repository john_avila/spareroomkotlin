package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.ui.widget.PlaceAdButton;

public class PlaceAdOfferedAreYouAtPropertyFragment extends PlaceAdFragment {
    private PlaceAdButton _previouslySelected;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_offered_are_you_at_property, container, false);

        setTitle(getString(R.string.place_ad_offered_are_you_at_property_title));

        final PlaceAdButton bYes = rootView.findViewById(R.id.place_ad_offered_are_you_at_property_bYes);
        final PlaceAdButton bNo = rootView.findViewById(R.id.place_ad_offered_are_you_at_property_bNo);

        Boolean isAtProperty = ((DraftAdOffered) getDraftAd()).is_isAtProperty();

        if (isAtProperty != null) {
            if (isAtProperty) {
                bYes.setPreviouslySelected();
                _previouslySelected = bYes;
            } else {
                bNo.setPreviouslySelected();
                _previouslySelected = bNo;
            }
        }

        bYes.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bYes.setPreviouslySelected();
            ((DraftAdOffered) getDraftAd()).set_isAtProperty(true);
            ((DraftAdOffered) getDraftAd()).set_postCode("");
            PlaceAdOfferedAreYouAtPropertyFragment.this.finish();
        });

        bNo.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bNo.setPreviouslySelected();
            ((DraftAdOffered) getDraftAd()).set_isAtProperty(false);
            PlaceAdOfferedAreYouAtPropertyFragment.this.finish();
        });

        return rootView;
    }
}
