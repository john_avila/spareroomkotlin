package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.NoOfRooms;
import com.spareroom.ui.filters.adapter.viewparams.*;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import java.util.*;

import androidx.annotation.NonNull;

class FilterOfferedPropertyTypeItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.propertyType;
    private final static String KEY_NO_ROOM_GROUP = "key_no_room_group";

    private final static String NUMBER_OF_BEDROOMS_TITLE = "number_of_bedrooms_title";
    private final static String AMENITIES_TITLE = "amenities_title";
    final static String ROOMS = "rooms";
    final static String STUDIOS = "studios";
    final static String WHOLE_PROPERTY = "whole_property";
    final static String ONE_BEDROOM = "one_bedroom";
    final static String TWO_BEDROOMS = "two_bedrooms";
    final static String THREE_BEDROOMS = "three_bedrooms";
    final static String PARKING = "parking";
    final static String SHARED_LIVING_ROOM = "shared_living_room";
    final static String DISABLED_ACCESS = "disabled_access";

    private final List<GroupedCompoundButtonParams> roomGroup = new ArrayList<>();
    private final Map<String, List<GroupedCompoundButtonParams>> groupList = new HashMap<>();

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesOffered previousProperties = (SearchAdvertListPropertiesOffered) properties;
        List<FilterItem> list = new ArrayList<>();
        roomGroup.clear();
        groupList.clear();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        CompoundButtonParams rooms = new CompoundButtonParams(ROOMS);
        rooms.size(CompoundButtonParams.Size.MEDIUM);
        rooms.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        rooms.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        rooms.enableHighlightBar(true);
        rooms.text(application.getString(R.string.rooms));
        rooms.textColorResId(R.color.black);
        rooms.iconResId(R.drawable.ic_room);
        rooms.selected(previousProperties.getShowRooms());
        list.add(new FilterItem<>(rooms));

        CompoundButtonParams studios = new CompoundButtonParams(STUDIOS);
        studios.size(CompoundButtonParams.Size.MEDIUM);
        studios.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        studios.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        studios.enableHighlightBar(true);
        studios.text(application.getString(R.string.studios1bedFlats));
        studios.textColorResId(R.color.black);
        studios.iconResId(R.drawable.ic_studio_apartment);
        studios.selected(previousProperties.getShowStudios());
        list.add(new FilterItem<>(studios));

        CompoundButtonParams wholeProperty = new CompoundButtonParams(WHOLE_PROPERTY);
        wholeProperty.size(CompoundButtonParams.Size.MEDIUM);
        wholeProperty.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        wholeProperty.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        wholeProperty.enableHighlightBar(true);
        wholeProperty.text(application.getString(R.string.wholeProperties));
        wholeProperty.textColorResId(R.color.black);
        wholeProperty.iconResId(R.drawable.ic_whole_property);
        wholeProperty.selected(previousProperties.getShowWholeProperty());
        list.add(new FilterItem<>(wholeProperty));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        TitleViewParams titleViewParams = new TitleViewParams(NUMBER_OF_BEDROOMS_TITLE);
        titleViewParams.title(application.getString(R.string.numberOfBedrooms));
        list.add(new FilterItem<>(titleViewParams));

        GroupedCompoundButtonParams oneBedroom = new GroupedCompoundButtonParams(ONE_BEDROOM);
        oneBedroom.size(CompoundButtonParams.Size.MEDIUM);
        oneBedroom.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        oneBedroom.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.GRAY);
        oneBedroom.enableHighlightBar(false);
        oneBedroom.text(application.getString(R.string.oneOrMoreBedrooms));
        oneBedroom.textColorResId(R.color.black);
        oneBedroom.iconResId(R.drawable.ic_one_room);
        oneBedroom.value(NoOfRooms.NOT_SET.toString());
        oneBedroom.defaultItem(true);
        oneBedroom.groupName(KEY_NO_ROOM_GROUP);
        roomGroup.add(oneBedroom);
        list.add(new FilterItem<>(oneBedroom));

        GroupedCompoundButtonParams twoBedrooms = new GroupedCompoundButtonParams(TWO_BEDROOMS);
        twoBedrooms.size(CompoundButtonParams.Size.MEDIUM);
        twoBedrooms.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        twoBedrooms.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        twoBedrooms.enableHighlightBar(true);
        twoBedrooms.text(application.getString(R.string.twoOrMoreBedrooms));
        twoBedrooms.textColorResId(R.color.black);
        twoBedrooms.iconResId(R.drawable.ic_two_rooms);
        twoBedrooms.value(NoOfRooms.MIN_TWO.toString());
        twoBedrooms.groupName(KEY_NO_ROOM_GROUP);
        roomGroup.add(twoBedrooms);
        list.add(new FilterItem<>(twoBedrooms));

        GroupedCompoundButtonParams threeBedrooms = new GroupedCompoundButtonParams(THREE_BEDROOMS);
        threeBedrooms.size(CompoundButtonParams.Size.MEDIUM);
        threeBedrooms.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        threeBedrooms.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        threeBedrooms.enableHighlightBar(true);
        threeBedrooms.text(application.getString(R.string.threeOrMoreBedrooms));
        threeBedrooms.textColorResId(R.color.black);
        threeBedrooms.iconResId(R.drawable.ic_three_rooms);
        threeBedrooms.value(NoOfRooms.MIN_THREE.toString());
        threeBedrooms.groupName(KEY_NO_ROOM_GROUP);
        roomGroup.add(threeBedrooms);
        list.add(new FilterItem<>(threeBedrooms));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        titleViewParams = new TitleViewParams(AMENITIES_TITLE);
        titleViewParams.title(application.getString(R.string.amenities));
        list.add(new FilterItem<>(titleViewParams));

        CompoundButtonParams parking = new CompoundButtonParams(PARKING);
        parking.size(CompoundButtonParams.Size.MEDIUM);
        parking.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        parking.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        parking.enableHighlightBar(true);
        parking.text(application.getString(R.string.parking));
        parking.textColorResId(R.color.black);
        parking.iconResId(R.drawable.ic_parking);
        parking.selected(previousProperties.getParking());
        list.add(new FilterItem<>(parking));

        CompoundButtonParams sharedLivingRoom = new CompoundButtonParams(SHARED_LIVING_ROOM);
        sharedLivingRoom.size(CompoundButtonParams.Size.MEDIUM);
        sharedLivingRoom.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        sharedLivingRoom.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        sharedLivingRoom.enableHighlightBar(true);
        sharedLivingRoom.text(application.getString(R.string.sharedLivingRoom));
        sharedLivingRoom.textColorResId(R.color.black);
        sharedLivingRoom.iconResId(R.drawable.ic_shared_living_room);
        sharedLivingRoom.selected(previousProperties.getLivingRoom());
        list.add(new FilterItem<>(sharedLivingRoom));

        CompoundButtonParams disabledAccess = new CompoundButtonParams(DISABLED_ACCESS);
        disabledAccess.size(CompoundButtonParams.Size.MEDIUM);
        disabledAccess.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        disabledAccess.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        disabledAccess.enableHighlightBar(true);
        disabledAccess.text(application.getString(R.string.disabledAccess));
        disabledAccess.textColorResId(R.color.black);
        disabledAccess.iconResId(R.drawable.ic_disabled_access);
        disabledAccess.selected(previousProperties.getDisabledAccess());
        list.add(new FilterItem<>(disabledAccess));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        groupList.put(KEY_NO_ROOM_GROUP, roomGroup);
        setGroupPreviousSearch(previousProperties);

        this.list = list;
    }

    private void setGroupPreviousSearch(SearchAdvertListPropertiesOffered properties) {
        Set<String> groupListKeySet = groupList.keySet();

        for (String key : groupListKeySet) {
            List<GroupedCompoundButtonParams> group = groupList.get(key);

            for (GroupedCompoundButtonParams params : group) {
                switch (key) {
                    case KEY_NO_ROOM_GROUP:
                        params.selected((properties.getNoOfRooms() != NoOfRooms.NOT_SET) && params.value().equals(properties.getNoOfRooms().toString()));
                        break;
                }
            }
            selectedItem(group);
        }

    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return groupList;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesOffered propertiesOffered = (SearchAdvertListPropertiesOffered) propertiesToUpdate;
        for (FilterItem item : list) {
            switch (item.tag()) {
                case ROOMS:
                    propertiesOffered.setShowRooms(((CompoundButtonParams) item.params()).selected());
                    break;
                case STUDIOS:
                    propertiesOffered.setShowStudios(((CompoundButtonParams) item.params()).selected());
                    break;
                case WHOLE_PROPERTY:
                    propertiesOffered.setShowWholeProperty(((CompoundButtonParams) item.params()).selected());
                    break;
                case PARKING:
                    propertiesOffered.setParking(((CompoundButtonParams) item.params()).selected());
                    break;
                case SHARED_LIVING_ROOM:
                    propertiesOffered.setLivingRoom(((CompoundButtonParams) item.params()).selected());
                    break;
                case DISABLED_ACCESS:
                    propertiesOffered.setDisabledAccess(((CompoundButtonParams) item.params()).selected());
                    break;
            }
        }
        propertiesOffered.setNoOfRooms(NoOfRooms.valueOf(selectedValue(groupList.get(KEY_NO_ROOM_GROUP))));

    }

}
