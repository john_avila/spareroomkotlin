package com.spareroom.ui.util;

import android.app.Application;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;

import com.google.android.gms.common.GoogleApiAvailability;
import com.spareroom.R;

import javax.inject.Inject;

import static com.google.android.gms.common.ConnectionResult.*;

public class GoogleApiUtil {
    private static final int REQUEST_CODE = 0;

    private final Context context;
    private final GoogleApiAvailability googleApiAvailability;
    private PendingIntent pendingIntent;

    private int actionTextResId;
    private int messageResId;

    public enum GoogleApiConnectionResult {SERVICE_DISABLED, UPDATE_REQUIRED, SUCCESS}

    @Inject
    public GoogleApiUtil(Application context) {
        this.context = context;
        googleApiAvailability = GoogleApiAvailability.getInstance();
    }

    public GoogleApiConnectionResult isGooglePlayServicesAvailable() {
        int connectionResult = googleApiAvailability.isGooglePlayServicesAvailable(context);

        if (connectionResult != SUCCESS) {
            pendingIntent =
                    googleApiAvailability
                            .getErrorResolutionPendingIntent(
                                    context,
                                    connectionResult == SERVICE_DISABLED ? SERVICE_DISABLED : SERVICE_VERSION_UPDATE_REQUIRED,
                                    REQUEST_CODE);
        }

        switch (connectionResult) {
            case SUCCESS:
                return GoogleApiConnectionResult.SUCCESS;

            case SERVICE_DISABLED:
                actionTextResId = R.string.enable;
                messageResId = R.string.enableGooglePlayServices;
                return GoogleApiConnectionResult.SERVICE_DISABLED;

            default:
                actionTextResId = R.string.update;
                messageResId = R.string.updateGooglePlayServices;
                return GoogleApiConnectionResult.UPDATE_REQUIRED;
        }
    }

    public boolean enableServices() {
        try {
            pendingIntent.send();
            return true;
        } catch (CanceledException e) {
            return false;
        }

    }

    public int actionTextResId() {
        return actionTextResId;
    }

    public int messageResId() {
        return messageResId;
    }

}
