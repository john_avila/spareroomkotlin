package com.spareroom.ui.util

import android.app.Activity
import android.net.Uri
import android.os.Build
import com.spareroom.R
import com.spareroom.controller.AppVersion
import com.spareroom.controller.SpareroomApplication
import java.util.*

private const val TABLET = "Tablet"
private const val PHONE = "Phone"
private const val NO_VALUE = ""
private const val UPGRADED = "Upgraded"
private const val NOT_UPGRADED = "Not upgraded"

object Feedback {

    @JvmStatic
    fun feedbackUrl(activity: Activity): Uri {
        return Uri.parse(StringBuilder()
                .append("https://www.surveymonkey.co.uk/r/spareroom?os=Android")
                .append("&osv=${Build.VERSION.RELEASE}")
                .append("&dvc=${if (activity.resources.getBoolean(R.bool.isTablet)) TABLET else PHONE}")
                .append("&brd=${Build.MANUFACTURER}")
                .append("&mdl=${Build.MODEL}")
                .append("&vrsn=${AppVersion.versionName()}")
                .append("&cntry=${activity.getString(R.string.country)}")
                .append("&jrny=${firstInstallTimeInDays(activity)}")
                .append("&id=${userId()}")
                .append("&sts=${upgradeStatus()}")
                .toString())
    }

    private fun firstInstallTimeInDays(activity: Activity): String {
        return try {
            val updateTime = activity.packageManager.getPackageInfo(activity.packageName, 0).firstInstallTime
            val updateTimeCalendar = Calendar.getInstance()
            updateTimeCalendar.timeInMillis = updateTime

            DateUtils.getTimeDifferenceInDays(Calendar.getInstance(), updateTimeCalendar).toString()
        } catch (e: Exception) {
            NO_VALUE
        }
    }

    private fun userId(): String {
        val userId = SpareroomApplication.getSpareRoomContext().userId
        return if (userId.isNotEmpty()) userId else NO_VALUE
    }

    private fun upgradeStatus(): String {
        return if (SpareroomApplication.getSpareRoomContext().isUserUpgraded) UPGRADED else NOT_UPGRADED
    }
}