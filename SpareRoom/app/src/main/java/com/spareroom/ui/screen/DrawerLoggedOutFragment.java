package com.spareroom.ui.screen;

import android.content.Intent;
import android.os.Bundle;
import android.view.*;

import com.spareroom.R;

import static android.app.Activity.RESULT_CANCELED;
import static com.spareroom.ui.screen.NewExistingFragment.*;
import static com.spareroom.ui.screen.legacy.MainActivity.REQUEST_CODE_NAVIGATION_DRAWER;

/**
 * Side menu for logged in user.
 * <p/>
 * Created by miguel.rossi on 20/01/2017.
 */

public class DrawerLoggedOutFragment extends DrawerFragment {

    //region METHODS CONTROLLER

    @Override
    void launchSavedSearches() {
        launchDefault(EXTRA_ACTION_CODE_SAVED_SEARCHES);
    }

    @Override
    void launchMessages() {
        launchDefault(EXTRA_ACTION_CODE_VIEW_MESSAGES);
    }

    @Override
    void launchSavedAdverts() {
        launchDefault(EXTRA_ACTION_CODE_SAVED_ADVERTS);
    }

    @Override
    public void launchMyAdverts() {
        launchDefault(EXTRA_ACTION_CODE_MY_ADVERTS);
    }

    @Override
    public void launchEditMyDetails() {
        launchDefault(EXTRA_ACTION_CODE_PROFILE);
    }

    @Override
    public void launchUpgrade() {
        launchDefault(EXTRA_ACTION_CODE_UPGRADE);
    }

    private void launchDefault(String actionCode) {
        Intent intent = new Intent();
        intent.setClass(getActivity(), NewExistingActivity.class);
        intent.putExtra(EXTRA_ACTION_CODE, actionCode);
        getActivity().startActivityForResult(intent, REQUEST_CODE_NAVIGATION_DRAWER);

    }

    //endregion METHODS CONTROLLER

    //region METHODS LIFECYCLE

    public static DrawerLoggedOutFragment getInstance() {
        Bundle args = new Bundle();
        args.putString(HIGHLIGHT_OPTION, HIGHLIGHT_HOME);
        DrawerLoggedOutFragment drawer = new DrawerLoggedOutFragment();
        drawer.setArguments(args);
        return drawer;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // Shows the beta and log out options
        setLogInViews(false);

        // Sets the header
        setHeader(R.layout.material_navigation_drawer_header_anonymous);
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // The user hasn't logged in when asked
        if (requestCode == REQUEST_CODE_NAVIGATION_DRAWER && resultCode == RESULT_CANCELED)
            highlight(HIGHLIGHT_HOME);

    }

    //endregion METHODS LIFECYCLE

}
