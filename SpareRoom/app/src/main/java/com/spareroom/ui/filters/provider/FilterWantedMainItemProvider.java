package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListProperties.*;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted.Couples;
import com.spareroom.ui.filters.adapter.viewparams.*;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import java.util.*;

import androidx.annotation.NonNull;

public class FilterWantedMainItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.filter;

    private static final String SUBTITLE_EMPTY_STRING = "";

    static final String WANTED_MONTHLY_BUDGET = "wanted_monthly_budget";
    static final String WANTED_AVAILABILITY = "wanted_availability";
    static final String WANTED_BEDROOM_PREFERENCE = "wanted_bedroom_preference";
    static final String WANTED_GENDER = "wanted_gender";
    static final String WANTED_AGE = "wanted_age";
    static final String WANTED_EMPLOYMENT_STATUS = "wanted_employment_status";
    static final String WANTED_RELATIONSHIP = "wanted_relationship";
    static final String WANTED_SMOKING = "wanted_smoking";
    static final String WANTED_WITH_PHOTOS = "wanted_with_photos";
    static final String WANTED_UNSUITABLE = "wanted_unsuitable";

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesWanted previousProperties = (SearchAdvertListPropertiesWanted) properties;
        List<FilterItem> list = new ArrayList<>();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        SummaryViewParams monthlyBudget = new SummaryViewParams(WANTED_MONTHLY_BUDGET);
        monthlyBudget.iconDrawableId(R.drawable.ic_budget);
        monthlyBudget.titleStringId(R.string.monthly_budget);
        monthlyBudget.defaultSubtitleString(application.getString(R.string.noPreference));
        monthlyBudget.subtitleString(getMonthlyBudgetSubtitle(previousProperties));
        list.add(new FilterItem<>(monthlyBudget));

        SummaryViewParams availability = new SummaryViewParams(WANTED_AVAILABILITY);
        availability.iconDrawableId(R.drawable.ic_availability);
        availability.titleStringId(R.string.availability);
        availability.defaultSubtitleString(application.getString(R.string.noPreference));
        availability.subtitleString(getAvailabilitySubtitle(previousProperties));
        list.add(new FilterItem<>(availability));

        SummaryViewParams bedroomPreference = new SummaryViewParams(WANTED_BEDROOM_PREFERENCE);
        bedroomPreference.iconDrawableId(R.drawable.ic_double_bed);
        bedroomPreference.titleStringId(R.string.bedroomPreference);
        bedroomPreference.defaultSubtitleString(application.getString(R.string.noPreference));
        bedroomPreference.subtitleString(getBedroomTypeSubtitle(previousProperties));
        list.add(new FilterItem<>(bedroomPreference));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        SummaryViewParams gender = new SummaryViewParams(WANTED_GENDER);
        gender.iconDrawableId(R.drawable.ic_generic_user);
        gender.titleStringId(R.string.genderSexuality);
        gender.defaultSubtitleString(application.getString(R.string.noPreference));
        gender.subtitleString(getGenderSexualitySubtitle(previousProperties));
        list.add(new FilterItem<>(gender));

        SummaryViewParams age = new SummaryViewParams(WANTED_AGE);
        age.iconDrawableId(R.drawable.ic_age);
        age.titleStringId(R.string.age);
        age.defaultSubtitleString(application.getString(R.string.noPreference));
        age.subtitleString(getAgeSubtitle(previousProperties));
        list.add(new FilterItem<>(age));

        SummaryViewParams employmentStatus = new SummaryViewParams(WANTED_EMPLOYMENT_STATUS);
        employmentStatus.iconDrawableId(R.drawable.ic_professional);
        employmentStatus.titleStringId(R.string.employmentStatus);
        employmentStatus.defaultSubtitleString(application.getString(R.string.noPreference));
        employmentStatus.subtitleString(getEmploymentStatusSubtitle(previousProperties));
        list.add(new FilterItem<>(employmentStatus));

        SummaryViewParams relationship = new SummaryViewParams(WANTED_RELATIONSHIP);
        relationship.iconDrawableId(R.drawable.ic_couple);
        relationship.titleStringId(R.string.relationshipStatus);
        relationship.defaultSubtitleString(application.getString(R.string.noPreference));
        relationship.subtitleString(getRelationshipSubtitle(previousProperties));
        list.add(new FilterItem<>(relationship));

        SummaryViewParams smoking = new SummaryViewParams(WANTED_SMOKING);
        smoking.iconDrawableId(R.drawable.ic_smoking);
        smoking.titleStringId(R.string.smokingPreference);
        smoking.defaultSubtitleString(application.getString(R.string.noPreference));
        smoking.subtitleString(getSmokingSubtitle(previousProperties));
        list.add(new FilterItem<>(smoking));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        CompoundButtonParams withPhotos = new CompoundButtonParams(WANTED_WITH_PHOTOS);
        withPhotos.size(CompoundButtonParams.Size.MEDIUM);
        withPhotos.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        withPhotos.enableHighlightBar(true);
        withPhotos.text(application.getString(R.string.advertsWithPhotos));
        withPhotos.textColorResId(R.color.black);
        withPhotos.iconResId(R.drawable.ic_photos);
        withPhotos.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        withPhotos.selected(previousProperties.getPhotoAdvertsOnly());
        list.add(new FilterItem<>(withPhotos));

        CompoundButtonParams showHidden = new CompoundButtonParams(WANTED_UNSUITABLE);
        showHidden.size(CompoundButtonParams.Size.MEDIUM);
        showHidden.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        showHidden.enableHighlightBar(true);
        showHidden.text(application.getString(R.string.includeHidden));
        showHidden.textColorResId(R.color.black);
        showHidden.iconResId(R.drawable.ic_unsuitables);
        showHidden.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        showHidden.selected(previousProperties.getIncludeHidden());
        list.add(new FilterItem<>(showHidden));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.BIG)));

        this.list = list;
    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return null;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesWanted propertiesWanted = (SearchAdvertListPropertiesWanted) propertiesToUpdate;
        for (FilterItem item : list) {
            switch (item.tag()) {
                case WANTED_WITH_PHOTOS:
                    propertiesWanted.setPhotoAdvertsOnly(((CompoundButtonParams) item.params()).selected());
                    break;
                case WANTED_UNSUITABLE:
                    propertiesWanted.setIncludeHidden(((CompoundButtonParams) item.params()).selected());
                    break;
            }
        }

    }

    @NonNull
    String getMonthlyBudgetSubtitle(SearchAdvertListPropertiesWanted previousProperties) {
        Integer minRent = previousProperties.getMinRent();
        Integer maxRent = previousProperties.getMaxRent();

        if ((minRent != null) || (maxRent != null))
            return subtitleUtil.getRentAmountText(application, minRent, maxRent);
        else
            return SUBTITLE_EMPTY_STRING;
    }

    @NonNull
    String getAvailabilitySubtitle(SearchAdvertListPropertiesWanted previousProperties) {
        StringBuilder stringBuilder = new StringBuilder();

        String availableFrom = previousProperties.getAvailableFrom();
        if (availableFrom != null)
            stringBuilder.append(subtitleUtil.getMoveInText(availableFrom));

        boolean isMinTerm = previousProperties.getMinTerm() != MinTerm.NOT_SET;
        boolean isMaxTerm = previousProperties.getMaxTerm() != MaxTerm.NOT_SET;

        if (isMinTerm || isMaxTerm) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));

            stringBuilder.append(
                    subtitleUtil.getLengthOfStayText(
                            application,
                            isMinTerm ? previousProperties.getMinTerm().getIntValue() : null,
                            isMaxTerm ? previousProperties.getMaxTerm().getIntValue() : null));
        }

        if (previousProperties.getDaysOfWeekAvailable() != DaysOfWeek.NOT_SET) {
            String daysOfWeekAvailable = null;
            switch (previousProperties.getDaysOfWeekAvailable()) {
                case SEVEN_DAYS:
                    daysOfWeekAvailable = application.getString(R.string.allWeekRenters);
                    break;
                case WEEKDAYS:
                    daysOfWeekAvailable = application.getString(R.string.weekdayOnlyRenters);
                    break;
                case WEEKENDS:
                    daysOfWeekAvailable = application.getString(R.string.weekendOnlyRenters);
                    break;
            }

            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));
            stringBuilder.append(daysOfWeekAvailable);
        }

        return stringBuilder.toString();
    }

    @NonNull
    String getBedroomTypeSubtitle(SearchAdvertListPropertiesWanted previousProperties) {
        RoomTypes roomType = previousProperties.getRoomType();

        if (roomType != RoomTypes.NOT_SET)
            return subtitleUtil.getRoomTypeText(application, roomType);
        else
            return SUBTITLE_EMPTY_STRING;

    }

    @NonNull
    String getGenderSexualitySubtitle(SearchAdvertListPropertiesWanted previousProperties) {
        StringBuilder stringBuilder = new StringBuilder();

        GenderFilter genderFilter = previousProperties.getGenderFilter();
        if (genderFilter != GenderFilter.NOT_SET)
            stringBuilder.append(subtitleUtil.getGenderFilterText(application, genderFilter));

        if (previousProperties.getGayShare()) {
            if (stringBuilder.length() != 0)
                stringBuilder.append(application.getString(R.string.bulletPoint));
            stringBuilder.append(application.getString(R.string.lgbtq));
        }
        return stringBuilder.toString();
    }

    @NonNull
    String getAgeSubtitle(SearchAdvertListPropertiesWanted previousProperties) {
        Integer minAge = previousProperties.getMinAge();
        Integer maxAge = previousProperties.getMaxAge();

        if ((minAge != null) || (maxAge != null))
            return subtitleUtil.getAgeText(application, minAge, maxAge);
        else
            return SUBTITLE_EMPTY_STRING;

    }

    @NonNull
    String getEmploymentStatusSubtitle(SearchAdvertListPropertiesWanted previousProperties) {
        ShareType shareType = previousProperties.getShareType();

        if (shareType != ShareType.NOT_SET)
            return subtitleUtil.getEmploymentStatusText(application, shareType);
        else
            return SUBTITLE_EMPTY_STRING;

    }

    @NonNull
    String getRelationshipSubtitle(SearchAdvertListPropertiesWanted previousProperties) {
        Couples couples = previousProperties.getCouples();

        if (couples == Couples.YES)
            return application.getString(R.string.couples);
        if (couples == Couples.NO)
            return application.getString(R.string.noCouples);
        else
            return SUBTITLE_EMPTY_STRING;

    }

    @NonNull
    String getSmokingSubtitle(SearchAdvertListPropertiesWanted previousProperties) {
        Smoking smoking = previousProperties.getSmoking();

        if (smoking == Smoking.YES)
            return application.getString(R.string.smokers);
        if (smoking == Smoking.NO)
            return application.getString(R.string.nonSmokers);
        else
            return SUBTITLE_EMPTY_STRING;
    }

}
