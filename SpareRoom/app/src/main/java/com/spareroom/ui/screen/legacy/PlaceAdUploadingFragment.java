package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.view.animation.*;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.controller.legacy.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.model.business.*;
import com.spareroom.ui.anim.ProgressbarAnim;
import com.spareroom.ui.util.ToastUtils;
import com.spareroom.ui.util.UiUtils;

public class PlaceAdUploadingFragment extends PlaceAdFragment {
    private static final int PROGRESS_BAR_ANIMATION_TIME = 5000;
    private View _vProgress;
    private TextView _tvProgress;
    private int finalPixel;
    private Thread _countThread;
    private boolean apiFinished = false;
    private boolean animFinished = false;
    private AsyncTaskManager _atManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_uploading, container, false);
        setTitle(getString(R.string.place_ad_uploading_title));
        ImageView _ivGear = rootView.findViewById(R.id.place_ad_uploading_ivGear);
        _vProgress = rootView.findViewById(R.id.place_ad_uploading_vProgress);
        View _vProgressBackground = rootView.findViewById(R.id.place_ad_uploading_rlProgress);
        _tvProgress = rootView.findViewById(R.id.place_ad_uploading_tvProgress);

        RotateAnimation anim = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(4000);
        _ivGear.startAnimation(anim);

        finalPixel = _vProgressBackground.getLayoutParams().width;

        ProgressbarAnim barAnim = new ProgressbarAnim(_vProgress, finalPixel);
        barAnim.setInterpolator(new AccelerateDecelerateInterpolator());
        barAnim.setDuration(PROGRESS_BAR_ANIMATION_TIME);
        _vProgress.startAnimation(barAnim);

        animateForwardProgressCount();

        DraftAd draft = getDraftAd();
        SpareroomApplication application = SpareroomApplication.getInstance(getActivity().getApplicationContext());

        Session session = application.getSpareroomContext().getSession();
        if (session != null) {
            draft.set_firstName(session.get_user().get_firstName());
            draft.set_lastName(session.get_user().get_lastName());
            draft.set_email(session.get_email());
        }

        PlaceAdUploadAsyncTask at =
                new PlaceAdUploadAsyncTask(application.getPlaceAdFacade(), new AsyncResult((draft instanceof DraftAdOffered)));
        _atManager = new AsyncTaskManager();
        _atManager.register(at);
        at.execute(draft);

        return rootView;
    }

    private void uploadFinished() {
        apiFinished = true;
        if (animFinished)
            finishUploading();
    }

    private void uploadFinished(SpareroomStatus im) {
        apiFinished = true;

        if (im.getCode().equals(SpareroomStatusCode.CODE_SUCCESS)) {
            if (animFinished)
                finishUploading();
            if (im.getMessage() != null) {
                getDraftAd().setId(im.getMessage());
            }
        } else {
            Toast.makeText(getActivity(), im.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void uploadPicture() {
        NewPicture picture = new NewPicture();
        picture.setFlatshareId(getDraftAd().getId());
        picture.setUri(getDraftAd().getPicturePath());

        UploadPictureControllerAction uploadPictureControllerAction = UploadPictureControllerAction.get(new UploadPictureObserver());
        addObserver(uploadPictureControllerAction);
        uploadPictureControllerAction.execute(picture);
    }

    private class AsyncResult extends IAsyncResultImpl {
        private final boolean isOffered;

        AsyncResult(boolean isOffered) {
            this.isOffered = isOffered;
        }

        @Override
        public void update(Object o) {
            SpareroomStatus im = (SpareroomStatus) o;

            if (im.getCode().equals(SpareroomStatusCode.CODE_SUCCESS)) {
                if (im.getMessage() != null)
                    getDraftAd().setId(im.getMessage());
                if (getDraftAd().hasPicture())
                    uploadPicture();
                else
                    uploadFinished(im);
            } else {
                if (isOffered)
                    AnalyticsTrackerComposite.getInstance().trackEvent_PlaceAd_PlaceOfferedAdWithErrors();
                else
                    AnalyticsTrackerComposite.getInstance().trackEvent_PlaceAd_PlaceWantedAdWithErrors();
                ((PlaceAdAbstractActivity) getActivity()).handleError(im);
            }
        }

        @Override
        public void handleServiceUnavailableException(String message) {
            ToastUtils.showToast(R.string.no_connection);
            ((PlaceAdAbstractActivity) getActivity()).previousScreen();
        }

    }

    private final class UploadPictureObserver implements ControllerActionObserver {

        @Override
        public void onResult(Object o) {
            uploadFinished();
        }

        @Override
        public void onSpareroomStatus(SpareroomStatus status) {
            ToastUtils.showToast(R.string.activity_manage_photos_tPictureUploadedError);
            uploadFinished();
        }

        @Override
        public void onException(Exception exception) {
            ToastUtils.showToast(R.string.activity_manage_photos_tPictureUploadedError);
            uploadFinished();
        }
    }

    private void animateForwardProgressCount() {
        final int originalProgress = 0;
        final int finalProgress = 100;
        final float updateWait = (float) PROGRESS_BAR_ANIMATION_TIME / (float) (finalProgress - originalProgress);

        _countThread = new Thread(new Runnable() {
            private int currentProgress = originalProgress;

            public void run() {
                while (currentProgress < finalProgress) {
                    if (Thread.interrupted())
                        return;
                    currentProgress++;
                    try {
                        Thread.sleep((long) updateWait);
                    } catch (InterruptedException e) {
                        return;
                    }
                    if (currentProgress == 100) {
                        animFinished = true;
                        if (apiFinished)
                            finishUploading();
                    } else
                        _tvProgress.post(new ProgressCountUpdater(currentProgress));

                }
            }
        });
        _countThread.start();
    }

    private void finishUploading() {
        if (UiUtils.isFragmentAlive(this)) {
            getActivity().runOnUiThread(() -> {
                _tvProgress.setText(R.string.placeAdUploadingFragment_complete);
                _vProgress.getLayoutParams().width = finalPixel;
                _vProgress.requestLayout();
                finish();
            });
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        _atManager.cancelAll();
        _countThread.interrupt();
    }

    private class ProgressCountUpdater implements Runnable {
        private final int _progress;

        ProgressCountUpdater(int progress) {
            this._progress = progress;
        }

        public void run() {
            _tvProgress.setText(String.format(AppVersion.flavor().getLocale(), "%d%%", _progress));
        }
    }
}
