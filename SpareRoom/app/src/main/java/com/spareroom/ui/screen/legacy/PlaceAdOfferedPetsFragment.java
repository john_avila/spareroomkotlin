package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.ui.widget.PlaceAdButton;

public class PlaceAdOfferedPetsFragment extends PlaceAdFragment {

    //region UI

    private PlaceAdButton _previouslySelected;

    //endregion UI

    //region METHODS LIFECYCLE

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.place_ad_offered_pets, container, false);

        setTitle(getString(R.string.place_ad_offered_pets_title));

        final PlaceAdButton bYes = rootView.findViewById(R.id.place_ad_offered_pets_bYes);
        final PlaceAdButton bNo = rootView.findViewById(R.id.place_ad_offered_pets_bNo);

        final DraftAdOffered draft = ((DraftAdOffered) getDraftAd());

        // In case there is an activity state change and we lost the draft
        if (draft == null) {
            getActivity().finish();
            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }

        // If there is data from a previous advert process it is shown
        if (draft.is_isTherePet() != null) {
            if (draft.is_isTherePet()) {
                bYes.setPreviouslySelected();
                _previouslySelected = bYes;
            } else {
                bNo.setPreviouslySelected();
                _previouslySelected = bNo;
            }
        }

        bYes.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bYes.setPreviouslySelected();
            draft.set_isTherePet(true);
            PlaceAdOfferedPetsFragment.this.finish();
        });

        bNo.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bNo.setPreviouslySelected();
            draft.set_isTherePet(false);
            PlaceAdOfferedPetsFragment.this.finish();
        });

        return rootView;
    } //end onCreateView(LayoutInflater, ViewGroup, Bundle)

    //endregion METHODS LIFECYCLE

}
