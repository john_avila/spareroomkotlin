/**
 * View of the Model-View-Controller. Classes with directly related to the User Interface.
 */
package com.spareroom.ui;