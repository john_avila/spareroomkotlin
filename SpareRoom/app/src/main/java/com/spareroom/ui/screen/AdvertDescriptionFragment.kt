package com.spareroom.ui.screen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.spareroom.R
import kotlinx.android.synthetic.main.advert_description_fragment.*
import kotlinx.android.synthetic.main.app_bar_view.*

class AdvertDescriptionFragment : Fragment() {

    companion object {
        const val TAG = "AdvertDescriptionFragmentTag"
        const val TITLE_INTENT_KEY = "TitleIntentKey"
        const val DESCRIPTION_INTENT_KEY = "DescriptionIntentKey"

        fun getInstance(title: String?, description: String?): AdvertDescriptionFragment {
            val args = Bundle()
            args.putString(TITLE_INTENT_KEY, title)
            args.putString(DESCRIPTION_INTENT_KEY, description)

            val fragment = AdvertDescriptionFragment()
            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.advert_description_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)

        setToolbar(toolbar, getString(R.string.description), true, R.drawable.ic_close)
        txtAdvertTitle.text = arguments?.getString(TITLE_INTENT_KEY)?.trim()
        txtAdvertDescription.text = arguments?.getString(DESCRIPTION_INTENT_KEY)?.trim()
    }

    override fun onOptionsItemSelected(item: MenuItem) = handleHomeButton(item)

}
