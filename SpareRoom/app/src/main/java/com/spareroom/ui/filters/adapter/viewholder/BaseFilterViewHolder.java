package com.spareroom.ui.filters.adapter.viewholder;

import android.view.View;

import com.spareroom.ui.filters.provider.FilterItem;

import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseFilterViewHolder extends RecyclerView.ViewHolder {
    BaseFilterViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bind(FilterItem filterItem);

}
