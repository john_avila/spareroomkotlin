package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListProperties.Smoking;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import java.util.*;

import androidx.annotation.NonNull;

class FilterWantedSmokingItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.smokingPreference;
    private final static String KEY_SMOKING_GROUP = "key_smoking_group";

    final static String NON_SMOKERS = "non_Smokers";
    final static String SMOKERS = "smokers";
    final static String NO_PREFERENCE = "no_Preference";

    private final List<GroupedCompoundButtonParams> smokingGroup = new ArrayList<>();
    private final Map<String, List<GroupedCompoundButtonParams>> groupList = new HashMap<>();

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesWanted previousProperties = (SearchAdvertListPropertiesWanted) properties;
        List<FilterItem> list = new ArrayList<>();
        smokingGroup.clear();
        groupList.clear();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        GroupedCompoundButtonParams smokers = new GroupedCompoundButtonParams(SMOKERS);
        smokers.size(CompoundButtonParams.Size.MEDIUM);
        smokers.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        smokers.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        smokers.enableHighlightBar(true);
        smokers.text(application.getString(R.string.smokers));
        smokers.textColorResId(R.color.black);
        smokers.iconResId(R.drawable.ic_smoking);
        smokers.value(Smoking.YES.toString());
        smokers.groupName(KEY_SMOKING_GROUP);
        smokingGroup.add(smokers);
        list.add(new FilterItem<>(smokers));

        GroupedCompoundButtonParams nonSmokers = new GroupedCompoundButtonParams(NON_SMOKERS);
        nonSmokers.size(CompoundButtonParams.Size.MEDIUM);
        nonSmokers.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        nonSmokers.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        nonSmokers.enableHighlightBar(true);
        nonSmokers.text(application.getString(R.string.nonSmokers));
        nonSmokers.textColorResId(R.color.black);
        nonSmokers.iconResId(R.drawable.ic_non_smoking);
        nonSmokers.value(Smoking.NO.toString());
        nonSmokers.groupName(KEY_SMOKING_GROUP);
        smokingGroup.add(nonSmokers);
        list.add(new FilterItem<>(nonSmokers));

        GroupedCompoundButtonParams noPreference = new GroupedCompoundButtonParams(NO_PREFERENCE);
        noPreference.size(CompoundButtonParams.Size.MEDIUM);
        noPreference.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        noPreference.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.GRAY);
        noPreference.enableHighlightBar(false);
        noPreference.text(application.getString(R.string.noPreference));
        noPreference.textColorResId(R.color.black);
        noPreference.value(Smoking.NOT_SET.toString());
        noPreference.defaultItem(true);
        noPreference.groupName(KEY_SMOKING_GROUP);
        smokingGroup.add(noPreference);
        list.add(new FilterItem<>(noPreference));

        groupList.put(KEY_SMOKING_GROUP, smokingGroup);
        setGroupPreviousSearch(previousProperties);

        this.list = list;
    }

    private void setGroupPreviousSearch(SearchAdvertListPropertiesWanted properties) {
        Set<String> groupListKeySet = groupList.keySet();

        for (String key : groupListKeySet) {
            List<GroupedCompoundButtonParams> group = groupList.get(key);

            for (GroupedCompoundButtonParams params : group) {
                switch (key) {
                    case KEY_SMOKING_GROUP:
                        params.selected((properties.getSmoking() != Smoking.NOT_SET)
                                && params.value().equals(properties.getSmoking().toString()));
                        break;
                }
            }
            selectedItem(group);
        }

    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return groupList;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesWanted propertiesWanted = (SearchAdvertListPropertiesWanted) propertiesToUpdate;
        propertiesWanted.setSmoking(Smoking.valueOf(selectedValue(groupList.get(KEY_SMOKING_GROUP))));

    }

}
