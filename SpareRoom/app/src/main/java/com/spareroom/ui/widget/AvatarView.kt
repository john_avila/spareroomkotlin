package com.spareroom.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.spareroom.R
import com.spareroom.integration.imageloader.ImageLoader
import com.spareroom.ui.screen.Fragment
import com.spareroom.ui.util.StringUtils
import kotlinx.android.synthetic.main.avatar_view.view.*

class AvatarView : ConstraintLayout {

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private fun init(context: Context) {
        inflate(context, R.layout.avatar_view, this)
    }

    fun show() {
        visibility = VISIBLE
    }

    fun hide() {
        visibility = GONE
    }

    fun bind(avatarClickListener: OnClickListener, imageLoader: ImageLoader, fragment: Fragment, avatarUrl: String, fullName: String, firstName: String,
             lastName: String, description: String, extraInfo: String) {
        setTag(R.id.image_url_tag, avatarUrl)
        setOnClickListener(avatarClickListener)
        setInitials(firstName, lastName)
        setName(fullName)
        setExtraInfo(extraInfo)
        setUserDescription(description)
        loadImage(imageLoader, fragment, avatarUrl)
    }

    private fun setInitials(firstName: String, lastName: String) {
        txtUserInitials.text = StringUtils.getInitials(firstName, lastName)
    }

    private fun setName(fullName: String) {
        txtUserName.text = fullName
    }

    private fun setExtraInfo(extraInfo: String) {
        txtUserNameExtraData.text = extraInfo
        txtUserNameExtraData.visibility = if (extraInfo.isNotBlank()) View.VISIBLE else View.GONE
    }

    private fun setUserDescription(description: String) {
        txtUserDescription.text = description
        txtUserDescription.visibility = if (description.isNotBlank()) View.VISIBLE else View.GONE
    }

    private fun loadImage(imageLoader: ImageLoader, fragment: Fragment, avatarUrl: String) {
        imageLoader.clear(imgUserAvatar)

        if (avatarUrl.isNotBlank())
            imageLoader.loadCircularImage(avatarUrl, imgUserAvatar, fragment)
    }

}
