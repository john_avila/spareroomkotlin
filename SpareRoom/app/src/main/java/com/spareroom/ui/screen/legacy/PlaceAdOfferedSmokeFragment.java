package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.ui.widget.PlaceAdButton;

public class PlaceAdOfferedSmokeFragment extends PlaceAdFragment {
    private PlaceAdButton _previouslySelected;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_offered_smoke, container, false);

        setTitle(getString(R.string.place_ad_offered_smoke_title));

        final PlaceAdButton bYes = rootView.findViewById(R.id.place_ad_offered_smoke_bYes);
        final PlaceAdButton bNo = rootView.findViewById(R.id.place_ad_offered_smoke_bNo);
        final DraftAdOffered draft = ((DraftAdOffered) getDraftAd());

        // In case there is an activity state change and we lost the draft
        if (draft == null) {
            getActivity().finish();

            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }

        if (draft.is_areThereSmokers() != null) {
            if (draft.is_areThereSmokers()) {
                bYes.setPreviouslySelected();
                _previouslySelected = bYes;
            } else {
                bNo.setPreviouslySelected();
                _previouslySelected = bNo;
            }
        }

        bYes.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bYes.setPreviouslySelected();
            draft.set_areThereSmokers(true);
            PlaceAdOfferedSmokeFragment.this.finish();
        });

        bNo.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bNo.setPreviouslySelected();
            draft.set_areThereSmokers(false);
            PlaceAdOfferedSmokeFragment.this.finish();
        });

        return rootView;
    }
}
