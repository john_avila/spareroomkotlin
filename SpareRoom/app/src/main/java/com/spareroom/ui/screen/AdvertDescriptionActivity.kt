package com.spareroom.ui.screen

import android.content.Intent
import android.os.Bundle
import com.spareroom.R

class AdvertDescriptionActivity : Activity() {

    companion object {

        @JvmStatic
        fun start(activity: Activity, title: String?, description: String?) {
            val fullDescriptionIntent = Intent(activity, AdvertDescriptionActivity::class.java)
            fullDescriptionIntent.putExtra(AdvertDescriptionFragment.TITLE_INTENT_KEY, title)
            fullDescriptionIntent.putExtra(AdvertDescriptionFragment.DESCRIPTION_INTENT_KEY, description)
            activity.startActivity(fullDescriptionIntent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.advert_description_activity)

        if (savedInstanceState == null) {
            val title = intent.getStringExtra(AdvertDescriptionFragment.TITLE_INTENT_KEY)
            val description = intent.getStringExtra(AdvertDescriptionFragment.DESCRIPTION_INTENT_KEY)
            val fragment = AdvertDescriptionFragment.getInstance(title, description)
            supportFragmentManager.beginTransaction().replace(R.id.main_frame, fragment, AdvertDescriptionFragment.TAG).commit()
        }
    }
}