package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListProperties.DaysOfWeek;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted;
import com.spareroom.ui.filters.adapter.viewparams.AbstractSummaryViewParams.SummaryAction;
import com.spareroom.ui.filters.adapter.viewparams.*;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import java.util.*;

import androidx.annotation.NonNull;

class FilterWantedAvailabilityItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.availability;

    private final static String KEY_DAYS_REQUIRED_GROUP = "days_required_group";

    private final static String DAYS_AVAILABLE_TITLE = "days_available_title";
    private final static String LENGTH_OF_STAY_TITLE = "length_of_stay_title";
    final static String WEEKDAY_RENTERS = "weekday_renters";
    final static String ALL_WEEK_RENTERS = "all_week_renters";
    final static String WEEKEND_RENTERS = "weekend_renters";
    final static String NO_PREFERENCE = "no_preference";

    private final List<GroupedCompoundButtonParams> daysRequiredGroup = new ArrayList<>();
    private final Map<String, List<GroupedCompoundButtonParams>> groupList = new HashMap<>();

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesWanted previousProperties = (SearchAdvertListPropertiesWanted) properties;
        List<FilterItem> list = new ArrayList<>();

        daysRequiredGroup.clear();
        groupList.clear();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        SummaryViewDateParams moveIn = new SummaryViewDateParams(MOVE_IN);
        moveIn.iconDrawableId(R.drawable.ic_calendar);
        moveIn.titleStringId(R.string.moveInOn);
        moveIn.defaultSubtitleString(application.getString(R.string.noPreference));
        moveIn.summaryAction(SummaryAction.SHOW_DATE_DIALOG);
        if (previousProperties.getAvailableFrom() != null) {
            moveIn.subtitleString(subtitleUtil.getMoveInText(previousProperties.getAvailableFrom()));
            moveIn.date(previousProperties.getAvailableFrom());
        }
        list.add(new FilterItem<>(moveIn));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        TitleViewParams lengthOfStay = new TitleViewParams(LENGTH_OF_STAY_TITLE);
        lengthOfStay.title(application.getString(R.string.lengthOfStay));
        list.add(new FilterItem<>(lengthOfStay));

        SummaryViewMinTermParams lengthOfStayMin = new SummaryViewMinTermParams(LENGTH_OF_STAY_MIN);
        lengthOfStayMin.iconDrawableId(R.drawable.ic_length_of_stay);
        lengthOfStayMin.titleStringId(R.string.minimumTerm);
        lengthOfStayMin.defaultSubtitleString(application.getString(R.string.noPreference));
        lengthOfStayMin.summaryAction(SummaryAction.SHOW_SCROLL_DIALOG);
        lengthOfStayMin.subtitleString(subtitleUtil.getTermText(application, previousProperties.getMinTerm().getIntValue()));
        lengthOfStayMin.minTerm(previousProperties.getMinTerm());
        list.add(new FilterItem<>(lengthOfStayMin));

        SummaryViewMaxTermParams lengthOfStayMax = new SummaryViewMaxTermParams(LENGTH_OF_STAY_MAX);
        lengthOfStayMax.iconDrawableId(R.drawable.ic_length_of_stay);
        lengthOfStayMax.titleStringId(R.string.maximumTerm);
        lengthOfStayMax.defaultSubtitleString(application.getString(R.string.noPreference));
        lengthOfStayMax.summaryAction(SummaryAction.SHOW_SCROLL_DIALOG);
        lengthOfStayMax.subtitleString(subtitleUtil.getTermText(application, previousProperties.getMaxTerm().getIntValue()));
        lengthOfStayMax.maxTerm(previousProperties.getMaxTerm());
        list.add(new FilterItem<>(lengthOfStayMax));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        TitleViewParams titleViewParams = new TitleViewParams(DAYS_AVAILABLE_TITLE);
        titleViewParams.title(application.getString(R.string.daysAvailable));
        list.add(new FilterItem<>(titleViewParams));

        GroupedCompoundButtonParams allWeekRenters = new GroupedCompoundButtonParams(ALL_WEEK_RENTERS);
        allWeekRenters.size(CompoundButtonParams.Size.MEDIUM);
        allWeekRenters.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        allWeekRenters.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        allWeekRenters.enableHighlightBar(true);
        allWeekRenters.text(application.getString(R.string.allWeekRenters));
        allWeekRenters.textColorResId(R.color.black);
        allWeekRenters.subtitle(application.getString(R.string.peopleWantingRoomsFromMondayToSunday));
        allWeekRenters.iconResId(R.drawable.ic_all_week);
        allWeekRenters.value(DaysOfWeek.SEVEN_DAYS.toString());
        allWeekRenters.groupName(KEY_DAYS_REQUIRED_GROUP);
        daysRequiredGroup.add(allWeekRenters);
        list.add(new FilterItem<>(allWeekRenters));

        GroupedCompoundButtonParams weekdayRenters = new GroupedCompoundButtonParams(WEEKDAY_RENTERS);
        weekdayRenters.size(CompoundButtonParams.Size.MEDIUM);
        weekdayRenters.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        weekdayRenters.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        weekdayRenters.enableHighlightBar(true);
        weekdayRenters.text(application.getString(R.string.weekdayOnlyRenters));
        weekdayRenters.textColorResId(R.color.black);
        weekdayRenters.subtitle(application.getString(R.string.peopleWantingRoomsFromMondayToFriday));
        weekdayRenters.iconResId(R.drawable.ic_weekdays);
        weekdayRenters.value(DaysOfWeek.WEEKDAYS.toString());
        weekdayRenters.groupName(KEY_DAYS_REQUIRED_GROUP);
        daysRequiredGroup.add(weekdayRenters);
        list.add(new FilterItem<>(weekdayRenters));

        GroupedCompoundButtonParams weekendRenters = new GroupedCompoundButtonParams(WEEKEND_RENTERS);
        weekendRenters.size(CompoundButtonParams.Size.MEDIUM);
        weekendRenters.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        weekendRenters.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        weekendRenters.enableHighlightBar(true);
        weekendRenters.text(application.getString(R.string.weekendOnlyRenters));
        weekendRenters.textColorResId(R.color.black);
        weekendRenters.subtitle(application.getString(R.string.peopleWantingRoomsFromSaturdayAndSunday));
        weekendRenters.iconResId(R.drawable.ic_weekends);
        weekendRenters.value(DaysOfWeek.WEEKENDS.toString());
        weekendRenters.groupName(KEY_DAYS_REQUIRED_GROUP);
        daysRequiredGroup.add(weekendRenters);
        list.add(new FilterItem<>(weekendRenters));

        GroupedCompoundButtonParams noPreference = new GroupedCompoundButtonParams(NO_PREFERENCE);
        noPreference.size(CompoundButtonParams.Size.MEDIUM);
        noPreference.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        noPreference.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.GRAY);
        noPreference.enableHighlightBar(false);
        noPreference.text(application.getString(R.string.noPreference));
        noPreference.textColorResId(R.color.black);
        noPreference.value(DaysOfWeek.NOT_SET.toString());
        noPreference.defaultItem(true);
        noPreference.groupName(KEY_DAYS_REQUIRED_GROUP);
        daysRequiredGroup.add(noPreference);
        list.add(new FilterItem<>(noPreference));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        groupList.put(KEY_DAYS_REQUIRED_GROUP, daysRequiredGroup);
        setGroupPreviousSearch(previousProperties);

        this.list = list;
    }

    private void setGroupPreviousSearch(SearchAdvertListPropertiesWanted properties) {
        Set<String> groupListKeySet = groupList.keySet();

        for (String key : groupListKeySet) {
            List<GroupedCompoundButtonParams> group = groupList.get(key);

            for (GroupedCompoundButtonParams params : group) {
                switch (key) {
                    case KEY_DAYS_REQUIRED_GROUP:
                        params.selected((properties.getDaysOfWeekAvailable() != DaysOfWeek.NOT_SET)
                                && params.value().equals(properties.getDaysOfWeekAvailable().toString()));
                        break;
                }
            }
            selectedItem(group);
        }

    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return groupList;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesWanted propertiesWanted = (SearchAdvertListPropertiesWanted) propertiesToUpdate;
        for (FilterItem item : list) {
            switch (item.tag()) {
                case MOVE_IN:
                    propertiesWanted.setAvailableFrom(((SummaryViewDateParams) item.params()).date());
                    break;
                case LENGTH_OF_STAY_MIN:
                    propertiesWanted.setMinTerm(((SummaryViewMinTermParams) item.params()).minTerm());
                    break;
                case LENGTH_OF_STAY_MAX:
                    propertiesWanted.setMaxTerm(((SummaryViewMaxTermParams) item.params()).maxTerm());
                    break;
            }
        }
        propertiesWanted.setDaysOfWeekAvailable(DaysOfWeek.valueOf(selectedValue(groupList.get(KEY_DAYS_REQUIRED_GROUP))));

        if (sortProperties)
            sortLengthOfStay(propertiesWanted);

    }

}
