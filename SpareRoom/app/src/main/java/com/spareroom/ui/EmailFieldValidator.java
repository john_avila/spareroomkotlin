package com.spareroom.ui;

import com.spareroom.ui.util.StringUtils;

public class EmailFieldValidator implements FieldValidator {
    public static final int VALID = 0;
    public static final int EMPTY = 1;
    public static final int MALFORMED_ADDRESS = 2;

    @Override
    public int validate(String email) {
        if (StringUtils.isNullOrEmpty(email))
            return EMPTY;
        if (!isEmailFormatValid(email))
            return MALFORMED_ADDRESS;
        return VALID;
    }

    /**
     * This method checks:
     * <ul>
     * <li>there is an <i>@</i></li>
     * <li>there is a String before and after the <i>@</i></li>
     * <li>there is at least a dot after the <i>@</i></li>
     * <li>there is a String after the dot</li>
     * </ul>
     *
     * @param email the email we need to check
     * @return if the email format is valid
     */
    boolean isEmailFormatValid(String email) {
        email = email.trim();

        // Find if there is an "@" in the string
        if (!email.contains("@"))
            return false;
        // Check if there is a dot after the  "@"
        if (!email.substring(email.indexOf("@"), email.length()).contains("."))
            return false;
        // Check if there is a String before "@"
        if (StringUtils.isNullOrEmpty(email.substring(0, email.indexOf("@"))))
            return false;
        // Check if there is a String after the "@" and before the dot
        if (StringUtils.isNullOrEmpty(email.substring(email.indexOf("@") + 1, email.lastIndexOf("."))))
            return false;
        // Check if there is a String after the dot
        return !StringUtils.isNullOrEmpty(email.substring(email.lastIndexOf(".") + 1, email.length()));

    }

}
