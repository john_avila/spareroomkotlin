package com.spareroom.ui.screen.customtabs

import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.widget.ProgressBar
import com.spareroom.ui.util.AnimationUtils

private const val PAGE_LOADED = 100

class ProgressWebChromeClient(private val progressBar: ProgressBar) : WebChromeClient() {

    override fun onProgressChanged(view: WebView?, newProgress: Int) {
        progressBar.progress = newProgress
        if (newProgress == PAGE_LOADED) {
            if (progressBar.visibility != View.INVISIBLE)
                AnimationUtils.hideView(progressBar)
        } else {
            if (progressBar.visibility != View.VISIBLE)
                progressBar.visibility = View.VISIBLE
        }
    }
}