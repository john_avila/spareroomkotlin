package com.spareroom.ui.screen.legacy;

import android.app.Dialog;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.ui.screen.AlertDialogBuilder;
import com.spareroom.ui.widget.PlaceAdButton;

public class PlaceAdOfferedPhoneFragment extends PlaceAdFragment {

    //region FIELDS UI

    private PlaceAdButton _bYes;
    private PlaceAdButton _bNo;

    //endregion FIELDS UI

    //region FIELDS CONTROLLER

    private DraftAdOffered _draftAd;
    private Boolean _previousSelection;

    //endregion FIELDS CONTROLLER

    //region METHODS UI

    private void setButtonStatus(Boolean selection) {
        if (selection == null) {
            _bYes.setPreviouslyNotSelected();
            _bNo.setPreviouslyNotSelected();
        } else if (!selection) {
            _bYes.setPreviouslyNotSelected();
            _bNo.setPreviouslySelected();
        } else { //if (selection)
            _bYes.setPreviouslySelected();
            _bNo.setPreviouslyNotSelected();
        }
    } //end setButtonStatus(Boolean selection)

    //endregion METHODS UI

    //region METHODS LIFECYCLE

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.place_ad_phone, container, false);
        _draftAd = (DraftAdOffered) getDraftAd();

        // In case there is an activity state change and we lost the draft
        if (_draftAd == null) {
            getActivity().finish();
            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();
            return null;
        }

        setTitle(getString(R.string.place_ad_offered_phone_title));

        _bYes = rootView.findViewById(R.id.place_ad_phone_bYes);
        _bNo = rootView.findViewById(R.id.place_ad_phone_bNo);
        _previousSelection = _draftAd.getDisplayPhone(); // null if no previous selection (placing ad)

        setButtonStatus(_previousSelection);

        _bYes.setOnClickListener(v -> {

            View vContent = LayoutInflater.from(v.getContext()).inflate(R.layout.place_ad_phone_edit, null);
            TextView tvTitle = vContent.findViewById(R.id.dialog_title);
            EditText etPhoneNo = vContent.findViewById(R.id.place_ad_phone_edit_etPhone);
            Button bCancel = vContent.findViewById(R.id.place_ad_phone_edit_bCancel);
            Button bConfirm = vContent.findViewById(R.id.place_ad_phone_edit_bConfirm);

            tvTitle.setText(getString(R.string.place_ad_offered_phone_dialog_title));

            String phoneNo = _draftAd.getPhoneNumber();
            if (phoneNo != null) {
                etPhoneNo.setText(phoneNo);
            }

            Dialog dialog = new AlertDialogBuilder(v.getContext(), vContent).create();
            bConfirm.setOnClickListener(new ConfirmOnClickListener(dialog, etPhoneNo));
            bCancel.setOnClickListener(new CancelOnClickListener(dialog));
            dialog.show();
        });

        _bNo.setOnClickListener(v -> {

            // Manages the buttons
            setButtonStatus(false);

            // Adds the data
            _draftAd.setDisplayPhone(false);

            finish();
        });

        return rootView;
    }

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    private class CancelOnClickListener extends DialogUtil.CancelOnClickListener {

        public CancelOnClickListener(Dialog d) {
            super(d);
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            setButtonStatus(_previousSelection);
        }

    } //end class CancelOnClickListener

    private class ConfirmOnClickListener implements OnClickListener {
        final Dialog _dialog;
        final EditText _etPhoneNo;

        ConfirmOnClickListener(Dialog d, EditText etPhoneNo) {
            _dialog = d;
            _etPhoneNo = etPhoneNo;
        }

        @Override
        public void onClick(View v) {
            if (_dialog != null)
                _dialog.dismiss();

            _draftAd.setPhoneNumber(_etPhoneNo.getText().toString());
            _draftAd.setDisplayPhone(true);

            setButtonStatus(true);

            finish();
        }

    } //end class ConfirmOnClickListener

    //endregion INNER CLASSES

}
