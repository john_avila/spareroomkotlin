package com.spareroom.ui.screen.legacy

import android.os.Bundle
import android.view.*
import android.view.inputmethod.EditorInfo
import com.spareroom.R
import com.spareroom.model.business.DraftAdOffered
import com.spareroom.ui.util.KeyboardUtils
import com.spareroom.ui.util.StringUtils
import kotlinx.android.synthetic.main.place_ad_confirm.*
import kotlinx.android.synthetic.main.place_ad_offered_deposit.*

class PlaceAdOfferedDepositFragment : PlaceAdFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.place_ad_offered_deposit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setTitle(getString(R.string.securityDeposit))
        setUpDepositView()
        setUpConfirmButton()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        placeAdOfferedDeposit.setOnEditorActionListener(null)
        place_ad_bConfirm.setOnClickListener(null)
    }

    private fun setUpDepositView() {
        placeAdOfferedDeposit.apply {
            requestFocus()
            setText(deposit())
            setSelection(placeAdOfferedDeposit.text.length)
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_NEXT)
                    confirm()
                return@setOnEditorActionListener false
            }
        }
        KeyboardUtils.showKeyboard(placeAdOfferedDeposit)
    }

    private fun deposit(): String {
        val securityDeposit = StringUtils.toDouble((draftAd as DraftAdOffered).wholePropertyDeposit())
        return (securityDeposit?.toInt() ?: 0).toString()
    }

    private fun setUpConfirmButton() {
        place_ad_bConfirm.setOnClickListener { confirm() }
    }

    private fun confirm() {
        (draftAd as DraftAdOffered).wholePropertyDeposit(placeAdOfferedDeposit.text.toString())
        KeyboardUtils.hideKeyboard(placeAdOfferedDeposit)
        finish()
    }

}
