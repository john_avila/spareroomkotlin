package com.spareroom.ui.filters.adapter.viewparams;

import com.spareroom.model.business.SearchAdvertListPropertiesOffered.MaxFlatmates;

public class SummaryViewMaxFlatmatesParams extends AbstractSummaryViewParams {
    private MaxFlatmates maxFlatmates;

    public SummaryViewMaxFlatmatesParams(String tag) {
        super(tag);
    }

    public MaxFlatmates maxFlatmates() {
        return maxFlatmates;
    }

    public void maxFlatmates(MaxFlatmates maxFlatmates) {
        this.maxFlatmates = maxFlatmates;
    }

    public void reset() {
        super.reset();
        maxFlatmates(MaxFlatmates.NOT_SET);
    }
}
