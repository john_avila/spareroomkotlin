package com.spareroom.ui.filters.adapter.viewparams;

public class SummaryViewDateParams extends AbstractSummaryViewParams {
    private String date;

    public SummaryViewDateParams(String tag) {
        super(tag);
    }

    public String date() {
        return date;
    }

    public void date(String date) {
        this.date = date;
    }

    public void reset() {
        super.reset();
        date(null);
    }

}
