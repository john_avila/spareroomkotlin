package com.spareroom.ui.screen;

import android.os.Bundle;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.lib.core.UiController;
import com.spareroom.model.business.AccountRegisterForm;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.extra.RegisterExtra;
import com.spareroom.ui.util.SnackBarUtils;
import com.spareroom.ui.util.ToastUtils;
import com.spareroom.ui.widget.CompoundButtonView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

/**
 * Fragment for set the type of account for the registration process.
 * Final fragment for the registration process.
 */
public class RegisterTypeFragment extends Fragment {

    //region FIELDS UI.CONTROL

    private UiController _checkboxUiController;
    private UiController _registerUiController;

    //endregion FIELDS UI.CONTROL

    //region FIELDS CONTROLLER

    private AccountRegisterForm _accountRegisterForm;

    private final ControllerActionObserver _registerObserver = new ControllerActionObserver() {

        // Processes the successful response - it can only be null
        @Override
        public void onResult(Object o) {

            FirebaseIdController.register(SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext());
            if (((RegisterActivity) getActivity()).getRegisterMethod() == RegisterActivity.INTENT_VALUE_REGISTER_METHOD_FACEBOOK) {
                AnalyticsTrackerComposite.getInstance().trackEvent_Account_Register_Facebook(_accountRegisterForm.getAdvertiserType().toString());
            } else {
                AnalyticsTrackerComposite.getInstance().trackEvent_Account_Register_Spareroom(_accountRegisterForm.getAdvertiserType().toString());
            }

            _registerUiController.setResult();

            ((RegisterActivity) getActivity()).setActivityResultOK();

            getActivity().finish();

        }

        // Processes any error when updating the advert
        @Override
        public void onSpareroomStatus(SpareroomStatus status) {

            AnalyticsTrackerComposite.getInstance().trackEvent_Account_Register_Spareroom_Fail(status);

            _registerUiController.setSpareroomStatus(status);

        }

        @Override
        public void onException(Exception exception) {
            _registerUiController.setException(exception);
        }

    };

    //endregion FIELDS CONTROLLER

    //region METHODS LIFECYCLE

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        _accountRegisterForm = (AccountRegisterForm) ((RegisterActivity) getActivity()).getSharedObject(RegisterActivity.KEY_REGISTER_FORM);
        View rootView = inflater.inflate(R.layout.material_register_type_fragment, container, false);
        // Title
        TextView tvTitle = rootView.findViewById(R.id.registerTypeFragment_title);
        // Renting box
        CompoundButtonView cbRenting = rootView.findViewById(R.id.registerTypeFragment_rentingCheckbox);
        // Searching box
        CompoundButtonView cbSearching = rootView.findViewById(R.id.registerTypeFragment_searchingCheckbox);
        // Bottom Toolbar
        View bottomToolbar = getActivity().findViewById(R.id.activity_toolbarBottom);
        TextView tvBottomToolbarRight = bottomToolbar.findViewById(R.id.materialToolbarBottom_rightText);
        // Controllers
        _checkboxUiController = new CheckboxUiController(bottomToolbar);
        _registerUiController = new RegisterUiController(rootView, bottomToolbar);

        // Toolbar
        setToolbar(getActivity().findViewById(R.id.appBarLayout), getString(R.string.registerTypeFragment_toolbarTitle), true);

        // Title
        tvTitle.setText(String.format("%s %s.", tvTitle.getText(), _accountRegisterForm.getFirstName()));

        // Bottom Toolbar
        bottomToolbar.setVisibility(View.VISIBLE);

        tvBottomToolbarRight.setOnClickListener(new RegisterTypeButtonOnClickListener(_accountRegisterForm));
        tvBottomToolbarRight.setText(getString(R.string.registerTypeFragment_bottomToolbar));

        // Starts the views
        _checkboxUiController.reset();

        // Renting and Searching boxes
        cbRenting.setOnCheckedChangeListener(new RentingChangeListener(cbSearching));

        cbSearching.setOnCheckedChangeListener(new SearchingChangeListener(cbRenting));

        if (_accountRegisterForm.getAdvertiserType() != null) {
            switch (_accountRegisterForm.getAdvertiserType()) {
                case OFFERER:
                    cbRenting.setChecked(true);
                    cbSearching.setChecked(false);
                    break;
                case SEEKER:
                    cbRenting.setChecked(false);
                    cbSearching.setChecked(true);
                    break;
                case OFFERER_AND_SEEKER:
                    cbRenting.setChecked(true);
                    cbSearching.setChecked(true);
            }
            _checkboxUiController.setLoading();
        } else {
            cbRenting.setChecked(false);
            cbSearching.setChecked(false);
        }

        return rootView;
    } // end onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ((LinearMultiStepActivity) getActivity()).onNavigateBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    /**
     * It is used for set the different views status when the register is in process or we already have the response.
     */
    private class CheckboxUiController extends UiController {
        private final TextView _tvBottomToolbarRight;

        CheckboxUiController(View bottomToolbar) {
            _tvBottomToolbarRight = bottomToolbar.findViewById(R.id.materialToolbarBottom_rightText);
        }

        @Override
        public void setLoading() {
            super.setLoading();

            _tvBottomToolbarRight.setTextColor(ContextCompat.getColor(_tvBottomToolbarRight.getContext(), R.color.white));
            _tvBottomToolbarRight.setEnabled(true);

        }

        @Override
        public void reset() {
            super.reset();

            _tvBottomToolbarRight.setTextColor(ContextCompat.getColor(_tvBottomToolbarRight.getContext(), R.color.light_blue));
            _tvBottomToolbarRight.setEnabled(false);

        }

    } //end class CheckboxUiController

    /**
     * It is used when the inputs are checked for controlling the the "REGISTER" button status.
     */
    private class RegisterUiController extends UiController {
        private final TextView _tvBottomToolbarRight;
        private final ProgressBar _pbLoading;
        private final TextView _tvErrorMessage;
        private final ImageView _ivErrorIcon;

        RegisterUiController(View rootView, View bottomToolbar) {
            _pbLoading = bottomToolbar.findViewById(R.id.materialToolbarBottom_loading);
            _tvBottomToolbarRight = bottomToolbar.findViewById(R.id.materialToolbarBottom_rightText);
            // Error message and icon
            _tvErrorMessage = rootView.findViewById(R.id.registerTypeFragment_errorMessage);
            _ivErrorIcon = rootView.findViewById(R.id.registerTypeFragment_errorIcon);
        }

        @Override
        public void setLoading() {
            super.setLoading();

            _tvBottomToolbarRight.setEnabled(false);
            _tvBottomToolbarRight.setVisibility(View.GONE);
            _pbLoading.setVisibility(View.VISIBLE);

        }

        @Override
        public void reset() {
            super.reset();

            _tvBottomToolbarRight.setEnabled(true);
            _tvBottomToolbarRight.setVisibility(View.VISIBLE);
            _pbLoading.setVisibility(View.GONE);

        }

        @Override
        public void setResult(Object... o) {
            super.setResult(o);

            reset();
            ToastUtils.showToast(R.string.signed_in);

        }

        @Override
        public void setSpareroomStatus(SpareroomStatus spareroomStatus) {
            super.setSpareroomStatus(spareroomStatus);

            reset();

            _tvErrorMessage.setText(getString(R.string.registerTypeFragment_registerKo));
            _tvErrorMessage.setVisibility(View.VISIBLE);
            _ivErrorIcon.setVisibility(View.VISIBLE);
        }

        @Override
        public void setException(Exception exception) {
            super.setException(exception);

            reset();

            SnackBarUtils.showException(RegisterTypeFragment.this.getView(), exception);

        }

    } //end class RegisterTypeUiController

    private class RegisterTypeButtonOnClickListener implements View.OnClickListener {
        private final AccountRegisterForm _accountRegisterForm;

        RegisterTypeButtonOnClickListener(AccountRegisterForm accRegForm) {
            _accountRegisterForm = accRegForm;
        }

        @Override
        public void onClick(View v) {

            if (_accountRegisterForm.getAdvertiserType() == null) {
                Animation shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake_to_left);
                v.startAnimation(shake);

            } else {
                RegisterExtra registerExtra = new RegisterExtra();
                registerExtra.setPageJoinedFrom(((RegisterActivity) getActivity()).getPageJoinedFrom());
                registerExtra.setVersion(AppVersion.appId());

                if (((RegisterActivity) getActivity()).getRegisterMethod() == RegisterActivity.INTENT_VALUE_REGISTER_METHOD_FACEBOOK) {
                    RegisterFacebookStep2ControllerAction registerControllerAction =
                            new RegisterFacebookStep2ControllerAction(
                                    SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext());
                    registerControllerAction.addControllerActionObserver(_registerObserver);
                    addObserver(registerControllerAction);
                    registerControllerAction.execute(_accountRegisterForm, registerExtra);

                    _registerUiController.setLoading();
                } else {
                    // User registered with email
                    RegisterControllerAction registerControllerAction =
                            new RegisterControllerAction(
                                    SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext());
                    registerControllerAction.addControllerActionObserver(_registerObserver);
                    addObserver(registerControllerAction);
                    registerControllerAction.execute(_accountRegisterForm, registerExtra);

                    _registerUiController.setLoading();
                }

            }

        }

    } //end class RegisterNameButtonOnClickListener

    private class RentingChangeListener implements CompoundButtonView.OnCheckedChangeListener {
        private final CompoundButtonView _searchingCheckBox;

        private RentingChangeListener(CompoundButtonView searchingCheckBox) {
            _searchingCheckBox = searchingCheckBox;
        }

        @Override
        public void onCheckedChangeEvent(boolean isChecked) {
            if (isChecked && _searchingCheckBox.isChecked()) {
                _accountRegisterForm.setAdvertiserType(AccountRegisterForm.AdvertiserType.OFFERER_AND_SEEKER);
                _checkboxUiController.setLoading();

            } else if (isChecked && !_searchingCheckBox.isChecked()) {
                _accountRegisterForm.setAdvertiserType(AccountRegisterForm.AdvertiserType.OFFERER);
                _checkboxUiController.setLoading();

            } else if (!isChecked && _searchingCheckBox.isChecked()) {
                _accountRegisterForm.setAdvertiserType(AccountRegisterForm.AdvertiserType.SEEKER);
                _checkboxUiController.setLoading();

            } else {
                _accountRegisterForm.setAdvertiserType(null);
                _checkboxUiController.reset();

            }
            ((LinearMultiStepActivity) getActivity()).setSharedObject(RegisterActivity.KEY_REGISTER_FORM, _accountRegisterForm);
        }

    } //end class RentingChangeListener

    private class SearchingChangeListener implements CompoundButtonView.OnCheckedChangeListener {
        private final CompoundButtonView _rentingCheckBox;

        private SearchingChangeListener(CompoundButtonView rentingCheckBox) {
            _rentingCheckBox = rentingCheckBox;

        }

        @Override
        public void onCheckedChangeEvent(boolean isChecked) {
            if (isChecked && _rentingCheckBox.isChecked()) {
                _accountRegisterForm.setAdvertiserType(AccountRegisterForm.AdvertiserType.OFFERER_AND_SEEKER);
                _checkboxUiController.setLoading();

            } else if (!isChecked && _rentingCheckBox.isChecked()) {
                _accountRegisterForm.setAdvertiserType(AccountRegisterForm.AdvertiserType.OFFERER);
                _checkboxUiController.setLoading();

            } else if (isChecked && !_rentingCheckBox.isChecked()) {
                _accountRegisterForm.setAdvertiserType(AccountRegisterForm.AdvertiserType.SEEKER);
                _checkboxUiController.setLoading();

            } else {
                _accountRegisterForm.setAdvertiserType(null);
                _checkboxUiController.reset();

            }
            ((LinearMultiStepActivity) getActivity()).setSharedObject(RegisterActivity.KEY_REGISTER_FORM, _accountRegisterForm);
        }

    } //end class SearchingChangeListener

    //endregion INNER CLASSES

}
