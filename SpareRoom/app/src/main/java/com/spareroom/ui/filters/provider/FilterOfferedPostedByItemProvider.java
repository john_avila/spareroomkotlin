package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.PostedBy;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import java.util.*;

import androidx.annotation.NonNull;

class FilterOfferedPostedByItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.postedBy;
    private final static String KEY_ADVERTISER_GROUP = "key_advertiser_group";

    final static String AGENTS = "agents";
    final static String INDIVIDUAL_USER = "individual_user";
    final static String NO_PREFERENCE = "no_preference";

    private final List<GroupedCompoundButtonParams> advertisersGroup = new ArrayList<>();
    private final Map<String, List<GroupedCompoundButtonParams>> groupList = new HashMap<>();

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesOffered previousProperties = (SearchAdvertListPropertiesOffered) properties;
        List<FilterItem> list = new ArrayList<>();

        advertisersGroup.clear();
        groupList.clear();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        GroupedCompoundButtonParams individualUser = new GroupedCompoundButtonParams(INDIVIDUAL_USER);
        individualUser.size(CompoundButtonParams.Size.MEDIUM);
        individualUser.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        individualUser.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        individualUser.enableHighlightBar(true);
        individualUser.text(application.getString(R.string.individualUsers));
        individualUser.textColorResId(R.color.black);
        individualUser.iconResId(R.drawable.ic_generic_user);
        individualUser.value(PostedBy.PRIVATE_LANDLORDS.toString());
        individualUser.groupName(KEY_ADVERTISER_GROUP);
        advertisersGroup.add(individualUser);
        list.add(new FilterItem<>(individualUser));

        GroupedCompoundButtonParams agents = new GroupedCompoundButtonParams(AGENTS);
        agents.size(CompoundButtonParams.Size.MEDIUM);
        agents.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        agents.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        agents.enableHighlightBar(true);
        agents.text(application.getString(R.string.agents));
        agents.textColorResId(R.color.black);
        agents.iconResId(R.drawable.ic_agent);
        agents.value(PostedBy.AGENTS.toString());
        agents.groupName(KEY_ADVERTISER_GROUP);
        advertisersGroup.add(agents);
        list.add(new FilterItem<>(agents));

        GroupedCompoundButtonParams noPreference = new GroupedCompoundButtonParams(NO_PREFERENCE);
        noPreference.size(CompoundButtonParams.Size.MEDIUM);
        noPreference.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        noPreference.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.GRAY);
        noPreference.enableHighlightBar(false);
        noPreference.text(application.getString(R.string.noPreference));
        noPreference.textColorResId(R.color.black);
        noPreference.value(PostedBy.NOT_SET.toString());
        noPreference.defaultItem(true);
        noPreference.groupName(KEY_ADVERTISER_GROUP);
        advertisersGroup.add(noPreference);
        list.add(new FilterItem<>(noPreference));

        groupList.put(KEY_ADVERTISER_GROUP, advertisersGroup);
        setGroupPreviousSearch(previousProperties);

        this.list = list;
    }

    private void setGroupPreviousSearch(SearchAdvertListPropertiesOffered properties) {
        Set<String> groupListKeySet = groupList.keySet();

        for (String key : groupListKeySet) {
            List<GroupedCompoundButtonParams> group = groupList.get(key);

            for (GroupedCompoundButtonParams params : group) {
                switch (key) {
                    case KEY_ADVERTISER_GROUP:
                        params.selected((properties.getPostedBy() != PostedBy.NOT_SET) && params.value().equals(properties.getPostedBy().toString()));
                        break;
                }
            }
            selectedItem(group);
        }

    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return groupList;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesOffered propertiesOffered = (SearchAdvertListPropertiesOffered) propertiesToUpdate;
        propertiesOffered.setPostedBy(PostedBy.valueOf(selectedValue(groupList.get(KEY_ADVERTISER_GROUP))));

    }

}
