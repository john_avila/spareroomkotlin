package com.spareroom.ui.adapter

import android.content.Context
import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.spareroom.R
import com.spareroom.integration.imageloader.ImageLoader
import com.spareroom.model.business.ConversationPreview
import com.spareroom.ui.screen.ConversationListFragment
import com.spareroom.ui.util.DateUtils
import com.spareroom.ui.util.StringUtils.*
import com.spareroom.ui.util.removeExtraWhiteSpaces
import javax.inject.Inject

class ConversationListAdapter @Inject constructor(
    private val imageLoader: ImageLoader,
    private val dateUtils: DateUtils
) : RecyclerViewAdapter<ConversationPreview, ConversationListAdapter.ViewHolder>() {

    private lateinit var fragment: ConversationListFragment
    private lateinit var onClickListener: View.OnClickListener

    fun init(context: Context, fragment: ConversationListFragment, onClickListener: View.OnClickListener) {
        super.context = context
        this.fragment = fragment
        this.onClickListener = onClickListener
    }

    private val you by lazy { context.getString(R.string.you) }
    private val typefaceRead by lazy { Typeface.create(context.getString(R.string.font_family_regular), Typeface.NORMAL) }
    private val typefaceUnread by lazy { Typeface.create(context.getString(R.string.font_family_regular), Typeface.BOLD) }
    private val black by lazy { ContextCompat.getColor(context, R.color.black) }
    private val grey by lazy { ContextCompat.getColor(context, R.color.grey_2) }
    private val blackForegroundColorSpan by lazy { ForegroundColorSpan(black) }
    private val greyForegroundColorSpan by lazy { ForegroundColorSpan(grey) }
    private val normalStyleSpan = StyleSpan(Typeface.NORMAL)
    private val boldStyleSpan = StyleSpan(Typeface.BOLD)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.conversation_list_item, parent, false)
        view.setOnClickListener(onClickListener)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val conversationPreview = getItem(position)

        holder.itemView.setTag(R.id.item_tag, conversationPreview)
        setUpAvatar(holder, conversationPreview)
        setUpName(holder, conversationPreview)
        setUpBody(holder, conversationPreview)
        setUpTime(holder, conversationPreview)
    }

    private fun setUpAvatar(holder: ViewHolder, preview: ConversationPreview) {
        holder.initials.text = getInitials(preview.otherUserName, preview.otherUserLastName)
        imageLoader.loadCircularImage(preview.otherUserAvatarUrl, holder.avatar, fragment)
    }

    private fun setUpName(holder: ViewHolder, preview: ConversationPreview) {
        holder.name.typeface = if (preview.isUnread) typefaceUnread else typefaceRead
        holder.name.text = getInitialsWithFullFirstName(getFirstString(preview.otherUserName), preview.otherUserLastName)
    }

    private fun setUpBody(holder: ViewHolder, preview: ConversationPreview) {
        val stringBuilder = StringBuilder()
        stringBuilder.append(preview.advertTitle)
        stringBuilder.append(" — ")
        if (preview.isOut)
            stringBuilder.append("$you: ")
        stringBuilder.append(preview.snippet.removeExtraWhiteSpaces())

        val spannableStringBuilder = SpannableStringBuilder(stringBuilder)
        // Set colours
        spannableStringBuilder.setSpan(blackForegroundColorSpan, 0, preview.advertTitle.length, 0)
        spannableStringBuilder.setSpan(greyForegroundColorSpan, preview.advertTitle.length, spannableStringBuilder.length, 0)
        // Set typeface
        spannableStringBuilder.setSpan(if (preview.isUnread) boldStyleSpan else normalStyleSpan, 0, preview.advertTitle.length, 0)
        spannableStringBuilder.setSpan(normalStyleSpan, preview.advertTitle.length, spannableStringBuilder.length, 0)

        holder.body.text = spannableStringBuilder
    }

    private fun setUpTime(holder: ViewHolder, preview: ConversationPreview) {
        holder.time.setTextColor(if (preview.isUnread) black else grey)
        holder.time.typeface = if (preview.isUnread) typefaceUnread else typefaceRead
        holder.time.text = dateUtils.getFormattedDateOfContact(preview.dateLastActivity, true)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val avatar: ImageView = itemView.findViewById(R.id.avatarImageView)
        val initials: TextView = itemView.findViewById(R.id.avatarInitialsTextView)
        val name: TextView = itemView.findViewById(R.id.nameTextView)
        val body: TextView = itemView.findViewById(R.id.bodyTextView)
        val time: TextView = itemView.findViewById(R.id.timeTextView)
    }

}
