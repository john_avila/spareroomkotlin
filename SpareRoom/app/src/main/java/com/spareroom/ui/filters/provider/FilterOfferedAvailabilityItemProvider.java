package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListProperties.DaysOfWeek;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.ui.filters.adapter.viewparams.AbstractSummaryViewParams.SummaryAction;
import com.spareroom.ui.filters.adapter.viewparams.*;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import java.util.*;

import androidx.annotation.NonNull;

public class FilterOfferedAvailabilityItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.availability;
    private final static String KEY_DAYS_GROUP = "key_days_group";

    private final static String LENGTH_OF_STAY_TITLE = "length_of_stay_title";
    private final static String DAYS_AVAILABLE_TITLE = "days_available_title";
    final static String ALL_WEEK = "all_week";
    final static String WEEK_ENDS = "week_ends";
    private final static String SHORT_LETS = "short_lets";
    final static String WEEK_DAYS = "week_days";
    final static String NO_PREFERENCE = "no_preference";

    private final List<GroupedCompoundButtonParams> daysGroup = new ArrayList<>();
    private final Map<String, List<GroupedCompoundButtonParams>> groupList = new HashMap<>();

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesOffered previousProperties = (SearchAdvertListPropertiesOffered) properties;
        List<FilterItem> list = new ArrayList<>();

        daysGroup.clear();
        groupList.clear();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        SummaryViewDateParams moveIn = new SummaryViewDateParams(MOVE_IN);
        moveIn.iconDrawableId(R.drawable.ic_calendar);
        moveIn.titleStringId(R.string.moveIn);
        moveIn.defaultSubtitleString(application.getString(R.string.noPreference));
        moveIn.summaryAction(SummaryAction.SHOW_DATE_DIALOG);
        if (previousProperties.getAvailableFrom() != null) {
            moveIn.subtitleString(subtitleUtil.getMoveInText(previousProperties.getAvailableFrom()));
            moveIn.date(previousProperties.getAvailableFrom());
        }
        list.add(new FilterItem<>(moveIn));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        TitleViewParams titleViewParams = new TitleViewParams(LENGTH_OF_STAY_TITLE);
        titleViewParams.title(application.getString(R.string.lengthOfStay));
        list.add(new FilterItem<>(titleViewParams));

        SummaryViewMinTermParams lengthOfStayMin = new SummaryViewMinTermParams(LENGTH_OF_STAY_MIN);
        lengthOfStayMin.iconDrawableId(R.drawable.ic_length_of_stay);
        lengthOfStayMin.titleStringId(R.string.minimumTerm);
        lengthOfStayMin.defaultSubtitleString(application.getString(R.string.noPreference));
        lengthOfStayMin.summaryAction(SummaryAction.SHOW_SCROLL_DIALOG);
        lengthOfStayMin.subtitleString(subtitleUtil.getTermText(application, previousProperties.getMinTerm().getIntValue()));
        lengthOfStayMin.minTerm(previousProperties.getMinTerm());
        list.add(new FilterItem<>(lengthOfStayMin));

        SummaryViewMaxTermParams lengthOfStayMax = new SummaryViewMaxTermParams(LENGTH_OF_STAY_MAX);
        lengthOfStayMax.iconDrawableId(R.drawable.ic_length_of_stay);
        lengthOfStayMax.titleStringId(R.string.maximumTerm);
        lengthOfStayMax.defaultSubtitleString(application.getString(R.string.noPreference));
        lengthOfStayMax.summaryAction(SummaryAction.SHOW_SCROLL_DIALOG);
        lengthOfStayMax.subtitleString(subtitleUtil.getTermText(application, previousProperties.getMaxTerm().getIntValue()));
        lengthOfStayMax.maxTerm(previousProperties.getMaxTerm());
        list.add(new FilterItem<>(lengthOfStayMax));

        CompoundButtonParams shortLets = new CompoundButtonParams(SHORT_LETS);
        shortLets.size(CompoundButtonParams.Size.MEDIUM);
        shortLets.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        shortLets.enableHighlightBar(true);
        shortLets.text(application.getString(R.string.shortTermLets));
        shortLets.textColorResId(R.color.black);
        shortLets.iconResId(R.drawable.ic_length_of_stay);
        shortLets.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        shortLets.selected(previousProperties.getShortLets());
        list.add(new FilterItem<>(shortLets));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        titleViewParams = new TitleViewParams(DAYS_AVAILABLE_TITLE);
        titleViewParams.title(application.getString(R.string.daysAvailable));
        list.add(new FilterItem<>(titleViewParams));

        GroupedCompoundButtonParams allWeek = new GroupedCompoundButtonParams(ALL_WEEK);
        allWeek.size(CompoundButtonParams.Size.MEDIUM);
        allWeek.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        allWeek.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        allWeek.enableHighlightBar(true);
        allWeek.text(application.getString(R.string.allWeekAdverts));
        allWeek.textColorResId(R.color.black);
        allWeek.subtitle(application.getString(R.string.suitableForMostPeople));
        allWeek.iconResId(R.drawable.ic_all_week);
        allWeek.value(DaysOfWeek.SEVEN_DAYS.toString());
        allWeek.groupName(KEY_DAYS_GROUP);
        daysGroup.add(allWeek);
        list.add(new FilterItem<>(allWeek));

        GroupedCompoundButtonParams weekdays = new GroupedCompoundButtonParams(WEEK_DAYS);
        weekdays.size(CompoundButtonParams.Size.MEDIUM);
        weekdays.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        weekdays.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        weekdays.enableHighlightBar(true);
        weekdays.text(application.getString(R.string.weekdayOnlyAdverts));
        weekdays.textColorResId(R.color.black);
        weekdays.subtitle(application.getString(R.string.suitableForBusinessPeople));
        weekdays.iconResId(R.drawable.ic_weekdays);
        weekdays.value(DaysOfWeek.WEEKDAYS.toString());
        weekdays.groupName(KEY_DAYS_GROUP);
        daysGroup.add(weekdays);
        list.add(new FilterItem<>(weekdays));

        GroupedCompoundButtonParams weekends = new GroupedCompoundButtonParams(WEEK_ENDS);
        weekends.size(CompoundButtonParams.Size.MEDIUM);
        weekends.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        weekends.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        weekends.enableHighlightBar(true);
        weekends.text(application.getString(R.string.weekendOnlyAdverts));
        weekends.textColorResId(R.color.black);
        weekends.subtitle(application.getString(R.string.roomsOnlyAvailableOnSaturdayAndSunday));
        weekends.iconResId(R.drawable.ic_weekends);
        weekends.value(DaysOfWeek.WEEKENDS.toString());
        weekends.groupName(KEY_DAYS_GROUP);
        daysGroup.add(weekends);
        list.add(new FilterItem<>(weekends));

        GroupedCompoundButtonParams noPreference = new GroupedCompoundButtonParams(NO_PREFERENCE);
        noPreference.size(CompoundButtonParams.Size.MEDIUM);
        noPreference.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        noPreference.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.GRAY);
        noPreference.enableHighlightBar(false);
        noPreference.text(application.getString(R.string.noPreference));
        noPreference.textColorResId(R.color.black);
        noPreference.value(DaysOfWeek.NOT_SET.toString());
        noPreference.defaultItem(true);
        noPreference.groupName(KEY_DAYS_GROUP);
        daysGroup.add(noPreference);
        list.add(new FilterItem<>(noPreference));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        groupList.put(KEY_DAYS_GROUP, daysGroup);
        setGroupPreviousSearch(previousProperties);

        this.list = list;
    }

    private void setGroupPreviousSearch(SearchAdvertListPropertiesOffered properties) {
        Set<String> groupListKeySet = groupList.keySet();

        for (String key : groupListKeySet) {
            List<GroupedCompoundButtonParams> group = groupList.get(key);

            for (GroupedCompoundButtonParams params : group) {
                switch (key) {
                    case KEY_DAYS_GROUP:
                        params.selected((properties.getDaysOfWeekAvailable() != DaysOfWeek.NOT_SET)
                                && params.value().equals(properties.getDaysOfWeekAvailable().toString()));
                        break;
                }
            }
            selectedItem(group);
        }

    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return groupList;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesOffered propertiesOffered = (SearchAdvertListPropertiesOffered) propertiesToUpdate;
        for (FilterItem item : list) {
            switch (item.tag()) {
                case MOVE_IN:
                    propertiesOffered.setAvailableFrom(((SummaryViewDateParams) item.params()).date());
                    break;
                case SHORT_LETS:
                    propertiesOffered.setShortLets(((CompoundButtonParams) item.params()).selected());
                    break;
                case LENGTH_OF_STAY_MIN:
                    propertiesOffered.setMinTerm(((SummaryViewMinTermParams) item.params()).minTerm());
                    break;
                case LENGTH_OF_STAY_MAX:
                    propertiesOffered.setMaxTerm(((SummaryViewMaxTermParams) item.params()).maxTerm());
                    break;
            }
        }
        propertiesOffered.setDaysOfWeekAvailable(DaysOfWeek.valueOf(selectedValue(groupList.get(KEY_DAYS_GROUP))));

        if (sortProperties)
            sortLengthOfStay(propertiesOffered);

    }

}
