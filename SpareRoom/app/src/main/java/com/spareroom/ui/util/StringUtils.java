package com.spareroom.ui.util;

import com.spareroom.controller.AppVersion;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;

import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class StringUtils {

    public static final String EMPTY_STRING = "";
    public static final String EN_DASH = "\u2013";
    public static final String PLUS_CHARACTER = "+";

    /**
     * Returns true if a and b are equal, including if they are both null.
     */
    public static boolean equals(CharSequence a, CharSequence b) {
        if (a == b) return true;
        int length;
        if (a != null && b != null && (length = a.length()) == b.length()) {
            if (a instanceof String && b instanceof String) {
                return a.equals(b);
            } else {
                for (int i = 0; i < length; i++) {
                    if (a.charAt(i) != b.charAt(i)) return false;
                }
                return true;
            }
        }
        return false;
    }

    public static boolean isNullOrEmpty(CharSequence string) {
        return isNullOrEmpty(string, false);
    }

    public static boolean isNullOrEmpty(CharSequence string, boolean trim) {
        return string == null || string.length() == 0 || (trim && string.toString().trim().length() == 0);
    }

    public static String trim(String text) {
        if (isNullOrEmpty(text, true))
            return EMPTY_STRING;

        return text.trim();
    }

    @NonNull
    public static String getFirstString(String strings) {
        if (isNullOrEmpty(strings))
            return EMPTY_STRING;

        strings = strings.trim();
        String[] wordsArray = strings.split(" ");
        return wordsArray.length > 1 ? wordsArray[0].trim() : strings;
    }

    @NonNull
    public static String getLastName(String strings) {
        if (isNullOrEmpty(strings))
            return EMPTY_STRING;

        strings = strings.trim();
        String[] wordsArray = strings.split(" ");
        return wordsArray.length > 1 ? wordsArray[wordsArray.length - 1].trim() : EMPTY_STRING;
    }

    public static String getInitialsWithFullFirstName(String firstName, String lastName) {
        String firstLetterOfLastName = !isNullOrEmpty(lastName, true) ? String.valueOf(lastName.trim().charAt(0)) : EMPTY_STRING;
        String names = capitalizeEachFirstLetter(firstName);
        StringBuilder title = new StringBuilder();
        if (!isNullOrEmpty(names, true))
            title.append(names);

        if (!isNullOrEmpty(firstLetterOfLastName)) {
            if (!isNullOrEmpty(title, true))
                title.append(" ");

            title.append(firstLetterOfLastName.toUpperCase());
            title.append(".");
        }

        return title.toString();
    }

    public static String getInitials(String firstName, String lastName) {
        String firstLetterOfFirstName = !isNullOrEmpty(firstName, true) ? String.valueOf(firstName.trim().charAt(0)) : EMPTY_STRING;
        String firstLetterOfLastName = !isNullOrEmpty(lastName, true) ? String.valueOf(lastName.trim().charAt(0)) : EMPTY_STRING;
        return String.format("%s%s", firstLetterOfFirstName.toUpperCase(), firstLetterOfLastName.toUpperCase());
    }

    // TODO write tests for this method (it got moved from other class)
    public static String capitalizeEachFirstLetter(String string) {
        if (isNullOrEmpty(string, true))
            return EMPTY_STRING;

        Locale locale = AppVersion.flavor().getLocale();

        String capitalizedString = string.trim();
        capitalizedString = capitalizedString.substring(0, 1).toUpperCase(locale).concat(capitalizedString.substring(1, capitalizedString.length()).toLowerCase(locale));

        int index = capitalizedString.indexOf(" ");

        while (index != -1) {
            capitalizedString = capitalizedString.substring(0, index + 1).concat(capitalizedString.substring(index + 1, index + 2).toUpperCase(locale)).concat(capitalizedString.substring(index + 2, capitalizedString.length()));
            index = capitalizedString.indexOf(" ", index + 1);
        }

        return capitalizedString;
    }

    @Nullable
    public static Double toDouble(@Nullable String number) {
        if (isNullOrEmpty(number))
            return null;

        try {
            return Double.valueOf(number);
        } catch (Exception e) {
            AnalyticsTrackerComposite.getInstance().logHandledException("Failed to convert string to double", e);
            return null;
        }

    }

}
