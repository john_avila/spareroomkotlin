package com.spareroom.ui.screen;

import android.os.Bundle;

import com.spareroom.integration.analytics.AnalyticsTrackerComposite;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

public abstract class DialogFragment extends androidx.fragment.app.DialogFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AnalyticsTrackerComposite.getInstance().trackScreen_Fragment(this);
    }

    public void show(Fragment parentFragment, String tag) {
        if (parentFragment.isResumed())
            show(parentFragment.getFragmentManager(), tag);
    }

    public void show(Activity activity, String tag) {
        if (activity.isActivityResumed())
            show(activity.getSupportFragmentManager(), tag);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        if (manager.findFragmentByTag(tag) == null)
            super.show(manager, tag);
    }

    protected Activity getBaseActivity() {
        return (Activity) getActivity();
    }
}
