package com.spareroom.ui.screen.legacy;

import android.app.Dialog;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.model.business.DraftAd;
import com.spareroom.model.business.Session;
import com.spareroom.ui.screen.AlertDialogBuilder;

public class PlaceAdWelcomeFragment extends PlaceAdFragment {
    private Button _bStart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_welcome, container, false);

        setTitle(getString(R.string.post_ad));

        _bStart = rootView.findViewById(R.id.place_ad_welcome_bStart);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        _bStart.setOnClickListener(v -> {
            Session session = SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext().getSession();
            DraftAd draft = getDraftAd();

            if (session == null) {
                View vContent = LayoutInflater.from(v.getContext()).inflate(R.layout.place_ad_name_email, null);
                TextView tvTitle = vContent.findViewById(R.id.dialog_title);
                EditText etFirstName = vContent.findViewById(R.id.place_ad_tvFirstName);
                EditText etLastName = vContent.findViewById(R.id.place_ad_tvLastName);
                EditText etEmail = vContent.findViewById(R.id.place_ad_tvEmail);
                Button bCancel = vContent.findViewById(R.id.dialog_bNegative);
                Button bConfirm = vContent.findViewById(R.id.dialog_bPositive);

                tvTitle.setText(R.string.placeAdWelcomeFragment_email);

                Dialog dialog = new AlertDialogBuilder(v.getContext(), vContent).create();
                bConfirm.setOnClickListener(new ConfirmOnClickListener(dialog, etFirstName, etLastName, etEmail));
                bCancel.setOnClickListener(new DialogUtil.CancelOnClickListener(dialog));
                dialog.show();

                if (draft != null) {
                    if (draft.get_firstName() != null) {
                        etFirstName.setText(draft.get_firstName());
                    }
                    if (draft.get_lastName() != null) {
                        etLastName.setText(draft.get_lastName());
                    }
                    if (draft.get_email() != null) {
                        etEmail.setText(draft.get_email());
                    }
                }
            } else {
                draft = new DraftAd();
                draft.set_firstName(session.get_user().get_firstName());
                draft.set_lastName(session.get_user().get_lastName());
                draft.set_email(session.get_email());
                ((PlaceAdAbstractActivity) getActivity()).setDraftAd(draft);
                PlaceAdWelcomeFragment.this.finish();
            }
        });
    }

    private class ConfirmOnClickListener implements OnClickListener {
        final Dialog _dialog;
        final EditText _etFirstName;
        final EditText _etLastName;
        final EditText _etEmail;

        ConfirmOnClickListener(Dialog d, EditText etFirstName, EditText etLastName, EditText etEmail) {
            _dialog = d;
            _etFirstName = etFirstName;
            _etLastName = etLastName;
            _etEmail = etEmail;
        }

        @Override
        public void onClick(View v) {
            if (_etFirstName.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), getString(R.string.placeAdWelcomeFragment_missingFirstName), Toast.LENGTH_LONG).show();
                return;
            }
            if (_etLastName.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), getString(R.string.placeAdWelcomeFragment_missingLastName), Toast.LENGTH_LONG).show();
                return;
            }
            if (_etEmail.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), getString(R.string.placeAdWelcomeFragment_missingEmail), Toast.LENGTH_LONG).show();
                return;
            }
            if (_dialog != null)
                _dialog.dismiss();

            DraftAd draftAd = (getDraftAd() != null) ? getDraftAd() : new DraftAd();
            draftAd.set_firstName(_etFirstName.getText().toString());
            draftAd.set_lastName(_etLastName.getText().toString());
            draftAd.set_email(_etEmail.getText().toString());
            ((PlaceAdAbstractActivity) getActivity()).setDraftAd(draftAd);

            PlaceAdWelcomeFragment.this.finish();
        }
    }
}
