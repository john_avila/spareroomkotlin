package com.spareroom.ui.screen;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.*;
import android.view.animation.CycleInterpolator;
import android.widget.EditText;
import android.widget.ImageView;

import com.spareroom.R;
import com.spareroom.controller.SpareroomContext;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.model.business.*;
import com.spareroom.ui.adapter.conversation.ConversationAdapter;
import com.spareroom.ui.util.*;
import com.spareroom.ui.widget.ErrorView;
import com.spareroom.ui.widget.ProgressView;
import com.spareroom.viewmodel.ConversationViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class ConversationFragment extends Fragment implements Injectable {

    public static final String TAG = "ConversationFragmentTag";
    public static final int LOG_IN_REQUEST_CODE = 0;

    private static final String SAVED_STATE = "SAVED_STATE";

    private final Handler handler = new Handler();

    private final ConstraintSet bottomDividerConstraintSet = new ConstraintSet();
    private ConversationViewModel viewModel;
    private ConstraintLayout rootView;
    private EditText txtNewMessage;
    private ImageView btnSendNewMessage;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerViewConversation;
    private LinearLayoutManager linearLayoutManager;
    private View bottomDivider;
    private View btnReply;
    private View btnDecline;
    private ViewGroup loadingView;
    private View progressBarSendingMessage;
    private ErrorView errorView;
    private ProgressView progressView;

    private String otherUserId;
    private String otherUserFirstName;
    private String otherUserLastName;
    private String advertId;
    private String message;
    private boolean hasUserScrolled;
    private boolean hasUserOpenedMessageBar;
    private boolean clearMessage;
    private boolean showProgressBar;
    private boolean canShowErrorView;

    private final Runnable scrollToBottomOnFocusChange = new Runnable() {
        @Override
        public void run() {
            recyclerViewConversation.scrollToPosition(conversationAdapter.getItemCount() - 1);
        }
    };

    @Inject
    SpareroomContext spareroomContext;

    @Inject
    ConversationAdapter conversationAdapter;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    ConnectivityChecker connectivityChecker;

    @NonNull
    public static ConversationFragment getInstance() {
        return new ConversationFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        viewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(ConversationViewModel.class);

        ConversationInfo args = viewModel.getConversationInfo();
        otherUserId = args.getOtherUserId();
        advertId = args.getAdvertId();
        otherUserFirstName = StringUtils.getFirstString(args.getOtherUserFirstNames());
        otherUserLastName = args.getOtherUserLastName();

        String userAvatarUrl = "";
        String loggedInUserId = "";
        String loggedInUserInitials = "";

        if (isUserLoggedIn()) {
            Session session = spareroomContext.getSession();
            userAvatarUrl = session.get_profilePictureUrl();
            User user = session.get_user();
            if (user != null) {
                loggedInUserId = user.get_userId();
                loggedInUserInitials = StringUtils.getInitials(user.get_firstName(), user.get_lastName());
            }
        }

        conversationAdapter.init(getActivity(), this, loggedInUserId, loggedInUserInitials, userAvatarUrl,
                args.getOtherUserFirstNames(), args.getOtherUserLastName(), args.getOtherUserAvatarUrl(), args.getOtherUserId());

        rootView = (ConstraintLayout) inflater.inflate(R.layout.material_fragment_conversation, container, false);
        bottomDivider = rootView.findViewById(R.id.conversationDivider);

        restoreState(savedInstanceState);

        setScreenTitle();

        setUpLoadingView();
        setUpErrorView();
        setUpProgressView();
        setUpRecyclerView();
        setUpSwipeRefreshLayout();
        setUpSendMessageButton();
        setUpNewMessageEditText();
        setUpReplyButton();
        setUpSendingMessageProgressBar();
        setUpDeclineButton();
        showSendingMessageState(showProgressBar);

        observeLoadConversation();
        observeSendNewMessage();
        observeConfirmSendMessage();
        observeConfirmEditMessage();
        observeConfirmDeclineConversation();
        observeDeclineConversation();

        if (shouldLoadConversation(savedInstanceState)) {
            canShowErrorView(true);
            showLoadingView(true);
            loadConversation();
        }

        return rootView;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SAVED_STATE, new SavedState()
                .setMessage(txtNewMessage.getText().toString())
                .setUserOpenedMessageBar(hasUserOpenedMessageBar)
                .setUserScrolled(hasUserScrolled())
                .setClearMessage(clearMessage)
                .setShowProgressBar(showProgressBar));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.refresh_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_refresh) {
            if (canShowErrorView()) {
                loadConversationWithLoadingView();
            } else {
                swipeRefreshLayout.setRefreshing(true);
                loadConversation();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LOG_IN_REQUEST_CODE && resultCode == Activity.RESULT_OK)
            loadConversationWithLoadingView();
    }

    private void setUpErrorView() {
        errorView = rootView.findViewById(R.id.conversationErrorView);
    }

    private void setUpLoadingView() {
        loadingView = rootView.findViewById(R.id.conversationLoadingView);
    }

    private void setUpProgressView() {
        progressView = rootView.findViewById(R.id.conversationProgressView);
    }

    private void showLoadingView(boolean showLoadingView) {
        if (showLoadingView) {
            loadingView.setVisibility(View.VISIBLE);
            List<Integer> viewsToExcludeFormAnimation = new ArrayList<>();
            viewsToExcludeFormAnimation.add(R.id.conversationDetailsGreyBackground);
            viewsToExcludeFormAnimation.add(R.id.txtConversationDetailsDivider);

            AnimationUtils.INSTANCE.startFadeAnimation(loadingView, viewsToExcludeFormAnimation, ObjectAnimator.INFINITE);
        } else {
            loadingView.setVisibility(View.GONE);
            AnimationUtils.INSTANCE.clearAnimation(loadingView);
        }
    }

    private void shakeView(View view) {
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", 0, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics()));
        animation.setDuration(300);
        animation.setRepeatCount(0);
        animation.setInterpolator(new CycleInterpolator(2));
        animation.start();
    }

    private void showErrorView(boolean isConnectionError, String message) {
        showErrorView(isConnectionError, message, getString(R.string.retry).toUpperCase(), view -> loadConversationWithLoadingView());
    }

    private void showErrorViewNotLoggedIn(String message) {
        showErrorView(false, message, getString(R.string.log_in),
                view -> startActivityForResult(new Intent(getActivity(), NewExistingActivity.class), LOG_IN_REQUEST_CODE));
    }

    private void showErrorView(boolean isConnectionError, String message, String action, View.OnClickListener actionListener) {
        errorView.title(getString(isConnectionError ? R.string.no_connection : R.string.oops));
        errorView.message(message);
        errorView.icon(isConnectionError ? R.drawable.ic_offline : R.drawable.icon_error);
        errorView.action(actionListener, action);
        errorView.show(isConnectionError);
    }

    private boolean canShowErrorView() {
        return canShowErrorView;
    }

    private void canShowErrorView(boolean canShowErrorView) {
        this.canShowErrorView = canShowErrorView;
    }

    private void loadConversationWithLoadingView() {
        if (connectivityChecker.isConnected()) {
            showLoadingView(true);
            UiUtils.hideViews(errorView);
            loadConversation();
        } else {
            if (errorView.getVisibility() == View.VISIBLE && errorView.isConnectionError()) {
                AnimationUtils.INSTANCE.startFadeAnimation(errorView, new ArrayList<>(), 0);
                return;
            }

            showErrorView(true, getString(R.string.errorOffline_body));
        }
    }

    private boolean shouldLoadConversation(Bundle savedInstanceState) {
        ConversationResponse response = viewModel.getFetchConversation().getValue();
        return savedInstanceState == null || response == null || !response.isSuccessful();
    }

    private void restoreState(Bundle savedInstanceState) {
        if (savedInstanceState == null)
            return;

        SavedState savedState = (SavedState) savedInstanceState.getSerializable(SAVED_STATE);
        assert savedState != null;

        // clear the state keeping the message if fragment was reused
        if (viewModel.getFetchConversation().getValue() == null) {
            String message = savedState.getMessage();
            savedState = new SavedState().setMessage(message);
        }

        setUserScrolled(savedState.hasUserScrolled());
        hasUserOpenedMessageBar = savedState.hasUserOpenedMessageBar();
        message = savedState.getMessage();
        clearMessage = savedState.shouldClearMessage();
        showProgressBar = savedState.shouldShowProgressBar();
    }

    private void setUpRecyclerView() {
        recyclerViewConversation = rootView.findViewById(R.id.conversationDetailsRecyclerView);
        recyclerViewConversation.setHasFixedSize(true);
        recyclerViewConversation.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getActivity(), R.drawable.horizontal_divider), true));
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewConversation.setLayoutManager(linearLayoutManager);
        recyclerViewConversation.setAdapter(conversationAdapter);
        recyclerViewConversation.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            // scroll recycler view to the bottom if user opens keyboard and hasn't scrolled manually
            if (bottom < oldBottom && txtNewMessage.isFocused() && !hasUserScrolled()) {
                recyclerViewConversation.removeCallbacks(scrollToBottomOnFocusChange);
                recyclerViewConversation.post(scrollToBottomOnFocusChange);
            }
        });

        recyclerViewConversation.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    setUserScrolled(true);
                } else if (newState == RecyclerView.SCROLL_STATE_IDLE && linearLayoutManager.findLastVisibleItemPosition() == conversationAdapter.getItemCount() - 1) {
                    setUserScrolled(false);
                }
            }
        });
    }

    private void setUpSwipeRefreshLayout() {
        swipeRefreshLayout = rootView.findViewById(R.id.conversationSwipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.material_deep_sea_blue));
        swipeRefreshLayout.setOnRefreshListener(this::loadConversation);
    }

    private void setUpSendMessageButton() {
        btnSendNewMessage = rootView.findViewById(R.id.imgConversationSend);
        btnSendNewMessage.setOnClickListener(v -> {
            if (v.getTag(R.id.enabled) != null && !(Boolean) v.getTag(R.id.enabled)) {
                shakeView(btnSendNewMessage);
                return;
            }

            NewMessage newMessage = new NewMessage();
            newMessage.setMessageId(getLastMessageId());
            newMessage.setMessage(txtNewMessage.getText().toString());
            newMessage.setFormAction(NewMessage.FORM_ACTION_REPLY);
            sendMessage(newMessage);
        });
    }

    private void setUpNewMessageEditText() {
        txtNewMessage = rootView.findViewById(R.id.txtConversationMessage);
        txtNewMessage.setHint(getString(R.string.conversation_writeReplyTo, otherUserFirstName));
        txtNewMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                toggleEnableSendButton();
            }
        });

        txtNewMessage.setText(message);
    }

    private void setUpReplyButton() {
        btnReply = rootView.findViewById(R.id.btnConversationReply);
        btnReply.setOnClickListener(v -> {
            toggleBottomBarVisibility(false, true);
            requestFocusOnNewMessageEditText();
            hasUserOpenedMessageBar = true;
        });
    }

    private void setUpDeclineButton() {
        btnDecline = rootView.findViewById(R.id.btnConversationDecline);
        btnDecline.setOnClickListener(v -> DeclineConversationDialog.getInstance(otherUserFirstName).show(this, DeclineConversationDialog.TAG));
    }

    private void setUpSendingMessageProgressBar() {
        progressBarSendingMessage = rootView.findViewById(R.id.progressBarConversationSend);
    }

    private void observeLoadConversation() {
        viewModel.getFetchConversation().observe(this, conversationResponse -> {
            swipeRefreshLayout.setRefreshing(false);
            showLoadingView(false);

            if (clearMessage) {
                txtNewMessage.setText("");
                txtNewMessage.clearFocus();
                clearMessage = false;
                showSendingMessageState(false);
            }

            if (!isUserLoggedIn()) {
                if (canShowErrorView()) {
                    showErrorViewNotLoggedIn(getString(R.string.conversation_loadConversation_error_log_in));
                    return;
                }

                showLogInSnackbar();
                return;
            }

            assert conversationResponse != null;
            if (conversationResponse.isSuccessful()) {
                KeyboardUtils.hideKeyboard(txtNewMessage);
                updateAdapter(conversationResponse);
                canShowErrorView(false);
                UiUtils.hideViews(errorView);
                return;
            }

            if (canShowErrorView()) {
                boolean isConnectionError = conversationResponse.isNoConnectionException();
                String errorMessage = isConnectionError ?
                        getString(R.string.errorOffline_body) : conversationResponse.getErrorMessage(getActivity(), getString(R.string.conversation_loadConversation_error));
                showErrorView(isConnectionError, errorMessage);
                return;
            }

            showErrorSnackbar(conversationResponse.getErrorMessage(getActivity(), getString(R.string.conversation_loadConversation_error)));
        });
    }

    private void observeSendNewMessage() {
        viewModel.getSendMessage().observe(this, messageResponse -> {
            if (!isUserLoggedIn()) {
                showLogInSnackbar();
                showSendingMessageState(false);
                return;
            }

            assert messageResponse != null;
            if (messageResponse.isMessageWithExternalLinks()) {
                showSendingMessageState(false);
                SendMessageConfirmationDialog.getInstance(otherUserFirstName).show(this, SendMessageConfirmationDialog.TAG);
                return;
            }

            if (!messageResponse.isSuccessful()) {
                showSendingMessageState(false);
                showErrorSnackbar(messageResponse.getErrorMessage(getActivity(), getString(R.string.failedToSend)));
                return;
            }

            clearMessage = true;
            loadConversation();
        });
    }

    private void observeConfirmSendMessage() {
        viewModel.getSendNewMessageConfirmation().observe(this, sendNewMessageConfirmationEvent -> {
            NewMessage newMessage = new NewMessage();
            newMessage.setMessageId(getLastMessageId());
            newMessage.setMessage(txtNewMessage.getText().toString());
            newMessage.setFormAction(NewMessage.FORM_ACTION_REPLY);
            newMessage.setConfirm(NewMessage.CONFIRMED);
            sendMessage(newMessage);
        });
    }

    private void observeConfirmEditMessage() {
        viewModel.getEditNewMessageConfirmation().observe(this, editNewMessageConfirmation -> requestFocusOnNewMessageEditText());
    }

    private void requestFocusOnNewMessageEditText() {
        postDelayed(() -> {
            txtNewMessage.setSelection(txtNewMessage.getText().length());
            txtNewMessage.requestFocus();
            KeyboardUtils.showKeyboard(txtNewMessage);
        });
    }

    private void observeConfirmDeclineConversation() {
        viewModel.getDeclineConversationConfirmation().observe(this, declineConversationEvent -> {
            progressView.show(getString(R.string.declining_conversation));
            setUserScrolled(false);
            viewModel.getDeclineConversation().decline(getLastMessageId(), getDeclineTemplate());
        });
    }

    private void observeDeclineConversation() {
        viewModel.getDeclineConversation().observe(this, response -> {
            progressView.hide();

            if (!isUserLoggedIn()) {
                showLogInSnackbar();
                return;
            }

            assert response != null;
            if (!response.isSuccessful()) {
                showErrorSnackbar(response.getErrorMessage(getActivity(), getString(R.string.failedToDecline)));
                return;
            }

            toggleBottomBarVisibility(false, false);
            SnackBarUtils.show(rootView, getString(R.string.declined));
            loadConversation();
        });
    }

    private void loadConversation() {
        viewModel.getFetchConversation().loadConversation(new GetConversationRequest(otherUserId, advertId));
    }

    private void sendMessage(NewMessage newMessage) {
        toggleEnableSendButton(false);
        showSendingMessageState(true);
        setUserScrolled(false);
        viewModel.getSendMessage().sendNewMessage(newMessage);
    }

    private void showSendingMessageState(boolean isSending) {
        btnSendNewMessage.setVisibility(isSending ? View.GONE : View.VISIBLE);
        progressBarSendingMessage.setVisibility(isSending ? View.VISIBLE : View.GONE);
        showProgressBar = isSending;
        toggleEnableSendButton();
    }

    private String getLastMessageId() {
        return conversationAdapter.getLastMessageId();
    }

    private String getDeclineTemplate() {
        return NewMessage.E_TEMPLATE_DECLINE + conversationAdapter.getDeclineTemplate();
    }

    private void setUserScrolled(boolean scrolled) {
        hasUserScrolled = scrolled;
    }

    private boolean hasUserScrolled() {
        return hasUserScrolled;
    }

    private void updateAdapter(final ConversationResponse conversationResponse) {
        final Conversation conversation = conversationResponse.getResponse();
        conversationAdapter.update(conversationResponse);

        if (conversationAdapter.hasMoreThanOneMessage() && !hasUserScrolled())
            recyclerViewConversation.scrollToPosition(conversationAdapter.getItemCount() - 1);

        otherUserFirstName = StringUtils.getFirstString(conversation.getOtherUserFirstNames());
        otherUserLastName = conversation.getOtherUserLastName();
        setScreenTitle();

        boolean isConversationClosed = conversation.isClosed();
        boolean isLastMessageMyOwn = conversationAdapter.isLastMessageMyOwn();
        boolean showReplyBar = !isLastMessageMyOwn && !isConversationClosed && !hasUserOpenedMessageBar;
        boolean showNewMessageBar = conversationAdapter.hasMessages() && !isConversationClosed && (isLastMessageMyOwn || hasUserOpenedMessageBar);
        txtNewMessage.setHint(getString(isLastMessageMyOwn ? R.string.conversation_writeMessageTo : R.string.conversation_writeReplyTo, otherUserFirstName));

        toggleBottomBarVisibility(showReplyBar, showNewMessageBar);
        toggleEnableSendButton();
    }

    private void setScreenTitle() {
        setTitle(StringUtils.getInitialsWithFullFirstName(otherUserFirstName, otherUserLastName));
    }

    private void toggleBottomBarVisibility(boolean showReplyBar, boolean showNewMessageBar) {
        toggleReplyBarVisibility(showReplyBar);
        toggleNewMessageBarVisibility(showNewMessageBar);
        positionBottomDivider(showReplyBar, showNewMessageBar);
    }

    private void toggleNewMessageBarVisibility(boolean showNewMessageBar) {
        btnSendNewMessage.setVisibility(showNewMessageBar ? View.VISIBLE : View.GONE);
        txtNewMessage.setVisibility(showNewMessageBar ? View.VISIBLE : View.GONE);
    }

    private void toggleReplyBarVisibility(boolean showReplyBar) {
        btnReply.setVisibility(showReplyBar ? View.VISIBLE : View.GONE);
        btnDecline.setVisibility(showReplyBar ? View.VISIBLE : View.GONE);
    }

    private void positionBottomDivider(boolean showReplyBar, boolean showNewMessageBar) {
        bottomDividerConstraintSet.clone(rootView);
        bottomDividerConstraintSet.connect(bottomDivider.getId(), ConstraintSet.BOTTOM, showReplyBar ? btnReply.getId() : txtNewMessage.getId(), ConstraintSet.TOP);
        bottomDividerConstraintSet.applyTo(rootView);

        bottomDivider.setVisibility(showReplyBar || showNewMessageBar ? View.VISIBLE : View.GONE);
    }

    private void toggleEnableSendButton() {
        toggleEnableSendButton(true);
    }

    private void toggleEnableSendButton(boolean enable) {
        boolean enabled = enable && conversationAdapter.hasMessages() && !StringUtils.isNullOrEmpty(txtNewMessage.getText(), true);
        btnSendNewMessage.setTag(R.id.enabled, enabled);
        btnSendNewMessage.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(getActivity(), enabled ? R.color.orange : R.color.grey_3), PorterDuff.Mode.SRC_IN));
    }

    private void showErrorSnackbar(String message) {
        SnackBarUtils.show(rootView, message);
    }

    private boolean isUserLoggedIn() {
        return spareroomContext.isUserLoggedIn();
    }

    private void showLogInSnackbar() {
        SnackBarUtils.show(rootView, getString(R.string.conversation_please_log_in), getString(R.string.log_in),
                v -> startActivityForResult(new Intent(getActivity(), NewExistingActivity.class), LOG_IN_REQUEST_CODE));
    }

    private void postDelayed(final Runnable runnable) {
        handler.postDelayed(() -> {
            if (UiUtils.isFragmentAlive(this))
                runnable.run();
        }, KeyboardUtils.KEYBOARD_DELAY_MILLIS);
    }

    private static class SavedState implements Serializable {
        private boolean hasUserScrolled;
        private boolean hasUserOpenedMessageBar;
        private boolean clearMessage;
        private String message;
        private boolean showProgressBar;

        private boolean hasUserScrolled() {
            return hasUserScrolled;
        }

        private SavedState setUserScrolled(boolean hasUserScrolled) {
            this.hasUserScrolled = hasUserScrolled;
            return this;
        }

        private boolean hasUserOpenedMessageBar() {
            return hasUserOpenedMessageBar;
        }

        private SavedState setUserOpenedMessageBar(boolean hasUserOpenedMessageBar) {
            this.hasUserOpenedMessageBar = hasUserOpenedMessageBar;
            return this;
        }

        private String getMessage() {
            return message != null ? message : "";
        }

        private SavedState setMessage(String message) {
            this.message = message;
            return this;
        }

        private boolean shouldClearMessage() {
            return clearMessage;
        }

        private SavedState setClearMessage(boolean clearMessage) {
            this.clearMessage = clearMessage;
            return this;
        }

        private boolean shouldShowProgressBar() {
            return showProgressBar;
        }

        private SavedState setShowProgressBar(boolean showProgressBar) {
            this.showProgressBar = showProgressBar;
            return this;
        }
    }
}
