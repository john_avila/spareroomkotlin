package com.spareroom.ui.screen.legacy;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.controller.legacy.*;
import com.spareroom.integration.analytics.AnalyticsTags;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.sharedpreferences.UploadVideoDAO;
import com.spareroom.lib.core.IObserver;
import com.spareroom.lib.core.LongTask;
import com.spareroom.lib.util.HardwareFeatures;
import com.spareroom.model.business.*;
import com.spareroom.ui.VideoUploadApplicationObserver;
import com.spareroom.ui.adapter.legacy.ManagePhotosAdapter;
import com.spareroom.ui.screen.Activity;
import com.spareroom.ui.screen.AlertDialogBuilder;
import com.spareroom.ui.util.*;

import java.io.File;
import java.util.LinkedList;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

public class ManagePhotosActivity extends Activity implements IAsyncResult {

    private static final String CAMERA_FILE_URI = "CAMERA_FILE_URI";

    //region FIELDS UI

    private ProgressBar _pb;
    private ListView _lv;
    private View bAddPicture;
    private ManagePhotosAdapter adapter;

    //endregion FIELDS UI

    //region FIELDS CONTROLLER

    private PictureGallery _pictureGallery;
    private VideoGallery _videoGallery;

    private AsyncTaskManager _asyncManager;
    private String _id;
    private String _userType;

    private boolean _maxVideoReached = false;
    private boolean _maxPhotosReached = false;

    private String fileUri;

    private int _maxPhotos;
    private int _maxVideos;

    private static final int PHOTO_GALLERY_REQUEST = 50;
    private static final int PHOTO_CAMERA_REQUEST = 51;
    private static final int VIDEO_GALLERY_REQUEST = 52;
    private static final int VIDEO_CAMERA_REQUEST = 53;
    private static final int VIDEO_GALLERY_PERMISSION_REQUEST = 60;
    private static final int PHOTO_CAMERA_PERMISSIONS_REQUEST = 61;

    public static final String EXTRA_KEY_PHOTO_UPLOAD_LIMIT = "photo_upload_limit";
    public static final String EXTRA_KEY_FLATSHARE_ID = "flatshare_id";
    public static final String EXTRA_KEY_FLATSHARE_TYPE = "flatshare_type";
    public static final String EXTRA_KEY_AT_RETRIEVE = "at_retrieve";
    @SuppressWarnings("SpellCheckingInspection")
    public static final String EXTRA_KEY_AT_EDIT_CAPTION = "at_editcaption";
    public static final String EXTRA_KEY_AT_DELETE = "at_delete";
    @SuppressWarnings("SpellCheckingInspection")
    public static final String EXTRA_KEY_AT_CHANGE_ORDER = "at_changeorder";

    // VIDEO //

    public static PendingIntent startFromNotificationIntent(Context context, int maxPhotos, String flatshareId, String flatshareType) {
        Intent intent = new Intent(context, ManagePhotosActivity.class)
                .putExtra(ManagePhotosActivity.EXTRA_KEY_PHOTO_UPLOAD_LIMIT, maxPhotos)
                .putExtra(ManagePhotosActivity.EXTRA_KEY_FLATSHARE_ID, flatshareId)
                .putExtra(ManagePhotosActivity.EXTRA_KEY_FLATSHARE_TYPE, flatshareType);

        return PendingIntent.getActivity(context, 0, intent, 0);
    }

    /**
     * Manages the delete video response
     */
    private final IAsyncResult deleteVideoAsyncResult = new IAsyncResult() {

        @Override
        public void update(Object o) {
            ManagePhotosActivity.this.setResult(Activity.RESULT_OK);
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tVideoDeleted), Toast.LENGTH_LONG).show();
            UploadVideoDAO.delete(ManagePhotosActivity.this, _id);

            adapter.notifyDataSetChanged();

            launchVideosTask();
        }

        @Override
        public void handleInvalidUserException(String message) {
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tVideoDeletedError), Toast.LENGTH_LONG).show();
            _pb.setVisibility(View.GONE);
        }

        @Override
        public void handleMissingParameterException(String message) {
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tVideoDeletedError), Toast.LENGTH_LONG).show();
            _pb.setVisibility(View.GONE);
        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tVideoDeletedError), Toast.LENGTH_LONG).show();
            _pb.setVisibility(View.GONE);
        }

        @Override
        public void handleServiceUnavailableException(String message) {
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tVideoDeletedError), Toast.LENGTH_LONG).show();
            _pb.setVisibility(View.GONE);
        }

        @Override
        public void handleInconsistentStateException(String message) {
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tVideoDeletedError), Toast.LENGTH_LONG).show();
            _pb.setVisibility(View.GONE);
        }

    };

    // PHOTO //

    private final class UploadPictureObserver implements ControllerActionObserver {

        @Override
        public void onResult(Object o) {
            setResult(Activity.RESULT_OK);
            ToastUtils.showToast(R.string.activity_manage_tPictureUploaded);
            launchPhotosTask();
        }

        @Override
        public void onSpareroomStatus(SpareroomStatus status) {
            hideProgressbar();
            ToastUtils.showToast(status.getMessage());
        }

        @Override
        public void onException(Exception exception) {
            hideProgressbar();
            ToastUtils.showToast(R.string.activity_manage_tPictureUploadedError);
        }

        private void hideProgressbar() {
            _pb.setVisibility(View.GONE);
        }
    }

    /**
     * Manages the delete picture response
     */
    private final IAsyncResult deleteAsyncResult = new IAsyncResult() {

        @Override
        public void update(Object o) {
            ManagePhotosActivity.this.setResult(Activity.RESULT_OK);
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tPictureDeleted), Toast.LENGTH_LONG).show();

            launchPhotosTask();
        }

        @Override
        public void handleInvalidUserException(String message) {
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tPictureDeletedError), Toast.LENGTH_LONG).show();
        }

        @Override
        public void handleMissingParameterException(String message) {
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tPictureDeletedError), Toast.LENGTH_LONG).show();
        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tPictureDeletedError), Toast.LENGTH_LONG).show();
        }

        @Override
        public void handleServiceUnavailableException(String message) {
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tPictureDeletedError), Toast.LENGTH_LONG).show();
        }

        @Override
        public void handleInconsistentStateException(String message) {
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tPictureDeletedError), Toast.LENGTH_LONG).show();
        }

    };

    /**
     * Manages the update picture (like adding a caption, change the order, ...) response
     */
    private final IAsyncResult updateAsyncResult = new IAsyncResult() {

        @Override
        public void update(Object o) {
            ManagePhotosActivity.this.setResult(Activity.RESULT_OK);
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tPictureUpdated), Toast.LENGTH_LONG).show();

            launchPhotosTask();
        }

        @Override
        public void handleInvalidUserException(String message) {
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tPictureUpdatedError), Toast.LENGTH_LONG).show();
        }

        @Override
        public void handleMissingParameterException(String message) {
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tPictureUpdatedError), Toast.LENGTH_LONG).show();
        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tPictureUpdatedError), Toast.LENGTH_LONG).show();
        }

        @Override
        public void handleServiceUnavailableException(String message) {
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tPictureUpdatedError), Toast.LENGTH_LONG).show();
        }

        @Override
        public void handleInconsistentStateException(String message) {
            Toast.makeText(ManagePhotosActivity.this, getString(R.string.activity_manage_tPictureUpdatedError), Toast.LENGTH_LONG).show();
        }

    };

    // OBSERVER

    private final IObserver _videoLongTaskObserver = o -> runOnUiThread(this::launchVideosTask);

    //endregion FIELDS CONTROLLER

    //region METHODS UI

    private void addManagePhoto() {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_picture_source, null);

        if (view != null) {
            TextView tvPhotoCamera = view.findViewById(R.id.dialog_tvPhotoCamera);
            TextView tvPhotoGallery = view.findViewById(R.id.dialog_tvPhotoGallery);
            TextView tvVideoCamera = view.findViewById(R.id.dialog_tvVideoCamera);
            tvVideoCamera.setVisibility(HardwareFeatures.supportsCamera(this) && HardwareFeatures.supportsMicrophone(this) ? View.VISIBLE : View.GONE);
            TextView tvVideoGallery = view.findViewById(R.id.dialog_tvVideoGallery);

            Dialog d = new AlertDialogBuilder(this, view).create();

            if (_maxPhotosReached) {
                tvPhotoCamera.setText(String.format("%s\n%s", getString(R.string.dialog_tvCamera), getString(R.string.dialog_tvPhotoMax)));
                tvPhotoGallery.setText(String.format("%s\n%s", getString(R.string.dialog_tvGallery), getString(R.string.dialog_tvPhotoMax)));

            } else {
                tvPhotoCamera.setOnClickListener(new PhotoCameraOnClickListener(d));
                tvPhotoGallery.setOnClickListener(new PhotoGalleryOnClickListener(d));

            }

            if (_maxVideoReached) {
                tvVideoCamera.setText(String.format("%s\n%s", getString(R.string.dialog_tvVideo), getString(R.string.dialog_tvVideoMax)));
                tvVideoGallery.setText(String.format("%s\n%s", getString(R.string.dialog_tvVideoGallery), getString(R.string.dialog_tvVideoMax)));

            } else {
                tvVideoCamera.setOnClickListener(new VideoCameraOnClickListener(d));
                tvVideoGallery.setOnClickListener(new VideoGalleryOnClickListener(d));

            }

            tvPhotoCamera.setCompoundDrawablesWithIntrinsicBounds(
                    UiUtils.getTintedDrawable(this, R.drawable.ic_camera, R.color.material_deep_sea_blue), null, null, null);
            tvPhotoGallery.setCompoundDrawablesWithIntrinsicBounds(
                    UiUtils.getTintedDrawable(this, R.drawable.ic_photos, R.color.material_deep_sea_blue), null, null, null);
            tvVideoCamera.setCompoundDrawablesWithIntrinsicBounds(
                    UiUtils.getTintedDrawable(this, R.drawable.ic_video_camera, R.color.material_deep_sea_blue), null, null, null);
            tvVideoGallery.setCompoundDrawablesWithIntrinsicBounds(
                    UiUtils.getTintedDrawable(this, R.drawable.ic_videos, R.color.material_deep_sea_blue), null, null, null);

            d.show();
        }

    } //end addManagePhoto()

    private Video.Status getPendingVideoStatus() {

        if (LongTaskManager.getInstance().getLongTask(UploadVideoTask.ID) != null)
            return Video.Status.UPLOADING;

        else {
            if (UploadVideoDAO.read(this, _id) == UploadVideoDAO.VIDEO_UPLOAD_FAILED)
                return Video.Status.UPLOAD_FAILED;
            else
                return Video.Status.BLANK;
        }

    }

    private int getNumberProcessing(VideoGallery videoGallery) {

        if ((videoGallery != null) && (videoGallery.size() > 0))
            return videoGallery.size();

        else {
            if (LongTaskManager.getInstance().getLongTask(UploadVideoTask.ID) != null)
                return 1;

            else {
                if (UploadVideoDAO.read(this, _id) == UploadVideoDAO.VIDEO_UPLOAD_FAILED)
                    return 1;
                else
                    return 0;
            }
        }

    }

    private VideoGallery getUpdatedVideoGallery(VideoGallery uploadedVideoGallery, Video.Status pendingVideoStatus) {
        LinkedList<Video> videoLinkedList = uploadedVideoGallery.getGallery();
        Video video = new Video();

        switch (pendingVideoStatus) {
            case UPLOAD_FAILED:
                video.setStatus(Video.Status.UPLOAD_FAILED);
                videoLinkedList.add(video);
                uploadedVideoGallery.setGallery(videoLinkedList);
                break;

            case UPLOADING:
                video.setStatus(Video.Status.UPLOADING);
                videoLinkedList.add(video);
                uploadedVideoGallery.setGallery(videoLinkedList);
                break;

            default:
                break;
        }

        return uploadedVideoGallery;

    }

    @Override
    public void update(Object o) {
        setResult(Activity.RESULT_OK);

        if (o instanceof PictureGallery)
            _pictureGallery = (PictureGallery) o;
        else {
            _videoGallery = (VideoGallery) o;
            _maxVideoReached = (getNumberProcessing(_videoGallery) >= _maxVideos);
        }

        if (_videoGallery != null && _pictureGallery != null) {
            _maxPhotosReached = (_maxPhotos - _pictureGallery.get_gallery().size()) == 0;
            _maxVideoReached = (_maxVideos - _videoGallery.getGallery().size()) == 0;

            TextView tvEmpty = findViewById(R.id.activity_manage_tvEmpty);

            Video.Status videoStatus = getPendingVideoStatus();
            _videoGallery = getUpdatedVideoGallery(_videoGallery, videoStatus);

            _pb.setVisibility(View.GONE);

            int photos = _pictureGallery.get_gallery().size();
            int videos = _videoGallery.getGallery().size();

            setSubtitle(String.format(Locale.getDefault(), "%s %s, %s %s",
                    photos,
                    getResources().getQuantityString(R.plurals.photos, photos),
                    videos,
                    getResources().getQuantityString(R.plurals.videos, videos)));

            if (tvEmpty != null) {
                if ((_pictureGallery.size() == 0) && (_videoGallery.size() == 0))
                    tvEmpty.setVisibility(View.VISIBLE);
                else
                    tvEmpty.setVisibility(View.GONE);

                adapter.stop();
                adapter.setPictureGallery(_pictureGallery);
                adapter.setVideoGallery(_videoGallery);

                adapter.notifyDataSetChanged();
            }

            bAddPicture.setVisibility(View.VISIBLE);
            if (!_maxPhotosReached || !_maxVideoReached) {
                bAddPicture.setClickable(true);
                bAddPicture.setVisibility(View.VISIBLE);
                bAddPicture.setBackgroundResource(R.drawable.material_orange_ripple);
            } else {
                bAddPicture.setClickable(false);
                bAddPicture.setVisibility(View.GONE);
            }
        }

    } //end update(Object o)

    @Override
    public void handleInvalidUserException(String message) {

    }

    @Override
    public void handleMissingParameterException(String message) {

    }

    @Override
    public void handleMissingSystemFeatureException(String message) {

    }

    @Override
    public void handleServiceUnavailableException(String message) {

    }

    @Override
    public void handleInconsistentStateException(String message) {

    }

    //endregion METHODS UI

    //region METHODS CONTROLLER

    private void launchPhotosTask() {
        Parameters p = new Parameters();
        AsyncTask<Parameters, Integer, Object> at;

        _pb.setVisibility(View.VISIBLE);

        if (_id != null)
            p.add(EXTRA_KEY_FLATSHARE_ID, _id);

        at = instantiateRetrievePicturesAsyncTask();
        _asyncManager.register(at);
        at.execute(p);

    }

    private void launchVideosTask() {
        Parameters p = new Parameters();
        AsyncTask<Parameters, Integer, Object> at;
        Parameters[] vParameters = new Parameters[1];

        _pb.setVisibility(View.VISIBLE);

        if (_id != null)
            p.add(EXTRA_KEY_FLATSHARE_ID, _id);
        vParameters[0] = p;

        at = instantiateRetrieveVideoGalleryAsyncTask();
        _asyncManager.register(at);
        at.execute(vParameters);

    }

    /**
     * Remove the video from the shared preferences
     */
    public final void deleteVideoUpload() {
        adapter.clearVideo();
        adapter.notifyDataSetChanged();

        bAddPicture.setClickable(true);
        bAddPicture.setBackgroundResource(R.drawable.material_orange_ripple);

        _maxVideoReached = (getNumberProcessing(_videoGallery) >= _maxVideos);
    }

    // VIDEOS //
    private AsyncTask<Parameters, Integer, Object> instantiateRetrieveVideoGalleryAsyncTask() {
        return new RetrieveVideoGalleryAsyncTask(SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(), this);
    }

    public final AsyncTask<Parameters, Integer, Object> instantiateDeleteVideoAsyncTask() {
        return new DeleteVideoAsyncTask(SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(), deleteVideoAsyncResult);
    }

    // PICTURES //
    private AsyncTask<Parameters, Integer, Object> instantiateRetrievePicturesAsyncTask() {
        return new RetrievePhotosAsyncTask(SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(), this);
    }

    public final AsyncTask<Parameters, Integer, Object> instantiateEditCaptionAsyncTask() {
        return new ChangeOrderPictureAsyncTask(
                SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(),
                SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                updateAsyncResult);
    }

    public final AsyncTask<Parameters, Integer, Object> instantiateDeletePictureAsyncTask() {
        return new DeletePictureAsyncTask(
                SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(),
                SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                deleteAsyncResult);
    }

    public final AsyncTask<Parameters, Integer, Object> instantiateChangeOrderAsyncTask() {
        return new ChangeOrderPictureAsyncTask(
                SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(),
                SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                updateAsyncResult);
    }

    private void refreshGallery(String path) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(path);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }

    private void takePhoto() {
        File mediaFile = FileUtils.createNewFileForPhoto();
        fileUri = Uri.fromFile(mediaFile).toString();
        Launcher.launchCamera(this, PHOTO_CAMERA_REQUEST, mediaFile);
    }

    private void takeVideoFromGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("video/*");
        photoPickerIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(photoPickerIntent, VIDEO_GALLERY_REQUEST);
    }

    //endregion METHODS CONTROLLER

    //region METHODS LIFECYCLE

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_photos);
        _maxPhotos = getIntent().getIntExtra(EXTRA_KEY_PHOTO_UPLOAD_LIMIT, 5);
        _maxVideos = 1;
        _userType = getIntent().getStringExtra(EXTRA_KEY_FLATSHARE_TYPE);
        _id = getIntent().getStringExtra(EXTRA_KEY_FLATSHARE_ID);

        fileUri = savedInstanceState == null ? null : savedInstanceState.getString(CAMERA_FILE_URI);

        _asyncManager = new AsyncTaskManager();

        setToolbar();

        bAddPicture = findViewById(R.id.activity_manage_bAddPicture);
        bAddPicture.setOnClickListener(v -> addManagePhoto());

        _pb = findViewById(R.id.activity_manage_photos_pb);
        _lv = findViewById(R.id.activity_manage_photos_lv);

        init();
    }

    private void init() {
        _lv.setAdapter(adapter =
                new ManagePhotosAdapter(this, new PictureGallery(), new VideoGallery(), _id, getIntent().getStringExtra(EXTRA_KEY_FLATSHARE_TYPE)));

        launchPhotosTask();
        launchVideosTask();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        _maxPhotos = intent.getIntExtra(EXTRA_KEY_PHOTO_UPLOAD_LIMIT, 5);
        _maxVideos = 1;
        _userType = intent.getStringExtra(EXTRA_KEY_FLATSHARE_TYPE);
        _id = intent.getStringExtra(EXTRA_KEY_FLATSHARE_ID);

        init();
    }

    @Override
    protected void onPause() {
        super.onPause();

        LongTask uploadVideoTask = LongTaskManager.getInstance().getLongTask(UploadVideoTask.ID);

        if (uploadVideoTask != null) {
            uploadVideoTask.removeObserver(_videoLongTaskObserver);
            NotificationUtils.showVideoUploadNotification(this, getString(R.string.notification_video_uploading_message),
                    startFromNotificationIntent(getApplicationContext(), _maxPhotos, _id, _userType), true);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        _asyncManager.cancelAll();

        adapter.stop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(CAMERA_FILE_URI, fileUri);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == PHOTO_GALLERY_REQUEST
                || requestCode == VIDEO_GALLERY_REQUEST
                || requestCode == PHOTO_CAMERA_REQUEST
                || requestCode == VIDEO_CAMERA_REQUEST) {

            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == PHOTO_CAMERA_REQUEST || requestCode == PHOTO_GALLERY_REQUEST) {
                    _pb.setVisibility(View.VISIBLE);

                    NewPicture picture = new NewPicture();
                    picture.setFlatshareId(_id);
                    picture.setUri(requestCode == PHOTO_GALLERY_REQUEST ? intent.getData().toString() : fileUri);

                    UploadPictureControllerAction uploadPictureControllerAction = UploadPictureControllerAction.get(new UploadPictureObserver());
                    addObserver(uploadPictureControllerAction);
                    uploadPictureControllerAction.execute(picture);
                } else {
                    String realPath = null;
                    Cursor cursor = null;
                    Uri resourceUri = intent.getData();

                    if (resourceUri != null)
                        cursor = getContentResolver().query(resourceUri, null, null, null, null);

                    if (cursor == null) {
                        if (resourceUri != null)
                            realPath = resourceUri.getPath();
                    } else {
                        cursor.moveToFirst();
                        realPath = cursor.getString(
                                cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA));
                        cursor.close();
                    }

                    MediaPlayer mp;
                    File f;

                    if (StringUtils.isNullOrEmpty(realPath) || (mp = MediaPlayer.create(this, resourceUri)) == null || !(f = new File(realPath)).exists()) {
                        ToastUtils.showToast(getString(R.string.incorrect_video_file));
                        return;
                    }

                    refreshGallery(realPath);

                    int duration = mp.getDuration();
                    mp.reset();
                    mp.release();

                    if (duration > Video.MAX_LENGTH_IN_MILLIS) {
                        int minutes = (Video.MAX_LENGTH_IN_MILLIS / 1000) / 60;
                        int seconds = (Video.MAX_LENGTH_IN_MILLIS / 1000) % 60;
                        Toast.makeText(this,
                                "The video selected is too long (maximum length allowed " +
                                        Integer.toString(minutes) +
                                        " min" + ((seconds > 0) ? " " + (Integer.toString(seconds) +
                                        " seconds") : "") + ")",
                                Toast.LENGTH_LONG).show();

                        if (requestCode == VIDEO_CAMERA_REQUEST) {
                            if (_userType.equals("offered"))
                                AnalyticsTrackerComposite
                                        .getInstance()
                                        .trackEvent_AdvertVideo_ErrorUploadingOfferedCamera(AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_LONG);
                            else
                                AnalyticsTrackerComposite
                                        .getInstance()
                                        .trackEvent_AdvertVideo_ErrorUploadingWantedCamera(AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_LONG);

                        } else {
                            if (_userType.equals("offered"))
                                AnalyticsTrackerComposite
                                        .getInstance()
                                        .trackEvent_AdvertVideo_ErrorUploadingOfferedGallery(AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_LONG);
                            else
                                AnalyticsTrackerComposite
                                        .getInstance()
                                        .trackEvent_AdvertVideo_ErrorUploadingWantedGallery(AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_LONG);

                        }

                    } else if (f.length() > Video.MAX_SIZE_IN_BYTES) {
                        Toast.makeText(
                                this,
                                "The video selected is too large (maximum size " + String.format("%s", ((Video.MAX_SIZE_IN_BYTES / 1024f) / 1024f)) + " MB)",
                                Toast.LENGTH_LONG).show();

                        if (requestCode == VIDEO_CAMERA_REQUEST) {
                            if (_userType.equals("offered"))
                                AnalyticsTrackerComposite
                                        .getInstance()
                                        .trackEvent_AdvertVideo_ErrorUploadingOfferedCamera(AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_BIG);
                            else
                                AnalyticsTrackerComposite
                                        .getInstance()
                                        .trackEvent_AdvertVideo_ErrorUploadingWantedCamera(AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_BIG);

                        } else {
                            if (_userType.equals("offered"))
                                AnalyticsTrackerComposite
                                        .getInstance()
                                        .trackEvent_AdvertVideo_ErrorUploadingOfferedGallery(AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_BIG);
                            else
                                AnalyticsTrackerComposite
                                        .getInstance()
                                        .trackEvent_AdvertVideo_ErrorUploadingWantedGallery(AnalyticsTags.VIDEO_UPLOAD_ERROR_TOO_BIG);

                        }

                    } else {
                        LongTask uploadVideoTask = LongTaskManager.getInstance().getLongTask(UploadVideoTask.ID);

                        if (uploadVideoTask == null) {
                            TextView tvEmpty = findViewById(R.id.activity_manage_tvEmpty);

                            UploadVideoDAO.saveVideoPath(this, _id, realPath);

                            adapter.addVideo();
                            adapter.notifyDataSetChanged();

                            if (tvEmpty != null)
                                tvEmpty.setVisibility(View.GONE);
                            _maxVideoReached = (getNumberProcessing(_videoGallery) >= _maxVideos);

                            Parameters params = new Parameters();
                            if (_id != null)
                                params.add(EXTRA_KEY_FLATSHARE_ID, _id);
                            uploadVideo(params);
                        }

                        if (requestCode == VIDEO_CAMERA_REQUEST) {
                            if (_userType.equals("offered"))
                                AnalyticsTrackerComposite.getInstance().trackEvent_AdvertVideo_UploadOfferedCamera();
                            else
                                AnalyticsTrackerComposite.getInstance().trackEvent_AdvertVideo_UploadWantedCamera();

                        } else {
                            if (_userType.equals("offered"))
                                AnalyticsTrackerComposite.getInstance().trackEvent_AdvertVideo_UploadOfferedGallery();
                            else
                                AnalyticsTrackerComposite.getInstance().trackEvent_AdvertVideo_UploadWantedGallery();

                        }

                    }
                }

            }
        }

    } //end onActivityResult(int requestCode, int resultCode, Intent intent)

    public void uploadVideo(Parameters params) {
        LongTask uploadVideoTask;
        String videoPath = UploadVideoDAO.retrieveVideoPath(this, _id);

        if (videoPath != null) {
            params.add("videoPath", videoPath);
            params.add("flatshare_type", _userType);
            params.add("caption", "");

            uploadVideoTask = new UploadVideoTask(SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(), params);
            uploadVideoTask.addObserver(_videoLongTaskObserver);
            uploadVideoTask.addObserver(new VideoUploadApplicationObserver(getApplicationContext(), _maxPhotos, _id, _userType));

            LongTaskManager.getInstance().addLongTask(uploadVideoTask);
            uploadVideoTask.run();

        } else {
            Toast.makeText(this, getString(R.string.activity_manage_photos_video_upload_video_not_found), Toast.LENGTH_LONG).show();
            UploadVideoDAO.delete(this, _id);

            deleteVideoUpload();
        }

    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {
            case VIDEO_GALLERY_PERMISSION_REQUEST:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    takeVideoFromGallery();
                } else {
                    Toast.makeText(this, getString(R.string.permission_denied), Toast.LENGTH_LONG).show();
                }
                break;
            case PHOTO_CAMERA_PERMISSIONS_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    takePhoto();
                } else {
                    Toast.makeText(this, getString(R.string.permission_denied), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.delete_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_item_delete:
                if ((_videoGallery != null && _videoGallery.size() != 0) || (_pictureGallery != null && _pictureGallery.size() != 0)) {

                    bAddPicture.setClickable(false);
                    bAddPicture.setBackgroundColor(ContextCompat.getColor(this, R.color.grey_5));

                    adapter.editMode();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    /**
     * Listener for the photo upload option, whe clicking the gallery option
     */
    private class PhotoGalleryOnClickListener implements OnClickListener {
        private final Dialog _dialog;

        private PhotoGalleryOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            Launcher.launchGallery(ManagePhotosActivity.this, PHOTO_GALLERY_REQUEST);
            _dialog.dismiss();
        }

    } //end class PhotoGalleryOnClickListener

    /**
     * Listener for the photo upload option, whe clicking the camera option
     */
    private class PhotoCameraOnClickListener implements OnClickListener {
        private final Dialog _dialog;

        private PhotoCameraOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            if (PermissionUtils.hasCameraPermissions(ManagePhotosActivity.this)) {
                takePhoto();
            } else {
                PermissionUtils.requestCameraPermissions(ManagePhotosActivity.this, PHOTO_CAMERA_PERMISSIONS_REQUEST);
            }

            _dialog.dismiss();

        }

    } //end class PhotoCameraOnClickListener

    /**
     * Listener for the video upload option, whe clicking the gallery option
     */
    private class VideoGalleryOnClickListener implements OnClickListener {
        private final Dialog _dialog;

        private VideoGalleryOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            if (PermissionUtils.hasStoragePermissions(ManagePhotosActivity.this)) {
                takeVideoFromGallery();
            } else {
                PermissionUtils.requestStoragePermissions(ManagePhotosActivity.this, VIDEO_GALLERY_PERMISSION_REQUEST);
            }
            _dialog.dismiss();
        }

    } //end class VideoGalleryOnClickListener

    /**
     * Listener for the video upload option, whe clicking the camera option
     */
    private class VideoCameraOnClickListener implements OnClickListener {
        private final Dialog _dialog;

        private VideoCameraOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            Intent intentCamera = new Intent(ManagePhotosActivity.this, CameraActivity.class);
            startActivityForResult(intentCamera, VIDEO_CAMERA_REQUEST);
            _dialog.dismiss();

        }

    } //end class VideoCameraOnClickListener

    //endregion INNER CLASSES

}
