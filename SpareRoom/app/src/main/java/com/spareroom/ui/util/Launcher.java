package com.spareroom.ui.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.provider.MediaStore;
import com.spareroom.ui.screen.Fragment;

import com.spareroom.R;

import java.io.File;
import java.util.List;

/**
 * Utility class for starting external apps, granting Uri permissions, etc.
 */
public class Launcher {

    private static final String INTENT_IMAGE_TYPE = "image/*";

    private static List<ResolveInfo> getIntentActivities(Context context, Intent intent) {
        return context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
    }

    private static void grantUriPermissions(Context context, List<ResolveInfo> intentActivities, Uri fileUri) {
        for (ResolveInfo resolveInfo : intentActivities) {
            context.grantUriPermission(resolveInfo.activityInfo.packageName, fileUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }
    }

    public static boolean launchCamera(Activity activity, int requestCode, File mediaFile) {
        Uri contentFileUri = FileUtils.getFileUri(activity, mediaFile);
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, contentFileUri);
        List<ResolveInfo> intentActivities = getIntentActivities(activity, takePictureIntent);

        if (!intentActivities.isEmpty()) {
            grantUriPermissions(activity, intentActivities, contentFileUri);
            activity.startActivityForResult(takePictureIntent, requestCode);
            return true;
        }

        return false;
    }

    public static boolean launchCamera(Fragment fragment, int requestCode, File mediaFile) {
        Uri contentFileUri = FileUtils.getFileUri(fragment.getActivity(), mediaFile);

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, contentFileUri);
        List<ResolveInfo> intentActivities = getIntentActivities(fragment.getActivity(), takePictureIntent);
        if (!intentActivities.isEmpty()) {
            grantUriPermissions(fragment.getActivity(), intentActivities, contentFileUri);
            fragment.startActivityForResult(takePictureIntent, requestCode);
            return true;
        }

        return false;
    }

    public static void launchGallery(Activity activity, int requestCode) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType(INTENT_IMAGE_TYPE);
        photoPickerIntent.addCategory(Intent.CATEGORY_OPENABLE);
        activity.startActivityForResult(Intent.createChooser(photoPickerIntent, activity.getString(R.string.pick_photo_from)), requestCode);
    }

    public static void launchGallery(Fragment fragment, int requestCode) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType(INTENT_IMAGE_TYPE);
        photoPickerIntent.addCategory(Intent.CATEGORY_OPENABLE);
        fragment.startActivityForResult(Intent.createChooser(photoPickerIntent, fragment.getString(R.string.pick_photo_from)), requestCode);
    }

    public static boolean canHandleIntent(Context context, Intent intent) {
        List<ResolveInfo> activities = getIntentActivities(context, intent);
        return activities != null && !activities.isEmpty();
    }
}
