package com.spareroom.ui.anim;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class PushInAnim extends Animation {
    private final int targetWidth;
    private final View view;
    private final boolean expanded;

    public PushInAnim(View view, int targetWidth, boolean expanded) {
        this.view = view;
        this.targetWidth = targetWidth;
        this.expanded = expanded;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        int newWidth;
        if (!expanded) {
            newWidth = (int) (targetWidth * interpolatedTime);
        } else {
            newWidth = (int) (targetWidth * (1 - interpolatedTime));
        }
        view.getLayoutParams().width = newWidth;
        view.requestLayout();
    }

    @Override
    public void initialize(int width, int height, int parentWidth,
                           int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}