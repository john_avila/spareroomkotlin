package com.spareroom.ui.screen;

import android.os.Bundle;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.lib.core.IObservable;
import com.spareroom.lib.core.IObserver;
import com.spareroom.model.business.AccountRegisterForm;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.ui.PasswordFieldValidator;
import com.spareroom.ui.controller.ActionUiController;
import com.spareroom.ui.controller.ViewUiController;
import com.spareroom.ui.util.AnimationUtils;
import com.spareroom.ui.widget.TextBox;

import java.util.LinkedList;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import static com.spareroom.ui.screen.RegisterActivity.KEY_REGISTER_FORM;

/**
 * Fragment for set the password for the registration process
 */
public class RegisterPasswordFragment extends Fragment {

    private TextBox tbPassword;

    //region METHODS LIFECYCLE

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        // Account register form
        AccountRegisterForm accountRegisterForm =
                (AccountRegisterForm) ((RegisterActivity) getActivity()).getSharedObject(KEY_REGISTER_FORM);
        //Views
        View rootView = inflater.inflate(R.layout.material_register_password_fragment, container, false);
        // Password input
        tbPassword = rootView.findViewById(R.id.registerPasswordFragment_passwordTextBox);
        // Bottom Toolbar
        View bottomToolbar = getActivity().findViewById(R.id.activity_toolbarBottom);
        final TextView tvContinueButton = bottomToolbar.findViewById(R.id.materialToolbarBottom_rightText);
        // Controllers
        ContinueButtonUiController continueButtonUiController = new ContinueButtonUiController(tvContinueButton);

        ViewsCompositeUiController viewsCompositeUiController =
                new ViewsCompositeUiController(new PasswordTextBoxUiController(tbPassword), new ContinueButtonUiController(tvContinueButton));
        TextBoxOnContentChangedListener textBoxOnContentChangedListener = new TextBoxOnContentChangedListener(viewsCompositeUiController);
        AccountCredentialsObserver accountCredentialsObserver =
                new AccountCredentialsObserver(accountRegisterForm, (LinearMultiStepActivity) getActivity());

        // Password
        textBoxOnContentChangedListener.addObserver(accountCredentialsObserver);
        tbPassword.setOnContentChangedListener(textBoxOnContentChangedListener);

        tbPassword.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                tvContinueButton.performClick();
                return true;
            }
            return false;
        });

        // Toolbar
        setToolbar(getActivity().findViewById(R.id.appBarLayout), getString(R.string.registerPasswordFragment_toolbarTitle), true);

        // Bottom Toolbar
        bottomToolbar.setVisibility(View.VISIBLE);

        tvContinueButton.setOnClickListener(new ContinueButtonOnClickListener(tbPassword, viewsCompositeUiController));
        tvContinueButton.setText(getString(R.string.registerPasswordFragment_bottomToolbar));

        viewsCompositeUiController.disable();

        // Account register form, loading previous values
        if (accountRegisterForm.getPassword() != null) {
            tbPassword.setText(accountRegisterForm.getPassword());
            if (accountRegisterForm.getPassword().length() > 0)
                continueButtonUiController.enable();
            else
                continueButtonUiController.disable();
        } else {
            continueButtonUiController.disable();
        }

        return rootView;
    } // end onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((RegisterActivity) getActivity()).showKeyboard();

    } //end onViewCreated(View view, @Nullable Bundle savedInstanceState)

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ((LinearMultiStepActivity) getActivity()).onNavigateBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        tbPassword.setOnContentChangedListener(null);
        tbPassword.setOnEditorActionListener(null);
    }

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    /**
     * Observer for updating the form in all its scopes.
     */
    private class AccountCredentialsObserver implements IObserver {
        private final AccountRegisterForm _accountRegisterForm;
        private final LinearMultiStepActivity _linearMultiStepActivity;

        AccountCredentialsObserver(AccountRegisterForm accountRegisterForm, LinearMultiStepActivity linearMultiStepActivity) {
            _linearMultiStepActivity = linearMultiStepActivity;
            _accountRegisterForm = accountRegisterForm;
        }

        @Override
        public void notifyObserver(Object o) {
            _accountRegisterForm.setPassword((String) o);
            _linearMultiStepActivity.setSharedObject(KEY_REGISTER_FORM, _accountRegisterForm);
        }

    } //end class AccountCredentialsObserver

    /**
     * Composite controller that updates all UI widgets involved in the process
     */
    private class ViewsCompositeUiController implements ViewUiController, ActionUiController {
        private final PasswordTextBoxUiController _passwordBoxUiController;
        private final ContinueButtonUiController _continueButtonUiController;

        ViewsCompositeUiController(PasswordTextBoxUiController passwordBoxUiController, ContinueButtonUiController continueButtonUiController) {
            _passwordBoxUiController = passwordBoxUiController;
            _continueButtonUiController = continueButtonUiController;
        }

        /**
         * Clears the error message if needed and disable the button
         */
        @Override
        public void disable() {
            _passwordBoxUiController.enable();
            _continueButtonUiController.disable();
        }

        /**
         * Clears the error message if needed and enable the button
         */
        @Override
        public void enable() {
            _passwordBoxUiController.enable();
            _continueButtonUiController.enable();
        }

        /**
         * Sets the views in "loading" mode
         */
        @Override
        public void setLoading() {
        }

        /**
         * Sets a status for this view (normally an error for fields that require validation)
         *
         * @param error a <code>SparerooomStatus</code> status
         */
        @Override
        public void setStatus(SpareroomStatus error) {

        }

        /**
         * Clears the status for this view
         */
        @Override
        public void clearStatus() {

        }

        @Override
        public void setResult(Object... o) {
        }

        /**
         * Sets the views in the proper status when there is a non successful reset for any reason
         * (API response, malformed email,...)
         */
        @Override
        public void setSpareroomStatus(SpareroomStatus spareroomStatus) {
            _passwordBoxUiController.setSpareroomStatus(spareroomStatus);
            _continueButtonUiController.setSpareroomStatus(spareroomStatus);
        }

        @Override
        public void setException(Exception exception) {
        }

    } //end class ViewsCompositeUiController

    /**
     * It is used for setting the different status for the continue button
     */
    private class ContinueButtonUiController implements ViewUiController, ActionUiController {
        private final TextView _tvContinueButton;

        ContinueButtonUiController(View bottomToolbar) {
            _tvContinueButton = bottomToolbar.findViewById(R.id.materialToolbarBottom_rightText);
        }

        /**
         * Sets the initial status: disable the button
         */
        @Override
        public void disable() {
            _tvContinueButton.setEnabled(false);
            _tvContinueButton.setVisibility(View.VISIBLE);
            _tvContinueButton.setTextColor(ContextCompat.getColor(_tvContinueButton.getContext(), R.color.light_blue));
        }

        /**
         * Sets the button ready for pressing the it
         */
        @Override
        public void enable() {
            _tvContinueButton.setEnabled(true);
            _tvContinueButton.setVisibility(View.VISIBLE);
            _tvContinueButton.setTextColor(ContextCompat.getColor(_tvContinueButton.getContext(), R.color.white));
        }

        /**
         * Sets the button in "loading" mode until the reset password response
         */
        @Override
        public void setLoading() {
        }

        /**
         * Sets a status for this view (normally an error for fields that require validation)
         *
         * @param error a <code>SparerooomStatus</code> status
         */
        @Override
        public void setStatus(SpareroomStatus error) {

        }

        /**
         * Clears the status for this view
         */
        @Override
        public void clearStatus() {

        }

        @Override
        public void setResult(Object... o) {
        }

        /**
         * Warns the user for non successful reset for any reason (API response, malformed email,...)
         */
        @Override
        public void setSpareroomStatus(SpareroomStatus spareroomStatus) {
            enable();
            AnimationUtils.INSTANCE.shakeLeft(_tvContinueButton);
        }

        @Override
        public void setException(Exception exception) {
        }

    } //end class ContinueButtonUiController

    /**
     * It is used for setting the the TextBox status.
     */
    private class PasswordTextBoxUiController implements ViewUiController, ActionUiController {
        private final TextBox _tbPassword;

        PasswordTextBoxUiController(TextBox tbPassword) {
            _tbPassword = tbPassword;
        }

        /**
         * Clears the error displayed. We don't change the text in the input field.
         */
        @Override
        public void enable() {
            if (!_tbPassword.isEnabled())
                _tbPassword.setEnabled(true);
        }

        @Override
        public void disable() {
        }

        @Override
        public void setLoading() {
        }

        /**
         * Sets a status for this view (normally an error for fields that require validation)
         *
         * @param error a <code>SparerooomStatus</code> status
         */
        @Override
        public void setStatus(SpareroomStatus error) {

        }

        /**
         * Clears the status for this view
         */
        @Override
        public void clearStatus() {

        }

        @Override
        public void setResult(Object... o) {
        }

        /**
         * Shows the proper error message
         */
        @Override
        public void setSpareroomStatus(SpareroomStatus spareroomStatus) {
            _tbPassword.setMessageText(
                    (spareroomStatus != null) ? spareroomStatus.getMessage() : getString(R.string.loginEmailFragment_errorGeneric));
            _tbPassword.showMessage();
        }

        @Override
        public void setException(Exception exception) {
        }

    } //end class PasswordTextBoxUiController

    private class ContinueButtonOnClickListener implements View.OnClickListener {
        private final TextBox _tbPassword;
        private final ViewsCompositeUiController _viewsCompositeUiController;
        private final PasswordFieldValidator _passwordFieldValidator;

        private ContinueButtonOnClickListener(TextBox tbPassword, ViewsCompositeUiController viewsCompositeUiController) {

            _tbPassword = tbPassword;
            _viewsCompositeUiController = viewsCompositeUiController;
            _passwordFieldValidator = new PasswordFieldValidator();
        }

        @Override
        public void onClick(View v) {
            String password = _tbPassword.getText();
            int passwordValidationState = _passwordFieldValidator.validate(password);

            if (passwordValidationState != PasswordFieldValidator.VALID) {

                switch (passwordValidationState) {
                    case PasswordFieldValidator.TOO_SHORT:
                    case PasswordFieldValidator.TOO_LONG:
                    case PasswordFieldValidator.INVALID_CHARACTERS:
                    case PasswordFieldValidator.EMPTY:
                        contentMalformed(passwordValidationState);
                        break;
                }

            } else
                ((RegisterActivity) getActivity()).nextFragment();

        }

        /**
         * Does the corresponding actions when the TextBox content is malformed
         */
        private void contentMalformed(int emailValidationState) {
            SpareroomStatus spareroomStatus = new SpareroomStatus();
            spareroomStatus.setCode(Integer.toString(emailValidationState));
            spareroomStatus.setMessage(getString(R.string.registerPasswordFragment_errorMessage));

            _viewsCompositeUiController.setSpareroomStatus(spareroomStatus);

        }

    } //end class RegisterPasswordButtonOnClickListener

    private class TextBoxOnContentChangedListener implements TextBox.OnContentChangedListener, IObservable {
        private final ViewsCompositeUiController _viewsCompositeUiController;
        private final LinkedList<IObserver> _observerList = new LinkedList<>();

        TextBoxOnContentChangedListener(ViewsCompositeUiController viewsCompositeUiController) {
            _viewsCompositeUiController = viewsCompositeUiController;
        }

        @Override
        public void onContentChangedEvent(CharSequence s) {
            if (s.length() > 0)
                _viewsCompositeUiController.enable();
            else
                _viewsCompositeUiController.disable();

            for (IObserver observer : _observerList)
                observer.notifyObserver(s.toString());

        }

        @Override
        public void addObserver(IObserver o) {
            _observerList.add(o);
        }

        @Override
        public void removeObserver(IObserver o) {
            _observerList.remove(o);
        }

    } //end class TextBoxOnContentChangedListener

    //endregion INNER CLASSES

}
