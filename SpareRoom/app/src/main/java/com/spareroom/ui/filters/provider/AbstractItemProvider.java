package com.spareroom.ui.filters.provider;

import android.app.Application;

import com.spareroom.integration.dependency.component.ComponentRepository;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListProperties.MaxTerm;
import com.spareroom.model.business.SearchAdvertListProperties.MinTerm;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static com.spareroom.lib.util.NumberUtil.MIN_AGE;
import static com.spareroom.lib.util.NumberUtil.isLowerThanMinAge;

public abstract class AbstractItemProvider {
    public final static String LENGTH_OF_STAY_MIN = "length_of_stay_min";
    public final static String LENGTH_OF_STAY_MAX = "length_of_stay_max";
    public final static String MOVE_IN = "move_in";
    public final static String NUMBER_OF_FLATMATES_MIN = "number_of_flatmates_min";
    public final static String NUMBER_OF_FLATMATES_MAX = "number_of_flatmates_max";
    public final static String SEARCH_RADIUS = "search_Radius";

    private String filterName;

    @Inject
    Application application;

    @Inject
    SubtitleUtil subtitleUtil;

    protected List<FilterItem> list;

    AbstractItemProvider() {
        ComponentRepository.get().getAppComponent().inject(this);
    }

    public abstract int titleId();

    public void createNewList(@NonNull SearchAdvertListProperties properties, @Nullable String filterName) {
        this.filterName = filterName;
    }

    public abstract Map<String, List<GroupedCompoundButtonParams>> groupList();

    public abstract void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties);

    String selectedValue(List<GroupedCompoundButtonParams> paramsList) {
        for (GroupedCompoundButtonParams params : paramsList) {
            if (params.selected())
                return params.value();
        }
        return null;
    }

    /**
     * In case there isn't any previous selection it finds it and select the default one
     */
    void selectedItem(List<GroupedCompoundButtonParams> group) {

        for (GroupedCompoundButtonParams params : group) {
            if (params.selected())
                return;
        }

        for (GroupedCompoundButtonParams params : group) {
            params.selected(params.defaultItem());
        }

    }

    public List<FilterItem> reset(List<FilterItem> itemProvider) {
        for (FilterItem item : itemProvider) {
            item.params().reset();
        }
        return itemProvider;
    }

    public List<FilterItem> get() {
        return list;
    }

    void sortAgeRange(SearchAdvertListProperties properties) {
        Integer min = checkMinAge(properties.getMinAge());
        Integer max = checkMinAge(properties.getMaxAge());

        properties.setMinAge(getMin(min, max));
        properties.setMaxAge(getMax(min, max));
    }

    private Integer checkMinAge(Integer age) {
        if (age == null || age <= 0)
            return null;
        if (isLowerThanMinAge(age))
            return MIN_AGE;
        return age;
    }

    void sortPriceRange(SearchAdvertListProperties properties) {
        Integer min = checkPriceRange(properties.getMinRent());
        Integer max = checkPriceRange(properties.getMaxRent());

        properties.minMonthlyRent(getMin(min, max));
        properties.maxMonthlyRent(getMax(min, max));
    }

    private Integer getMin(Integer min, Integer max) {
        return (min == null || max == null) ? min : (min <= max ? min : max);
    }

    private Integer getMax(Integer min, Integer max) {
        return (max == null || min == null) ? max : (max >= min ? max : min);
    }

    private Integer checkPriceRange(Integer price) {
        return price == null || price <= 0 ? null : price;
    }

    void sortLengthOfStay(SearchAdvertListProperties properties) {
        MinTerm minTerm = properties.getMinTerm();
        MaxTerm maxTerm = properties.getMaxTerm();

        if (minTerm == MinTerm.NOT_SET || maxTerm == MaxTerm.NOT_SET)
            return;

        if (minTerm.ordinal() > maxTerm.ordinal()) {
            properties.setMinTerm(MinTerm.values()[maxTerm.ordinal()]);
            properties.setMaxTerm(MaxTerm.values()[minTerm.ordinal()]);
        }

    }

    public String filterName() {
        return filterName;
    }

}
