package com.spareroom.ui.filters.adapter.viewholder;

import android.view.View;

import com.spareroom.R;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams;
import com.spareroom.ui.filters.provider.FilterItem;

import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams;

public class SpaceViewHolder extends BaseFilterViewHolder {
    private final View firstSpace;
    private final View divider;
    private final View secondSpace;

    private final LayoutParams smallLayoutParams;
    private final LayoutParams bigLayoutParams;

    public SpaceViewHolder(View space) {
        super(space);
        firstSpace = space.findViewById(R.id.listSpaceItemSpace);
        divider = space.findViewById(R.id.listSpaceItemDivider);
        secondSpace = space.findViewById(R.id.listSpaceItemSecondSpace);

        smallLayoutParams = new LayoutParams(0, secondSpace.getContext().getResources().getDimensionPixelSize(R.dimen.spaceViewSmallSize));
        bigLayoutParams = new LayoutParams(0, secondSpace.getContext().getResources().getDimensionPixelSize(R.dimen.spaceViewBigSize));
    }

    @Override
    public void bind(FilterItem filterItem) {

        switch (((SpaceViewParams) filterItem.params()).spaceType()) {
            case SMALL:
                firstSpace.setLayoutParams(smallLayoutParams);
                divider.setVisibility(View.GONE);
                secondSpace.setVisibility(View.GONE);
                break;
            case DIVIDER:
                firstSpace.setLayoutParams(smallLayoutParams);
                divider.setVisibility(View.VISIBLE);
                secondSpace.setVisibility(View.VISIBLE);
                break;
            case BIG:
                firstSpace.setLayoutParams(bigLayoutParams);
                divider.setVisibility(View.GONE);
                secondSpace.setVisibility(View.GONE);
                break;
        }

    }

}
