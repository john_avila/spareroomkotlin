package com.spareroom.ui.filters.adapter.viewholder;

import android.view.View;

import com.spareroom.ui.filters.provider.FilterItem;
import com.spareroom.ui.widget.MinMaxView;
import com.spareroom.ui.widget.params.MinMaxEditTextParams;

public class MinMaxViewHolder extends BaseFilterViewHolder {
    private final MinMaxView minMaxView;

    public MinMaxViewHolder(View itemView) {
        super(itemView);
        minMaxView = (MinMaxView) itemView;
    }

    @Override
    public void bind(FilterItem filterItem) {
        final MinMaxEditTextParams minMaxEditTextParams = (MinMaxEditTextParams) filterItem.params();
        minMaxView.params(minMaxEditTextParams);
        minMaxView.observer(new MinMaxView.OnValuesChangeListener() {
            @Override
            public void onMinChangedEvent(Integer value) {
                minMaxEditTextParams.min(value);
            }

            @Override
            public void onMaxChangedEvent(Integer value) {
                minMaxEditTextParams.max(value);
            }
        });
    }

}
