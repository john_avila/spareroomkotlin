package com.spareroom.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.annotation.*
import androidx.constraintlayout.widget.ConstraintLayout
import com.spareroom.R
import com.spareroom.ui.util.hide
import com.spareroom.ui.util.show
import kotlinx.android.synthetic.main.error_view.view.*

class ErrorView : ConstraintLayout {

    var isConnectionError = false
        private set

    private var container: View? = null

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, @AttrRes defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private fun init(context: Context) {
        inflate(context, R.layout.error_view, this)
        setBackgroundResource(R.color.white_six)
    }

    fun showIcon(show: Boolean) {
        if (show) imgError.show() else imgError.hide()
    }

    fun icon(@DrawableRes resId: Int) {
        imgError.setImageResource(resId)
    }

    fun title(title: String) {
        txtErrorTitle.text = title
    }

    fun message(message: String) {
        txtErrorMessage.text = message
    }

    fun message(@StringRes message: Int) {
        message(context.getString(message))
    }

    fun action(onClickListener: View.OnClickListener, action: String) {
        txtErrorButton.text = action
        txtErrorButton.setOnClickListener(onClickListener)
    }

    fun action(onClickListener: View.OnClickListener, @StringRes action: Int) {
        action(onClickListener, context.getString(action))
    }

    fun backgroundResource(@DrawableRes resId: Int) {
        setBackgroundResource(resId)
    }

    fun show(isConnectionError: Boolean) {
        this.isConnectionError = isConnectionError
        container?.show()
        show()
    }

    fun hideAllViews() {
        hide()
        container?.hide()
    }

    fun container(container: View) {
        this.container = container
    }

    fun addExtraContent(task: (view: ConstraintLayout) -> Unit) {
        task(errorViewExtraContent)
    }
}
