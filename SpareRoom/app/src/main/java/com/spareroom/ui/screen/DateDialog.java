package com.spareroom.ui.screen;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import com.spareroom.integration.dependency.Injectable;
import com.spareroom.ui.util.DateUtils;
import com.spareroom.viewmodel.FilterViewModel;

import java.util.Calendar;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

public class DateDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener, Injectable {
    public static final String TAG = DateDialog.class.getName() + "Tag";

    private final static String ARG_POSITION = "arg_position";
    private final static String ARG_PREVIOUS_DATE = "arg_previous_date";

    @Inject
    public DateUtils dateUtils;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private FilterViewModel viewModel;
    private int position;

    public static DateDialog getInstance(int position, String previousDate) {
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        args.putString(ARG_PREVIOUS_DATE, previousDate);

        DateDialog dateDialog = new DateDialog();
        dateDialog.setArguments(args);
        return dateDialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(FilterViewModel.class);
        position = getArguments().getInt(ARG_POSITION);

        String previousDate = getArguments().getString(ARG_PREVIOUS_DATE);
        final Calendar now = Calendar.getInstance();
        final Calendar calendar = previousDate == null ? now : dateUtils.getCalendar(DateUtils.YEAR_MONTH_DAY_FORMAT, previousDate);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        now.add(Calendar.SECOND, -1); // Fix for Android 4 exception when setting the min date doesn't precede the current one
        datePickerDialog.getDatePicker().setMinDate(now.getTimeInMillis());
        return datePickerDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        viewModel.filterAvailabilityDate().saveValues(position, dateUtils.createStringDate(year, month + 1, day));
    }

}
