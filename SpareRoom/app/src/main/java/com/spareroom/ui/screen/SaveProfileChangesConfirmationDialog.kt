package com.spareroom.ui.screen

import android.app.Dialog
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.spareroom.R
import com.spareroom.viewmodel.EditProfileActivityViewModel

class SaveProfileChangesConfirmationDialog : DialogFragment() {

    companion object {
        const val TAG = "SaveProfileChangesConfirmationDialogTag"

        @JvmStatic
        fun getInstance() = SaveProfileChangesConfirmationDialog()
    }

    private val viewModel by lazy { ViewModelProviders.of(activity!!).get(EditProfileActivityViewModel::class.java) }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialogBuilder(activity!!)
        builder.setTitle(R.string.confirm)
        builder.setMessage(R.string.unsaved_changed)
        builder.setPositiveButton(R.string.save) { _, _ -> saveChanges() }
        builder.setNegativeButton(R.string.discard) { _, _ -> discardChanges() }
        return builder.create()
    }

    private fun saveChanges() {
        viewModel.saveProfileChanges.save()
        dismiss()
    }

    private fun discardChanges() {
        viewModel.discardProfileChanges.discard()
        dismiss()
    }

}
