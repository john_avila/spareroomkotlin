package com.spareroom.ui.screen.legacy;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.ui.util.StringUtils;

public class PlaceAdOfferedCompanyNameFragment extends PlaceAdFragment {
    private EditText _etCompanyName;
    private Button _bConfirm;
    private TextWatcher tw;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_offered_company_name, container, false);

        setTitle(getString(R.string.place_ad_offered_company_name));

        _etCompanyName = rootView.findViewById(R.id.etCompanyName);
        _bConfirm = rootView.findViewById(R.id.place_ad_bConfirm);

        DraftAdOffered draft = (DraftAdOffered) getDraftAd();

        if ((draft != null) && (draft.get_companyName() != null) && (!draft.get_companyName().equals(""))) {
            _etCompanyName.setText(draft.get_companyName());
            enableConfirm();
        } else {
            _bConfirm.setVisibility(View.INVISIBLE);
        }

        _etCompanyName.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                if (StringUtils.isNullOrEmpty(_etCompanyName.getText(), true)) {
                    disableConfirm();
                    return false;
                }
                confirm();
                finish();
                return false;
            }
            return false;
        });

        _bConfirm.setOnClickListener(v -> {
            confirm();
            finish();
        });

        tw = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("")) {
                    disableConfirm();
                } else if (!StringUtils.isNullOrEmpty(s, true)) {
                    enableConfirm();
                }
            }
        };
        _etCompanyName.addTextChangedListener(tw);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        _etCompanyName.setOnEditorActionListener(null);
        _etCompanyName.removeTextChangedListener(tw);
        _bConfirm.setOnClickListener(null);
    }

    private void confirm() {
        ((DraftAdOffered) getDraftAd()).set_companyName(_etCompanyName.getText().toString());
        // the following code hides the keyboard
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void disableConfirm() {
        _bConfirm.setVisibility(View.INVISIBLE);
        Toast.makeText(getActivity(), "Agents need to provide a company name", Toast.LENGTH_LONG).show();
    }

    private void enableConfirm() {
        _bConfirm.setVisibility(View.VISIBLE);
    }
}
