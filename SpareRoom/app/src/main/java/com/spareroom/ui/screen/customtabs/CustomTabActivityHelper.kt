package com.spareroom.ui.screen.customtabs

import android.app.Activity
import android.net.Uri
import androidx.browser.customtabs.*
import com.spareroom.integration.analytics.AnalyticsTrackerComposite

class CustomTabActivityHelper : ServiceConnectionCallback {

    private var customTabsSession: CustomTabsSession? = null
    private var client: CustomTabsClient? = null
    private var connection: CustomTabsServiceConnection? = null

    companion object {
        fun openCustomTab(activity: Activity, customTabsIntent: CustomTabsIntent, uri: Uri, fallback: CustomTabFallback) {
            val packageName = CustomTabsHelper.getPackageNameToUse(activity)

            if (packageName.isBlank()) {
                launchFallback(activity, uri, fallback)
            } else {
                launchCustomTab(activity, customTabsIntent, uri, fallback, packageName)
            }
        }

        private fun launchCustomTab(activity: Activity, customTabsIntent: CustomTabsIntent, uri: Uri, fallback: CustomTabFallback, packageName: String) {
            try {
                customTabsIntent.intent.`package` = packageName
                CustomTabsHelper.addKeepAliveExtra(activity, customTabsIntent.intent)
                customTabsIntent.launchUrl(activity, uri)

                AnalyticsTrackerComposite.getInstance().trackOpenedCustomTab(true)
            } catch (exception: Exception) {
                launchFallback(activity, uri, fallback)
            }
        }

        private fun launchFallback(activity: Activity, uri: Uri, fallback: CustomTabFallback) {
            AnalyticsTrackerComposite.getInstance().trackOpenedCustomTab(false)
            fallback.openUri(activity, uri)
        }
    }

    fun bindCustomTabsService(activity: Activity) {
        if (client != null)
            return

        val packageName = CustomTabsHelper.getPackageNameToUse(activity)
        if (packageName.isBlank())
            return

        connection = ServiceConnection(this)
        CustomTabsClient.bindCustomTabsService(activity, packageName, connection)
    }

    fun unbindCustomTabsService(activity: Activity) {
        if (connection == null)
            return

        activity.unbindService(connection)
        client = null
        customTabsSession = null
        connection = null
    }

    override fun onServiceConnected(client: CustomTabsClient) {
        this.client = client
        this.client?.warmup(0)
    }

    override fun onServiceDisconnected() {
        client = null
        customTabsSession = null
    }

}
