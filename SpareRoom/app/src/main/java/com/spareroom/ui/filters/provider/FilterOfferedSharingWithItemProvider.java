package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListProperties.GenderFilter;
import com.spareroom.model.business.SearchAdvertListProperties.ShareType;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered.*;
import com.spareroom.ui.filters.adapter.viewparams.AbstractSummaryViewParams.SummaryAction;
import com.spareroom.ui.filters.adapter.viewparams.*;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.widget.params.CompoundButtonParams;
import com.spareroom.ui.widget.params.MinMaxEditTextParams;

import java.util.*;

import androidx.annotation.NonNull;

class FilterOfferedSharingWithItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.sharingWith;
    private final static String KEY_GENDER_SUITABLE_GROUP = "key_gender_group";
    private final static String KEY_JOB_GROUP = "key_job_group";
    private final static String KEY_LANDLORD_GROUP = "key_landlord_group";

    private final static String HOUSEHOLD_COMPOSITION_TITLE = "household_compositionTitle";
    private final static String EMPLOYMENT_STATUS_TITLE = "employment_statusTitle";
    private final static String NUMBER_OF_FLATMATES_TITLE = "number_of_flatmatesTitle";
    private final static String MISCELLANEOUS_TITLE = "miscellaneousTitle";
    final static String MIXED = "mixed";
    final static String FEMALES = "females";
    final static String MALES = "males";
    final static String GENDER_NO_PREFERENCES = "gender_no_preferences";
    final static String AGE_RANGE = "age_range";
    final static String STUDENTS = "students";
    final static String PROFESSIONALS = "professionals";
    final static String SHARED_TYPE_NO_PREFERENCE = "shared_type_no_preference";
    final static String WITH_LANDLORD = "with_landlord";
    final static String NO_LANDLORD = "no_landlord";
    final static String LANDLORD_NO_PREFERENCE = "landlord_no_preference";

    private final List<GroupedCompoundButtonParams> genderSuitableGroup = new ArrayList<>();
    private final List<GroupedCompoundButtonParams> jobGroup = new ArrayList<>();
    private final List<GroupedCompoundButtonParams> landlordGroup = new ArrayList<>();
    private final Map<String, List<GroupedCompoundButtonParams>> groupList = new HashMap<>();

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesOffered previousProperties = (SearchAdvertListPropertiesOffered) properties;
        List<FilterItem> list = new ArrayList<>();

        genderSuitableGroup.clear();
        jobGroup.clear();
        landlordGroup.clear();
        groupList.clear();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        MinMaxEditTextParams ageRange = new MinMaxEditTextParams(AGE_RANGE);
        ageRange.iconId(R.drawable.ic_age);
        ageRange.title(application.getString(R.string.ageRange));
        ageRange.minHint(application.getString(R.string.min_age));
        ageRange.minTitle(application.getString(R.string.minimum));
        ageRange.maxHint(application.getString(R.string.sixty_five_plus));
        ageRange.maxTitle(application.getString(R.string.maximum));
        ageRange.minMaxCharacters(2);
        ageRange.maxMaxCharacters(2);
        ageRange.min(previousProperties.getMinAge());
        ageRange.max(previousProperties.getMaxAge());
        list.add(new FilterItem<>(ageRange));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        TitleViewParams houseHoldTitleViewParams = new TitleViewParams(HOUSEHOLD_COMPOSITION_TITLE);
        houseHoldTitleViewParams.title(application.getString(R.string.householdComposition));
        list.add(new FilterItem<>(houseHoldTitleViewParams));

        GroupedCompoundButtonParams females = new GroupedCompoundButtonParams(FEMALES);
        females.size(CompoundButtonParams.Size.MEDIUM);
        females.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        females.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        females.enableHighlightBar(true);
        females.text(application.getString(R.string.allFemaleHousehold));
        females.textColorResId(R.color.black);
        females.iconResId(R.drawable.ic_female_user);
        females.value(GenderFilter.FEMALES.toString());
        females.groupName(KEY_GENDER_SUITABLE_GROUP);
        genderSuitableGroup.add(females);
        list.add(new FilterItem<>(females));

        GroupedCompoundButtonParams males = new GroupedCompoundButtonParams(MALES);
        males.size(CompoundButtonParams.Size.MEDIUM);
        males.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        males.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        males.enableHighlightBar(true);
        males.text(application.getString(R.string.allMaleHousehold));
        males.textColorResId(R.color.black);
        males.iconResId(R.drawable.ic_male_user);
        males.value(GenderFilter.MALES.toString());
        males.groupName(KEY_GENDER_SUITABLE_GROUP);
        genderSuitableGroup.add(males);
        list.add(new FilterItem<>(males));

        GroupedCompoundButtonParams mixed = new GroupedCompoundButtonParams(MIXED);
        mixed.size(CompoundButtonParams.Size.MEDIUM);
        mixed.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        mixed.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        mixed.enableHighlightBar(true);
        mixed.text(application.getString(R.string.mixed));
        mixed.textColorResId(R.color.black);
        mixed.iconResId(R.drawable.ic_couple);
        mixed.value(GenderFilter.MIXED.toString());
        mixed.groupName(KEY_GENDER_SUITABLE_GROUP);
        genderSuitableGroup.add(mixed);
        list.add(new FilterItem<>(mixed));

        GroupedCompoundButtonParams noPreferences = new GroupedCompoundButtonParams(GENDER_NO_PREFERENCES);
        noPreferences.size(CompoundButtonParams.Size.MEDIUM);
        noPreferences.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        noPreferences.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.GRAY);
        noPreferences.enableHighlightBar(false);
        noPreferences.text(application.getString(R.string.noPreference));
        noPreferences.textColorResId(R.color.black);
        noPreferences.value(GenderFilter.NOT_SET.toString());
        noPreferences.groupName(KEY_GENDER_SUITABLE_GROUP);
        noPreferences.defaultItem(true);
        genderSuitableGroup.add(noPreferences);
        list.add(new FilterItem<>(noPreferences));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        TitleViewParams employmentTitleViewParams = new TitleViewParams(EMPLOYMENT_STATUS_TITLE);
        employmentTitleViewParams.title(application.getString(R.string.employmentStatus));
        list.add(new FilterItem<>(employmentTitleViewParams));

        GroupedCompoundButtonParams professionals = new GroupedCompoundButtonParams(PROFESSIONALS);
        professionals.size(CompoundButtonParams.Size.MEDIUM);
        professionals.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        professionals.enableHighlightBar(true);
        professionals.text(application.getString(R.string.professionals));
        professionals.textColorResId(R.color.black);
        professionals.iconResId(R.drawable.ic_professional);
        professionals.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        professionals.value(ShareType.PROFESSIONALS.toString());
        professionals.groupName(KEY_JOB_GROUP);
        jobGroup.add(professionals);
        list.add(new FilterItem<>(professionals));

        GroupedCompoundButtonParams students = new GroupedCompoundButtonParams(STUDENTS);
        students.size(CompoundButtonParams.Size.MEDIUM);
        students.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        students.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        students.enableHighlightBar(true);
        students.text(application.getString(R.string.students));
        students.textColorResId(R.color.black);
        students.iconResId(R.drawable.ic_student);
        students.value(ShareType.STUDENTS.toString());
        students.groupName(KEY_JOB_GROUP);
        jobGroup.add(students);
        list.add(new FilterItem<>(students));

        GroupedCompoundButtonParams sharedTypeNoPreference = new GroupedCompoundButtonParams(SHARED_TYPE_NO_PREFERENCE);
        sharedTypeNoPreference.size(CompoundButtonParams.Size.MEDIUM);
        sharedTypeNoPreference.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        sharedTypeNoPreference.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.GRAY);
        sharedTypeNoPreference.enableHighlightBar(false);
        sharedTypeNoPreference.text(application.getString(R.string.noPreference));
        sharedTypeNoPreference.textColorResId(R.color.black);
        sharedTypeNoPreference.value(ShareType.NOT_SET.toString());
        sharedTypeNoPreference.groupName(KEY_JOB_GROUP);
        sharedTypeNoPreference.defaultItem(true);
        jobGroup.add(sharedTypeNoPreference);
        list.add(new FilterItem<>(sharedTypeNoPreference));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        TitleViewParams flatmatesTitleViewParams = new TitleViewParams(NUMBER_OF_FLATMATES_TITLE);
        flatmatesTitleViewParams.title(application.getString(R.string.numberOfFlatmates));
        list.add(new FilterItem<>(flatmatesTitleViewParams));

        SummaryViewMinFlatmatesParams minFlatmates = new SummaryViewMinFlatmatesParams(NUMBER_OF_FLATMATES_MIN);
        minFlatmates.iconDrawableId(R.drawable.ic_group);
        minFlatmates.titleStringId(R.string.minimumFlatmates);
        minFlatmates.defaultSubtitleString(application.getString(R.string.noPreference));
        minFlatmates.summaryAction(SummaryAction.SHOW_SCROLL_DIALOG);
        minFlatmates.subtitleString(subtitleUtil.getFlatmatesText(application, previousProperties.getMinFlatmates().getValue()));
        minFlatmates.minFlatmates(previousProperties.getMinFlatmates());
        list.add(new FilterItem<>(minFlatmates));

        SummaryViewMaxFlatmatesParams maxFlatmates = new SummaryViewMaxFlatmatesParams(NUMBER_OF_FLATMATES_MAX);
        maxFlatmates.iconDrawableId(R.drawable.ic_group);
        maxFlatmates.titleStringId(R.string.maximumFlatmates);
        maxFlatmates.defaultSubtitleString(application.getString(R.string.noPreference));
        maxFlatmates.summaryAction(SummaryAction.SHOW_SCROLL_DIALOG);
        maxFlatmates.subtitleString(subtitleUtil.getFlatmatesText(application, previousProperties.maxFlatmates().getValue()));
        maxFlatmates.maxFlatmates(previousProperties.maxFlatmates());
        list.add(new FilterItem<>(maxFlatmates));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.DIVIDER)));

        TitleViewParams miscellaneousTitleViewParams = new TitleViewParams(MISCELLANEOUS_TITLE);
        miscellaneousTitleViewParams.title(application.getString(R.string.miscellaneous));
        list.add(new FilterItem<>(miscellaneousTitleViewParams));

        GroupedCompoundButtonParams withLandlord = new GroupedCompoundButtonParams(WITH_LANDLORD);
        withLandlord.size(CompoundButtonParams.Size.MEDIUM);
        withLandlord.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        withLandlord.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        withLandlord.enableHighlightBar(true);
        withLandlord.text(application.getString(R.string.withLandlord));
        withLandlord.textColorResId(R.color.black);
        withLandlord.iconResId(R.drawable.ic_landlord);
        withLandlord.value(Landlord.LIVE_IN.toString());
        withLandlord.groupName(KEY_LANDLORD_GROUP);
        landlordGroup.add(withLandlord);
        list.add(new FilterItem<>(withLandlord));

        GroupedCompoundButtonParams noLandlord = new GroupedCompoundButtonParams(NO_LANDLORD);
        noLandlord.size(CompoundButtonParams.Size.MEDIUM);
        noLandlord.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        noLandlord.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        noLandlord.enableHighlightBar(true);
        noLandlord.text(application.getString(R.string.notWithLandlord));
        noLandlord.textColorResId(R.color.black);
        noLandlord.iconResId(R.drawable.ic_generic_user);
        noLandlord.value(Landlord.LIVE_OUT.toString());
        noLandlord.groupName(KEY_LANDLORD_GROUP);
        landlordGroup.add(noLandlord);
        list.add(new FilterItem<>(noLandlord));

        GroupedCompoundButtonParams landlordNoPreference = new GroupedCompoundButtonParams(LANDLORD_NO_PREFERENCE);
        landlordNoPreference.size(CompoundButtonParams.Size.MEDIUM);
        landlordNoPreference.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        landlordNoPreference.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.GRAY);
        landlordNoPreference.enableHighlightBar(false);
        landlordNoPreference.text(application.getString(R.string.noPreference));
        landlordNoPreference.textColorResId(R.color.black);
        landlordNoPreference.value(Landlord.NOT_SET.toString());
        landlordNoPreference.defaultItem(true);
        landlordNoPreference.groupName(KEY_LANDLORD_GROUP);
        landlordGroup.add(landlordNoPreference);
        list.add(new FilterItem<>(landlordNoPreference));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        groupList.put(KEY_JOB_GROUP, jobGroup);
        groupList.put(KEY_GENDER_SUITABLE_GROUP, genderSuitableGroup);
        groupList.put(KEY_LANDLORD_GROUP, landlordGroup);
        setGroupPreviousSearch(previousProperties);

        this.list = list;
    }

    private void setGroupPreviousSearch(SearchAdvertListPropertiesOffered properties) {
        Set<String> groupListKeySet = groupList.keySet();

        for (String key : groupListKeySet) {
            List<GroupedCompoundButtonParams> group = groupList.get(key);

            for (GroupedCompoundButtonParams params : group) {
                switch (key) {
                    case KEY_GENDER_SUITABLE_GROUP:
                        params.selected((properties.getGenderFilter() != GenderFilter.NOT_SET) && params.value().equals(properties.getGenderFilter().toString()));
                        break;
                    case KEY_JOB_GROUP:
                        params.selected((properties.getShareType() != ShareType.NOT_SET) && params.value().equals(properties.getShareType().toString()));
                        break;
                    case KEY_LANDLORD_GROUP:
                        params.selected((properties.getLandlord() != Landlord.NOT_SET) && params.value().equals(properties.getLandlord().toString()));
                        break;
                }
            }
            selectedItem(group);
        }

    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return groupList;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesOffered propertiesOffered = (SearchAdvertListPropertiesOffered) propertiesToUpdate;
        for (FilterItem item : list) {
            switch (item.tag()) {
                case AGE_RANGE:
                    propertiesOffered.setMinAge(((MinMaxEditTextParams) item.params()).min());
                    propertiesOffered.setMaxAge(((MinMaxEditTextParams) item.params()).max());
                    break;
                case NUMBER_OF_FLATMATES_MIN:
                    propertiesOffered.setMinFlatmates(((SummaryViewMinFlatmatesParams) item.params()).minFlatmates());
                    break;
                case NUMBER_OF_FLATMATES_MAX:
                    propertiesOffered.maxFlatmates(((SummaryViewMaxFlatmatesParams) item.params()).maxFlatmates());
                    break;
            }
        }

        propertiesOffered.setGenderFilter(GenderFilter.valueOf(selectedValue(groupList.get(KEY_GENDER_SUITABLE_GROUP))));
        propertiesOffered.setShareType(ShareType.valueOf(selectedValue(groupList.get(KEY_JOB_GROUP))));
        propertiesOffered.setLandlord(Landlord.valueOf(selectedValue(groupList.get(KEY_LANDLORD_GROUP))));

        if (sortProperties) {
            sortAgeRange(propertiesOffered);
            sortFlatmatesNumber(propertiesOffered);
        }
    }

    private void sortFlatmatesNumber(SearchAdvertListPropertiesOffered propertiesOffered) {
        MinFlatmates minFlatmates = propertiesOffered.getMinFlatmates();
        MaxFlatmates maxFlatmates = propertiesOffered.maxFlatmates();

        if (minFlatmates != MinFlatmates.NOT_SET && maxFlatmates != MaxFlatmates.NOT_SET) {
            if (minFlatmates.ordinal() > maxFlatmates.ordinal()) {
                propertiesOffered.setMinFlatmates(MinFlatmates.values()[maxFlatmates.ordinal()]);
                propertiesOffered.maxFlatmates(MaxFlatmates.values()[minFlatmates.ordinal()]);
            }
        }
    }

}
