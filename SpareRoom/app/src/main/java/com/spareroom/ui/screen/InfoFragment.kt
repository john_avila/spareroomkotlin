package com.spareroom.ui.screen

import android.app.Dialog
import android.os.Bundle
import com.spareroom.R

private const val MESSAGE = "message"

class InfoFragment : DialogFragment() {

    companion object {
        const val TAG = "InfoFragmentTag"

        @JvmStatic
        fun getInstance(message: String): InfoFragment {
            val infoFragment = InfoFragment()
            infoFragment.arguments = Bundle().also { it.putString(MESSAGE, message) }
            return infoFragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialogBuilder(activity!!)
            .setTitle(R.string.info)
            .setMessage(arguments?.getString(MESSAGE))
            .setPositiveButton(R.string.ok) { _, _ -> dismiss() }
            .create()
    }
}