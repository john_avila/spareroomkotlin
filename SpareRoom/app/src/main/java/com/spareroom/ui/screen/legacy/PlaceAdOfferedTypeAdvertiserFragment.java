package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.ui.widget.PlaceAdButton;

public class PlaceAdOfferedTypeAdvertiserFragment extends PlaceAdFragment {
    private PlaceAdButton _previouslySelected;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_offered_type_advertiser, container, false);

        setTitle(getString(R.string.place_ad_offered_type_advertiser_title));

        final PlaceAdButton bLIL = rootView.findViewById(R.id.place_ad_offered_type_advertiser_bLIL);
        final PlaceAdButton bLOL = rootView.findViewById(R.id.place_ad_offered_type_advertiser_bLOL);
        final PlaceAdButton bCurrentFlatmate = rootView.findViewById(R.id.place_ad_offered_type_advertiser_bCurrentFlatmate);
        final PlaceAdButton bAgent = rootView.findViewById(R.id.place_ad_offered_type_advertiser_bAgent);
        final PlaceAdButton bFormerFlatmate = rootView.findViewById(R.id.place_ad_offered_type_advertiser_bFormerFlatmate);

        DraftAdOffered.AdvertiserType advertiserType = ((DraftAdOffered) getDraftAd()).get_advertiserType();

        if (advertiserType != null)
            switch (advertiserType) {
                case AGENT:
                    _previouslySelected = bAgent;
                    bAgent.setPreviouslySelected();
                    break;
                case CURRENT_FLATMATE:
                    _previouslySelected = bCurrentFlatmate;
                    bCurrentFlatmate.setPreviouslySelected();
                    break;
                case FORMER_FLATMATE:
                    _previouslySelected = bFormerFlatmate;
                    bFormerFlatmate.setPreviouslySelected();
                    break;
                case LIVE_IN_LANDLORD:
                    _previouslySelected = bLIL;
                    bLIL.setPreviouslySelected();
                    break;
                case LIVE_OUT_LANDLORD:
                    _previouslySelected = bLOL;
                    bLOL.setPreviouslySelected();
                    break;
                default:
            }

        bLIL.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bLIL.setPreviouslySelected();
            DraftAdOffered draft = (DraftAdOffered) getDraftAd();
            draft.set_advertiserType(DraftAdOffered.AdvertiserType.LIVE_IN_LANDLORD);
            PlaceAdOfferedTypeAdvertiserFragment.this.finish();
        });

        bLOL.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bLOL.setPreviouslySelected();
            DraftAdOffered draft = (DraftAdOffered) getDraftAd();
            draft.set_advertiserType(DraftAdOffered.AdvertiserType.LIVE_OUT_LANDLORD);
            PlaceAdOfferedTypeAdvertiserFragment.this.finish();
        });

        bCurrentFlatmate.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bCurrentFlatmate.setPreviouslySelected();
            DraftAdOffered draft = (DraftAdOffered) getDraftAd();
            draft.set_advertiserType(DraftAdOffered.AdvertiserType.CURRENT_FLATMATE);
            PlaceAdOfferedTypeAdvertiserFragment.this.finish();
        });

        bAgent.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bAgent.setPreviouslySelected();
            DraftAdOffered draft = (DraftAdOffered) getDraftAd();
            draft.set_advertiserType(DraftAdOffered.AdvertiserType.AGENT);
            PlaceAdOfferedTypeAdvertiserFragment.this.finish();
        });

        bFormerFlatmate.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bFormerFlatmate.setPreviouslySelected();
            DraftAdOffered draft = (DraftAdOffered) getDraftAd();
            draft.set_advertiserType(DraftAdOffered.AdvertiserType.FORMER_FLATMATE);
            PlaceAdOfferedTypeAdvertiserFragment.this.finish();
        });

        return rootView;
    }
}
