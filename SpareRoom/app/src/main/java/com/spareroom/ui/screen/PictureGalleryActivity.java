package com.spareroom.ui.screen;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.MenuItem;

import com.spareroom.R;
import com.spareroom.model.business.Picture;
import com.spareroom.model.business.PicturesList;
import com.spareroom.ui.adapter.FullScreenPictureAdapter;
import com.spareroom.ui.util.UiUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.*;

public class PictureGalleryActivity extends InjectableActivity {

    private static final String PICTURES_KEY = "PICTURES_KEY";
    private static final String POSITION_KEY = "POSITION_KEY";
    private static final int DEFAULT_POSITION = 0;

    public static void start(@NonNull Context context, @NonNull List<Picture> pictures, int selectedPosition) {
        Intent intent = new Intent(context, PictureGalleryActivity.class);
        intent.putExtra(PICTURES_KEY, new PicturesList(pictures));
        intent.putExtra(POSITION_KEY, selectedPosition);
        context.startActivity(intent);
    }

    public static void start(@NonNull Context context, @NonNull String pictureUrl) {
        List<Picture> pictures = new ArrayList<>();
        pictures.add(new Picture("", pictureUrl));

        start(context, pictures, DEFAULT_POSITION);
    }

    @Inject
    FullScreenPictureAdapter pictureAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_picture_gallery);

        setToolbar("", true, R.drawable.ic_close);

        pictureAdapter.setUp(this, ((PicturesList) getIntent().getSerializableExtra(PICTURES_KEY)).getPictures());
        setUpRecyclerView(getIntent().getIntExtra(POSITION_KEY, DEFAULT_POSITION));

        UiUtils.setImmersiveMode(getWindow().getDecorView());
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus)
            UiUtils.setImmersiveMode(getWindow().getDecorView());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeScreen();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpRecyclerView(int position) {
        RecyclerView recyclerViewGallery = findViewById(R.id.fullScreenPictureGalleryRecyclerView);
        recyclerViewGallery.setHasFixedSize(true);
        recyclerViewGallery.setItemViewCacheSize(0);
        recyclerViewGallery.setDrawingCacheEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false) {
            @Override
            protected int getExtraLayoutSpace(RecyclerView.State state) {
                return 0;
            }
        };
        linearLayoutManager.setInitialPrefetchItemCount(0);
        recyclerViewGallery.setLayoutManager(linearLayoutManager);

        PagerSnapHelper pagerSnapHelper = new PagerSnapHelper();
        pagerSnapHelper.attachToRecyclerView(recyclerViewGallery);

        recyclerViewGallery.setAdapter(pictureAdapter);
        recyclerViewGallery.scrollToPosition(position);
    }

    private void closeScreen() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        finish();
    }

    @Override
    public void onBackPressed() {
        closeScreen();
    }
}
