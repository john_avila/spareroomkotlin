package com.spareroom.ui.screen.legacy;

import android.content.Context;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.model.business.DraftAdWanted.AdvertiserType;
import com.spareroom.model.business.Price;
import com.spareroom.ui.widget.util.exclusivitymanager.*;

import androidx.core.content.ContextCompat;

public class PlaceAdWantedBudgetFragment extends PlaceAdFragment {
    private Button _bPerWeek;
    private Button _bPerMonth;
    private Button _bConfirm;
    private EditText _etPrice;
    private Price _price;

    private ButtonViewActivationCommand _bPerWeekVac;
    private ButtonViewActivationCommand _bPerMonthVac;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_wanted_budget, container, false);
        _bConfirm = rootView.findViewById(R.id.place_ad_bConfirm);
        _bPerWeek = rootView.findViewById(R.id.place_ad_wanted_budget_bPerWeek);
        _bPerMonth = rootView.findViewById(R.id.place_ad_wanted_budget_bPerMonth);
        _etPrice = rootView.findViewById(R.id.place_ad_wanted_budget_etPrice);

        _etPrice.setOnClickListener(v -> {
            if (((EditText) v).getText().toString().equals("0")) {
                ((EditText) v).setText("");
            }
        });

        _etPrice.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                if (getActivity().getCurrentFocus() != null)
                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                return true;
            }
            return false;
        });

        final DraftAdWanted draft = (DraftAdWanted) getDraftAd();

        // In case there is an activity state change and we lost the draft
        if (draft == null) {
            getActivity().finish();

            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }

        if ((draft.get_advertiserType() == AdvertiserType.MALE_FEMALE)
                || (draft.get_advertiserType() == AdvertiserType.TWO_FEMALES)
                || (draft.get_advertiserType() == AdvertiserType.TWO_MALES)) {
            setTitle(getString(R.string.place_ad_wanted_budget_combined_title));
        } else {
            setTitle(getString(R.string.place_ad_wanted_budget_title));
        }

        _price = draft.get_budget();

        if (_price == null) {
            _price = new Price();
            _price.set_amount("0");
            _price.set_periodicity(Price.PERIODICITY_MONTH);
            _price.set_areBillsIncluded(Price.BILLS_INCLUDED_NO);
        }

        _etPrice.setText(_price.get_amount());

        ExclusivityManager billingCycleManager = new ExclusivityManager();

        _bPerWeekVac = new ButtonViewActivationCommand(_bPerWeek);
        _bPerMonthVac = new ButtonViewActivationCommand(_bPerMonth);

        billingCycleManager.add(_bPerWeekVac);
        billingCycleManager.add(_bPerMonthVac);

        _bPerWeek.setOnClickListener(new ButtonOnClickListener(billingCycleManager, _bPerWeek));
        _bPerMonth.setOnClickListener(new ButtonOnClickListener(billingCycleManager, _bPerMonth));

        try {
            if (_price.get_periodicity() != null) {
                if (_price.get_periodicity().equals(Price.PERIODICITY_MONTH)) {
                    billingCycleManager.activate(_bPerMonth);
                } else {
                    billingCycleManager.activate(_bPerWeek);
                }
            }
        } catch (Exception e) {
            //another empty catch
        }
        _bConfirm.setOnClickListener(v -> {
            if (_etPrice.getText().toString().length() == 0)
                _price.set_amount("0");
            else
                _price.set_amount(Integer.toString(Integer.parseInt(_etPrice.getText().toString())));
            if (_bPerMonthVac.isActivated()) {
                _price.set_periodicity(Price.PERIODICITY_MONTH);
            } else if (_bPerWeekVac.isActivated()) {
                _price.set_periodicity(Price.PERIODICITY_WEEK);
            }
            Integer intAmount = 0;
            try {
                intAmount = Integer.parseInt(_etPrice.getText().toString());
            } catch (NumberFormatException e) {
                //another empty catch
            }
            if (intAmount == 0) {
                Toast.makeText(getActivity(), "Please specify your budget", Toast.LENGTH_LONG).show();
                return;
            }
            draft.set_budget(_price);
            finish();
        });

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        _etPrice.setOnClickListener(null);
        _etPrice.setOnEditorActionListener(null);
        _bConfirm.setOnClickListener(null);
        _bPerMonth.setOnClickListener(null);
        _bPerWeek.setOnClickListener(null);
    }

    private class ButtonViewActivationCommand extends ViewActivationCommand {
        private final Button _bView;
        private boolean _isActivated = false;

        ButtonViewActivationCommand(Button button) {
            super(button);
            _bView = button;
        }

        @Override
        public void activate() {
            super.activate();
            _bView.setBackgroundResource(R.drawable.place_ad_blue_box);
            _bView.setTextColor(ContextCompat.getColor(getActivity(), R.color.cyan));

            if (_bView == _bPerMonth) {
                _price.set_areBillsIncluded(Price.PERIODICITY_MONTH);
            } else if (_bView == _bPerWeek) {
                _price.set_areBillsIncluded(Price.PERIODICITY_WEEK);
            }
            _isActivated = true;
        }

        @Override
        public void deactivate() {
            super.deactivate();
            _bView.setBackgroundResource(R.drawable.place_ad_grey_box);
            _bView.setTextColor(ContextCompat.getColor(getActivity(), R.color.grey_2));
            _isActivated = false;
        }

        boolean isActivated() {
            return _isActivated;
        }
    }

    private class ButtonOnClickListener implements OnClickListener {
        private final ExclusivityManager _exclusivityManger;
        private final View _view;

        ButtonOnClickListener(ExclusivityManager em, View v) {
            _exclusivityManger = em;
            _view = v;
        }

        @Override
        public void onClick(View v) {
            try {
                _exclusivityManger.activate(_view);
            } catch (ExclusiveViewNotFoundException e) {
                //another empty catch
            }
        }
    }
}
