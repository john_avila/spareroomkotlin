package com.spareroom.ui.adapter.legacy;

import android.content.Context;
import android.content.Intent;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.integration.dependency.component.ComponentRepository;
import com.spareroom.integration.imageloader.ImageLoader;
import com.spareroom.model.business.*;
import com.spareroom.ui.screen.Fragment;
import com.spareroom.ui.screen.legacy.AdActivity;
import com.spareroom.ui.util.StringUtils;

import java.util.List;

import javax.inject.Inject;

public class SavedAdOfferedListAdapter extends BaseAdapter {
    private final AdBoard _results;
    private final Fragment _fragment;
    private final int imageSize;

    @Inject
    ImageLoader _imageLoader;

    public SavedAdOfferedListAdapter(Fragment parentFragment, AdBoard objects) {
        ComponentRepository.get().getAppComponent().inject(this);
        _fragment = parentFragment;
        _results = objects;
        imageSize = parentFragment.getResources().getDimensionPixelSize(R.dimen.conversationHeaderAvatarSize);
    }

    @Override
    public int getCount() {
        return _results.get_board().size();
    }

    @Override
    public Object getItem(int position) {
        if (position < _results.get_board().size())
            return _results.get_board().get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if ((_results != null) && (_results.get_board() != null))
            if (position < _results.get_board().size())
                return _results.get_board().get(position).getKind();
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    private View getAd(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        AdOffered ad = (AdOffered) _results.get_board().get(position);
        ImageView ivPicture;
        TextView tvLocation;
        TextView tvRoom;
        TextView tvPrice;
        TextView tvDescription;
        ImageView ivTag;

        int resKind;

        if (view == null || view.findViewById(R.id.activity_results_result_ll) == null) {
            final LayoutInflater vi = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (ad.isBold()) {
                view = vi.inflate(R.layout.results_list_item_bold, parent, false);
            } else if (ad.isFeatured()) {
                view = vi.inflate(R.layout.results_list_item_featured, parent, false);
            } else {
                view = vi.inflate(R.layout.results_list_item_normal, parent, false);
            }

            view.findViewById(R.id.activity_results_result_ivSaved).setVisibility(View.VISIBLE);
        }

        if (ad.isBold()) {
            ivPicture = view.findViewById(R.id.activity_results_result_bold_ivPicture);
            tvLocation = view.findViewById(R.id.activity_results_result_bold_tvLocation);
            tvRoom = view.findViewById(R.id.activity_results_result_bold_tvRoom);
            tvPrice = view.findViewById(R.id.activity_results_result_bold_tvPrice);
            tvDescription = view.findViewById(R.id.activity_results_result_bold_tvDescription);
            ivTag = view.findViewById(R.id.activity_results_result_bold_ivTag);

            tvLocation.setText(
                    String.format(
                            AppVersion.flavor().getLocale(),
                            "%s (%s)",
                            ad.getArea(),
                            ad.getPostcode()));
            tvRoom.setText(ad.getAccommodationType());
            tvPrice.setText(ad.getPrice());
            tvDescription.setText(ad.getShortDescription());

            resKind = R.drawable.ad_status_free_to_contact_blue;
        } else if (ad.isFeatured()) {
            ivPicture = view.findViewById(R.id.activity_results_result_featured_ivPicture);
            tvLocation = view.findViewById(R.id.activity_results_result_featured_tvLocation);
            tvRoom = view.findViewById(R.id.activity_results_result_featured_tvRoom);
            tvPrice = view.findViewById(R.id.activity_results_result_featured_tvPrice);
            tvDescription = view.findViewById(R.id.activity_results_result_featured_tvDescription);
            ivTag = view.findViewById(R.id.activity_results_result_featured_ivTag);

            tvLocation.setText(
                    String.format(
                            AppVersion.flavor().getLocale(),
                            "%s (%s)",
                            ad.getArea(),
                            ad.getPostcode()));
            tvRoom.setText(ad.getAccommodationType());
            tvPrice.setText(ad.getPrice());
            tvDescription.setText(ad.getShortDescription());

            resKind = R.drawable.ad_status_free_to_contact_yellow;

        } else {
            ivPicture = view.findViewById(R.id.activity_results_result_normal_ivPicture);
            tvLocation = view.findViewById(R.id.activity_results_result_normal_tvLocation);
            tvRoom = view.findViewById(R.id.activity_results_result_normal_tvRoom);
            tvPrice = view.findViewById(R.id.activity_results_result_normal_tvPrice);
            tvDescription = view.findViewById(R.id.activity_results_result_normal_tvDescription);
            ivTag = view.findViewById(R.id.activity_results_result_normal_ivTag);

            tvLocation.setText(
                    String.format(
                            AppVersion.flavor().getLocale(),
                            "%s (%s)",
                            ad.getArea(),
                            ad.getPostcode()));
            tvRoom.setText(ad.getAccommodationType());
            tvPrice.setText(ad.getPrice());
            tvDescription.setText(ad.getShortDescription());

            resKind = R.drawable.ad_status_free_to_contact_grey;
        }

        final String largePictureUrl = ad.getLargePictureUrl();
        if (!StringUtils.isNullOrEmpty(largePictureUrl)) {
            _imageLoader.loadImage(largePictureUrl + "?square=" + imageSize, ivPicture);
        } else {
            ivPicture.setImageResource(R.drawable.placeholder_offered);
        }

        if (ad.getContacted())
            ivTag.setImageResource(R.drawable.ad_status_contacted);
        else {
            if (ad.getEarlyBirdRequired()) {
                ivTag.setImageResource(R.drawable.ad_status_early_bird);
            } else {
                ivTag.setImageResource(resKind);
            }
        }

        view.setOnClickListener(new

                AdOnClickListener(ad));

        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position < _results.get_board().size())
            return getAd(position, convertView, parent);
        return null;
    }

    public void add(AdOffered ad) {
        List<AbstractAd> board = _results.get_board();

        for (int i = 0; i < board.size(); i += 10) {
            if (board.get(i).isFeatured() && board.get(i).getId().equals(ad.getId()))
                return;
        }
        board.add(ad);
    }

    private class AdOnClickListener implements OnClickListener {
        private final AdOffered _ad;

        private AdOnClickListener(AdOffered ad) {
            _ad = ad;
        }

        @Override
        public void onClick(View v) {
            Intent adActivity = new Intent();
            adActivity.setClass(_fragment.getActivity(), AdActivity.class);
            adActivity.putExtra("id", _ad.getId());
            adActivity.putExtra("searchType", SearchType.OFFERED);

            (_fragment.getActivity()).startActivityForResult(adActivity, 31);
        }
    }

}