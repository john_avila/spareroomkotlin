package com.spareroom.ui.filters.screen;

import android.os.Bundle;
import android.view.*;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import com.spareroom.R;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.model.business.Coordinates;
import com.spareroom.model.business.SearchAdvertListPropertiesOffered;
import com.spareroom.ui.screen.Fragment;
import com.spareroom.ui.screen.OnMapAndViewReadyListener;
import com.spareroom.ui.screen.OnMapAndViewReadyListener.OnGlobalLayoutAndMapReadyListener;
import com.spareroom.ui.util.*;
import com.spareroom.ui.util.GoogleApiUtil.GoogleApiConnectionResult;
import com.spareroom.ui.widget.ErrorView;
import com.spareroom.viewmodel.FilterViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import static android.view.View.*;

public class FilterMapsFragment extends Fragment implements Injectable {
    private int innerOrange;
    private int strokeOrange;
    private int strokeDashLength;
    private int strokeWidth;
    private final CircleOptions circleOptions = new CircleOptions();
    private Circle circle;

    private static final int SEEK_BAR_MAX_VALUE = 15;
    private static final String MILES_FROM_MAX = "miles_from_max";
    private static final String SEARCH_ADVERT_LIST_PROPERTIES_KEY = "search_advert_list_properties_key";

    private View highlightBar;
    private TextView textViewSubtitle;
    private SeekBar seekBar;
    private GoogleMap googleMap;
    private View mapView;

    private SearchAdvertListPropertiesOffered currentSearchProperties;
    private int milesFromMax;

    private FilterViewModel viewModel;

    private MapViewController mapViewController;

    private OnGlobalLayoutAndMapReadyListener mapReadyListener = new OnGlobalLayoutAndMapReadyListener() {
        @Override
        public void onMapReady(@NonNull GoogleMap googleMap) {
            MapsUtil.disableIndoor(googleMap);
            mapViewController.showMap();
            googleMap(googleMap);
            googleMap.setPadding(strokeWidth, strokeWidth, strokeWidth, strokeWidth);
            drawMap();
        }
    };

    @Inject
    GoogleApiUtil googleApiUtil;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    public static FilterMapsFragment newInstance(SearchAdvertListPropertiesOffered properties) {
        Bundle fragmentArguments = new Bundle();
        fragmentArguments.putSerializable(SEARCH_ADVERT_LIST_PROPERTIES_KEY, properties);

        FilterMapsFragment fragment = new FilterMapsFragment();
        fragment.setArguments(fragmentArguments);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.material_filters_specific_search_radius, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        innerOrange = ContextCompat.getColor(getBaseActivity(), R.color.material_orange_10);
        strokeOrange = ContextCompat.getColor(getBaseActivity(), R.color.orange);
        strokeDashLength = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.mapStrokeDashLength);
        strokeWidth = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.mapStrokeWidth);

        highlightBar = view.findViewById(R.id.seekBarMap_highlightBar);
        textViewSubtitle = view.findViewById(R.id.seekBarMap_subtitle);
        seekBar = view.findViewById(R.id.seekBarMap_seekBar);
        initErrorView(view);

        viewModel = ViewModelProviders.of(getBaseActivity(), viewModelFactory).get(FilterViewModel.class);

        if (savedInstanceState != null) {
            currentSearchProperties =
                    (SearchAdvertListPropertiesOffered) savedInstanceState.getSerializable(SEARCH_ADVERT_LIST_PROPERTIES_KEY);
            milesFromMax = savedInstanceState.getInt(MILES_FROM_MAX);

        } else {
            assert getArguments() != null;
            currentSearchProperties = (SearchAdvertListPropertiesOffered) getArguments().getSerializable(SEARCH_ADVERT_LIST_PROPERTIES_KEY);
            assert currentSearchProperties != null;
            milesFromMax = currentSearchProperties.getMilesFromMax() == null ? 0 : currentSearchProperties.getMilesFromMax();

        }

        initToolBar();
        initButtons(view.findViewById(R.id.filtersSpecific_reset), view.findViewById(R.id.filtersSpecific_apply));

        subtitleText();
        initSeekBar();
        initLabels(view);
        initMap();
        showHighlightBar(milesFromMax > 0);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt(MILES_FROM_MAX, milesFromMax);
        outState.putSerializable(SEARCH_ADVERT_LIST_PROPERTIES_KEY, currentSearchProperties);
        super.onSaveInstanceState(outState);
    }

    private void initErrorView(View parentView) {
        mapViewController = new MapViewController(parentView.findViewById(R.id.seekBarMap_mapError));
    }

    private void initToolBar() {
        setTitle(getString(R.string.searchRadius));
        setHomeAsUpIndicator(R.drawable.ic_back);
    }

    private void initButtons(final TextView reset, TextView apply) {
        reset.setOnClickListener(v -> reset());

        apply.setOnClickListener(v -> {
            currentSearchProperties.setMilesFromMax(milesFromMax == 0 ? null : (milesFromMax > seekBar.getMax() ? seekBar.getMax() : milesFromMax));
            viewModel.searchProperties(currentSearchProperties);
            viewModel.shallPerformMatchesSearch(true);
            viewModel.shallPerformEmptySearch(true);
            getBaseActivity().onBackPressed();
        });

    }

    private void initSeekBar() {
        seekBar.setMax(SEEK_BAR_MAX_VALUE);
        seekBar.setProgress(milesFromMax);
        seekBar.setOnSeekBarChangeListener(new SeekBarChangeListener(this));
    }

    private void initLabels(View view) {
        ViewGroup layout = view.findViewById(R.id.seekBarMap_seekBar_constraintLayout);
        setLabelText(layout);
        distributeLabels((ConstraintLayout) layout);
    }

    private void setLabelText(View layout) {
        ((TextView) layout.findViewById(R.id.label01).findViewById(R.id.widget_seekBarLabel_text)).setText(getString(R.string.none));
        ((TextView) layout.findViewById(R.id.label02).findViewById(R.id.widget_seekBarLabel_text)).setText(getString(R.string.fiveMi));
        ((TextView) layout.findViewById(R.id.label03).findViewById(R.id.widget_seekBarLabel_text)).setText(getString(R.string.tenMi));
        ((TextView) layout.findViewById(R.id.label04).findViewById(R.id.widget_seekBarLabel_text)).setText(getString(R.string.fifteenMi));
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.seekBarMap_map);
        mapView = mapFragment.getView();
        assert mapView != null;
        mapView.setClickable(false);

        if (googleApiUtil.isGooglePlayServicesAvailable() == GoogleApiConnectionResult.SUCCESS) {
            new OnMapAndViewReadyListener(mapFragment, mapReadyListener);

        } else {
            mapViewController.showError();

        }

    }

    public void reset() {
        milesFromMax = 0;
        seekBar.setProgress(0);
        drawMap();
    }

    private void drawMap() {
        if (googleMap == null) {
            AnalyticsTrackerComposite.getInstance().logHandledException("Drawing map in search radius", new Exception("GoogleMap object is null"));
            return;
        }

        if (!UiUtils.isFragmentAlive(this))
            return;

        Coordinates coordinates = currentSearchProperties.getCoordinates();

        LatLngBounds latLngBounds = MapsUtil.changeLatLngBoundsSize(MapsUtil.createLatLngBounds(coordinates), milesFromMax);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 0));
        drawCircle(coordinates, latLngBounds.northeast);

    }

    private void drawCircle(Coordinates coordinates, LatLng side) {
        List<PatternItem> strokePattern = new ArrayList<>();
        strokePattern.add(new Dash(strokeDashLength));
        strokePattern.add(new Gap(strokeDashLength));

        if (circle != null)
            circle.remove();

        circleOptions
                .center(new LatLng(coordinates.getLatitude(), coordinates.getLongitude()))
                .radius(MapsUtil.getDistance(coordinates, side))
                .strokeColor(strokeOrange)
                .strokeWidth(strokeWidth)
                .strokePattern(strokePattern)
                .fillColor(innerOrange);

        circle = googleMap.addCircle(circleOptions);
    }

    private void subtitleText() {
        String where = currentSearchProperties.getWhere();

        if (milesFromMax != 0)
            where = where + " + " + milesFromMax + " " + getResources().getQuantityString(R.plurals.miles, milesFromMax).toLowerCase();

        textViewSubtitle.setText(where);
    }

    private void showHighlightBar(boolean show) {
        highlightBar.setVisibility(show ? VISIBLE : INVISIBLE);
    }

    private void googleMap(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    private class MapViewController {
        private final ErrorView errorView;

        private MapViewController(ErrorView errorView) {
            this.errorView = errorView;

            initIcon();
        }

        private void initIcon() {
            errorView.showIcon(false);

        }

        private void showMap() {
            showMap(true);
            showError(false);
        }

        private void showError() {
            showMap(false);
            showError(true);
        }

        private void showMap(boolean show) {
            mapView.setVisibility(show ? VISIBLE : INVISIBLE);
        }

        private void showError(boolean show) {
            errorView.setVisibility(show ? VISIBLE : GONE);

            if (!show)
                return;

            errorView.action(view -> {
                if (!googleApiUtil.enableServices())
                    errorView.setVisibility(GONE);
            }, getString(googleApiUtil.actionTextResId()));
            errorView.message(getString(googleApiUtil.messageResId()));
        }

    }

    private static class SeekBarChangeListener implements OnSeekBarChangeListener {
        private final FilterMapsFragment mapsFragment;

        private SeekBarChangeListener(FilterMapsFragment mapsFragment) {
            this.mapsFragment = mapsFragment;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            mapsFragment.showHighlightBar(progress != 0);
            mapsFragment.milesFromMax = progress;
            mapsFragment.subtitleText();
            mapsFragment.drawMap();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }

    }

    private void distributeLabels(ConstraintLayout layout) {
        ViewTreeObserver observer = layout.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ConstraintLayoutObserver(layout));
    }

    private class ConstraintLayoutObserver implements ViewTreeObserver.OnGlobalLayoutListener {
        private final ConstraintLayout layout;

        private ConstraintLayoutObserver(ConstraintLayout layout) {
            this.layout = layout;
        }

        @Override
        public void onGlobalLayout() {
            layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);

            ConstraintSet set = new ConstraintSet();
            int margin = seekBar.getPaddingLeft() + ((MarginLayoutParams) seekBar.getLayoutParams()).leftMargin;
            int step = (layout.getMeasuredWidth() - margin * 2) / 3;

            set.clone(layout);
            set.setGuidelineBegin(layout.findViewById(R.id.guideline01).getId(), margin);
            set.setGuidelineBegin(layout.findViewById(R.id.guideline02).getId(), margin + step);
            set.setGuidelineBegin(layout.findViewById(R.id.guideline03).getId(), margin + step * 2);
            set.setGuidelineEnd(layout.findViewById(R.id.guideline04).getId(), margin);
            set.applyTo(layout);
        }

    }

}
