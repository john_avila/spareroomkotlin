package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.widget.Button;
import android.widget.ImageView;

import com.spareroom.R;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.integration.imageloader.ImageLoader;

import javax.inject.Inject;

public class PlaceAdPhotoChosenFragment extends PlaceAdFragment implements Injectable {

    @Inject
    ImageLoader imageLoader;

    private Button _bConfirm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.place_ad_wanted_photo_chosen, container, false);
        setTitle(getString(R.string.place_ad_wanted_photo_chosen_title));

        _bConfirm = rootView.findViewById(R.id.place_ad_wanted_photo_chosen_bConfirm);
        Button _bCancel = rootView.findViewById(R.id.place_ad_wanted_photo_chosen_bCancel);
        ImageView _ivPhoto = rootView.findViewById(R.id.place_ad_wanted_photo_chosen_ivPhoto);

        if (getDraftAd().hasPicture()) {
            imageLoader.loadImage(getDraftAd().getPicturePath(), _ivPhoto);
        }

        _bConfirm.setOnClickListener(v -> finish());

        _bCancel.setOnClickListener(v -> {
            getDraftAd().setPicturePath(null);
            ((PlaceAdAbstractActivity) getActivity()).previousScreen();
        });

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        _bConfirm.setOnClickListener(null);
    }
}
