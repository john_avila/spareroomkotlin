package com.spareroom.ui.screen.legacy;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.lib.core.IObserver;
import com.spareroom.model.business.ConversationInfo;
import com.spareroom.model.business.Session;
import com.spareroom.ui.IDrawerObserver;
import com.spareroom.ui.screen.*;
import com.spareroom.ui.screen.customtabs.CustomTabActivityHelper;
import com.spareroom.ui.util.StringUtils;
import com.spareroom.ui.util.UiUtils;
import com.spareroom.viewmodel.MainActivityViewModel;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import static com.spareroom.platform.push.PushNotificationsMessagingService.KEY_USER_ID_TO;
import static com.spareroom.ui.screen.DrawerFragment.HIGHLIGHT_HOME;
import static com.spareroom.ui.screen.DrawerFragment.HIGHLIGHT_MESSAGES;

public class MainActivity extends InjectableActivity implements IDrawerObserver {
    private MainActivityViewModel viewModel;

    private static final String CURRENT_FRAGMENT = "current_fragment";

    private CustomTabActivityHelper customTabActivityHelper;

    private TextView _tvMessageCounter;
    private TextView _tvUpgradeCounter;

    public final static String REQUESTED_FRAGMENT_TAG = "requested_fragment_tag";

    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private DrawerFragment _drawerFragment;

    private final IObserver _deleteSessionObserver = o -> loadDrawer();

    public static final int REQUEST_CODE_NAVIGATION_DRAWER = 200;

    private String _currentFragment = HomeFragment.TAG;

    @Inject
    SpareroomContext spareroomContext;

    private void loadDrawer() {
        loadDrawer(HIGHLIGHT_HOME);
    }

    private void loadDrawer(String highlightedOption) {
        final String DRAWER_LOGGED_IN_FRAGMENT = "DrawerLoggedInFragment";
        final String DRAWER_LOGGED_OUT_FRAGMENT = "DrawerLoggedOutFragment";
        Session session = SpareroomApplication.getInstance().getSpareroomContext().getSession();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (session == null) {
            _drawerFragment = (DrawerFragment) getSupportFragmentManager().findFragmentByTag(DRAWER_LOGGED_OUT_FRAGMENT);
            if (_drawerFragment == null) {
                _drawerFragment = DrawerLoggedOutFragment.getInstance();
                transaction.replace(R.id.left_drawer, _drawerFragment, DRAWER_LOGGED_OUT_FRAGMENT);
                transaction.commit();
            }
        } else {
            _drawerFragment = (DrawerFragment) getSupportFragmentManager().findFragmentByTag(DRAWER_LOGGED_IN_FRAGMENT);
            if (_drawerFragment == null) {
                _drawerFragment = DrawerLoggedInFragment.getInstance(highlightedOption);
                transaction.replace(R.id.left_drawer, _drawerFragment, DRAWER_LOGGED_IN_FRAGMENT);
                transaction.commit();
            }
        }
    }

    @Override
    public void notifyDrawerChanged(String option) {
        _currentFragment = option;
        supportInvalidateOptionsMenu();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleNotification(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        customTabActivityHelper.bindCustomTabsService(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (drawerToggle != null)
            drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();

        if (AppVersion.isUs() && (SpareroomApplication.getInstance().getSpareroomContext().getSession() == null)) {
            startActivity(new Intent(MainActivity.this, OnboardingActivity.class));
            finish();
            return;
        }

        SpareroomApplication.getInstance().getSpareroomContext().addDeleteSessionObserver(_deleteSessionObserver);

        replaceHomeFragment();
        loadDrawer();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.material_main);
        customTabActivityHelper = new CustomTabActivityHelper();

        drawerLayout = findViewById(R.id.drawer_layout);
        _tvMessageCounter = findViewById(R.id.navigationDrawer_messages).findViewById(R.id.drawer_item_counter);
        _tvUpgradeCounter = findViewById(R.id.navigationDrawer_upgrade).findViewById(R.id.drawer_item_counter);

        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        viewModel.getSyncDrawer().observe(this, syncDrawerEvent -> syncDrawerToggle());

        if (savedInstanceState != null)
            _currentFragment = savedInstanceState.getString(CURRENT_FRAGMENT);

        handleNotification(getIntent());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(CURRENT_FRAGMENT, _currentFragment);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext().removeDeleteSessionObserver(_deleteSessionObserver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        customTabActivityHelper.unbindCustomTabsService(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (drawerToggle != null)
            drawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else if (HomeFragment.TAG.equals(_currentFragment)) {
            super.onBackPressed();
        } else {
            if (UiUtils.isFragmentAlive(_drawerFragment)) {
                _drawerFragment.activateDefaultOption();
            } else {
                AnalyticsTrackerComposite.getInstance().trackFailedToShowHomeFragmentOnBackPressEvent();
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = (Fragment) getSupportFragmentManager().findFragmentById(R.id.left_drawer);
        if (fragment != null)
            fragment.onActivityResult(requestCode, resultCode, data);
    }

    private void handleNotification(Intent intent) {
        String requestedFragment = intent.getStringExtra(REQUESTED_FRAGMENT_TAG);
        boolean expectedTag = ConversationListFragment.TAG.equals(requestedFragment) || ConversationFragment.TAG.equals(requestedFragment);

        if (!expectedTag)
            return;

        if (!isRightUser(intent)) {
            AnalyticsTrackerComposite.getInstance().logHandledException(
                    "Wrong user notification",
                    new Exception(
                            String.format(
                                    "userToId: %s, currentUserId: %s, tag: %s",
                                    intent.getSerializableExtra(KEY_USER_ID_TO),
                                    spareroomContext.getUserId(),
                                    requestedFragment)));
            return;
        }

        intent.removeExtra(REQUESTED_FRAGMENT_TAG);

        boolean conversationListFound = getSupportFragmentManager().findFragmentByTag(ConversationListFragment.TAG) != null;
        if (conversationListFound) {
            viewModel.getConversationListRequests().refreshList();

        } else {
            replaceFragment(ConversationListFragment.getInstance(), ConversationListFragment.TAG);
            loadDrawer(HIGHLIGHT_MESSAGES);
            _drawerFragment.highlightConversationList();
        }

        if (ConversationFragment.TAG.equals(requestedFragment))
            viewModel.getConversationListRequests().openConversation((ConversationInfo) intent.getSerializableExtra(ConversationInfo.TAG));
    }

    private boolean isRightUser(Intent intent) {
        return isUserLoggedIn() && isNotificationForThisUser(intent);
    }

    private boolean isUserLoggedIn() {
        return !StringUtils.isNullOrEmpty(spareroomContext.getUserId());
    }

    private boolean isNotificationForThisUser(Intent intent) {
        return StringUtils.equals(spareroomContext.getUserId(), intent.getStringExtra(KEY_USER_ID_TO));
    }

    private void replaceHomeFragment() {
        if (shouldReplaceHomeFragment())
            replaceFragment(HomeFragment.getInstance(), HomeFragment.TAG);
    }

    private boolean shouldReplaceHomeFragment() {
        if (!HomeFragment.TAG.equalsIgnoreCase(_currentFragment))
            return false;

        HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentByTag(HomeFragment.TAG);
        return homeFragment == null || homeFragment.loggedIn() != SpareroomApplication.getSpareRoomContext().isUserLoggedIn();
    }

    private void replaceFragment(Fragment fragment, String newFragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_frame, fragment, newFragment);
        transaction.commit();

        notifyDrawerChanged(newFragment);
    }

    private void syncDrawerToggle() {
        if (drawerToggle != null)
            drawerLayout.removeDrawerListener(drawerToggle);

        drawerToggle =
                new ActionBarDrawerToggle(this, drawerLayout, findViewById(R.id.toolbar), R.string.openDrawer, R.string.closeDrawer) {

                    public void onDrawerClosed(View view) {
                        supportInvalidateOptionsMenu();
                    }

                    public void onDrawerOpened(View drawerView) {
                        supportInvalidateOptionsMenu();

                        Session session = SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext().getSession();

                        _tvMessageCounter.setVisibility(View.GONE);
                        _tvUpgradeCounter.setVisibility(View.GONE);

                        if (session != null) {
                            int messageCount = session.getMessageCount();
                            if (messageCount > 0) {
                                _tvMessageCounter.setVisibility(View.VISIBLE);
                                _tvMessageCounter.setText(String.format("%s %s", messageCount, getString(R.string.navigationDrawer_messagesUnread)));
                            }

                            if (session.get_upgradeTimeLeft() != null && !session.get_upgradeTimeLeft().isEmpty()) {
                                String daysLeft = session.get_upgradeTimeLeft();

                                if (daysLeft.contains(",")) {
                                    daysLeft = daysLeft.substring(0, daysLeft.indexOf(","));

                                }
                                _tvUpgradeCounter.setVisibility(View.VISIBLE);
                                _tvUpgradeCounter.setText(String.format("%s %s", daysLeft, getString(R.string.navigationDrawer_upgradeDaysLeft)));

                            }

                        } else {
                            SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext().deleteUserSession();
                        }

                    }
                };

        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }
}
