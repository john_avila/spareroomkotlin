package com.spareroom.ui.screen;

import android.content.Intent;
import android.os.Bundle;

import com.spareroom.R;

import androidx.fragment.app.FragmentTransaction;

/**
 * Sing In screen for the Material design
 */
public class NewExistingActivity extends Activity {

    //region CONTROLLER

    public static final int REQUEST_CODE_LOGIN = 16519;
    public static final int REQUEST_CODE_REGISTER = 16520;

    public static final String PLACEAD_FIRST_NAME = "placead_first_name";
    public static final String PLACEAD_LAST_NAME = "placead_last_name";
    public static final String PLACEAD_EMAIL = "placead_email";
    public static final String TARGET = "target";
    public static final String ADVERT_TYPE = "advert_type";
    public static final String PAGE_JOINED_FROM = "page_joined_from";

    private Intent _intentWithExtras;

    //endregion CONTROLLER

    //region METHODS LIFECYCLE

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.material_activity);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        _intentWithExtras = new Intent();
        _intentWithExtras.putExtra(PLACEAD_FIRST_NAME, getIntent().getStringExtra(PLACEAD_FIRST_NAME));
        _intentWithExtras.putExtra(PLACEAD_LAST_NAME, getIntent().getStringExtra(PLACEAD_LAST_NAME));
        _intentWithExtras.putExtra(PLACEAD_EMAIL, getIntent().getStringExtra(PLACEAD_EMAIL));
        _intentWithExtras.putExtra(TARGET, getIntent().getStringExtra(TARGET));
        _intentWithExtras.putExtra(ADVERT_TYPE, getIntent().getStringExtra(ADVERT_TYPE));
        _intentWithExtras.putExtra(PAGE_JOINED_FROM, getIntent().getStringExtra(PAGE_JOINED_FROM));

        transaction.replace(R.id.activity_fragment, new NewExistingFragment()).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        setResult(resultCode, data);

        if (resultCode == RESULT_OK)
            finish();
    }

    //endregion METHODS LIFECYCLE

    //region CONTROLLER

    public Intent getIntentWithExtras() {
        return _intentWithExtras;
    }

    //endregion CONTROLLER

}
