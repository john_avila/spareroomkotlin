package com.spareroom.ui.screen.legacy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.model.business.*;
import com.spareroom.ui.screen.NewExistingActivity;
import com.spareroom.ui.util.ConnectivityChecker;
import com.spareroom.ui.util.ToastUtils;

import javax.inject.Inject;

import static com.spareroom.ui.screen.NewExistingFragment.EXTRA_ACTION_CODE;
import static com.spareroom.ui.screen.NewExistingFragment.EXTRA_ACTION_CODE_SAVE_AD;

public class PlaceAdTitleDescriptionFragment extends PlaceAdFragment implements Injectable {

    //region FIELDS UI

    private EditText _etTitle;
    private EditText _etDescription;
    private Button bPlaceAd;

    //endregion FIELDS UI

    //region FIELDS CONTROLLER

    private DraftAd _draft;
    private final int _requestCode = 29;

    //endregion FIELDS CONTROLLER

    //region METHODS CONTROLLER

    private void placeAdvert() {
        _draft.setTitle(_etTitle.getText().toString());
        _draft.setDescription(_etDescription.getText().toString());
        finish();
    } //end placeAdvert()

    //endregion METHODS CONTROLLER

    //region METHODS LIFECYCLE

    @Inject
    ConnectivityChecker connectivityChecker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setTitle(getString(R.string.place_ad_offered_title_description_title));

        View rootView = inflater.inflate(R.layout.place_ad_offered_title_description, container, false);
        bPlaceAd = rootView.findViewById(R.id.place_ad_offered_title_description_bPlaceAd);
        _etDescription = rootView.findViewById(R.id.place_ad_offered_title_description_etDescription);

        _draft = getDraftAd();

        // In case there is an activity state change and we lost the draft
        if (_draft == null) {
            getActivity().finish();

            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }

        _etTitle = rootView.findViewById(R.id.place_ad_offered_title_description_etTitle);

        if (_draft.getTitle() != null)
            _etTitle.setText(_draft.getTitle());
        if (_draft.getDescription() != null)
            _etDescription.setText(_draft.getDescription());

        _etDescription.setOnTouchListener((view, event) -> {
            if (view.getId() == R.id.place_ad_offered_title_description_etDescription) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
            }
            return false;
        });

        bPlaceAd.setOnClickListener(v -> {
            if (connectivityChecker.isConnected()) {
                boolean offered = _draft instanceof DraftAdOffered;

                if (offered) {
                    AnalyticsTrackerComposite.getInstance().trackEvent_PlaceAd_PlaceOfferedAd();
                } else {
                    AnalyticsTrackerComposite.getInstance().trackEvent_PlaceAd_PlaceWantedAd();
                }

                Session s = SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext().getSession();
                if (s == null) {
                    Intent activityLogin = new Intent();
                    activityLogin.setClass(getActivity(), NewExistingActivity.class);
                    activityLogin.putExtra(EXTRA_ACTION_CODE, EXTRA_ACTION_CODE_SAVE_AD);
                    activityLogin.putExtra(NewExistingActivity.PLACEAD_FIRST_NAME, _draft.get_firstName());
                    activityLogin.putExtra(NewExistingActivity.PLACEAD_LAST_NAME, _draft.get_lastName());
                    activityLogin.putExtra(NewExistingActivity.PLACEAD_EMAIL, _draft.get_email());
                    activityLogin.putExtra(NewExistingActivity.TARGET, "PlaceAdUploadingFragment");
                    activityLogin.putExtra(NewExistingActivity.ADVERT_TYPE, offered ? "offered" : "wanted");
                    activityLogin.putExtra(NewExistingActivity.PAGE_JOINED_FROM, offered ? "place offered ad" : "place wanted ad");
                    getActivity().startActivityForResult(activityLogin, _requestCode);

                } else {
                    placeAdvert();
                }
            } else {
                ToastUtils.showToast(R.string.no_connection);
            }

        });

        Activity activity = getActivity();
        if (activity instanceof EditAdActivity)
            bPlaceAd.setText(getString(R.string.edit_ad_upload_button));

        return rootView;
    } //end onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bPlaceAd.setOnClickListener(null);
        _etDescription.setOnTouchListener(null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == _requestCode && resultCode == Activity.RESULT_OK) {
            placeAdvert();
        }
    } //end onActivityResult(int requestCode, int resultCode, Intent data)

    @Override
    public void onPause() {
        super.onPause();
        _draft.setTitle(_etTitle.getText().toString());
        _draft.setDescription(_etDescription.getText().toString());
    } //end onPause()

    //region METHODS LIFECYCLE

}
