package com.spareroom.ui.filters.adapter.viewparams;

import com.spareroom.model.business.SearchAdvertListProperties.MaxTerm;

public class SummaryViewMaxTermParams extends AbstractSummaryViewParams {
    private MaxTerm maxTerm;

    public SummaryViewMaxTermParams(String tag) {
        super(tag);
    }

    public MaxTerm maxTerm() {
        return maxTerm;
    }

    public void maxTerm(MaxTerm maxTerm) {
        this.maxTerm = maxTerm;
    }

    public void reset() {
        super.reset();
        maxTerm(MaxTerm.NOT_SET);
    }
}
