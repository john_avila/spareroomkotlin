package com.spareroom.ui.screen.legacy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.GoogleMap.*;
import com.google.android.gms.maps.model.*;
import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.controller.legacy.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.integration.imageloader.ImageLoader;
import com.spareroom.integration.imageloader.ImageLoadingListener;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.model.business.*;
import com.spareroom.ui.screen.Activity;
import com.spareroom.ui.util.*;

import java.util.TreeMap;

import javax.inject.Inject;

import androidx.annotation.Nullable;

public class SearchMapAdOfferedFragment extends SupportMapFragment implements
        IAsyncResult, OnMarkerClickListener, OnInfoWindowClickListener, Injectable {
    private TreeMap<String, AdOffered> _tMarker;
    private AsyncTaskManager _asyncManager;

    private Button _bSearchHere;

    private GoogleMap _map;

    private ProgressBar _pb;

    private Parameters _currentSearch;

    public AdOffered _currentAd;

    private OnCameraChanged _onCameraChangeListener;

    private boolean _firstRun = true;

    private BitmapDescriptor normalAdvertPin;
    private BitmapDescriptor newAdvertPin;
    private BitmapDescriptor contactedAdvertPin;
    private BitmapDescriptor unsuitableAdvertPin;
    private BitmapDescriptor savedAdvertPin;

    @Inject
    ImageLoader imageLoader;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AnalyticsTrackerComposite.getInstance().trackScreen_Fragment(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        _currentSearch = SpareroomApplication.getInstance(getActivity().getApplicationContext())
                .getSpareroomContext().getCurrentSearch();

        _asyncManager = new AsyncTaskManager();
        _tMarker = new TreeMap<>();

        _pb = getActivity().findViewById(R.id.activityResultsMap_pb);
        _bSearchHere = getActivity().findViewById(R.id.activityResultsMap_bSearchHere);

        _bSearchHere.setOnClickListener(v -> {
            reload(_map.getCameraPosition());
            _bSearchHere.setVisibility(View.GONE);
        });

        setTitle(getActivity().getIntent().getStringExtra("location"));
        setupMapIfNeeded();
    }

    private void setupMapIfNeeded() {
        if (_map == null) {
            getMapAsync(new MapReadyCallback());
        }
    }

    private void setUpMap() {
        MapsUtil.disableIndoor(_map);
        _map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(55.378051,
                -3.435973), 5));
        _onCameraChangeListener = new OnCameraChanged();
        _map.setOnCameraChangeListener(new OnCameraChanged());
        _map.setOnMarkerClickListener(this);

        _map.setOnInfoWindowClickListener(this);

        _map.setOnMapLoadedCallback(() -> {
            if (UiUtils.isFragmentAlive(this)) {
                normalAdvertPin = BitmapDescriptorFactory.fromResource(R.drawable.map_pin_normal);
                newAdvertPin = BitmapDescriptorFactory.fromResource(R.drawable.map_pin_new);
                contactedAdvertPin = BitmapDescriptorFactory.fromResource(R.drawable.map_pin_contacted);
                unsuitableAdvertPin = BitmapDescriptorFactory.fromResource(R.drawable.map_pin_unsuitable);
                savedAdvertPin = BitmapDescriptorFactory.fromResource(R.drawable.map_pin_saved);

                searchNew();
            }
        });
    }

    private void searchNew() {
        SpareroomApplication srApp = SpareroomApplication.getInstance(
                getActivity().getApplicationContext()
        );

        Parameters p = new Parameters();

        if (_currentSearch != null) {

            p.add(_currentSearch);
            p.add(SearchParamEnum.PAGE, "1");
            p.remove(SearchParamEnum.MAX_PER_PAGE);
            p.add(SearchParamEnum.MAX_PER_PAGE, "50");

            p.add(SearchParamEnum.FEATURED, "1");
            _currentSearch = p;

            Parameters[] arrayParameters = {p};
            SearchAdAsyncTask at = new SearchAdAsyncTask(srApp.getSearchFacade(), this, SearchType.OFFERED);
            _asyncManager.register(at);
            at.execute(arrayParameters);

        } else {
            p.add(SearchParamEnum.WHERE, getActivity().getIntent()
                    .getStringExtra("location"));
            p.add(SearchParamEnum.PAGE, "1");
            p.add(SearchParamEnum.MAX_PER_PAGE, "50");
            p.add(SearchParamEnum.FEATURED, "1");

            Parameters[] arrayParameters = {p};

            SearchAdAsyncTask at = new SearchAdAsyncTask(srApp.getSearchFacade(), this, SearchType.OFFERED);
            _asyncManager.register(at);
            at.execute(arrayParameters);
        }
    }

    @Override
    public void update(Object o) {
        if (UiUtils.isFragmentAlive(SearchMapAdOfferedFragment.this)) {
            if (o instanceof AdBoard) {
                AdBoard board = (AdBoard) o;

                if (_tMarker.size() > 100) {
                    _tMarker.clear();
                    _map.clear();
                    if (_currentAd != null) {
                        Marker m = addMarker(_currentAd);
                        m.showInfoWindow();
                        _tMarker.put(m.getId(), _currentAd);
                    }
                }

                if (board.get_mapReference() != null) {

                    int total = board.get_total();
                    int results = board.get_board().size();
                    setSubtitle(
                            String.format(
                                    AppVersion.flavor().getLocale(),
                                    "%d %s %d %s",
                                    results,
                                    getString(R.string.searchMapAdOfferedFragment_of),
                                    total >= results ? total : results,
                                    getResources().getQuantityString(R.plurals.results, total)));

                    float neLat = board.get_mapReference().get_latitude()
                            - (board.get_mapReference().get_latitudeDelta() / 2);
                    float neLong = board.get_mapReference().get_longitude()
                            - (board.get_mapReference().get_longitudeDelta() / 2);

                    float swLat = board.get_mapReference().get_latitude()
                            + (board.get_mapReference().get_latitudeDelta() / 2);
                    float swLong = board.get_mapReference().get_longitude()
                            + (board.get_mapReference().get_longitudeDelta() / 2);

                    if (_map != null) {
                        // _map.clear();
                        _map.animateCamera(CameraUpdateFactory.newLatLngBounds(
                                new LatLngBounds(new LatLng(neLat, neLong),
                                        new LatLng(swLat, swLong)), 10));

                        _map.setInfoWindowAdapter(new MyInfoWindowAdapter());

                        for (AbstractAd ad : board.get_board()) {
                            addMarker((AdOffered) ad);
                        }
                    }
                }
            }
        }
    }

    private class MyInfoWindowAdapter implements InfoWindowAdapter {
        private final View _infoWindow;

        MyInfoWindowAdapter() {
            _infoWindow = getActivity().getLayoutInflater().inflate(
                    R.layout.map_marker_info_window, null);
        }

        @Override
        public View getInfoWindow(final Marker arg0) {
            final TextView tvTitle = _infoWindow.findViewById(R.id.map_marker_tvTitle);
            final TextView tvSnippet = _infoWindow.findViewById(R.id.map_marker_tvSnippet);
            final ImageView ivPicture = _infoWindow.findViewById(R.id.map_marker_ivPicture);
            final AdOffered ad = _tMarker.get(arg0.getId());

            tvTitle.setText(arg0.getTitle());
            tvSnippet.setText(arg0.getSnippet());

            final String largePictureUrl = ad.getLargePictureUrl();
            if (!StringUtils.isNullOrEmpty(largePictureUrl)) {
                if (UiUtils.isFragmentAlive(SearchMapAdOfferedFragment.this)) {
                    imageLoader.loadCenterCropImageFromFragment(largePictureUrl, ivPicture, new ImageLoadingListener() {
                        @Override
                        public void onLoadingComplete() {
                            getInfoContents(arg0);
                        }
                    }, SearchMapAdOfferedFragment.this);
                }
            } else {
                ivPicture.setImageResource(R.drawable.placeholder_offered);
            }

            return _infoWindow;
        }

        @Override
        public View getInfoContents(Marker arg0) {
            if (arg0 != null
                    && arg0.isInfoWindowShown()) {
                arg0.hideInfoWindow();
                arg0.showInfoWindow();
            }
            return null;
        }
    }

    private Marker addMarker(AdOffered ad) {
        MarkerOptions markerOptions;
        if (ad.getContext().equals(AbstractAd.CONTEXT_SHORTLIST) || ad.getContext().equals(AbstractAd.CONTEXT_INTEREST)) {
            markerOptions = markerOptions(ad, savedAdvertPin);
        } else if (ad.getContext().equals(AbstractAd.CONTEXT_UNSUITABLE)) {
            markerOptions = markerOptions(ad, unsuitableAdvertPin);
        } else if (ad.getContacted()) {
            markerOptions = markerOptions(ad, contactedAdvertPin);
        } else if ((ad.isNewToday())) {
            markerOptions = markerOptions(ad, newAdvertPin);
        } else {
            markerOptions = markerOptions(ad, normalAdvertPin);
        }

        Marker m = _map.addMarker(markerOptions);
        _tMarker.put(m.getId(), ad);

        return m;
    }

    private MarkerOptions markerOptions(AdOffered ad, BitmapDescriptor icon) {
        return new MarkerOptions()
                .position(new LatLng(ad.getLatitude(), ad.getLongitude()))
                .title(ad.getArea() + " (" + ad.getPostcode() + ")")
                .icon(icon)
                .snippet(ad.getAccommodationType() + " - " + ad.getPrice());
    }

    private void reload(CameraPosition arg0) {
        if (_tMarker.size() > 0) {
            LatLngBounds llb = _map.getProjection().getVisibleRegion().latLngBounds;

            Parameters p = new Parameters();
            p.add(SpareroomApplication.getInstance(getActivity().getApplicationContext())
                    .getSpareroomContext().getCurrentSearch());
            p.remove(SearchParamEnum.WHERE);
            p.add("latitude", Double.toString(arg0.target.latitude));
            p.add("longitude", Double.toString(arg0.target.longitude));
            p.add("latitude_delta",
                    Double.toString((llb.northeast.latitude - arg0.target.latitude) * 2));
            p.add("longitude_delta",
                    Double.toString((llb.northeast.longitude - arg0.target.longitude) * 2));
            p.add(SearchParamEnum.MAX_PER_PAGE, "50");

            _asyncManager.cancelAll();
            SearchAdAsyncTask at =
                    new SearchAdAsyncTask(
                            SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSearchFacade(),
                            _onCameraChangeListener,
                            SearchType.OFFERED);
            _pb.setVisibility(View.VISIBLE);
            _asyncManager.register(at);
            at.execute(p);
        }
    }

    private class OnCameraChanged implements OnCameraChangeListener,
            IAsyncResult {

        @Override
        public void onCameraChange(CameraPosition arg0) {
            if (_firstRun)
                _firstRun = false;
            else
                _bSearchHere.setVisibility(View.VISIBLE);
        }

        @Override
        public void update(Object o) {
            if (UiUtils.isFragmentAlive(SearchMapAdOfferedFragment.this)) {
                if (o instanceof AdBoard) {
                    AdBoard board = (AdBoard) o;

                    if (_tMarker.size() > 100) {
                        _tMarker.clear();
                        _map.clear();

                        if (_currentAd != null) {
                            Marker m = addMarker(_currentAd);
                            m.showInfoWindow();
                            _tMarker.put(m.getId(), _currentAd);
                        }
                    }

                    _pb.setVisibility(View.GONE);

                    setSubtitle(
                            String.format(
                                    AppVersion.flavor().getLocale(),
                                    "%d %s %d %s",
                                    board.get_board().size(),
                                    getString(R.string.searchMapAdOfferedFragment_of),
                                    board.get_total(),
                                    getResources().getQuantityString(R.plurals.results, board.get_total())));

                    for (AbstractAd ad : board.get_board()) {
                        LatLngBounds mapBounds = _map.getProjection()
                                .getVisibleRegion().latLngBounds;
                        LatLng adLocation = new LatLng(((AdOffered) ad).getLatitude(), ((AdOffered) ad).getLongitude());
                        if (mapBounds.contains(adLocation)) {
                            addMarker((AdOffered) ad);
                        }
                    }
                }
            }
        }

        @Override
        public void handleInvalidUserException(String message) {
            // TODO Auto-generated method stub

        }

        @Override
        public void handleMissingParameterException(String message) {
            if (UiUtils.isFragmentAlive(SearchMapAdOfferedFragment.this))
                DialogUtil.updateDialog(getActivity()).show();

        }

        @Override
        public void handleMissingSystemFeatureException(String message) {
            if (UiUtils.isFragmentAlive(SearchMapAdOfferedFragment.this))
                DialogUtil.updateDialog(getActivity()).show();

        }

        @Override
        public void handleServiceUnavailableException(String message) {
            if (UiUtils.isFragmentAlive(SearchMapAdOfferedFragment.this))
                Toast.makeText(
                        getActivity(),
                        getActivity().getString(
                                R.string.fragmentMapAdOffered_tNoConnection),
                        Toast.LENGTH_LONG).show();

        }

        @Override
        public void handleInconsistentStateException(String message) {
            if (UiUtils.isFragmentAlive(SearchMapAdOfferedFragment.this))
                DialogUtil.updateDialog(getActivity()).show();

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        _asyncManager.cancelAll();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        _pb.setVisibility(View.GONE);
    }

    @Override
    public boolean onMarkerClick(Marker arg0) {
        arg0.showInfoWindow();
        _currentAd = _tMarker.get(arg0.getId());
        return true;
    }

    @Override
    public void onInfoWindowClick(Marker arg0) {
        AdOffered ad = _tMarker.get(arg0.getId());

        Intent adActivity = new Intent();
        adActivity.setClass(getActivity(), AdActivity.class);
        adActivity.putExtra("id", ad.getId());
        adActivity.putExtra("searchType", SearchType.OFFERED);
        startActivity(adActivity);
    }

    @Override
    public void handleInvalidUserException(String message) {
        // TODO Auto-generated method stub

    }

    @Override
    public void handleMissingParameterException(String message) {
        if (UiUtils.isFragmentAlive(SearchMapAdOfferedFragment.this))
            DialogUtil.updateDialog(getActivity()).show();
    }

    @Override
    public void handleMissingSystemFeatureException(String message) {
        if (UiUtils.isFragmentAlive(SearchMapAdOfferedFragment.this))
            DialogUtil.updateDialog(getActivity()).show();
    }

    @Override
    public void handleServiceUnavailableException(String message) {
        if (UiUtils.isFragmentAlive(SearchMapAdOfferedFragment.this))
            Toast.makeText(
                    getActivity(),
                    getActivity().getString(
                            R.string.fragmentMapAdOffered_tNoConnection),
                    Toast.LENGTH_LONG).show();
    }

    @Override
    public void handleInconsistentStateException(String message) {
        if (UiUtils.isFragmentAlive(SearchMapAdOfferedFragment.this))
            DialogUtil.updateDialog(getActivity()).show();
    }

    private class MapReadyCallback implements OnMapReadyCallback {

        @Override
        public void onMapReady(GoogleMap googleMap) {
            _map = googleMap;

            if (_map != null) {
                setUpMap();
            }
        }
    }

    private void setTitle(String title) {
        Activity activity = (Activity) getActivity();
        if (activity != null)
            activity.setTitle(title);
    }

    private void setSubtitle(String subtitle) {
        Activity activity = (Activity) getActivity();
        if (activity != null)
            activity.setSubtitle(subtitle);
    }
}
