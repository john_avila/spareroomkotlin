package com.spareroom.ui.screen

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.KeyEvent
import com.spareroom.R
import com.spareroom.integration.analytics.AnalyticsTrackerComposite
import com.spareroom.integration.dependency.Injectable
import com.spareroom.integration.sharedpreferences.EventsDao
import com.spareroom.ui.util.AppRating
import java.util.*
import javax.inject.Inject

class RatingReminderDialog : DialogFragment(), Injectable {

    private var tappedOutside = true

    @Inject
    lateinit var eventsDao: EventsDao

    @Inject
    lateinit var appRating: AppRating

    companion object {
        const val TAG = "RatingReminderDialogTag"
        fun getInstance() = RatingReminderDialog()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if (savedInstanceState == null)
            eventsDao.logShowReminderRatingPopupEvent(Calendar.getInstance())

        return AlertDialogBuilder(activity!!)
            .setTitle(R.string.rating_reminder_title)
            .setMessage(R.string.rating_reminder_message)
            .setPositiveButton(R.string.rate_app) { _, _ -> rateApp() }
            .setNegativeButton(R.string.dismiss) { _, _ -> stopAskingForRating() }
            .setOnKeyListener { _, keyCode, keyEvent -> handleBackButton(keyCode, keyEvent) }
            .create()
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        if (isDismissedByTapOutside())
            AnalyticsTrackerComposite.getInstance().trackReminderRatingPopupTapOutside()
    }

    private fun stopAskingForRating() {
        resetTapOutside()
        AnalyticsTrackerComposite.getInstance().trackReminderRatingPopupDismiss()
        dismiss()
    }

    private fun rateApp() {
        resetTapOutside()
        AnalyticsTrackerComposite.getInstance().trackReminderRatingPopupRateApp()
        eventsDao.logRateAppEvent()
        appRating.rateApp(activity as Activity)
        dismiss()
    }

    private fun handleBackButton(keyCode: Int, keyEvent: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_UP == keyEvent?.action) {
            resetTapOutside()
            AnalyticsTrackerComposite.getInstance().trackReminderRatingPopupBackButton()
        }

        return false
    }

    private fun resetTapOutside() {
        tappedOutside = false
    }

    private fun isDismissedByTapOutside() = tappedOutside && isResumed
}