package com.spareroom.ui.screen;

import android.os.Bundle;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.lib.core.IObservable;
import com.spareroom.lib.core.IObserver;
import com.spareroom.model.business.AccountCredentials;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.model.extra.LoginExtra;
import com.spareroom.ui.EmailFieldValidator;
import com.spareroom.ui.PasswordFieldValidator;
import com.spareroom.ui.controller.*;
import com.spareroom.ui.util.*;
import com.spareroom.ui.widget.TextBox;

import java.util.LinkedList;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

/**
 * Fragment for set the email details for the sign in
 */
public class LoginEmailFragment extends Fragment {

    //region FIELDS UI.CONTROL

    private static final int ANIMATION_DURATION = 400;
    private static final String FIELD_USERNAME = "username";
    private static final String FIELD_PASSWORD = "password";
    private SignInProcessCompositeUiController _signInProcessCompositeUiController;

    //endregion FIELDS UI.CONTROL

    //region FIELDS CONTROLLER

    private AccountCredentials _accountCredentials;

    private final ControllerActionObserver _signInObserver = new ControllerActionObserver() {

        // Processes the successful response - it can only be null
        @Override
        public void onResult(Object o) {
            _signInProcessCompositeUiController.setResult(o);

            ((LoginActivity) getActivity()).setActivityResultOK();

            FirebaseIdController.register(SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext());

            AnalyticsTrackerComposite.getInstance().trackEvent_Account_Login_Spareroom();

            getActivity().finish();
        }

        // Processes any error when updating the advert
        @Override
        public void onSpareroomStatus(SpareroomStatus status) {
            _signInProcessCompositeUiController.setSpareroomStatus(status);

            AnalyticsTrackerComposite.getInstance().trackEvent_Account_Login_Spareroom_Failed(status);

        }

        @Override
        public void onException(Exception exception) {
            _signInProcessCompositeUiController.setException(exception);

        }

    };

    //endregion FIELDS CONTROLLER

    //region METHODS CONTROLLER

    private void launchLogin(AccountCredentials accountCredentials) {

        LoginExtra loginExtra = new LoginExtra();
        loginExtra.setPageJoinedFrom(((LoginActivity) getActivity()).getPageJoinedFrom());
        loginExtra.setVersion(AppVersion.appId());

        LoginControllerAction loginControllerAction =
                new LoginControllerAction(SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext());
        loginControllerAction.addControllerActionObserver(_signInObserver);
        addObserver(loginControllerAction);
        loginControllerAction.execute(accountCredentials, loginExtra);

    } //end launchLogin(AccountCredentials accountCredentials)

    //endregion METHODS CONTROLLER

    //region METHODS LIFECYCLE

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        _accountCredentials = (AccountCredentials) ((LinearMultiStepActivity) getActivity()).getSharedObject(LoginActivity.CREDENTIALS);

        View rootView = inflater.inflate(R.layout.material_login_email_fragment, container, false);
        // Email input
        TextBox tbUsername = rootView.findViewById(R.id.loginEmailFragment_emailTextBox);
        // Email input
        TextBox tbPassword = rootView.findViewById(R.id.loginEmailFragment_passwordTextBox);
        // Bottom Toolbar
        View bottomToolbar = getActivity().findViewById(R.id.activity_toolbarBottom);
        TextView tvForgotButton = bottomToolbar.findViewById(R.id.materialToolbarBottom_leftText);
        final TextView tvBottomToolbarRight = bottomToolbar.findViewById(R.id.materialToolbarBottom_rightText);

        // Toolbar
        setToolbar(getActivity().findViewById(R.id.appBarLayout), getString(R.string.loginMethodFragment_toolbarTitle), true);

        // Ui Controllers
        UsernameTextBoxUiController usernameUiController = new UsernameTextBoxUiController(tbUsername, tbPassword);
        PasswordTextBoxUiController passwordUiController = new PasswordTextBoxUiController(tbPassword);
        SignInButtonUiController signInButtonUiController = new SignInButtonUiController(bottomToolbar);
        ForgotButtonUiController _forgotButtonUiController = new ForgotButtonUiController(bottomToolbar);

        _signInProcessCompositeUiController =
                new SignInProcessCompositeUiController(
                        usernameUiController,
                        passwordUiController,
                        signInButtonUiController,
                        _forgotButtonUiController);
        EnableSignInButtonUiController enableSignInButtonUiController = new EnableSignInButtonUiController(signInButtonUiController);
        ErrorStateController errorStateController = new ErrorStateController(_signInProcessCompositeUiController);

        _signInProcessCompositeUiController.addObserver(errorStateController.getErrorObserver());
        // Email input
        tbUsername.setOnContentChangedListener(
                new UsernameOnContentChangedListener(
                        enableSignInButtonUiController.getUsernameObserver(),
                        errorStateController.getInputObserver()));

        // Password input
        tbPassword.setOnContentChangedListener(
                new PasswordOnContentChangedListener(
                        enableSignInButtonUiController.getPasswordObserver(),
                        errorStateController.getInputObserver()));

        tbPassword.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                tvBottomToolbarRight.performClick();
                return true;
            }
            return false;
        });

        // Bottom Toolbar
        bottomToolbar.setVisibility(View.VISIBLE);

        tvForgotButton.setVisibility(View.VISIBLE);
        tvForgotButton.setText(getString(R.string.loginEmailFragment_bottomToolbarForgot));
        tvForgotButton.setOnClickListener(view -> {
            view.setVisibility(View.GONE);
            ((LoginActivity) getActivity()).nextFragment();
        });

        tvBottomToolbarRight.setOnClickListener(new SignInButtonOnClickListener(tbUsername, tbPassword, _signInProcessCompositeUiController));
        tvBottomToolbarRight.setText(getString(R.string.loginEmailFragment_bottomToolbarSignIn));

        _signInProcessCompositeUiController.disable();

        // Fill the TextBoxes if needed
        if (((LinearMultiStepActivity) getActivity()).getSharedObject(LoginActivity.CREDENTIALS) != null) {
            tbUsername.setText(_accountCredentials.getUsername());
            tbPassword.setText(_accountCredentials.getPassword());
        } else {
            _accountCredentials = new AccountCredentials();
            ((LinearMultiStepActivity) getActivity()).setSharedObject(LoginActivity.CREDENTIALS, _accountCredentials);
        }

        return rootView;
    } // end onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((LoginActivity) getActivity()).showKeyboard();

    } //end onViewCreated(View view, Bundle savedInstanceState)

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ((LinearMultiStepActivity) getActivity()).onNavigateBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    private class UsernameOnContentChangedListener implements TextBox.OnContentChangedListener, IObservable {
        private final LinkedList<IObserver> _observerList = new LinkedList<>();

        UsernameOnContentChangedListener(
                IObserver emptyFieldObserver,
                IObserver errorStateObserver) {

            // I decided to use the constructor header for the observers rather than the public
            // method addObserver because the order in which the actual observers we are using
            // execute matters, and using addObserver does not explicitly set the order in which
            // observers should be notified
            addObserver(errorStateObserver);
            addObserver(emptyFieldObserver);
        }

        @Override
        public void onContentChangedEvent(CharSequence s) {

            notifyObservers(s.toString());

            _accountCredentials.setUsername(s.toString());

            ((LinearMultiStepActivity) getActivity()).setSharedObject(LoginActivity.CREDENTIALS, _accountCredentials);

        }

        @Override
        public void addObserver(IObserver o) {
            _observerList.add(o);
        }

        @Override
        public void removeObserver(IObserver o) {
            // Never used
            _observerList.remove(o);
        }

        private void notifyObservers(Object o) {
            for (IObserver observer : _observerList) {
                observer.notifyObserver(o);
            }
        }
    } //end class UsernameOnContentChangedListener

    private class PasswordOnContentChangedListener implements TextBox.OnContentChangedListener, IObservable {
        private final LinkedList<IObserver> _observerList = new LinkedList<>();

        PasswordOnContentChangedListener(IObserver emptyFieldObserver, IObserver errorStateObserver) {

            // I decided to use the constructor header for the observers rather than the public
            // method addObserver because the order in which the actual observers we are using
            // execute matters, and using addObserver does not explicitly set the order in which
            // observers should be notified
            addObserver(errorStateObserver);
            addObserver(emptyFieldObserver);
        }

        @Override
        public void onContentChangedEvent(CharSequence s) {

            notifyObservers(s.toString());

            _accountCredentials.setPassword(s.toString());

            ((LinearMultiStepActivity) getActivity()).setSharedObject(LoginActivity.CREDENTIALS, _accountCredentials);

        }

        @Override
        public void addObserver(IObserver o) {
            _observerList.add(o);
        }

        @Override
        public void removeObserver(IObserver o) {
            // Never used
            _observerList.remove(o);
        }

        private void notifyObservers(Object o) {
            for (IObserver observer : _observerList) {
                observer.notifyObserver(o);
            }
        }

    } //end class PasswordOnContentChangedListener

    /**
     * UiController that defines how to enable or disable the SignInButton
     */
    private class EnableSignInButtonUiController implements ViewUiController {
        private final ViewUiController _signInButtonController;
        private boolean _isEnabled = true;
        private boolean _isUsernameEmpty = true;
        private boolean _isPasswordEmpty = true;
        private final IObserver _usernameObserver = o -> {
            String username = (String) o;
            _isUsernameEmpty = (username == null) || (username.length() == 0);

            if (_isUsernameEmpty || _isPasswordEmpty) {
                if (_isEnabled) {
                    disable();
                    _isEnabled = false;
                }
            } else {
                if (!_isEnabled) {
                    enable();
                    _isEnabled = true;
                }
            }
        };
        private final IObserver _passwordObserver = o -> {
            String password = (String) o;
            _isPasswordEmpty = (password == null) || (password.length() == 0);

            if (_isUsernameEmpty || _isPasswordEmpty) {
                if (_isEnabled) {
                    disable();
                    _isEnabled = false;
                }
            } else {
                if (!_isEnabled) {
                    enable();
                    _isEnabled = true;
                }
            }
        };

        EnableSignInButtonUiController(ViewUiController signInButtonController) {
            _signInButtonController = signInButtonController;
        }

        @Override
        public void enable() {
            _signInButtonController.enable();
        }

        @Override
        public void disable() {
            _signInButtonController.disable();
        }

        @Override
        public void setLoading() {
        }

        /**
         * Sets a status for this view (normally an error for fields that require validation)
         *
         * @param error a <code>SparerooomStatus</code> status
         */
        @Override
        public void setStatus(SpareroomStatus error) {

        }

        /**
         * Clears the status for this view
         */
        @Override
        public void clearStatus() {

        }

        IObserver getUsernameObserver() {
            return _usernameObserver;
        }

        IObserver getPasswordObserver() {
            return _passwordObserver;
        }

    } //end class EnableSignInButtonUiController

    /**
     * Composite controller that updates all UI widgets involved in the sign in process
     */
    private class SignInProcessCompositeUiController implements ViewUiController, ActionUiController, IObservable {
        private final UsernameTextBoxUiController _usernameTextBoxUiController;
        private final PasswordTextBoxUiController _passwordTextBoxUiController;
        private final SignInButtonUiController _signInButtonUiController;
        private final ForgotButtonUiController _forgotButtonUiController;
        private final LinkedList<IObserver> _errorObserverList = new LinkedList<>();

        SignInProcessCompositeUiController(
                UsernameTextBoxUiController usernameTextBoxUiController,
                PasswordTextBoxUiController passwordTextBoxUiController,
                SignInButtonUiController signInButtonUiController,
                ForgotButtonUiController forgotButtonUiController) {

            _usernameTextBoxUiController = usernameTextBoxUiController;
            _passwordTextBoxUiController = passwordTextBoxUiController;
            _signInButtonUiController = signInButtonUiController;
            _forgotButtonUiController = forgotButtonUiController;
        }

        @Override
        public void enable() {
            _usernameTextBoxUiController.enable();
            _passwordTextBoxUiController.enable();
            _signInButtonUiController.enable();
            _forgotButtonUiController.enable();
        }

        @Override
        public void disable() {
            _usernameTextBoxUiController.disable();
            _passwordTextBoxUiController.disable();
            _signInButtonUiController.disable();
            _forgotButtonUiController.disable();
        }

        @Override
        public void setLoading() {
            _usernameTextBoxUiController.setLoading();
            _passwordTextBoxUiController.setLoading();
            _signInButtonUiController.setLoading();
            _forgotButtonUiController.setLoading();
        }

        /**
         * Sets a status for this view (normally an error for fields that require validation)
         *
         * @param error a <code>SparerooomStatus</code> status
         */
        @Override
        public void setStatus(SpareroomStatus error) {

        }

        /**
         * Clears the status for this view
         */
        @Override
        public void clearStatus() {

        }

        @Override
        public void setResult(Object... o) {
            _usernameTextBoxUiController.setResult(o);
            _passwordTextBoxUiController.setResult(o);
            _signInButtonUiController.setResult(o);
            _forgotButtonUiController.enable();
        }

        @Override
        public void setSpareroomStatus(SpareroomStatus spareroomStatus) {

            if ((spareroomStatus != null) && (spareroomStatus.getCause() != null) && spareroomStatus.getCause().equals(FIELD_USERNAME)) {
                _usernameTextBoxUiController.setSpareroomStatus(spareroomStatus);

            } else {
                // if (spareroomStatus.getCause().equals("password")) OR any other API error
                if ((spareroomStatus != null) && (spareroomStatus.getCode() != null) &&
                        // TODO: this code doesn't belong to here
                        (spareroomStatus.getCode().equals("604") || spareroomStatus.getCode().equals("605"))) {

                    spareroomStatus.setMessage(getString(R.string.loginEmailFragment_passwordError));
                }
                _passwordTextBoxUiController.setSpareroomStatus(spareroomStatus);
            }

            _signInButtonUiController.setSpareroomStatus(spareroomStatus);
            _forgotButtonUiController.enable();

            for (IObserver observer : _errorObserverList) {
                observer.notifyObserver(null);
            }
        }

        @Override
        public void setException(Exception exception) {
            SnackBarUtils.showException(LoginEmailFragment.this.getView(), exception);
            enable();

        }

        @Override
        public void addObserver(IObserver o) {
            _errorObserverList.add(o);
        }

        @Override
        public void removeObserver(IObserver o) {
            _errorObserverList.remove(o);
        }

    } //end class SignInProcessCompositeUiController

    /**
     * It is used for show correct views in the BottomToolbar when starting and leaving the fragment
     */
    private class ForgotButtonUiController implements ViewUiController {
        private final TextView _tvForgotButton;

        private ForgotButtonUiController(View bottomToolbar) {
            _tvForgotButton = bottomToolbar.findViewById(R.id.materialToolbarBottom_leftText);

        }

        @Override
        public void enable() {
            _tvForgotButton.setEnabled(true);
            _tvForgotButton.setTextColor(ContextCompat.getColor(_tvForgotButton.getContext(), R.color.white));
        }

        @Override
        public void disable() {
        }

        @Override
        public void setLoading() {
            _tvForgotButton.setEnabled(false);
            _tvForgotButton.setTextColor(ContextCompat.getColor(_tvForgotButton.getContext(), R.color.light_blue));
        }

        /**
         * Sets a status for this view (normally an error for fields that require validation)
         *
         * @param error a <code>SparerooomStatus</code> status
         */
        @Override
        public void setStatus(SpareroomStatus error) {

        }

        /**
         * Clears the status for this view
         */
        @Override
        public void clearStatus() {

        }

    } //end class ForgotController

    /**
     * It is used for set the different views status when the login is in process or we already have
     * the response.
     */
    private class SignInButtonUiController implements ViewUiController, ActionUiController {
        private final TextView _signInButton;
        private final ProgressBar _pbLoading;

        SignInButtonUiController(View bottomToolbar) {
            _signInButton = bottomToolbar.findViewById(R.id.materialToolbarBottom_rightText);
            _pbLoading = bottomToolbar.findViewById(R.id.materialToolbarBottom_loading);
        }

        @Override
        public void disable() {
            _signInButton.setEnabled(false);
            _signInButton.setVisibility(View.VISIBLE);
            _signInButton.setTextColor(ContextCompat.getColor(_signInButton.getContext(), R.color.light_blue));

            _pbLoading.setVisibility(View.GONE);

        }

        @Override
        public void enable() {
            _signInButton.setEnabled(true);
            _signInButton.setVisibility(View.VISIBLE);
            _signInButton.setTextColor(ContextCompat.getColor(_signInButton.getContext(), R.color.white));

            _pbLoading.setVisibility(View.GONE);
        }

        @Override
        public void setLoading() {
            _signInButton.setEnabled(false);
            _signInButton.setVisibility(View.INVISIBLE);

            _pbLoading.setVisibility(View.VISIBLE);
        }

        /**
         * Sets a status for this view (normally an error for fields that require validation)
         *
         * @param error a <code>SpareroomStatus</code> status
         */
        @Override
        public void setStatus(SpareroomStatus error) {

        }

        /**
         * Clears the status for this view
         */
        @Override
        public void clearStatus() {

        }

        @Override
        public void setResult(Object... o) {
            enable();

            ToastUtils.showToast(R.string.signed_in);

        }

        @Override
        public void setSpareroomStatus(SpareroomStatus spareroomStatus) {
            enable();
            AnimationUtils.INSTANCE.shakeLeft(_signInButton);
        }

        @Override
        public void setException(Exception exception) {
            enable();
            AnimationUtils.INSTANCE.shakeLeft(_signInButton);
        }

    } //end class SignInButtonController

    /**
     * UiController that defines the status of the Username TextBox
     */
    private class UsernameTextBoxUiController implements ViewUiController, ActionUiController {
        private final TextBox _tbUsername;
        private final TextBox _tbPassword;
        private boolean _hasBeenMoved = false;

        UsernameTextBoxUiController(TextBox tbUsername, TextBox tbPassword) {
            _tbUsername = tbUsername;
            _tbPassword = tbPassword;

        }

        /**
         * For this UiController, <i>enable</i> means clearing the error displayed. We don't change
         * the text in the input field.
         */
        @Override
        public void enable() {
            if (!_tbUsername.isEnabled())
                _tbUsername.setEnabled(true);
            if (_tbUsername.isMessageDisplayed())
                _tbUsername.hideMessage();

        }

        @Override
        public void setLoading() {
            _tbUsername.setEnabled(false);
            KeyboardUtils.hideKeyboard(_tbUsername);

        }

        /**
         * Sets a status for this view (normally an error for fields that require validation)
         *
         * @param error a <code>SparerooomStatus</code> status
         */
        @Override
        public void setStatus(SpareroomStatus error) {

        }

        /**
         * Clears the status for this view
         */
        @Override
        public void clearStatus() {

        }

        @Override
        public void disable() {
        }

        /**
         * This is how the UI is updated on a successful processing of the TextBox
         */
        @Override
        public void setResult(Object... o) {
            enable();
        }

        /**
         * This processes an error on this field
         */
        @Override
        public void setSpareroomStatus(SpareroomStatus spareroomStatus) {

            _tbUsername.setEnabled(false);

            if (spareroomStatus != null)
                _tbUsername.setMessageText(spareroomStatus.getMessage());

            else
                // should never happen - report to analytics?
                _tbUsername.setMessageText(getString(R.string.loginEmailFragment_errorGeneric));

            if (!_tbUsername.isMessageDisplayed() && !_hasBeenMoved) {
                _tbPassword.animate().translationY(_tbUsername.getMessageTextHeight()).setDuration(ANIMATION_DURATION);

                _hasBeenMoved = !_hasBeenMoved;
            }

            _tbUsername.showMessage();

        }

        @Override
        public void setException(Exception exception) {
        }

    } //end class LoginUiController

    /**
     * UiController that defines the status of the PasswordTextBox
     */
    private class PasswordTextBoxUiController implements ViewUiController, ActionUiController {
        private final TextBox _tbPassword;

        PasswordTextBoxUiController(TextBox tbPassword) {
            _tbPassword = tbPassword;

        }

        /**
         * For this UiController, <i>enable</i> means clearing the error displayed. We don't change
         * the text in the input field.
         */
        @Override
        public void enable() {
            if (!_tbPassword.isEnabled())
                _tbPassword.setEnabled(true);
            if (_tbPassword.isMessageDisplayed())
                _tbPassword.hideMessage();

        }

        @Override
        public void disable() {
        }

        @Override
        public void setLoading() {
            _tbPassword.setEnabled(false);
            KeyboardUtils.hideKeyboard(_tbPassword);

        }

        /**
         * Sets a status for this view (normally an error for fields that require validation)
         *
         * @param error a <code>SparerooomStatus</code> status
         */
        @Override
        public void setStatus(SpareroomStatus error) {

        }

        /**
         * Clears the status for this view
         */
        @Override
        public void clearStatus() {

        }

        /**
         * This is how the UI is updated on a successful processing of the TextBox
         */
        @Override
        public void setResult(Object... o) {
            enable();
        }

        /**
         * This processes an error on this field
         */
        @Override
        public void setSpareroomStatus(SpareroomStatus spareroomStatus) {

            _tbPassword.setEnabled(true);

            if (spareroomStatus != null)
                _tbPassword.setMessageText(spareroomStatus.getMessage());

            else
                // should never happen - report to analytics?
                _tbPassword.setMessageText(getString(R.string.loginEmailFragment_errorGeneric));

            _tbPassword.showMessage();

        }

        @Override
        public void setException(Exception exception) {
        }

    } //end class PasswordUiController

    private class SignInButtonOnClickListener implements View.OnClickListener {
        private final TextBox _tbPassword;
        private final TextBox _tbUsername;
        private final EmailFieldValidator _emailFieldValidator;
        private final PasswordFieldValidator _passwordFieldValidator;
        private final SignInProcessCompositeUiController _signInProcessUiController;

        SignInButtonOnClickListener(TextBox tbUsername, TextBox tbPassword, SignInProcessCompositeUiController signInProcessUiController) {
            _tbUsername = tbUsername;
            _tbPassword = tbPassword;
            _emailFieldValidator = new EmailFieldValidator();
            _passwordFieldValidator = new PasswordFieldValidator();
            _signInProcessUiController = signInProcessUiController;

        }

        @Override
        public void onClick(View v) {
            _signInProcessUiController.setLoading();

            int usernameValidationState = _emailFieldValidator.validate(_tbUsername.getText());
            int passwordValidationState = _passwordFieldValidator.validate(_tbPassword.getText());

            if (usernameValidationState != EmailFieldValidator.VALID) {
                SpareroomStatus spareroomStatus = new SpareroomStatus();
                spareroomStatus.setCode(Integer.toString(usernameValidationState));
                switch (usernameValidationState) {
                    case EmailFieldValidator.EMPTY:
                        spareroomStatus.setMessage(getString(R.string.loginEmailFragment_errorUsernameEmpty));
                        break;
                    case EmailFieldValidator.MALFORMED_ADDRESS:
                        spareroomStatus.setMessage(getString(R.string.loginEmailFragment_errorUsernameMalformed));
                        break;
                }
                spareroomStatus.setCause(FIELD_USERNAME);
                _signInProcessUiController.setSpareroomStatus(spareroomStatus);

            } else if (passwordValidationState != PasswordFieldValidator.VALID) {
                SpareroomStatus spareroomStatus = new SpareroomStatus();
                spareroomStatus.setCode(Integer.toString(usernameValidationState));
                switch (passwordValidationState) {
                    case PasswordFieldValidator.EMPTY:
                        spareroomStatus.setMessage(getString(R.string.loginEmailFragment_errorPasswordEmpty));
                        break;
                    case PasswordFieldValidator.TOO_SHORT:
                        spareroomStatus.setMessage(getString(R.string.loginEmailFragment_errorPasswordTooShort));
                        break;
                    case PasswordFieldValidator.TOO_LONG:
                        spareroomStatus.setMessage(getString(R.string.loginEmailFragment_errorPasswordTooLong));
                        break;
                }
                spareroomStatus.setCause(FIELD_PASSWORD);
                _signInProcessUiController.setSpareroomStatus(spareroomStatus);

            } else {
                launchLogin(_accountCredentials);

            }

        }

    } //end class SignInButtonOnClickListener

    //endregion INNER CLASSES
}
