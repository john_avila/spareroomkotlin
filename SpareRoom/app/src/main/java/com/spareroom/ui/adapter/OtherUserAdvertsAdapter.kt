package com.spareroom.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.spareroom.R
import com.spareroom.integration.imageloader.ImageLoader
import com.spareroom.model.business.AbstractAd
import com.spareroom.model.business.AdvertItem
import com.spareroom.ui.screen.Fragment
import com.spareroom.ui.util.AdvertUtils
import com.spareroom.ui.util.DateUtils
import com.spareroom.ui.widget.AdvertCardView
import javax.inject.Inject

class OtherUserAdvertsAdapter @Inject constructor(
    private val imageLoader: ImageLoader,
    private val dateUtils: DateUtils,
    private val advertUtils: AdvertUtils,
    private val animation: Animation
) : RecyclerViewAdapter<AdvertItem, RecyclerView.ViewHolder>() {

    private lateinit var fragment: Fragment
    private lateinit var regardingAdvertId: String
    private var otherUserFirstName = ""
    private val regarding by lazy { context.getString(R.string.regarding) }
    private val advertClickListener = View.OnClickListener { openAdvertDetails(it) }

    fun init(context: Context, fragment: Fragment, regardingAdvertId: String) {
        super.context = context
        this.fragment = fragment
        this.regardingAdvertId = regardingAdvertId
    }

    fun update(diffResult: DiffUtil.DiffResult, items: List<AdvertItem>, otherUserFirstName: String) {
        this.otherUserFirstName = otherUserFirstName
        update(diffResult, items)
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position).advert.id == AdvertItem.ADVERT_UNAVAILABLE) R.layout.other_user_regarding_advert_unavailable else R.layout.advert_card_view_item
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = inflater.inflate(viewType, parent, false)
        return if (viewType == R.layout.other_user_regarding_advert_unavailable) AdvertUnavailableViewHolder(view) else AdvertViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is AdvertViewHolder -> bindAdvertViewHolder(position, holder)
        }
    }

    private fun bindAdvertViewHolder(position: Int, holder: AdvertViewHolder) {
        val advert = getItem(position).advert
        val showHeader = isRegardingAdvert(advert, position) || position == 1
        val headerTitle = headerTitle(advert, position)
        val showDivider = position == 1

        holder.advertCardView.bind(advert, advertUtils, dateUtils, advertClickListener, imageLoader, fragment, animation,
            showHeader, headerTitle, showDivider)
    }

    private fun headerTitle(advert: AbstractAd, position: Int): String {
        return when {
            isRegardingAdvert(advert, position) -> regarding
            position == 1 -> advertUtils.getFormattedOtherUserAdvertsHeader(otherUserFirstName)
            else -> ""
        }
    }

    private fun isRegardingAdvert(advert: AbstractAd, position: Int) = advert.id == regardingAdvertId && position == 0

    private fun openAdvertDetails(view: View) {
        advertUtils.openAdvertDetails(fragment.activity!!, view.getTag(R.id.item_tag) as AbstractAd, view, regardingAdvertId)
    }

    private class AdvertViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val advertCardView: AdvertCardView = itemView as AdvertCardView
    }

    private class AdvertUnavailableViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}