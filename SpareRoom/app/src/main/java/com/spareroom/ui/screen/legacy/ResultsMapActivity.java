package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.MenuItem;

import com.spareroom.R;
import com.spareroom.controller.legacy.AsyncTaskManager;
import com.spareroom.ui.screen.InjectableActivity;

import androidx.fragment.app.FragmentTransaction;

public class ResultsMapActivity extends InjectableActivity {

    private AsyncTaskManager _asyncManager = new AsyncTaskManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_map);

        setToolbar();

        SearchMapAdOfferedFragment _mapAdOfferedFragment = new SearchMapAdOfferedFragment();
        _mapAdOfferedFragment.setArguments(getIntent().getExtras());

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(R.id.activityResultsMap_flResults, _mapAdOfferedFragment);
        transaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return handleHomeButton(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        _asyncManager.cancelAll();
    }

}
