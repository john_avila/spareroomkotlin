package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.lib.core.Pair;
import com.spareroom.model.business.Session;
import com.spareroom.model.business.Upgrade;
import com.spareroom.ui.screen.AlertDialogBuilder;
import com.spareroom.ui.screen.Fragment;
import com.spareroom.ui.util.UiUtils;

import java.util.List;

public class UpgradeItemFragment extends Fragment {
    private Upgrade _upgrade;
    private int _position;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.upgrade_item, container, false);

        View ivIncludes = rootView.findViewById(R.id.upgradeItem_ivIncludes);
        ivIncludes.setBackground(UiUtils.getTintedDrawable(ivIncludes.getContext(), R.drawable.ribbon, R.color.cyan));

        Bundle b = getArguments();
        _upgrade = (Upgrade) b.getSerializable("upgrade");
        _position = b.getInt("position");

        TextView tvPrice = rootView.findViewById(R.id.upgradeItem_tvPrice);
        Button bUpgrade = rootView.findViewById(R.id.upgradeItem_bUpgrade);
        ListView lvFeatures = rootView.findViewById(R.id.upgradeItem_lvFeatures);

        tvPrice.setText(b.getString("price"));
        bUpgrade.setOnClickListener(new UpdateOnClickListener(b.getString("code")));

        lvFeatures.setAdapter(new UpgradeItemAdapter((List<Pair<String, String>>) b.getSerializable("features")));
        return rootView;
    }

    private void showInfo() {
        final String context = getArguments().getString("contextId");

        Session s = SpareroomApplication.getInstance(
                getActivity().getApplicationContext()
        ).getSpareroomContext().getSession();

        LayoutInflater inflater = getLayoutInflater();

        View vDialog = inflater.inflate(R.layout.dialog_upgrade_info, null);

        for (int i = 0; i < s.get_upgradeOptions().get_options().get(context).get_benefits().length; i++) {

            View vEntry = inflater.inflate(R.layout.dialog_upgrade_info_entry, null);

            TextView tvTitle = vEntry.findViewById(R.id.dialog_upgrade_info_tvTitle);
            TextView tvBody = vEntry.findViewById(R.id.dialog_upgrade_info_tvBody);

            LinearLayout llDialog = vDialog.findViewById(R.id.dialog_upgrade_info_ll);
            tvTitle.setText(s.get_upgradeOptions().get_options().get(context).get_benefits()[i][1]);
            tvBody.setText(s.get_upgradeOptions().get_options().get(context).get_benefits()[i][0]);

            llDialog.addView(vEntry);
        }

        new AlertDialogBuilder(getActivity(), vDialog).show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.info_menu, menu);
        menu.findItem(R.id.menu_item_info).setIcon(UiUtils.getTintedDrawable(getActivity(), R.drawable.ic_info, R.color.white));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_info) {
            showInfo();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class UpgradeItemAdapter extends BaseAdapter {
        private List<Pair<String, String>> _list;

        public UpgradeItemAdapter(List<Pair<String, String>> list) {
            super();
            _list = list;
        }

        @Override
        public int getCount() {
            return _list.size();
        }

        @Override
        public Object getItem(int position) {
            return _list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null)
                v = View.inflate(parent.getContext(), R.layout.upgrade_item_feature, null);

            Pair<String, String> p = _list.get(position);

            int topPadding = position == 0 ? v.getContext().getResources().getDimensionPixelOffset(R.dimen.material_side_text_box_margin) : 0;
            v.setPadding(0, topPadding, 0, 0);

            ((TextView) v.findViewById(R.id.txtUpgradeItemFeatureTitle)).setText(p.getKey());
            ((TextView) v.findViewById(R.id.txtUpgradeItemFeatureDescription)).setText(p.getValue());
            return v;
        }
    }

    public class UpdateOnClickListener implements OnClickListener {
        private String _sku;

        public UpdateOnClickListener(String sku) {
            _sku = sku;
        }

        @Override
        public void onClick(View v) {
            ((UpgradeFragmentActivity) getActivity()).launchPurchaseFlow(_sku, _upgrade, _position);
        }

    }
}
