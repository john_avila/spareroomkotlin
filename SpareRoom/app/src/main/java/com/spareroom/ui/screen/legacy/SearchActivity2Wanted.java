package com.spareroom.ui.screen.legacy;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.*;
import android.text.style.StyleSpan;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.controller.legacy.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.sharedpreferences.SearchHistoryDAO;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.lib.util.HardwareFeatures;
import com.spareroom.model.business.*;
import com.spareroom.model.legacy.SearchHistory;
import com.spareroom.platform.location.ILocationObserver;
import com.spareroom.platform.location.LocationService;
import com.spareroom.ui.screen.Activity;
import com.spareroom.ui.screen.AlertDialogBuilder;
import com.spareroom.ui.util.PermissionUtils;
import com.spareroom.ui.util.ToastUtils;

import java.io.*;
import java.text.DecimalFormat;
import java.util.List;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;

import static com.spareroom.ui.screen.legacy.ResultsActivity.BREADCRUMBS;

public class SearchActivity2Wanted extends Activity implements ILocationObserver {
    private ImageView _ivClear;
    private EditText _etSearch;
    private LinearLayout _llHistory;
    private SearchHistory _searchHistory;
    private AsyncTaskManager _asyncManager;
    private View _svSuggestions;
    private View searchToolbar;
    private View shadowView;
    private LinearLayout _llSuggestions;

    private TextView _bLocation;
    private LocationService _ls;

    private boolean _isAutocompleteDisplayed = false;

    private static final int MY_PERMISSIONS_REQUEST_READ_LOCATION = 1;
    private LinearLayout.LayoutParams suggestionParams;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_2);
        suggestionParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        suggestionParams.setMargins(0, 0, 0, getResources().getDimensionPixelSize(R.dimen.material_margin_small));

        _asyncManager = new AsyncTaskManager();

        _etSearch = findViewById(R.id.search_view);
        ImageView ivBack = findViewById(R.id.search_back);
        _bLocation = findViewById(R.id.tvLocation);
        _bLocation.setVisibility(HardwareFeatures.supportsLocation(this) ? View.VISIBLE : View.GONE);
        _ivClear = findViewById(R.id.search_clear);
        _llHistory = findViewById(R.id.llHistory);
        _svSuggestions = findViewById(R.id.svSuggestions);
        _svSuggestions.setOnClickListener(v -> hideAutocomplete());
        _llSuggestions = findViewById(R.id.search_suggestions);
        searchToolbar = findViewById(R.id.searchToolbar);
        shadowView = findViewById(R.id.shadowView);

        // Avoids the problem of characters that cannot be used for file names (see: SearchSuggestionListDAO.read(String))
        InputFilter filter = (source, start, end, dest, dStart, dEnd) -> {
            // List of the characters not allowed
            String blockedCharacters = "/";

            if (blockedCharacters.contains(source))
                return "";

            return null;
        };
        _etSearch.setFilters(new InputFilter[]{filter});

        _etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                String search = _etSearch.getText().toString();
                if (search.length() == 0) {
                    Toast.makeText(SearchActivity2Wanted.this, R.string.activitySearch_tEmptyLocation, Toast.LENGTH_LONG).show();
                    return true;
                }

                // if ((the search is a number) && (it is not an US ZIP code))
                if (Pattern.matches("[0-9]+", search) && !(AppVersion.isUs() && (search.length() == 5))) {
                    _searchHistory.add(_etSearch.getText().toString());

                    try {
                        SearchHistoryDAO.save(SearchActivity2Wanted.this, _searchHistory);
                    } catch (FileNotFoundException e) {
                        // another empty catch
                    } catch (IOException e) {
                        // another empty catch
                    }
                    AnalyticsTrackerComposite.getInstance().trackEvent_SearchFlatmates_ManualAdvertSearch();
                    launchAdvertSearch(search);
                    return true;
                }
                _searchHistory.add(_etSearch.getText().toString());

                try {
                    SearchHistoryDAO.save(SearchActivity2Wanted.this, _searchHistory);
                } catch (FileNotFoundException e) {
                    // another empty catch
                } catch (IOException e) {
                    // another empty catch
                }
                AnalyticsTrackerComposite.getInstance().trackEvent_SearchFlatmates_ManualSearch();
                launchSearch(search);
                return true;
            }
            return false;
        });

        _etSearch.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus)
                hideAutocomplete();
        });
        ivBack.setOnClickListener(v -> SearchActivity2Wanted.this.finish());

        _ivClear.setOnClickListener(v -> _etSearch.setText(""));

        TextWatcher tw = new TextWatcher() {

            AutocompleteUpdater pendingAutocomplete = null;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                _ls.disableUpdates();
                enableLocation();
                if (s.toString().equals("")) {
                    _ivClear.setVisibility(View.GONE);
                    hideAutocomplete();
                } else {
                    _ivClear.setVisibility(View.VISIBLE);

                    if (pendingAutocomplete != null)
                        pendingAutocomplete.cancel();
                    pendingAutocomplete = new AutocompleteUpdater(s.toString());
                    pendingAutocomplete.execute();
                }
            }
        };

        _etSearch.addTextChangedListener(tw);

        _bLocation.setOnClickListener(new LocationOnClickListener());

        _ls = new LocationService(this);
        _ls.addObserver(this);

    }

    @Override
    public void onBackPressed() {
        if (isAutocompleteDisplayed())
            hideAutocomplete();
        else
            super.onBackPressed();
    }

    private void displayAutocomplete() {
        searchToolbar.setBackgroundResource(R.drawable.rounded_corner_top_white);
        _svSuggestions.setVisibility(View.VISIBLE);
        shadowView.setVisibility(View.VISIBLE);
        _isAutocompleteDisplayed = true;
    }

    private boolean isAutocompleteDisplayed() {
        return _isAutocompleteDisplayed;
    }

    private void hideAutocomplete() {
        searchToolbar.setBackgroundResource(R.drawable.rounded_corner_rect_white);
        _svSuggestions.setVisibility(View.INVISIBLE);
        shadowView.setVisibility(View.INVISIBLE);
        _isAutocompleteDisplayed = false;
    }

    private class AutocompleteUpdater {
        private final String _search;
        private boolean _isCancelled = false;

        AutocompleteUpdater(String search) {
            _search = search;
        }

        public void execute() {
            new CountDownTimer(800, 800) {

                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    if (!_isCancelled) {
                        SearchSuggestionListAsyncTask at =
                                new SearchSuggestionListAsyncTask(
                                        SpareroomApplication.getInstance(getApplicationContext()).getSearchFacade(),
                                        SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                                        new AutocompleteListener(_search));
                        _asyncManager.register(at);
                        at.execute(getApplicationContext(), _search);
                    }
                }
            }.start();
        }

        public void cancel() {
            _isCancelled = true;
        }

    }

    private class AutocompleteListener extends IAsyncResultImpl {
        private final String _search;

        AutocompleteListener(String search) {
            _search = search;
        }

        @Override
        public void update(Object o) {
            if (_search.equals(_etSearch.getText().toString())) {
                _llSuggestions.removeAllViews();
                if ((o != null) && (o instanceof SearchAutocompleteList)) {
                    SearchAutocompleteList lSearchSuggestion = (SearchAutocompleteList) o;
                    if (AppVersion.isUk()) {
                        if ((lSearchSuggestion.get_lSuggestion().size() == 0) && (!Pattern.matches("[0-9]+", _etSearch.getText().toString()))) {
                            addGeneralSearchOption();
                        } else if (Pattern.matches("[0-9]+", _etSearch.getText().toString())) {
                            addAdvertOption();
                        } else {
                            addAutocompleteOptions(lSearchSuggestion);
                        }
                    } else if (AppVersion.isUs()) {
                        String strSearch = _etSearch.getText().toString();

                        if (Pattern.matches("[0-9]+", strSearch)) {
                            if (strSearch.length() == 5) {
                                addGeneralSearchOption();
                            } else {
                                addAdvertOption();
                            }
                        } else if (lSearchSuggestion.get_lSuggestion().size() == 0) {
                            addGeneralSearchOption();
                        } else { // show autocomplete options
                            addAutocompleteOptions(lSearchSuggestion);
                        }
                    }
                }
                displayAutocomplete();
            }
        }

        private void addAdvertOption() {
            View v = LayoutInflater.from(SearchActivity2Wanted.this).inflate(R.layout.search_suggestion, _llSuggestions, false);
            v.setBackgroundResource(R.drawable.rounded_corner_bottom_white);
            ImageView ivKind = v.findViewById(R.id.ivKind);
            TextView tvSuggestion = v.findViewById(R.id.tvText);
            ivKind.setImageResource(R.drawable.ic_search);
            tvSuggestion.setText(
                    String.format(
                            AppVersion.flavor().getLocale(),
                            "%s%s",
                            getString(R.string.searchActivity2_searchAdvert),
                            _etSearch.getText().toString()));
            OnAdvertAutocompleteClickListener listener = new OnAdvertAutocompleteClickListener(_etSearch.getText().toString());
            v.setOnClickListener(listener);
            tvSuggestion.setOnClickListener(listener);
            ivKind.setOnClickListener(listener);
            _llSuggestions.addView(v);
        }

        private void addGeneralSearchOption() {
            View v = LayoutInflater.from(SearchActivity2Wanted.this).inflate(R.layout.search_suggestion, _llSuggestions, false);
            v.setBackgroundResource(R.drawable.rounded_corner_bottom_white);
            ImageView ivKind = v.findViewById(R.id.ivKind);
            TextView tvSuggestion = v.findViewById(R.id.tvText);
            ivKind.setImageResource(R.drawable.ic_search);
            tvSuggestion.setText(
                    String.format(
                            AppVersion.flavor().getLocale(),
                            "%s %s",
                            getString(R.string.searchActivity2_searchFor),
                            _etSearch.getText().toString()));
            OnAutocompleteClickListener listener = new OnAutocompleteClickListener(_etSearch.getText().toString());
            v.setOnClickListener(listener);
            tvSuggestion.setOnClickListener(listener);
            ivKind.setOnClickListener(listener);
            _llSuggestions.addView(v);

        }

        private void addAutocompleteOptions(SearchAutocompleteList lSearchSuggestion) {
            List<SearchAutocomplete> lSuggestion = lSearchSuggestion.get_lSuggestion();
            int suggestionsCount = lSearchSuggestion.get_lSuggestion().size();
            for (int i = 0; i < suggestionsCount; i++) {
                SearchAutocomplete suggestion = lSuggestion.get(i);
                View v = LayoutInflater.from(SearchActivity2Wanted.this).inflate(R.layout.search_suggestion, _llSuggestions, false);
                if (i == suggestionsCount - 1) {
                    v.setBackgroundResource(R.drawable.rounded_corner_bottom_white);
                    v.setLayoutParams(suggestionParams);
                }
                TextView tvSuggestion = v.findViewById(R.id.tvText);
                ImageView ivKind = v.findViewById(R.id.ivKind);
                SpannableStringBuilder ssb = new SpannableStringBuilder(suggestion.get_suggestion());
                int posStart =
                        suggestion
                                .get_suggestion()
                                .toLowerCase(AppVersion.flavor().getLocale())
                                .indexOf(_etSearch.getText().toString().toLowerCase(AppVersion.flavor().getLocale()));
                int posEnd = posStart + _etSearch.getText().toString().length();
                OnAutocompleteClickListener listener = new OnAutocompleteClickListener(suggestion.get_suggestion());

                if ((posStart != -1) && (posEnd != -1)) {
                    ssb.setSpan(new StyleSpan(Typeface.BOLD), posStart, posEnd, 0);
                }

                tvSuggestion.setText(ssb);
                _llSuggestions.addView(v);

                v.setOnClickListener(listener);
                tvSuggestion.setOnClickListener(listener);
                ivKind.setOnClickListener(listener);
            }
        }
    }

    private class OnAutocompleteClickListener implements OnClickListener {
        private final String _suggestion;

        OnAutocompleteClickListener(String suggestion) {
            _suggestion = suggestion;
        }

        @Override
        public void onClick(View v) {
            _searchHistory.add(_suggestion);

            try {
                SearchHistoryDAO.save(SearchActivity2Wanted.this, _searchHistory);
            } catch (FileNotFoundException e) {
                // another empty catch
            } catch (IOException e) {
                // another empty catch
            }
            AnalyticsTrackerComposite.getInstance().trackEvent_SearchFlatmates_AutocompleteSearch();
            launchSearch(_suggestion);
        }
    }

    private class OnAdvertAutocompleteClickListener implements OnClickListener {
        private final String _suggestion;

        OnAdvertAutocompleteClickListener(String suggestion) {
            _suggestion = suggestion;
        }

        @Override
        public void onClick(View v) {
            AnalyticsTrackerComposite.getInstance().trackEvent_SearchFlatmates_AutocompleteAdvertSearch();
            launchAdvertSearch(_suggestion);
        }

    }

    private class LocationOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            AnalyticsTrackerComposite.getInstance().trackEvent_SearchFlatmates_GetLocation();
            if (PermissionUtils.hasLocationPermissions(SearchActivity2Wanted.this)) {
                disableLocation();
                _ls.enableUpdates();
            } else {
                enableLocation();
                locationDisabledForApp();
            }
        }

    }

    private void enableLocation() {
        _bLocation.setEnabled(true);
        _bLocation.setText(R.string.search_nearby);
    }

    private void disableLocation() {
        _ls.disableUpdates();
        _bLocation.setEnabled(false);
        _bLocation.setText(R.string.activitySearch_bLocation_loading);
    }

    private void launchSearch(String location) {
        Intent activityResults = new Intent();
        activityResults.setClass(SearchActivity2Wanted.this, ResultsActivity.class);
        activityResults.putExtra("search", location);
        activityResults.putExtra("type", SearchType.WANTED);
        activityResults.putExtra(BREADCRUMBS, "SearchActivity2Wanted");
        startActivity(activityResults);
    }

    private void launchAdvertSearch(String advertId) {
        Intent activityAd = new Intent();
        activityAd.setClass(SearchActivity2Wanted.this, AdActivity.class);
        activityAd.putExtra("id", advertId);
        activityAd.putExtra("searchType", SearchType.OFFERED);
        activityAd.putExtra("tryWanted", true);
        startActivity(activityAd);
    }

    @Override
    protected void onResume() {
        super.onResume();
        _llHistory.removeAllViews();
        try {
            _searchHistory = SearchHistoryDAO.load(this);
        } catch (StreamCorruptedException e) {
            // Nothing to do, go with empty history
        } catch (FileNotFoundException e) {
            // Nothing to do, go with empty history
        } catch (ClassNotFoundException e) {
            // Nothing to do, go with empty history
        } catch (IOException e) {
            // Nothing to do, go with empty history
        }

        if (_searchHistory == null) {
            _searchHistory = new SearchHistory();
        }

        if (_searchHistory.isEmpty() && SpareroomApplication.getInstance(getApplicationContext())
                .getSpareroomContext().getSession() == null) {
            if (_etSearch.requestFocus()) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        } else {
            if (_etSearch.requestFocus()) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        }

        String[] vHistory = _searchHistory.getAll();
        for (String s : vHistory) {
            if (s != null) {
                TextView tv = (TextView) LayoutInflater.from(this).inflate(R.layout.search_history_item, _llHistory, false);
                tv.setText(s);
                tv.setOnClickListener(v -> {
                    String search = ((TextView) v).getText().toString();

                    // if ((the search is a number) && (it is not an US ZIP code))
                    if (Pattern.matches("[0-9]+", search) && !(AppVersion.isUs() && (search.length() == 5))) {
                        AnalyticsTrackerComposite.getInstance().trackEvent_SearchFlatmates_HistoryAdvertSearch();
                        launchAdvertSearch(((TextView) v).getText().toString());
                    } else {
                        AnalyticsTrackerComposite.getInstance().trackEvent_SearchFlatmates_HistorySearch();
                        launchSearch(((TextView) v).getText().toString());
                    }
                });
                _llHistory.addView(tv);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        _asyncManager.cancelAll();
    }

    @Override
    public void updateLocation(Location l) {
        SpareroomApplication srApplication = SpareroomApplication.getInstance(getApplicationContext());
        DecimalFormat df = new DecimalFormat("####.########");
        Parameters p = new Parameters();

        p.add("latitude", df.format(l.getLatitude()));
        p.add("longitude", df.format(l.getLongitude()));

        Point2PostcodeAsyncTask at =
                new Point2PostcodeAsyncTask(srApplication.getLocationFacade(), srApplication.getSpareroomContext(), locationAsyncResult);

        _asyncManager.register(at);
        at.execute(p);
    }

    private class SettingsOnClickListener implements android.view.View.OnClickListener {
        private final Dialog _dialog;

        SettingsOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            _dialog.dismiss();
        }

    }

    @Override
    public void locationProviderDisabled() {
        View v = LayoutInflater.from(this).inflate(R.layout.dialog_message_buttons, null);

        TextView tvMessage = v.findViewById(R.id.dialog_message);
        Button bPositive = v.findViewById(R.id.dialog_bPositive);
        Button bNegative = v.findViewById(R.id.dialog_bNegative);

        tvMessage.setText(getString(R.string.dialog_location_message));

        Dialog d = new AlertDialogBuilder(this, v).create();

        bPositive.setOnClickListener(new SettingsOnClickListener(d));
        bPositive.setText(R.string.settings);
        bNegative.setOnClickListener(new DialogUtil.CancelOnClickListener(d));
        bNegative.setText(R.string.dialog_location_bCancel);

        d.show();

        enableLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    disableLocation();
                    _ls.enableUpdates();
                } else {
                    ToastUtils.showToast(getString(R.string.permission_denied));
                }
                break;
            }
        }

    }

    private void locationDisabledForApp() {
        PermissionUtils.requestLocationPermissions(this, MY_PERMISSIONS_REQUEST_READ_LOCATION);
        enableLocation();
    }

    private final IAsyncResult locationAsyncResult = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            if (o instanceof String) {
                String location = (String) o;

                if (location.isEmpty() || location.equals("null"))
                    Toast.makeText(getApplication(), "We could not identify your current location", Toast.LENGTH_LONG).show();
                else {
                    _etSearch.setText("");
                    _etSearch.append(location);
                    if (isActivityResumed())
                        new OnAutocompleteClickListener(location).onClick(_etSearch);
                }

                enableLocation();
            }
        }

    };

}
