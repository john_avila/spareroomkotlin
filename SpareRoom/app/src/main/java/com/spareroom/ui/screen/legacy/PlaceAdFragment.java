package com.spareroom.ui.screen.legacy;

import android.app.Activity;

import com.spareroom.ui.screen.Fragment;
import com.spareroom.model.business.DraftAd;

public class PlaceAdFragment extends Fragment {
    private PlaceAdAbstractActivity _activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof PlaceAdAbstractActivity) {
            _activity = (PlaceAdAbstractActivity) activity;
        }
    }

    protected DraftAd getDraftAd() {
        return _activity.getDraftAd();
    }

    protected void setDraftAd(DraftAd draft) {
        _activity.setDraftAd(draft);
    }

    protected void finish() {
        _activity.screenFinished();
    }

    protected void setTitle(String title) {
        _activity.setTitle(title);
    }
}
