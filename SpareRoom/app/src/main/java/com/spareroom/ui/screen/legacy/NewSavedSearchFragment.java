package com.spareroom.ui.screen.legacy;

import android.app.Activity;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.controller.legacy.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.business.apiv2.util.SearchAdvertListPropertiesAdapter;
import com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.model.business.*;
import com.spareroom.ui.screen.Fragment;
import com.spareroom.ui.util.*;

import javax.inject.Inject;

import androidx.annotation.NonNull;

public class NewSavedSearchFragment extends Fragment implements Injectable {
    private static final String SEARCH_PROPERTIES_ARGS_KEY = "search_properties_args_key";

    private final AsyncTaskManager _asyncManager = new AsyncTaskManager();
    private EditText _etName;
    private CheckBox _cbDailyAlert;
    private CheckBox _cbInstantAlert;

    @Inject
    ConnectivityChecker connectivityChecker;

    public static NewSavedSearchFragment newInstance(SearchAdvertListProperties searchAdvertListProperties, SearchType searchType) {
        NewSavedSearchFragment f = new NewSavedSearchFragment();
        Bundle arguments = new Bundle();
        arguments.putSerializable(SEARCH_PROPERTIES_ARGS_KEY, searchAdvertListProperties);
        arguments.putSerializable("searchType", searchType);
        f.setArguments(arguments);
        return f;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_savedsearch_new, container, false);

        _etName = rootView.findViewById(R.id.fragment_savedSearch_new_etName);
        _cbDailyAlert = rootView.findViewById(R.id.fragment_savedSearch_new_cbDailyAlerts);
        _cbInstantAlert = rootView.findViewById(R.id.fragment_savedSearch_new_cbInstantAlerts);
        Button _bSave = rootView.findViewById(R.id.fragment_savedSearch_new_bSave);

        _bSave.setOnClickListener(new SaveOnClickListener());

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        _asyncManager.cancelAll();
    }

    private class SaveOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            if (StringUtils.isNullOrEmpty(_etName.getText())) {
                SnackBarUtils.show(_etName, getString(R.string.search_name_empty));
                return;
            }

            if (!connectivityChecker.isConnected()) {
                SnackBarUtils.show(_etName, getString(R.string.no_connection));
                return;
            }

            // track save search
            AnalyticsTrackerComposite.getInstance().trackEvent_SearchRoomsMenu_SaveSearch();

            SearchType searchType = (SearchType) getArguments().get("searchType");
            Parameters p = new Parameters();
            p.add("set_up_id", "1");
            p.add("format", "json");
            p.add("search_name", _etName.getText().toString());
            p.add("emailnotify", _cbDailyAlert.isChecked() ? "Y" : "N");
            p.add("emailnotify_justin", _cbInstantAlert.isChecked() ? "Y" : "N");
            p.add("flatshare_type", (searchType == SearchType.WANTED) ? "wanted" : "offered");
            p.add(SearchParamEnum.FEATURED, "1");

            SearchAdvertListProperties searchAdvertListProperties =
                    (SearchAdvertListProperties) getArguments().getSerializable(SEARCH_PROPERTIES_ARGS_KEY);
            assert searchAdvertListProperties != null;
            p.add(new SearchAdvertListPropertiesAdapter().adapt(searchAdvertListProperties, false));

            SavedSearchNewAsyncTask at =
                    new SavedSearchNewAsyncTask(
                            SpareroomApplication.getInstance().getSearchFacade(),
                            SpareroomApplication.getInstance().getSpareroomContext(),
                            new IAsyncResultImpl() {

                                @Override
                                public void update(Object o) {
                                    getActivity().setResult(Activity.RESULT_OK);
                                    getActivity().finish();
                                }

                                @Override
                                public void handleMissingSystemFeatureException(String message) {
                                    DialogUtil.updateDialog(getActivity()).show();
                                }

                                @Override
                                public void handleMissingParameterException(String message) {
                                    DialogUtil.updateDialog(getActivity()).show();
                                }

                                @Override
                                public void handleInconsistentStateException(String message) {
                                    DialogUtil.updateDialog(getActivity()).show();
                                }
                            });
            _asyncManager.register(at);
            at.execute(p);
        }

    }

}
