package com.spareroom.ui.screen;

import android.content.Intent;
import android.os.Bundle;
import android.view.*;

import com.google.android.material.tabs.TabLayout;
import com.spareroom.R;
import com.spareroom.model.business.*;
import com.spareroom.ui.adapter.ConversationPagerAdapter;
import com.spareroom.ui.util.*;
import com.spareroom.ui.widget.ProgressView;
import com.spareroom.viewmodel.ConversationViewModel;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import static com.spareroom.ui.screen.ConversationListFragment.*;

public class ConversationActivity extends InjectableActivity {
    public static final String NAVIGATE_TO_CONVERSATION = "NAVIGATE_TO_CONVERSATION";
    public static final int ADVERT_DETAILS_REQUEST_CODE = 0;

    private ConversationInfo conversationInfo;
    private ConversationViewModel viewModel;
    private View rootView;
    private ViewPager viewPager;
    private ProgressView progressView;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    public static void startForResult(Fragment fragment, int requestCode, ConversationInfo conversationInfo) {
        Intent conversationActivity = new Intent(fragment.getActivity(), ConversationActivity.class);
        conversationActivity.putExtra(ConversationInfo.TAG, conversationInfo);
        fragment.startActivityForResult(conversationActivity, requestCode);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.material_conversation_activity);

        rootView = findViewById(R.id.conversationActivityRootView);
        progressView = findViewById(R.id.conversationActivityProgressView);

        conversationInfo = getArgs(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ConversationViewModel.class);
        viewModel.setConversationInfo(conversationInfo);

        setToolbar();
        setUpViewPager();
        setUpDeleteConversationConfirmationObserver();
        setUpDeleteConversationObserver();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.conversation_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity(UPDATE_CONVERSATION);
            return true;
        }

        if (item.getItemId() == R.id.conversation_menu_report) {
            EmailUtils.reportAdvert(this, conversationInfo.getAdvertId());
            return true;
        }

        if (item.getItemId() == R.id.conversation_menu_delete) {
            DeleteConversationDialog.getInstance().show(this, DeleteConversationDialog.TAG);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ConversationInfo.TAG, getIntent().getSerializableExtra(ConversationInfo.TAG));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADVERT_DETAILS_REQUEST_CODE && resultCode == RESULT_OK
                && data != null && data.getBooleanExtra(NAVIGATE_TO_CONVERSATION, false))
            viewPager.setCurrentItem(0);
    }

    @Override
    public void onBackPressed() {
        closeActivity(UPDATE_CONVERSATION);
    }

    private ConversationInfo getArgs(Bundle savedInstanceState) {
        if (savedInstanceState == null)
            return (ConversationInfo) getIntent().getSerializableExtra(ConversationInfo.TAG);

        return (ConversationInfo) savedInstanceState.getSerializable(ConversationInfo.TAG);
    }

    private void setUpViewPager() {
        viewPager = findViewById(R.id.conversation_view_pager);
        viewPager.setPageMargin(getResources().getDimensionPixelSize(R.dimen.page_margin));
        viewPager.setPageMarginDrawable(R.drawable.vertical_page_divider);
        viewPager.setAdapter(new ConversationPagerAdapter(getSupportFragmentManager(), this));
        TabLayout tabLayout = findViewById(R.id.conversation_tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position == 1)
                    KeyboardUtils.hideKeyboard(viewPager);
            }
        });
    }

    private void setUpDeleteConversationConfirmationObserver() {
        viewModel.getDeleteConversationConfirmation().observe(this, deleteConversationConfirmationEvent -> {
            progressView.show(getString(R.string.deleting_conversation));

            DeleteConversationRequest deleteConversationRequest = new DeleteConversationRequest();
            deleteConversationRequest.setAdvertId(conversationInfo.getAdvertId());
            deleteConversationRequest.setOtherUserId(conversationInfo.getOtherUserId());
            viewModel.getDeleteConversation().delete(deleteConversationRequest);
        });
    }

    private void setUpDeleteConversationObserver() {
        viewModel.getDeleteConversation().observe(this, response -> {
            assert response != null;
            if (response.isSuccessful()) {
                closeActivity(CONVERSATION_DELETED);
                return;
            }

            progressView.hide();
            SnackBarUtils.show(rootView, response.getErrorMessage(ConversationActivity.this, getString(R.string.failedToDelete)));
        });
    }

    private void closeActivity(String responseType) {
        int messages = viewModel.getFetchConversation().messages().size();
        Intent intent = new Intent();
        intent.putExtra(CONVERSATION_ID, conversationInfo.getThreadId());

        if (UPDATE_CONVERSATION.equals(responseType) && messages > 0) {
            Message lastMessage = viewModel.getFetchConversation().messages().get(messages - 1);

            intent.putExtra(UPDATE_CONVERSATION, true);
            intent.putExtra(MESSAGE_DIRECTION, lastMessage.direction() == null ? "" : lastMessage.direction());
            intent.putExtra(CONVERSATION_SNIPPET, lastMessage.getMessage() == null ? "" : lastMessage.getMessage());
            intent.putExtra(DATE_LAST_ACTIVITY, lastMessage.getDateOfContact() == null ? "" : lastMessage.getDateOfContact());
            setResult(RESULT_OK, intent);

        } else if (CONVERSATION_DELETED.equals(responseType)) {
            intent.putExtra(CONVERSATION_DELETED, true);
            setResult(RESULT_OK, intent);
        }

        finish();
    }

}
