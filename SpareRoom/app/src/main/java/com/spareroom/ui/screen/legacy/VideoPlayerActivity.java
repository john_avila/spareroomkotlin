package com.spareroom.ui.screen.legacy;

import android.app.Dialog;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.*;
import android.widget.Button;
import android.widget.ImageView;

import com.spareroom.R;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.ui.screen.*;
import com.spareroom.ui.util.ConnectivityChecker;

import java.io.IOException;

import javax.inject.Inject;

import androidx.annotation.Nullable;

public class VideoPlayerActivity extends InjectableActivity {

    //region FIELDS UI

    private MediaPlayer _playbackPlayer;

    //endregion FIELDS UI

    //region FIELDS CONTROLLER

    private Uri _uriVideo;
    private boolean _isPlaying = true;

    //endregion FIELDS CONTROLLER

    //region METHODS UI

    @Inject
    ConnectivityChecker connectivityChecker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        Button bOk = findViewById(R.id.activity_video_bOk);
        Button bCancel = findViewById(R.id.activity_video_bCancel);

        _uriVideo = getIntent().getData();

        if (bOk != null)
            bOk.setOnClickListener(new OkOnClickListener());

        if (bCancel != null)
            bCancel.setOnClickListener(v -> {
                setResult(Activity.RESULT_CANCELED);
                finish();
            });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (_playbackPlayer != null) {
            _playbackPlayer.stop();
            _playbackPlayer.release();
            _playbackPlayer = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        TextureView sfVideo = findViewById(R.id.activity_video_svVideo);
        ImageView ivPlay = findViewById(R.id.activity_video_ivPlay);

        if (_playbackPlayer == null)
            _playbackPlayer = new MediaPlayer();

        if (sfVideo != null) {
            sfVideo.setSurfaceTextureListener(new VideoPlayerSurfaceTextureListener(ivPlay, _playbackPlayer));
            sfVideo.setOnClickListener(new PlayPauseOnClickListener(ivPlay, _playbackPlayer));
        }

    }

    //endregion METHODS UI

    //region INNER CLASSES

    private class OkOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (!connectivityChecker.isConnected()) {
                View vContent = LayoutInflater.from(v.getContext()).inflate(R.layout.dialog_mobile_network, null);

                Button bPositive = vContent.findViewById(R.id.dialog_bPositive);
                Button bNegative = vContent.findViewById(R.id.dialog_bNegative);

                Dialog d = new AlertDialogBuilder(vContent.getContext(), vContent).create();

                bPositive.setOnClickListener(new UploadOnClickListener(d));
                bNegative.setOnClickListener(new DialogUtil.CancelOnClickListener(d));

                d.show();
            } else {
                acceptVideo();
            }

        }

        private class UploadOnClickListener implements View.OnClickListener {
            private final Dialog _dialog;

            UploadOnClickListener(Dialog d) {
                _dialog = d;
            }

            @Override
            public void onClick(View v) {
                _dialog.dismiss();
                acceptVideo();
            }
        }

        private void acceptVideo() {
            setResult(Activity.RESULT_OK);
            finish();
        }

    }

    private class PlayPauseOnClickListener implements View.OnClickListener {
        private final ImageView _ivPlay;
        private final MediaPlayer _mediaPlayer;

        PlayPauseOnClickListener(ImageView ivPlay, MediaPlayer mediaPlayer) {
            _ivPlay = ivPlay;
            _mediaPlayer = mediaPlayer;
        }

        @Override
        public void onClick(View v) {
            if (_isPlaying) {
                _mediaPlayer.pause();
                _ivPlay.setVisibility(View.VISIBLE);
            } else {
                _mediaPlayer.start();
                _ivPlay.setVisibility(View.GONE);
            }
            _isPlaying = !_isPlaying;
        }

    }

    private class VideoPlayerSurfaceTextureListener implements TextureView.SurfaceTextureListener {
        private final MediaPlayer _mediaPlayer;
        private final ImageView _ivPlay;

        VideoPlayerSurfaceTextureListener(ImageView ivPlay, MediaPlayer mediaPlayer) {
            _mediaPlayer = mediaPlayer;
            _ivPlay = ivPlay;
        }

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            try {
                _mediaPlayer.setDataSource(getApplicationContext(), _uriVideo);
            } catch (IOException ignored) {
            }
            _mediaPlayer.setSurface(new Surface(surface));
            _mediaPlayer.setLooping(false);

            _mediaPlayer.setOnPreparedListener(mp -> {
                mp.start();
                mp.pause();
                _isPlaying = false;
            });

            _mediaPlayer.setOnCompletionListener(mp -> {
                _ivPlay.setVisibility(View.VISIBLE);
                _isPlaying = false;
            });

            _mediaPlayer.prepareAsync();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            // TODO: remove this if? This might not be needed as _mediaPlayer is the same reference the activity has
            if (_playbackPlayer != null) {
                _playbackPlayer.stop();
                _playbackPlayer.release();
                _playbackPlayer = null;
            }
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }

    }

    //endregion INNER CLASSES

}
