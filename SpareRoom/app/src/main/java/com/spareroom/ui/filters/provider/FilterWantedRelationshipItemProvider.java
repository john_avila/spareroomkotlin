package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted.Couples;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import java.util.*;

import androidx.annotation.NonNull;

class FilterWantedRelationshipItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.relationshipStatus;
    private final static String KEY_RELATIONSHIP_GROUP = "key_relationship_group";

    final static String COUPLES = "couples";
    final static String NO_COUPLES = "no_couples";
    final static String NO_PREFERENCE = "no_preference";

    private final List<GroupedCompoundButtonParams> relationshipGroup = new ArrayList<>();
    private final Map<String, List<GroupedCompoundButtonParams>> groupList = new HashMap<>();

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesWanted previousProperties = (SearchAdvertListPropertiesWanted) properties;
        List<FilterItem> list = new ArrayList<>();
        relationshipGroup.clear();
        groupList.clear();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        GroupedCompoundButtonParams couples = new GroupedCompoundButtonParams(COUPLES);
        couples.size(CompoundButtonParams.Size.MEDIUM);
        couples.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        couples.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        couples.enableHighlightBar(true);
        couples.text(application.getString(R.string.couples));
        couples.textColorResId(R.color.black);
        couples.iconResId(R.drawable.ic_couple);
        couples.value(Couples.YES.toString());
        couples.groupName(KEY_RELATIONSHIP_GROUP);
        relationshipGroup.add(couples);
        list.add(new FilterItem<>(couples));

        GroupedCompoundButtonParams noCouples = new GroupedCompoundButtonParams(NO_COUPLES);
        noCouples.size(CompoundButtonParams.Size.MEDIUM);
        noCouples.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        noCouples.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        noCouples.enableHighlightBar(true);
        noCouples.text(application.getString(R.string.noCouples));
        noCouples.textColorResId(R.color.black);
        noCouples.iconResId(R.drawable.ic_generic_user);
        noCouples.value(Couples.NO.toString());
        noCouples.groupName(KEY_RELATIONSHIP_GROUP);
        relationshipGroup.add(noCouples);
        list.add(new FilterItem<>(noCouples));

        GroupedCompoundButtonParams noPreference = new GroupedCompoundButtonParams(NO_PREFERENCE);
        noPreference.size(CompoundButtonParams.Size.MEDIUM);
        noPreference.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        noPreference.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.GRAY);
        noPreference.enableHighlightBar(false);
        noPreference.text(application.getString(R.string.noPreference));
        noPreference.textColorResId(R.color.black);
        noPreference.value(Couples.NOT_SET.toString());
        noPreference.groupName(KEY_RELATIONSHIP_GROUP);
        noPreference.defaultItem(true);
        relationshipGroup.add(noPreference);
        list.add(new FilterItem<>(noPreference));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        groupList.put(KEY_RELATIONSHIP_GROUP, relationshipGroup);
        setGroupPreviousSearch(previousProperties);

        this.list = list;
    }

    private void setGroupPreviousSearch(SearchAdvertListPropertiesWanted properties) {
        Set<String> groupListKeySet = groupList.keySet();

        for (String key : groupListKeySet) {
            List<GroupedCompoundButtonParams> group = groupList.get(key);

            for (GroupedCompoundButtonParams params : group) {
                switch (key) {
                    case KEY_RELATIONSHIP_GROUP:
                        params.selected((properties.getCouples() != Couples.NOT_SET)
                                && params.value().equals(properties.getCouples().toString()));
                        break;
                }
            }
            selectedItem(group);
        }

    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return groupList;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesWanted propertiesWanted = (SearchAdvertListPropertiesWanted) propertiesToUpdate;
        propertiesWanted.setCouples(Couples.valueOf(selectedValue(groupList.get(KEY_RELATIONSHIP_GROUP))));

    }

}
