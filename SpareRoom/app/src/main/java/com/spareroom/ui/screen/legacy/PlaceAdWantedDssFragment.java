package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.ui.widget.PlaceAdButton;

public class PlaceAdWantedDssFragment extends PlaceAdFragment {

    //region FIELDS UI CONTROLLER

    private PlaceAdButton _previouslySelected;

    //endregion FIELDS UI CONTROLLER

    //region METHODS OVERRIDDEN

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_wanted_dss, container, false);

        setTitle(getString(R.string.place_ad_wanted_dss_title));

        final PlaceAdButton bYes = rootView.findViewById(R.id.place_ad_wanted_dss_bYes);
        final PlaceAdButton bNo = rootView.findViewById(R.id.place_ad_wanted_dss_bNo);
        final DraftAdWanted draft = ((DraftAdWanted) getDraftAd());

        // In case there is an activity state change and we lost the draft
        if (draft == null) {
            getActivity().finish();

            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }

        if (draft.isDss() != null) {
            if (draft.isDss()) {
                bYes.setPreviouslySelected();
                _previouslySelected = bYes;
            } else {
                bNo.setPreviouslySelected();
                _previouslySelected = bNo;
            }
        }

        bYes.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bYes.setPreviouslySelected();
            draft.setDss(true);
            finish();
        });

        bNo.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bNo.setPreviouslySelected();
            draft.setDss(false);
            finish();
        });

        return rootView;
    } //end onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)

    //region METHODS OVERRIDDEN

}
