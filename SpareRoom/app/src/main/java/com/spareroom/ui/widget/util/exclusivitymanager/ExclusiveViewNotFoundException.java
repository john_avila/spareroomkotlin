package com.spareroom.ui.widget.util.exclusivitymanager;

/**
 * Third party library developed by Manuel Luis Padilla Mochon under Apache License, Version 2.0
 * This code was developed before his employment with Flatshare Ltd
 *
 * @author Manuel Luis Padilla Mochon
 */
public class ExclusiveViewNotFoundException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -1895651252910458783L;

}
