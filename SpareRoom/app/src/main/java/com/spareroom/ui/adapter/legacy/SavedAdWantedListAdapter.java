package com.spareroom.ui.adapter.legacy;

import android.content.Context;
import android.content.Intent;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.integration.dependency.component.ComponentRepository;
import com.spareroom.integration.imageloader.ImageLoader;
import com.spareroom.model.business.*;
import com.spareroom.ui.screen.Fragment;
import com.spareroom.ui.screen.legacy.AdActivity;
import com.spareroom.ui.util.StringUtils;

import javax.inject.Inject;

public class SavedAdWantedListAdapter extends BaseAdapter {
    private final AdBoard _results;
    private final Fragment _fragment;
    private final int imageSize;

    @Inject
    ImageLoader _imageLoader;

    public SavedAdWantedListAdapter(Fragment fragment, AdBoard objects) {
        ComponentRepository.get().getAppComponent().inject(this);
        _fragment = fragment;
        _results = objects;
        imageSize = fragment.getResources().getDimensionPixelSize(R.dimen.conversationHeaderAvatarSize);
    }

    @Override
    public int getCount() {
        return _results.get_board().size();
    }

    @Override
    public Object getItem(int position) {
        if (position < _results.get_board().size())
            return _results.get_board().get(position);
        else if (position == _results.get_board().size())
            return getTitleString(_fragment.getActivity()); // title
        else if (position > _results.get_board().size())
            return _results.get_broadenSearch().get(position - _results.get_board().size() - 1);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < _results.get_board().size())
            return _results.get_board().get(position).getKind();
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    private View getAd(int position, View convertView, ViewGroup parent) {
        AdWanted ad = (AdWanted) _results.get_board().get(position);
        int resPic = R.drawable.placeholder_wanted;
        int resKind;
        View view = convertView;
        ImageView ivPicture;
        TextView tvLocation;
        TextView tvRoom;
        TextView tvPrice;
        TextView tvDescription;
        ImageView ivTag;

        if (view == null) {
            final LayoutInflater vi = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (ad.isBold()) {
                view = vi.inflate(R.layout.results_list_item_bold, parent, false);
            } else if (ad.isFeatured()) {
                view = vi.inflate(R.layout.results_list_item_featured, parent, false);
            } else {
                view = vi.inflate(R.layout.results_list_item_normal, parent, false);
            }
            view.findViewById(R.id.activity_results_result_ivSaved).setVisibility(View.VISIBLE);
        }

        if (ad.isBold()) {
            ivPicture = view.findViewById(R.id.activity_results_result_bold_ivPicture);
            tvLocation = view.findViewById(R.id.activity_results_result_bold_tvLocation);
            tvRoom = view.findViewById(R.id.activity_results_result_bold_tvRoom);
            tvPrice = view.findViewById(R.id.activity_results_result_bold_tvPrice);
            tvDescription = view.findViewById(R.id.activity_results_result_bold_tvDescription);
            ivTag = view.findViewById(R.id.activity_results_result_bold_ivTag);
            resKind = R.drawable.ad_status_free_to_contact_blue;
        } else if (ad.isFeatured()) {
            ivPicture = view.findViewById(R.id.activity_results_result_featured_ivPicture);
            tvLocation = view.findViewById(R.id.activity_results_result_featured_tvLocation);
            tvRoom = view.findViewById(R.id.activity_results_result_featured_tvRoom);
            tvPrice = view.findViewById(R.id.activity_results_result_featured_tvPrice);
            tvDescription = view.findViewById(R.id.activity_results_result_featured_tvDescription);
            ivTag = view.findViewById(R.id.activity_results_result_featured_ivTag);

            resKind = R.drawable.ad_status_free_to_contact_yellow;
        } else {
            ivPicture = view.findViewById(R.id.activity_results_result_normal_ivPicture);
            tvLocation = view.findViewById(R.id.activity_results_result_normal_tvLocation);
            tvRoom = view.findViewById(R.id.activity_results_result_normal_tvRoom);
            tvPrice = view.findViewById(R.id.activity_results_result_normal_tvPrice);
            tvDescription = view.findViewById(R.id.activity_results_result_normal_tvDescription);
            ivTag = view.findViewById(R.id.activity_results_result_normal_ivTag);

            resKind = R.drawable.ad_status_free_to_contact_grey;
        }

        tvLocation.setText(ad.getWantedAdvertiserDesc());
        tvRoom.setText(ad.getRoomSize());
        tvPrice.setText(ad.getPrice());
        tvDescription.setText(ad.getShortDescription());

        final String largePictureUrl = ad.getLargePictureUrl();
        if (!StringUtils.isNullOrEmpty(largePictureUrl)) {
            _imageLoader.loadImage(largePictureUrl + "?square=" + imageSize, ivPicture);
        } else
            ivPicture.setImageResource(resPic);

        if (ad.getContacted())
            ivTag.setImageResource(R.drawable.ad_status_contacted);
        else {
            if (ad.getEarlyBirdRequired()) {
                ivTag.setImageResource(R.drawable.ad_status_early_bird);
            } else {
                ivTag.setImageResource(resKind);
            }
        }
        view.setOnClickListener(new AdOnClickListener(ad));

        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position < _results.get_board().size())
            return getAd(position, convertView, parent);
        return null;
    }

    public void add(AdWanted ad) {
        _results.get_board().add(ad);
    }

    private class AdOnClickListener implements OnClickListener {
        private final AdWanted _ad;

        private AdOnClickListener(AdWanted ad) {
            _ad = ad;
        }

        @Override
        public void onClick(View v) {
            Context c = _fragment.getActivity();
            Intent adActivity = new Intent();
            adActivity.setClass(c, AdActivity.class);
            adActivity.putExtra("id", _ad.getId());
            adActivity.putExtra("searchType", SearchType.WANTED);
            _fragment.getActivity().startActivityForResult(adActivity, 32);
        }
    }

    private String getTitleString(Context context) {
        int boardSize = _results.get_total();
        return String.format(
                AppVersion.flavor().getLocale(),
                "%s %d %s, %s",
                context.getString(R.string.only),
                boardSize,
                context.getResources().getQuantityString(R.plurals.results, boardSize),
                context.getString(R.string.try_adjusting_your_criteria));
    }

}