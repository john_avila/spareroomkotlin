package com.spareroom.ui.screen.legacy;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.controller.legacy.*;
import com.spareroom.model.business.*;
import com.spareroom.ui.adapter.legacy.ManagePhotoProfileAdapter;
import com.spareroom.ui.screen.Activity;
import com.spareroom.ui.screen.AlertDialogBuilder;
import com.spareroom.ui.util.*;

import java.io.File;
import java.util.Locale;

import androidx.annotation.NonNull;

public class ManagePhotoProfileActivity extends Activity {

    private static final String CAMERA_FILE_URI = "CAMERA_FILE_URI";

    private AsyncTaskManager _asyncManager;
    private String _id;

    private View _bAdd;
    private ProgressBar _pb;
    private int _maxPhotos;

    private static final int PHOTO_REQUEST = 50;
    private static final int CAMERA_REQUEST = 51;
    private static final int PHOTO_CAMERA_PERMISSIONS_REQUEST = 61;
    private String fileUri;

    public static final String EXTRA_KEY_PHOTO_UPLOAD_LIMIT = "photo_upload_limit";
    public static final String EXTRA_KEY_FLATSHARE_ID = "flatshare_id";
    public static final String EXTRA_KEY_FLATSHARE_TYPE = "flatshare_type";
    public static final String EXTRA_KEY_AT_RETRIEVE = "at_retrieve";
    public static final String EXTRA_KEY_AT_EDIT_CAPTION = "at_editcaption";
    public static final String EXTRA_KEY_AT_DELETE = "at_delete";
    public static final String EXTRA_KEY_AT_CHANGE_ORDER = "at_changeorder";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _maxPhotos = getIntent().getIntExtra(EXTRA_KEY_PHOTO_UPLOAD_LIMIT, 5); // The 5 works for photo profiles
        _id = getIntent().getStringExtra(EXTRA_KEY_FLATSHARE_ID);
        _asyncManager = new AsyncTaskManager();

        fileUri = savedInstanceState == null ? null : savedInstanceState.getString(CAMERA_FILE_URI);

        setContentView(R.layout.activity_manage_photo_profile);

        setToolbar();

        _bAdd = findViewById(R.id.activity_manage_bAddPicture);
        _pb = findViewById(R.id.activity_manage_photos_pb);

        _bAdd.setOnClickListener(v -> addManagePhoto());

        launchPhotosTask();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(CAMERA_FILE_URI, fileUri);
    }

    private void addManagePhoto() {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_profile_picture_source, null);

        Dialog d = new AlertDialogBuilder(this, view).create();

        TextView tvGallery = view.findViewById(R.id.dialog_tvGallery);
        tvGallery.setCompoundDrawablesWithIntrinsicBounds(
                UiUtils.getTintedDrawable(this, R.drawable.ic_photos, R.color.material_deep_sea_blue), null, null, null);
        tvGallery.setOnClickListener(new GalleryOnClickListener(d));

        TextView tvCamera = view.findViewById(R.id.dialog_tvCamera);
        tvCamera.setCompoundDrawablesWithIntrinsicBounds(
                UiUtils.getTintedDrawable(this, R.drawable.ic_camera, R.color.material_deep_sea_blue), null, null, null);
        tvCamera.setOnClickListener(new CameraOnClickListener(d));

        d.show();
    }

    public final AsyncTask<Parameters, Integer, Object> instantiateRetrievePicturesAsyncTask() {
        return new RetrievePhotosAsyncTask(
                SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(),
                retrievePicturesAsyncResult);
    }

    public final AsyncTask<Parameters, Integer, Object> instantiateEditCaptionAsyncTask() {
        return new ChangeOrderPictureAsyncTask(
                SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(),
                SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                updateAsyncResult);
    }

    public final AsyncTask<Parameters, Integer, Object> instantiateDeletePictureAsyncTask() {
        return new DeletePictureAsyncTask(SpareroomApplication.getInstance(
                getApplicationContext()).getAccountFacade(),
                SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                deleteAsyncResult);
    }

    public final AsyncTask<Parameters, Integer, Object> instantiateChangeOrderAsyncTask() {
        return new ChangeOrderPictureAsyncTask(
                SpareroomApplication.getInstance(getApplicationContext()).getAccountFacade(),
                SpareroomApplication.getInstance(getApplicationContext()).getSpareroomContext(),
                updateAsyncResult);
    }

    @Override
    protected void onStop() {
        super.onStop();
        _asyncManager.cancelAll();
        ManagePhotoProfileAdapter adapter = (ManagePhotoProfileAdapter) ((ListView) findViewById(R.id.activity_manage_photos_lv)).getAdapter();
        if (adapter != null)
            adapter.stop();

    }

    private void launchPhotosTask() {
        Parameters p = new Parameters();
        if (_id != null)
            p.add(EXTRA_KEY_FLATSHARE_ID, _id);

        _pb.setVisibility(View.VISIBLE);
        AsyncTask<Parameters, Integer, Object> at = instantiateRetrievePicturesAsyncTask();
        _asyncManager.register(at);
        at.execute(p);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == PHOTO_REQUEST || requestCode == CAMERA_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                _pb.setVisibility(View.VISIBLE);

                NewPicture picture = new NewPicture();
                picture.setFlatshareId(_id);
                picture.setUri(requestCode == PHOTO_REQUEST ? intent.getData().toString() : fileUri);

                UploadPictureControllerAction uploadPictureControllerAction = UploadPictureControllerAction.get(new UploadPictureObserver());
                addObserver(uploadPictureControllerAction);
                uploadPictureControllerAction.execute(picture);
            }
        }
    }

    private final class UploadPictureObserver implements ControllerActionObserver {

        @Override
        public void onResult(Object o) {
            hideProgressbar();
            setResult(Activity.RESULT_OK);

            ToastUtils.showToast(R.string.activity_manage_tPictureUploaded);
            launchPhotosTask();
        }

        @Override
        public void onSpareroomStatus(SpareroomStatus status) {
            hideProgressbar();
            ToastUtils.showToast(status.getMessage());
        }

        @Override
        public void onException(Exception exception) {
            hideProgressbar();
            ToastUtils.showToast(R.string.activity_manage_tPictureUploadedError);
        }

        private void hideProgressbar() {
            _pb.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PHOTO_CAMERA_PERMISSIONS_REQUEST) {
            if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED) && (grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                launchCamera();
            } else {
                Toast.makeText(this, getString(R.string.permission_denied), Toast.LENGTH_LONG).show();
            }
        }
    }

    private final IAsyncResult retrievePicturesAsyncResult = new IAsyncResultImpl() {
        @Override
        public void update(Object o) {
            _pb.setVisibility(View.GONE);
            ManagePhotoProfileActivity.this.setResult(Activity.RESULT_OK);

            PictureGallery gallery = (PictureGallery) o;

            ListView lv = findViewById(R.id.activity_manage_photos_lv);

            int photosCount = gallery.get_gallery().size();
            String photos = getResources().getQuantityString(R.plurals.photos, photosCount);
            setSubtitle(String.format(Locale.getDefault(), "%s %s %s", photosCount, photos, getString(R.string.outOf, _maxPhotos)));

            if (lv != null) {
                ManagePhotoProfileAdapter adapter = (ManagePhotoProfileAdapter) lv.getAdapter();
                if (adapter != null)
                    adapter.stop();
                lv.setAdapter(new ManagePhotoProfileAdapter(ManagePhotoProfileActivity.this, gallery, _id, getIntent().getStringExtra(EXTRA_KEY_FLATSHARE_TYPE)));
            }

            _bAdd.setVisibility(photosCount < _maxPhotos ? View.VISIBLE : View.GONE);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.delete_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_item_delete:
                ListView lv = findViewById(R.id.activity_manage_photos_lv);
                if (lv != null && lv.getAdapter() != null)
                    ((ManagePhotoProfileAdapter) lv.getAdapter()).editMode();
        }
        return super.onOptionsItemSelected(item);
    }

    private final IAsyncResult deleteAsyncResult = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            ManagePhotoProfileActivity.this.setResult(Activity.RESULT_OK);
            Toast.makeText(ManagePhotoProfileActivity.this, getString(R.string.activity_manage_tPictureDeleted), Toast.LENGTH_LONG).show();
            launchPhotosTask();
        }
    };

    private final IAsyncResult updateAsyncResult = new IAsyncResultImpl() {

        @Override
        public void update(Object o) {
            ManagePhotoProfileActivity.this.setResult(Activity.RESULT_OK);
            Toast.makeText(ManagePhotoProfileActivity.this, getString(R.string.activity_manage_tPictureUpdated), Toast.LENGTH_LONG).show();
            launchPhotosTask();
        }
    };

    private void launchGallery() {
        Launcher.launchGallery(this, PHOTO_REQUEST);
    }

    private void launchCamera() {
        File mediaFile = FileUtils.createNewFileForPhoto();
        fileUri = Uri.fromFile(mediaFile).toString();
        Launcher.launchCamera(this, CAMERA_REQUEST, mediaFile);
    }

    //region INNER CLASSES

    private class GalleryOnClickListener implements OnClickListener {
        private final Dialog _dialog;

        private GalleryOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            launchGallery();

            _dialog.dismiss();

        }

    } //end class GalleryOnClickListener

    private class CameraOnClickListener implements OnClickListener {
        private final Dialog _dialog;

        private CameraOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            if (PermissionUtils.hasCameraPermissions(ManagePhotoProfileActivity.this)) {
                launchCamera();
            } else {
                PermissionUtils.requestCameraPermissions(ManagePhotoProfileActivity.this, PHOTO_CAMERA_PERMISSIONS_REQUEST);
            }

            _dialog.dismiss();

        }

    } //end class CameraOnClickListener

    //endregion INNER CLASSES

}
