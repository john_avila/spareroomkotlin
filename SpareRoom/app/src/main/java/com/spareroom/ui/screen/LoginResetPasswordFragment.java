package com.spareroom.ui.screen;

import android.os.Bundle;
import android.view.*;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.lib.core.IObservable;
import com.spareroom.lib.core.IObserver;
import com.spareroom.model.business.AccountCredentials;
import com.spareroom.model.business.SpareroomStatus;
import com.spareroom.ui.EmailFieldValidator;
import com.spareroom.ui.controller.ActionUiController;
import com.spareroom.ui.controller.ViewUiController;
import com.spareroom.ui.util.*;
import com.spareroom.ui.widget.TextBox;

import java.util.LinkedList;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

/**
 * Fragment for reset the password.
 */
public class LoginResetPasswordFragment extends Fragment {

    //region METHODS LIFECYCLE

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        // Account credentials
        AccountCredentials accountCredentials =
                (AccountCredentials) ((LinearMultiStepActivity) getActivity()).getSharedObject(LoginActivity.CREDENTIALS);
        // Views
        View rootView = inflater.inflate(R.layout.material_reset_password_fragment, container, false);
        // Email input
        TextBox tbEmail = rootView.findViewById(R.id.resetPasswordFragment_email);
        // Bottom Toolbar
        View bottomToolbar = getActivity().findViewById(R.id.activity_toolbarBottom);
        TextView tvResetButton = bottomToolbar.findViewById(R.id.materialToolbarBottom_rightText);

        // UI Controllers
        ViewsCompositeUiController viewsCompositeUiController =
                new ViewsCompositeUiController(new EmailTextBoxUiController(tbEmail), new ResetButtonUiController(bottomToolbar));
        TextBoxOnContentChangedListener textBoxOnContentChangedListener =
                new TextBoxOnContentChangedListener(viewsCompositeUiController);
        AccountCredentialsObserver accountCredentialsObserver =
                new AccountCredentialsObserver(accountCredentials, (LinearMultiStepActivity) getActivity());

        // Email input
        textBoxOnContentChangedListener.addObserver(accountCredentialsObserver);
        tbEmail.setOnContentChangedListener(textBoxOnContentChangedListener);

        // Toolbar
        setToolbar(getActivity().findViewById(R.id.appBarLayout), getString(R.string.reset), true);

        // Bottom Toolbar
        bottomToolbar.setVisibility(View.VISIBLE);

        tvResetButton.setOnClickListener(new ResetButtonOnClickListener(accountCredentials, tbEmail, viewsCompositeUiController));
        tvResetButton.setText(getString(R.string.resetPasswordFragment_bottomToolbar));

        // Account credentials, loading previous values
        if (accountCredentials != null) {
            tbEmail.setText(accountCredentials.getUsername());
        }

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((LoginActivity) getActivity()).showKeyboard();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ((LinearMultiStepActivity) getActivity()).onNavigateBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //endregion METHODS LIFECYCLE

    //region INNER CLASS

    /**
     * Observer for updating the account credentials in all its scopes.
     */
    private class AccountCredentialsObserver implements IObserver {
        private final AccountCredentials _accountCredentials;
        private final LinearMultiStepActivity _linearMultiStepActivity;

        AccountCredentialsObserver(AccountCredentials accountCredentials, LinearMultiStepActivity linearMultiStepActivity) {
            _linearMultiStepActivity = linearMultiStepActivity;
            _accountCredentials = accountCredentials;
        }

        @Override
        public void notifyObserver(Object o) {
            _accountCredentials.setUsername((String) o);
            _linearMultiStepActivity.setSharedObject(LoginActivity.CREDENTIALS, _accountCredentials);
        }

    }

    /**
     * Composite controller that updates all UI widgets involved in the process
     */
    private class ViewsCompositeUiController implements ViewUiController, ActionUiController {
        private final EmailTextBoxUiController _emailTextBoxUiController;
        private final ResetButtonUiController _resetButtonUiController;

        ViewsCompositeUiController(EmailTextBoxUiController emailTextBoxUiController, ResetButtonUiController resetButtonUiController) {
            _emailTextBoxUiController = emailTextBoxUiController;
            _resetButtonUiController = resetButtonUiController;
        }

        /**
         * Clears the error message, if needed, and disable the button
         */
        @Override
        public void disable() {
            _emailTextBoxUiController.enable();
            _resetButtonUiController.disable();
        }

        /**
         * Clears the error message, if needed, and enable the button
         */
        @Override
        public void enable() {
            _emailTextBoxUiController.enable();
            _resetButtonUiController.enable();
        }

        /**
         * Sets the views in "loading" mode
         */
        @Override
        public void setLoading() {
            _emailTextBoxUiController.setLoading();
            _resetButtonUiController.setLoading();
        }

        /**
         * Sets a status for this view (normally an error for fields that require validation)
         *
         * @param error a <code>SparerooomStatus</code> status
         */
        @Override
        public void setStatus(SpareroomStatus error) {

        }

        /**
         * Clears the status for this view
         */
        @Override
        public void clearStatus() {

        }

        /**
         * Sets the views in the successful reset process status
         */
        @Override
        public void setResult(Object... o) {
            _emailTextBoxUiController.enable();
            _resetButtonUiController.setResult(o);
        }

        /**
         * Sets the views in the proper status when there is a non successful reset for any reason
         * (API response, malformed email,...)
         */
        @Override
        public void setSpareroomStatus(SpareroomStatus spareroomStatus) {
            _emailTextBoxUiController.setStatus(spareroomStatus);
            _resetButtonUiController.setSpareroomStatus(spareroomStatus);
        }

        /**
         * Sets the views in the unexpected error status
         */
        @Override
        public void setException(Exception exception) {
            SnackBarUtils.showException(LoginResetPasswordFragment.this.getView(), exception);
            enable();
        }

    }

    /**
     * It is used for setting the different status for the ResetPassword button
     */
    private class ResetButtonUiController implements ViewUiController, ActionUiController {
        private final TextView _tvResetButton;
        private final ProgressBar _pbLoading;

        ResetButtonUiController(View bottomToolbar) {
            _tvResetButton = bottomToolbar.findViewById(R.id.materialToolbarBottom_rightText);
            _pbLoading = bottomToolbar.findViewById(R.id.materialToolbarBottom_loading);
        }

        /**
         * Sets the initial status: disable the button
         */
        @Override
        public void disable() {
            _tvResetButton.setEnabled(false);
            _tvResetButton.setVisibility(View.VISIBLE);
            _tvResetButton.setTextColor(ContextCompat.getColor(_tvResetButton.getContext(), R.color.light_blue));

            _pbLoading.setVisibility(View.GONE);

        }

        /**
         * Sets the button ready for pressing the it
         */
        @Override
        public void enable() {
            _tvResetButton.setEnabled(true);
            _tvResetButton.setVisibility(View.VISIBLE);
            _tvResetButton.setTextColor(ContextCompat.getColor(_tvResetButton.getContext(), R.color.white));

            _pbLoading.setVisibility(View.GONE);
        }

        /**
         * Sets the button in "loading" mode until the reset password response
         */
        @Override
        public void setLoading() {
            _tvResetButton.setEnabled(false);
            _tvResetButton.setVisibility(View.INVISIBLE);

            _pbLoading.setVisibility(View.VISIBLE);
        }

        /**
         * Sets a status for this view (normally an error for fields that require validation)
         *
         * @param status a <code>SpareroomStatus</code> status
         */
        @Override
        public void setStatus(SpareroomStatus status) {

        }

        /**
         * Clears the status for this view
         */
        @Override
        public void clearStatus() {

        }

        /**
         * Warns the user when the reset process has been successfully finished
         */
        @Override
        public void setResult(Object... o) {
            enable();
            Toast.makeText(getActivity(), getResources().getString(R.string.resetPasswordFragment_requestSent), Toast.LENGTH_LONG).show();
        }

        /**
         * Warns the user for non successful reset for any reason (API response, malformed email,...)
         */
        @Override
        public void setSpareroomStatus(SpareroomStatus spareroomStatus) {
            enable();
            AnimationUtils.INSTANCE.shakeLeft(_tvResetButton);

        }

        /**
         * Shows a Toast warning about an unexpected error when trying to reset the password
         */
        @Override
        public void setException(Exception exception) {
            enable();
            Toast.makeText(getActivity(), getString(R.string.exceptionMessage), Toast.LENGTH_LONG).show();
        }

    } //end class ResetPasswordButtonController

    /**
     * It is used for setting the the email TextBox status.
     */
    private class EmailTextBoxUiController implements ViewUiController {
        private final TextBox _tbEmail;

        EmailTextBoxUiController(TextBox tbEmail) {
            _tbEmail = tbEmail;
        }

        @Override
        public void enable() {
            if (!_tbEmail.isEnabled())
                _tbEmail.setEnabled(true);
        }

        /**
         * Locks the TextBox while performing the operation
         */
        @Override
        public void disable() {
            _tbEmail.setEnabled(false);
            KeyboardUtils.hideKeyboard(_tbEmail);

        }

        /**
         * Locks the TextBox while performing the operation. (It calls {@link #disable()})
         */
        @Override
        public void setLoading() {
            disable();
        }

        /**
         * Sets a status for this view (normally an error for fields that require validation)
         *
         * @param status a <code>SparerooomStatus</code> status
         */
        @Override
        public void setStatus(SpareroomStatus status) {
            _tbEmail.setMessageText((status != null) ? status.getMessage() : getString(R.string.loginEmailFragment_errorGeneric));
            _tbEmail.showMessage();
        }

        /**
         * Clears the status for this view
         */
        @Override
        public void clearStatus() {

        }

    } //end class ResetPasswordUiController

    private class ResetButtonOnClickListener implements View.OnClickListener {
        private final AccountCredentials _accountCredentials;
        private final TextBox _etEmail;
        private final ViewsCompositeUiController _viewsCompositeUiController;
        private final EmailFieldValidator _emailFieldValidator;

        ResetButtonOnClickListener(AccountCredentials accountCredentials, TextBox tbEmail, ViewsCompositeUiController viewsCompositeUiController) {
            _accountCredentials = accountCredentials;
            _etEmail = tbEmail;
            _viewsCompositeUiController = viewsCompositeUiController;
            _emailFieldValidator = new EmailFieldValidator();
        }

        @Override
        public void onClick(View v) {
            int emailValidationState = _emailFieldValidator.validate(_etEmail.getText());

            _viewsCompositeUiController.setLoading();

            if (emailValidationState != EmailFieldValidator.VALID) {

                switch (emailValidationState) {
                    case EmailFieldValidator.MALFORMED_ADDRESS:
                        contentMalformed(emailValidationState);
                        break;
                }

            } else {
                launchResetPassword(_accountCredentials);
            }

        }

        /**
         * Does the corresponding actions when the TextBox content is malformed
         */
        private void contentMalformed(int emailValidationState) {
            SpareroomStatus spareroomStatus = new SpareroomStatus();
            spareroomStatus.setCode(Integer.toString(emailValidationState));
            spareroomStatus.setMessage(getString(R.string.resetPasswordFragment_errorMessage));

            _viewsCompositeUiController.setSpareroomStatus(spareroomStatus);

        }

        /**
         * Does the reset request to the API
         */
        private void launchResetPassword(AccountCredentials accountCredentials) {
            AnalyticsTrackerObserver analyticsTrackerObserver = new AnalyticsTrackerObserver();
            ResponseObserver responseObserver = new ResponseObserver();
            ResetPasswordControllerAction resetPasswordControllerAction =
                    new ResetPasswordControllerAction(SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext());

            resetPasswordControllerAction.addControllerActionObserver(responseObserver);
            resetPasswordControllerAction.addControllerActionObserver(analyticsTrackerObserver);
            addObserver(resetPasswordControllerAction);
            resetPasswordControllerAction.execute(accountCredentials, null);

        } //end launchResetPassword(AccountCredentials accountCredentials)

        /**
         * Controller for the AnalyticsTracker
         */
        private class AnalyticsTrackerObserver implements ControllerActionObserver {

            @Override
            public void onResult(Object o) {
                AnalyticsTrackerComposite.getInstance().trackEvent_Account_ForgotPassword();

            }

            @Override
            public void onSpareroomStatus(SpareroomStatus status) {
                AnalyticsTrackerComposite.getInstance().trackEvent_Account_ForgotPassword_Failed(status);

            }

            @Override
            public void onException(Exception exception) {
            }

        } //end class AnalyticsTrackerObserver

        /**
         * Controller for the API responses
         */
        private class ResponseObserver implements ControllerActionObserver {

            /**
             * Does the corresponding actions when the TextBox content is Ok and the API response is successful
             */
            @Override
            public void onResult(Object o) {
                _viewsCompositeUiController.setResult(o);
                ((LinearMultiStepActivity) getActivity()).goToSpecificFragment(LoginMethodFragment.class.getName());
            }

            /**
             * Does the corresponding actions when the TextBox content is Ok and the API response is an exception
             */
            @Override
            public void onSpareroomStatus(SpareroomStatus status) {
                _viewsCompositeUiController.setSpareroomStatus(status);
            }

            /**
             * Does the corresponding actions when the TextBox content is Ok and the API response is an error
             */
            @Override
            public void onException(Exception exception) {
                _viewsCompositeUiController.setException(exception);
            }

        }

    } //end class ResetButtonOnClickListener

    private class TextBoxOnContentChangedListener implements TextBox.OnContentChangedListener, IObservable {
        private final ViewsCompositeUiController _viewsCompositeUiController;
        private final LinkedList<IObserver> _observerList = new LinkedList<>();

        TextBoxOnContentChangedListener(ViewsCompositeUiController viewsCompositeUiController) {
            _viewsCompositeUiController = viewsCompositeUiController;
        }

        @Override
        public void onContentChangedEvent(CharSequence s) {
            if (s.length() > 0)
                _viewsCompositeUiController.enable();
            else
                _viewsCompositeUiController.disable();

            for (IObserver observer : _observerList)
                observer.notifyObserver(s.toString());

        }

        @Override
        public void addObserver(IObserver o) {
            _observerList.add(o);
        }

        @Override
        public void removeObserver(IObserver o) {
            _observerList.remove(o);
        }

    } //end class TextBoxOnContentChangedListener

    //endregion INNER CLASS

}
