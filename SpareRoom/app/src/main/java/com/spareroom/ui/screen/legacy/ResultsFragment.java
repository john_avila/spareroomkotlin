package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

import com.spareroom.R;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.model.business.*;
import com.spareroom.ui.IScrollableContainer;
import com.spareroom.ui.adapter.legacy.AdListAdapter;
import com.spareroom.ui.util.ToastUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.ListFragment;

public class ResultsFragment extends ListFragment {
    public static final String TAG = ResultsFragment.class.getSimpleName() + "TAG";

    private BoardOnScrollListener _scrollListener = new BoardOnScrollListener();
    private SearchType searchType;
    private AdListAdapter adapter;
    private static final String SEARCH_TYPE = "search_type";

    public static ResultsFragment newInstance(SearchType searchType) {
        Bundle bundle = new Bundle();
        ResultsFragment resultsFragment = new ResultsFragment();

        bundle.putSerializable(SEARCH_TYPE, searchType);
        resultsFragment.setArguments(bundle);

        return resultsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AnalyticsTrackerComposite.getInstance().trackScreen_Fragment(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        searchType = (SearchType) getArguments().get(SEARCH_TYPE);
        getListView().setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
        getListView().setDividerHeight(0);
        setListAdapter(adapter = new AdListAdapter((ResultsActivity) getActivity(), new AdBoard(), searchType));
    }

    public void empty() {
        disableScroll();
        setListAdapter(adapter = new AdListAdapter((ResultsActivity) getActivity(), new AdBoard(), searchType));
    }

    public void update(AdBoard board, boolean clearAdapter) {
        disableScroll();

        if (clearAdapter) {
            adapter.set_results(new AdBoard());
            _scrollListener.clearLastSearch();
        }

        for (AbstractAd ad : board.get_board()) {
            try {
                if (searchType == SearchType.OFFERED) {
                    adapter.add((AdOffered) ad);
                } else {
                    adapter.add((AdWanted) ad);
                }

            } catch (Exception e) {
                ToastUtils.showToast(R.string.amend_search);

                trackClassCastException(e, ad.getId());

                assert getActivity() != null;
                getActivity().finish();
            }

        }
        if (((board.get_broadenSearch()) != null) && (board.get_broadenSearch().size() > 0))
            adapter.setBroadenSearch(board.get_broadenSearch());

        adapter.get_results().set_total(board.get_total());

        enableScroll();

        adapter.notifyDataSetChanged();

        if (clearAdapter)
            getListView().setSelection(0);
    }

    private void trackClassCastException(Exception e, String advertId) {
        assert getActivity() != null;
        ResultsActivity activity = (ResultsActivity) getActivity();
        AnalyticsTrackerComposite.getInstance().leaveBreadcrumb(
                String.format(
                        "Advert id: %s; ResultsFragment searchType: %s; Intended searchType: %s; %s",
                        advertId,
                        searchType,
                        activity.getIntent().getSerializableExtra("type"),
                        activity.breadcrumbs()));
        AnalyticsTrackerComposite.getInstance().logHandledException("Exception filling ResultsActivity", e);
    }

    private void disableScroll() {
        getListView().setOnScrollListener(null);
    }

    private void enableScroll() {
        getListView().setOnScrollListener(_scrollListener);
    }

    private class BoardOnScrollListener implements OnScrollListener {
        private int _lastSearch;
        private int _lastVisibleItem;
        private int _totalItemCount;

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            _lastVisibleItem = firstVisibleItem + visibleItemCount;
            _totalItemCount = totalItemCount;
        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

            if (scrollState == SCROLL_STATE_IDLE) {
                if (_lastSearch < adapter.getCount()) {
                    _lastSearch = adapter.getCount();
                }

                // Reached the bottom of the list && Reached the last result
                if (_lastVisibleItem == _totalItemCount
                        && _lastSearch < adapter.get_results().get_total()
                        && adapter.get_results().get_broadenSearch().size() == 0) {
                    ((IScrollableContainer) getActivity()).onMoreResultsRequest();
                }
            }
        }

        private void clearLastSearch() {
            _lastSearch = 0;
        }
    }

    public void markStatus(String adId, int status) {
        adapter.get_results().markStatus(adId, status);
        adapter.notifyDataSetChanged();
    }

}
