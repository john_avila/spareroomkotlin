package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.widget.*;
import android.widget.AbsListView.OnScrollListener;

import com.spareroom.R;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.controller.legacy.*;
import com.spareroom.integration.business.legacy.rest.apiv1.SearchParamEnum;
import com.spareroom.model.business.*;
import com.spareroom.ui.IScrollableContainer;
import com.spareroom.ui.adapter.legacy.SavedAdOfferedListAdapter;
import com.spareroom.ui.screen.Fragment;
import com.spareroom.ui.util.ToastUtils;

import java.util.Calendar;
import java.util.GregorianCalendar;

import androidx.core.content.ContextCompat;

public class SavedAdOfferedFragment extends Fragment implements IAsyncResult, IScrollableContainer {

    private ListView listView;
    private ProgressBar progressBar;
    private TextView textViewEmptyText;

    private AdBoard _currentBoard;
    private AsyncTaskManager _asyncManager;

    private int _page;
    private int _firstMaxResults;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        launchSavedAdOfferedSearch();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.ad_list_fragment, container, false);
        listView = rootView.findViewById(R.id.adListFragment_list);
        progressBar = rootView.findViewById(R.id.adListFragment_progressBar);
        textViewEmptyText = rootView.findViewById(R.id.adListFragment_emptyText);

        _asyncManager = new AsyncTaskManager();
        _page = 1;

        float screenHeight = SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext().get_screenHeight();
        float maxResults = screenHeight / 80;
        int firstPages = (int) Math.floor((maxResults / 10) + 1);

        _firstMaxResults = firstPages * 10;
        _currentBoard = new AdBoard();

        listView.setAdapter(new SavedAdOfferedListAdapter(this, _currentBoard));

        listView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
        listView.setDividerHeight(0);

        return rootView;
    }

    public void resetSearch() {
        empty();
        launchSavedAdOfferedSearch();
    }

    private void launchSavedAdOfferedSearch() {
        SpareroomApplication app = SpareroomApplication.getInstance(getActivity().getApplicationContext());

        SavedAdListAsyncTask at = new SavedAdListAsyncTask(app.getSearchFacade(), this, SearchType.OFFERED);
        Parameters p = new Parameters();
        p.add(SearchParamEnum.PAGE, Integer.toString(_page));
        p.add(SearchParamEnum.MAX_PER_PAGE, Integer.toString((_page == 1) ? _firstMaxResults : 10));

        _asyncManager.register(at);
        at.execute(p);
    }

    @Override
    public void onStop() {
        _asyncManager.cancelAll();
        super.onStop();
    }

    public void empty() {
        listView.setOnScrollListener(null);
        listView.setAdapter(new SavedAdOfferedListAdapter(this, new AdBoard()));
    }

    private class AdOfferedBoardOnScrollListener implements OnScrollListener {
        private int _lastSearch;
        private int _lastVisibleItem;
        private int _totalItemCount;

        AdOfferedBoardOnScrollListener() {
            super();
            _lastSearch = 0;
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            _lastVisibleItem = firstVisibleItem + visibleItemCount;
            _totalItemCount = totalItemCount;
        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

            if (scrollState == SCROLL_STATE_IDLE) {
                if (_lastSearch < listView.getAdapter().getCount())
                    _lastSearch = listView.getAdapter().getCount();

                // (Reached the bottom of the list) && (Reached the last result)
                if ((_lastVisibleItem == _totalItemCount) && (_lastSearch < _currentBoard.get_total()))
                    (SavedAdOfferedFragment.this).onMoreResultsRequest();
            }
        }
    }

    @Override
    public void update(Object o) {
        showProgressBar(false);

        if (o instanceof AdBoard) {
            AdBoard board = (AdBoard) o;

            if (board.get_total() > 0) {
                _currentBoard = board;

                listView.setOnScrollListener(null);
                SavedAdOfferedListAdapter adapter = (SavedAdOfferedListAdapter) listView.getAdapter();

                for (AbstractAd ad : board.get_board())
                    adapter.add((AdOffered) ad);

                adapter.notifyDataSetChanged();
                listView.setOnScrollListener(new AdOfferedBoardOnScrollListener());
            } else {
                textViewEmptyText.setText(getString(R.string.fragment_savedAdOffered_tvEmpty));
                textViewEmptyText.setVisibility(View.VISIBLE);
            }

        } else if (o instanceof SpareroomStatus) {
            if (((SpareroomStatus) o).getCode().equals(SpareroomStatusCode.NOT_LOGGED_IN))
                SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext().deleteUserSession();

            Toast.makeText(getActivity(), ((SpareroomStatus) o).getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onMoreResultsRequest() {
        _page++;
        launchSavedAdOfferedSearch();
    }

    @Override
    public void handleInvalidUserException(String message) {
        showProgressBar(false);
    }

    @Override
    public void handleMissingParameterException(String message) {
        showProgressBar(false);
        ToastUtils.showToast(R.string.dialogUpdate_tvMessage);
    }

    @Override
    public void handleMissingSystemFeatureException(String message) {
        showProgressBar(false);
        ToastUtils.showToast(R.string.dialogUpdate_tvMessage);
    }

    @Override
    public void handleServiceUnavailableException(String message) {
        showProgressBar(false);

        if ((message != null) && (message.contains("Certificate not valid until"))) {
            Calendar calendar = Calendar.getInstance();
            Calendar certificateDate = new GregorianCalendar(2009, Calendar.APRIL, 2, 13, 0);

            if (calendar.before(certificateDate))
                ToastUtils.showToast(R.string.dialog_datetime_message);

        } else
            Toast.makeText(getActivity(), R.string.fragmentSavedAd_tNoConnection, Toast.LENGTH_LONG).show();
    }

    @Override
    public void handleInconsistentStateException(String message) {
        showProgressBar(false);
        ToastUtils.showToast(R.string.dialogUpdate_tvMessage);
    }

    private void showProgressBar(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

}
