package com.spareroom.ui.screen

import android.location.Location
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.*
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.lifecycle.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.spareroom.R
import com.spareroom.controller.AppVersion
import com.spareroom.integration.analytics.AnalyticsTrackerComposite
import com.spareroom.integration.dependency.Injectable
import com.spareroom.integration.location.LocationService
import com.spareroom.integration.webservice.exception.NetworkConnectivityException
import com.spareroom.lib.util.HardwareFeatures
import com.spareroom.model.business.*
import com.spareroom.ui.adapter.SearchRecyclerAdapter
import com.spareroom.ui.screen.legacy.AdActivity
import com.spareroom.ui.screen.legacy.ResultsActivity
import com.spareroom.ui.util.*
import com.spareroom.viewmodel.SearchViewModel
import kotlinx.android.synthetic.main.search_fragment.*
import javax.inject.Inject

private const val SEARCH_TYPE_ARG = "search_type_arg"
private const val REQUESTING_LOCATION_UPDATES_KEY = "requesting_location_updates_key"
private const val LOCATION_REQUESTED_KEY = "location_requested_key"
private const val REQUEST_LOCATION_PERMISSION = 0
private const val MIN_US_ADVERT_ID_LENGTH = 6

class SearchFragment : Fragment(), Injectable {
    private val viewModel by lazy { ViewModelProviders.of(activity!!, viewModelFactory).get(SearchViewModel::class.java) }
    private val analyticsTracker = AnalyticsTrackerComposite.getInstance()

    private val searchType by lazy { arguments!!.getSerializable(SEARCH_TYPE_ARG) as SearchType }
    private val isOffered by lazy { SearchType.OFFERED == searchType }

    private var fromBackground = false
    private var requestingLocationUpdates = false

    @Inject
    lateinit var locationService: LocationService

    @Inject
    lateinit var connectivityChecker: ConnectivityChecker

    @Inject
    lateinit var adapter: SearchRecyclerAdapter

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    companion object {
        const val TAG = "SearchFragmentTag"
        const val SETTINGS_CHECK_REQUEST_CODE = 1

        fun getInstance(searchType: SearchType): SearchFragment {
            val args = Bundle()
            args.putSerializable(SEARCH_TYPE_ARG, searchType)

            val fragment = SearchFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.search_fragment, container, false)
    }

    override fun onViewCreated(v: View, savedInstanceState: Bundle?) {
        requestingLocationUpdates = savedInstanceState?.getBoolean(REQUESTING_LOCATION_UPDATES_KEY) ?: false

        setUpBackView()
        setUpCleanView()
        setUpRecyclerView()
        setUpSwipeRefreshLayout()
        setUpAdapter()
        setUpLocationService(savedInstanceState)
        setUpSearchView()

        observeLocationSettings()
        observeSearch()
        observeLocation()
        observePostCode()
    }

    override fun onStart() {
        super.onStart()
        if (requestingLocationUpdates) {
            fromBackground = true
            locationProgressBar(true)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, requestingLocationUpdates)
        outState.putBoolean(LOCATION_REQUESTED_KEY, locationService.requested)
        super.onSaveInstanceState(outState)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_LOCATION_PERMISSION -> {
                if (PermissionUtils.havePermissionsBeenGranted(permissions, grantResults)) {
                    findLocation()
                } else {
                    handleLocationPermissionsNotGranted()
                }
            }
        }
    }

    private fun handleLocationPermissionsNotGranted() {
        showSnackBar(
            R.string.searching_nearby_requires_your_location,
            getString(R.string.settings),
            View.OnClickListener { PermissionUtils.openAppSettings(this, activity!!.packageName) })
    }

    private fun setUpSearchView() {
        fun stopLocationSearch() {
            if (requestingLocationUpdates) {
                locationProgressBar(false)
                locationService.stopLocationUpdates()
            }
        }

        searchEditText.addTextChangedListener(object : TextWatcherImpl() {
            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrEmpty()) cleanSearchImageView.hide() else cleanSearchImageView.show()
                stopLocationSearch()
            }
        })

        searchEditText.setOnTouchListener { _, _ ->
            stopLocationSearch()
            false
        }

        searchEditText.setOnKeyListener { view, keyCode, event ->
            if (KeyEvent.ACTION_UP == event?.action && KeyEvent.KEYCODE_ENTER == keyCode) {
                stopLocationSearch()
                handleSearchQuery(view, searchEditText.text.toString().trim())
                return@setOnKeyListener true
            }
            false
        }
    }

    private fun handleSearchQuery(view: View, search: String) {
        if (search.isNotEmpty()) {
            if (isAdvertId(search)) openAdvert(search) else triggerSearch(search)
        } else {
            SnackBarUtils.show(view, getString(R.string.specify_search))
        }
    }

    private fun setUpBackView() {
        searchBackImageView.setOnClickListener { activity?.onBackPressed() }
    }

    private fun setUpCleanView() {
        cleanSearchImageView.setOnClickListener { searchEditText.text.clear() }
    }

    private fun setUpRecyclerView() {
        searchRecyclerView.setHasFixedSize(true)
        searchRecyclerView.layoutManager = LinearLayoutManager(activity)
        searchRecyclerView.adapter = adapter
    }

    private fun setUpSwipeRefreshLayout() {
        searchSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(activity!!, R.color.material_deep_sea_blue))
        searchSwipeRefreshLayout.setOnRefreshListener { /*loadSearches()*/ }
    }

    private fun setUpAdapter() {
        adapter.init(activity!!, View.OnClickListener { view -> handleListItemClick(view) })
        viewModel.searchLiveData.initList(emptyStateList())
    }

    private fun setUpLocationService(savedInstanceState: Bundle?) {
        locationService.init(this, savedInstanceState?.getBoolean(LOCATION_REQUESTED_KEY) ?: false, viewModel.locationLiveData)
    }

    private fun observeLocationSettings() {
        viewModel.locationSettingsLiveData.observe(this, Observer { locationSettingsLoaded(it!!) })
    }

    private fun observeSearch() {
        viewModel.searchLiveData.observe(this, Observer { searchLoaded(it!!) })
    }

    private fun observeLocation() {
        viewModel.locationLiveData.observe(this, Observer { locationLoaded(it!!) })
    }

    private fun observePostCode() {
        viewModel.postCodeLiveData.observe(this, Observer { handlePostCode(it!!) })
    }

    private fun locationSettingsLoaded(locationSettingsCheckResponse: LocationSettingsCheckResponse) {
        when (locationSettingsCheckResponse) {
            is SuccessfulLocationSettingsCheck -> startLocationUpdates()
            is FailedLocationSettingsCheck -> cancelLocationUpdates()
        }
    }

    private fun searchLoaded(searchResponse: SearchResponse) {
        when (searchResponse) {
            is NewListSearch -> handleNewList(searchResponse)
            is FailedNewListSearch -> handleFailedNewList(searchResponse)
        }
    }

    private fun handleNewList(response: NewListSearch) {
        adapter.update(response.diff, response.searchItemList)
    }

    private fun handleFailedNewList(response: FailedNewListSearch) {
        when (response.throwable) {
            is NetworkConnectivityException -> showSnackBar(R.string.no_connection)
        }
    }

    private fun handlePostCode(searchPostCodeResponse: SearchPostCodeResponse) {
        when (searchPostCodeResponse) {
            is SuccessfulPostCodeSearch -> handleSuccessfulPostCode(searchPostCodeResponse)
            is FailedPostCodeSearch -> handleFailedPostCode()
        }
    }

    private fun handleSuccessfulPostCode(response: SuccessfulPostCodeSearch) {
        searchEditText.setText(response.postCode)
        searchEditText.setSelection(searchEditText.text.length)

        locationProgressBar(false)

        if (!fromBackground) {
            fromBackground = false
            triggerSearch(response.postCode)
        }
    }

    private fun handleFailedPostCode() {
        fromBackground = false
        locationProgressBar(false)
        showSnackBar(R.string.location_not_available)
    }

    private fun locationLoaded(response: LocationResponse) {
        requestingLocationUpdates = false

        when (response) {
            is SuccessfulLocation -> successfulLocation(response.location)
            is FailedLocation -> failedLocation()
        }
    }

    private fun successfulLocation(location: Location) {
        if (!connectivityChecker.isConnected()) {
            showSnackBar(R.string.no_connection)
            locationProgressBar(false)
            return
        }
        viewModel.postCodeLiveData.findPostCode(location.latitude, location.longitude)
    }

    private fun failedLocation() {
        showSnackBar(R.string.location_not_available)
        locationProgressBar(false)
    }

    private fun isAdvertId(search: String): Boolean {
        if (!TextUtils.isDigitsOnly(search))
            return false

        if (AppVersion.isUs())
            return search.length >= MIN_US_ADVERT_ID_LENGTH

        return true
    }

    private fun openAdvert(advertId: String) {
        AdActivity.start(activity as Activity, advertId)
        if (isOffered) analyticsTracker.trackEvent_SearchRooms_ManualAdvertSearch() else analyticsTracker.trackEvent_SearchFlatmates_ManualAdvertSearch()
    }

    private fun triggerSearch(search: String) {
        if (!connectivityChecker.isConnected()) {
            showSnackBar(R.string.no_connection)
            return
        }

        ResultsActivity.start(activity as Activity, search, searchType)
        if (isOffered) analyticsTracker.trackEvent_SearchRooms_ManualSearch() else analyticsTracker.trackEvent_SearchFlatmates_ManualSearch()
    }

    private fun emptyStateList(): List<SearchListItem> {
        val emptyList = mutableListOf<SearchListItem>()
        if (HardwareFeatures.supportsLocation(activity!!)) {
            emptyList.add(SearchListItem(getString(R.string.nearby), R.drawable.ic_nearby, searchItemType = SearchItemType.LOCATION))
            emptyList.add(SearchListItem(searchItemType = SearchItemType.DIVIDER))
        }
        emptyList.add(SearchListItem(searchItemType = SearchItemType.TITLE))
        emptyList.add(SearchListItem(getString(R.string.london), R.drawable.ic_area, getString(R.string.city)))
        emptyList.add(SearchListItem(getString(R.string.clapham), R.drawable.ic_area, getString(R.string.area).capitalize()))
        emptyList.add(SearchListItem(getString(R.string.postcode_sample), R.drawable.ic_location, getString(R.string.postcode)))
        if (isOffered)
            emptyList.add(SearchListItem(getString(R.string.victoria), R.drawable.ic_underground, getString(R.string.london_underground_station)))
        emptyList.add(SearchListItem(getString(R.string.advert_id_sample), R.drawable.ic_area, getString(R.string.advert_reference)))

        return emptyList
    }

    private fun handleListItemClick(view: View) {
        when ((view.getTag(R.id.item_tag) as SearchListItem).searchItemType) {
            SearchItemType.LOCATION -> handleLocationItemClick()
            else -> Unit // Temporary for hiding warnings
        }
    }

    private fun handleLocationItemClick() {
        fromBackground = false
        findLocation()
    }

    private fun findLocation() {
        if (PermissionUtils.hasLocationPermissions(activity!!)) {
            requestLocation()
        } else {
            PermissionUtils.requestLocationPermissions(this, REQUEST_LOCATION_PERMISSION)
        }
    }

    private fun requestLocation() {
        if (!connectivityChecker.isConnected()) {
            showSnackBar(R.string.no_connection)
            return
        }

        locationProgressBar(true)
        locationService.checkLocationSettings(activity as Activity, SETTINGS_CHECK_REQUEST_CODE, { locationProgressBar(false) }) { searchLocationFailed() }
    }

    private fun searchLocationFailed() {
        showSnackBar(R.string.location_not_available)
        locationProgressBar(false)
    }

    private fun startLocationUpdates() {
        locationService.startLocationUpdates()
        locationProgressBar(true)
    }

    private fun cancelLocationUpdates() {
        showSnackBar(
            R.string.searching_nearby_requires_your_location,
            getString(R.string.settings),
            View.OnClickListener { locationService.openLocationSettings(this) })
    }

    private fun locationProgressBar(show: Boolean) {
        viewModel.searchLiveData.setAsSearching(show)
        requestingLocationUpdates = show
    }

    private fun showSnackBar(@StringRes resId: Int, actionTitle: CharSequence? = null, action: View.OnClickListener? = null) {
        SnackBarUtils.show(searchParent, getString(resId), actionTitle, action)
    }

}
