package com.spareroom.ui.screen

import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.view.animation.AnimationUtils
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.constraintlayout.widget.ConstraintSet.*
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.*
import androidx.recyclerview.widget.*
import com.spareroom.R
import com.spareroom.controller.SpareroomContext
import com.spareroom.integration.dependency.Injectable
import com.spareroom.integration.webservice.exception.NetworkConnectivityException
import com.spareroom.integration.webservice.exception.NotLoggedInException
import com.spareroom.model.business.*
import com.spareroom.ui.adapter.LoadMoreScrollListener
import com.spareroom.ui.adapter.MyAdvertsAdapter
import com.spareroom.ui.screen.legacy.MyAdActivity
import com.spareroom.ui.screen.legacy.PlaceAdActivity
import com.spareroom.ui.util.*
import com.spareroom.ui.util.AnimationUtils.clearAnimation
import com.spareroom.ui.util.AnimationUtils.startFadeAnimation
import com.spareroom.ui.widget.AdvertCardViewSkeleton
import com.spareroom.viewmodel.MyAdvertsListViewModel
import kotlinx.android.synthetic.main.my_adverts_fragment.*
import kotlinx.android.synthetic.main.my_adverts_loading.*
import javax.inject.Inject

private const val OFFERED = "offered"

private const val ADVERT_STATS_REQUEST_CODE = 1
private const val CREATE_ADVERT_REQUEST_CODE = 2

private const val TOP_OF_THE_LIST = 0

class MyAdvertsListFragment : Fragment(), Injectable {

    private val offered by lazy { arguments?.getBoolean(OFFERED) == true }
    private val layoutManager by lazy { LinearLayoutManager(activity) }
    private val viewModel by lazy { viewModel() }
    private val scrollState by lazy { if (offered) viewModel?.offeredScrollState else viewModel?.wantedScrollState }

    @Inject
    lateinit var adapter: MyAdvertsAdapter

    @Inject
    lateinit var spareroomContext: SpareroomContext

    @Inject
    lateinit var connectivityChecker: ConnectivityChecker

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    companion object {

        fun getInstance(offered: Boolean): MyAdvertsListFragment {
            val fragment = MyAdvertsListFragment()
            fragment.arguments = Bundle().apply { putBoolean(OFFERED, offered) }
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.my_adverts_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        adapter.init(activity!!, this, View.OnClickListener { editAdvert(it) })
        setUpRecyclerView()
        setUpSwipeRefreshLayout()
        setUpErrorView()

        observeLoadingAdverts()

        if (shouldLoadAdverts(savedInstanceState)) {
            showLoadingView()

            if (offered)
                loadFirstPage()
        }
    }

    override fun onStart() {
        super.onStart()
        startRecyclerView()
    }

    override fun onStop() {
        super.onStop()
        stopRecyclerView()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.refresh_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when {
            item.itemId == R.id.menu_item_refresh -> refreshAdverts()
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when {
            requestCode == ADVERT_STATS_REQUEST_CODE && data != null -> updateAdvert(data)
            requestCode == CREATE_ADVERT_REQUEST_CODE -> refreshAdverts()
        }
    }

    private fun shouldLoadAdverts(savedInstanceState: Bundle?): Boolean {
        return savedInstanceState == null || (viewModel?.myAdverts?.value == null && !isLoadingAdverts())
    }

    private fun isLoadingAdverts() = viewModel?.myAdverts?.isLoading == true

    private fun showLoadingView() {
        addSkeletonAdverts(myAdvertsLoadingAdvertsContainer)
        myAdvertsLoadingView.show()
        startFadeAnimation(myAdvertsLoadingView, repeatCount = ObjectAnimator.INFINITE)
    }

    private fun hideLoadingView() {
        myAdvertsLoadingView.hide()
        clearAnimation(myAdvertsLoadingView)
    }

    private fun addSkeletonAdverts(container: ConstraintLayout) {
        container.show()
        container.removeAllViews()

        val inflater = LayoutInflater.from(activity)
        val advertHeight = resources.getDimensionPixelSize(R.dimen.advert_image_height).toDouble()
        val items = Math.ceil(UiUtils.screenHeight(activity) / advertHeight).toInt()
        for (i in 0 until items) {
            val viewToAdd = inflater.inflate(R.layout.advert_card_view_skeleton_item, container, false) as AdvertCardViewSkeleton
            viewToAdd.hideHeader()
            viewToAdd.hideFooter()
            viewToAdd.hideBottomDivider()
            viewToAdd.id = ViewCompat.generateViewId()
            container.addView(viewToAdd)

            val constraintSet = ConstraintSet()
            constraintSet.clone(container)
            constraintSet.connect(viewToAdd.id, LEFT, PARENT_ID, LEFT)
            constraintSet.connect(viewToAdd.id, RIGHT, PARENT_ID, RIGHT)
            if (i == 0) {
                constraintSet.connect(viewToAdd.id, TOP, PARENT_ID, TOP)
            } else {
                val previousView = container.getChildAt(i - 1)
                constraintSet.connect(viewToAdd.id, TOP, previousView.id, BOTTOM)
            }

            constraintSet.applyTo(container)
        }
    }

    private fun updateAdvert(data: Intent) {
        val advertId = data.getStringExtra(MyAdActivity.ID)
        val advertInfo = data.getSerializableExtra(MyAdActivity.ADVERT_INFO) as? AdvertInfo
        viewModel?.myAdverts?.updateAdvert(advertId, advertInfo, offered)
    }

    private fun refreshAdverts(): Boolean {
        if (isShowingAdverts()) {
            swipeRefreshLayout.isRefreshing = true
            loadFirstPage()
        } else {
            refreshFirstPage()
        }

        return true
    }

    private fun setUpRecyclerView() {
        recyclerViewMyAdverts.setHasFixedSize(true)
        recyclerViewMyAdverts.layoutManager = layoutManager
    }

    private fun addOnScrollListener() {
        if (offered) {
            recyclerViewMyAdverts.addOnScrollListener(object : LoadMoreScrollListener(layoutManager) {

                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (newState == RecyclerView.SCROLL_STATE_DRAGGING)
                        canScrollToTheTop(false)
                }

                override fun isLoading() = isLoadingAdverts()

                override fun hasMoreItems() = viewModel?.myAdverts?.hasMoreItems == true

                override fun loadMore() {
                    showBottomProgressbar()
                    loadNextPage()
                }
            })
        }
    }

    private fun removeOnScrollListener() {
        recyclerViewMyAdverts.clearOnScrollListeners()
    }

    private fun setUpSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(activity!!, R.color.material_deep_sea_blue))
        swipeRefreshLayout.setOnRefreshListener { loadFirstPage() }
    }

    private fun setUpErrorView() {
        myAdvertsErrorView.backgroundResource(R.color.transparent)
    }

    private fun hideBottomProgressbar() {
        if (myAdvertsBottomProgressBar.isVisible()) {
            myAdvertsBottomProgressBar.animation = AnimationUtils.loadAnimation(activity, R.anim.slide_out_to_bottom)
            myAdvertsBottomProgressBar.hide()
        }
    }

    private fun showBottomProgressbar() {
        myAdvertsBottomProgressBar.show()
        myAdvertsBottomProgressBar.animation = AnimationUtils.loadAnimation(activity, R.anim.slide_in_from_bottom)
    }

    private fun startRecyclerView() {
        recyclerViewMyAdverts.adapter = adapter
        scrollState?.restore(layoutManager)
        addOnScrollListener()
    }

    private fun stopRecyclerView() {
        scrollState?.save(layoutManager, recyclerViewMyAdverts)
        recyclerViewMyAdverts.adapter = null
        removeOnScrollListener()
    }

    private fun loadFirstPage() {
        canScrollToTheTop(true)
        viewModel?.myAdverts?.getAdverts(true)
    }

    private fun loadNextPage() {
        viewModel?.myAdverts?.getAdverts(false)
    }

    private fun observeLoadingAdverts() {
        viewModel?.myAdverts?.observe(this, Observer { advertsLoaded(it!!) })
    }

    private fun emitNoNetworkError() {
        viewModel?.myAdverts?.emitNoNetworkError()
    }

    private fun canScrollToTheTop(canScroll: Boolean) {
        scrollState?.canScrollToTheTop = canScroll
    }

    private fun canScrollToTheTop() = scrollState?.canScrollToTheTop == true

    private fun advertsLoaded(myAdvertsListResponse: MyAdvertsListResponse) {
        swipeRefreshLayout.isRefreshing = false
        hideLoadingView()
        hideErrorView()
        hideBottomProgressbar()
        when (myAdvertsListResponse) {
            is MyAdvertsListFirstPage -> handleFirstPage(myAdvertsListResponse)
            is MyAdvertsListNextPage -> handleNextPage(myAdvertsListResponse)
            is MyAdvertUpdated -> handleAdvertUpdated(myAdvertsListResponse)
            is FailedToLoadMyAdvertsList -> handleFailure(myAdvertsListResponse)
        }
    }

    private fun handleFirstPage(firstPage: MyAdvertsListFirstPage) {
        updateAdapter(
            ads = if (offered) firstPage.offeredAdverts else firstPage.wantedAdverts,
            diff = if (offered) firstPage.offeredAdvertsDiff else firstPage.wantedAdvertsDiff)

        if (canScrollToTheTop())
            layoutManager.scrollToPosition(TOP_OF_THE_LIST)

        if (!isShowingAdverts()) {
            showNoAdsErrorView()
            hideList()
        }
    }

    private fun handleNextPage(nextPage: MyAdvertsListNextPage) {
        updateAdapter(
            ads = if (offered) nextPage.offeredAdverts else nextPage.wantedAdverts,
            diff = if (offered) nextPage.offeredAdvertsDiff else nextPage.wantedAdvertsDiff)
    }

    private fun handleAdvertUpdated(advertUpdated: MyAdvertUpdated) {
        updateAdapter(
            ads = if (offered) advertUpdated.offeredAdverts else advertUpdated.wantedAdverts,
            diff = if (offered) advertUpdated.offeredAdvertsDiff else advertUpdated.wantedAdvertsDiff)

        if (!isShowingAdverts()) {
            showNoAdsErrorView()
            hideList()
        }
    }

    private fun updateAdapter(diff: DiffUtil.DiffResult, ads: List<AdvertItem>) {
        showList()
        adapter.update(diff, ads)
    }

    private fun editAdvert(view: View) {
        if (!connectivityChecker.isConnected()) {
            showSnackbar(getString(R.string.no_connection))
            return
        } else if (!spareroomContext.isUserLoggedIn) {
            showSnackbar(getString(R.string.please_log_in))
            return
        }

        MyAdActivity.startForResult(this, AdvertInfo.fromAdvert(view.getTag(R.id.item_tag) as AbstractAd), offered, ADVERT_STATS_REQUEST_CODE)
    }

    private fun showSnackbar(message: String) {
        if (userVisibleHint)
            SnackBarUtils.show(recyclerViewMyAdverts, message)
    }

    private fun handleFailure(failure: FailedToLoadMyAdvertsList) {
        if (isShowingAdverts()) {
            showSnackbar(snackBarErrorMessage(failure))
        } else {
            hideList()
            showErrorView(failure)
        }
    }

    private fun isShowingAdverts() = adapter.itemCount > 0

    private fun showErrorView(failure: FailedToLoadMyAdvertsList) {
        when (failure.throwable) {
            is NetworkConnectivityException -> showNoConnectionErrorView()
            else -> showGenericErrorView(failure.throwable is NotLoggedInException)
        }
    }

    private fun showNoConnectionErrorView() {
        myAdvertsErrorView.apply {
            container(errorViewContainer)
            addExtraContent { addSkeletonAdverts(it) }
            title(getString(R.string.no_connection))
            message(R.string.errorOffline_body)
            icon(R.drawable.ic_offline)
            action(View.OnClickListener { refreshFirstPage() }, R.string.retry)
            show(true)
        }
    }

    private fun showGenericErrorView(notLoggedIn: Boolean) {
        myAdvertsErrorView.apply {
            container(errorViewContainer)
            addExtraContent { addSkeletonAdverts(it) }
            title(getString(R.string.oops))
            message(if (notLoggedIn) R.string.adverts_loading_error_log_in else R.string.adverts_loading_error)
            icon(R.drawable.ic_error)
            action(View.OnClickListener { refreshFirstPage() }, R.string.retry)
            show(false)
        }
    }

    private fun showNoAdsErrorView() {
        myAdvertsErrorView.apply {
            container(errorViewContainer)
            addExtraContent { addSkeletonAdverts(it) }
            title(getString(R.string.create_advert))
            message(getString(R.string.no_adverts_hint, getString(if (offered) R.string.rooms else R.string.search).toLowerCase()))
            icon(R.drawable.ic_my_adverts)
            action(View.OnClickListener { postAdvert() }, R.string.post_ad)
            show(false)
        }
    }

    private fun refreshFirstPage() {
        if (connectivityChecker.isConnected()) {
            showLoadingView()
            hideList()
            hideErrorView()
            loadFirstPage()
        } else {
            emitNoNetworkError()
            if (myAdvertsErrorView.isVisible() && myAdvertsErrorView.isConnectionError) {
                startFadeAnimation(myAdvertsErrorView, repeatCount = 0)
            } else {
                showNoConnectionErrorView()
            }
        }
    }

    private fun postAdvert() {
        startActivityForResult(Intent(activity!!, PlaceAdActivity::class.java), CREATE_ADVERT_REQUEST_CODE)
    }

    private fun snackBarErrorMessage(failure: FailedToLoadMyAdvertsList): String {
        return getString(when (failure.throwable) {
            is NetworkConnectivityException -> R.string.no_connection
            is NotLoggedInException -> R.string.please_log_in
            else -> R.string.failed_to_load_adverts
        })
    }

    private fun hideErrorView() {
        myAdvertsErrorView.hideAllViews()
    }

    private fun hideList() {
        swipeRefreshLayout.hide()
    }

    private fun showList() {
        swipeRefreshLayout.show()
    }

    private fun viewModel(): MyAdvertsListViewModel? {
        val hostFragment = parentFragment
        return if (hostFragment != null) ViewModelProviders.of(hostFragment, viewModelFactory).get(MyAdvertsListViewModel::class.java) else null
    }

}