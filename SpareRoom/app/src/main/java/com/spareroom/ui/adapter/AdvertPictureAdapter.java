package com.spareroom.ui.adapter;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.spareroom.R;
import com.spareroom.integration.imageloader.ImageLoader;
import com.spareroom.integration.imageloader.ImageLoadingListener;
import com.spareroom.model.business.Picture;
import com.spareroom.ui.screen.PictureGalleryActivity;
import com.spareroom.ui.util.StringUtils;
import com.spareroom.ui.util.ToastUtils;
import com.spareroom.ui.widget.ProgressView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.RecyclerView;

public class AdvertPictureAdapter extends RecyclerViewAdapter<Picture, AdvertPictureAdapter.ViewHolder> {

    private final static String VIDEO_THUMBNAIL_TEMPLATE = "http://img.youtube.com/vi/%s/hqdefault.jpg";
    private final static String VIDEO_PLAYBACK_TEMPLATE = "http://www.youtube.com/watch?v=%s";

    private static final int VIDEO_VIEW_TYPE = 0;
    private static final int PICTURE_VIEW_TYPE = 1;

    private final ImageLoader imageLoader;
    private final int maxImageHeight;
    private List<Picture> pictures;
    private String videoIdUrl;
    private boolean hasVideo;
    private final View.OnClickListener pictureOnClickListener = this::onPictureClicked;
    private final View.OnClickListener videoOnClickListener = this::onVideoClicked;

    @Inject
    public AdvertPictureAdapter(ImageLoader imageLoader, Application application) {
        this.imageLoader = imageLoader;
        DisplayMetrics displayMetrics = application.getResources().getDisplayMetrics();
        maxImageHeight = Math.min(Math.max(displayMetrics.widthPixels, displayMetrics.heightPixels), Picture.MAX_DIMENSION);
    }

    public void setUp(Context context) {
        setContext(context);
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getVideoThumbnail() ? VIDEO_VIEW_TYPE : PICTURE_VIEW_TYPE;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new ViewHolder(getInflater().inflate(R.layout.advert_picture_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Picture picture = getItem(position);

        int itemViewType = getItemViewType(position);

        viewHolder.imgVideoIcon.setVisibility(itemViewType == VIDEO_VIEW_TYPE ? View.VISIBLE : View.GONE);

        // set image
        viewHolder.imgPicture.setImageDrawable(null);
        imageLoader.clear(viewHolder.imgPicture);
        viewHolder.imgPicture.setTag(R.id.position_tag, hasVideo ? position - 1 : position);
        viewHolder.imgPicture.setOnClickListener(itemViewType == VIDEO_VIEW_TYPE ? videoOnClickListener : pictureOnClickListener);
        String pictureUrl = generatePictureUrl(picture, itemViewType);
        if (StringUtils.isNullOrEmpty(pictureUrl)) {
            viewHolder.progressBar.hide();
        } else {
            viewHolder.progressBar.show();
            imageLoader.loadCenterCropImage(pictureUrl, viewHolder.imgPicture, new PictureLoadingListener(viewHolder.progressBar), true);
        }
    }

    private String generatePictureUrl(Picture picture, int itemViewType) {
        String largeUrl = picture.getLargeUrl();
        if (StringUtils.isNullOrEmpty(largeUrl, true))
            return "";

        if (itemViewType == VIDEO_VIEW_TYPE)
            return largeUrl;

        return String.format(Picture.LARGE_PICTURE_URL_TEMPLATE, largeUrl, maxImageHeight);
    }

    public void update(String videoIdUrl, List<Picture> pictures) {
        this.videoIdUrl = videoIdUrl;
        this.pictures = pictures;

        List<Picture> allPictures = new ArrayList<>();

        if (!StringUtils.isNullOrEmpty(videoIdUrl, true)) {
            hasVideo = true;
            Picture youtubeThumbnail = new Picture();
            youtubeThumbnail.setLargeUrl(String.format(VIDEO_THUMBNAIL_TEMPLATE, videoIdUrl));
            youtubeThumbnail.setVideoThumbnail(true);
            allPictures.add(youtubeThumbnail);
        }

        allPictures.addAll(pictures);
        update(allPictures, true);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView imgPicture;
        private final View imgVideoIcon;
        private final ProgressView progressBar;

        private ViewHolder(View itemView) {
            super(itemView);
            imgPicture = itemView.findViewById(R.id.imgPicture);
            imgVideoIcon = itemView.findViewById(R.id.imageVideoIcon);
            progressBar = itemView.findViewById(R.id.progressBarPicture);
            progressBar.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private static class PictureLoadingListener extends ImageLoadingListener {
        private final ProgressView progressBar;

        private PictureLoadingListener(ProgressView progressBar) {
            this.progressBar = progressBar;
        }

        @Override
        public void onLoadingFailed(Exception exception) {
            progressBar.hide();
        }

        @Override
        public void onLoadingComplete() {
            progressBar.hide();
        }
    }

    private void onPictureClicked(View view) {
        PictureGalleryActivity.start(view.getContext(), pictures, (int) view.getTag(R.id.position_tag));
    }

    private void onVideoClicked(View view) {
        String url = String.format(VIDEO_PLAYBACK_TEMPLATE, videoIdUrl);
        Intent videoIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));

        if (videoIntent.resolveActivity(view.getContext().getPackageManager()) != null) {
            view.getContext().startActivity(videoIntent);
        } else {
            ToastUtils.showToast(R.string.video_opening_error);
        }
    }
}
