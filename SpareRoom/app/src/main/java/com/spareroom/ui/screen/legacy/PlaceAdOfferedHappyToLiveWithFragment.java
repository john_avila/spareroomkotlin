package com.spareroom.ui.screen.legacy;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.model.business.*;

import androidx.core.content.ContextCompat;

public class PlaceAdOfferedHappyToLiveWithFragment extends PlaceAdFragment {
    private DraftAdOffered _draftAd;
    private PreferenceList _pl;
    private ListView _lvAreas;
    private Button _bConfirm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_offered_happy_to_live_with, container, false);

        setTitle(getString(R.string.place_ad_offered_happy_to_live_title));

        _lvAreas = rootView.findViewById(R.id.place_ad_offered_happy_to_live_with_lv);
        _bConfirm = rootView.findViewById(R.id.place_ad_bConfirm);

        _draftAd = (DraftAdOffered) getDraftAd();

        // In case there is an activity state change and we lost the draft
        if (_draftAd == null) {
            getActivity().finish();

            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }

        _pl = _draftAd.get_preferenceList();

        _lvAreas.setAdapter(new ContentAdapter());
        _bConfirm.setOnClickListener(v -> {
            _draftAd.set_preferenceList(_pl);
            finish();
        });
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        _lvAreas.setAdapter(null);
        _bConfirm.setOnClickListener(null);
    }

    private Drawable findIcon(int preference) {
        if (preference == PlaceAdOfferedPreferenceEnum.GUYS.ordinal())
            return ContextCompat.getDrawable(getActivity(), R.drawable.new_tenant_guys);
        if (preference == PlaceAdOfferedPreferenceEnum.GIRLS.ordinal())
            return ContextCompat.getDrawable(getActivity(), R.drawable.new_tenant_girls);
        if (preference == PlaceAdOfferedPreferenceEnum.COUPLES.ordinal())
            return ContextCompat.getDrawable(getActivity(), R.drawable.new_tenant_couples);
        if (preference == PlaceAdOfferedPreferenceEnum.DSS.ordinal())
            return ContextCompat.getDrawable(getActivity(), R.drawable.ic_budget);
        if (preference == PlaceAdOfferedPreferenceEnum.PETS.ordinal())
            return ContextCompat.getDrawable(getActivity(), R.drawable.new_tenant_pets);
        if (preference == PlaceAdOfferedPreferenceEnum.PROFESSIONALS.ordinal())
            return ContextCompat.getDrawable(getActivity(), R.drawable.new_tenant_professionals);
        if (preference == PlaceAdOfferedPreferenceEnum.SMOKERS.ordinal())
            return ContextCompat.getDrawable(getActivity(), R.drawable.new_tenant_smokers);
        if (preference == PlaceAdOfferedPreferenceEnum.STUDENTS.ordinal())
            return ContextCompat.getDrawable(getActivity(), R.drawable.new_tenant_students);
        return null;
    }

    private class ContentAdapter extends BaseAdapter {

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.place_ad_offered_happy_option_chosen, parent, false);
            }

            ((TextView) convertView.findViewById(R.id.place_ad_offered_areas_option_tvOption)).setText(getName(_pl.get(position).getId()));
            ((ImageView) convertView.findViewById(R.id.place_ad_offered_areas_option_chosen_ivIcon)).setImageDrawable(findIcon(_pl.get(position).getId()));

            if (_pl.get(position).isChecked()) {
                select(convertView);
            } else {
                unselect(convertView);
            }

            convertView.setOnClickListener(new OnRowClickListener(position));

            return convertView;
        }

        private String getName(int preference) {
            if (preference == PlaceAdOfferedPreferenceEnum.GUYS.ordinal())
                return getString(R.string.guys);
            if (preference == PlaceAdOfferedPreferenceEnum.GIRLS.ordinal())
                return getString(R.string.girls);
            if (preference == PlaceAdOfferedPreferenceEnum.COUPLES.ordinal())
                return getString(R.string.couples);
            if (preference == PlaceAdOfferedPreferenceEnum.DSS.ordinal())
                return getString(R.string.housing_benefit_recipients);
            if (preference == PlaceAdOfferedPreferenceEnum.PETS.ordinal())
                return getString(R.string.people_with_pets);
            if (preference == PlaceAdOfferedPreferenceEnum.PROFESSIONALS.ordinal())
                return getString(R.string.professionals);
            if (preference == PlaceAdOfferedPreferenceEnum.SMOKERS.ordinal())
                return getString(R.string.smokers);
            if (preference == PlaceAdOfferedPreferenceEnum.STUDENTS.ordinal())
                return getString(R.string.students);
            return null;
        }

        private void unselect(View row) {
            View vRow = row.findViewById(R.id.place_ad_offered_areas_option_chosen_rl);
            View vIcon = row.findViewById(R.id.place_ad_offered_areas_option_chosen_ivChosen);
            vRow.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
            vIcon.setVisibility(View.GONE);
        }

        private void select(View row) {
            View vRow = row.findViewById(R.id.place_ad_offered_areas_option_chosen_rl);
            View vIcon = row.findViewById(R.id.place_ad_offered_areas_option_chosen_ivChosen);
            vRow.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.light_blue_2));
            vIcon.setVisibility(View.VISIBLE);
        }

        private class OnRowClickListener implements OnClickListener {
            private final int _position;

            OnRowClickListener(int position) {
                _position = position;
            }

            @Override
            public void onClick(View v) {
                if (_pl.get(_position).isChecked()) { // (sSelected.contains(_pl.get(_position).getId())) {
                    unselect(v);
                    _pl.get(_position).setChecked(false);
                } else {
                    select(v);
                    _pl.get(_position).setChecked(true);
                }
            }
        }

        @Override
        public long getItemId(int position) {
            return _pl.get(position).getId();
        }

        @Override
        public Object getItem(int position) {
            return _pl.get(position);
        }

        @Override
        public int getCount() {
            return _pl.size();
        }
    }

}
