package com.spareroom.ui.anim;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class ProgressbarAnim extends Animation {
    private final int startWidth;
    private final int targetWidth;
    private final View view;

    public ProgressbarAnim(View view, int targetWidth) {
        this.view = view;
        this.targetWidth = targetWidth;
        this.startWidth = view.getLayoutParams().width;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        int newWidth;

        // newWidth = (int) (targetWidth * interpolatedTime);
        newWidth = startWidth + (int) ((targetWidth - startWidth) * interpolatedTime);
        view.getLayoutParams().width = newWidth;
        view.requestLayout();
    }

    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}