package com.spareroom.ui.widget.util.exclusivitymanager;
/*
 * Third party library developed by Manuel Luis Padilla Mochon under Apache License, Version 2.0
 * This code was developed before his employment with Flatshare Ltd
 *
 * @author Manuel Luis Padilla Mochon
 */

import android.view.View;

import java.util.HashMap;

public class ExclusivityManager {
    private HashMap<View, ViewActivationCommand> _views;
    private ViewActivationCommand _activeView;

    public ExclusivityManager() {
        _views = new HashMap<>();
    }

    public void add(ViewActivationCommand vac) {
        _views.put(vac._view, vac);
    }

    public void remove(View v) {
        _views.remove(v);
    }

    public void activate(View target) throws ExclusiveViewNotFoundException {
        if (!_views.containsKey(target))
            throw new ExclusiveViewNotFoundException();
        if (_activeView != null)
            _activeView.deactivate();
        _activeView = _views.get(target);
        _activeView.activate();
    }

    public void deactivateAll() {
        if (_activeView != null)
            _activeView.deactivate();
        _activeView = null;
    }
}
