package com.spareroom.ui.screen.legacy;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.integration.internal.DraftAdDAO;
import com.spareroom.integration.sharedpreferences.DraftAdStateDAO;
import com.spareroom.lib.util.DialogUtil;
import com.spareroom.model.business.*;
import com.spareroom.ui.flow.FlowState;
import com.spareroom.ui.flow.PlaceAdStateLoggedOutOffered;
import com.spareroom.ui.screen.AlertDialogBuilder;

import java.io.IOException;

public class PlaceAdActivity extends PlaceAdAbstractActivity {

    private static final int UNKNOWN_FRAGMENT_POSITION = -1;

    //region METHODS UI

    @Override
    public String getId() {
        return BUNDLE_VALUE_FORM_ID_PLACE;
    }

    protected void startScreen(boolean isForward) {
        try {
            saveDraft(_draft);
        } catch (IOException e) {
            // draft is only kept in memory, exiting would discard changes
        }

        super.startScreen(isForward);
    }

    //endregion METHODS UI

    //region METHODS CONTROLLER

    private void init() {
        if (SpareroomApplication.getInstance(
                getApplicationContext()
        ).getSpareroomContext().getSession() == null)
            _manager.setState(new PlaceAdStateLoggedOutOffered());

        _draft = null;
        _currentFragmentName = null;
        _nextFragmentName = null;

        try {
            _draft = loadDraft();
            _nextFragmentName = _manager.getNextFragmentName(_currentFragmentName, _draft);
        } catch (IOException e) {
            // do nothing, let it start over again for placing a new advert
        }

        DraftAdStateDAO draftAdStateDAO = new DraftAdStateDAO(this);
        DraftAdState draftAdState = draftAdStateDAO.read();

        if (_draft != null &&
                draftAdState.get_placeAdState() != null &&
                draftAdState.get_screen() != null) {
            try {
                if ((draftAdState != null) // if state was loaded successfully
                        && (draftAdState.get_placeAdState() != null)
                        && (draftAdState.get_screen() != null)) {
                    _nextFragmentName = draftAdState.get_screen();
                    _manager.setState(
                            (FlowState) Class.forName(draftAdState.get_placeAdState()).newInstance()
                    );

                    if (_manager.getFragmentPosition(_nextFragmentName) == UNKNOWN_FRAGMENT_POSITION) {
                        _nextFragmentName = _manager.getNextFragmentName(_currentFragmentName, _draft);
                        draftAdState = null;
                    }

                } else { // state was not loaded loaded succesfully, so _currentFragmentName and _draft == null
                    _nextFragmentName = _manager.getNextFragmentName(_currentFragmentName, _draft);
                }
            } catch (InstantiationException e) {
                _nextFragmentName = _manager.getNextFragmentName(_currentFragmentName, _draft);
            } catch (IllegalAccessException e) {
                _nextFragmentName = _manager.getNextFragmentName(_currentFragmentName, _draft);
            } catch (ClassNotFoundException e) {
                _nextFragmentName = _manager.getNextFragmentName(_currentFragmentName, _draft);
            }
        } else {
            _draft = null;
            _nextFragmentName = _manager.getNextFragmentName(_currentFragmentName, _draft);
        }

        _ghostFragmentName = _nextFragmentName;
        changeForwardProgress(_manager.getProgress(_nextFragmentName));

        startScreen(true);

        View vContent = LayoutInflater.from(this).inflate(R.layout.dialog_icon_message_buttons_blue, null);
        ImageView ivIcon = vContent.findViewById(R.id.dialog_icon);
        TextView tvMessage = vContent.findViewById(R.id.dialog_message);
        Button bConfirm = vContent.findViewById(R.id.dialog_bPositive);
        Button bCancel = vContent.findViewById(R.id.dialog_bNegative);

        String message =
                getString(R.string.placeAd_dialogResume_tvMessage_chunk1) +
                        ((getDraftAd() instanceof DraftAdOffered) ?
                                getString(R.string.placeAd_dialogResume_tvMessage_chunk2) :
                                ((getDraftAd() instanceof DraftAdWanted) ?
                                        getString(R.string.placeAd_dialogResume_tvMessage_chunk3) :
                                        getString(R.string.placeAd_dialogResume_tvMessage_chunk4))
                        ) + getString(R.string.placeAd_dialogResume_tvMessage_chunk5);

        ivIcon.setImageResource(R.drawable.looks_like_you_started);
        tvMessage.setText(message);
        bConfirm.setText(R.string.placeAdActivity_resume);
        bCancel.setText(R.string.placeAdActivity_startAgain);

        if (_draft != null) {
            if ((draftAdState != null)
                    && (draftAdState.get_placeAdState() != null)
                    && (draftAdState.get_placeAdState() != null)) {
                Dialog dialog = new AlertDialogBuilder(this, vContent).create();
                bConfirm.setOnClickListener(new DialogUtil.CancelOnClickListener(dialog));
                bCancel.setOnClickListener(new RestartOnClickListener(dialog));
                dialog.show();
            }
        }
    } // end init()

    private void restart() {
        DraftAdStateDAO draftAdStateDAO = new DraftAdStateDAO(PlaceAdActivity.this);
        DraftAdDAO draftAdDAO = new DraftAdDAO();

        draftAdDAO.delete(PlaceAdActivity.this);
        draftAdStateDAO.delete();
        _draft = null;
        _currentProgress = 0;
        animateForwardProgress(0);
        animateForwardProgressCount(_currentProgress, 0);
        changeNavigationProgress();
        _tvProgress.setText(String.format(AppVersion.flavor().getLocale(), "%d%%", _currentProgress));

        init();
    } // end restart()

    protected void screenFinished() {
        hideKeyboard();
        if (_manager.isLastFragment(_currentFragmentName)) {
            restart();

            return;
        }

        _nextFragmentName = _manager.getNextFragmentName(_currentFragmentName, _draft);
        _ghostFragmentName = _nextFragmentName;

        DraftAdStateDAO draftAdStateDAO = new DraftAdStateDAO(this);
        DraftAdDAO draftAdDAO = new DraftAdDAO();

        if ((!_nextFragmentName.equals(PlaceAdUploadCompleteFragment.class.getName())) &&
                (_nextFragmentName != null)) { // if the advert has not being uploaded yet
            DraftAdState draftAdState = new DraftAdState();
            draftAdState.set_screen(_nextFragmentName);
            draftAdState.set_placeAdState(_manager.getState().getClass().getName());
            draftAdStateDAO.update(draftAdState);
        } else { // if it's one of the last steps
            draftAdDAO.delete(this);
            draftAdStateDAO.delete();
        } // end if

        changeForwardProgress(_manager.getProgress(_nextFragmentName));

        startScreen(true);

    } //end screenFinished()

    //endregion METHODS CONTROLLER

    //region METHODS LIFECYCLE

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);

        init();
    }

    @Override
    public void onBackPressed() {
        int currentProgress = _manager.getProgress(_currentFragmentName);

        if (currentProgress == 100) {
            Toast.makeText(this, "Your advert has already been placed", Toast.LENGTH_LONG).show();
            finish();
        }

        super.onBackPressed();

    } //end onBackPressed()

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    private class RestartOnClickListener implements View.OnClickListener {
        Dialog _dialog;

        public RestartOnClickListener(Dialog d) {
            _dialog = d;
        }

        @Override
        public void onClick(View v) {
            restart();

            _dialog.dismiss();
        }
    }

    //endregion INNER CLASSES

}
