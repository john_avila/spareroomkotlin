package com.spareroom.ui.screen.legacy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.model.business.PlaceAdArea;
import com.spareroom.model.business.PlaceAdFullAreaSet;

import java.util.Collection;

public class PlaceAdWantedEditAreasLookingInFragment extends PlaceAdFragment {
    private DraftAdWanted _draft;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.place_ad_wanted_edit_areas_looking_in, container, false);

        setTitle(getString(R.string.place_ad_wanted_areas_looking_in));

        View bSelect = rootView.findViewById(R.id.place_ad_wanted_edit_areas_looking_in_bSelect);
        TextView tvAreasSelected = rootView.findViewById(R.id.place_ad_wanted_edit_areas_looking_in_tvAreasSelected);
        _draft = (DraftAdWanted) getDraftAd();

        // In case there is an activity state change and we lost the draft
        if (_draft == null) {
            getActivity().finish();
            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }

		/* Listeners */

        bSelect.setOnClickListener(new SelectOnClickListener());

        PlaceAdFullAreaSet placeAdFullArea = _draft.get_areas();

        if (placeAdFullArea != null) {
            Collection<PlaceAdArea> c = placeAdFullArea.getLevel(2);
            StringBuilder sb = new StringBuilder();

            if (c.size() != 0) {
                for (PlaceAdArea placeAdArea : c) {
                    sb.append(placeAdArea.getName());
                    sb.append(", ");
                }

                sb.delete(sb.length() - 2, sb.length());

            }

            tvAreasSelected.setText(sb.toString());

        }

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1478) {
            if (resultCode == Activity.RESULT_OK) {
                PlaceAdFullAreaSet areas = (PlaceAdFullAreaSet) data.getSerializableExtra("areas");
                _draft.set_areas(areas);
                if ((areas != null) && (areas.size() > 0)) {
                    finish();
                }
            }
        }
    }

    //region INNER CLASSES

    private class SelectOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            Intent activityAreas = new Intent();
            activityAreas.setClass(getActivity(), PlaceAdAreasActivity.class);
            activityAreas.putExtra("areas", new PlaceAdFullAreaSet());
            startActivityForResult(activityAreas, 1478);
        }

    }

    //endregion INNER CLASSES

}
