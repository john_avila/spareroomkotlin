package com.spareroom.ui.screen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.spareroom.R;
import com.spareroom.model.business.AccountRegisterForm;
import com.spareroom.model.business.AccountRegisterFormFacebook;
import com.spareroom.ui.flow.*;

import static com.spareroom.ui.screen.NewExistingFragment.EXTRA_ACTION_CODE;

/**
 * Sing In screen for the Material design
 * <p>
 * Created by miguel.rossi on 04/07/2016.
 */
public class RegisterActivity extends LinearMultiStepActivity {

    //region FIELDS CONTROLLER

    // The value of this field cannot be the same value as LoginActivity.CREDENTIALS
    public static final int KEY_REGISTER_FORM = 1;

    public static final String INTENT_KEY_REGISTER_METHOD = "method";
    public static final int INTENT_VALUE_REGISTER_METHOD_EMAIL = 0;
    public static final int INTENT_VALUE_REGISTER_METHOD_FACEBOOK = 1;
    public static final String INTENT_KEY_REGISTER_STEP = "step";
    public static final int INTENT_VALUE_REGISTER_STEP_1 = 1;
    public static final int INTENT_VALUE_REGISTER_STEP_2 = 2;
    public static final String INTENT_KEY_REGISTER_FACEBOOK_DATA_EMAIL = "email";
    public static final String INTENT_KEY_REGISTER_FACEBOOK_DATA_FIRSTNAME = "first_name";
    public static final String INTENT_KEY_REGISTER_FACEBOOK_DATA_LASTNAME = "last_name";

    private String _pageJoinedFrom = null;
    private int _registerMethod;
    private int _registerStep;

    //endregion FIELDS CONTROLLER

    //region METHODS CONTROLLER

    public String getPageJoinedFrom() {
        return _pageJoinedFrom;
    }

    public int getRegisterMethod() {
        return _registerMethod;
    }

    public void setRegisterMethod(int registerMethod) {
        _registerMethod = registerMethod;
    }

    public void initRegisterFlow() {
        FlowManager flowManager = new FlowManager();
        FlowState registerFlow = new RegisterFlow();

        if (_registerMethod == INTENT_VALUE_REGISTER_METHOD_FACEBOOK)
            if (_registerStep == INTENT_VALUE_REGISTER_STEP_1)
                registerFlow = new RegisterStep1Flow();
            else if (_registerStep == INTENT_VALUE_REGISTER_STEP_2) {
                registerFlow = new RegisterStep2Flow();
            }

        flowManager.setState(registerFlow);
        setFlowManager(flowManager);
    }

    public void setActivityResultOK() {
        Intent intent = new Intent();
        intent.putExtra(
                EXTRA_ACTION_CODE,
                getIntent().getStringExtra(EXTRA_ACTION_CODE));
        setResult(android.app.Activity.RESULT_OK, intent);

    } //end setActivityResultOK()

    //endregion METHODS CONTROLLER

    //region METHODS LIFECYCLE

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.material_activity);

        setResult(Activity.RESULT_CANCELED);

        Intent intent = getIntent();
        _registerMethod = intent.getIntExtra(INTENT_KEY_REGISTER_METHOD,
                INTENT_VALUE_REGISTER_METHOD_EMAIL);
        _registerStep = intent.getIntExtra(INTENT_KEY_REGISTER_STEP,
                INTENT_VALUE_REGISTER_STEP_1);

        AccountRegisterForm accountRegisterForm;

        if (_registerMethod == INTENT_VALUE_REGISTER_METHOD_FACEBOOK) {
            accountRegisterForm = new AccountRegisterFormFacebook();
        } else {
            accountRegisterForm = new AccountRegisterForm();
        }

        accountRegisterForm.setInAgreement(true);

        // If it comes from NewExistingActivity
        if (intent.hasExtra(NewExistingActivity.PLACEAD_FIRST_NAME))
            accountRegisterForm.setFirstName(
                    getIntent().getStringExtra(NewExistingActivity.PLACEAD_FIRST_NAME));
        if (intent.hasExtra(NewExistingActivity.PLACEAD_LAST_NAME))
            accountRegisterForm.setLastName(
                    getIntent().getStringExtra(NewExistingActivity.PLACEAD_LAST_NAME));
        if (intent.hasExtra(NewExistingActivity.PLACEAD_EMAIL))
            accountRegisterForm.setEmail(
                    getIntent().getStringExtra(NewExistingActivity.PLACEAD_EMAIL));

        // If it comes from LoginActivity
        if (intent.hasExtra(INTENT_KEY_REGISTER_FACEBOOK_DATA_EMAIL))
            accountRegisterForm.setEmail(
                    intent.getStringExtra(INTENT_KEY_REGISTER_FACEBOOK_DATA_EMAIL));

        if (intent.hasExtra(INTENT_KEY_REGISTER_FACEBOOK_DATA_FIRSTNAME))
            accountRegisterForm.setFirstName(
                    intent.getStringExtra(INTENT_KEY_REGISTER_FACEBOOK_DATA_FIRSTNAME));

        if (intent.hasExtra(INTENT_KEY_REGISTER_FACEBOOK_DATA_LASTNAME))
            accountRegisterForm.setLastName(
                    intent.getStringExtra(INTENT_KEY_REGISTER_FACEBOOK_DATA_LASTNAME));

        initRegisterFlow();

        _pageJoinedFrom = getIntent().getStringExtra(NewExistingActivity.PAGE_JOINED_FROM);

        setSharedObject(KEY_REGISTER_FORM, accountRegisterForm);

        activateProgressBarAnimation();

        // Sets the first fragment
        nextFragment();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = (Fragment) getSupportFragmentManager().findFragmentById(R.id.activity_fragment);
        fragment.onActivityResult(requestCode, resultCode, data);
    }

    //endregion METHODS LIFECYCLE

}
