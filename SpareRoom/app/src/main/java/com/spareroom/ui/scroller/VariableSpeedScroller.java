package com.spareroom.ui.scroller;

import android.content.Context;
import android.widget.Scroller;

/**
 * Scroller with custom transition velocity
 */
public class VariableSpeedScroller extends Scroller {
    private final int mDuration;

    public VariableSpeedScroller(Context context, int mDuration) {
        super(context);
        this.mDuration = mDuration;
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy, int duration) {
        // Ignore received duration, use fixed one instead
        super.startScroll(startX, startY, dx, dy, mDuration);
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy) {
        // Ignore received duration, use fixed one instead
        super.startScroll(startX, startY, dx, dy, mDuration);
    }
}
