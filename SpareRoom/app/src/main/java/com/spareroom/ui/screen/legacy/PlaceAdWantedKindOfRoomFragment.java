package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.ui.widget.PlaceAdButton;

public class PlaceAdWantedKindOfRoomFragment extends PlaceAdFragment {
    private PlaceAdButton _bSingleDouble;
    private PlaceAdButton _bDouble;
    private PlaceAdButton _bTwin;
    private PlaceAdButton _b2Rooms;

    private PlaceAdButton _previouslySelected;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_wanted_kind_of_room, container, false);

        setTitle(getString(R.string.place_ad_wanted_kind_of_room));

        _bSingleDouble = rootView.findViewById(R.id.place_ad_wanted_iam_bSingleDouble);
        _bDouble = rootView.findViewById(R.id.place_ad_wanted_iam_bDouble);
        _bTwin = rootView.findViewById(R.id.place_ad_wanted_iam_bTwin);
        _b2Rooms = rootView.findViewById(R.id.place_ad_wanted_iam_b2Rooms);

        final DraftAdWanted draft = (DraftAdWanted) getDraftAd();

        // In case there is an activity state change and we lost the draft
        if (draft == null) {
            getActivity().finish();

            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }

        DraftAdWanted.RoomType propertyType = draft.getRoomType();

        if (propertyType != null)
            switch (propertyType) {
                case DOUBLE:
                case DOUBLE_COUPLE:
                    _previouslySelected = _bDouble;
                    _bDouble.setPreviouslySelected();
                    break;
                case SINGLE_OR_DOUBLE:
                    _previouslySelected = _bSingleDouble;
                    _bSingleDouble.setPreviouslySelected();
                    break;
                case TWIN_OR_TWO_ROOMS:
                    _previouslySelected = _bTwin;
                    _bTwin.setPreviouslySelected();
                    break;
                case TWO_ROOMS:
                    _previouslySelected = _b2Rooms;
                    _b2Rooms.setPreviouslySelected();
                    break;
                default:
            }

        _bSingleDouble.setOnClickListener(v -> {
            setSingleDouble(draft);
            finish();
        });

        _bDouble.setOnClickListener(v -> {
            setDouble(draft);
            finish();
        });

        _bTwin.setOnClickListener(v -> {
            setTwin(draft);
            finish();
        });

        _b2Rooms.setOnClickListener(v -> {
            set2Rooms(draft);
            finish();
        });

        if ((draft.get_advertiserType() == DraftAdWanted.AdvertiserType.MALE_FEMALE) ||
                (draft.get_advertiserType() == DraftAdWanted.AdvertiserType.TWO_FEMALES) ||
                (draft.get_advertiserType() == DraftAdWanted.AdvertiserType.TWO_MALES)
                ) {
            if (draft.isCouple()) {
                _bSingleDouble.setVisibility(View.GONE);
                _bDouble.setVisibility(View.VISIBLE);
                _bTwin.setVisibility(View.GONE);
                _b2Rooms.setVisibility(View.VISIBLE);

                if (propertyType != null)
                    if (propertyType.equals(DraftAdWanted.RoomType.SINGLE_OR_DOUBLE)
                            || propertyType.equals(DraftAdWanted.RoomType.TWIN_OR_TWO_ROOMS))
                        setDouble(draft);

            } else {
                _bSingleDouble.setVisibility(View.GONE);
                _bDouble.setVisibility(View.GONE);
                _bTwin.setVisibility(View.VISIBLE);
                _b2Rooms.setVisibility(View.VISIBLE);

                if (propertyType != null)
                    if (propertyType.equals(DraftAdWanted.RoomType.SINGLE_OR_DOUBLE)
                            || propertyType.equals(DraftAdWanted.RoomType.DOUBLE_COUPLE)
                            || propertyType.equals(DraftAdWanted.RoomType.DOUBLE))
                        setTwin(draft);
            }
        } else {
            _bSingleDouble.setVisibility(View.VISIBLE);
            _bDouble.setVisibility(View.VISIBLE);
            _bTwin.setVisibility(View.GONE);
            _b2Rooms.setVisibility(View.GONE);

            if (propertyType != null)
                if (propertyType.equals(DraftAdWanted.RoomType.TWIN_OR_TWO_ROOMS)
                        || propertyType.equals(DraftAdWanted.RoomType.TWO_ROOMS))
                    setSingleDouble(draft);
        }

        return rootView;
    }

    private void setSingleDouble(DraftAdWanted draft) {
        if (_previouslySelected != null)
            _previouslySelected.setPreviouslyNotSelected();
        _bSingleDouble.setPreviouslySelected();
        draft.setRoomType(DraftAdWanted.RoomType.SINGLE_OR_DOUBLE);
    }

    private void setDouble(DraftAdWanted draft) {
        if (_previouslySelected != null)
            _previouslySelected.setPreviouslyNotSelected();
        _bDouble.setPreviouslySelected();

        if (draft.isCouple() != null && draft.isCouple())
            draft.setRoomType(DraftAdWanted.RoomType.DOUBLE_COUPLE);
        else
            draft.setRoomType(DraftAdWanted.RoomType.DOUBLE);
    }

    private void setTwin(DraftAdWanted draft) {
        if (_previouslySelected != null)
            _previouslySelected.setPreviouslyNotSelected();
        _bTwin.setPreviouslySelected();
        draft.setRoomType(DraftAdWanted.RoomType.TWIN_OR_TWO_ROOMS);
    }

    private void set2Rooms(DraftAdWanted draft) {
        if (_previouslySelected != null)
            _previouslySelected.setPreviouslyNotSelected();
        _b2Rooms.setPreviouslySelected();
        draft.setRoomType(DraftAdWanted.RoomType.TWO_ROOMS);
    }

}
