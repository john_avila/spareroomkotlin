package com.spareroom.ui.screen;

import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.widget.ImageView;
import android.widget.TextView;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomApplication;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.integration.imageloader.ImageLoader;
import com.spareroom.model.business.Session;
import com.spareroom.ui.screen.customtabs.feedback.SendFeedbackWebViewFallback;
import com.spareroom.ui.screen.legacy.SearchActivity2Offered;
import com.spareroom.ui.screen.legacy.SearchActivity2Wanted;
import com.spareroom.ui.util.*;
import com.spareroom.viewmodel.MainActivityViewModel;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import static com.spareroom.ui.screen.legacy.MainActivity.REQUEST_CODE_NAVIGATION_DRAWER;

/**
 * The default screen of the app.
 */
public class HomeFragment extends Fragment implements Injectable {

    public static final String TAG = "HomeFragmentTag";

    private boolean loggedIn;
    private ImageView ivAvatar;
    private TextView tvSubtitle;

    @Inject
    ImageLoader imageLoader;

    @Inject
    ConnectivityChecker connectivityChecker;

    public static HomeFragment getInstance() {
        return new HomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Session session = SpareroomApplication.getInstance().getSpareroomContext().getSession();
        loggedIn = session != null;

        // Views
        View rootView;
        TextView tvFeedBack, tvTitle, tvLogin, tvRegister;

        // Check if the user is logged in or out for setting the views
        ImageView ivSearchOffered;
        ImageView ivSearchWanted;
        if (session != null) {
            rootView = inflater.inflate(R.layout.material_home_logged_in_fragment, container, false);
            View tiles = rootView.findViewById(R.id.homeLoggedIn_tiles);
            ivSearchOffered = tiles.findViewById(R.id.home_searchRooms);
            ivSearchWanted = tiles.findViewById(R.id.home_searchMates);
            tvFeedBack = rootView.findViewById(R.id.homeLoggedIn_feedback).findViewById(R.id.homeFeedback_button);
            tvTitle = rootView.findViewById(R.id.homeLoggedIn_title);
            tvSubtitle = rootView.findViewById(R.id.homeLoggedIn_subtitle);
            ivAvatar = rootView.findViewById(R.id.homeLoggedIn_avatar);

            tvTitle.setText(
                    String.format(
                            AppVersion.flavor().getLocale(),
                            "%s %s!",
                            getString(R.string.homeLoggedIn_feedBack_title),
                            StringUtils
                                    .capitalizeEachFirstLetter(session.get_user().get_firstName())));

        } else {
            rootView = inflater.inflate(R.layout.material_home_logged_out_fragment, container, false);
            View tiles = rootView.findViewById(R.id.homeLoggedOut_tiles);
            ivSearchOffered = tiles.findViewById(R.id.home_searchRooms);
            ivSearchWanted = tiles.findViewById(R.id.home_searchMates);
            tvFeedBack = rootView.findViewById(R.id.homeLoggedOut_feedback).findViewById(R.id.homeFeedback_button);
            tvLogin = rootView.findViewById(R.id.homeLoggedOut_login);
            tvRegister = rootView.findViewById(R.id.homeLoggedOut_register);

            tvLogin.setOnClickListener(v -> {
                Intent intent = new Intent();
                intent.setClass(getActivity(), LoginActivity.class);
                getActivity().startActivityForResult(intent, REQUEST_CODE_NAVIGATION_DRAWER);

            });

            tvRegister.setOnClickListener(v -> {
                Intent intent = new Intent();
                intent.setClass(getActivity(), RegisterActivity.class);
                getActivity().startActivityForResult(intent, REQUEST_CODE_NAVIGATION_DRAWER);
            });

        }

        // Set the other views
        imageLoader.loadImage(R.drawable.rooms, ivSearchOffered, this);
        ivSearchOffered.setOnClickListener(v -> {
            Intent searchOfferedActivity = new Intent();
            searchOfferedActivity.setClass(getActivity(), SearchActivity2Offered.class);
            startActivity(searchOfferedActivity);
        });

        imageLoader.loadImage(R.drawable.flatmates, ivSearchWanted, this);
        ivSearchWanted.setOnClickListener(v -> {
            Intent searchWantedActivity = new Intent();
            searchWantedActivity.setClass(getActivity(), SearchActivity2Wanted.class);
            startActivity(searchWantedActivity);
        });

        tvFeedBack.setOnClickListener(v -> {
            if (connectivityChecker.isConnected()) {
                new SendFeedbackWebViewFallback().openUri(getActivity(), Feedback.feedbackUrl(getActivity()));
            } else {
                SnackBarUtils.show(v, getString(R.string.no_connection));
            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        setToolbar(view, getString(R.string.app_name));
        ViewModelProviders.of(getActivity()).get(MainActivityViewModel.class).getSyncDrawer().sync();
    }

    @Override
    public void onResume() {
        super.onResume();

        updateAvatar();
        updateUnreadMessagesCount();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }

    public boolean loggedIn() {
        return loggedIn;
    }

    private void updateAvatar() {
        Session session = SpareroomApplication.getSpareRoomContext().getSession();
        String avatarUrl = session != null ? session.get_profilePictureUrl() : null;
        if (avatarUrl != null && ivAvatar != null && !StringUtils.equals(avatarUrl, (String) ivAvatar.getTag(R.id.image_url_tag))) {
            ivAvatar.setTag(R.id.image_url_tag, avatarUrl);
            imageLoader.loadCircularImage(avatarUrl, ivAvatar, this);
        }
    }

    private void updateUnreadMessagesCount() {
        Session session = SpareroomApplication.getSpareRoomContext().getSession();
        if (session != null && tvSubtitle != null) {
            int messagesCount = session.getMessageCount();
            tvSubtitle.setText(messagesCount == 0 ? getString(R.string.homeLoggedIn_feedBack_subtitle) : getMessagesCount(messagesCount));
        }
    }

    private String getMessagesCount(int messagesCount) {
        return String.format("%s %s %s %s",
                getString(R.string.homeLoggedIn_feedBack_subtitle_start),
                messagesCount,
                getString(R.string.navigationDrawer_messagesUnread),
                getResources().getQuantityString(R.plurals.messages, messagesCount).toLowerCase());
    }

}
