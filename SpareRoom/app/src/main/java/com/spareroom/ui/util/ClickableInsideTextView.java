package com.spareroom.ui.util;

import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

/**
 * Transforms a String in a SpannableString (text whose content is immutable but to which markup
 * objects can be attached and detached) and makes it clickable
 * <p>
 * Created by miguel.rossi on 07/07/2016.
 */
public class ClickableInsideTextView {

    /**
     * Transforms a String in a SpannableString, a part of a TextView clickable.
     * The String is later inserted in a TextView with others having its our listener.
     *
     * @param text     the text to convert
     * @param listener the listener for the clickable String
     * @return the String with superpowers
     */
    public SpannableString makeLinkSpan(CharSequence text, View.OnClickListener listener) {
        SpannableString link = new SpannableString(text);
        link.setSpan(new ClickableString(listener), 0, text.length(),
                SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
        return link;
    }

    /**
     * Makes clickable the part of the TextView that need it.
     *
     * @param tv the TextView for the magic
     */
    public void makeLinksFocusable(TextView tv) {
        MovementMethod m = tv.getMovementMethod();
        if ((m == null) || !(m instanceof LinkMovementMethod)) {
            if (tv.getLinksClickable()) {
                tv.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }
    }

    /**
     * Customized ClickableSpan
     */
    private static class ClickableString extends ClickableSpan {
        private View.OnClickListener mListener;

        public ClickableString(View.OnClickListener listener) {
            mListener = listener;
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(v);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setUnderlineText(false);
            ds.setARGB(255, 255, 255, 255);
        }

    } // end class ClickableString

}
