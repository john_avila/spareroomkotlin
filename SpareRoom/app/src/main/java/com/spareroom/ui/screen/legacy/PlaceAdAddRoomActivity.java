package com.spareroom.ui.screen.legacy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.model.business.Price;
import com.spareroom.model.business.Room;
import com.spareroom.ui.screen.Activity;
import com.spareroom.ui.util.StringUtils;
import com.spareroom.ui.widget.util.exclusivitymanager.*;

import androidx.core.content.ContextCompat;

public class PlaceAdAddRoomActivity extends Activity {

    //region FIELDS UI

    private EditText _etPrice;
    private EditText deposit;

    //endregion FIELDS UI

    //region FIELDS CONTROLLER

    private Room _room;

    private int requestCode;

    //endregion FIELDS CONTROLLER

    //region METHODS CONTROLLER

    private void setActivityResultOKAdd() {
        Intent intent = new Intent();

        intent.putExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_DEPOSIT, _room.deposit());
        intent.putExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_ROOM_RENT_AMOUNT, _room.get_rent().get_amount());
        intent.putExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_ROOM_RENT_ARE_BILLS_INCLUDED, _room.get_rent().get_areBillsIncluded());
        intent.putExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_ROOM_RENT_PERIODICITY, _room.get_rent().get_periodicity());
        intent.putExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_ROOM_SIZE, _room.get_roomSize());
        intent.putExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_ROOM_STATUS, _room.get_roomStatus());
        intent.putExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_POSITION, getIntent().getIntExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_POSITION, -1));
        setResult(Activity.RESULT_OK, intent);

    } //end setActivityResultOKAdd()

    private void deleteRoom() {
        Intent intent = new Intent();

        intent.putExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_POSITION, getIntent().getIntExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_POSITION, -1));
        intent.putExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_ACTION, PlaceAdOfferedAddRoomFragment.VALUE_REMOVE);
        setResult(Activity.RESULT_OK, intent);
        finish();

    } //end setActivityResultOKRemove()

    //endregion METHODS CONTROLLER

    //region METHODS LIFECYCLE

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.place_ad_offered_room);

        Button bPerMonth = findViewById(R.id.place_ad_offered_room_bPerMonth);
        Button bPerWeek = findViewById(R.id.place_ad_offered_room_bPerWeek);
        Button bDouble = findViewById(R.id.place_ad_offered_room_bDouble);
        Button bSingle = findViewById(R.id.place_ad_offered_room_bSingle);
        TextView tvStatusTitle = findViewById(R.id.place_ad_offered_room_statusTitle);
        LinearLayout llStatusButtons = findViewById(R.id.place_ad_offered_room_statusButtons);
        Button bAvailable = findViewById(R.id.place_ad_offered_room_bAvailable);
        Button bTaken = findViewById(R.id.place_ad_offered_room_bTaken);
        Button bOccupied = findViewById(R.id.place_ad_offered_room_bOccupied);

        Price rent = new Price();
        requestCode = getIntent().getIntExtra(PlaceAdOfferedAddRoomFragment.KEY_REQUEST_CODE, -1);
        String formId = getIntent().getStringExtra(PlaceAdAbstractActivity.BUNDLE_KEY_FORM_ID);

        _etPrice = findViewById(R.id.place_ad_offered_room_etPrice);
        _room = new Room();

        setToolbar(getString(isAddingRoom() ? R.string.add_room : R.string.edit_room));

        // The room data
        if (getIntent().getStringExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_ROOM_RENT_AMOUNT) == null) {
            rent.set_amount(_etPrice.getText().toString());
            rent.set_areBillsIncluded(Price.BILLS_INCLUDED_NO);
            rent.set_periodicity(Price.PERIODICITY_MONTH);
            _room.set_rent(rent);
            _room.set_roomSize(Room.ROOM_SIZE_DOUBLE);
            _room.set_roomStatus(Room.ROOM_STATUS_AVAILABLE);

        } else {
            rent.set_amount(getIntent().getStringExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_ROOM_RENT_AMOUNT));
            rent.set_areBillsIncluded(getIntent().getStringExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_ROOM_RENT_ARE_BILLS_INCLUDED));
            rent.set_periodicity(getIntent().getStringExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_ROOM_RENT_PERIODICITY));
            _room.deposit(getIntent().getStringExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_DEPOSIT));
            _room.set_rent(rent);
            _room.set_roomSize(getIntent().getIntExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_ROOM_SIZE, Room.ROOM_SIZE_DOUBLE));
            _room.set_roomStatus(getIntent().getIntExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_ROOM_STATUS, Room.ROOM_STATUS_AVAILABLE));

        }

        // The price EditText
        _etPrice.setText(rent.get_amount());
        _etPrice.setSelection(_etPrice.getText().length());

        setUpDeposit(getIntent().getBooleanExtra(PlaceAdOfferedAddRoomFragment.KEY_EXTRA_WHOLE_PROPERTY, false));

        // Hides the keyboard to let the user see all the options in the screen
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null)
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);


        /* Buttons */

        // Billing Cycle

        ExclusivityManager billingManager = new ExclusivityManager();

        ViewActivationCommand vacPerMonth = new ButtonViewActivationCommand(bPerMonth);
        billingManager.add(vacPerMonth);
        bPerMonth.setOnClickListener(new ButtonPcmOnClickListener(billingManager));

        ViewActivationCommand vacPerWeek = new ButtonViewActivationCommand(bPerWeek);
        billingManager.add(vacPerWeek);
        bPerWeek.setOnClickListener(new ButtonPwOnClickListener(billingManager));

        try {
            if (_room.get_rent().get_periodicity().equals(Price.PERIODICITY_WEEK)) {
                billingManager.activate(bPerWeek);
            } else if (_room.get_rent().get_periodicity().equals(Price.PERIODICITY_MONTH)) {
                billingManager.activate(bPerMonth);
            }
        } catch (ExclusiveViewNotFoundException ignored) {
        }

        // Room Size

        ExclusivityManager roomManager = new ExclusivityManager();

        ViewActivationCommand vacDouble = new ButtonViewActivationCommand(bDouble);
        roomManager.add(vacDouble);
        bDouble.setOnClickListener(new ButtonDoubleOnClickListener(roomManager));

        ViewActivationCommand vacSingle = new ButtonViewActivationCommand(bSingle);
        roomManager.add(vacSingle);
        bSingle.setOnClickListener(new ButtonSingleOnClickListener(roomManager));

        try {
            if (_room.get_roomSize() == Room.ROOM_SIZE_DOUBLE) {
                roomManager.activate(bDouble);
            } else if (_room.get_roomSize() == Room.ROOM_SIZE_SINGLE) {
                roomManager.activate(bSingle);
            }
        } catch (ExclusiveViewNotFoundException ignored) {
        }

        // Room Status

        if (formId.equals(PlaceAdAbstractActivity.BUNDLE_VALUE_FORM_ID_EDIT)) {

            tvStatusTitle.setVisibility(View.VISIBLE);
            llStatusButtons.setVisibility(View.VISIBLE);

            ExclusivityManager statusManager = new ExclusivityManager();

            ViewActivationCommand vacAvailable = new ButtonViewActivationCommand(bAvailable);
            statusManager.add(vacAvailable);
            bAvailable.setOnClickListener(new ButtonAvailableOnClickListener(statusManager));

            ViewActivationCommand vacTaken = new ButtonViewActivationCommand(bTaken);
            statusManager.add(vacTaken);
            bTaken.setOnClickListener(new ButtonTakenOnClickListener(statusManager));

            ViewActivationCommand vacOccupied = new ButtonViewActivationCommand(bOccupied);
            statusManager.add(vacOccupied);
            bOccupied.setOnClickListener(new ButtonOccupiedOnClickListener(statusManager));

            try {
                if (_room.get_roomStatus() == Room.ROOM_STATUS_OCCUPIED)
                    statusManager.activate(bOccupied);
                else if (_room.get_roomStatus() == Room.ROOM_STATUS_AVAILABLE)
                    statusManager.activate(bAvailable);
                else if (_room.get_roomStatus() == Room.ROOM_STATUS_TAKEN)
                    statusManager.activate(bTaken);
            } catch (ExclusiveViewNotFoundException ignored) {
            }

        }

    } //end onCreate(Bundle savedInstanceState)

    private void setUpDeposit(boolean wholeProperty) {
        if (wholeProperty)
            return;

        View depositParent = findViewById(R.id.place_ad_offered_room_llDeposit);
        depositParent.setVisibility(View.VISIBLE);

        deposit = findViewById(R.id.place_ad_offered_room_deposit);
        Double securityDeposit = StringUtils.toDouble(_room.deposit());
        deposit.setText(String.valueOf(securityDeposit == null ? 0 : securityDeposit.intValue()));
        deposit.setSelection(deposit.getText().length());
    }

    private boolean isAddingRoom() {
        return requestCode == PlaceAdOfferedAddRoomFragment.VALUE_REQUEST_CODE_ADD_ROOM;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_room_menu, menu);
        menu.findItem(R.id.menu_item_delete_room).setVisible(!isAddingRoom());
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_item_delete_room:
                deleteRoom();
                return true;
            case R.id.menu_item_add_room:
                addRoom();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    private class ButtonViewActivationCommand extends ViewActivationCommand {

        public ButtonViewActivationCommand(View v) {
            super(v);
        }

        @Override
        public void activate() {
            super.activate();
            _view.setBackgroundResource(R.drawable.button_place_ad_bkg_selected);
            if (_view instanceof TextView)
                ((TextView) _view).setTextColor(ContextCompat.getColor(_view.getContext(), R.color.cyan));
        } //end activate()

        @Override
        public void deactivate() {
            super.deactivate();
            _view.setBackgroundResource(R.drawable.button_place_ad_bkg);
            if (_view instanceof TextView)
                ((TextView) _view).setTextColor(ContextCompat.getColor(_view.getContext(), R.color.grey_2));
        } //end deactivate()

    } //end class ButtonViewActivationCommand

    private class ButtonPwOnClickListener implements OnClickListener {
        private final ExclusivityManager _em;

        public ButtonPwOnClickListener(ExclusivityManager em) {
            _em = em;
        }

        @Override
        public void onClick(View v) {
            if (v != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }

            try {
                _em.activate(v);
                _room.get_rent().set_periodicity(Price.PERIODICITY_WEEK);
            } catch (ExclusiveViewNotFoundException ignored) {
            }
        } // onClick(View v)

    } //end class ButtonPwOnClickListener

    private class ButtonPcmOnClickListener implements OnClickListener {
        private final ExclusivityManager _em;

        public ButtonPcmOnClickListener(ExclusivityManager em) {
            _em = em;
        }

        @Override
        public void onClick(View v) {
            if (v != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }

            try {
                _em.activate(v);
                _room.get_rent().set_periodicity(Price.PERIODICITY_MONTH);
            } catch (ExclusiveViewNotFoundException ignored) {
            }
        } //end onClick(View v)

    } //end class ButtonPcmOnClickListener

    private class ButtonDoubleOnClickListener implements OnClickListener {
        private final ExclusivityManager _em;

        public ButtonDoubleOnClickListener(ExclusivityManager em) {
            _em = em;
        }

        @Override
        public void onClick(View v) {
            if (v != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }

            try {
                _em.activate(v);
                _room.set_roomSize(Room.ROOM_SIZE_DOUBLE);
            } catch (ExclusiveViewNotFoundException ignored) {
            }
        } //end onClick(View v)

    } //end class ButtonDoubleOnClickListener

    private class ButtonSingleOnClickListener implements OnClickListener {
        private final ExclusivityManager _em;

        public ButtonSingleOnClickListener(ExclusivityManager em) {
            _em = em;
        }

        @Override
        public void onClick(View v) {
            if (v != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }

            try {
                _em.activate(v);
                _room.set_roomSize(Room.ROOM_SIZE_SINGLE);
            } catch (ExclusiveViewNotFoundException ignored) {
            }
        } //end onClick(View v)

    } //end class ButtonSingleOnClickListener

    private class ButtonAvailableOnClickListener implements OnClickListener {
        private final ExclusivityManager _em;

        public ButtonAvailableOnClickListener(ExclusivityManager em) {
            _em = em;
        }

        @Override
        public void onClick(View v) {
            if (v != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }

            try {
                _em.activate(v);
                _room.set_roomStatus(Room.ROOM_STATUS_AVAILABLE);
            } catch (ExclusiveViewNotFoundException ignored) {
            }
        } //end onClick(View v)

    } //end class ButtonAvailableOnClickListener

    private class ButtonTakenOnClickListener implements OnClickListener {
        private final ExclusivityManager _em;

        public ButtonTakenOnClickListener(ExclusivityManager em) {
            _em = em;
        }

        @Override
        public void onClick(View v) {
            if (v != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }

            try {
                _em.activate(v);
                _room.set_roomStatus(Room.ROOM_STATUS_TAKEN);
            } catch (ExclusiveViewNotFoundException ignored) {
            }
        } //end onClick(View v)

    } //end class ButtonTakenOnClickListener

    private class ButtonOccupiedOnClickListener implements OnClickListener {
        private final ExclusivityManager _em;

        public ButtonOccupiedOnClickListener(ExclusivityManager em) {
            _em = em;
        }

        @Override
        public void onClick(View v) {
            if (v != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }

            try {
                _em.activate(v);
                _room.set_roomStatus(Room.ROOM_STATUS_OCCUPIED);
            } catch (ExclusiveViewNotFoundException ignored) {
            }
        } //end onClick(View v)

    } //end class ButtonOccupiedOnClickListener

    private void addRoom() {
        Integer intPrice = 0;

        try {
            intPrice = Integer.parseInt(_etPrice.getText().toString());
        } catch (NumberFormatException ignored) {
        }

        if (intPrice == 0)
            Toast.makeText(PlaceAdAddRoomActivity.this, getString(R.string.place_ad_offered_add_room_add_rent), Toast.LENGTH_LONG).show();

        else {
            _room.get_rent().set_amount(Integer.toString(Integer.parseInt(_etPrice.getText().toString())));

            if (deposit != null)
                _room.deposit(deposit.getText().toString());

            setActivityResultOKAdd();
            finish();

        }
    }

    //endregion INNER classes

}
