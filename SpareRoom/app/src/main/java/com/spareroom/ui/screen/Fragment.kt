package com.spareroom.ui.screen

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.annotation.DrawableRes
import androidx.fragment.app.Fragment
import com.spareroom.App
import com.spareroom.R
import com.spareroom.integration.analytics.AnalyticsTrackerComposite
import com.spareroom.lib.core.ObserverLifecycleInterface
import java.util.*

open class Fragment : Fragment() {

    private val lifecycleObservers = LinkedList<ObserverLifecycleInterface>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AnalyticsTrackerComposite.getInstance().trackScreen_Fragment(this)
    }

    override fun onStop() {
        for (lifecycleInterface in lifecycleObservers)
            lifecycleInterface.notifyOnStop()

        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()

        App.get().refWatcher.watch(this)
    }

    fun addObserver(o: ObserverLifecycleInterface) {
        lifecycleObservers.add(o)
    }

    fun getBaseActivity() = activity as Activity

    fun handleHomeButton(menuItem: MenuItem): Boolean {
        return when (menuItem.itemId) {
            android.R.id.home -> {
                activity?.finish()
                true
            }
            else -> super.onOptionsItemSelected(menuItem)
        }
    }

    @JvmOverloads
    protected fun setToolbar(view: View, title: String, homeAsUp: Boolean = false, @DrawableRes icon: Int = R.drawable.ic_back) {
        (activity as? Activity)?.setToolbar(title, homeAsUp, icon, view)
    }

    protected open fun setTitle(title: String?) {
        (activity as? Activity)?.title = title
    }

    protected fun setHomeAsUpIndicator(@DrawableRes icon: Int) {
        (activity as? Activity)?.supportActionBar?.setHomeAsUpIndicator(icon)
    }

}