package com.spareroom.ui.filters.screen;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;

import com.spareroom.R;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.model.business.SearchAdvertListProperties.MaxTerm;
import com.spareroom.ui.filters.adapter.FiltersAlertDialogAdapter;
import com.spareroom.ui.filters.provider.SubtitleUtil;
import com.spareroom.ui.screen.AlertDialogBuilder;
import com.spareroom.ui.screen.DialogFragment;
import com.spareroom.ui.util.UiUtils;
import com.spareroom.viewmodel.FilterViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MaxTermDialog extends DialogFragment implements Injectable {
    public final static String TAG = MaxTermDialog.class.getName() + "Tag";

    private final static String KEY_POSITION = "key_position";
    private final static String KEY_PREVIOUS_SELECTION = "key_previous_selection";

    private RecyclerView recyclerView;
    private FilterViewModel viewModel;
    private int position;
    private int previousSelection;

    @Inject
    SubtitleUtil subtitleUtil;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    public static MaxTermDialog newInstance(int position, int previousSelection) {
        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        args.putInt(KEY_PREVIOUS_SELECTION, previousSelection);

        MaxTermDialog fragment = new MaxTermDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(getBaseActivity(), viewModelFactory).get(FilterViewModel.class);
        assert getArguments() != null;
        position = getArguments().getInt(KEY_POSITION);
        previousSelection =
                savedInstanceState == null
                        ? getArguments().getInt(KEY_PREVIOUS_SELECTION)
                        : savedInstanceState.getInt(KEY_PREVIOUS_SELECTION, MaxTerm.NOT_SET.ordinal());
        return createDialog(createRecyclerView()).create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(KEY_PREVIOUS_SELECTION, ((FiltersAlertDialogAdapter) recyclerView.getAdapter()).valueSelectedByPosition());
        super.onSaveInstanceState(outState);
    }

    private RecyclerView createRecyclerView() {
        recyclerView = (RecyclerView) UiUtils.inflateParentLess(getBaseActivity(), R.layout.dialog_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        recyclerView.setAdapter(new FiltersAlertDialogAdapter(getBaseActivity(), maxTermOptions(), previousSelection));
        return recyclerView;
    }

    private AlertDialogBuilder createDialog(View customView) {
        AlertDialogBuilder builder = new AlertDialogBuilder(getBaseActivity(), customView);
        builder.setTitle(getString(R.string.maximumTerm));
        builder.setPositiveButton(R.string.confirm, (dialog, which) -> {
            int selectedPosition = ((FiltersAlertDialogAdapter) ((RecyclerView) customView).getAdapter()).valueSelectedByPosition();
            viewModel.filterAvailabilityMaxTerm().saveValues(position, MaxTerm.values()[selectedPosition]);
            dismiss();
        });
        builder.setNegativeButton(R.string.cancel, (dialog, which) -> dismiss());
        return builder;
    }

    private ArrayList<String> maxTermOptions() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < MaxTerm.values().length; i++) {
            list.add(
                    (i == 0)
                            ? getString(R.string.noPreference)
                            : subtitleUtil.getTermText(getBaseActivity(), MaxTerm.values()[i].getIntValue()));
        }
        return list;
    }

}
