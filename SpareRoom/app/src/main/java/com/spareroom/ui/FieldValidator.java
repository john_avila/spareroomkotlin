package com.spareroom.ui;

/**
 * Created by manuel on 04/11/2016.
 */

public interface FieldValidator {

    int validate(String field);
}
