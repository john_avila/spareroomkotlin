package com.spareroom.ui.screen;

import android.content.Intent;
import android.os.Bundle;

import com.spareroom.R;
import com.spareroom.model.business.AccountCredentials;
import com.spareroom.ui.flow.*;

import static com.spareroom.ui.screen.NewExistingFragment.EXTRA_ACTION_CODE;

/**
 * Sing In screen for the Material design
 */
public class LoginActivity extends LinearMultiStepActivity {

    //region FIELDS CONTROLLER

    protected static final int REQUEST_CODE_REGISTER = 9842;

    // The value of this field cannot be the same value as RegisterActivity.KEY_REGISTER_FORM
    public static final int CREDENTIALS = 0;

    private String _pageJoinedFrom = null;

    //endregion FIELDS CONTROLLER

    //region METHODS CONTROLLER

    public String getPageJoinedFrom() {
        return _pageJoinedFrom;
    }

    public void setActivityResultOK() {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_ACTION_CODE, getIntent().getStringExtra(EXTRA_ACTION_CODE));
        setResult(Activity.RESULT_OK, intent);

    } //end setActivityResultOK()

    //endregion METHODS CONTROLLER

    //region METHODS LIFECYCLE

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.material_activity);

        FlowState registerFlow = new LoginFlow();
        FlowManager flowManager = new FlowManager();
        flowManager.setState(registerFlow);
        setFlowManager(flowManager);

        AccountCredentials accountCredentials = new AccountCredentials();
        Intent intent = getIntent();

        _pageJoinedFrom = intent.getStringExtra(NewExistingActivity.PAGE_JOINED_FROM);

        // If it comes from NewExistingActivity
        if (intent.hasExtra(NewExistingActivity.PLACEAD_EMAIL))
            accountCredentials.setUsername(getIntent().getStringExtra(NewExistingActivity.PLACEAD_EMAIL));

        setSharedObject(LoginActivity.CREDENTIALS, accountCredentials);

        // Sets the first fragment
        nextFragment();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == REQUEST_CODE_REGISTER) && (resultCode == Activity.RESULT_OK)) {
            finish();
        } else {
            Fragment fragment = (Fragment) getSupportFragmentManager().findFragmentById(R.id.activity_fragment);
            fragment.onActivityResult(requestCode, resultCode, data);
        }

    }

    //endregion METHODS LIFECYCLE

}
