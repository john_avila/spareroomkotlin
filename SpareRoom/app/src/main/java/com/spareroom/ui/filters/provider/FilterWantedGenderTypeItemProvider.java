package com.spareroom.ui.filters.provider;

import com.spareroom.R;
import com.spareroom.model.business.SearchAdvertListProperties;
import com.spareroom.model.business.SearchAdvertListProperties.GenderFilter;
import com.spareroom.model.business.SearchAdvertListPropertiesWanted;
import com.spareroom.ui.filters.adapter.viewparams.GroupedCompoundButtonParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams;
import com.spareroom.ui.filters.adapter.viewparams.SpaceViewParams.SpaceType;
import com.spareroom.ui.widget.params.CompoundButtonParams;

import java.util.*;

import androidx.annotation.NonNull;

class FilterWantedGenderTypeItemProvider extends AbstractItemProvider {
    private static final int TITLE_ID = R.string.genderSexuality;
    private final static String KEY_GENDER_GROUP = "key_gender_group";

    private final static String MIXED = "mixed";
    final static String MALES = "males";
    final static String FEMALES = "females";
    final static String NO_PREFERENCE = "no_Preference";
    final static String LGBTQ = "lgbtq";

    private final List<GroupedCompoundButtonParams> genderGroup = new ArrayList<>();
    private final Map<String, List<GroupedCompoundButtonParams>> groupList = new HashMap<>();

    @Override
    public int titleId() {
        return TITLE_ID;
    }

    @Override
    public void createNewList(@NonNull SearchAdvertListProperties properties, String filterName) {
        super.createNewList(properties, filterName);

        SearchAdvertListPropertiesWanted previousProperties = (SearchAdvertListPropertiesWanted) properties;
        List<FilterItem> list = new ArrayList<>();
        genderGroup.clear();
        groupList.clear();

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        GroupedCompoundButtonParams females = new GroupedCompoundButtonParams(FEMALES);
        females.size(CompoundButtonParams.Size.MEDIUM);
        females.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        females.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        females.enableHighlightBar(true);
        females.text(application.getString(R.string.females));
        females.textColorResId(R.color.black);
        females.iconResId(R.drawable.ic_female_user);
        females.value(GenderFilter.FEMALES.toString());
        females.groupName(KEY_GENDER_GROUP);
        genderGroup.add(females);
        list.add(new FilterItem<>(females));

        GroupedCompoundButtonParams males = new GroupedCompoundButtonParams(MALES);
        males.size(CompoundButtonParams.Size.MEDIUM);
        males.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        males.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        males.enableHighlightBar(true);
        males.text(application.getString(R.string.males));
        males.textColorResId(R.color.black);
        males.iconResId(R.drawable.ic_male_user);
        males.value(GenderFilter.MALES.toString());
        males.groupName(KEY_GENDER_GROUP);
        genderGroup.add(males);
        list.add(new FilterItem<>(males));

        GroupedCompoundButtonParams mixed = new GroupedCompoundButtonParams(MIXED);
        mixed.size(CompoundButtonParams.Size.MEDIUM);
        mixed.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        mixed.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        mixed.enableHighlightBar(true);
        mixed.text(application.getString(R.string.mixed));
        mixed.textColorResId(R.color.black);
        mixed.iconResId(R.drawable.ic_couple);
        mixed.value(GenderFilter.MIXED.toString());
        mixed.groupName(KEY_GENDER_GROUP);
        genderGroup.add(mixed);
        list.add(new FilterItem<>(mixed));

        GroupedCompoundButtonParams noPreference = new GroupedCompoundButtonParams(NO_PREFERENCE);
        noPreference.size(CompoundButtonParams.Size.MEDIUM);
        noPreference.compoundButtonType(CompoundButtonParams.CompoundButtonType.RADIO_BUTTON);
        noPreference.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.GRAY);
        noPreference.enableHighlightBar(false);
        noPreference.text(application.getString(R.string.noPreference));
        noPreference.textColorResId(R.color.black);
        noPreference.value(GenderFilter.NOT_SET.toString());
        noPreference.groupName(KEY_GENDER_GROUP);
        noPreference.defaultItem(true);
        genderGroup.add(noPreference);
        list.add(new FilterItem<>(noPreference));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.SMALL)));

        CompoundButtonParams lgbtq = new CompoundButtonParams(LGBTQ);
        lgbtq.size(CompoundButtonParams.Size.MEDIUM);
        lgbtq.compoundButtonType(CompoundButtonParams.CompoundButtonType.CHECK_BOX);
        lgbtq.compoundButtonColorStateList(CompoundButtonParams.CompoundButtonColorStateList.ORANGE);
        lgbtq.enableHighlightBar(true);
        lgbtq.text(application.getString(R.string.lgbtq));
        lgbtq.textColorResId(R.color.black);
        lgbtq.iconResId(R.drawable.ic_generic_user);
        lgbtq.selected(previousProperties.getGayShare());
        list.add(new FilterItem<>(lgbtq));

        list.add(new FilterItem<>(new SpaceViewParams(SpaceType.BIG)));

        groupList.put(KEY_GENDER_GROUP, genderGroup);
        setGroupPreviousSearch(previousProperties);

        this.list = list;
    }

    private void setGroupPreviousSearch(SearchAdvertListPropertiesWanted properties) {
        Set<String> groupListKeySet = groupList.keySet();

        for (String key : groupListKeySet) {
            List<GroupedCompoundButtonParams> group = groupList.get(key);

            for (GroupedCompoundButtonParams params : group) {
                switch (key) {
                    case KEY_GENDER_GROUP:
                        params.selected((properties.getGenderFilter() != GenderFilter.NOT_SET)
                                && params.value().equals(properties.getGenderFilter().toString()));
                        break;
                }
            }
            selectedItem(group);
        }

    }

    @Override
    public Map<String, List<GroupedCompoundButtonParams>> groupList() {
        return groupList;
    }

    @Override
    public void updateProperties(SearchAdvertListProperties propertiesToUpdate, boolean sortProperties) {
        SearchAdvertListPropertiesWanted propertiesWanted = (SearchAdvertListPropertiesWanted) propertiesToUpdate;
        for (FilterItem item : list) {
            switch (item.tag()) {
                case LGBTQ:
                    propertiesWanted.setGayShare(((CompoundButtonParams) item.params()).selected());
                    break;
            }
        }

        propertiesWanted.setGenderFilter(GenderFilter.valueOf(selectedValue(groupList.get(KEY_GENDER_GROUP))));

    }

}
