package com.spareroom.ui.widget

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.spareroom.R
import com.spareroom.ui.util.hide
import kotlinx.android.synthetic.main.advert_card_view_skeleton.view.*

class AdvertCardViewSkeleton : ConstraintLayout {

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private fun init(context: Context) {
        inflate(context, R.layout.advert_card_view_skeleton, this)
    }

    fun hideHeader() {
        userAvatar.hide()
        userName.hide()
        moreIcon.hide()
        favouriteIcon.hide()
    }

    fun hideFooter() {
        budgetLabel.hide()
        budget.hide()
        depositLabel.hide()
        deposit.hide()
        availabilityLabel.hide()
        availability.hide()
    }

    fun hideBottomDivider() {
        bottomDivider.hide()
    }

}
