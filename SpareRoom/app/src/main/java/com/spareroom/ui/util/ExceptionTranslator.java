package com.spareroom.ui.util;

import android.content.Context;

import com.spareroom.R;
import com.spareroom.integration.exception.InvalidArgumentException;
import com.spareroom.integration.exception.UnavailableDataSourceException;
import com.spareroom.integration.exception.UnexpectedResultException;
import com.spareroom.model.exception.ModelInvalidArgumentException;
import com.spareroom.model.exception.ModelOperationCannotBePerformedException;
import com.spareroom.model.exception.ModelUnexpectedInternalIssueException;

/**
 * Exceptions translator
 * <p/>
 * Created by miguel.rossi on 31/03/2017.
 */
public class ExceptionTranslator {

    private ExceptionTranslator() {
    }

    /**
     * Choose an intelligible  message for show to the user in the UI
     *
     * @param c - you know ;)
     * @param e - Exception to translate
     * @return - The intelligible message
     */
    public static String translate(Context c, Exception e) {
        int userMessage = R.string.exception_message_generic;

        if (e instanceof UnavailableDataSourceException
                || e instanceof ModelOperationCannotBePerformedException) {

            userMessage = R.string.exception_message_unavailableDataSourceException;

        } else if (e instanceof InvalidArgumentException
                || e instanceof ModelInvalidArgumentException) {

            userMessage = R.string.exception_message_invalidArgumentException;

        } else if (e instanceof UnexpectedResultException
                || e instanceof ModelUnexpectedInternalIssueException) {

            userMessage = R.string.exception_message_unexpectedResultException;

        }

        return c.getString(userMessage);
    }

}
