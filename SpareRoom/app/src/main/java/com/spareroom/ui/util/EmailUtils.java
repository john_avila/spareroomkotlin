package com.spareroom.ui.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.controller.SpareroomApplication;

/**
 * Different ways of sending an email to CS
 */

public class EmailUtils {
    private final static String TYPE = "message/rfc822";

    public static void reportAdvert(Context context, String advertId) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{AppVersion.flavor().getCsEmail()});
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.email_report_body, advertId));
        sendIntent.setType(TYPE);

        context.startActivity(Intent.createChooser(sendIntent, context.getString(R.string.report)));

    }

    public static void feedbackEmail(Context context) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        ComponentName componentName;
        String body =
                context.getString(R.string.home_feedBack_body_text)
                        + "\n\n"
                        + getSystemInfo(context)
                        + context.getString(R.string.home_feedBack_body_feedback);

        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{AppVersion.flavor().getCsEmail()});
        intent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.home_feedBack_subject));
        intent.putExtra(Intent.EXTRA_TEXT, body);

        componentName = intent.resolveActivity(context.getPackageManager());
        // "com.android.fallback.Fallback" when using the app in an emulator
        if (componentName != null && !"com.android.fallback.Fallback".equals(componentName.getClassName())) {
            context.startActivity(intent);

        } else {
            Toast.makeText(context, context.getString(R.string.home_feedBack_noApp), Toast.LENGTH_SHORT).show();

        }

    }

    private static String getSystemInfo(Context context) {
        String userId = SpareroomApplication.getSpareRoomContext().getUserId();

        return context.getString(R.string.report_systemInfo_header)
                + android.os.Build.VERSION.RELEASE
                + " | "
                + StringUtils.capitalizeEachFirstLetter(android.os.Build.MANUFACTURER)
                + " "
                + android.os.Build.MODEL
                + " | "
                + AppVersion.versionName()
                + (!StringUtils.isNullOrEmpty(userId) ? " | #" + userId : "")
                + "\n\n";
    }

}
