package com.spareroom.ui.screen;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.spareroom.R;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.viewmodel.ConversationViewModel;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

public class SendMessageConfirmationDialog extends DialogFragment implements Injectable {

    public static final String TAG = SendMessageConfirmationDialog.class.getName() + "Tag";

    private static final String USER_NAME_KEY = "USERNAME_KEY";

    private ConversationViewModel viewModel;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    public static SendMessageConfirmationDialog getInstance(String userName) {
        Bundle args = new Bundle();
        args.putString(USER_NAME_KEY, userName);

        SendMessageConfirmationDialog dialogFragment = new SendMessageConfirmationDialog();
        dialogFragment.setArguments(args);

        return dialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(ConversationViewModel.class);
        setCancelable(true);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        viewModel.getEditNewMessageConfirmation().edit();
        dismiss();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        final String userName = args.getString(USER_NAME_KEY);

        AlertDialogBuilder builder = new AlertDialogBuilder(getActivity());
        builder.setTitle(R.string.confirm);
        builder.setMessage(getString(R.string.fragmentConversation_externalLinksWarning, userName));
        builder.setPositiveButton(R.string.send_anyway, (dialog, which) -> {
            viewModel.getSendNewMessageConfirmation().send();
            dismiss();
        });

        builder.setNegativeButton(R.string.edit, (dialog, which) -> {
            viewModel.getEditNewMessageConfirmation().edit();
            dismiss();
        });

        return builder.create();
    }
}
