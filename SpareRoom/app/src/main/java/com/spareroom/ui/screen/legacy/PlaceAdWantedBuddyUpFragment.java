package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.ui.widget.PlaceAdButton;

public class PlaceAdWantedBuddyUpFragment extends PlaceAdFragment {
    private PlaceAdButton _previouslySelected;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_wanted_buddy_up, container, false);

        setTitle(getString(R.string.place_ad_wanted_buddy_up));

        final PlaceAdButton bYes = rootView.findViewById(R.id.place_ad_wanted_buddy_up_bYes);
        final PlaceAdButton bNo = rootView.findViewById(R.id.place_ad_wanted_buddy_up_bNo);

        Boolean isInterestedInBuddyUp;
        final DraftAdWanted draft = ((DraftAdWanted) getDraftAd());

        // In case there is an activity state change and we lost the draft
        if (draft == null) {
            getActivity().finish();

            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }
        isInterestedInBuddyUp = draft.is_isInterestedInBuddyingUp();

        if (isInterestedInBuddyUp != null)
            if (isInterestedInBuddyUp) {
                bYes.setPreviouslySelected();
                _previouslySelected = bYes;
            } else {
                bNo.setPreviouslySelected();
                _previouslySelected = bNo;
            }

        bYes.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bYes.setPreviouslySelected();
            draft.set_isInterestedInBuddyingUp(true);
            PlaceAdWantedBuddyUpFragment.this.finish();
        });

        bNo.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bNo.setPreviouslySelected();
            draft.set_isInterestedInBuddyingUp(false);
            PlaceAdWantedBuddyUpFragment.this.finish();
        });

        return rootView;
    }
}
