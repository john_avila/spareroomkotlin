package com.spareroom.ui.screen;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.*;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.*;
import com.spareroom.integration.analytics.AnalyticsTrackerComposite;
import com.spareroom.integration.dependency.Injectable;
import com.spareroom.integration.sharedpreferences.OnboardingDao;
import com.spareroom.ui.screen.legacy.MainActivity;
import com.spareroom.ui.screen.legacy.PlaceAdActivity;
import com.spareroom.ui.util.*;
import com.spareroom.ui.widget.util.exclusivitymanager.*;

import java.util.List;

import javax.inject.Inject;

import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

/**
 * Main class for the side menu.
 */

public abstract class DrawerFragment extends Fragment implements Injectable {

    private final static int LEFT = 0;
    private final static int TOP = 0;

    private View _vBetaProgram;
    private View _vLogOutDivider;
    private View _vLogOut;
    private View _vSearch;
    private View _vMyAdverts;
    private View _vSavedAdverts;
    private View _vMessages;
    private View drawerItemsContainer;

    private final static int COLOR_NORMAL = R.color.grey_2;

    protected final static String HIGHLIGHT_OPTION = "home";
    public final static String HIGHLIGHT_HOME = "home";
    public final static String HIGHLIGHT_MESSAGES = "messages";
    protected final static String HIGHLIGHT_SAVED_ADVERTS = "savedAdverts";
    protected final static String HIGHLIGHT_MY_ADVERTS = "myAdverts";

    private final ExclusivityManager _exclusivityManager = new ExclusivityManager();
    protected DrawerLayout _drawerLayout;

    @Inject
    ConnectivityChecker connectivityChecker;

    /**
     * Selects the selected option by default in the menu
     */
    public void activateDefaultOption() {
        AnalyticsTrackerComposite.getInstance().trackEvent_Menu_Search(true);

        try {
            _exclusivityManager.activate(_vSearch);
        } catch (ExclusiveViewNotFoundException e) {
            // it shouldn't happen. If happens, is the app stopped?
        }

        launchSearch();
        _drawerLayout.closeDrawer(GravityCompat.START);
    }

    public void highlightConversationList() {
        highlight(HIGHLIGHT_MESSAGES);
    }

    /**
     * Shows or hides the options for the logged in status
     */
    protected void setLogInViews(boolean visible) {
        int visibility = visible ? View.VISIBLE : View.GONE;
        _vBetaProgram.setVisibility(AppVersion.isUk() && visible ? View.VISIBLE : View.GONE);
        _vLogOutDivider.setVisibility(visibility);
        _vLogOut.setVisibility(visibility);
    }

    /**
     * Sets the selected header
     *
     * @param headerId the header layout id
     */
    protected void setHeader(int headerId) {
        RelativeLayout vHolder = getActivity().findViewById(R.id.navigation_header);
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View vHeader = inflater.inflate(headerId, null);
        RelativeLayout.LayoutParams layoutParams =
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        if (vHolder.getChildCount() > 0)
            vHolder.removeAllViews();

        layoutParams.setMargins(0, UiUtils.isLollipopOrNewer() ? UiUtils.statusBarHeight(getActivity()) : 0, 0, 0);
        vHeader.setLayoutParams(layoutParams);
        vHolder.addView(vHeader);

    }

    /**
     * Get rid of all the "SavedSearches" process when that screen is redesigned
     */
    @Deprecated
    abstract void launchSavedSearches();

    abstract void launchMessages();

    abstract void launchSavedAdverts();

    abstract void launchMyAdverts();

    abstract void launchEditMyDetails();

    abstract void launchUpgrade();

    private void launchSearch() {
        if (!UiUtils.isFragmentAlive(this))
            return;

        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            HomeFragment homeFragment = (HomeFragment) fragmentManager.findFragmentByTag(HomeFragment.TAG);
            if (homeFragment == null || homeFragment.loggedIn() != SpareroomApplication.getSpareRoomContext().isUserLoggedIn()) {
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.main_frame, HomeFragment.getInstance(), HomeFragment.TAG);
                transaction.commit();
            }

            if (getActivity() != null)
                ((MainActivity) getActivity()).notifyDrawerChanged(HomeFragment.TAG);
        }
    }

    private void launchPlaceAdvert() {
        Intent intent = new Intent();
        intent.setClass(getActivity(), PlaceAdActivity.class);
        startActivity(intent);

    }

    private void launchSupportEmail() {
        EmailUtils.feedbackEmail(getContext());
    }

    private void launchSupportPhone() {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + AppVersion.flavor().getPhone()));

        if (isCallsApp(callIntent))
            getActivity().startActivity(callIntent);
        else {
            Toast.makeText(getActivity(), getString(R.string.navigationDrawer_supportPhone_noApp), Toast.LENGTH_LONG).show();
        }

    }

    private void launchBetaProgram() {
        Activity activity = getActivity();
        if (activity != null)
            WebLauncher.launchBetaProgram(activity);
    }

    private void launchLogOut() {
        AnalyticsTrackerComposite.getInstance().trackEvent_Menu_LogOut(true);
        LogOutControllerAction logOutControllerAction =
                new LogOutControllerAction(SpareroomApplication.getInstance(getActivity().getApplicationContext()).getSpareroomContext());

        logOutControllerAction.execute();

        ToastUtils.showToast(R.string.signed_out);

        drawerItemsContainer.scrollTo(LEFT, TOP);
        activateDefaultOption();

        // Shows again the Onboarding when starting
        if (AppVersion.isUs()) {
            OnboardingDao.delete(getActivity());
            startActivity(new Intent(getActivity(), OnboardingActivity.class));
            getActivity().finish();
        }

        AnalyticsTrackerComposite.getInstance().trackEvent_Account_Logout();

    }

    /**
     * Highlights an specific menu option
     *
     * @param selection constant for choosing
     */
    protected void highlight(String selection) {
        try {
            switch (selection) {
                case HIGHLIGHT_HOME:
                    _exclusivityManager.activate(_vSearch);
                    break;
                case HIGHLIGHT_MESSAGES:
                    _exclusivityManager.activate(_vMessages);
                    break;
                case HIGHLIGHT_SAVED_ADVERTS:
                    _exclusivityManager.activate(_vSavedAdverts);
                    break;
                case HIGHLIGHT_MY_ADVERTS:
                    _exclusivityManager.activate(_vMyAdverts);
                    break;
            }

        } catch (ExclusiveViewNotFoundException e) {
            // it shouldn't happen. If happens, is the app stopped?
        }
    }

    /**
     * Checks if there is any app that can handle the call in the device
     *
     * @param intent of the call
     * @return <code>true</code> if yes; <code>false</code> otherwise
     */
    private boolean isCallsApp(Intent intent) {
        PackageManager packageManager = getActivity().getPackageManager();
        List activities = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return activities.size() > 0;

    }

    private void setUpDrawerItem(View viewList, int text, int iconId) {
        setUpDrawerItem(viewList, text, iconId, false);
    }

    private void setUpDrawerItem(View viewList, int text, int iconId, boolean hideIcon) {
        TextView textView = viewList.findViewById(R.id.drawer_item_title);
        textView.setText(getString(text));
        textView.setTextColor(ContextCompat.getColor(getActivity(), COLOR_NORMAL));

        ImageView imageView = viewList.findViewById(R.id.drawer_item_icon);
        imageView.setImageResource(iconId);
        imageView.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(getActivity(), COLOR_NORMAL), PorterDuff.Mode.SRC_IN));
        imageView.setVisibility(hideIcon ? View.GONE : View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        _drawerLayout = getActivity().findViewById(R.id.drawer_layout);
        drawerItemsContainer = container.findViewById(R.id.drawerItemsContainer);

        _vSearch = container.findViewById(R.id.navigationDrawer_search);
        View vSavedSearches = container.findViewById(R.id.navigationDrawer_savedSearches);
        _vMessages = container.findViewById(R.id.navigationDrawer_messages);
        _vSavedAdverts = container.findViewById(R.id.navigationDrawer_savedAdverts);
        View vPlaceAdvert = container.findViewById(R.id.navigationDrawer_placeAdvert);
        _vMyAdverts = container.findViewById(R.id.navigationDrawer_myAdverts);
        View vEditMyDetails = container.findViewById(R.id.navigationDrawer_editMyDetails);
        View vUpgrade = container.findViewById(R.id.navigationDrawer_upgrade);
        View vSupportEmail = container.findViewById(R.id.navigationDrawer_supportEmail);
        View vSupportPhone = container.findViewById(R.id.navigationDrawer_supportPhone);
        _vBetaProgram = container.findViewById(R.id.navigationDrawer_beta_program);
        _vLogOut = container.findViewById(R.id.navigationDrawer_logOut);
        _vLogOutDivider = container.findViewById(R.id.navigationDrawer_logOut_divider);

        setUpDrawerItem(_vSearch, R.string.search, R.drawable.ic_search);
        setUpDrawerItem(vSavedSearches, R.string.saved_searches, R.drawable.ic_saved_searches_layer_list);
        setUpDrawerItem(_vMessages, R.string.navigationDrawer_messages, R.drawable.ic_messages);
        setUpDrawerItem(_vSavedAdverts, R.string.saved_adverts, R.drawable.ic_favourites);
        setUpDrawerItem(vPlaceAdvert, R.string.post_ad, R.drawable.ic_place_advert);
        setUpDrawerItem(_vMyAdverts, R.string.my_adverts, R.drawable.ic_my_adverts);
        setUpDrawerItem(vEditMyDetails, R.string.my_details, R.drawable.ic_my_details);
        setUpDrawerItem(vUpgrade, R.string.upgrade, R.drawable.ic_upgrade);
        setUpDrawerItem(vSupportEmail, R.string.navigationDrawer_supportEmail, R.drawable.ic_email);
        setUpDrawerItem(vSupportPhone, R.string.navigationDrawer_supportPhone, R.drawable.ic_phone);
        setUpDrawerItem(_vBetaProgram, R.string.beta_programme, 0, true);
        setUpDrawerItem(_vLogOut, R.string.sign_out, 0, true);

        // Adding items to the exclusivity manager
        _exclusivityManager.add(new ItemActivationCommand(_vSearch));
        _exclusivityManager.add(new ItemActivationCommand(vSavedSearches));
        _exclusivityManager.add(new ItemActivationCommand(_vMessages));
        _exclusivityManager.add(new ItemActivationCommand(_vSavedAdverts));
        _exclusivityManager.add(new ItemActivationCommand(_vMyAdverts));
        _exclusivityManager.add(new ItemActivationCommand(_vBetaProgram));

        // On click listeners
        _vSearch.setOnClickListener(v -> {
            AnalyticsTrackerComposite.getInstance().trackEvent_Menu_Search(true);

            try {
                _exclusivityManager.activate(v);
            } catch (ExclusiveViewNotFoundException e) {
                // it shouldn't happen. If happens, is the app stopped?
            }

            launchSearch();
            _drawerLayout.closeDrawer(GravityCompat.START);
        });

        vSavedSearches.setOnClickListener(new View.OnClickListener() {
            @Deprecated
            @Override
            public void onClick(View v) {
                AnalyticsTrackerComposite.getInstance().trackEvent_Menu_SavedSearches(true);

                launchSavedSearches();
                _drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        _vMessages.setOnClickListener(v -> {
            AnalyticsTrackerComposite.getInstance().trackEvent_Menu_Messages(true);

            try {
                _exclusivityManager.activate(v);
            } catch (ExclusiveViewNotFoundException e) {
                // it shouldn't happen. If happens, is the app stopped?
            }

            launchMessages();
            _drawerLayout.closeDrawer(GravityCompat.START);
        });

        _vSavedAdverts.setOnClickListener(v -> {
            AnalyticsTrackerComposite.getInstance().trackEvent_Menu_SavedAds(true);

            try {
                _exclusivityManager.activate(v);
            } catch (ExclusiveViewNotFoundException e) {
                // it shouldn't happen. If happens, is the app stopped?
            }

            launchSavedAdverts();
            _drawerLayout.closeDrawer(GravityCompat.START);
        });

        vPlaceAdvert.setOnClickListener(v -> {
            AnalyticsTrackerComposite.getInstance().trackEvent_Menu_PlaceAdvert(true);

            try {
                _exclusivityManager.activate(v);
            } catch (ExclusiveViewNotFoundException e) {
                // it shouldn't happen. If happens, is the app stopped?
            }

            launchPlaceAdvert();
            _drawerLayout.closeDrawer(GravityCompat.START);
        });

        _vMyAdverts.setOnClickListener(v -> {
            AnalyticsTrackerComposite.getInstance().trackEvent_Menu_MyAdverts(true);

            try {
                _exclusivityManager.activate(v);
            } catch (ExclusiveViewNotFoundException e) {
                // it shouldn't happen. If happens, is the app stopped?
            }

            launchMyAdverts();
            _drawerLayout.closeDrawer(GravityCompat.START);
        });

        vEditMyDetails.setOnClickListener(v -> {
            AnalyticsTrackerComposite.getInstance().trackEvent_Menu_EditMyDetails(true);

            launchEditMyDetails();
            _drawerLayout.closeDrawer(GravityCompat.START);
        });

        vUpgrade.setOnClickListener(v -> {
            AnalyticsTrackerComposite.getInstance().trackEvent_Menu_Upgrade(true);

            launchUpgrade();
            _drawerLayout.closeDrawer(GravityCompat.START);
        });

        vSupportEmail.setOnClickListener(v -> {
            AnalyticsTrackerComposite.getInstance().trackEvent_Menu_ContactUsMail(true);

            launchSupportEmail();
            _drawerLayout.closeDrawer(GravityCompat.START);
        });

        vSupportPhone.setOnClickListener(v -> {
            AnalyticsTrackerComposite.getInstance().trackEvent_Menu_ContactUsPhone(true);

            launchSupportPhone();
            _drawerLayout.closeDrawer(GravityCompat.START);
        });

        _vBetaProgram.setOnClickListener(v -> {
            AnalyticsTrackerComposite.getInstance().trackEvent_Menu_BetaProgramme();
            launchBetaProgram();
            _drawerLayout.closeDrawer(GravityCompat.START);
        });

        _vLogOut.setOnClickListener(v -> {
            AnalyticsTrackerComposite.getInstance().trackEvent_Menu_LogOut(true);

            launchLogOut();
            _drawerLayout.closeDrawer(GravityCompat.START);
        });

        // Highlights the default option in the menu
        highlight(getArguments().getString(HIGHLIGHT_OPTION, HIGHLIGHT_HOME));

        return null;
    }

    protected boolean isOffline() {
        if (!connectivityChecker.isConnected()) {
            ToastUtils.showToast(R.string.no_connection);
            return true;
        }

        return false;
    }

    private class ItemActivationCommand extends ViewActivationCommand {
        private final TextView _textView;
        private final ImageView _imageView;

        private ItemActivationCommand(View v) {
            super(v);
            _imageView = v.findViewById(R.id.drawer_item_icon);
            _textView = v.findViewById(R.id.drawer_item_title);
        }

        @Override
        public void activate() {
            changeColor(R.color.orange);
        }

        @Override
        public void deactivate() {
            changeColor(COLOR_NORMAL);
        }

        private void changeColor(int newColor) {
            _imageView.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(getActivity(), newColor), PorterDuff.Mode.SRC_IN));
            _textView.setTextColor(ContextCompat.getColor(_textView.getContext(), newColor));
        }

    }

}
