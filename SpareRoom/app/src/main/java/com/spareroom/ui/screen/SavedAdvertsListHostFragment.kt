package com.spareroom.ui.screen

import android.os.Bundle
import android.view.*
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import com.spareroom.R
import com.spareroom.ui.adapter.SavedAdvertsPagerAdapter
import com.spareroom.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.app_bar_view_tabs.*
import kotlinx.android.synthetic.main.saved_adverts_host_fragment.*

class SavedAdvertsListHostFragment : Fragment() {

    companion object {

        const val TAG = "SavedAdvertsHostFragmentTag"

        @JvmStatic
        fun getInstance() = SavedAdvertsListHostFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.saved_adverts_host_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        setUpToolbar(view)
        setUpViewPager()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
    }

    private fun setUpToolbar(view: View) {
        setToolbar(view, getString(R.string.saved_adverts))
        ViewModelProviders.of(activity!!).get(MainActivityViewModel::class.java).syncDrawer.sync()
    }

    private fun setUpViewPager() {
        advertsViewPager.adapter = SavedAdvertsPagerAdapter(childFragmentManager, activity!!)
        tabLayout.tabMode = TabLayout.MODE_FIXED
        tabLayout.setupWithViewPager(advertsViewPager)
    }
}