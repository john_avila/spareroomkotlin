package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdWanted;
import com.spareroom.ui.widget.PlaceAdButton;

public class PlaceAdWantedOccupationFragment extends PlaceAdFragment {
    private PlaceAdButton _previouslySelected;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_ad_wanted_occupation, container, false);

        setTitle(getString(R.string.place_ad_wanted_occupation_title));

        final PlaceAdButton bStudent = rootView.findViewById(R.id.place_ad_wanted_occupation_bStudent);
        final PlaceAdButton bProfessional = rootView.findViewById(R.id.place_ad_wanted_occupation_bProfessional);
        final PlaceAdButton bOther = rootView.findViewById(R.id.place_ad_wanted_occupation_bOther);
        final DraftAdWanted draft = ((DraftAdWanted) getDraftAd());

        // In case there is an activity state change and we lost the draft
        if (draft == null) {
            getActivity().finish();

            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();

            return null;
        }

        DraftAdWanted.Occupation occupation = draft.get_occupation();

        if (occupation != null)
            switch (occupation) {
                case PROFESSIONAL:
                    _previouslySelected = bProfessional;
                    bProfessional.setPreviouslySelected();
                    break;
                case STUDENT:
                    _previouslySelected = bStudent;
                    bStudent.setPreviouslySelected();
                    break;
                case OTHER:
                    _previouslySelected = bOther;
                    bOther.setPreviouslySelected();
                    break;
                default:
            }

        bStudent.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bStudent.setPreviouslySelected();
            draft.set_occupation(DraftAdWanted.Occupation.STUDENT);
            PlaceAdWantedOccupationFragment.this.finish();
        });

        bProfessional.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bProfessional.setPreviouslySelected();
            draft.set_occupation(DraftAdWanted.Occupation.PROFESSIONAL);
            PlaceAdWantedOccupationFragment.this.finish();
        });

        bOther.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            bOther.setPreviouslySelected();
            draft.set_occupation(DraftAdWanted.Occupation.OTHER);
            PlaceAdWantedOccupationFragment.this.finish();
        });

        return rootView;
    }
}
