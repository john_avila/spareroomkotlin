package com.spareroom.ui.adapter.conversation;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

class ConversationClosedViewHolder extends RecyclerView.ViewHolder {

    ConversationClosedViewHolder(View itemView) {
        super(itemView);
    }
}