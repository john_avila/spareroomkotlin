package com.spareroom.ui.screen.legacy;

import android.os.Bundle;
import android.view.*;
import android.widget.Toast;

import com.spareroom.R;
import com.spareroom.model.business.DraftAdOffered;
import com.spareroom.ui.widget.PlaceAdButton;

/**
 * Offered place add: broadband fragment.
 */
public class PlaceAdOfferedBroadBandFragment extends PlaceAdFragment {

    //region FIELDS

    private PlaceAdButton _previouslySelected;

    //endregion FIELDS

    //region METHODS OVERRIDDEN

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.place_ad_offered_broadband, container, false);

        setTitle(getString(R.string.place_ad_offered_broadband_title));

        final PlaceAdButton buttonYes = rootView.findViewById(R.id.placeAdOffered_broadband_buttonYes);
        final PlaceAdButton buttonNo = rootView.findViewById(R.id.placeAdOffered_broadband_buttonNo);

        final DraftAdOffered draft = ((DraftAdOffered) getDraftAd());

        // In case there is an activity state change and we lost the draft
        if (draft == null) {
            getActivity().finish();
            Toast.makeText(getActivity(), getString(R.string.editAdActivity_notAbleToEdit_toast), Toast.LENGTH_LONG).show();
            return null;
        }

        // If there is data from a previous advert process it is shown
        if (draft.isBroadband() != null) {
            if (draft.isBroadband()) {
                buttonYes.setPreviouslySelected();
                _previouslySelected = buttonYes;
            } else {
                buttonNo.setPreviouslySelected();
                _previouslySelected = buttonNo;
            }
        }

        buttonYes.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            buttonYes.setPreviouslySelected();
            draft.setBroadband(true);
            finish();
        });

        buttonNo.setOnClickListener(v -> {
            if (_previouslySelected != null)
                _previouslySelected.setPreviouslyNotSelected();
            buttonNo.setPreviouslySelected();
            draft.setBroadband(false);
            finish();
        });

        return rootView;
    }

    //endregion METHODS OVERRIDDEN

}
