package com.spareroom.ui.filters.adapter.viewparams;

import com.spareroom.ui.widget.params.ViewParamsType;

public class TitleViewParams implements IResettableParams {
    private String title;
    private String tag;

    public TitleViewParams(String tag) {
        this.tag = tag;
    }

    public String title() {
        return title;
    }

    public void title(String title) {
        this.title = title;
    }

    @Override
    public String tag() {
        return tag;
    }

    @Override
    public ViewParamsType type() {
        return ViewParamsType.TITLE;
    }

    @Override
    public void reset() {

    }

}
