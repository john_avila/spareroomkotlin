package com.spareroom.ui.filters.adapter.viewparams;

import com.spareroom.ui.widget.params.ViewParamsType;

public abstract class AbstractSummaryViewParams implements IResettableParams {
    private int iconDrawableId;
    private int titleStringId;
    private String defaultSubtitleString;
    private String subtitleString;
    private SummaryAction summaryAction = SummaryAction.DEFAULT;

    private String tag;

    public AbstractSummaryViewParams(String tag) {
        this.tag = tag;
    }

    public ViewParamsType type() {
        return ViewParamsType.SUMMARY;
    }

    public String tag() {
        return tag;
    }

    public enum SummaryAction {SHOW_DATE_DIALOG, SHOW_SCROLL_DIALOG, DEFAULT}

    public int iconDrawableId() {
        return iconDrawableId;
    }

    public void iconDrawableId(int iconDrawableId) {
        this.iconDrawableId = iconDrawableId;
    }

    public int titleStringId() {
        return titleStringId;
    }

    public void titleStringId(int titleStringId) {
        this.titleStringId = titleStringId;
    }

    public String defaultSubtitleString() {
        return defaultSubtitleString;
    }

    public void defaultSubtitleString(String defaultSubtitleString) {
        this.defaultSubtitleString = defaultSubtitleString;
    }

    public String subtitleString() {
        return subtitleString;
    }

    public void subtitleString(String subtitleString) {
        this.subtitleString = subtitleString;
    }

    public SummaryAction summaryAction() {
        return summaryAction;
    }

    public void summaryAction(SummaryAction summaryAction) {
        this.summaryAction = summaryAction;
    }

    public void reset() {
        subtitleString(null);
    }

}
