package com.spareroom.ui.flow;

import com.spareroom.ui.screen.RegisterPasswordFragment;
import com.spareroom.ui.screen.RegisterTypeFragment;

/**
 * Register UI flow
 * <p>
 * Created by miguel.rossi on 26/07/2016.
 */
public class RegisterStep2Flow extends FlowState {

    private static final String PAGE_REGISTER_PASSWORD = RegisterPasswordFragment.class.getName();
    private static final String PAGE_REGISTER_TYPE = RegisterTypeFragment.class.getName();

    public RegisterStep2Flow() {
        SCREENS =
                new String[]{
                        PAGE_REGISTER_PASSWORD,
                        PAGE_REGISTER_TYPE};
        init();
    } // end Register() {
}
