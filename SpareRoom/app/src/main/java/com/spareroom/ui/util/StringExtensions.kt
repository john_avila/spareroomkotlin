package com.spareroom.ui.util

fun String.filterOnlyLetters() = filter { it.isLetter() }

/**
 * @return Trimmed string containing only letters, digits and whitespaces (inside)
 */
fun String.sanitise() = filter { it.isLetterOrDigit() || it.isWhitespace() }.trim()

/**
 * @return trimmed string with all occurrences of white spaces replaced by single space
 */
fun String.removeExtraWhiteSpaces() = replace(Regex("\\s+"), " ").trim()
