package com.spareroom.ui.screen.legacy;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.view.TextureView;
import android.view.View;
import android.widget.*;

import com.spareroom.R;
import com.spareroom.controller.AppVersion;
import com.spareroom.model.business.Video;
import com.spareroom.platform.camera.MediaRecorderHelper;
import com.spareroom.ui.screen.Activity;
import com.spareroom.ui.util.PermissionUtils;
import com.spareroom.ui.util.ToastUtils;

import java.io.File;
import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

/**
 * Activity for record videos
 */
public class CameraActivity extends Activity {

    //region FIELDS UI

    private static final String INITIAL_TIME = "00:00";
    private static final int REQUEST_CODE_PREVIEW = 1235;
    private final static int PERMISSION_CAMERA = 200;
    private final CameraReadyObserver _cameraReadyObserver = new CameraReadyObserver();
    private TextureView _preview;
    private Button _bRecord;
    private ImageView _ivCamera;
    private TextView _tvRecording;
    private LinearLayout _llTime;
    private TextView _tvTimeLabel;
    private TextView _tvTimeElapsed;
    private TextView _tvTimeTotal;
    private MediaRecorderHelper _mediaRecorderHelper;
    private final TextureView.SurfaceTextureListener _surfaceTextureListener =
            new TextureView.SurfaceTextureListener() {
                @Override
                public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                    _cameraReadyObserver.notify(CameraReadyObserver.SURFACE_READY);
                    _bRecord.setOnClickListener(new RecordButtonOnClickListener());
                    initCameraSwitch(_ivCamera);
                }

                @Override
                public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

                }

                @Override
                public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                    _ivCamera.setVisibility(View.GONE);
                    // TODO: remove this if? This might not be needed as _mediaRecorderHelper is the same reference the activity has
                    if (_mediaRecorderHelper.isCameraReady()) {
                        if (_mediaRecorderHelper.isRecording()) {
                            _mediaRecorderHelper.stop();
                            _mediaRecorderHelper.step6VideoRecording();
                        } else {
                            // this is actually the same thing that _mediaRecorderHelper.step6VideoRecording();
                            _mediaRecorderHelper.step9();
                            _mediaRecorderHelper.step10();
                        }
                    }
                    return false;
                }

                @Override
                public void onSurfaceTextureUpdated(SurfaceTexture surface) {
                    if (!_mediaRecorderHelper.isRecording())
                        _ivCamera.setVisibility(View.VISIBLE);
                }
            };
    private Uri _videoUri = null;

    //endregion FIELDS UI

    //region FIELDS CONTROLLER
    private Thread _threadElapsedTime = null;
    private RecordButtonOnClickListener _recordButtonOnClickListener;

    private void updateUIStopRecording() {
        _bRecord.setBackgroundResource(R.drawable.record_button_start);
        _tvRecording.setVisibility(View.GONE);
        _llTime.setVisibility(View.GONE);
        if (_threadElapsedTime != null) {
            _threadElapsedTime.interrupt();
            _threadElapsedTime = null;
        }

    }

    private void updateUIStartRecording() {
        _bRecord.setBackgroundResource(R.drawable.record_button_stop);
        _tvRecording.setVisibility(View.VISIBLE);
        _llTime.setVisibility(View.VISIBLE);
        _tvTimeElapsed.setText(INITIAL_TIME);
        _threadElapsedTime = runTimeElapsedAnimation();
        _tvTimeLabel.setVisibility(View.VISIBLE);
        _tvTimeElapsed.setVisibility(View.VISIBLE);
        _tvTimeTotal.setVisibility(View.VISIBLE);
    }

    private void updateUIAcceptConfirmation() {
        _bRecord.setVisibility(View.VISIBLE);
    }

    private void updateUIDeclineConfirmation() {
        _bRecord.setVisibility(View.VISIBLE);
    }

    private Thread runTimeElapsedAnimation() {
        _tvTimeElapsed.setTextColor(ContextCompat.getColor(_tvTimeElapsed.getContext(), R.color.white));

        Thread threadElapsedTime = new Thread(new ElapsedTimeRunnable());
        threadElapsedTime.start();
        return threadElapsedTime;
    }

    //endregion FIELDS CONTROLLER

    //region METHODS UI

    private void startVideoPreviewActivity() {
        Intent intentVideoPreview = new Intent(CameraActivity.this, VideoPlayerActivity.class);
        intentVideoPreview.setData(_videoUri);
        startActivityForResult(intentVideoPreview, REQUEST_CODE_PREVIEW);
    }

    private void initCameraSwitch(ImageView ivCamera) {
        ivCamera.setVisibility(View.GONE);
        ivCamera.setImageResource(R.drawable.ic_camera_rear);
        ivCamera.setOnClickListener(new CameraButtonOnClickListener(ivCamera));
    }

    //endregion METHODS UI

    //region METHODS CONTROLLER

    private void endVideoRecording() {
        File f = _mediaRecorderHelper.stop();
        _mediaRecorderHelper.step6VideoRecording();
        _videoUri = Uri.fromFile(f);
        updateUIStopRecording();
        _recordButtonOnClickListener.setRecording(false);

        startVideoPreviewActivity();
    }

    //endregion METHODS CONTROLLER

    //region METHODS LIFECYCLE

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        setResult(Activity.RESULT_CANCELED);

        _preview = findViewById(R.id.activityCamera_sView);
        _bRecord = findViewById(R.id.activityCamera_bCapture);
        _ivCamera = findViewById(R.id.activityCamera_bCamera);
        _tvRecording = findViewById(R.id.activityCamera_tvRecording);
        _llTime = findViewById(R.id.activityCamera_llTime);
        _tvTimeLabel = findViewById(R.id.activityCamera_tvTimeLabel);
        _tvTimeElapsed = findViewById(R.id.activityCamera_tvTimeElapsed);
        _tvTimeTotal = findViewById(R.id.activityCamera_tvTimeTotal);

        _tvTimeElapsed.setText(INITIAL_TIME);
        int minutes = (Video.RECOMMENDED_LENGTH_IN_MILLIS / 1000) / 60;
        int seconds = (Video.RECOMMENDED_LENGTH_IN_MILLIS / 1000) % 60;

        if (_tvTimeTotal != null)
            _tvTimeTotal.setText(String.format(AppVersion.flavor().getLocale(), " / %02d:%02d", minutes, seconds));

        _preview.setSurfaceTextureListener(_surfaceTextureListener);

        if (!PermissionUtils.hasRecordVideoPermissions(this)) {
            PermissionUtils.requestVideoPermissions(this, PERMISSION_CAMERA);
        } else {
            _cameraReadyObserver.notify(CameraReadyObserver.PERMISSIONS_GRANTED);
        }
        _mediaRecorderHelper = new MediaRecorderHelper();
        _recordButtonOnClickListener = new RecordButtonOnClickListener();

        //        initCameraSwitch(_ivCamera);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (_mediaRecorderHelper.isCameraReady()) {
            if (_mediaRecorderHelper.isRecording()) {
                _mediaRecorderHelper.stop();
                _mediaRecorderHelper.step6VideoRecording();
            } else {
                // this is actually the same thing that _mediaRecorderHelper.step6VideoRecording();
                _mediaRecorderHelper.step9();
                _mediaRecorderHelper.step10();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!_mediaRecorderHelper.isRecording())
            updateUIStopRecording();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PREVIEW) {
            if (resultCode == Activity.RESULT_OK) {
                updateUIAcceptConfirmation();
                if (_videoUri != null)
                    setResult(Activity.RESULT_OK, getIntent().setData(_videoUri));
                finish();
            } else {
                updateUIDeclineConfirmation();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CAMERA:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED) {

                    _cameraReadyObserver.notify(CameraReadyObserver.PERMISSIONS_GRANTED);
                } else {
                    ToastUtils.showToast(getString(R.string.activityCamera_toast_permission_denied));
                    finish();
                }
        }

    }

    //endregion METHODS LIFECYCLE

    //region INNER CLASSES

    private class RecordButtonOnClickListener implements View.OnClickListener {
        private final MediaRecorderOnInfoListener _mediaRecorderOnInfoListener = new MediaRecorderOnInfoListener();
        private boolean _isRecording = false;

        @Override
        public void onClick(View v) {
            if (_isRecording) {
                // stop recording
                endVideoRecording();
                _ivCamera.setVisibility(View.VISIBLE);

            } else {
                // start recording
                _mediaRecorderHelper.step2VideoRecording();
                try {
                    _mediaRecorderHelper.step3VideoRecording(_mediaRecorderOnInfoListener);
                    _ivCamera.setVisibility(View.GONE);
                } catch (IOException e) {
                    Toast.makeText(CameraActivity.this, getString(R.string.activity_camera_tFailed), Toast.LENGTH_LONG).show();
                    finish();
                }
                _mediaRecorderHelper.record();
                updateUIStartRecording();
                _isRecording = true;
            }
        }

        void setRecording(boolean isRecording) {
            _isRecording = isRecording;
        }
    }

    private class CameraButtonOnClickListener implements View.OnClickListener {
        private final ImageView _ivCamera;
        private int _currentCamera = Camera.CameraInfo.CAMERA_FACING_FRONT;

        private CameraButtonOnClickListener(ImageView ivCamera) {
            _ivCamera = ivCamera;
        }

        @Override
        public void onClick(View v) {
            _mediaRecorderHelper.step9();
            _mediaRecorderHelper.step10();

            changeCamera();

            _mediaRecorderHelper.step1VideoRecording(CameraActivity.this, _preview.getSurfaceTexture(), _currentCamera);

            _ivCamera.setVisibility(View.GONE);
        }

        private void changeCamera() {
            if (_currentCamera == Camera.CameraInfo.CAMERA_FACING_BACK) {
                _currentCamera = Camera.CameraInfo.CAMERA_FACING_FRONT;
                _ivCamera.setImageResource(R.drawable.ic_camera_rear);
            } else {
                _currentCamera = Camera.CameraInfo.CAMERA_FACING_BACK;
                _ivCamera.setImageResource(R.drawable.ic_camera_front);
            }
        }

    }

    private class ElapsedTimeRunnable implements Runnable {
        final int timeStart = 0;
        final int timeEnd = Video.MAX_LENGTH_IN_MILLIS;
        final int updateWait = 1000; // 1 sec
        int timeElapsed = timeStart;

        @Override
        public void run() {
            while (timeElapsed < timeEnd) {
                timeElapsed += 1000; // 1 sec
                try {
                    Thread.sleep((long) updateWait);
                    _tvTimeElapsed.post(new ElapsedTimeUpdated(timeElapsed));
                } catch (InterruptedException e) {
                    break;
                }
            }
        }
    }

    private class ElapsedTimeUpdated implements Runnable {
        private final int _timeElapsed;

        ElapsedTimeUpdated(int timeElapsed) {
            _timeElapsed = timeElapsed;
        }

        @Override
        public void run() {
            int minutes = (_timeElapsed / 1000) / 60;
            int seconds = (_timeElapsed / 1000) % 60;

            // if 4/5 of the time is consumed, make time elapsed blink
            if (_timeElapsed > ((4 * Video.RECOMMENDED_LENGTH_IN_MILLIS) / 5)) {
                if ((seconds % 2) > 0) {
                    _tvTimeElapsed.setTextColor(ContextCompat.getColor(_tvTimeElapsed.getContext(), R.color.red_1));
                } else {
                    _tvTimeElapsed.setTextColor(ContextCompat.getColor(_tvTimeElapsed.getContext(), R.color.white));
                }
            }
            if (_timeElapsed < ((9 * Video.RECOMMENDED_LENGTH_IN_MILLIS) / 10)) {
                _tvTimeElapsed.setText(String.format(AppVersion.flavor().getLocale(), "%02d:%02d", minutes, seconds));
            } else {
                _tvTimeLabel.setVisibility(View.GONE);
                _tvTimeElapsed.setText(R.string.activity_camera_tvElapsed_finish);
                _tvTimeTotal.setVisibility(View.GONE);

            }
        }

    }

    private class CameraReadyObserver {
        static final int PERMISSIONS_GRANTED = 0;
        static final int SURFACE_READY = 1;
        private boolean _arePermissionsGranted = false;
        private boolean _isSurfaceReady = false;

        void notify(int event) {
            switch (event) {
                case PERMISSIONS_GRANTED:
                    _arePermissionsGranted = true;
                    break;
                case SURFACE_READY:
                    _isSurfaceReady = true;
                    break;
            }

            if (_arePermissionsGranted && _isSurfaceReady) {
                _mediaRecorderHelper.step1VideoRecording(CameraActivity.this, _preview.getSurfaceTexture(), Camera.CameraInfo.CAMERA_FACING_FRONT);

            }

        }
    }

    private class MediaRecorderOnInfoListener implements MediaRecorder.OnInfoListener {

        @Override
        public void onInfo(MediaRecorder mr, int what, int extra) {
            if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                if (_mediaRecorderHelper.isRecording())
                    endVideoRecording();
            }
        }

    }

    //endregion INNER CLASSES

}
