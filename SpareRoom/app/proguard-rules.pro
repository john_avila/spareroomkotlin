# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/manuel/android-sdks/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService
-keep public class android.app.SearchManager
-keep public class android.support.v4.view.MenuItemCompat
-keep public class * extends com.spareroom.lib.core.IObserver

-keep class android.support.v7.internal.** { *; }
-keep interface android.support.v7.internal.** { *; }
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }
-keep interface com.spareroom.lib.core.IObserver { *; }


#keep all classes that might be used in XML layouts
-keep public class * extends android.view.View
-keep public class * extends android.app.Fragment
-keep public class * extends android.support.v4.app.Fragment

#keep all public and protected methods that could be used by java reflection

-keepclassmembernames class * {
    public protected <methods>;
}

-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}



#keep classes required by google play services
-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

-dontwarn **CompatHoneycomb
-dontwarn org.htmlcleaner.*

-keep class com.android.vending.billing.**

# -keep class android.support.v7.widget.**
-keep class android.support.v4.**
-keep class android.support.v7.**
-keep class com.facebook.**
-keep class com.flatshare.android.**
-keep class com.google.android.**

-dontwarn android.support.v4.**
-dontwarn android.support.v7.**
-dontwarn com.facebook.**
-dontwarn com.flatshare.android.**
-dontwarn com.google.android.**

-assumenosideeffects class android.util.Log {
   public static *** v(...);
   public static *** i(...);
   public static *** w(...);
   public static *** d(...);
   public static *** e(...);
}

# uncomment to debug
# -renamesourcefileattribute SourceFile

# Meaningful crash reports
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception

# OkHttp & Okio
-dontwarn okio.**
-dontwarn okhttp3.**
-dontwarn javax.annotation.Nullable
-dontwarn javax.annotation.ParametersAreNonnullByDefault
-keep class okhttp3.** {*;}

# Glide - image loading library
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

# Dagger
-dontwarn dagger.internal.codegen.**
-dontwarn com.google.errorprone.annotations.*
-keepclassmembers,allowobfuscation class * {
    @javax.inject.* *;
    @dagger.* *;
    <init>();
}

-keep class dagger.* { *; }
-keep class javax.inject.* { *; }
-keep class * extends dagger.internal.Binding
-keep class * extends dagger.internal.ModuleAdapter
-keep class * extends dagger.internal.StaticInjection

# Android architecture components: Lifecycle
-keepclassmembers class * implements android.arch.lifecycle.LifecycleObserver {
    <init>(...);
}
-keepclassmembers class * extends android.arch.lifecycle.ViewModel {
    <init>(...);
}
-keepclassmembers class android.arch.lifecycle.Lifecycle$State { *; }
-keepclassmembers class android.arch.lifecycle.Lifecycle$Event { *; }
-keepclassmembers class * {
    @android.arch.lifecycle.OnLifecycleEvent *;
}
-keep class * implements android.arch.lifecycle.LifecycleObserver {
    <init>(...);
}

# AppsFlyer
-dontwarn com.appsflyer.AFKeystoreWrapper
-dontwarn com.appsflyer.GcmInstanceIdListener

# Play Install Referrer
-dontwarn com.android.installreferrer

# GSON
-dontwarn sun.misc.**
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

# Application classes that will be serialized/deserialized over Gson
-keep class com.spareroom.model.business.** { *; }

