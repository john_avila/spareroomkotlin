buildscript {

    repositories {
        google()
        jcenter()
        maven(url = "https://maven.fabric.io/public")
    }

    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.KOTLIN}")
        classpath("io.fabric.tools:gradle:${Versions.FABRIC_PLUGIN}")
        classpath("com.android.tools.build:gradle:${Versions.GRADLE_PLUGIN}")
        classpath("com.google.gms:google-services:${Versions.GOOGLE_SERVICES_PLUGIN}")
        classpath("com.google.firebase:firebase-plugins:${Versions.FIREBASE_PLUGIN}")
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        maven(url = "https://maven.fabric.io/public")
    }
}
